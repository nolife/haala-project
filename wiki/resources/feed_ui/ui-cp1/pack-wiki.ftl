
<@onPackContent "left-column">

</@>

<@onPackContent "right-column">

</@>

<@onPackContent "right-column-top">
  <@h.onPage index="02wikiView,02wikiSearch">
    <@htm.form id="wiki-search">
      <p><@label "wiki.10"/></p>
      <input type="text" maxlength="100" value="${requestParams.query!}"/>
      <button><@label "btn.search"/></button>
    </@>
  </@>
</@>

<@onPackContent "account-items">
  <@h.onRole role="wiki">
    <a class="pack-wiki" href="/cp/wiki/main.htm" title="<@label "wiki.3"/>" ><@label "wiki.2"/></a>
  </@>
</@>

<@onPackContent "user-view-table">

</@>
