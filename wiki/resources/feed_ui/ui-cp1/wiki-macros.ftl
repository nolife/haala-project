<#import "/web/common.ftl" as c >
<#import "/web/common-htm.ftl" as htm >

<#macro wikiArticleTitle article >
  <h1 class="wiki-article-title">
    <@wiki.articleText article=article key="title" />
  </h1>
</#macro>

<#macro wikiArticleText article >
  <section class="wiki-article-text">
    <@wiki.articleText article=article key="text" />
  </section>
</#macro>

<#macro wikiArticleItems article >
  <#if article.items()?size != 0 >
    <nav class="wiki-article-items">
      <ul>
        <@wiki.listArticleItems article=article ; x>
          <li><a href="view.htm?ref=${x.ref()}" <#if x.status() == 0> class="private" </#if> ><@wiki.articleText article=x key="title" /></a></li>
        </@wiki.listArticleItems>
      </ul>
    </nav>
  </#if>
</#macro>
