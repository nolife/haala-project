
(function($) {

  Haala.Misc.onDocReady(function() {
    initNavigation();
    initBookList();
  });


  var initNavigation = function() {
    $("#context-navigation").kdxMenu({
      items: [
        { element: "#context-navigation .add-book",
          gotoFn: function(/*json*/ item, /*json*/ model) { addFrame(); }}
      ]
    });
  };



  var initBookList = function() {
    var msgFn = function(/*string*/ msg, /*json*/ tokens) {
      return Haala.Misc.messageMapping("#page-messages", msg, tokens);
    };

    var listModel = {
      loadItemsFn: function(/*json*/ model, /*fn*/ callback) {
        Haala.AquaList.Integ.loadItemsFn(WikiArticleSearchF.dsearch, { target: 2 }, model, callback);
      },
      rowElementFn: function(/*json*/ vo, /*int*/ position, /*json*/ model) {
        var card = null;
        if (vo != null) {
          card = $("#page-templates .data-item").clone();
          card.find("a").attr("href", "view.htm?ref="+vo.ref);
          card.find("img").attr("src", "/web/wiki/book-"+(vo.status == 1 ? "published" : "private")+".png");
          card.find("p.title").html(vo.title);
          card.aclick(function() {
            document.location = "view.htm?ref="+vo.ref;
          });
        }
        return card;
      }
    };
    var list = $("#article-list .rows").aquaList(
        $.extend(true, {},
            Haala.AquaList.Integ.DefaultOptions,
            { rows: 999 },
            listModel));
    list.addRenderedFn(Haala.Code.stretch);

  };

  var addFrame = function() {
    var msgFn = function(/*string*/ msg, /*json*/ tokens) {
      return Haala.Misc.messageMapping("#page-messages", msg, tokens);
    };

    var panel = $("#add-frame").kdxPanel(
        $.extend(true, {}, Haala.KdxPanel.Integ.DefaultOptions, {
          input: [
            { element: "#add-frame .title input", key: "title",
              validate: { element: "#add-frame .title .error", require: true }}
          ]
        }));

    $("#add-frame").dialog({
      zIndex: 70, width: 900, height: 170, resizable: false,
      buttons: [
        { text: msgFn("button-ok"), click: function() {
          if (panel.validate()) {
            var command = Haala.Misc.collectInput(panel.collect(), ["title"]);
            command.type = 1;
            addArticle(command);
            $(this).dialog("close");
          } else {
            $("#add-frame .error:visible:first").toViewport();
          }
        }},
        { text: msgFn("button-cancel"), click: function() {
          $(this).dialog("close");
        }}],
      title: msgFn("add-title"),
      close: function() {
        panel.validate("reset");
        $("#add-frame input").val("");
      }
    });

    var addArticle = function(/*json*/ command) {
      Haala.AJAX.call("body", WikiArticleF.create, command,
          function(data) { document.location = "edit.htm?ref="+data.result; return Haala.AJAX.DO_NOTHING; });
    };

  };


})(jQuery);

