<#import "/web/common.ftl" as c >
<#import "/web/common-htm.ftl" as htm >
<#include "/web/macros.ftl:"+currentSite >
<#include "/web/wiki-macros.ftl:"+currentSite >

<#if articles?? && articles?size != 0 >

  <h3><@wiki.articleText article=book key="title" /></h3>

  <ul class="article-list">
    <#list articles as o>
      <li><a href="view.htm?ref=${o.ref()}"><@wiki.articleText article=o key="title" /></a></li>
    </#list>
  </ul>

<#else>

  <@label "002"/>

</#if>

<@htm.nodiv "quick-navigation-content">
  <a href="main.htm"><@label "003"/></a>
</@>

<@htm.nodiv "page-ivars">
  { ref: "${requestParams.ref!}" }
</@>
