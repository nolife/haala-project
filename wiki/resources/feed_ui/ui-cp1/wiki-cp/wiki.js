
var Haala = Haala || {};

(function($) {

  Haala.Wiki = {

    initSearch: function() {
      var ivars = Haala.Misc.evalJSON("#page-ivars");
      if (!ivars) return;

      var submitHandler = function() {
        var query = encodeURIComponent($("#wiki-search input").val());
        document.location = "search.htm?ref="+ivars.ref+"&query="+query;
      };
      $("#wiki-search").submit(submitHandler);
      $("#wiki-search button").button();
    }

  };

})(jQuery);
