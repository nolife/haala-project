
(function($) {

  Haala.Misc.onDocReady(function() {
    initEditForm();
  });


  var initEditForm = function() {
    var ivars = Haala.Misc.evalJSON("#page-ivars");
    if (!ivars) return;

    $("#page-form .submit button").button();

    Haala.Misc.tinymce4("#page-form textarea:not(.norich)", {
      plugins: "autolink lists table image hr link advlist contextmenu media fullscreen paste code directionality visualchars nonbreaking anchor charmap textcolor",
      toolbar1: "code | undo redo | bold italic underline strikethrough | alignleft aligncenter alignright alignjustify | formatselect fontselect fontsizeselect",
      toolbar2: "table | bullist numlist | outdent indent | forecolor backcolor | link unlink anchor image media | charmap hr nonbreaking | ltr rtl | fullscreen",
      menubar: true,
      width : "730" // tinymce-4.9.1 bug: width of textarea calculated incorrectly
    });

    $("#page-form .items ul").sortable();
    $("#page-form .items ul").disableSelection();

    var finp = Haala.Util.panelFormInput;
    var input = [
      finp("published"),
      finp("title", { require: true })
    ];

    var TYPE_ARTICLE = 0;
    if (ivars.type == TYPE_ARTICLE) {
      input.push({ element: "#page-form .text textarea", key: "text" });
    }

    var panel = $("#page-form").kdxPanel(
        $.extend(true, {}, Haala.KdxPanel.Integ.DefaultOptions, {
          input: input
        }));

    Haala.Util.formSubmitPanel(panel, function(/*json*/ container) {
      var command = Haala.Misc.collectInput(container, ["published", "title", "text"]);
      command.articleId = ivars.articleId;

      command.domains = "";
      $("#page-form .domains input:checked").each(function() {
        command.domains += ","+$(this).attr("data-id");
      });
      if (command.domains) command.domains = command.domains.substring(1);

      command.items = "";
      $("#page-form .items li").each(function() {
        command.items += ","+$(this).attr("data-id");
      });
      if (command.items) command.items = command.items.substring(1);

      command.options = [];
      editArticle(command);
    });

    var editArticle = function(/*json*/ command) {
      Haala.AJAX.call("body", WikiArticleF.edit, command,
          function(data) { document.location = "view.htm?ref="+ivars.ref; return Haala.AJAX.DO_NOTHING; });
    };

  };

})(jQuery);
