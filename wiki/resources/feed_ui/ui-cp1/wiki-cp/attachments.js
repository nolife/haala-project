
(function($) {

  Haala.Misc.onDocReady(function() {
    initAttachmentsForm();
  });


  var initAttachmentsForm = function() {
    var ivars = Haala.Misc.evalJSON("#page-ivars");
    if (!ivars) return;

    var msgFn = function(/*string*/ msg, /*json*/ tokens) {
      return Haala.Misc.messageMapping("#page-messages", msg, tokens);
    };

    initUploadForm();

    if ($("#content .attachments .item").length) $("#content .attachments").show();

    $("#content .attachments a").click(function(event) {
      event.preventDefault();
      Haala.Messages.showOK($(this).attr("href"), msgFn("title-url"));
      has = true;
    });

    $("#content .attachments button").button().click(function() {
      Haala.Messages.showYN(msgFn("attachments-delete"), "title-confirm", function() {
        var command = { articleId: ivars.articleId };
        command.attachments = "";
        $("#content .attachments input:checked").each(function() {
          command.attachments += ","+$(this).attr("data-id");
        });
        if (command.attachments) command.attachments = command.attachments.substring(1);
        removeAttachments(command);
      });
    });

    var removeAttachments = function(/*json*/ command) {
      Haala.AJAX.call("body", WikiArticleF.removeAttachments, command,
        function() { document.location = document.location.href; return Haala.AJAX.DO_NOTHING; });
    };

  };



  var initUploadForm = function() {
    var ivars = Haala.Misc.evalJSON("#page-ivars");

    $("#upload-form .submit button").button();

    var integ = Haala.UploadForm.Integ;
    var form = $("#upload-form").uploadForm(
      $.extend(true, {}, integ.DefaultOptions, {
        uploadSuccessFn: function(/*json*/ model) {
          integ.collapseForm(model);
          Haala.AJAX.call("body", WikiArticleF.addAttachments, { articleId: ivars.articleId },
            function() { document.location = document.location.href; });
        }
      }));
    integ.resetExpandableForm(form.model);
    integ.bindSubmitTo("#upload-form .submit button", form.model);
  };


})(jQuery);
