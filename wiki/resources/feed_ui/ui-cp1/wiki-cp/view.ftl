<#import "/web/common.ftl" as c >
<#import "/web/common-htm.ftl" as htm >
<#include "/web/macros.ftl:"+currentSite >
<#include "/web/wiki-macros.ftl:"+currentSite >

<#assign TYPE_ARTICLE=0 TYPE_BOOK=1 />

<#if article??>

  <@wiki.articleParent var="parent" article=article />

  <@wikiArticleTitle article />
  <@wikiArticleText article />
  <@wikiArticleItems article />

  <@htm.nodiv "context-navigation-content">
    <a class="edit-article" href="edit.htm?ref=${article.ref()}" title="<@label "010"/>"><@label "011"/></a>
    <a class="add-article" href="#" title="<@label "006"/>"><@label "007"/></a>
    <a class="delete-article" href="#" title="<@label "008"/>"><@label "009"/></a>
    <#if article.type()==TYPE_BOOK >
      <a class="attachments" href="attachments.htm?ref=${article.ref()}" title="<@label "012"/>"><@label "013"/></a>
    </#if>
  </@>

  <@htm.nodiv "quick-navigation-content">
    <#if parent??><a href="view.htm?ref=${parent.ref()}"><@label "004"/></a></#if>
    <a href="main.htm"><@label "003"/></a>
  </@>

  <@htm.nodiv "page-ivars">
    { articleId: "${article.id()}", ref: "${article.ref()}", parentRef: "${(parent.ref())!}" }
  </@>

  <@htm.nodiv "page-messages">
    <div data-key="button-ok"><@label "btn.ok"/></div>
    <div data-key="button-cancel"><@label "btn.cancel"/></div>
    <div data-key="add-title"><@label "002"/></div>
    <div data-key="confirm-delete"><@label "014"/></div>
  </@>

  <@htm.nodiv "page-templates">

    <div id="add-frame">
      <div class="form-panel">
        <div class="cell title">
          <label><@label "005"/></label>
          <input type="text" maxlength="100"/>
          <p class="error"></p>
        </div>
      </div>
    </div>

  </@>

<#else>

  <@label "wiki.4"/>

</#if>
