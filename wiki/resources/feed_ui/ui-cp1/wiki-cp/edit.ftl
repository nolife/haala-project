<#import "/web/common.ftl" as c >
<#import "/web/common-htm.ftl" as htm >
<#include "/web/macros.ftl:"+currentSite >

<#function x_hasDomain domainId>
  <#list article.domains() as d>
    <#if d.id()==domainId><#return true></#if>
  </#list>
  <#return false>
</#function>

<#assign TYPE_ARTICLE=0 TYPE_BOOK=1 />

<#if article??>

  <@htm.form id="page-form">
    <div class="form-panel">
      <div class="cell published">
        <label><@label "002"/></label>
        <input type="checkbox" <#if article.status()==1> checked="true" </#if> />
        <p class="error"></p>
      </div>
      <div class="cell title">
        <label><@label "005"/></label>
        <input type="text" maxlength="100" value="<@wiki.articleText article=article key="title" />" />
        <p class="error"></p>
      </div>
      <#if article.type()==TYPE_ARTICLE >
        <div class="cell text">
          <textarea><@wiki.articleText article=article key="text" output="html-lite"/></textarea>
        </div>
      </#if>
      <#if article.type()==TYPE_BOOK >
        <div class="cell domains">
          <label><@label "004"/></label>
          <@h.listUserDomains ; x >
            <p><input data-id="${x.id()}" type="checkbox" <#if x_hasDomain(x.id()) > checked="true" </#if> /><span>${x.domain()}</span></p>
          </@>
        </div>
      </#if>
      <#if article.items()?size != 0 >
        <div class="cell items">
          <ul>
            <@wiki.listArticleItems article=article ; x>
              <li class="ui-state-default" data-id="${x.id()}">
                <span class="ui-icon ui-icon-arrowthick-2-n-s"></span>
                <@wiki.articleText article=x key="title" />
              </li>
            </@>
          </ul>
        </div>
      </#if>
      <div class="cell submit">
        <button><@label "btn.save"/></button>
      </div>
    </div>
  </@>

  <@htm.nodiv "quick-navigation-content">
    <a href="view.htm?ref=${article.ref()}"><@label "003"/></a>
  </@>

  <@htm.nodiv "page-ivars">
    { articleId: "${article.id()}", ref: "${article.ref()}", type: "${article.type()}" }
  </@>

<#else>

  <@label "wiki.4"/>

</#if>
