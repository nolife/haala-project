
/* js::import

 { path = web/pages/cp/wiki; js = wiki }

 */

(function($) {

  Haala.Misc.onDocReady(function() {
    Haala.Wiki.initSearch();
  });


})(jQuery);
