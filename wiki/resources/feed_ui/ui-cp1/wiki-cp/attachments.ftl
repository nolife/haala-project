<#import "/web/common.ftl" as c >
<#import "/web/common-htm.ftl" as htm >
<#include "/web/macros.ftl:"+currentSite >
<#include "/web/wiki-macros.ftl:"+currentSite >

<#if article??>

  <@htm.form id="upload-form" mode="upload">
    <div class="form-panel">
      <h3 class="cell">
        <@label"wiki.9"/>
        <span class="expand" title="<@label "wiki.6"/>"></span>
      </h3>
      <div class="slide">
        <div class="file-upload">
          <@h.paramLimit var="uplim" key="upload"/>
          <#list 1..uplim as z>
            <div class="cell" data-row="${z}">
              <label><#if z==1 ><@label "wiki.7"/><#else> &nbsp; </#if></label>
              <input name="dat${z}" type="file" size="20"/>
              <span data-clear="dat${z}" title="<@label "wiki.8"/>" ></span>
            </div>
          </#list>
        </div>
        <div class="cell submit">
          <button><@label "btn.upload"/></button>
        </div>
      </div>
    </div>
  </@>

  <div class="attachments">
    <@wiki.listArticleAttachments article=article ; x >
      <#assign tmpurl="/"+x.file().path />
      <div class="item">
        <input type="checkbox" data-id="${x.id()}"/>
        <img src="${tmpurl}"/>
        <a href="${tmpurl}">${x.file().name()}</a>
      </div>
    </@>
    <div class="nofloat"></div>
    <button><@label "btn.delete"/></button>
  </div>

  <@htm.nodiv "page-ivars">
    { articleId: "${article.id()}", ref: "${article.ref()}" }
  </@>

  <@htm.nodiv "quick-navigation-content">
  <a href="view.htm?ref=${article.ref()}"><@label "003"/></a>
  </@>

  <@htm.nodiv "page-messages">
    <div data-key="attachments-delete"><@label "002"/></div>
    <div data-key="title-url"><@label "004"/></div>
  </@>

<#else>

  <@label "wiki.4"/>

</#if>

