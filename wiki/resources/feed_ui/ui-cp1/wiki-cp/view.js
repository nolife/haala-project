
/* js::import

 { path = web/pages/cp/wiki; js = wiki }

 */

(function($) {

  Haala.Misc.onDocReady(function() {
    initNavigation();
    Haala.Wiki.initSearch();
  });



  var initNavigation = function() {
    $("#context-navigation").kdxMenu({
      items: [
        { element: "#context-navigation .add-article",
          gotoFn: function(/*json*/ item, /*json*/ model) { addFrame(); }},
        { element: "#context-navigation .delete-article",
          gotoFn: function(/*json*/ item, /*json*/ model) { removeArticle(); }}
      ]
    });
  };



  var removeArticle = function() {
    var ivars = Haala.Misc.evalJSON("#page-ivars");
    if (!ivars) return;

    var msgFn = function(/*string*/ msg, /*json*/ tokens) {
      return Haala.Misc.messageMapping("#page-messages", msg, tokens);
    };

    Haala.Messages.showYN(msgFn("confirm-delete"), "title-confirm", function() {
      Haala.AJAX.call("body", WikiArticleF.remove, { articleId: ivars.articleId },
          function() {
            document.location = ivars.parentRef ? "view.htm?ref="+ivars.parentRef : "main.htm";
            return Haala.AJAX.DO_NOTHING;
          });
    });

  };


  var addFrame = function() {
    var ivars = Haala.Misc.evalJSON("#page-ivars");
    if (!ivars) return;

    var msgFn = function(/*string*/ msg, /*json*/ tokens) {
      return Haala.Misc.messageMapping("#page-messages", msg, tokens);
    };

    var panel = $("#add-frame").kdxPanel(
        $.extend(true, {}, Haala.KdxPanel.Integ.DefaultOptions, {
          input: [
            { element: "#add-frame .title input", key: "title",
              validate: { element: "#add-frame .title .error", require: true }}
          ]
        }));

    $("#add-frame").dialog({
      zIndex: 70, width: 900, height: 170, resizable: false,
      buttons: [
        { text: msgFn("button-ok"), click: function() {
          if (panel.validate()) {
            var command = Haala.Misc.collectInput(panel.collect(), ["title"]);
            command.type = 0;
            command.parentId = ivars.articleId;
            addArticle(command);
            $(this).dialog("close");
          } else {
            $("#add-frame .error:visible:first").toViewport();
          }
        }},
        { text: msgFn("button-cancel"), click: function() {
          $(this).dialog("close");
        }}],
      title: msgFn("add-title"),
      close: function() {
        panel.validate("reset");
        $("#add-frame input").val("");
      }
    });

    var addArticle = function(/*json*/ command) {
      Haala.AJAX.call("body", WikiArticleF.create, command,
          function(data) { document.location = "edit.htm?ref="+data.result; return Haala.AJAX.DO_NOTHING; });
    };

  };

})(jQuery);
