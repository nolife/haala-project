<#import "/web/common.ftl" as c >
<#import "/web/common-htm.ftl" as htm >
<#include "/web/macros.ftl:"+currentSite >

<div id="article-list">
  <div class="rows"></div>
  <div class="nofloat"></div>
</div>

<@htm.nodiv "context-navigation-content">
  <a class="add-book" href="#" title="<@label "002"/>"><@label "003"/></a>
</@>

<@htm.nodiv "page-messages">
  <div data-key="button-ok"><@label "btn.ok"/></div>
  <div data-key="button-cancel"><@label "btn.cancel"/></div>
  <div data-key="add-title"><@label "005"/></div>
</@>

<@htm.nodiv "page-templates">

  <div class="data-item">
    <a href="#">
      <img src=""/>
      <p class="title"></p>
    </a>
  </div>

  <div id="add-frame">
    <div class="form-panel">
      <div class="cell title">
        <label><@label "004"/></label>
        <input type="text" maxlength="100"/>
        <p class="error"></p>
      </div>
    </div>
  </div>

</@>

