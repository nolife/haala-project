-----------------------------------------------------------
--   DROP
-----------------------------------------------------------

  DROP TABLE WIKI_article_attachment_map CASCADE;

  DROP TABLE WIKI_article_options CASCADE;
  DROP SEQUENCE WIKI_article_options_seq;

  DROP TABLE WIKI_articles CASCADE;
  DROP SEQUENCE WIKI_articles_seq;

  DROP TABLE WIKI_wiki_packs CASCADE;

  DROP TABLE WIKI_article_domain_map CASCADE;

  delete from packs where pack = 'wiki';
