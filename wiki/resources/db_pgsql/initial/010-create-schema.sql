-----------------------------------------------------------
--   TABLES
-----------------------------------------------------------

CREATE TABLE WIKI_wiki_packs
(
  id bigint NOT NULL,
  CONSTRAINT WIKI_wiki_packs_pkey PRIMARY KEY (id)
)
WITHOUT OIDS;



CREATE TABLE WIKI_articles
(
  id bigint NOT NULL,
  ref character varying(100) NOT NULL,
  status integer NOT NULL,
  type integer NOT NULL,
  index integer NOT NULL,
  pack_id bigint,
  parent_id bigint,
  CONSTRAINT WIKI_articles_pkey PRIMARY KEY (id)
)
WITHOUT OIDS;



CREATE TABLE WIKI_article_domain_map
(
  article_id bigint NOT NULL,
  domain_id bigint NOT NULL
)
WITHOUT OIDS;


CREATE TABLE WIKI_article_options
(
  id bigint NOT NULL,
  "type" integer NOT NULL,
  "value" text,
  article_id bigint,
  CONSTRAINT WIKI_article_options_pkey PRIMARY KEY (id)
)
WITHOUT OIDS;



CREATE TABLE WIKI_article_attachment_map
(
  article_id bigint NOT NULL,
  attachment_id bigint NOT NULL
)
WITHOUT OIDS;













-----------------------------------------------------------
--   CONSTRAINTS
-----------------------------------------------------------


ALTER TABLE WIKI_wiki_packs
  ADD CONSTRAINT WIKI_wiki_pack_sub_fk FOREIGN KEY (id)
      REFERENCES packs (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION;



ALTER TABLE WIKI_articles
  ADD CONSTRAINT WIKI_wiki_pack_fk FOREIGN KEY (pack_id)
      REFERENCES WIKI_wiki_packs (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION;

ALTER TABLE WIKI_articles
  ADD CONSTRAINT WIKI_articles_ref_key UNIQUE(ref);

ALTER TABLE WIKI_articles
  ADD CONSTRAINT WIKI_article_self_fk FOREIGN KEY (parent_id)
      REFERENCES WIKI_articles (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION;



ALTER TABLE WIKI_article_domain_map
  ADD CONSTRAINT WIKI_domain_article_fk FOREIGN KEY (article_id)
      REFERENCES WIKI_articles (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION;

ALTER TABLE WIKI_article_domain_map
  ADD CONSTRAINT WIKI_article_domain_fk FOREIGN KEY (domain_id)
      REFERENCES domains (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION;



ALTER TABLE WIKI_article_options
  ADD CONSTRAINT WIKI_article_option_fk FOREIGN KEY (article_id)
      REFERENCES WIKI_articles (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION;



ALTER TABLE WIKI_article_attachment_map
  ADD CONSTRAINT WIKI_attachment_article_fk FOREIGN KEY (article_id)
      REFERENCES WIKI_articles (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION;

ALTER TABLE WIKI_article_attachment_map
  ADD CONSTRAINT WIKI_article_attachment_fk FOREIGN KEY (attachment_id)
      REFERENCES attachments (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION;








-----------------------------------------------------------
--   INDEXES
-----------------------------------------------------------


CREATE INDEX "WIKI_articles_ind1"
  ON WIKI_articles (ref);










-----------------------------------------------------------
--   SEQUENCES
-----------------------------------------------------------


CREATE SEQUENCE WIKI_articles_seq
  INCREMENT 1
  MINVALUE 1
  MAXVALUE 9223372036854775807
  START 1
  CACHE 1;



CREATE SEQUENCE WIKI_article_options_seq
  INCREMENT 1
  MINVALUE 1
  MAXVALUE 9223372036854775807
  START 1
  CACHE 1;
