if (typeof dwr == 'undefined' || dwr.engine == undefined) throw new Error('You must include DWR engine before including this file');

(function() {
  if (dwr.engine._getObject("WikiArticleF") == undefined) {
    var p;

    p = {};
    p._path = '/dwr';

    p.addAttachments = function(p0, callback) {
      return dwr.engine._execute(p._path, 'WikiArticleF', 'addAttachments', arguments);
    };

    p.removeAttachments = function(p0, callback) {
      return dwr.engine._execute(p._path, 'WikiArticleF', 'removeAttachments', arguments);
    };

    p.remove = function(p0, callback) {
      return dwr.engine._execute(p._path, 'WikiArticleF', 'remove', arguments);
    };

    p.create = function(p0, callback) {
      return dwr.engine._execute(p._path, 'WikiArticleF', 'create', arguments);
    };

    p.edit = function(p0, callback) {
      return dwr.engine._execute(p._path, 'WikiArticleF', 'edit', arguments);
    };

    dwr.engine._setObject("WikiArticleF", p);
  }
})();
