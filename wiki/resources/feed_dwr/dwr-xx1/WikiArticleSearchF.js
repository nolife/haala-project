if (typeof dwr == 'undefined' || dwr.engine == undefined) throw new Error('You must include DWR engine before including this file');

(function() {
  if (dwr.engine._getObject("WikiArticleSearchF") == undefined) {
    var p;

    p = {};
    p._path = '/dwr';

    p.dfilter = function(p0, callback) {
      return dwr.engine._execute(p._path, 'WikiArticleSearchF', 'dfilter', arguments);
    };

    p.dsearch = function(p0, callback) {
      return dwr.engine._execute(p._path, 'WikiArticleSearchF', 'dsearch', arguments);
    };

    p.search = function(p0, callback) {
      return dwr.engine._execute(p._path, 'WikiArticleSearchF', 'search', arguments);
    };

    dwr.engine._setObject("WikiArticleSearchF", p);
  }
})();
