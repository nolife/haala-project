package com.coldcore.haala
package web.core
package wiki

import com.coldcore.haala.service.DomainService
import javax.servlet.http.HttpServletRequest
import com.coldcore.haala.domain.wiki.Article

class WikiRequestExt(rx: WikiRequestX) extends CoreActions {
  private val request = rx.request

  /** Check if current domain supports supplied article. */
  def isDomainSupportsArticle(article: Article): Boolean = isDomainSupports(article, request)
}

case class WikiRequestX(override val request: HttpServletRequest) extends RequestX(request) {
  val xwiki = new WikiRequestExt(this)
}
