package com.coldcore.haala
package service
package wiki

import service.FileService.Body
import org.springframework.transaction.annotation.{Propagation, Transactional}
import core.{Constants, VirtualPath}
import com.coldcore.haala.domain.wiki.{Article, WikiPack}
import domain.Attachment
import web.core.CustomMultipartResolver.TemporaryFile
import com.coldcore.haala.service.wiki.Util._

trait AttachmentService {
  def createDefault(article: Article, file: TemporaryFile): Attachment
}

@Transactional
class AttachmentServiceImpl extends BaseService with AttachmentService {
  /** Attach to article's attachments. */
  private def create(article: Article, body: Body, position: Int, typ: Int, name: String): Attachment = {
    val o = sc.get[service.AttachmentService].create(position, typ)
    sc.get[service.AttachmentService].attachBody(o, VirtualPath("files/"+packPath("ar", article.ref, "a"), name), Constants.SITE_PUBLIC, body)
    o
  }

  /** Attach to article's attachments. */
  @Transactional (readOnly = false)
  override def createDefault(article: Article, file: TemporaryFile): Attachment = {
    create(article, file.body, 0, 0, file.name)
  }
}
