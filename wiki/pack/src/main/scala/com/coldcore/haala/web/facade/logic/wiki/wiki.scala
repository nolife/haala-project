package com.coldcore.haala
package web
package facade.logic
package wiki

import scala.beans.BeanProperty
import com.coldcore.haala.web.facade.annotations.{ErrorCR, ErrorCRs, LogClass, ValidatorClass}
import com.coldcore.haala.web.facade.validator.Validator
import com.coldcore.haala.security.annotations.SecuredRole
import javax.servlet.http.HttpServletRequest

import com.coldcore.haala.core.NamedLog
import com.coldcore.haala.web.facade.vo.CallResult
import com.coldcore.haala.service.exceptions.{LimitExceededException, ObjectExistsException, ObjectNotFoundException}
import com.coldcore.haala.web.core.{SearchHelper, WebConstants}
import com.coldcore.haala.service.wiki.ArticleService.{CreateIN, OptionIN, RemoveAttachmentsIN, UpdateIN}
import com.coldcore.haala.web.facade.vo.cmd.wiki._
import com.coldcore.haala.service.wiki.{ArticleSearchService, ArticleService}
import com.coldcore.haala.web.facade.vo.cmd.{FilterVO, SearchVO}
import com.coldcore.haala.web.facade.logic.BaseFacade.{SortMapping, TargetedFilter, TargetedSearch}
import com.coldcore.haala.service.SearchInput
import com.coldcore.haala.service.wiki.ArticleSearchService.{ByDomainSearchInput, ByUserSearchInput}
import com.coldcore.haala.domain.wiki.Article
import com.coldcore.haala.service.wiki.Util._

/** Convert input and output data */
trait ArticleConvert {
  self: ArticleFacade =>

  implicit def addArticleVOtoIN(cmd: AddArticleVO) =
    CreateIN(cmd.`type`, cmd.title, cmd.parentId)

  implicit def editArticleVOtoIN(cmd: EditArticleVO) =
    UpdateIN(cmd.title, cmd.text, cmd.options.map(x => OptionIN(x.`type`, x.value)).toList, cmd.published,
      cmd.domains.safe.parseCSV, cmd.items.safe.parseCSV)

  implicit def removeAttachmentsVOtoIN(cmd: RemoveAttachmentsVO) =
    RemoveAttachmentsIN(cmd.attachments.safe.parseCSV)
}

class ArticleFacade extends BaseFacade with ArticleConvert {
  @BeanProperty @LogClass lazy val LOG = new NamedLog("wiki.article-facade", log)

  @BeanProperty @ValidatorClass var wikiArticleValidator: Validator = _

  @SecuredRole("WIKI")
  @ErrorCRs(Array(
    new ErrorCR(on = classOf[ObjectNotFoundException], key = "fcall.nofnd", log = "Article #{parentId} not found"),
    new ErrorCR(on = classOf[LimitExceededException], key = "fcall.limit", log = "Articles limit reached for user {currentUsername}")
  ))
  def create(cmd: AddArticleVO, request: HttpServletRequest): CallResult = {
    val o = sc.get[ArticleService].create(request.xuser.currentId, cmd)
    LOG.info(s"User ${request.xuser.currentUsername} created article #${o.id}")
    dataCR(o.ref)
  }

  @SecuredRole("WIKI")
  def remove(cmd: RemoveArticleVO, request: HttpServletRequest): CallResult = {
    sc.get[ArticleService].delete(cmd.articleId.toLong)
    LOG.info(s"User ${request.xuser.currentUsername} removed article #${cmd.articleId}")
    dataCR()
  }

  @SecuredRole("WIKI")
  @ErrorCR(on = classOf[ObjectNotFoundException], key = "fcall.nofnd", log = "Article #{articleId} not found")
  def edit(cmd: EditArticleVO, request: HttpServletRequest): CallResult = {
    sc.get[ArticleService].update(cmd.articleId.toLong, cmd)
    LOG.info(s"User ${request.xuser.currentUsername} updated article #${cmd.articleId}")
    dataCR()
  }

  @SecuredRole("WIKI")
  @ErrorCRs(Array(
    new ErrorCR(on = classOf[ObjectNotFoundException], key = "fcall.nofnd", log = "Article #{articleId} not found"),
    new ErrorCR(on = classOf[ObjectExistsException], key = "fcall.exist", log = "Attachment already exists"),
    new ErrorCR(on = classOf[LimitExceededException], key = "fcall.limit", log = "Attachments limit reached for article #{articleId}")
  ))
  def addAttachments(cmd: AddAttachmentsVO, request: HttpServletRequest): CallResult =
    uploadedFiles(request) match {
      case files if files.nonEmpty =>
        sc.get[ArticleService].createAttachments(cmd.articleId.toLong, files)
        LOG.info(s"User ${request.xuser.currentUsername} updated article #${cmd.articleId}")
        dataCR()
      case _ => errorCR(WebConstants.FACADE_ERROR_NO_UPLOAD)
    }

  @SecuredRole("WIKI")
  @ErrorCR(on = classOf[ObjectNotFoundException], key = "fcall.nofnd", log = "Article #{articleId} not found")
  def removeAttachments(cmd: RemoveAttachmentsVO, request: HttpServletRequest): CallResult = {
    sc.get[ArticleService].removeAttachments(cmd.articleId.toLong, cmd)
    LOG.info(s"User ${request.xuser.currentUsername} updated article #${cmd.articleId}")
    dataCR()
  }

}

/** Convert input and output data */
trait ArticleSearchConvert {
  self: ArticleSearchFacade =>

  val articleVO = (request: HttpServletRequest) => (o: Article) => {
    val site = request.xsite.current
    val title = label(packLabel("ar", o.ref, "title"), site)
    Map('id -> o.id, 'ref -> o.ref, 'title -> title, 'status -> o.status, 'type -> o.`type`)
  }
}

class ArticleSearchFacade extends BaseFacade with ArticleSearchConvert {
  @BeanProperty @ValidatorClass var baseValidator: Validator = _

  private val searchOwnedBooks = (si: SearchInput, sh: SearchHelper) => {
    val input = new ByUserSearchInput(si) {
      override val filterValue = sh.filter(0).toLowerCase
      override val `type` = Some(Article.TYPE_BOOK)
    }
    sc.get[ArticleSearchService].searchByUser(sh.request.xuser.currentId, input).serializeAsMap(articleVO(sh.request))
  }

  private val searchByDomain = (si: SearchInput, sh: SearchHelper) => {
    val input = new ByDomainSearchInput(si) {
      override val domainId = sh.request.xsite.state.currentDomainId
    }
    sc.get[ArticleSearchService].searchByDomain(input).serializeAsMap(articleVO(sh.request))
  }

  @SecuredRole("ANONYMOUS")
  def search(cmd: SearchVO, request: HttpServletRequest): CallResult =
    genericSearch(cmd, request,
      TargetedSearch(1, "wikiArticles", "wikiArticles", orderBy(cmd), searchByDomain))

  @SecuredRole("WIKI")
  def dsearch(cmd: SearchVO, request: HttpServletRequest): CallResult =
    genericSearch(cmd, request,
      TargetedSearch(2, "wikiMyBooks", "wikiMyBooks",
        orderBy(cmd, SortMapping("ref")),
        searchOwnedBooks))

  @SecuredRole("WIKI")
  def dfilter(cmd: FilterVO, request: HttpServletRequest): CallResult = {
    genericSearchFilter(cmd, request, TargetedFilter(1, "wikiMyArticles"))
    dataCR()
  }

}
