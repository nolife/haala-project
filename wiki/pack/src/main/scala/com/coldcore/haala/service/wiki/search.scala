package com.coldcore.haala
package service
package wiki

import com.coldcore.haala.domain.wiki.Article
import com.coldcore.haala.service.SolrService._
import org.springframework.transaction.annotation.Transactional
import scala.beans.BeanProperty
import com.coldcore.haala.dao.GenericDAO
import com.coldcore.haala.service.wiki.Util._
import com.coldcore.haala.service.SearchResult
import com.coldcore.haala.service.SolrService.ObjectTypeTerm
import com.coldcore.haala.service.SearchInput

object ArticleSearchService {
  class ByUserSearchInput(from: Long, max: Int, orderBy: String = "") extends SearchInput(from, max, orderBy) {
    def this(in: SearchInput) = this(in.from, in.max, in.orderBy)
    val filterValue = ""
    val `type`: Option[Int] = None
  }

  class ByDomainSearchInput(from: Long, max: Int, orderBy: String = "") extends SearchInput(from, max, orderBy) {
    def this(in: SearchInput) = this(in.from, in.max, in.orderBy)
    val domainId = 0L
  }

  class ByBookSearchInput(from: Long, max: Int, orderBy: String = "") extends SearchInput(from, max, orderBy) {
    def this(in: SearchInput) = this(in.from, in.max, in.orderBy)
    val bookRef = ""
    val userQuery = ""
    val status: Option[Int] = None
    val domainId = 0L
  }
}

trait ArticleSearchService {
  import ArticleSearchService._

  def allIds: List[Long]
  def searchByUser(userId: Long, in: ByUserSearchInput): SearchResult[Article]
  def searchByDomain(in: ByDomainSearchInput): SearchResult[Article]
  def searchByBook(in: ByBookSearchInput): SearchResult[Article]
  def searchFast(in: SearchInput): SearchResult[Article]
  def allPublishedRefsByDomain(domainId: Long): List[String]
}

@Transactional
class ArticleSearchServiceImpl extends BaseService with ArticleSearchService {
  import ArticleSearchService._

  @BeanProperty var wikiArticleDao: GenericDAO[Article] = _

  private def solrSort = SolrService.solrSort(packArticlePrefix, _: String)
  private def articleType = packArticleType(sc)

  /** Find all IDs. */
  @Transactional (readOnly = true)
  override def allIds: List[Long] = wikiArticleDao.findX[Long]("select id from "+classOf[Article].getName)

  /** Find user's articles. */
  @Transactional (readOnly = true)
  override def searchByUser(userId: Long, in: ByUserSearchInput): SearchResult[Article] = {
    val result = sc.get[SolrService].searchWT(
      SolrQuery(
        AndTerm(
          ObjectTypeTerm(articleType), packArticleTerm("usr", userId),
          (in.filterValue != "") ? TextTerm(solrEscapeChars(in.filterValue)) | NoopTerm, //optional filter text
          in.`type`.isDefined ? packArticleTerm("typ", in.`type`.get) | NoopTerm)), //optional article type
      FlTerm("id"), StartTerm(in.from), RowsTerm(in.max), SortTerm((in.orderBy != "") ? solrSort(in.orderBy) | ""))
    sc.get[SolrService].fetchObjectsWT(result, wikiArticleDao, classOf[Article])
  }

  /** Find articles of specified domain (visitor search). */
  @Transactional (readOnly = true)
  override def searchByDomain(in: ByDomainSearchInput): SearchResult[Article] = {
    val result = sc.get[SolrService].searchWT(
      SolrQuery(
        AndTerm(
          ObjectTypeTerm(articleType), NotTerm(packArticleTerm("sta", Article.STATUS_PUBLISHED)),
          (in.domainId > 0) ? packArticleTerm("dom", in.domainId) | NoopTerm)),
      FlTerm("id"), StartTerm(in.from), RowsTerm(in.max), SortTerm((in.orderBy != "") ? solrSort(in.orderBy) | ""))
    sc.get[SolrService].fetchObjectsWT(result, wikiArticleDao, classOf[Article])
  }

  /** Find articles in a book. */
  @Transactional (readOnly = true)
  override def searchByBook(in: ByBookSearchInput): SearchResult[Article] = {
    val result = sc.get[SolrService].searchWT(
      SolrQuery(
        AndTerm(
          ObjectTypeTerm(articleType),
          packArticleTerm("typ", Article.TYPE_ARTICLE),
          packArticleTerm("bok", in.bookRef),
          in.status.isDefined ? packArticleTerm("sta", in.status.get) | NoopTerm,
          (in.domainId > 0) ? packArticleTerm("dom", in.domainId) | NoopTerm,
          TextTerm(in.userQuery))),
      FlTerm("id"), StartTerm(in.from), RowsTerm(in.max), SortTerm((in.orderBy != "") ? solrSort(in.orderBy) | ""))
    sc.get[SolrService].fetchObjectsWT(result, wikiArticleDao, classOf[Article])
  }

  /** Fast search without filtering. */
  @Transactional (readOnly = true)
  override def searchFast(in: SearchInput): SearchResult[Article] = {
    val query = "from "+classOf[Article].getName+" "+in.orderByClause
    searchObjects(in.from, in.max, wikiArticleDao, query)
  }

  /** Find all active REFs by domain ID. */
  @Transactional (readOnly = true)
  override def allPublishedRefsByDomain(domainId: Long): List[String] = {
    val query = "select o.ref from "+classOf[Article].getName+" o join o.domains as d "+
      "where o.status = ? and d.id = ?"
    wikiArticleDao.findX[String](query, Article.STATUS_PUBLISHED, domainId)
  }

}