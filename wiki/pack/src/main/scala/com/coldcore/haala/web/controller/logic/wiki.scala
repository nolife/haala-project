package com.coldcore.haala
package web
package controller.logic
package wiki

import controller.logic.BaseController.Mod
import javax.servlet.http.{HttpServletRequest, HttpServletResponse}
import service.UserRoleCheck
import service.wiki.{ArticleSearchService, ArticleService}
import service.wiki.ArticleSearchService.ByBookSearchInput
import core.Dependencies
import core.wiki.WikiRequestX
import com.coldcore.haala.domain.wiki.Article

import scala.collection.JavaConverters._

class ArticleMod extends Mod with UserRoleCheck {
  override def process(ctrl: BaseController, request: HttpServletRequest, response: HttpServletResponse): Option[String] = {
    val (id, ref) = (request("id").safeLong, request("ref").toUpperCase)

    (if (id > 0) sc.get[ArticleService].getById(id)
     else if (ref.nonEmpty) sc.get[ArticleService].getByRef(ref)
     else None
    ).foreach { article =>
      if (article.pack.user.id == request.xuser.currentId || isUserAdmin) request.xattr + ("article" -> article)
    }

    None
  }
}

class ViewArticleMod extends Mod {
  override def process(ctrl: BaseController, request: HttpServletRequest, response: HttpServletResponse): Option[String] = {
    val ref = request("ref") or request.last // .../view.htm?ref=3I51B1R4   .../3I51B1R4   3I51B1R4?enq=1 -> 3I51B1R4
    if (ref.nonEmpty) sc.get[ArticleService].getByRef(ref).foreach { article =>
      if (WikiRequestX(request).xwiki.isDomainSupportsArticle(article)) request.xattr + ("article" -> article)
    }
    None
  }
}

class MyBookSearchMod extends Mod with UserRoleCheck {
  override def process(ctrl: BaseController, request: HttpServletRequest, response: HttpServletResponse): Option[String] = {
    val (ref, query) = (request("ref").toUpperCase, request("query"))

    if (ref.nonEmpty) sc.get[ArticleService].getByRef(ref).foreach { article =>
      def parentsOf(o: Article): List[Article] = o :: (if (o.parent == null) Nil else parentsOf(o.parent))
      val book = parentsOf(article).last
      if (book.pack.user.id == request.xuser.currentId || isUserAdmin) {
        val result =
          sc.get[ArticleSearchService].searchByBook(new ByBookSearchInput(0, 999) {
            override val bookRef = book.ref
            override val userQuery = query
          })
        request.xattr + ("book" -> book, "articles" -> result.objects.asJava)
      }
    }

    None
  }
}

class BookSearchMod extends Mod {
  override def process(ctrl: BaseController, request: HttpServletRequest, response: HttpServletResponse): Option[String] = {
    val (ref, query) = (request("ref").toUpperCase, request("query"))

    if (ref.nonEmpty) sc.get[ArticleService].getByRef(ref).foreach { article =>
      if (WikiRequestX(request).xwiki.isDomainSupportsArticle(article)) {
        def parentsOf(o: Article): List[Article] = o :: (if (o.parent == null) Nil else parentsOf(o.parent))
        val book = parentsOf(article).last
        val result =
          sc.get[ArticleSearchService].searchByBook(new ByBookSearchInput(0, 999) {
            override val bookRef = book.ref
            override val userQuery = query
            override val status = Some(Article.STATUS_PUBLISHED)
            override val domainId = request.xsite.state.currentDomainId
          })
        request.xattr + ("book" -> book, "articles" -> result.objects.asJava)
      }
    }

    None
  }
}

class SitemapMod extends SitemapController.Mod with Dependencies {
  override def appendToModel(request: HttpServletRequest): Map[String,AnyRef] = {
    val refs = sc.get[ArticleSearchService].allPublishedRefsByDomain(request.xsite.state.currentDomainId)
    Map("articles" -> refs.toArray)
  }
}
