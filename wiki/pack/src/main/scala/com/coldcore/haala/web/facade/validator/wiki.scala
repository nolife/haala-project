package com.coldcore.haala
package web
package facade.validator
package wiki

import com.coldcore.haala.core.NamedLog
import service.wiki.ArticleService
import facade.annotations.DatatypeInput
import facade.validator.Validator.{Context, Field}

class ArticleValidator extends BaseValidator with CommonChecks {
  override lazy val LOG = new NamedLog("wiki.article-validator", log)

  /** Check if a user owns an article. */
  @DatatypeInput("owner")
  def checkOwner(field: Field, context: Context): Boolean =
    if (field.name == "domains") isDomainsOwner(field.strval, context) else isArticlesOwner(field.strval, context)

  /** Check if a user owns an article. */
  private def isArticlesOwner(value: String, context: Context): Boolean =
    isOwnerOf(value, context, sc.get[ArticleService].getById(_), "article")

}
