package com.coldcore.haala
package domain
package wiki

import scala.beans.BeanProperty
import com.coldcore.haala.domain.annotations.EqualsHashCode

object WikiPack {
  val PACK = "wiki"
}

class WikiPack extends Pack {
  import WikiPack._
  pack = PACK
  @BeanProperty var articles: JList[Article] = new JArrayList[Article]
}

object Article {
  val STATUS_PRIVATE = 0
  val STATUS_PUBLISHED = 1

  val TYPE_ARTICLE = 0
  val TYPE_BOOK = 1
}

class Article extends BaseObject with BasePackObject[WikiPack] with BaseOptionObject[ArticleOption] {
  import Article._
  @BeanProperty @EqualsHashCode var ref: String = _
  @BeanProperty @EqualsHashCode var status: JInteger = STATUS_PRIVATE
  @BeanProperty @EqualsHashCode var `type`: JInteger = TYPE_ARTICLE
  @BeanProperty @EqualsHashCode var index: JInteger = 0
  @BeanProperty var domains: JList[Domain] = new JArrayList[Domain]
  @BeanProperty var items: JList[Article] = new JArrayList[Article]
  @BeanProperty var attachments: JList[Attachment] = new JArrayList[Attachment]
  @BeanProperty var parent: Article = _
}

class ArticleOption extends BaseOption("wikiArOpt") {
  @BeanProperty var article: Article = _

  override def getMode(typ: Int): Int = typ match {
    case x if 101 to 200 contains x => MODE_INT_VALUE
    case x if 201 to 300 contains x => MODE_STRING_VALUE
    case _ => MODE_SIMPLE
  }
}

object ArticleOption {
  //assorted simple types 1..100

  //assorted Int types 101..200

  //assorted String types 201..300
}
