package com.coldcore.haala
package core
package wiki

import scala.beans.BeanProperty

/** JSON setting WikiParams */
class Params {
  @BeanProperty var article: String = _
}
