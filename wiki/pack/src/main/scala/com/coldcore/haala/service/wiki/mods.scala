package com.coldcore.haala
package service
package wiki

import com.coldcore.haala.domain.{Domain, User}
import com.coldcore.haala.domain.wiki.WikiPack

/** Addition to user service. */
class UserMod extends UserService.Mod with Dependencies {
  override def delete(o: User) = o.wikiPack.articles.map(x => sc.get[ArticleService].delete(x.id))
  override def register(o: User, domain: Domain) = addPack(o)
  override def addPack(o: User) = o.addPack(WikiPack.PACK) { new WikiPack }
}
