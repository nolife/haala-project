package com.coldcore.haala
package service
package wiki

import com.coldcore.haala.domain.wiki.{Article, ArticleOption}
import com.coldcore.haala.service.SolrService._
import org.springframework.transaction.annotation.Transactional

import scala.beans.BeanProperty
import com.coldcore.haala.dao.GenericDAO
import com.coldcore.haala.service.wiki.Util._
import com.coldcore.misc5.IdGenerator._
import com.coldcore.haala.core.{CoreException, VirtualPath}
import com.coldcore.haala.service.exceptions.{LimitExceededException, ObjectNotFoundException}
import com.google.gson.Gson
import com.coldcore.haala.core.wiki.Params

import scala.collection.JavaConverters._
import com.coldcore.haala.web.core.CustomMultipartResolver.TemporaryFile

object ArticleService {
  case class CreateIN(`type`: String, title: String, parentId: String)
  case class UpdateIN(title: String, text: String, options: List[OptionIN], published: String,
                      domains: List[String], items: List[String])
  case class OptionIN(`type`: String, value: String)
  case class RemoveAttachmentsIN(attachments: List[String])
}

trait ArticleService {
  import ArticleService._

  def getById(id: Long): Option[Article]
  def getByRef(ref: String): Option[Article]
  def delete(id: Long)
  def create(userId: Long, in: CreateIN): Article
  def update(id: Long, in: UpdateIN): Article
  def prepareSolrDoc(o: Article): SolrDoc
  def doSolrSubmit(o: Article)
  def doSolrDelete(o: Article)
  def createAttachments(id: Long, bodies: Seq[TemporaryFile])
  def removeAttachments(id: Long, in: RemoveAttachmentsIN)
}

@Transactional
class ArticleServiceImpl extends BaseService with ArticleService {
  import ArticleService._

  @BeanProperty var wikiArticleDao: GenericDAO[Article] = _

  private def articleType = packArticleType(sc)

  /** Prepare Solr doc. */
  override def prepareSolrDoc(o: Article): SolrDoc = {
    def parentsOf(o: Article): List[Article] = o :: (if (o.parent == null) Nil else parentsOf(o.parent))
    val book = parentsOf(o).last

    val terms =
      List(
        ObjectIdTerm(o.id, ObjectTypeTerm(articleType)),
        packArticleTerm("ref", o.ref),
        packArticleTerm("sta", o.status),
        packArticleTerm("usr", o.pack.user.id),
        packArticleTerm("typ", o.`type`)) :::
      //title and text
      (List.empty[Term] /: labels(packLabel("ar", o.ref, "title")))((a,b) => ValueTerm("text", b.value) :: a) :::
      (List.empty[Term] /: labels(packLabel("ar", o.ref, "text")))((a,b) => ValueTerm("text", b.value) :: a) :::
      //domains
      (List.empty[Term] /: o.domains)((a,b) => packArticleTerm("dom", b.id) :: a) :::
      //options
      (List.empty[Term] /: o.options)((a,b) => packArticleTerm("opt", b.`type`) :: a) :::
      //parent book
      ((o.`type` == Article.TYPE_ARTICLE && book.`type` == Article.TYPE_BOOK) ? List(packArticleTerm("bok", book.ref)) | Nil)
    SolrDoc(terms: _*)
  }

  /** Submit an object to Solr. */
  @Transactional (readOnly = true)
  override def doSolrSubmit(o: Article) =
    sc.get[SolrService].submitWT(prepareSolrDoc(o))

  /** Delete an object from Solr. */
  @Transactional (readOnly = true)
  override def doSolrDelete(o: Article) =
    sc.get[SolrService].deleteByIdsWT(ObjectIdTerm(o.id, ObjectTypeTerm(articleType)))

  /** Get an object by its ID. */
  @Transactional (readOnly = true)
  override def getById(id: Long): Option[Article] =
    wikiArticleDao.findById(id)

  /** Get an object by a REF. */
  @Transactional (readOnly = true)
  override def getByRef(ref: String): Option[Article] =
    wikiArticleDao.findUniqueByProperty("ref", ref)

  /** Generate a unique ref. */
  private def generateUniqueRef: String =
    Iterator.fill(10000)(generate(8,8).toUpperCase).find {
      case ref => ref.matches("\\D+") && !getByRef(ref).isDefined //nondigit
    }.getOrElse(throw new CoreException("Ref generator exhausted"))

  /** Create an article. */
  @Transactional (readOnly = false)
  override def create(userId: Long, in: CreateIN): Article = {
    val user = sc.get[UserService].getById(userId).getOrElse(throw new ObjectNotFoundException)

    //test that max inactive articles limit is not exceeded
    val params = Option(new Gson().fromJson(sc.get[SettingService].querySystem("WikiParams").safe, classOf[Params])).get
    val count = user.wikiPack.articles.count(_.status == Article.STATUS_PRIVATE)
    val limit = params.article.parseCSVMap.getOrElse("privateLimit", "").toInt
    if (count >= limit) throw new LimitExceededException

    val o = new Article
    o.pack = user.wikiPack
    o.ref = generateUniqueRef
    o.`type` = in.`type`.toInt

    if (in.parentId.isEmpty) {
      o.domains.addAll(user.domains)
      o.pack.articles.add(o)
    } else {
      val parent = assertArticle(in.parentId.safeLong)
      o.domains.addAll(parent.domains)
      o.parent = parent
      o.index = parent.items.map(_.index.toInt).reduceOption(_ max _).getOrElse(0)+1
      parent.items.add(o)
      wikiArticleDao.saveOrUpdate(parent)
    }

    val site = sc.get[SettingService].getParsedSites.head.key
    labelService.write(packLabel("ar", o.ref, "title"), in.title.safe, site)

    wikiArticleDao.saveOrUpdate(o)
    doSolrSubmit(o) //update Solr
    o
  }

  /** Delete an article. */
  @Transactional (readOnly = false)
  override def delete(id: Long) = getById(id).foreach { o =>
    def rmResources(a: Article) { //delete labels and files
      o.attachments.map(x => sc.get[service.AttachmentService].delete(x.id))
      o.attachments.clear
      labelService.deleteByKeyLike(packLabel("ar", a.ref, "%"))
      sc.get[FileService].deleteByFolder(VirtualPath(packPath("ar", a.ref), "*"))
      sc.get[FileService].deleteByFolder(VirtualPath("files/"+packPath("ar", a.ref), "*"))
      a.items.map(rmResources)
    }
    rmResources(o)

    if (o.parent != null) {
      o.parent.items.remove(o)
      wikiArticleDao.saveOrUpdate(o.parent)
    } else {
      o.pack.articles.remove(o)
      wikiArticleDao.delete(o)
    }
    doSolrDelete(o) //update Solr
  }

  private def assertArticle(id: Long): Article =
    getById(id).getOrElse(throw new ObjectNotFoundException)

  private def merge(o: ArticleOption, in: OptionIN) {
    o.`type` = in.`type`.toInt
    o.value = in.value.safe //nullable field
  }

  /** Update article. */
  @Transactional (readOnly = false)
  override def update(id: Long, in: UpdateIN): Article = {
    val o = assertArticle(id)
    val site = sc.get[SettingService].getParsedSites.head.key
    labelService.write(packLabel("ar", o.ref, "title"), in.title.safe, site)
    labelService.write(packLabel("ar", o.ref, "text"), in.text.safe, site)
    o.status = if (in.published.isTrue) Article.STATUS_PUBLISHED else Article.STATUS_PRIVATE

    expandOrShrink(o.options, in.options.size, () => new ArticleOption { article = o })
    for ((op, voop) <- o.options zip in.options) merge(op, voop)

    for ((id,n) <- in.items.zipWithIndex; item <- o.items; if item.id == id.toLong) item.index = n //reorder items

    val domains = (in.domains.isEmpty && o.parent != null) ?
      o.parent.domains | in.domains.map(x => sc.get[DomainService].getById(x.toLong).get).asJava
    o.domains.clear
    o.domains.addAll(domains)

    wikiArticleDao.saveOrUpdate(o)
    doSolrSubmit(o) //update Solr
    o
  }

  /** Create article's attachments from a collection of file bodies. */
  @Transactional (readOnly = false)
  override def createAttachments(id: Long, files: Seq[TemporaryFile]) {
    val o = assertArticle(id)

    //check if attachments limit is not exceeded
    val params = Option(new Gson().fromJson(sc.get[SettingService].querySystem("WikiParams").safe, classOf[Params])).get
    val limit = params.article.parseCSVMap.getOrElse("attachmentLimit", "").toInt
    if (o.attachments.size+files.size > limit) throw new LimitExceededException

    files.map(f => o.attachments.add(sc.get[AttachmentService].createDefault(o, f)))
    wikiArticleDao.saveOrUpdate(o)
  }

  /** Create article's attachments from a collection of file bodies. */
  @Transactional (readOnly = false)
  override def removeAttachments(id: Long, in: RemoveAttachmentsIN) {
    val o = assertArticle(id)
    val remove = for (id <- in.attachments; a <- o.attachments; if a.id == id.toLong) yield a
    remove.map { x =>
      sc.get[service.AttachmentService].delete(x.id)
      o.attachments.remove(x)
    }
    wikiArticleDao.saveOrUpdate(o)
  }

}
