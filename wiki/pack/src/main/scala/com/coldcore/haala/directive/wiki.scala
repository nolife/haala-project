package com.coldcore.haala
package directive.freemarker.wiki

import com.coldcore.haala.service.Dependencies
import com.coldcore.haala.directive.freemarker.{GetOptionObject, BaseDirective, State}
import com.coldcore.haala.domain.wiki.{ArticleOption, Article}
import com.coldcore.haala.service.wiki.ArticleService
import com.coldcore.haala.service.wiki.Util._
import com.coldcore.haala.domain.Attachment

trait GetArticle extends GetOptionObject[ArticleOption,Article] with Dependencies {
  def getArticle(state: State): Option[Article] = {
    import state._
    val pval = (x: String) => paramAsString(x).getOrElse("")
    val (id, ref) = (pval("articleId"), pval("articleRef"))

    paramAsObject[Article]("article").orElse { //article object as param
      if (id.nonEmpty) sc.get[ArticleService].getById(id.toLong) //by ID
      else if (ref.nonEmpty) sc.get[ArticleService].getByRef(ref) //by REF
      else None
    }
  }

  override def getObject(state: State) = getArticle(state)
}

class ArticleDirective extends BaseDirective with GetArticle {
  override def execute(state: State) = getArticle(state).foreach(article => outputTM(state, state.wrap(article)))
}

class ArticleParentDirective extends BaseDirective with GetArticle {
  override def execute(state: State) = getArticle(state).foreach(article =>
      if (article.parent != null) outputTM(state, state.wrap(article.parent)))
}

class ArticleTextDirective extends BaseDirective with GetArticle {
  override def execute(state: State) = getArticle(state).foreach { article =>
    import state._
    val pval = (x: String) => paramAsString(x).getOrElse("")
    val text = label(packLabel("ar", article.ref, pval("key")), resolveSite).safe
    output(state, text.convert(pval("output")))
  }
}

class ListArticleItemsDirective extends BaseDirective with GetArticle {
  override def execute(state: State) = getArticle(state).foreach { article =>
    val userId = state.varAsNumber("currentUserId").getOrElse(0L)
    val xs = article.items.filter(x => //published or belongs to a user
      x.status == Article.STATUS_PUBLISHED || x.pack.user.id == userId || isUserAdmin).
      sortWith((a,b) => a.index < b.index)
    render(state, xs, (x: Article) => state.wrap(x))
  }
}

class ListBookAttachments extends BaseDirective with GetArticle {
  override def execute(state: State) = getArticle(state).foreach { article =>
    def parentsOf(o: Article): List[Article] = o :: (if (o.parent == null) Nil else parentsOf(o.parent))
    val book = parentsOf(article).last
    val xs = book.attachments.sortBy(_.file.name)
    render(state, xs, (x: Attachment) => state.wrap(x))
  }
}

class ListArticleAttachments extends BaseDirective with GetArticle {
  override def execute(state: State) = getArticle(state).foreach { article =>
    val xs = article.attachments.sortBy(_.file.name)
    render(state, xs, (x: Attachment) => state.wrap(x))
  }
}
