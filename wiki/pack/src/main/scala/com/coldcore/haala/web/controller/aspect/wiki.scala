package com.coldcore.haala
package web
package controller.aspect
package wiki

import com.coldcore.haala.web.core.Dependencies
import javax.servlet.http.HttpServletRequest

import com.coldcore.misc.scala.StringOp._
import com.coldcore.haala.domain.wiki.Article
import com.coldcore.haala.service.LabelsReader

class PageMod extends PageAspect.Mod with Dependencies with LabelsReader {
  import PageAspect._

  override def createRecentTitle(request: HttpServletRequest): String =
    request.xattr.get[Article]("article") match {
      case Some(article) =>
        val template = label("ttl.r"+request.xmisc.pageIndex, request.xsite.current).safe
        parametrize(template, "ref:"+article.ref)
      case None => ""
    }

  override def createPageTitle(request: HttpServletRequest): String =
    request.xattr.get[Article]("article") match {
      case Some(article) =>
        val template = label("ttl.p"+request.xmisc.pageIndex, request.xsite.current).safe
        parametrize(template, "ref:"+article.ref)
      case None => ""
    }

  override def createMetaKeywords(request: HttpServletRequest): String =
    request.xattr.get[Article]("article") match {
      case Some(article) =>
        val template = label("met.k"+request.xmisc.pageIndex, request.xsite.state.defaultSite).safe
        parametrize(template, "ref:"+article.ref)
      case None => ""
    }

  override def createMetaDescription(request: HttpServletRequest): String =
    request.xattr.get[Article]("article") match {
      case Some(article) =>
        val template = label("met.d"+request.xmisc.pageIndex, request.xsite.state.defaultSite).safe
        parametrize(template, "ref:"+article.ref)
      case None => ""
    }

}
