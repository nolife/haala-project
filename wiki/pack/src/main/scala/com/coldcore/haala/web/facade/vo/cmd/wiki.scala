package com.coldcore.haala.web.facade.vo.cmd
package wiki

import scala.beans.BeanProperty
import com.coldcore.haala.web.facade.annotations.Input

class AddArticleVO {
  @BeanProperty @Input (datatype = "owner") var parentId: String = _
  @BeanProperty @Input var `type`: String = _
  @BeanProperty @Input (required = true) var title: String = _
}

class RemoveArticleVO {
  @BeanProperty @Input (datatype = "owner") var articleId: String = _
}

class EditArticleVO {
  @BeanProperty @Input (datatype = "owner") var articleId: String = _
  @BeanProperty @Input (required = true) var title: String = _
  @BeanProperty @Input var text: String = _
  @BeanProperty @Input (transform = "bool") var published: String = _
  @BeanProperty @Input var options: Array[OptionVO] = _
  @BeanProperty @Input (datatype = "owner") var domains: String = _
  @BeanProperty @Input (datatype = "owner") var items: String = _
}

class OptionVO {
  @BeanProperty var `type`: String = _
  @BeanProperty @Input var value: String = _
}

class AddAttachmentsVO {
  @BeanProperty @Input (datatype = "owner") var articleId: String = _
}

class RemoveAttachmentsVO {
  @BeanProperty @Input (datatype = "owner") var articleId: String = _
  @BeanProperty var attachments: String = _
}
