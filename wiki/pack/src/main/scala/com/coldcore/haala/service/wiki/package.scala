package com.coldcore.haala
package service

import com.coldcore.haala.domain.User
import com.coldcore.haala.domain.wiki.WikiPack

package object wiki {
  implicit class WikiUser(user: User) {
    def wikiPack = user.getPack[WikiPack](WikiPack.PACK)

    /** Method for traversing in JSP
      *  user.wikiPack.articles
      */
    def getWikiPack = wikiPack

    /** Method for passing regular user into JSP tags or traversing it
      *  user="${owner.unwrap}"   user.unwrap.address
      */
    def getUnwrap = user
  }
}
