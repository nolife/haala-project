package com.coldcore.haala
package service
package wiki

import com.coldcore.haala.service.SolrService.{ValueTerm, Term}
import com.coldcore.haala.domain.wiki.WikiPack

object Util {
  def packLabel(tokens: String*): String = WikiPack.PACK+"."+tokens.mkString(".")
  def packPath(tokens: String*): String = WikiPack.PACK+"/"+tokens.mkString("/")
  def packArticleType(sc: ServiceContainer): Int = sc.get[SettingService].queryConstant[Int]("Constants", WikiPack.PACK, "SOLR_TYPE_ARTICLE")
  def packArticlePrefix: String = "wiki_ar"
  def packArticleTerm(key: String, value: Any): Term = ValueTerm(packArticlePrefix+"_"+key, value)
  def installed(sc: ServiceContainer): Boolean = sc.get[SettingService].querySystem("InstalledPacks").safe.parseCSV.contains(WikiPack.PACK)
}