package com.coldcore.haala
package service

package wiki {
  package test {

    class MyMock extends com.coldcore.haala.service.test.MyMock {
      val articleService = mock[ArticleService]

      serviceContainer.services =
        articleService :: serviceContainer.services
    }

  }
}