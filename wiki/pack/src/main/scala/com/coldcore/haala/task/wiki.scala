package com.coldcore.haala
package task
package wiki

import com.coldcore.haala.service.SearchInput
import com.coldcore.haala.service.wiki.{ArticleService, ArticleSearchService}
import com.coldcore.haala.service.wiki.Util._
import com.coldcore.haala.domain.wiki.Article

/** On demand task to submit all articles into Solr. */
class SolrSubmitArticlesTask extends BaseSolrSubmitTask[Article] {
  override val name = "WikiSolrSubmitArticlesTask"
  override val flagName = "WikiSolrSubmitArticlesFlag"
  override def solrType: Int = packArticleType(sc)
  override def fastSearch(in: SearchInput) = sc.get[ArticleSearchService].searchFast(in)
  override def solrDoc(o: Article) = sc.get[ArticleService].prepareSolrDoc(o)
  override def goAhead: Boolean = super.goAhead && installed(sc)
}
