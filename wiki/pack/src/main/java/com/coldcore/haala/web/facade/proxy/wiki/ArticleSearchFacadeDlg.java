package com.coldcore.haala.web.facade.proxy.wiki;

import com.coldcore.haala.web.facade.annotations.AopFacade;
import com.coldcore.haala.web.facade.annotations.TargetClass;
import com.coldcore.haala.web.facade.logic.wiki.ArticleSearchFacade;
import com.coldcore.haala.web.facade.vo.cmd.wiki.*;
import com.coldcore.haala.web.facade.vo.cmd.*;

import javax.servlet.http.HttpServletRequest;

/** AOP proxy delegate */
@AopFacade
public class ArticleSearchFacadeDlg {

  @TargetClass private ArticleSearchFacade wikiArticleSearchFacade;

  public ArticleSearchFacade getWikiArticleSearchFacade() { return wikiArticleSearchFacade; }

  public void setWikiArticleSearchFacade(ArticleSearchFacade wikiArticleSearchFacade) { this.wikiArticleSearchFacade = wikiArticleSearchFacade; }

  /*** Stub methods. Invoked by 'InvokerAspect' directly on the 'target'. ***/
  public Object search(SearchVO cmd, HttpServletRequest request) { return null; }
  public Object dsearch(SearchVO cmd, HttpServletRequest request) { return null; }
  public Object dfilter(FilterVO cmd, HttpServletRequest request) { return null; }
}