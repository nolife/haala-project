package com.coldcore.haala.web.facade.proxy.wiki;

import com.coldcore.haala.web.facade.annotations.AopFacade;
import com.coldcore.haala.web.facade.annotations.TargetClass;
import com.coldcore.haala.web.facade.logic.wiki.ArticleFacade;
import com.coldcore.haala.web.facade.vo.cmd.wiki.*;

import javax.servlet.http.HttpServletRequest;

/** AOP proxy delegate */
@AopFacade
public class ArticleFacadeDlg {

  @TargetClass private ArticleFacade wikiArticleFacade;

  public ArticleFacade getWikiArticleFacade() { return wikiArticleFacade; }

  public void setWikiArticleFacade(ArticleFacade wikiArticleFacade) { this.wikiArticleFacade = wikiArticleFacade; }

  /*** Stub methods. Invoked by 'InvokerAspect' directly on the 'target'. ***/
  public Object create(AddArticleVO cmd, HttpServletRequest request) { return null; }
  public Object remove(RemoveArticleVO cmd, HttpServletRequest request) { return null; }
  public Object edit(EditArticleVO cmd, HttpServletRequest request) { return null; }
  public Object addAttachments(AddAttachmentsVO cmd, HttpServletRequest request) { return null; }
  public Object removeAttachments(RemoveAttachmentsVO cmd, HttpServletRequest request) { return null; }
}