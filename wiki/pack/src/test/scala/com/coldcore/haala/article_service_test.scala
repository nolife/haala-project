package com.coldcore.haala
package service
package wiki

import org.junit.runner.RunWith
import org.scalatest.junit.JUnitRunner
import org.scalatest.{BeforeAndAfter, BeforeAndAfterAll, WordSpec}
import org.scalatest.mockito.MockitoSugar

@RunWith(classOf[JUnitRunner])
class ArticleServiceSpec extends WordSpec with MockitoSugar with BeforeAndAfter {
  var m: MyMock = _

  before {
    m = new MyMock
  }

  class MyMock extends com.coldcore.haala.service.test.MyMock {
    val service = new ArticleServiceImpl |< { x => x.serviceContainer = serviceContainer }
  }

  "dummy" should {
    "be dummy" in {
    }
  }

}
