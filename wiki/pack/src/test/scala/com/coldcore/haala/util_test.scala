package com.coldcore.haala
package service
package wiki

import org.junit.runner.RunWith
import org.scalatest.junit.JUnitRunner
import org.scalatest.{BeforeAndAfter, BeforeAndAfterAll, WordSpec}
import org.scalatest.mockito.MockitoSugar
import com.coldcore.haala.service.wiki.Util._

@RunWith(classOf[JUnitRunner])
class UtilSpec extends WordSpec with MockitoSugar with BeforeAndAfter {

  "packLabel" should {
    "return correct label" in {
      assertResult("wiki.ar.AAFF.%") { packLabel("ar", "AAFF", "%") }
    }
  }

  "packPath" should {
    "return correct file path" in {
      assertResult("wiki/ar/AAFF/%") { packPath("ar", "AAFF", "%") }
    }
  }

}
