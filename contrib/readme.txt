As this pack is aggregation of other packs, the directory (package) structure is different:
  a) normal  pack   com.coldcore.haala.service.[packname]
  b) contrib pack   com.coldcore.haala.contrib.[packname].service
                    resources/[packname]
