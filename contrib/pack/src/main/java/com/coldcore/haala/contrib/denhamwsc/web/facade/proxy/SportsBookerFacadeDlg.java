package com.coldcore.haala.contrib.denhamwsc.web.facade.proxy;

import com.coldcore.haala.web.facade.annotations.AopFacade;
import com.coldcore.haala.web.facade.annotations.TargetClass;
import com.coldcore.haala.contrib.denhamwsc.web.facade.logic.SportsBookerFacade;
import com.coldcore.haala.contrib.denhamwsc.web.facade.vo.cmd.*;

import javax.servlet.http.HttpServletRequest;

/** AOP proxy delegate */
@AopFacade
public class SportsBookerFacadeDlg {

  @TargetClass
  private SportsBookerFacade denhamwscSportsBookerFacade;

  public SportsBookerFacade getDenhamwscSportsBookerFacade() {
    return denhamwscSportsBookerFacade;
  }

  public void setDenhamwscSportsBookerFacade(SportsBookerFacade denhamwscSportsBookerFacade) {
    this.denhamwscSportsBookerFacade = denhamwscSportsBookerFacade;
  }

  /**
   * Stub methods. Invoked by 'InvokerAspect' directly on the 'target'. **
   */
  public Object daySchedule(DayScheduleVO cmd, HttpServletRequest request) { return null; }

}