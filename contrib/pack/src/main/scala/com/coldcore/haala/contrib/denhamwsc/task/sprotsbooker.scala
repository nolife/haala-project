package com.coldcore.haala
package contrib.denhamwsc
package task

import com.coldcore.haala.task._
import com.coldcore.haala.contrib.denhamwsc.service.SportsBookerService

class GetDayScheduleTask extends BaseTask {
  override val name = "denhamwsc.GetDayScheduleTask"

  override def internalRun: Result = {
    sc.get[SportsBookerService].retrieveClassicHtml(loadConfig("pfx"))
    Result()
  }
}
