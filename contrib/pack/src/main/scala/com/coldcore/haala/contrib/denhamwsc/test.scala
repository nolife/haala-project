package com.coldcore.haala
package contrib.denhamwsc
package service

package test {

  class MyMock extends com.coldcore.haala.service.test.MyMock with MockServices

  trait MockServices {
    self: com.coldcore.haala.service.test.MyMock =>

    val sportsBookerService = mock[SportsBookerService]

    serviceContainer.services =
      sportsBookerService :: serviceContainer.services
  }

}
