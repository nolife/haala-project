package com.coldcore.haala
package contrib.denhamwsc
package directive.freemarker

import com.coldcore.haala.directive.freemarker._
import com.coldcore.haala.service.{DomainService, FileService}
import com.coldcore.haala.service.FileService.{SearchInputX => FileSearchInputX}
import com.coldcore.haala.core.SiteState

class ListAlbumPhotosDirective extends BaseDirective with StateWithVars {
  override def execute(state: StateWithVars) {
    import state._
    val domainId = systemVarAsObject[SiteState]("siteState").get.currentDomainId
    val domain = sc.get[DomainService].getById(domainId).get
    val siteK = sc.get[DomainService].listSites(domain)
    val album = pval("album")

    val xs = sc.get[FileService].search(new FileSearchInputX(0, 999, "nam asc") {
      override val queryAnd = s"of_fld:photo/$album"
      override val siteKeys = siteK
    }).objects.map(_.name)

    render(state, xs)
  }
}
