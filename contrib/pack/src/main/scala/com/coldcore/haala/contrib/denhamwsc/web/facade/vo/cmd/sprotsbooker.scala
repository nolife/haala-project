package com.coldcore.haala
package contrib.denhamwsc
package web.facade.vo.cmd

import scala.beans.BeanProperty
import com.coldcore.haala.web.facade.annotations.Input

class DayScheduleVO {
  @BeanProperty @Input (required = true) var pfx: String = _
}
