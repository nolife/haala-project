package com.coldcore.haala
package contrib.denhamwsc
package service

import java.util.Date

import com.coldcore.haala.service.BaseService
import com.coldcore.haala.service.exceptions.ExternalFailException
import com.coldcore.misc.scala.StringOp._
import core.HttpUtil.{HttpCallFailed, HttpCallSuccessful}
import core.{NamedLog, Timestamp}

object SportsBookerService {
  case class DayScheduleOUT(date: Date, slots: List[(String,String)])
}

trait SportsBookerService {
  import SportsBookerService._

  def classicDaySchedule(prefix: String): Option[DayScheduleOUT]
  def retrieveClassicHtml(prefix: String)
}

class SportsBookerServiceImpl extends BaseService with SportsBookerService {
  import SportsBookerService._

  private lazy val LOG = new NamedLog("denhamwsc.sportsbooker-service", log)

  override def classicDaySchedule(domainPfx: String): Option[DayScheduleOUT] =
    parseClassicHtml(label(s"sportsbooker.ds.$domainPfx", defaultSite).safe)

  override def retrieveClassicHtml(domainPfx: String) = {
    val lkey = s"sportsbooker.ds.$domainPfx"
    val now = Timestamp()
    sb.httpGet(s"http://$domainPfx.sports-booker.com/classic/index.php?showDate=${now.toString("yyyy-MM-dd")}") match {
      case call: HttpCallSuccessful =>
        val html = s"<!-- X-GET-DATE ${now.toString("dd/MM/yyyy HH:mm:ss")} -->\n"+call.body
        labelService.write(lkey, html, defaultSite.key)
      case call: HttpCallFailed =>
        labelService.delete(lkey, defaultSite.key)
        LOG.error("SportsBooker GET failed", call.error)
        throw new ExternalFailException
    }
  }

  protected def parseClassicHtml(html: String): Option[DayScheduleOUT] =
    findTokens("<!-- X-GET-DATE ", " -->", html) match {
      case Seq(sdate) =>
        // classic-sb-day-extract.html
        val (date, rtime) = (Timestamp(sdate, "dd/MM/yyyy HH:mm:ss"), "[0-2][0-9]:[0-5][0-9]")
        val strip = (s: String) => s.replaceAll("<[^>]*>", "")
        val slott = //slot tables have slots: TDs with 2 DIVs with time value in the first DIV
          findTokens("<table ", "</table>", html)
            .map(findTokens("<td ", "</td>", _)
            .map(findTokens("<div ", "</div>", _).map(x => x.substring(x.indexOf(">")+1))) //parse DIV values
            .filter(_.size == 2) //DIV with time and DIV with description
            .map { case (List(time,desc)) => (strip(time), strip(desc)) } //strip values into plain text
            .filter { case (time,_) => time.length >= 5 && time.substring(0,5).matches(rtime) } //check time with regexp
            .map { case (time,desc) => (time.substring(0,5), desc) }) //final time correction
        // List(List((09:00,SWIMMING), (09:12,SWIMMING)), List(...))
        // slot table is the one with the max amount of slots in it
        val slots = slott.nonEmpty ? slott.sortWith(_.size > _.size).head | Nil
        slots.nonEmpty ? Some(DayScheduleOUT(date.toDate, slots)) | None
      case _ => None
    }
}
