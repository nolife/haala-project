package com.coldcore.haala
package contrib.denhamwsc
package web.facade.logic

import scala.beans.BeanProperty
import com.coldcore.haala.web.facade.logic._
import com.coldcore.haala.web.facade.annotations.ValidatorClass
import com.coldcore.haala.web.facade.validator.Validator
import com.coldcore.haala.security.annotations.SecuredRole
import javax.servlet.http.HttpServletRequest

import com.coldcore.haala.web.facade.vo.CallResult
import com.coldcore.haala.contrib.denhamwsc.web.facade.vo.cmd.DayScheduleVO
import com.coldcore.haala.contrib.denhamwsc.service.SportsBookerService
import com.coldcore.haala.contrib.denhamwsc.service.SportsBookerService.DayScheduleOUT
import com.coldcore.haala.core.Timestamp

/** Convert input and output data */
trait SportsBookerConvert {
  self: SportsBookerFacade =>

  val dayScheduleVO = (o: DayScheduleOUT) =>
    Map('date -> Timestamp(o.date).toString("dd/MM/yyyy HH:mm:ss"), 'slots -> o.slots.map { case (time,desc) => time+"|"+desc })
}

class SportsBookerFacade extends BaseFacade with SportsBookerConvert {
  @BeanProperty @ValidatorClass var baseValidator: Validator = _

  val FACADE_NO_DAY_SCHEDULE = 1001

  @SecuredRole("ANONYMOUS")
  def daySchedule(cmd: DayScheduleVO, request: HttpServletRequest): CallResult =
    sc.get[SportsBookerService].classicDaySchedule(cmd.pfx) match {
      case Some(ds) => dataCR(dayScheduleVO(ds))
      case None => errorCR(FACADE_NO_DAY_SCHEDULE)
    }

}
