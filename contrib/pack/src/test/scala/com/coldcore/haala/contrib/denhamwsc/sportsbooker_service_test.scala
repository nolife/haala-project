package com.coldcore.haala
package contrib.denhamwsc
package service

import org.junit.runner.RunWith
import org.scalatest.junit.JUnitRunner
import org.scalatest.{BeforeAndAfter, WordSpec}
import org.scalatest.mockito.MockitoSugar
import java.text.SimpleDateFormat
import java.util.Date
import org.mockito.Mockito._
import core.HttpUtil.{HttpCallFailed, HttpCallSuccessful}
import com.coldcore.haala.service.exceptions.ExternalFailException
import core.SiteChain.Implicits._

@RunWith(classOf[JUnitRunner])
class SportsBookerServiceSpec extends WordSpec with MockitoSugar with BeforeAndAfter {
  var m: MyMock = _

  before {
    m = new MyMock
  }

  class XSportsBookerServiceImpl extends SportsBookerServiceImpl {
    override def parseClassicHtml(html: String) = super.parseClassicHtml(html)
  }

  class MyMock extends com.coldcore.haala.contrib.denhamwsc.service.test.MyMock {
    val service = new XSportsBookerServiceImpl { override val staticBlock = m.staticBlock; serviceContainer = m.serviceContainer }
    when(settingService.getDefaultSite).thenReturn("xx1")
  }

  val PACK = "denhamwsc"

  val strToDate = (date: String) => new SimpleDateFormat("dd/MM/yyyy HH:mm:ss").parse(date)

  "parseClassicHtml" should {
    "parse control day extract" in {
      val html = m.getResourceAsString(s"/$PACK/classic-sb-day-extract.html")
      val ds = m.service.parseClassicHtml(html).get
      assertResult(strToDate("10/10/2015 13:12:55")) { ds.date }
      assertResult(6) { ds.slots.size }

      val expected =
        "09:00|SWIMMING" :: "10:12|" :: "11:00|Booked" :: "11:12|" ::
        "13:00|Sergei" :: "20:48|Too far ahead to book." :: Nil
      ds.slots.zip(expected).foreach { case ((time,desc), e) => assertResult(e) { time+"|"+desc }}
    }

    "parse disabled login html" in {
      val html = m.getResourceAsString(s"/$PACK/classic-sb-20151010-disabled.html")
      val ds = m.service.parseClassicHtml(html).get
      assertResult(strToDate("10/10/2015 13:12:55")) { ds.date }
      assertResult(60) { ds.slots.size }
    }

    "parse editable login html" in {
      val html = m.getResourceAsString(s"/$PACK/classic-sb-20151031-editable.html")
      val ds = m.service.parseClassicHtml(html).get
      assertResult(strToDate("31/10/2015 03:34:13")) { ds.date }
      assertResult(60) { ds.slots.size }
    }

    "parse nologin html" in {
      val html = m.getResourceAsString(s"/$PACK/classic-sb-20151017-nologin.html")
      val ds = m.service.parseClassicHtml(html).get
      assertResult(strToDate("17/10/2015 18:22:16")) { ds.date }
      assertResult(60) { ds.slots.size }
    }

    "parse nologin too far ahead html" in {
      val html = m.getResourceAsString(s"/$PACK/classic-sb-20151121-toofar.html")
      val ds = m.service.parseClassicHtml(html).get
      assertResult(strToDate("21/11/2015 05:00:23")) { ds.date }
      assertResult(60) { ds.slots.size }
    }

    "none on damaged html" in {
      val html = "<!-- X-GET-DATE 17/10/2015 18:22:16 -->\n<html> damaged content </html>"
      val ds = m.service.parseClassicHtml(html)
      assertResult(None) { ds }
    }
  }

  "retrieveClassicHtml" should {
    "make get call and store html" in {
      val (sdf, sdf2, now) = (new SimpleDateFormat("yyyy-MM-dd"), new SimpleDateFormat("dd/MM/yyyy HH:mm:ss"), new Date)
      val url = s"http://denham.sports-booker.com/classic/index.php?showDate=${sdf.format(now)}"
      val html = m.getResourceAsString(s"/$PACK/classic-sb-20151017.html")
      when(m.staticBlock.httpGet(url)).thenReturn(HttpCallSuccessful(url, 200, html, Nil))
      m.service.retrieveClassicHtml("denham")
      verify(m.labelService).write("sportsbooker.ds.denham", s"<!-- X-GET-DATE ${sdf2.format(now)} -->\n"+html, "xx1")
    }

    "on error delete stored html" in {
      val (sdf, now) = (new SimpleDateFormat("yyyy-MM-dd"), new Date)
      val url = s"http://denham.sports-booker.com/classic/index.php?showDate=${sdf.format(now)}"
      when(m.staticBlock.httpGet(url)).thenReturn(HttpCallFailed(url, new Exception("failed")))
      intercept[ExternalFailException] { m.service.retrieveClassicHtml("denham") }
      verify(m.labelService).delete("sportsbooker.ds.denham", "xx1")
    }
  }

  "classicDaySchedule" should {
    "parse html and return day schedule" in {
      val html = m.getResourceAsString(s"/$PACK/classic-sb-20151017-nologin.html")
      when(m.labelService.query("sportsbooker.ds.denham", "xx1")).thenReturn(html)
      val ds = m.service.classicDaySchedule("denham")
      assertResult(60) { ds.get.slots.size }
    }

    "return none on missing schedule" in {
      val ds = m.service.classicDaySchedule("denham")
      assertResult(None) { ds }
    }
  }
}
