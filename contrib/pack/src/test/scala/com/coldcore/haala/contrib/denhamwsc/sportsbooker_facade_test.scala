package com.coldcore.haala
package contrib.denhamwsc
package web.facade.logic

import org.junit.runner.RunWith
import org.scalatest.junit.JUnitRunner
import org.scalatest.{BeforeAndAfter, WordSpec}
import org.scalatest.mockito.MockitoSugar
import org.mockito.Mockito._
import org.mockito.ArgumentMatchers._
import web.facade.vo.cmd.DayScheduleVO
import com.coldcore.haala.web._
import service.test.MockServices
import service.SportsBookerService.DayScheduleOUT
import java.util.Date
import java.text.SimpleDateFormat
import com.coldcore.haala.core.SiteChain.Implicits._
import test.SetupMods

@RunWith(classOf[JUnitRunner])
class SportsBookerFacadeSpec extends WordSpec with MockitoSugar with BeforeAndAfter {
  var m: MyMock = _

  before {
    m = new MyMock
  }

  class MyMock extends com.coldcore.haala.web.test.MyMock with MockServices with SetupMods {
    val facade = new SportsBookerFacade |< { x => x.serviceContainer = serviceContainer }
    runSetupMods()
  }

  "daySchedule" should {
    "return proper json" in {
      val (sdf, now) = (new SimpleDateFormat("dd/MM/yyyy HH:mm:ss"), new Date)
      val ds = DayScheduleOUT(now, ("09:00", "SWIMMING") :: ("09:12", "") :: ("20:48", "DARK") :: Nil)
      when(m.sportsBookerService.classicDaySchedule("denham")).thenReturn(Some(ds))
      val cr = m.facade.daySchedule(new DayScheduleVO { pfx = "denham" }, m.request)
      val rs = cr.result.asInstanceOf[Map[Symbol,Any]]
      assertResult(false) { cr.failed }
      assertResult(sdf.format(ds.date)) { rs('date) }
      assertResult("09:00|SWIMMING" :: "09:12|" :: "20:48|DARK" :: Nil) { rs('slots) }
    }

    "return error code when no schedule" in {
      when(m.sportsBookerService.classicDaySchedule("denham")).thenReturn(None)
      val cr = m.facade.daySchedule(new DayScheduleVO { pfx = "denham" }, m.request)
      assertResult(true) { cr.failed }
      assertResult(1001) { cr.errorCodes.head }
    }
  }

}
