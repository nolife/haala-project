if (typeof dwr == 'undefined' || dwr.engine == undefined) throw new Error('You must include DWR engine before including this file');

(function() {
  if (dwr.engine._getObject("DenhamwscSportsBookerF") == undefined) {
    var p;

    p = {};
    p._path = '/dwr';

    p.daySchedule = function(p0, callback) {
      return dwr.engine._execute(p._path, 'DenhamwscSportsBookerF', 'daySchedule', arguments);
    };

    dwr.engine._setObject("DenhamwscSportsBookerF", p);
  }
})();
