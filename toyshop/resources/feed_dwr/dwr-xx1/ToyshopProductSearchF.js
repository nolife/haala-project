if (typeof dwr == 'undefined' || dwr.engine == undefined) throw new Error('You must include DWR engine before including this file');

(function() {
  if (dwr.engine._getObject("ToyshopProductSearchF") == undefined) {
    var p;

    p = {};
    p._path = '/dwr';

    p.dfilter = function(p0, callback) {
      return dwr.engine._execute(p._path, 'ToyshopProductSearchF', 'dfilter', arguments);
    };

    p.dsearch = function(p0, callback) {
      return dwr.engine._execute(p._path, 'ToyshopProductSearchF', 'dsearch', arguments);
    };

    p.search = function(p0, callback) {
      return dwr.engine._execute(p._path, 'ToyshopProductSearchF', 'search', arguments);
    };

    dwr.engine._setObject("ToyshopProductSearchF", p);
  }
})();
