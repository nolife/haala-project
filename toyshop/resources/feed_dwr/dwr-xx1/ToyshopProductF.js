if (typeof dwr == 'undefined' || dwr.engine == undefined) throw new Error('You must include DWR engine before including this file');

(function() {
  if (dwr.engine._getObject("ToyshopProductF") == undefined) {
    var p;

    p = {};
    p._path = '/dwr';

    p.addFaces = function(p0, callback) {
      return dwr.engine._execute(p._path, 'ToyshopProductF', 'addFaces', arguments);
    };

    p.editFaces = function(p0, callback) {
      return dwr.engine._execute(p._path, 'ToyshopProductF', 'editFaces', arguments);
    };

    p.editPrice = function(p0, callback) {
      return dwr.engine._execute(p._path, 'ToyshopProductF', 'editPrice', arguments);
    };

    p.editWarehouse = function(p0, callback) {
      return dwr.engine._execute(p._path, 'ToyshopProductF', 'editWarehouse', arguments);
    };

    p.remove = function(p0, callback) {
      return dwr.engine._execute(p._path, 'ToyshopProductF', 'remove', arguments);
    };

    p.create = function(p0, callback) {
      return dwr.engine._execute(p._path, 'ToyshopProductF', 'create', arguments);
    };

    p.editDetails = function(p0, callback) {
      return dwr.engine._execute(p._path, 'ToyshopProductF', 'editDetails', arguments);
    };

    p.editProps = function(p0, callback) {
      return dwr.engine._execute(p._path, 'ToyshopProductF', 'editProps', arguments);
    };

    p.faces = function(p0, callback) {
      return dwr.engine._execute(p._path, 'ToyshopProductF', 'faces', arguments);
    };

    dwr.engine._setObject("ToyshopProductF", p);
  }
})();
