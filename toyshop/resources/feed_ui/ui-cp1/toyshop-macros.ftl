
<#macro productOptionRow product group >
  <div class="option-row">
    <@toyshop.listProductOptionTypes product=product group=group ; type >
      <div class="option" data-type="${type}">
        <@toyshop.productHasOption var="has" product=product type=type />
        <@toyshop.productOptionValue var="oval" product=product type=type />
        <@toyshop.productOptionMode var="mode" type=type />
        <input type="checkbox" <#if has > checked="true" </#if> />
        <p><@toyshop.productOptionKey type=type /></p>
        <#if mode==1 || mode==2 >
          <input type="text" value="${oval}"/>
        </#if>
      </div>
      <#local typesNotEmpty=true />
    </@>
    <#if typesNotEmpty!false ><div class="nofloat"></div></#if>
  </div>
</#macro>


<#macro productTextRow product key site txtshow=false >
  <div class="text-row" data-key="${key}">
    <@toyshop.productTextSpread var="spread" product=product key=key site="."+site />
    <@h.paramLimit var="frags" key="genFrag"/>
    <#if txtshow && spread?size==0 >
      <p class="add-extra"><@label "toyshop.5"/></p>
    </#if>
    <#list 1..frags as i>
      <#local val=""/>
      <#if i<=spread?size ><#local val=spread[i-1] /></#if>
      <input type="text" <#if val=="" && i!=1 || txtshow && spread?size==0 > style="display:none;" </#if> value="${val}"/>
    </#list>
    <div class="nofloat"></div>
  </div>
</#macro>


<#macro pictureUploadForm >
  <@htm.form id="upload-form" mode="upload">
  <div class="form-panel">
    <h3 class="cell">
      <@label"toyshop.9"/>
      <span class="expand" title="<@label "toyshop.6"/>"></span>
    </h3>
    <div class="slide">
      <div class="file-upload">
        <@h.paramLimit var="uplim" key="upload"/>
        <#list 1..uplim as z>
          <div class="cell" data-row="${z}">
            <label><#if z==1 ><@label "toyshop.7"/><#else> &nbsp; </#if></label>
            <input name="dat${z}" type="file" size="20"/>
            <span data-clear="dat${z}" title="<@label "toyshop.8"/>" ></span>
          </div>
        </#list>
      </div>
      <div class="cell submit">
        <button><@label "btn.upload"/></button>
      </div>
    </div>
  </div>
  </@>
</#macro>

