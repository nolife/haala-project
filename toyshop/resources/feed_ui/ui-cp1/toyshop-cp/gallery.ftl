<#import "/web/common.ftl" as c >
<#import "/web/common-htm.ftl" as htm >
<#include "/web/macros.ftl:"+currentSite >
<#include "/web/toyshop-macros.ftl:"+currentSite >

<#if product??>

  <@pictureUploadForm/>

  <@htm.form id="page-form">
    <div class="form-panel">
      <div class="tabs opener site-tabs">
        <@inputSiteTabs />
        <@h.listInputSites ; site >
          <div id="site-tab-${site}">

          </div>
        </@>
      </div>
      <div class="cell submit">
        <button><@label "btn.save"/></button>
      </div>
    </div>
  </@>

  <@htm.nodiv "quick-navigation-content">
    <a href="edit.htm?ref=${product.ref()}"><@label "003"/></a>
    <a href="main.htm"><@label "002"/></a>
  </@>

  <@htm.nodiv "page-ivars">
    { productId: "${product.id()}", ref: "${product.ref()}" }
  </@>

  <@htm.nodiv "page-templates">

    <div class="face-item">
      <img src=""/>
      <div>
        <input type="text" maxlength="100"/>
        <p><span class="icon move-up"></span><span class="icon move-down"></span><span class="icon remove"></span></p>
      </div>
    </div>

  </@>

<#else>

  <@label "toyshop.4"/>

</#if>
