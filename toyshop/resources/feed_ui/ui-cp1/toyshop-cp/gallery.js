
(function($) {

  Haala.Misc.onDocReady(function() {
    initGalleryForm();
  });



  var initGalleryForm = function() {
    var ivars = Haala.Misc.evalJSON("#page-ivars");
    if (!ivars) return;

    $("#page-form .site-tabs").tabs();
    $("#page-form .submit button").button();

    var sites = Haala.Misc.systemVar("inputSites");

    var faces = [];
    var facesLoaded = function(vos) {
      faces = [];
      if (vos.length) $("#page-form").show();
      $.each(sites, function(z, site) { $("#site-tab-"+site).empty(); });

      $.each(vos, function(n, vo) {
        $.each(sites, function(z, site) {

          var element = $("#page-templates .face-item").clone().appendTo("#site-tab-"+site);
          element.find("img").attr("src", "/"+vo.picture.path);
          $.each(vo.labels, function(z, v) {
            if (v.site == site) element.find("input").val(v.value);
          });

          var face = { element: element, site: site, id: vo.picture.id, position: n+1 };
          faces.push(face);

          /* delete picture */
          element.find(".remove").aclick(function() {
            element.find("img").hide("explode", 400, function() {
              element.hide("blind", { direction: "vertical" }, 200, function() {
                var a = facesByPosition(face.position);
                for (var z = 0; z < a.length; z++) {
                  a[z].element.remove();
                  a[z].deleted = true;
                  a[z].position = -100;
                  Haala.Code.stretch();
                }
              });
            });
          });

          /* move picture up */
          element.find(".move-up").aclick(function() {
            var x = faceBySiteAndPosition(site, face.position-1);
            if (x) {
              element.hide("slide", { direction: "up" }, 400, function() {
                var a = facesByPosition(face.position);
                var b = facesByPosition(x.position);
                for (var z = 0; z < a.length; z++) {
                  Haala.Misc.swapNodes(a[z].element[0], b[z].element[0]);
                  a[z].position--;
                  b[z].position++;
                }
                element.show("slide", { direction: "down" }, 400);
              });
            }
          });

          /* move picture down */
          element.find(".move-down").aclick(function() {
            var x = faceBySiteAndPosition(site, face.position+1);
            if (x) {
              element.hide("slide", { direction: "down" }, 400, function() {
                var a = facesByPosition(face.position);
                var b = facesByPosition(x.position);
                for (var z = 0; z < a.length; z++) {
                  Haala.Misc.swapNodes(a[z].element[0], b[z].element[0]);
                  a[z].position++;
                  b[z].position--;
                }
                element.show("slide", { direction: "up" }, 400);
              });
            }
          });

        });
      });

      setTimeout(Haala.Code.stretch, 100);
    };
    var loadFaces = function() {
      Haala.AJAX.call("#page-form", ToyshopProductF.faces, { productId: ivars.productId },
        function (data) { facesLoaded(data.result) });
    };

    var faceBySiteAndPosition = function(/*string*/ site, /*int*/ position) {
      var x = null;
      $.each(faces, function(n, face) {
        if (position == face.position && site == face.site) x = face;
      });
      return x;
    };
    var facesByPosition = function(/*int*/ position) {
      var x = [];
      $.each(faces, function(n, face) {
        if (position == face.position) x.push(face);
      });
      return x;
    };

    /* reflect visibility and position across tabs  */
    var syncFace = function(/*json*/ face) {
      $.each(faces, function(n, f) {
        if (f.index == face.index && f.site != face.site) {
          f.deleted = face.deleted;
          f.position = face.position;
          if (f.deleted) f.element.hide();
        }
      });
    };

    initUploadForm(loadFaces);
    loadFaces();

    var submitHandler = function() {
      var command = { productId: ivars.productId, faces: [] };
      $.each(faces, function(n, face) {
        command.faces.push({
          id: face.id,
          deleted: face.deleted ? 1 : 0,
          site: face.site,
          text: face.element.find("input").val(),
          position: face.position });
      });

      editFaces(command);
    };
    $("#page-form").submit(submitHandler);

    var editFaces = function(/*json*/ command) {
      Haala.AJAX.call("body", ToyshopProductF.editFaces, command,
        function() { document.location = "edit.htm?ref="+ivars.ref; return Haala.AJAX.DO_NOTHING; });
    };

  };

  var initUploadForm = function(/*fn*/ successFn) {
    var ivars = Haala.Misc.evalJSON("#page-ivars");

    var integ = Haala.UploadForm.Integ;
    var form = $("#upload-form").uploadForm(
      $.extend(true, {}, integ.DefaultOptions, {
        uploadSuccessFn: function(/*json*/ model) {
          integ.collapseForm(model);
          Haala.AJAX.call("body", ToyshopProductF.addFaces, { productId: ivars.productId },
            function() { successFn(); });
        }
      }));
    integ.resetExpandableForm(form.model);
    integ.bindSubmitTo("#upload-form .submit button", form.model);
  };

})(jQuery);

