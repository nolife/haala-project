
(function($) {

  Haala.Misc.onDocReady(function() {
    initNavigation();
    initProductList();
  });




  var initNavigation = function() {
    $("#context-navigation").kdxMenu({
      items: [
        { element: "#context-navigation .add-product",
          gotoFn: function(/*json*/ item, /*json*/ model) {
            Haala.AJAX.call("body", ToyshopProductF.create, {},
                function(data) { document.location = "edit.htm?ref="+data.result; return Haala.AJAX.DO_NOTHING; });
          }}
      ]
    });
  };


  var initProductList = function() {
    var msgFn = function(/*string*/ msg, /*json*/ tokens) {
      return Haala.Misc.messageMapping("#page-messages", msg, tokens);
    };

    var listModel = {
      loadItemsFn: function(/*json*/ model, /*fn*/ callback) {
        Haala.AquaList.Integ.loadItemsFn(ToyshopProductSearchF.dsearch, { target: 2 }, model, callback);
      },
      rowElementFn: function(/*json*/ vo, /*int*/ position, /*json*/ model) {
        var card = null;
        if (vo != null) {
          if (!vo.thumb) vo.thumb = "web/toyshop/list-thumb.gif";
          var status = msgFn("product-status"+vo.status);

          card = $("#page-templates .data-item").clone();
          card.find("a").attr("href", "edit.htm?ref="+vo.ref);
          card.find("img").attr("src", "/"+vo.thumb);
          card.find("p.name").html(vo.name);
          card.find("p.info").html(status);
          card.aclick(function() {
            document.location = "edit.htm?ref="+vo.ref;
          });
        }
        return card;
      }
    };
    var list = $("#product-list .rows").aquaList(
      $.extend(true, {},
        Haala.AquaList.Integ.DefaultOptions,
        { rows: 999 },
        listModel));
    list.addRenderedFn(Haala.Code.stretch);

  };

})(jQuery);

