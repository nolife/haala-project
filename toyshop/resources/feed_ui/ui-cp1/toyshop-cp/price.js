
(function($) {

  Haala.Misc.onDocReady(function() {
    initPriceForm();
  });




  var initPriceForm = function() {
    var ivars = Haala.Misc.evalJSON("#page-ivars");
    if (!ivars) return;

    $("#page-form .site-tabs").tabs();
    $("#page-form .submit button").button();

    Haala.Misc.tinymce4("#page-form textarea", {
      plugins: "autoresize",
      toolbar1: "undo redo | bold italic underline strikethrough | alignleft aligncenter alignright alignjustify | bullist numlist | outdent indent",
      width: "730" // tinymce-4.9.1 bug: width of textarea calculated incorrectly
    });

    var finp = Haala.Util.panelFormInput;
    var sites = Haala.Misc.systemVar("inputSites");
    var input = [
      finp("currency", { require: true }),
      finp("price",    { require: true, regex: ["float"] }),
      finp("dscprice", { regex: ["float"] })
    ];
    $.each(sites, function(n, site) {
      input.push({ element: "#site-tab-"+site+" .note textarea", key: "note_"+site});
    });
    var panel = $("#page-form").kdxPanel(
      $.extend(true, {}, Haala.KdxPanel.Integ.DefaultOptions, {
        input: input
      }));

    Haala.Util.formSubmitPanel(panel, function(/*json*/ container) {
      var command = Haala.Misc.collectInput(container,
        ["currency", "price", "dscprice"],
        ["currency", "price", "dscPrice"]);
      command.notes = Haala.Misc.collectSiteableInput(container, ["note"]);
      command.productId = ivars.productId;
      editPrice(command);
    });

    var editPrice = function(/*json*/ command) {
      Haala.AJAX.call("body", ToyshopProductF.editPrice, command,
        function() { document.location = "edit.htm?ref="+ivars.ref; return Haala.AJAX.DO_NOTHING; });
    };

  };

})(jQuery);
