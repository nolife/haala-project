
(function($) {

  Haala.Misc.onDocReady(function() {
    initWarehouseForm();
  });




  var initWarehouseForm = function() {
    var ivars = Haala.Misc.evalJSON("#page-ivars");
    if (!ivars) return;

    $("#page-form .submit button").button();
    $("#page-form .expected input").datepicker({ dateFormat: 'dd/mm/yy' });

    var finp = Haala.Util.panelFormInput;
    var input = [
      finp("status",   { require: true }),
      finp("quantity", { require: true, regex: ["number"] }),
      finp("expected", { regex: ["date"] })
    ];

    var panel = $("#page-form").kdxPanel(
      $.extend(true, {}, Haala.KdxPanel.Integ.DefaultOptions, {
        input: input
      }));

    Haala.Util.formSubmitPanel(panel, function(/*json*/ container) {
      var command = Haala.Misc.collectInput(container, ["status", "quantity", "expected"]);
      command.productId = ivars.productId;
      editWarehouse(command);
    });

    var editWarehouse = function(/*json*/ command) {
      Haala.AJAX.call("body", ToyshopProductF.editWarehouse, command,
        function() { document.location = "edit.htm?ref="+ivars.ref; return Haala.AJAX.DO_NOTHING; });
    };

  };

})(jQuery);
