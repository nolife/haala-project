
(function($) {

  Haala.Misc.onDocReady(function() {
    initDetailsForm();
  });




  var initDetailsForm = function() {
    var ivars = Haala.Misc.evalJSON("#page-ivars");
    if (!ivars) return;

    $("#page-form .site-tabs").tabs();
    $("#page-form .submit button").button();

    Haala.Misc.tinymce4("#page-form textarea:not(.norich)", {
      plugins: "autoresize",
      toolbar1: "undo redo | bold italic underline strikethrough | alignleft aligncenter alignright alignjustify | bullist numlist | outdent indent",
      width: "730" // tinymce-4.9.1 bug: width of textarea calculated incorrectly
    });

    var sites = Haala.Misc.systemVar("inputSites");

    /*
     frags:
     Multilingual input fields, such fields have values entered manually.
     If a user adds extra fields on one tab this has to be reflected on other tabs, so that she
     can easily provide translation without extra clicks. Frags collected from every tab.

     options:
     Checkboxes with attached input fields or select elements.
     Those are not multilingual and if a user changes something on one tab this must be reflected
     on other tabs. Options collected and from just one tab.
     */

    var frags = [], options = [];
    $.each(sites, function(n, site) {
      $.each([
        /* frag and option selectors */
        "#site-tab-"+site+" .cell.material",
        "#site-tab-"+site+" .cell.stuff"
      ], function(z, prefix) {

        /* frag with "add extra" link */
        var extra = $(prefix+" .add-extra");
        var frag = $(prefix+" .text-row").fragInput({
          showPerRow: 4,
          renderedFn: function(/*json*/ model) { fragSync(); },
          userData: { extraElement: extra, site: site }
        });
        frag.model.userData.key = frag.model.element.attr("data-key"); //comes from HTML
        if (frag.model.element.length) frags.push(frag);
        extra.click(function() { frag.showFirstRow(); });

        /* option with attached input */
        $(prefix+" .option-row .option").each(function(i) {
          var e = $(this);
          var ce = e.find(":checkbox"), ie = e.find(":text"), se = e.find("select");
          var option = { checkboxElement: ce, inputElement: ie, selectElement: se, site: site };
          option.type = e.attr("data-type"); //comes from HTML
          if (option.checkboxElement.length) options.push(option);
          ce.click(function() {
            se.focus();
            ie.focus();
            optionSync(option);
          });
          ie.keyup(function() { ce.prop("checked", ie.val() != ""); });
          ie.change(function() {
            ce.prop("checked", ie.val() != "");
            optionSync(option);
          });
          se.keyup(function() { ce.prop("checked", se.val() != ""); });
          se.change(function() {
            ce.prop("checked", se.val() != "");
            optionSync(option);
          });
        });

      });
    });

    /* reflect visibility state across tabs */
    var fragSync = function() {
      $.each(frags, function(a, fragA) {
        $.each(frags, function(b, fragB) {

          if (fragA.model.userData.key == fragB.model.userData.key)
            $.each(fragA.model.input, function(n, inp) {
              if (inp.element.is(":visible")) {
                fragB.model.input[n].element.show();
                fragB.model.userData.extraElement.hide();
              }
            });

        });
      });
    };

    /* reflect all changes across tabs */
    var optionSync = function(/*json*/ option) {
      $.each(options, function(n, opt) {
        if (opt.type == option.type) {
          opt.checkboxElement.prop("checked", option.checkboxElement.is(":checked"));
          opt.inputElement.val(option.inputElement.val());
          opt.selectElement.val(option.selectElement.val());
        }
      });
    };

    /* collect frags from specific tab (value stored as line-break-separated string) */
    var collectFrags = function(/*string*/ site) {
      var x = {};
      $.each(frags, function(n, frag) {
        if (frag.model.userData.site == site)
          $.each(frag.model.input, function(n, input) {
            var v = input.element.val();
            x[frag.model.userData.key] = (x[frag.model.userData.key] || "")+"\n"+(v || "");
          });
      });
      return x;
    };

    /* collect options from specific tab */
    var collectOptions = function(/*string*/ site) {
      var x = [];
      $.each(options, function(n, option) {
        if (option.site == site && option.checkboxElement.is(":checked")) {
          var v = option.inputElement.val() || option.selectElement.val() || "";
          x.push({ type: option.type, value: v });
        }
      });
      return x;
    };

    /* collect text from specific tab */
    var collectText = function(/*string*/ site) {
      var x = {};
      $.each([
        /* text selectors */
        "#site-tab-"+site+" .cell.name input",
        "#site-tab-"+site+" .cell.description textarea",
        "#site-tab-"+site+" .cell.summary textarea"
      ], function(z, selector) {
        var e = $(selector);
        var key = e.attr("data-key"); //comes from HTML
        x[key] = e.val();
      });
      return x;
    };

    var submitHandler = function() {
      var command = { productId: ivars.productId, details: [], options: [] };

      $.each(sites, function(n, site) {
        var x = { site: site };
        command.details.push(x);
        $.extend(true, x, collectText(site));
        $.extend(true, x, collectFrags(site));
        if (n == 0) command.options = collectOptions(site); //only form the first tab
      });

      editDetails(command);
    };
    $("#page-form").submit(submitHandler);

    var editDetails = function(/*json*/ command) {
      Haala.AJAX.call("body", ToyshopProductF.editDetails, command,
          function() { document.location = "edit.htm?ref="+ivars.ref; return Haala.AJAX.DO_NOTHING; });
    };

  };

})(jQuery);

