<#import "/web/common.ftl" as c >
<#import "/web/common-htm.ftl" as htm >
<#include "/web/macros.ftl:"+currentSite >
<#include "/web/toyshop-macros.ftl:"+currentSite >

<#if product??>

  <@htm.form id="page-form">
    <div class="form-panel">
      <div class="tabs opener site-tabs">
        <@inputSiteTabs />
        <@h.listInputSites ; site >
          <div id="site-tab-${site}">
            <div class="two-column">
              <div class="cell name">
                <label><@label "004"/></label>
                <input data-key="name" type="text" maxlength="100" value="<@toyshop.productText product=product key="name" site="."+site />" />
                <p class="error"></p>
              </div>
            </div>
            <div class="cell summary">
              <label><@label "008"/></label>
              <textarea data-key="sumr" class="norich"><@toyshop.productText product=product key="sumr" site="."+site /></textarea>
            </div>
            <div class="cell description">
              <label><@label "007"/></label>
              <textarea data-key="desc"><@toyshop.productText product=product key="desc" site="."+site /></textarea>
            </div>
            <div class="cell material">
              <label><@label "005"/></label>
              <@productOptionRow product "material" />
              <@productTextRow product "matr" site true/>
            </div>
            <div class="cell stuff">
              <label><@label "006"/></label>
              <@productOptionRow product "stuff"/>
              <@productTextRow product "stuf" site true/>
            </div>
          </div>
        </@>
      </div>
      <div class="cell submit">
        <button><@label "btn.save"/></button>
      </div>
    </div>
  </@>

  <@htm.nodiv "quick-navigation-content">
    <a href="edit.htm?ref=${product.ref()}"><@label "003"/></a>
    <a href="main.htm"><@label "002"/></a>
  </@>

  <@htm.nodiv "page-ivars">
    { productId: "${product.id()}", ref: "${product.ref()}" }
  </@>

<#else>

  <@label "toyshop.4"/>

</#if>
