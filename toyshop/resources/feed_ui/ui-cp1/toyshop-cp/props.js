
(function($) {

  Haala.Misc.onDocReady(function() {
    initPropsForm();
  });




  var initPropsForm = function() {
    var ivars = Haala.Misc.evalJSON("#page-ivars");
    if (!ivars) return;

    $("#page-form .submit button").button();

    var finp = Haala.Util.panelFormInput;
    var input = [
      finp("status", { require: true })
    ];

    var panel = $("#page-form").kdxPanel(
      $.extend(true, {}, Haala.KdxPanel.Integ.DefaultOptions, {
        input: input
      }));

    Haala.Util.formSubmitPanel(panel, function(/*json*/ container) {
      var command = Haala.Misc.collectInput(container, ["status"]);
      command.productId = ivars.productId;
      command.domains = "";
      $("#page-form .domains input:checked").each(function() {
        command.domains += ","+$(this).attr("data-id");
      });
      if (command.domains) command.domains = command.domains.substring(1);
      editProps(command);
    });

    var editProps = function(/*json*/ command) {
      Haala.AJAX.call("body", ToyshopProductF.editProps, command,
        function() { document.location = "edit.htm?ref="+ivars.ref; return Haala.AJAX.DO_NOTHING; });
    };

  };

})(jQuery);
