<#import "/web/common.ftl" as c >
<#import "/web/common-htm.ftl" as htm >
<#include "/web/macros.ftl:"+currentSite >

<#if product??>

  <div class="navigation-pane">
    <a class="details" href="details.htm?ref=${product.ref()}" title="<@label "010"/>" ><@label "009"/></a>
    <a class="gallery" href="gallery.htm?ref=${product.ref()}" title="<@label "014"/>" ><@label "013"/></a>
    <a class="price" href="price.htm?ref=${product.ref()}" title="<@label "012"/>" ><@label "011"/></a>
    <a class="warehouse" href="warehouse.htm?ref=${product.ref()}" title="<@label "016"/>" ><@label "015"/></a>
    <a class="props" href="props.htm?ref=${product.ref()}" title="<@label "019"/>" ><@label "018"/></a>
  </div>

  <@htm.nodiv "quick-navigation-content">
    <a href="main.htm"><@label "002"/></a>
  </@>

  <@htm.nodiv "context-navigation-content">
    <a class="remove" href="#" title="<@label "004"/>" ><@label "003"/></a>
  </@>

  <@htm.nodiv "page-ivars">
    { productId: "${product.id()}", ref: "${product.ref()}", status: "${product.status()}" }
  </@>

  <@htm.nodiv "page-messages">
    <div data-key="product-delete"><@label "017"/></div>
  </@>

<#else>

  <@label "toyshop.4"/>

</#if>
