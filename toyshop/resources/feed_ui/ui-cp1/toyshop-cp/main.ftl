<#import "/web/common.ftl" as c >
<#import "/web/common-htm.ftl" as htm >
<#include "/web/macros.ftl:"+currentSite >

<div id="product-list">
  <div class="rows"></div>
  <div class="nofloat"></div>
</div>

<@htm.nodiv "context-navigation-content">
  <a class="add-product" href="#" title="<@label "002"/>"><@label "003"/></a>
</@>

<@htm.nodiv "page-messages">
  <div data-key="product-status0"><@label "toyshop.prsta0"/></div>
  <div data-key="product-status1"><@label "toyshop.prsta1"/></div>
  <div data-key="product-status2"><@label "toyshop.prsta2"/></div>
</@>

<@htm.nodiv "page-templates">

  <div class="data-item">
    <a href="#">
      <img src=""/>
      <p class="name"></p>
      <p class="info"></p>
    </a>
  </div>

</@>
