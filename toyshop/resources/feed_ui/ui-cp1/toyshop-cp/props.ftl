<#import "/web/common.ftl" as c >
<#import "/web/common-htm.ftl" as htm >
<#include "/web/macros.ftl:"+currentSite >
<#include "/web/toyshop-macros.ftl:"+currentSite >

<#function x_hasDomain domainId>
  <#list product.domains() as d>
    <#if d.id()==domainId><#return true></#if>
  </#list>
  <#return false>
</#function>


<#if product??>

  <@htm.form id="page-form">
    <div class="form-panel">
      <div class="cell status">
        <label><@label "006"/></label>
        <select>
          <#list 0..2 as n>
            <option value="${n}" <#if product.status()==n > selected="true" </#if> ><@label "toyshop.prsta${n}"/></option>
          </#list>
        </select>
        <p class="error"></p>
      </div>
      <div class="cell domains">
        <label><@label "005"/></label>
        <@h.listUserDomains ; x >
          <p><input data-id="${x.id()}" type="checkbox" <#if x_hasDomain(x.id()) > checked="true" </#if> /><span>${x.domain()}</span></p>
        </@>
      </div>
      <div class="cell submit">
        <button><@label "btn.save"/></button>
      </div>
    </div>
  </@>

  <@htm.nodiv "quick-navigation-content">
    <a href="edit.htm?ref=${product.ref()}"><@label "003"/></a>
    <a href="main.htm"><@label "002"/></a>
  </@>

  <@htm.nodiv "page-ivars">
    { productId: "${product.id()}", ref: "${product.ref()}" }
  </@>

<#else>

  <@label "toyshop.4"/>

</#if>
