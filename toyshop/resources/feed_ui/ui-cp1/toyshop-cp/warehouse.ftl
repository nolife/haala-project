<#import "/web/common.ftl" as c >
<#import "/web/common-htm.ftl" as htm >
<#include "/web/macros.ftl:"+currentSite >
<#include "/web/toyshop-macros.ftl:"+currentSite >

<#if product??>

  <@htm.form id="page-form">
    <div class="form-panel">
      <div class="two-column">
        <div class="cell left status">
          <label><@label "006"/></label>
          <select>
            <#list 0..2 as n>
              <option value="${n}" <#if product.status()==n > selected="true" </#if> ><@label "toyshop.prsta${n}"/></option>
            </#list>
          </select>
          <p class="error"></p>
        </div>
        <div class="cell left quantity">
          <label><@label "005"/></label>
          <input type="text" maxlength="100" value="${(product.warehouse().quantity())!0}"/>
          <p class="error"></p>
        </div>
        <div class="cell right expected">
          <label><@label "009"/></label>
          <#assign tmpdate><#if (product.warehouse())??><@h.timestamp ts=product.warehouse().expected() format="dd/MM/yyyy" /></#if></#assign>
          <input type="text" maxlength="100" value="${tmpdate}"/>
          <p class="error"></p>
        </div>
      </div>
      <div class="cell submit">
        <button><@label "btn.save"/></button>
      </div>
    </div>
  </@>

  <@htm.nodiv "quick-navigation-content">
    <a href="edit.htm?ref=${product.ref()}"><@label "003"/></a>
    <a href="main.htm"><@label "002"/></a>
  </@>

  <@htm.nodiv "page-ivars">
    { productId: "${product.id()}", ref: "${product.ref()}" }
  </@>

<#else>

  <@label "toyshop.4"/>

</#if>
