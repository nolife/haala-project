<#import "/web/common.ftl" as c >
<#import "/web/common-htm.ftl" as htm >
<#include "/web/macros.ftl:"+currentSite >
<#include "/web/toyshop-macros.ftl:"+currentSite >

<#if product??>

  <@htm.form id="page-form">
    <div class="form-panel">
      <div class="two-column">
        <div class="cell left currency">
          <label><@label "005"/></label>
          <select>
            <option value=""></option>
            <@h.listCurrencies ; cur >
              <option value="${cur}" <#if cur == (product.price().currency())! > selected="true" </#if> ><@label "cur.${cur}"/></option>
            </@>
          </select>
          <p class="error"></p>
        </div>
        <div class="cell left price">
          <label><@label "009"/></label>
          <input type="text" maxlength="100" value="<@toyshop.productPrice product=product output="nonZero" />"/>
          <p class="error"></p>
        </div>
        <div class="cell right dscprice">
          <label><@label "008"/></label>
          <input type="text" maxlength="100" value="<@toyshop.productPrice product=product mode="discount" output="nonZero" />"/>
          <p class="error"></p>
        </div>
      </div>
      <div class="tabs site-tabs">
        <@inputSiteTabs />
        <@h.listInputSites ; site >
          <div id="site-tab-${site}">
            <div class="cell note">
              <label><@label "007"/></label>
              <textarea><@toyshop.productText product=product key="pnot" site="."+site /></textarea>
              <p class="error"></p>
            </div>
          </div>
        </@>
      </div>
      <div class="cell submit">
        <button><@label "btn.save"/></button>
      </div>
    </div>
  </@>

  <@htm.nodiv "quick-navigation-content">
    <a href="edit.htm?ref=${product.ref()}"><@label "003"/></a>
    <a href="main.htm"><@label "002"/></a>
  </@>

  <@htm.nodiv "page-ivars">
    { productId: "${product.id()}", ref: "${product.ref()}" }
  </@>

<#else>

  <@label "toyshop.4"/>

</#if>
