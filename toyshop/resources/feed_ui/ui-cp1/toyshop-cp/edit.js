
(function($) {

  Haala.Misc.onDocReady(function() {
    initNavigation();
  });




  var initNavigation = function() {
    var ivars = Haala.Misc.evalJSON("#page-ivars");
    var msgFn = function(/*string*/ msg, /*json*/ tokens) {
      return Haala.Misc.messageMapping("#page-messages", msg, tokens);
    };

    $("#content .navigation-pane").kdxMenu();

    $("#context-navigation").kdxMenu({
      items: [
        { element: "#context-navigation .remove",
          gotoFn: function(/*json*/ item, /*json*/ model) {
            Haala.Messages.showYN(msgFn("product-delete"), "title-confirm", function() {
              Haala.AJAX.call("body", ToyshopProductF.remove, { productId: ivars.productId },
                  function(data) { document.location = "main.htm"; return Haala.AJAX.DO_NOTHING; });
            });
          }}
      ]
    });
  };

})(jQuery);
