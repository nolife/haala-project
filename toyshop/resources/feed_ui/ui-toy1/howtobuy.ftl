<#import "/web/common.ftl" as c >
<#import "/web/common-htm.ftl" as htm >
<#include "/web/macros.ftl:"+currentSite >

<p>
  This website merely advertizes our toys but lack purchase functionality. If you like a toy
  you see here and would like to purchase it then please visit our eBay store by clicking on
  the link in the right column.
  <br/><br/>

  Delivery is UK only. Shipping cost and return policy can be found in our eBay store.
</p>
