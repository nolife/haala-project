
(function($) {

  Haala.Misc.onDocReady(function() {
    initProductList();
  });




  var initProductList = function() {
    var listModel = {
      loadItemsFn: function(/*json*/ model, /*fn*/ callback) {
        Haala.AquaList.Integ.loadItemsFn(ToyshopProductSearchF.search, { target: 1 }, model, callback);
      },
      rowElementFn: function(/*json*/ vo, /*int*/ position, /*json*/ model) {
        var card = null;
        if (vo != null) {
          if (!vo.thumb) vo.thumb = "web/list-thumb.gif";

          card = $("#page-templates .data-item").clone();
          card.find("a").attr("href", "view/"+vo.ref);
          card.find("img").attr("src", "/"+vo.thumb);
          card.find(".name").html(vo.name);

          card.find(".price").hide();
          card.find(".dscprice").hide();

          if (vo.oldprice) {
            card.find(".dscprice").show();
            card.find(".dscprice .old").html(vo.oldprice);
            card.find(".dscprice .new").html(vo.price);
          } else {
            card.find(".price").show();
            card.find(".price .txt").html(vo.price);
          }

          card.aclick(function(event) {
            document.location = "view/"+vo.ref;
          });
        }
        return card;
      }
    };
    var list = $("#product-list .rows").aquaList(
      $.extend(true, {},
        Haala.AquaList.Integ.DefaultOptions,
        { rows: 999 },
        listModel));

  };

})(jQuery);

