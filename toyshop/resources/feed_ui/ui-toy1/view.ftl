<#import "/web/common.ftl" as c >
<#import "/web/common-htm.ftl" as htm >
<#include "/web/macros.ftl:"+currentSite >

<#if product??>

  <h1><@toyshop.productText product=product key="01view.title" output="smart" /></h1>

  <p class="icons">
    <a href="#" class="fav-add" title="Add this toy to favourites"></a>
    <a href="#" class="fav-remove" title="Remove this toy from favourites"></a>
  </p>

  <section class="about">
    <@toyshop.productText product=product key="desc" />
  </section>

  <div id="gallery-panel">
    <div class="list"></div>
    <div class="more">
      <button>Show more</button>
    </div>
  </div>
  <div class="nofloat"></div>

  <section class="madeof">
    This toy is made of
    <span><@toyshop.productCombineText product=product key="matr" group="material" /></span>
    and stuffed with
    <span><@toyshop.productCombineText product=product key="stuf" group="stuff" /></span>
  </section>

  <section class="price">
    Price is now
    <@toyshop.productPrice var="tmpdsc" product=product mode="discount" output="nonZero" />
    <#if tmpdsc != "" >
      <span>£ ${tmpdsc}</span>
      (oridinal price was <span>£ <@toyshop.productPrice product=product output="nonZero" /></span>)
    <#else>
      <span>£ <@toyshop.productPrice product=product output="nonZero" /></span>
    </#if>
  </section>

  <section class="stock">
    <#if product.status() == 2 >
      Sorry but <@toyshop.productText product=product key="name" /> is <span class="out">out of stock</span>
    <#else>
      <@toyshop.productText product=product key="name" /> is <span class="in">available in our eBay shop</span>
    </#if>
  </section>

  <@htm.nodiv "page-ivars">
    { productId: "${product.id()}", ref: "${product.ref()}" }
  </@>

  <@htm.nodiv "page-templates">

    <div class="face-item">
      <img src=""/>
      <p></p>
    </div>

  </@>

<#else>

  <@label "x.1"/>

</#if>
