
(function($) {

  Haala.Misc.onDocReady(function() {
    initGalleryPanel();
    initFavoutites();
  });




  var initGalleryPanel = function() {
    var ivars = Haala.Misc.evalJSON("#page-ivars");
    if (!ivars) return;

    var items = [];
    var addItem = function(/*int*/ n, /*json*/ vo) {
      var e = $("#page-templates .face-item").clone();
      e.addClass("layout"+vo.layout);
      e.find("img").attr("src", "/"+vo.picture.path);

      var site = Haala.Misc.systemVar("rootSite");
      $.each(vo.labels, function(z, v) {
        if (v.value && v.site == site) e.find("p").html(v.value);
      });

      e.addClass((n+1)%2 == 0 ? "even" : "odd");
      e.appendTo("#gallery-panel .list");
      items.push({ element: e });
    };

    var more = $("#gallery-panel .more").hide();
    var loadedItems = null, showItems = 4;

    Haala.AJAX.call("body", ToyshopProductF.faces, { productId: ivars.productId, sort: "pair" },
        function(data) {
          loadedItems = data.result;
          if (loadedItems.length) $("#gallery-panel").show();
          $.each(loadedItems, function(n, vo) {
            if (n < showItems) addItem(n, vo); //show rest on request
            else more.show();
          });
        });

    var moreReady = true;
    more.find("button").button().aclick(function() {
      if (moreReady) {
        moreReady = false; //prevent multiple clicks
        more.hide();
        $.each(loadedItems, function(n, vo) { if (n >= showItems) addItem(n, vo); });
      }
    });
  };


  var initFavoutites = function() {
    var ivars = Haala.Misc.evalJSON("#page-ivars");
    if (!ivars) return;

    var fbar = $("#favourites").favouriteBar();
    fbar.model.currentRef = ivars.ref;

    var ie = $("#content .icons").show();
    ie.find(".fav-add").hide().aclick(function() {
      fbar.addRef(ivars.ref);
    });
    ie.find(".fav-remove").hide().aclick(function() {
      fbar.removeRef(ivars.ref);
    });

    fbar.model.renderedFn = function(/*json*/ model) {
      ie.find(".fav-add").hide();
      ie.find(".fav-remove").hide();
      if (fbar.hasRef(ivars.ref)) ie.find(".fav-remove").show();
      else ie.find(".fav-add").show();
    }

  };


})(jQuery);

