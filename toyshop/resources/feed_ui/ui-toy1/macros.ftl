<#import "/web/common.ftl" as c >
<#import "/web/common-htm.ftl" as htm >

<#macro label key tokens="">
  <@c.label key=key tokens=tokens />
</#macro>

<#-- Print captch image -->
<#macro captcha>
  <img src="<@htm.url path='${turingPath}'/>"/>
</#macro>

<#-- AJAX loading sign -->
<#macro ajaxFrame>
  <div id="ajax-frame">
    <div></div>
    <p></p>
  </div>
</#macro>
