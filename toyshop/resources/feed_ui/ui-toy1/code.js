
var Haala = Haala || {};

(function($) {

  Haala.Code = {

    init: function() {

      if (Haala.MereCms) Haala.MereCms.Integ.init();
      Haala.Misc.tooltips();

      $("button.history-back").button().click(function() {
        history.back();
      });

      $(".logo").click(function() {
        document.location = "/";
      });

      $("#mainmenu").kdxMenu({
        items: [
          { element: "#mainmenu .home", highlightOnURL: ["/"] }
        ]
      });

      $("#favourites").favouriteBar({
        hint: "toyshop",
        cardElementFn: function(/*json*/ vo, /*json*/ model) {
          if (!vo.thumb) vo.thumb = "web/list-thumb.gif";
          var card =
              $("<div class='data'>" +
                  "  <a href='/view/"+vo.ref+"'><img src='/"+vo.thumb+"'/></a>" +
                  "  <p>"+vo.name+"</p>" +
                  "</div>");
          return card;
        }
      });

    }

  };

})(jQuery);
