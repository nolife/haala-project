<#import "/web/common.ftl" as c >
<#import "/web/common-htm.ftl" as htm >
<#include "/web/macros.ftl:"+currentSite >

<header>
  <div class="logo">
    <h1>Toy Shop</h1>
    <h2>Hand made toys stuffed with love</h2>
  </div>
</header>

<nav id="mainmenu">
  <a class="home" href="/">Home</a>
  <a class="How to buy" href="/howtobuy.htm">How to buy</a>
  <a class="contact" href="/contact.htm">Contact</a>
  <@ajaxFrame />
</nav>

<#-- wrapper start -->
<div id="wrapper">

  <aside id="right-column">
    <a class="ebay" href="#" title="Visit our eBay store"></a>

    <div id="favourites">
      <p class="title"><@label "x.31"/></p>
      <a href="#" class="add"><@label "x.32"/></a>
      <a href="#" class="remove"><@label "x.33"/></a>
      <div class="list"></div>
      <a href="#" class="remove-all"><@label "x.34"/></a>
    </div>

  </aside>

  <#-- content -->