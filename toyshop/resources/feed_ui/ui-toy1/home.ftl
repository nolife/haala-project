<#import "/web/common.ftl" as c >
<#import "/web/common-htm.ftl" as htm >
<#include "/web/macros.ftl:"+currentSite >

<div id="product-list">
  <div class="rows"></div>
  <div class="nofloat"></div>
</div>

<@htm.nodiv "page-templates">

  <div class="data-item">
    <div class="dscprice">
      <p class="txt">
        <span class="old"></span>
        <span class="new"></span>
      </p>
      <p class="arr"></p>
    </div>
    <div class="price">
      <p class="txt"></p>
      <p class="arr"></p>
    </div>
    <a href="#">
      <img src=""/>
      <p class="name"></p>
    </a>
  </div>

</@>
