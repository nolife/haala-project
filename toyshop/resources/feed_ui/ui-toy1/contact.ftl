<#import "/web/common.ftl" as c >
<#import "/web/common-htm.ftl" as htm >
<#include "/web/macros.ftl:"+currentSite>

<@htm.form id="page-form">
  <div class="form-panel">
    <div class="cell">
      <@label "005"/>
    </div>
    <div class="cell name">
      <label><@label "002"/></label>
      <@h.userFullName var="name" />
      <input type="text" value="${name!}" maxlength="100"/>
      <p class="error"></p>
    </div>
    <div class="cell email">
      <label><@label "003"/></label>
      <@h.user var="user" />
      <input type="text" value="${(user.email())!}" maxlength="100"/>
      <p class="error"></p>
    </div>
    <div class="cell text">
      <label>&nbsp;</label>
      <textarea></textarea>
      <p class="error"></p>
    </div>
    <div class="cell title">
      <@label "006"/>
    </div>
    <div class="cell turing">
      <@captcha />
      <input type="text" maxlength="10"/>
      <p class="error"></p>
    </div>
    <div class="cell submit">
      <button><@label "btn.send"/></button>
    </div>
  </div>
</@>

<@htm.nodiv "page-messages">
  <div data-key="dialog-title"><@label "008"/></div>
  <div data-key="dialog-text"><@label "007"/></div>
</@>
