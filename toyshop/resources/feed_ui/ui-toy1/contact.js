
(function($) {

  Haala.Misc.onDocReady(function() {
    initFeedbackForm();
  });


  var initFeedbackForm = function() {
    $("#page-form .submit button").button();
    var msgFn = function(/*string*/ msg, /*json*/ tokens) {
      return Haala.Misc.messageMapping("#page-messages", msg, tokens);
    };

    var panel = $("#page-form").kdxPanel(
      $.extend(true, {}, Haala.KdxPanel.Integ.DefaultOptions, {
        input: [
          { element: "#page-form .name input", key: "name",
            validate: { element: "#page-form .name .error", require: true }},
          { element: "#page-form .text textarea", key: "text",
            validate: { element: "#page-form .text .error", require: true }},
          { element: "#page-form .email input", key: "email",
            validate: { element: "#page-form .email .error", require: true, regex: ["email"] }},
          { element: "#page-form .turing input", key: "turing",
            validate: { element: "#page-form .turing .error",
                        userFn: Haala.InputValidator.Integ.turingUserFn }}
        ]
      }));

    var submitHandler = function() {
      if (panel.validate()) {
        var command = Haala.Misc.collectInput(panel.collect(),
          ["turing", "name", "email", "text"]);

        feedback(command);
      } else {
        $("#page-form .error:visible:first").toViewport();
      }
    };
    $("#page-form").submit(submitHandler);

    var feedback = function(/*json*/ command) {
      Haala.AJAX.call("body", MessageF.feedback, command,
        function() {
          Haala.Messages.showOK(msgFn("dialog-text"), msgFn("dialog-title"), function() {
            document.location = "/";
          });
        });
    };

  };

})(jQuery);

