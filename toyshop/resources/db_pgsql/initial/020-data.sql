------------------------------
-- Extension to generic 020 --
------------------------------

INSERT INTO DOMAINS(ID, DOMAIN, DOMAIN_SITE, TYPE, META)
  VALUES (nextval('DOMAINS_SEQ'), 'toys.EXAMPLE.ORG', 'toy', 'other', 'style=html5,layout-modern; resolve=site,currency');

insert into SETTINGS (id, key, value, site) values (nextval('SETTINGS_SEQ'), 'MainURL', 'http://toys.EXAMPLE.ORG', 'toy1');

INSERT INTO ROLES(ID, ROLE, DESCRIPTION) VALUES (nextval('ROLES_SEQ'), 'ROLE_TOYSHOP', 'Toyshop pack');
