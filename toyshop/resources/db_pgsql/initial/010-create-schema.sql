-----------------------------------------------------------
--   TABLES
-----------------------------------------------------------

CREATE TABLE TOYSHOP_toyshop_packs
(
  id bigint NOT NULL,
  CONSTRAINT TOYSHOP_toyshop_packs_pkey PRIMARY KEY (id)
)
WITHOUT OIDS;



CREATE TABLE TOYSHOP_products
(
  id bigint NOT NULL,
  ref character varying(100) NOT NULL,
  status integer NOT NULL,
  price_id bigint,
  pack_id bigint,
  warehouse_id bigint,
  CONSTRAINT TOYSHOP_products_pkey PRIMARY KEY (id)
)
WITHOUT OIDS;



CREATE TABLE TOYSHOP_product_domain_map
(
  product_id bigint NOT NULL,
  domain_id bigint NOT NULL
)
WITHOUT OIDS;


CREATE TABLE TOYSHOP_product_options
(
  id bigint NOT NULL,
  "type" integer NOT NULL,
  "value" text,
  product_id bigint,
  CONSTRAINT TOYSHOP_product_options_pkey PRIMARY KEY (id)
)
WITHOUT OIDS;



CREATE TABLE TOYSHOP_prices
(
  id bigint NOT NULL,
  currency character varying(100) NOT NULL,
  price bigint NOT NULL,
  price_dc bigint NOT NULL,
  dsc_price bigint,
  dsc_price_dc bigint,
  CONSTRAINT TOYSHOP_prices_pkey PRIMARY KEY (id)
)
WITHOUT OIDS;



CREATE TABLE TOYSHOP_warehouses
(
  id bigint NOT NULL,
  quantity integer NOT NULL,
  expected bigint,
  CONSTRAINT TOYSHOP_warehouses_pkey PRIMARY KEY (id)
)
WITHOUT OIDS;



CREATE TABLE TOYSHOP_product_picture_map
(
  product_id bigint NOT NULL,
  picture_id bigint NOT NULL
)
WITHOUT OIDS;














-----------------------------------------------------------
--   CONSTRAINTS
-----------------------------------------------------------


ALTER TABLE TOYSHOP_toyshop_packs
  ADD CONSTRAINT TOYSHOP_toyshop_pack_sub_fk FOREIGN KEY (id)
      REFERENCES packs (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION;



ALTER TABLE TOYSHOP_products
  ADD CONSTRAINT TOYSHOP_toyshop_pack_fk FOREIGN KEY (pack_id)
      REFERENCES TOYSHOP_toyshop_packs (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION;

ALTER TABLE TOYSHOP_products
  ADD CONSTRAINT TOYSHOP_products_ref_key UNIQUE(ref);

ALTER TABLE TOYSHOP_products
  ADD CONSTRAINT TOYSHOP_product_price_fk FOREIGN KEY (price_id)
      REFERENCES TOYSHOP_prices (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION;

ALTER TABLE TOYSHOP_products
  ADD CONSTRAINT TOYSHOP_product_warehouse_fk FOREIGN KEY (warehouse_id)
      REFERENCES TOYSHOP_warehouses (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION;



ALTER TABLE TOYSHOP_product_domain_map
  ADD CONSTRAINT TOYSHOP_domain_product_fk FOREIGN KEY (product_id)
      REFERENCES TOYSHOP_products (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION;

ALTER TABLE TOYSHOP_product_domain_map
  ADD CONSTRAINT TOYSHOP_product_domain_fk FOREIGN KEY (domain_id)
      REFERENCES domains (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION;



ALTER TABLE TOYSHOP_product_options
  ADD CONSTRAINT TOYSHOP_product_option_fk FOREIGN KEY (product_id)
      REFERENCES TOYSHOP_products (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION;



ALTER TABLE TOYSHOP_product_picture_map
  ADD CONSTRAINT TOYSHOP_picture_product_fk FOREIGN KEY (product_id)
      REFERENCES TOYSHOP_products (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION;

ALTER TABLE TOYSHOP_product_picture_map
  ADD CONSTRAINT TOYSHOP_product_picture_fk FOREIGN KEY (picture_id)
      REFERENCES pictures (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION;









-----------------------------------------------------------
--   INDEXES
-----------------------------------------------------------


CREATE INDEX "TOYSHOP_products_ind1"
  ON TOYSHOP_products (ref);










-----------------------------------------------------------
--   SEQUENCES
-----------------------------------------------------------


CREATE SEQUENCE TOYSHOP_products_seq
  INCREMENT 1
  MINVALUE 1
  MAXVALUE 9223372036854775807
  START 1
  CACHE 1;



CREATE SEQUENCE TOYSHOP_product_options_seq
  INCREMENT 1
  MINVALUE 1
  MAXVALUE 9223372036854775807
  START 1
  CACHE 1;



CREATE SEQUENCE TOYSHOP_prices_seq
  INCREMENT 1
  MINVALUE 1
  MAXVALUE 9223372036854775807
  START 1
  CACHE 1;



CREATE SEQUENCE TOYSHOP_warehouses_seq
  INCREMENT 1
  MINVALUE 1
  MAXVALUE 9223372036854775807
  START 1
  CACHE 1;
