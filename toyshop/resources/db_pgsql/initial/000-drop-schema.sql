-----------------------------------------------------------
--   DROP
-----------------------------------------------------------

  DROP TABLE TOYSHOP_product_picture_map CASCADE;

  DROP TABLE TOYSHOP_product_options CASCADE;
  DROP SEQUENCE TOYSHOP_product_options_seq;

  DROP TABLE TOYSHOP_warehouses CASCADE;
  DROP SEQUENCE TOYSHOP_warehouses_seq;

  DROP TABLE TOYSHOP_prices CASCADE;
  DROP SEQUENCE TOYSHOP_prices_seq;

  DROP TABLE TOYSHOP_products CASCADE;
  DROP SEQUENCE TOYSHOP_products_seq;

  DROP TABLE TOYSHOP_toyshop_packs CASCADE;

  DROP TABLE TOYSHOP_product_domain_map CASCADE;

  delete from packs where pack = 'toyshop';
