package com.coldcore.haala.web.facade.proxy.toyshop;

import com.coldcore.haala.web.facade.annotations.AopFacade;
import com.coldcore.haala.web.facade.annotations.TargetClass;
import com.coldcore.haala.web.facade.logic.toyshop.ProductFacade;
import com.coldcore.haala.web.facade.vo.cmd.toyshop.*;

import javax.servlet.http.HttpServletRequest;

/** AOP proxy delegate */
@AopFacade
public class ProductFacadeDlg {

  @TargetClass private ProductFacade toyshopProductFacade;

  public ProductFacade getToyshopProductFacade() { return toyshopProductFacade; }

  public void setToyshopProductFacade(ProductFacade toyshopProductFacade) { this.toyshopProductFacade = toyshopProductFacade; }

  /*** Stub methods. Invoked by 'InvokerAspect' directly on the 'target'. ***/
  public Object create(AddProductVO cmd, HttpServletRequest request) { return null; }
  public Object addFaces(AddFacesVO cmd, HttpServletRequest request) { return null; }
  public Object faces(FacesVO cmd, HttpServletRequest request) { return null; }
  public Object editFaces(EditFacesVO cmd, HttpServletRequest request) { return null; }
  public Object editDetails(EditDetailsVO cmd, HttpServletRequest request) { return null; }
  public Object editPrice(EditPriceVO cmd, HttpServletRequest request) { return null; }
  public Object editWarehouse(EditWarehouseVO cmd, HttpServletRequest request) { return null; }
  public Object editProps(EditPropsVO cmd, HttpServletRequest request) { return null; }
  public Object remove(RemoveProductVO cmd, HttpServletRequest request) { return null; }
}