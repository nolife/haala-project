package com.coldcore.haala.web.facade.proxy.toyshop;

import com.coldcore.haala.web.facade.annotations.AopFacade;
import com.coldcore.haala.web.facade.annotations.TargetClass;
import com.coldcore.haala.web.facade.logic.toyshop.ProductSearchFacade;
import com.coldcore.haala.web.facade.vo.cmd.toyshop.*;
import com.coldcore.haala.web.facade.vo.cmd.*;

import javax.servlet.http.HttpServletRequest;

/** AOP proxy delegate */
@AopFacade
public class ProductSearchFacadeDlg {

  @TargetClass private ProductSearchFacade toyshopProductSearchFacade;

  public ProductSearchFacade getToyshopProductSearchFacade() { return toyshopProductSearchFacade; }

  public void setToyshopProductSearchFacade(ProductSearchFacade toyshopProductSearchFacade) { this.toyshopProductSearchFacade = toyshopProductSearchFacade; }

  /*** Stub methods. Invoked by 'InvokerAspect' directly on the 'target'. ***/
  public Object search(SearchVO cmd, HttpServletRequest request) { return null; }
  public Object dsearch(SearchVO cmd, HttpServletRequest request) { return null; }
  public Object dfilter(FilterVO cmd, HttpServletRequest request) { return null; }
}