package com.coldcore.haala
package directive.freemarker.toyshop

import com.coldcore.haala.service.Dependencies
import com.coldcore.haala.directive.freemarker._
import com.coldcore.haala.domain.toyshop.{ProductOption, Price, Product}
import com.coldcore.haala.service.toyshop.{ProductWordService, ProductService}
import com.coldcore.haala.service.toyshop.Util._
import com.coldcore.misc.scala.StringOp._
import com.coldcore.haala.service.toyshop.ProductWordService.SmartTextIN

trait GetProduct extends GetOptionObject[ProductOption,Product] with Dependencies {
  def getProduct(state: State): Option[Product] = {
    import state._
    val pval = (x: String) => paramAsString(x).getOrElse("")
    val (id, ref) = (pval("productId"), pval("productRef"))

    paramAsObject[Product]("product").orElse { //product object as param
      if (id.nonEmpty) sc.get[ProductService].getById(id.toLong) //by ID
      else if (ref.nonEmpty) sc.get[ProductService].getByRef(ref) //by REF
      else None
    }
  }

  override def getObject(state: State) = getProduct(state)
}

trait ProductOptionsGroup extends OptionsGroup {
  import ProductOption._
  override def onGroup(group: String): List[Int] = group match {
    case "material" =>
      TYPE_MATERIAL_FABRIC :: TYPE_MATERIAL_RUBBER :: TYPE_MATERIAL_WOOD :: TYPE_MATERIAL_CLAY ::
        Nil
    case "stuff" =>
      TYPE_STUFF_WOOL :: TYPE_STUFF_COTTON ::
        Nil
    case _ => Nil
  }
}

class ProductDirective extends BaseDirective with GetProduct {
  override def execute(state: State) = getProduct(state).foreach(product => outputTM(state, state.wrap(product)))
}

class ProductTextDirective extends BaseDirective with StateWithVars with GetProduct {
  override def execute(state: StateWithVars) = getProduct(state).foreach { product =>
    import state._
    output(state, pval("output") match {
      case "smart" => sc.get[ProductWordService].smartText(product, SmartTextIN(site, pval("key")))
      case _ => label(packLabel("pr", product.ref, pval("key")), site).safe
    })
  }
}

class ProductPriceDirective extends BaseDirective with StateWithVars with GetProduct {
  override def execute(state: StateWithVars) = getProduct(state).foreach { product =>
    import state._
    val price = Option(product.price).getOrElse(new Price)
    val x =
      pval("mode") match {
        case "discount" => Option(price.dscPrice).map(_.toLong).getOrElse(0L)
        case _ => Option(price.price).map(_.toLong).getOrElse(0L)
      }
    output(state, pval("output") match {
      case "nonZero" => (x != 0) ? toX100(x) | ""
      case _ => toX100(x)
    })
  }
}

class ProductTextSpreadDirective extends BaseDirective with StateWithVars with GetProduct {
  override def execute(state: StateWithVars) = getProduct(state).foreach { product =>
    import state._
    val key = packLabel("pr", product.ref, pval("key"))
    val value = label(key, site).safe.split("\n").filter(""!=)
    outputTM(state, wrap(value))
  }
}

class ProductCombineTextDirective extends BaseDirective with StateWithVars with GetProduct with ProductOptionsGroup {
  override def execute(state: StateWithVars) = getProduct(state).foreach { product =>
    import state._
    val (key, group, sp) = (pval("key"), optionsGroup(state), pval("sp") or ", ")

    val s =
      product.options.filter(o => group.contains(o.`type`)).map { o =>
        val x = label(o.getValueKey+"b", site) or label(o.getValueKey, site).safe
        (if (x == "" || o.value.safe == "") x else parametrize(x, "val:"+o.value)) or o.getValueKey
      }.mkString(sp)

    val value =
      if (key == "") s
      else {
        val x = label(packLabel("pr", product.ref, pval("key")), site).safe.replace("\n", sp)
        if (x == "") s else s+(if (s == "") "" else sp)+x
      }
    output(state, value)
  }
}

trait NewProductOption {
  def newOption = new ProductOption
}

class ListProductOptionTypesDirective extends ListOptionTypesDirective with ProductOptionsGroup

class ProductOptionKeyDirective extends OptionKeyDirective[ProductOption] with NewProductOption

class ProductOptionValueDirective extends OptionValueDirective[ProductOption,Product] with GetProduct

class ProductHasOptionDirective extends HasOptionDirective[ProductOption,Product] with GetProduct

class ProductOptionTextDirective extends OptionTextDirective[ProductOption,Product] with GetProduct

class ProductOptionModeDirective extends OptionModeDirective[ProductOption] with NewProductOption
