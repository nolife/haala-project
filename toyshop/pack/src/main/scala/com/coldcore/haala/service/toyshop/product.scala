package com.coldcore.haala
package service
package toyshop

import org.springframework.transaction.annotation.Transactional

import scala.beans.BeanProperty
import dao.GenericDAO
import domain.toyshop._
import domain.{File, Label, Picture}
import com.coldcore.misc5.IdGenerator._
import com.coldcore.misc.scala.StringOp._
import core.{CoreException, SiteChain, Timestamp, VirtualPath}
import core.toyshop.Params
import com.google.gson.Gson
import service.exceptions.{LimitExceededException, ObjectNotFoundException}
import service.toyshop.Util._
import service.SolrService._
import service.FileService.Body

import scala.collection.JavaConverters._
import core.toyshop.PictureConstants._

object ProductService {
  case class OptionIN(`type`: String, value: String)
  case class CreateIN()
  case class UpdateInfoIN(notes: List[SiteableText], options: List[OptionIN])
  case class UpdatePriceIN(currency: String, price: String, dscPrice: String, notes: List[SiteableText])
  case class UpdateWarehouseIN(status: String, quantity: String, expected: String)
  case class UpdatePropsIN(status: String, domains: List[String])
  case class UpdateListIN(notes: List[SiteableText])
  case class FindFacesIN(sort: String)
  case class UpdateFacesIN(faces: List[FaceIN])
  case class FaceIN(id: String, deleted: String, position: String, text: SiteableText)

  case class FaceOUT(layout: Int, picture: PictureOUT, labels: List[LabelOUT]) {
    def serializeAsMap: Map[Symbol,Any] =
      Map('layout -> layout, 'picture -> picture.serializeAsMap, 'labels -> labels.map(_.serializeAsMap))
  }
  case class PictureOUT(id: Long, path: VirtualPath, layout: Int) {
    def serializeAsMap: Map[Symbol,Any] = Map('id -> id, 'path -> path.path, 'layout -> layout)
  }
  case class LabelOUT(id: Long, key: String, value: String, site: String) {
    def serializeAsMap: Map[Symbol,Any] = Map('id -> id, 'key -> key, 'value -> value, 'site -> site)
  }
}

trait ProductService {
  import ProductService._

  def getById(id: Long): Option[Product]
  def getByRef(ref: String): Option[Product]
  def delete(id: Long)
  def create(userId: Long, in: CreateIN): Product
  def updateInfo(id: Long, in: UpdateInfoIN): Product
  def updatePrice(id: Long, in: UpdatePriceIN): Product
  def updateWarehouse(id: Long, in: UpdateWarehouseIN): Product
  def updateProps(id: Long, in: UpdatePropsIN): Product
  def findFaces(id: Long, in: FindFacesIN): List[FaceOUT]
  def createFaces(id: Long, bodies: Seq[Body])
  def updateFaces(id: Long, in: UpdateFacesIN)
  def createListThumb(id: Long, body: Body)
  def listSites(o: Product): List[SiteChain]
  def rescalePictures(id: Long)
  def prepareSolrDoc(o: Product): SolrDoc
  def doSolrSubmit(o: Product)
  def doSolrDelete(o: Product)
}

@Transactional
class ProductServiceImpl extends BaseService with ProductService {
  import ProductService._

  @BeanProperty var toyshopProductDao: GenericDAO[Product] = _

  private def productType = packProductType(sc)

  /** Prepare Solr doc. */
  override def prepareSolrDoc(o: Product): SolrDoc = {
    val terms =
      List(
        ObjectIdTerm(o.id, ObjectTypeTerm(productType)),
        packProductTerm("ref", o.ref),
        packProductTerm("sta", o.status),
        packProductTerm("usr", o.pack.user.id)) :::
      //domains
      (List.empty[Term] /: o.domains)((a,b) => packProductTerm("dom", b.id) :: a) :::
      //options
      (List.empty[Term] /: o.options)((a,b) => packProductTerm("opt", b.`type`) :: a)
    SolrDoc(terms: _*)
  }

  /** Submit an object to Solr. */
  @Transactional (readOnly = true)
  override def doSolrSubmit(o: Product) =
    sc.get[SolrService].submitWT(prepareSolrDoc(o))

  /** Delete an object from Solr. */
  @Transactional (readOnly = true)
  override def doSolrDelete(o: Product) =
    sc.get[SolrService].deleteByIdsWT(ObjectIdTerm(o.id, ObjectTypeTerm(productType)))

  /** Get an object by its ID. */
  @Transactional (readOnly = true)
  override def getById(id: Long): Option[Product] =
    toyshopProductDao.findById(id)

  /** Get an object by a REF. */
  @Transactional (readOnly = true)
  override def getByRef(ref: String): Option[Product] =
    toyshopProductDao.findUniqueByProperty("ref", ref)

  /** Generate a unique ref. */
  private def generateUniqueRef: String =
    Iterator.fill(10000)(generate(8,8).toUpperCase).find {
      case ref => ref.matches("\\D+") && !getByRef(ref).isDefined //nondigit
    }.getOrElse(throw new CoreException("Ref generator exhausted"))

  private def merge(o: ProductOption, in: OptionIN) {
    o.`type` = in.`type`.toInt
    o.value = in.value.safe //nullable field
  }

  private def outFace(p: Picture, list: List[Label]): FaceOUT =
    FaceOUT(p.getLayout, outPicture(p), list.map(outLabel).toList)

  private def outPicture(o: Picture): PictureOUT =
    PictureOUT(o.id, VirtualPath(o.file.getPath), o.getLayout)

  private def outLabel(o: Label): LabelOUT =
    LabelOUT(o.id, o.key, o.value.convert("html"), o.site)

  /** Create a product. */
  @Transactional (readOnly = false)
  override def create(userId: Long, in: CreateIN): Product = {
    val user = sc.get[UserService].getById(userId).getOrElse(throw new ObjectNotFoundException)

    //test that max inactive products limit is not exceeded
    val params = Option(new Gson().fromJson(sc.get[SettingService].querySystem("ToyshopParams").safe, classOf[Params])).get
    val count = user.toyshopPack.products.count(_.status == Product.STATUS_NOT_ACTIVE)
    val limit = params.product.parseCSVMap.getOrElse("inactiveLimit", "").toInt
    if (count >= limit) throw new LimitExceededException

    val o = new Product
    o.pack = user.toyshopPack
    o.ref = generateUniqueRef
    o.domains.addAll(user.domains)

    o.pack.products.add(o)
    toyshopProductDao.saveOrUpdate(o)
    doSolrSubmit(o) //update Solr
    o
  }

  /** Delete a product. */
  @Transactional (readOnly = false)
  override def delete(id: Long) = getById(id).foreach { o =>
    o.pictures.foreach(x => sc.get[service.PictureService].delete(x.id))
    o.pictures.clear
    labelService.deleteByKeyLike(packLabel("pr", o.ref, "%"))
    sc.get[FileService].deleteByFolder(VirtualPath(packPath("pr", o.ref), "*"))
    o.pack.products.remove(o)
    toyshopProductDao.delete(o)
    doSolrDelete(o) //update Solr
  }

  private def assertProduct(id: Long): Product = getById(id).getOrElse(throw new ObjectNotFoundException)

  /** Update product's info. */
  @Transactional (readOnly = false)
  override def updateInfo(id: Long, in: UpdateInfoIN): Product = {
    val o = assertProduct(id)
    writeLabels((k: String) => packLabel("pr", o.ref, k), in.notes: _*)
    expandOrShrink(o.options, in.options.size, () => new ProductOption { product = o })
    for ((op, voop) <- o.options zip in.options) merge(op, voop)
    toyshopProductDao.saveOrUpdate(o)
    doSolrSubmit(o) //update Solr
    o
  }

  /** Update product's price. */
  @Transactional (readOnly = false)
  override def updatePrice(id: Long, in: UpdatePriceIN): Product = {
    val o = assertProduct(id)
    if (o.price == null) o.price = new Price
    o.price.currency = in.currency
    o.price.price = toX100(in.price)
    o.price.dscPrice = toX100(in.dscPrice)
    writeLabels((_) => packLabel("pr", o.ref, "pnot"), in.notes: _*)
    toyshopProductDao.saveOrUpdate(o)
    doSolrSubmit(o) //update Solr
    o
  }

  /** Update product's warehouse. */
  @Transactional (readOnly = false)
  override def updateWarehouse(id: Long, in: UpdateWarehouseIN): Product = {
    val o = assertProduct(id)
    if (o.warehouse == null) o.warehouse = new Warehouse
    o.warehouse.quantity = in.quantity.toInt
    o.status = in.status.toInt
    o.warehouse.expected = if (in.expected.safe != "") Timestamp(in.expected, "dd/MM/yyyy").midnight else null

    toyshopProductDao.saveOrUpdate(o)
    doSolrSubmit(o) //update Solr
    o
  }

  /** Update product's properties. */
  @Transactional (readOnly = false)
  override def updateProps(id: Long, in: UpdatePropsIN): Product = {
    val o = assertProduct(id)
    o.status = in.status.toInt
    val domains = in.domains.map(x => sc.get[DomainService].getById(x.toLong).get)
    o.domains.clear
    o.domains.addAll(domains.asJava)
    toyshopProductDao.saveOrUpdate(o)
    doSolrSubmit(o) //update Solr
    o
  }

  /** Find product's faces. */
  @Transactional (readOnly = true)
  override def findFaces(id: Long, in: FindFacesIN): List[FaceOUT] = {
    val o = assertProduct(id)
    val sorted = o.pictures.filter(_.`type` == TYPE_PRODUCT_FACE).sortBy(_.position).toList //sort by position
    val fls = if (in.sort != "pair") sorted
      else { //re-sort by pairs
      val (a, b) = sorted.partition(_.getLayout != Picture.LAYOUT_PANORAMA)
        a ::: b
      }
    fls.map(p => outFace(p, labels(packLabel("pr", o.ref, "p", p.id.hex)))).toList
  }

  /** Create product's faces (pictures) from a collection of file bodies. */
  @Transactional (readOnly = false)
  override def createFaces(id: Long, bodies: Seq[Body]) {
    val o = assertProduct(id)

    //check if faces limit is not exceeded
    val params = Option(new Gson().fromJson(sc.get[SettingService].querySystem("ToyshopParams").safe, classOf[Params])).get
    val limit = params.product.parseCSVMap.getOrElse("facesLimit", "").toInt
    if (findFaces(id, FindFacesIN("")).size+bodies.size > limit) throw new LimitExceededException

    //create a thumb from the first face if does not exist and create faces
    if (!o.pictures.exists(_.`type` == TYPE_PRODUCT_LIST_THUMB) && bodies.nonEmpty)
      o.pictures.add(sc.get[PictureService].createListThumb(o, sc.get[FileService].copy(bodies.head)))
    bodies.zipWithIndex.map { case (b,n) => o.pictures.add(sc.get[PictureService].createFace(o, b, n+1)) }
    toyshopProductDao.saveOrUpdate(o)
  }

  /** Update product's faces info. */
  @Transactional (readOnly = false)
  override def updateFaces(id: Long, in: UpdateFacesIN) {
    val o = assertProduct(id)
    val delete =
      (for { p <- o.pictures; fvo <- in.faces
             if fvo.id.toLong == p.id && p.`type` == TYPE_PRODUCT_FACE } yield //proceed only on face picture with matching ID
        if (fvo.deleted.isTrue) Some(p) else { //delete of a face requested
          writeLabels((_) => packLabel("pr", o.ref, "p", p.id.hex), fvo.text)
          p.position = fvo.position.toInt
          None //do not add to delete
        }).flatten

    //delete scheduled pictures
    for (p <- delete) {
      labelService.deleteByKeyLike("pr."+o.ref+".p."+p.id.hex)
      sc.get[service.PictureService].delete(p.id)
      o.pictures.remove(p)
    }
    toyshopProductDao.saveOrUpdate(o)
  }

  /** Create a new listing thumb from a body (or from the first face). */
  @Transactional (readOnly = false)
  override def createListThumb(id: Long, body: Body) {
    val o = assertProduct(id)
    val b = Option(body).getOrElse {
      o.pictures.find(x => x.`type` == TYPE_PRODUCT_FACE && x.position == 1).map(p =>
        sc.get[FileService].copyBody(p.file, File.SUFFIX_ORIGINAL)).orNull
    }
    if (b != null) {
      o.pictures.find(_.`type` == TYPE_PRODUCT_LIST_THUMB).foreach { x =>
        sc.get[service.PictureService].delete(x.id)
        o.pictures.remove(x)
      }
      o.pictures.add(sc.get[PictureService].createListThumb(o, b))
      toyshopProductDao.saveOrUpdate(o)
    }
  }

  /** Return a list of product site chains based on its domains (xx1.pp1.wis1 ... xx1.mh1). */
  @Transactional (readOnly = true)
  override def listSites(o: Product): List[SiteChain] =
    o.domains.map(x => sc.get[DomainService].defaultSite(x.domain)).toList

  /** Re-scale all pictures with new measurements and watermarks. */
  @Transactional (readOnly = false)
  override def rescalePictures(id: Long) =
    assertProduct(id).pictures.map(sc.get[PictureService].updatePicture)

}