package com.coldcore.haala
package domain
package toyshop

import beans.BeanProperty
import annotations.EqualsHashCode
import java.util.Date

object ToyshopPack {
  val PACK = "toyshop"
}

class ToyshopPack extends Pack {
  import ToyshopPack._
  pack = PACK
  @BeanProperty var products: JList[Product] = new JArrayList[Product]
}

object Product {
  val STATUS_NOT_ACTIVE = 0
  val STATUS_IN_STOCK = 1
  val STATUS_SOLD_OUT = 2
}

class Product extends BaseObject with BasePackObject[ToyshopPack] with BaseOptionObject[ProductOption] {
  import Product._
  @BeanProperty @EqualsHashCode var ref: String = _
  @BeanProperty @EqualsHashCode var status: JInteger = STATUS_NOT_ACTIVE
  @BeanProperty var price: Price = _
  @BeanProperty var warehouse: Warehouse = _
  @BeanProperty var domains: JList[Domain] = new JArrayList[Domain]
  @BeanProperty var pictures: JList[Picture] = new JArrayList[Picture]
}

class ProductOption extends BaseOption("toyshopPrOpt") {
  @BeanProperty var product: Product = _

  override def getMode(typ: Int): Int = typ match {
    case x if 101 to 200 contains x => MODE_INT_VALUE
    case x if 201 to 300 contains x => MODE_STRING_VALUE
    case _ => MODE_SIMPLE
  }
}

object ProductOption {
  //assorted simple types 1..100
  val TYPE_MATERIAL_FABRIC = 1
  val TYPE_MATERIAL_RUBBER = 2
  val TYPE_MATERIAL_WOOD = 3
  val TYPE_MATERIAL_CLAY = 4
  val TYPE_STUFF_WOOL = 5
  val TYPE_STUFF_COTTON = 6

  //assorted Int types 101..200
  val TYPE_WEIGHT = 101
  val TYPE_HEIGHT = 102
  val TYPE_WIDTH = 103
  val TYPE_DEPTH = 104

  //assorted String types 201..300
}

class Price extends BaseObject {
  @BeanProperty @EqualsHashCode var currency: String = _
  @BeanProperty @EqualsHashCode var price: JLong = _ //x100
  @BeanProperty @EqualsHashCode var priceDC: JLong = 0 //x100 (converted into default currency)
  @BeanProperty @EqualsHashCode var dscPrice: JLong = _ //x100
  @BeanProperty @EqualsHashCode var dscPriceDC: JLong = 0 //x100 (converted into default currency)
}

class Warehouse extends BaseObject {
  @BeanProperty @EqualsHashCode var quantity: JInteger = _
  @BeanProperty @EqualsHashCode var expected: JLong = _
}
