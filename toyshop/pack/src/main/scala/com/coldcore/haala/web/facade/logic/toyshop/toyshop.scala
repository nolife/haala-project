package com.coldcore.haala
package web
package facade.logic
package toyshop

import beans.BeanProperty
import security.annotations.SecuredRole
import domain.toyshop.{Product, ToyshopPack}
import javax.servlet.http.HttpServletRequest
import core.WebConstants
import com.coldcore.haala.core.NamedLog
import com.coldcore.haala.core.toyshop.PictureConstants._
import service.toyshop.ProductService._
import service.toyshop.{ProductSearchService, ProductService, ProductWordService}
import service.toyshop.ProductSearchService.{ByDomainSearchInput, ByUserSearchInput, FavsSearchInput}
import service.toyshop.ProductService.FindFacesIN
import service.toyshop.ProductService.CreateIN
import service.toyshop.ProductService.FaceIN
import service.toyshop.ProductService.UpdateFacesIN
import service.toyshop.Util._
import service.toyshop.ProductWordService.PriceLabelIN
import service.exceptions._
import service.SearchInput
import web.facade.annotations.{ErrorCR, ErrorCRs, LogClass, ValidatorClass}
import web.facade.validator.Validator
import web.facade.vo.CallResult
import web.facade.vo.cmd.toyshop._
import web.facade.vo.cmd.{FilterVO, SearchVO}
import web.facade.logic.BaseFacade.SortMapping
import web.facade.logic.BaseFacade.TargetedSearch
import web.facade.logic.BaseFacade.TargetedFilter
import web.core.SearchHelper

/** Convert input and output data */
trait ProductConvert {
  self: ProductFacade =>

  implicit def addVOtoIN(cmd: AddProductVO) =
    CreateIN()

  implicit def facesVOtoIN(cmd: FacesVO) =
    FindFacesIN(cmd.sort)

  implicit def editFacesVOtoIN(cmd: EditFacesVO) =
    UpdateFacesIN(
      for (x <- cmd.faces.toList) yield
        FaceIN(x.id, x.deleted, x.position, siteableText(x, 'text :: Nil)))

  implicit def editDetailsVOtoIN(cmd: EditDetailsVO) =
    UpdateInfoIN(listSiteableText(cmd.details), cmd.options.map(x => OptionIN(x.`type`, x.value)).toList)

  implicit def editPriceVOtoIN(cmd: EditPriceVO) =
    UpdatePriceIN(cmd.currency, cmd.price, cmd.dscPrice, listSiteableText(cmd.notes))

  implicit def editWarehouseVOtoIN(cmd: EditWarehouseVO) =
    UpdateWarehouseIN(cmd.status, cmd.quantity, cmd.expected)

  implicit def editPropsVOtoIN(cmd: EditPropsVO) =
    UpdatePropsIN(cmd.status, cmd.domains.safe.parseCSV)
}

class ProductFacade extends BaseFacade with ProductConvert {
  @BeanProperty @LogClass lazy val LOG = new NamedLog("toyshop.product-facade", log)

  @BeanProperty @ValidatorClass var toyshopProductValidator: Validator = _

  @SecuredRole("TOYSHOP")
  @ErrorCR(on = classOf[LimitExceededException], key = "fcall.limit", log = "Products limit reached for user {currentUsername}")
  def create(cmd: AddProductVO, request: HttpServletRequest): CallResult = {
    val o = sc.get[ProductService].create(request.xuser.currentId, cmd)
    LOG.info(s"User ${request.xuser.currentUsername} created product #${o.id}")
    dataCR(o.ref)
  }

  @SecuredRole("TOYSHOP")
  @ErrorCRs(Array(
    new ErrorCR(on = classOf[LimitExceededException], key = "fcall.limit", log = "Faces limit reached for product #{productId}"),
    new ErrorCR(on = classOf[InvalidFormatException], key = "fcall.uppic", log = "Invalid pictures uploaded for product #{productId}")
  ))
  def addFaces(cmd: AddFacesVO, request: HttpServletRequest): CallResult =
    uploadedBodies(request) match {
      case files if files.nonEmpty =>
        sc.get[ProductService].createFaces(cmd.productId.toLong, files)
        LOG.info("User "+request.xuser.currentUsername+" updated product #"+cmd.productId)
        dataCR()
      case _ => errorCR(WebConstants.FACADE_ERROR_NO_UPLOAD)
    }

  @SecuredRole("ANONYMOUS")
  @ErrorCR(on = classOf[ObjectNotFoundException], key = "fcall.nofnd", log = "Product #{productId} not found")
  def faces(cmd: FacesVO, request: HttpServletRequest): CallResult = {
    val r = sc.get[ProductService].findFaces(cmd.productId.toLong, cmd)
    dataCR(r.map(_.serializeAsMap))
  }

  @SecuredRole("TOYSHOP")
  @ErrorCR(on = classOf[ObjectNotFoundException], key = "fcall.nofnd", log = "Product #{productId} not found")
  def editFaces(cmd: EditFacesVO, request: HttpServletRequest): CallResult = {
    sc.get[ProductService].updateFaces(cmd.productId.toLong, cmd)
    sc.get[ProductService].createListThumb(cmd.productId.toLong, null) //recreate thumb
    LOG.info("User "+request.xuser.currentUsername+" updated product #"+cmd.productId)
    dataCR()
  }

  @SecuredRole("TOYSHOP")
  @ErrorCR(on = classOf[ObjectNotFoundException], key = "fcall.nofnd", log = "Product #{productId} not found")
  def editDetails(cmd: EditDetailsVO, request: HttpServletRequest): CallResult = {
    sc.get[ProductService].updateInfo(cmd.productId.toLong, cmd)
    LOG.info("User "+request.xuser.currentUsername+" updated product #"+cmd.productId)
    dataCR()
  }

  @SecuredRole("TOYSHOP")
  @ErrorCR(on = classOf[ObjectNotFoundException], key = "fcall.nofnd", log = "Product #{productId} not found")
  def editPrice(cmd: EditPriceVO, request: HttpServletRequest): CallResult = {
    sc.get[ProductService].updatePrice(cmd.productId.toLong, cmd)
    LOG.info("User "+request.xuser.currentUsername+" updated product #"+cmd.productId)
    dataCR()
  }

  @SecuredRole("TOYSHOP")
  @ErrorCR(on = classOf[ObjectNotFoundException], key = "fcall.nofnd", log = "Product #{productId} not found")
  def editWarehouse(cmd: EditWarehouseVO, request: HttpServletRequest): CallResult = {
    sc.get[ProductService].updateWarehouse(cmd.productId.toLong, cmd)
    LOG.info("User "+request.xuser.currentUsername+" updated product #"+cmd.productId)
    dataCR()
  }

  @SecuredRole("TOYSHOP")
  @ErrorCR(on = classOf[ObjectNotFoundException], key = "fcall.nofnd", log = "Product #{productId} not found")
  def editProps(cmd: EditPropsVO, request: HttpServletRequest): CallResult = {
    sc.get[ProductService].updateProps(cmd.productId.toLong, cmd)
    LOG.info("User "+request.xuser.currentUsername+" updated product #"+cmd.productId)
    dataCR()
  }

  @SecuredRole("TOYSHOP")
  def remove(cmd: RemoveProductVO, request: HttpServletRequest): CallResult = {
    sc.get[ProductService].delete(cmd.productId.toLong)
    LOG.info("User "+request.xuser.currentUsername+" removed product #"+cmd.productId)
    dataCR()
  }

}

/** Convert input and output data */
trait ProductSearchConvert {
  self: ProductSearchFacade =>

  val itemVO = (request: HttpServletRequest) => (o: Product) => {
    val thumb = o.pictures.filter(_.`type` == TYPE_PRODUCT_LIST_THUMB) match {
      case Seq(p, _*) => p.file.getPath
      case _ => ""
    }
    val site = request.xsite.current
    val name = label(packLabel("pr", o.ref, "name"), site)
    val price = sc.get[ProductWordService].priceLabel(o, PriceLabelIN(site))
    val oldPrice = sc.get[ProductWordService].priceLabel(o, PriceLabelIN(site, "old"))

    Map('id -> o.id, 'ref -> o.ref, 'thumb -> thumb, 'name -> name, 'status -> o.status,
      'price -> price, 'oldprice -> oldPrice)
  }
}

class ProductSearchFacade extends BaseFacade with ProductSearchConvert {
  @BeanProperty @ValidatorClass var baseValidator: Validator = _

  private val searchOwned = (si: SearchInput, sh: SearchHelper) => {
    val input = new ByUserSearchInput(si) {
      override val filterValue = sh.filter(0).toLowerCase
    }
    sc.get[ProductSearchService].searchByUser(sh.request.xuser.currentId, input).serializeAsMap(itemVO(sh.request))
  }

  private val searchByDomain = (si: SearchInput, sh: SearchHelper) => {
    val input = new ByDomainSearchInput(si) {
      override val domainId = sh.request.xsite.state.currentDomainId
    }
    sc.get[ProductSearchService].searchByDomain(input).serializeAsMap(itemVO(sh.request))
  }

  @SecuredRole("ANONYMOUS")
  def search(cmd: SearchVO, request: HttpServletRequest): CallResult =
    genericSearch(cmd, request,
      TargetedSearch(1, "toyshopProducts", "toyshopProducts", orderBy(cmd), searchByDomain))

  @SecuredRole("TOYSHOP")
  def dsearch(cmd: SearchVO, request: HttpServletRequest): CallResult =
    genericSearch(cmd, request,
      TargetedSearch(2, "toyshopMyProducts", "toyshopMyProducts",
        orderBy(cmd, SortMapping("ref")),
        searchOwned))

  @SecuredRole("TOYSHOP")
  def dfilter(cmd: FilterVO, request: HttpServletRequest): CallResult = {
    genericSearchFilter(cmd, request, TargetedFilter(1, "toyshopMyProducts"))
    dataCR()
  }

  /** Return param. */
  private def param(index: Int, params: List[String]): String =
    if (index < params.size) params(index).safe.trim else ""

  private val searchFavs = (si: SearchInput, sh: SearchHelper) =>
    //params: refs (819746,345455,768576,FDRE32SE or empty)
    withCachedResult(si, sh) { //use cached results or search and cache new results
      sc.get[ProductSearchService].searchFavs(new FavsSearchInput(si) {
        override val refs = sh.param(0).parseCSV
      }).serializeAsMap(itemVO(sh.request))
    }

  /** Called from FavsMod */
  def favsSearch(cmd: SearchVO, request: HttpServletRequest): CallResult =
    if (cmd.hint != ToyshopPack.PACK) null
    else genericSearch(cmd, request,
      TargetedSearch(0, "favs", "toyshopProductFavs", orderBy(cmd), searchFavs))
}

class FavsMod extends MiscFacade.FavsMod {
  @BeanProperty var toyshopProductSearchFacade: ProductSearchFacade = _

  override def searchFavs(cmd: SearchVO, request: HttpServletRequest): CallResult =
    toyshopProductSearchFacade.favsSearch(cmd, request)
}
