package com.coldcore.haala
package service

package toyshop {
  package test {

    class MyMock extends com.coldcore.haala.service.test.MyMock {
      val productService = mock[ProductService]

      serviceContainer.services =
        productService :: serviceContainer.services
    }

  }
}