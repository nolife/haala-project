package com.coldcore.haala
package core
package toyshop

import scala.beans.BeanProperty

/** JSON setting ToyshopPictureScale */
class PictureScale {
  @BeanProperty var product: Category = _

  class Category {
    @BeanProperty var face: Scale = _
    @BeanProperty var face2: Scale = _
    @BeanProperty var list: Scale = _
    @BeanProperty var list2: Scale = _
  }

  class Scale {
    @BeanProperty var quality: String = _
    @BeanProperty var size: Array[String] = _
  }
}

/** JSON setting ToyshopParams */
class Params {
  @BeanProperty var product: String = _
}

object PictureConstants {
  val TYPE_PRODUCT_FACE = 1
  val TYPE_PRODUCT_LIST_THUMB = 2
}
