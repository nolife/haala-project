package com.coldcore.haala
package service

import domain.toyshop.{ToyshopPack, Product}
import domain.{Pack, User}

package object toyshop {
  implicit class ToyshopUser(user: User) {
    def toyshopPack = user.getPack[ToyshopPack](ToyshopPack.PACK)

    /** Method for traversing in JSP
     *  user.toyshopPack.products
     */
    def getToyshopPack = toyshopPack

    /** Method for passing regular user into JSP tags or traversing it
     *  user="${owner.unwrap}"   user.unwrap.address
     */
    def getUnwrap = user
  }
}
