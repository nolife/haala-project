package com.coldcore.haala
package service
package toyshop

import com.coldcore.haala.domain.toyshop.ToyshopPack
import com.coldcore.haala.service.SolrService.{Term, ValueTerm}

object Util {
  def packLabel(tokens: String*): String = ToyshopPack.PACK+"."+tokens.mkString(".")
  def packPath(tokens: String*): String = ToyshopPack.PACK+"/"+tokens.mkString("/")
  def packProductType(sc: ServiceContainer): Int = sc.get[SettingService].queryConstant[Int]("Constants", ToyshopPack.PACK, "SOLR_TYPE_PRODUCT")
  def packProductPrefix: String = "toyshop_pr"
  def packProductTerm(key: String, value: Any): Term = ValueTerm(packProductPrefix+"_"+key, value)
  def installed(sc: ServiceContainer): Boolean = sc.get[SettingService].querySystem("InstalledPacks").safe.parseCSV.contains(ToyshopPack.PACK)
}