package com.coldcore.haala.web.facade.vo.cmd
package toyshop

import com.coldcore.haala.web.facade.annotations.Input
import scala.beans.BeanProperty

class AddProductVO {

}

class RemoveProductVO {
  @BeanProperty @Input (datatype = "owner") var productId: String = _
}

class AddFacesVO {
  @BeanProperty @Input (datatype = "owner") var productId: String = _
}

class EditFacesVO {
  @BeanProperty @Input (datatype = "owner") var productId: String = _
  @BeanProperty @Input var faces: Array[FaceVO] = _
}

class FacesVO {
  @BeanProperty var productId: String = _
  @BeanProperty var sort: String = _
}

class FaceVO {
  @BeanProperty @Input (datatype = "sites") var site: String = _

  @BeanProperty @Input var text: String = _

  @BeanProperty var id: String = _
  @BeanProperty var deleted: String = _
  @BeanProperty var position: String = _
}

class EditDetailsVO {
  @BeanProperty @Input (datatype = "owner") var productId: String = _
  @BeanProperty @Input var details: Array[DetailsVO] = _
  @BeanProperty @Input var options: Array[OptionVO] = _
}

class DetailsVO {
  @BeanProperty @Input (datatype = "sites") var site: String = _

  @BeanProperty @Input var name: String = _
  @BeanProperty @Input var desc: String = _
  @BeanProperty @Input var sumr: String = _
  @BeanProperty @Input var stuf: String = _
  @BeanProperty @Input var matr: String = _
}

class OptionVO {
  @BeanProperty var `type`: String = _
  @BeanProperty @Input var value: String = _
}

class EditPriceVO {
  @BeanProperty @Input (datatype = "owner") var productId: String = _
  @BeanProperty @Input (required = true, transform = "upper") var currency: String = _
  @BeanProperty @Input (required = true, datatype = "float") var price: String = _
  @BeanProperty @Input (datatype = "float") var dscPrice: String = _
  @BeanProperty @Input var notes: Array[PriceNoteVO] = _
}

class PriceNoteVO {
  @BeanProperty @Input (datatype = "sites") var site: String = _
  @BeanProperty @Input var note: String = _
}

class EditWarehouseVO {
  @BeanProperty @Input (datatype = "owner") var productId: String = _
  @BeanProperty @Input (required = true) var status: String = _
  @BeanProperty @Input (required = true, datatype = "number") var quantity: String = _
  @BeanProperty @Input (datatype = "date") var expected: String = _
}

class EditPropsVO {
  @BeanProperty @Input (datatype = "owner") var productId: String = _
  @BeanProperty @Input (required = true) var status: String = _
  @BeanProperty @Input (datatype = "owner") var domains: String = _
}
