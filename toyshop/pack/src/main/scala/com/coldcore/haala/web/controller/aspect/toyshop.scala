package com.coldcore.haala
package web
package controller.aspect
package toyshop

import javax.servlet.http.HttpServletRequest
import domain.toyshop.Product
import core.Dependencies
import service.toyshop.ProductWordService
import service.toyshop.ProductWordService.SmartTextIN

class PageMod extends PageAspect.Mod with Dependencies {
  import PageAspect._

  override def createRecentTitle(request: HttpServletRequest): String =
    request.xattr.get[Product]("product") match {
      case Some(product) => sc.get[ProductWordService].smartText(product,
        SmartTextIN(request.xsite.current, "ttl.r"+request.xmisc.pageIndex))
      case None => ""
    }

  override def createPageTitle(request: HttpServletRequest): String =
    request.xattr.get[Product]("product") match {
      case Some(product) => sc.get[ProductWordService].smartText(product,
        SmartTextIN(request.xsite.current, "ttl.p"+request.xmisc.pageIndex))
      case None => ""
    }

  override def createMetaKeywords(request: HttpServletRequest): String =
    request.xattr.get[Product]("product") match {
      case Some(product) => sc.get[ProductWordService].smartText(product,
        SmartTextIN(request.xsite.state.defaultSite, "met.k"+request.xmisc.pageIndex))
      case None => ""
    }

  override def createMetaDescription(request: HttpServletRequest): String =
    request.xattr.get[Product]("product") match {
      case Some(product) => sc.get[ProductWordService].smartText(product,
        SmartTextIN(request.xsite.state.defaultSite, "met.d"+request.xmisc.pageIndex))
      case None => ""
    }

}
