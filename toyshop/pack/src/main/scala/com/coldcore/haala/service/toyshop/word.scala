package com.coldcore.haala
package service
package toyshop

import com.coldcore.haala.core.SiteChain
import com.coldcore.misc.scala.StringOp._
import core.GeneralUtil._
import com.coldcore.haala.domain.toyshop.{Product, ToyshopPack, ProductOption => Opt}
import collection.mutable.ListBuffer
import org.springframework.transaction.annotation.Transactional

object ProductWordService {
  case class SmartTextIN(site: SiteChain, key: String)
  case class PriceLabelIN(site: SiteChain, mode: String = "")
}

trait ProductWordService {
  import ProductWordService._

  def listOptionText(o: Product, site: SiteChain): List[String]
  def smartText(o: Product, in: SmartTextIN): String
  def priceLabel(o: Product, in: PriceLabelIN): String
}

@Transactional
class ProductWordServiceImpl extends BaseService with ProductWordService {
  import ProductWordService._

  /** Return an option by its type. */
  private def getOption(o: Product, typ: Int): Option[Opt] = o.options.toList.find(_.`type` == typ)

  /** Check if any of options exist. */
  private def hasOption(o: Product, typs: Int*): Boolean = o.options.exists(x => typs.contains(x.`type`))

  /** Return option's text by its type. Works with simple or valued options.
   *  8 -> opt.t8a / Beach -> Beach   18 -> opt.v18a / Sleeps {val} -> Sleeps 4
   */
  private def optionText(o: Product, typ: Int, site: SiteChain): String =
    getOption(o, typ) match {
      case Some(op) =>
        if (op.getMode() == op.MODE_SIMPLE) label(op.getValueKey+"a", site).safe
        else if (op.value.safe != "") parametrize(label(op.getValueKey+"a", site).safe, "val:"+op.value)
        else ""
      case None => ""
    }

  /** Return list items options. */
  @Transactional (readOnly = true)
  override def listOptionText(o: Product, site: SiteChain): List[String] =
    (optionText(o, Opt.TYPE_MATERIAL_FABRIC, site) ::
      optionText(o, Opt.TYPE_MATERIAL_RUBBER, site) ::
      Nil).filter(""!=)

  type TokensValues = (ListBuffer[String], ListBuffer[String])

  private val addOptionsValues = (o: Product, template: String, site: SiteChain, t: TokensValues) =>
    if (template.contains("{options}")) {
      t._1 += "options"; t._2 += listOptionText(o, site).mkString(", ")
    }

  private val addPriceValues = (o: Product, template: String, site: SiteChain, t: TokensValues) =>
    if (template.containsAny("{price}", "{oprice}")) {
      t._1 += "price"; t._2 += priceLabel(o, PriceLabelIN(site))
      t._1 += "oprice"; t._2 += priceLabel(o, PriceLabelIN(site, "old"))
    }

  /** Substitute tokens in a label by selecting relevant info. */
  private def smartFillLabel(o: Product, template: String, site: SiteChain): String = {
    val name = label(ToyshopPack.PACK+".pr."+o.ref+".name", site) or ""
    val t = (ListBuffer("ref", "name"), ListBuffer(o.ref, name)) //tokens and values
    (addOptionsValues :: addPriceValues :: Nil).foreach(f => f(o, template, site, t))

    var s = template
    for ((t, v) <- t._1 zip t._2) s = smartToken(s, "{"+t+"}", v)
    s
  }

  /** Build smart text (eg. page title). */
  @Transactional (readOnly = true)
  override def smartText(o: Product, in: SmartTextIN): String =
    smartFillLabel(o, label(in.key, in.site).safe, in.site)

  /** Build price text. */
  @Transactional (readOnly = true)
  override def priceLabel(o: Product, in: PriceLabelIN): String =
    if (o.price == null) label("price.nopr", in.site) //nothing to display
    else {
      val sign = sc.get[CurrencyService].getSign(o.price.currency)
      in.mode match {
        case "old" => //old price if discount price was set (or empty string)
          val price = (o.price.dscPrice == 0) ? 0L | o.price.price.toLong
          if (price == 0) ""
          else parametrize(label("price.sale", in.site).safe, "sign:"+sign, "price:"+toX100(price))
        case "" => //original or discount price
          val price = (o.price.dscPrice != 0) ? o.price.dscPrice.toLong | o.price.price.toLong
          val key = (price == 0) ? "nopr" | "sale"
          parametrize(label("price."+key, in.site).safe, "sign:"+sign, "price:"+toX100(price))
      }
    }

}
