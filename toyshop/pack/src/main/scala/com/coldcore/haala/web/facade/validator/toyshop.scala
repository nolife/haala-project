package com.coldcore.haala
package web
package facade.validator
package toyshop

import com.coldcore.haala.core.NamedLog
import service.toyshop.ProductService
import facade.annotations.DatatypeInput
import facade.validator.Validator.{Context, Field}

class ProductValidator extends BaseValidator with CommonChecks {
  override lazy val LOG = new NamedLog("toyshop.product-validator", log)

  /** Check if a user owns a product. */
  @DatatypeInput("owner")
  def checkOwner(field: Field, context: Context): Boolean =
    if (field.name == "domains") isDomainsOwner(field.strval, context) else isProductOwner(field.strval, context)

  /** Check if a user owns a product. */
  private def isProductOwner(value: String, context: Context): Boolean =
    isOwnerOf(value, context, sc.get[ProductService].getById(_), "product")

}
