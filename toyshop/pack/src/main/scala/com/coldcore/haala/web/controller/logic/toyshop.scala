package com.coldcore.haala
package web
package controller.logic
package toyshop

import BaseController.Mod
import javax.servlet.http.{HttpServletRequest, HttpServletResponse}
import service.toyshop.{ProductSearchService, ProductService}
import service.UserRoleCheck
import core.Dependencies
import core.toyshop.ToyshopRequestX

class ProductMod extends Mod with UserRoleCheck {
  override def process(ctrl: BaseController, request: HttpServletRequest, response: HttpServletResponse): Option[String] = {
    val (id, ref) = (request("id").safeLong, request("ref").toUpperCase)

    (if (id > 0) sc.get[ProductService].getById(id)
     else if (ref.nonEmpty) sc.get[ProductService].getByRef(ref)
     else None
    ).foreach { product =>
      if (product.pack.user.id == request.xuser.currentId || isUserAdmin) request.xattr + ("product" -> product)
    }

    None
  }
}

class ViewProductMod extends Mod {
  override def process(ctrl: BaseController, request: HttpServletRequest, response: HttpServletResponse): Option[String] = {
    val ref = request("ref") or request.last // .../view.htm?ref=3I51B1R4   .../3I51B1R4   3I51B1R4?enq=1 -> 3I51B1R4
    if (ref.nonEmpty) sc.get[ProductService].getByRef(ref).foreach { product =>
      if (ToyshopRequestX(request).xtoyshop.isDomainSupportsProduct(product)) request.xattr + ("product" -> product)
    }
    None
  }
}

class SitemapMod extends SitemapController.Mod with Dependencies {
  override def appendToModel(request: HttpServletRequest): Map[String,AnyRef] = {
    val refs = sc.get[ProductSearchService].allActiveRefsByDomain(request.xsite.state.currentDomainId)
    Map("products" -> refs.toArray)
  }
}



