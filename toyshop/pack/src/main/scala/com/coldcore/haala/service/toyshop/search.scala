package com.coldcore.haala
package service
package toyshop

import org.springframework.transaction.annotation.Transactional
import beans.BeanProperty
import com.coldcore.haala.dao.GenericDAO
import com.coldcore.haala.domain.toyshop.{Product, ToyshopPack}
import com.coldcore.haala.service.SolrService._
import com.coldcore.haala.service.SearchResult
import com.coldcore.haala.service.SearchInput
import com.coldcore.haala.service.toyshop.Util._

object ProductSearchService {
  class ByUserSearchInput(from: Long, max: Int, orderBy: String = "") extends SearchInput(from, max, orderBy) {
    def this(in: SearchInput) = this(in.from, in.max, in.orderBy)
    val filterValue = ""
  }

  class ByDomainSearchInput(from: Long, max: Int, orderBy: String = "") extends SearchInput(from, max, orderBy) {
    def this(in: SearchInput) = this(in.from, in.max, in.orderBy)
    val domainId = 0L
  }

  class FavsSearchInput(from: Long, max: Int, orderBy: String = "") extends SearchInput(from, max, orderBy) {
    def this(in: SearchInput) = this(in.from, in.max, in.orderBy)
    val refs = List.empty[String]
  }
}

trait ProductSearchService {
  import ProductSearchService._

  def allIds: List[Long]
  def searchByUser(userId: Long, in: ByUserSearchInput): SearchResult[Product]
  def searchByDomain(in: ByDomainSearchInput): SearchResult[Product]
  def searchFast(in: SearchInput): SearchResult[Product]
  def searchFavs(in: FavsSearchInput): SearchResult[Product]
  def allActiveRefsByDomain(domainId: Long): List[String]
}

@Transactional
class ProductSearchServiceImpl extends BaseService with ProductSearchService {
  import ProductSearchService._

  @BeanProperty var toyshopProductDao: GenericDAO[Product] = _

  private def solrSort = SolrService.solrSort(packProductPrefix, _: String)
  private def productType = packProductType(sc)

  /** Find all IDs. */
  @Transactional (readOnly = true)
  override def allIds: List[Long] = toyshopProductDao.findX[Long]("select id from "+classOf[Product].getName)

  /** Find user's products. */
  @Transactional (readOnly = true)
  override def searchByUser(userId: Long, in: ByUserSearchInput): SearchResult[Product] = {
    val result = sc.get[SolrService].searchWT(
      SolrQuery(
        AndTerm(
          ObjectTypeTerm(productType), packProductTerm("usr", userId),
          (in.filterValue != "") ? TextTerm(solrEscapeChars(in.filterValue)) | NoopTerm)),
      FlTerm("id"), StartTerm(in.from), RowsTerm(in.max), SortTerm((in.orderBy != "") ? solrSort(in.orderBy) | ""))
    sc.get[SolrService].fetchObjectsWT(result, toyshopProductDao, classOf[Product])
  }

  /** Find products of specified domain (visitor search). */
  @Transactional (readOnly = true)
  override def searchByDomain(in: ByDomainSearchInput): SearchResult[Product] = {
    val result = sc.get[SolrService].searchWT(
      SolrQuery(
        AndTerm(
          ObjectTypeTerm(productType), NotTerm(packProductTerm("sta", Product.STATUS_NOT_ACTIVE)),
          (in.domainId > 0) ? packProductTerm("dom", in.domainId) | NoopTerm)),
      FlTerm("id"), StartTerm(in.from), RowsTerm(in.max), SortTerm((in.orderBy != "") ? solrSort(in.orderBy) | ""))
    sc.get[SolrService].fetchObjectsWT(result, toyshopProductDao, classOf[Product])
  }

  /** Fast search without filtering. */
  @Transactional (readOnly = true)
  override def searchFast(in: SearchInput): SearchResult[Product] = {
    val query = "from "+classOf[Product].getName+" "+in.orderByClause
    searchObjects(in.from, in.max, toyshopProductDao, query)
  }

  /** Return favs selected by 'svo' parameters. */
  @Transactional (readOnly = true)
  override def searchFavs(in: FavsSearchInput): SearchResult[Product] =
    if (in.refs.isEmpty) SearchResult(0, Nil)
    else {
      val refs = in.refs.map("'"+_+"'").mkString(",")
      val query = "from "+classOf[Product].getName+" where ref in ("+refs+")"
      val result = searchObjects(in.from, in.max, toyshopProductDao, query)

      val matched = (ref: String) => result.objects.find(_.ref == ref) //filter an product by a ref
      val sorted = in.refs.map(matched).flatten.toList //sort results as in input refs
      SearchResult(sorted.size, sorted)
    }

  /** Find all active REFs by domain ID. */
  @Transactional (readOnly = true)
  override def allActiveRefsByDomain(domainId: Long): List[String] = {
    val query = "select o.ref from "+classOf[Product].getName+" o join o.domains as d "+
      "where (o.status = ? or o.status = ?) and d.id = ?"
    toyshopProductDao.findX[String](query, Product.STATUS_IN_STOCK, Product.STATUS_SOLD_OUT, domainId)
  }

}
