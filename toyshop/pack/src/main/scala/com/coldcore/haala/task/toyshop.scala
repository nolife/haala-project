package com.coldcore.haala
package task
package toyshop

import com.coldcore.haala.service.{SearchInput, SettingService}
import com.coldcore.haala.service.toyshop.{ProductService, ProductSearchService}
import com.coldcore.haala.domain.toyshop.Product
import com.coldcore.haala.service.toyshop.Util._

/** This task operates on all products one by one rescaling pictures.
  * WARN: Task may take plenty of time and should be executed when the system is offline to prevent
  *       it from restarting due to user interaction.
  */
class RescaleProductPicturesTask extends BaseUpdateTask[Product] {
  override val name = "ToyshopRescaleProductPicturesTask"
  override val flagName = "ToyshopRescaleProductPicturesFlag"
  override val logCounterAt: Int = 100
  override def fastSearch(in: SearchInput) = sc.get[ProductSearchService].searchFast(in)
  override def update(o: Product) = sc.get[ProductService].rescalePictures(o.id)
  override def goAhead: Boolean = super.goAhead && installed(sc)
}

/** On demand task to submit all products into Solr. */
class SolrSubmitProductsTask extends BaseSolrSubmitTask[Product] {
  override val name = "ToyshopSolrSubmitProductsTask"
  override val flagName = "ToyshopSolrSubmitProductsFlag"
  override def solrType: Int = packProductType(sc)
  override def fastSearch(in: SearchInput) = sc.get[ProductSearchService].searchFast(in)
  override def solrDoc(o: Product) = sc.get[ProductService].prepareSolrDoc(o)
  override def goAhead: Boolean = super.goAhead && installed(sc)
}
