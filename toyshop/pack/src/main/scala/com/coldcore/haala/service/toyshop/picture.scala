package com.coldcore.haala
package service
package toyshop

import com.coldcore.haala.core.JsonUtil.fromJson
import service.FileService.Body
import domain.{File, Picture}
import org.springframework.transaction.annotation.Transactional
import core.{CoreException, VirtualPath}
import com.coldcore.haala.domain.toyshop.Product
import com.coldcore.haala.core.toyshop.PictureScale
import com.coldcore.haala.service.toyshop.Util._
import core.toyshop.PictureConstants._

trait PictureService {
  def updatePicture(o: Picture): Picture
  def createFace(product: Product, body: Body, position: Int): Picture
  def createListThumb(product: Product, body: Body): Picture
}

@Transactional
class PictureServiceImpl extends BaseService with PictureService with ScaleAndCrop {

  /** Scale a picture based on a supplied type and configured parameters. */
  private def scalePicture(body: Body, typ: Int): List[ScaleResult] = {
    val config = fromJson(setting("ToyshopPictureScale"), classOf[PictureScale])
    val qas = typ match {
      case TYPE_PRODUCT_FACE =>
        (config.product.face.quality, config.product.face.size.toList, File.SUFFIX_DEFAULT) ::
        (config.product.face2.quality, config.product.face2.size.toList, "2") ::
        Nil
      case TYPE_PRODUCT_LIST_THUMB =>
        (config.product.list.quality, config.product.list.size.toList, File.SUFFIX_DEFAULT) ::
        (config.product.list2.quality, config.product.list2.size.toList, "2") ::
        Nil
      case _ => throw new CoreException("Unknown type: "+typ)
    }
    scale(body, qas)
  }

  /** (Re)scale a picture based on new measurements. */
  @Transactional (readOnly = false)
  override def updatePicture(o: Picture): Picture = {
    rescaleAndUpdate(o, scalePicture)
    o
  }

  /** Create a new picture. */
  private def createPicture(product: Product, body: Body, position: Int, typ: Int): Picture =
    createScaledPicture(body, position, typ, scalePicture,
                        o => VirtualPath(packPath("pr", product.ref, "p"), o.id.hex+".jpg"))

  /** Scale an image and attach to product's faces. */
  @Transactional (readOnly = false)
  override def createFace(product: Product, body: Body, position: Int): Picture =
    createPicture(product, body, position, TYPE_PRODUCT_FACE)

  /** Scale an image and attach as product's listing thumb. */
  @Transactional (readOnly = false)
  override def createListThumb(product: Product, body: Body): Picture =
    createPicture(product, body, 0, TYPE_PRODUCT_LIST_THUMB)
}
