package com.coldcore.haala
package web.core
package toyshop

import javax.servlet.http.HttpServletRequest
import com.coldcore.haala.domain.toyshop.Product
import com.coldcore.haala.service.DomainService

class ToyshopRequestExt(rx: ToyshopRequestX) extends CoreActions {
  private val request = rx.request

  /** Check if current domain supports supplied product. */
  def isDomainSupportsProduct(product: Product): Boolean = isDomainSupports(product, request)
}

case class ToyshopRequestX(override val request: HttpServletRequest) extends RequestX(request) {
  val xtoyshop = new ToyshopRequestExt(this)
}
