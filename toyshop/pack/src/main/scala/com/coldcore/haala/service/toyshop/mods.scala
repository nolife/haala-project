package com.coldcore.haala
package service
package toyshop

import domain.{Domain, Pack, User}
import domain.toyshop.ToyshopPack

/** Addition to user service. */
class UserMod extends UserService.Mod with Dependencies {
  override def delete(o: User) = o.toyshopPack.products.map(x => sc.get[ProductService].delete(x.id))
  override def register(o: User, domain: Domain) = addPack(o)
  override def addPack(o: User) = o.addPack(ToyshopPack.PACK) { new ToyshopPack }
}
