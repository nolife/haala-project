package com.coldcore.haala
package service
package toyshop

import org.scalatest.{BeforeAndAfter, BeforeAndAfterAll, WordSpec}
import org.scalatest.mockito.MockitoSugar
import org.mockito.Mockito._
import org.mockito.ArgumentMatchers._
import org.junit.runner.RunWith
import org.scalatest.junit.JUnitRunner
import com.coldcore.haala.service.toyshop.Util._

@RunWith(classOf[JUnitRunner])
class UtilSpec extends WordSpec with MockitoSugar with BeforeAndAfter {

  "packLabel" should {
    "return correct label" in {
      assertResult("toyshop.pr.AAFF.%") { packLabel("pr", "AAFF", "%") }
    }
  }

  "packPath" should {
    "return correct file path" in {
      assertResult("toyshop/pr/AAFF/%") { packPath("pr", "AAFF", "%") }
    }
  }

}
