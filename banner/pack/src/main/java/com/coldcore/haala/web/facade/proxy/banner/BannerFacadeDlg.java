package com.coldcore.haala.web.facade.proxy.banner;

import com.coldcore.haala.web.facade.annotations.AopFacade;
import com.coldcore.haala.web.facade.annotations.TargetClass;
import com.coldcore.haala.web.facade.logic.banner.BannerFacade;
import com.coldcore.haala.web.facade.vo.cmd.*;
import com.coldcore.haala.web.facade.vo.cmd.banner.*;

import javax.servlet.http.HttpServletRequest;

/** AOP proxy delegate */
@AopFacade
public class BannerFacadeDlg {

  @TargetClass private BannerFacade bannerFacade;

  public BannerFacade getBannerFacade() {
    return bannerFacade;
  }

  public void setBannerFacade(BannerFacade bannerFacade) {
    this.bannerFacade = bannerFacade;
  }

  /*** Stub methods. Invoked by 'InvokerAspect' directly on the 'target'. ***/
  public Object search(SearchVO cmd, HttpServletRequest request) { return null; }
  public Object filter(FilterVO cmd, HttpServletRequest request) { return null; }
  public Object create(AddBannerVO cmd, HttpServletRequest request) { return null; }
  public Object update(UpdateBannerVO cmd, HttpServletRequest request) { return null; }
  public Object remove(RemoveBannerVO cmd, HttpServletRequest request) { return null; }
  public Object editFile(EditFileVO cmd, HttpServletRequest request) { return null; }
}
