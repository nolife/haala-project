package com.coldcore.haala
package service
package banner

import com.coldcore.haala.service.SolrService.{ValueTerm, Term}
import com.coldcore.haala.domain.banner.BannerPack

object Util {
  def packLabel(tokens: String*): String = BannerPack.PACK+"."+tokens.mkString(".")
  def packPath(tokens: String*): String = BannerPack.PACK+"/"+tokens.mkString("/")
  def packBannerType(sc: ServiceContainer): Int = sc.get[SettingService].queryConstant[Int]("Constants", BannerPack.PACK, "SOLR_TYPE_BANNER")
  def packBannerPrefix: String = "banner_ba"
  def packBannerTerm(key: String, value: Any): Term = ValueTerm(packBannerPrefix+"_"+key, value)
  def installed(sc: ServiceContainer): Boolean = sc.get[SettingService].querySystem("InstalledPacks").safe.parseCSV.contains(BannerPack.PACK)
}