package com.coldcore.haala
package web
package tag.freemarker
package banner

import com.coldcore.haala.core.{Meta, SiteState}
import com.coldcore.haala.directive.freemarker.{BaseDirective, State, StateWithVars}
import com.coldcore.haala.domain.banner.Banner
import com.coldcore.haala.service.{LabelService, ServiceContainer}
import com.coldcore.haala.service.banner.BannerService
import com.coldcore.haala.service.banner.Util._

class BannerFetchTag extends BaseDirective with StateWithVars {
  override def execute(state: StateWithVars) {
    import state._
    val domainId = state.systemVarAsObject[SiteState]("siteState").get.currentDomainId
    val banners = sc.get[BannerService].fetch(domainId, pval("opts").parseCSV.map(_.toInt), pnum("max").toInt,
      fill = pval("fill").isEmpty || pval("fill").isTrue)
    output(state, banners)
  }
}

/** Reuse a banner fetched earlier. */
trait ReuseBanner {
  def reuse(banners: List[Banner], pos: Int): Option[Banner] =
    if (pos > 0 && pos <= banners.size) Some(banners(pos-1)) else if (banners.nonEmpty) Some(banners.random) else None
}

/** Print a banner */
trait RenderBanner {
  self: { val sc: ServiceContainer } =>

  def render(banner: Banner, state: State) {
    val site = state.resolveSite
    val text = (key: String) => sc.get[LabelService].query(packLabel("ba", banner.ref, key), site).safe.convert("html")
    val meta = Meta(banner.meta)
    val (href, target, title, alt) = (
      s""" href="${banner.url}" """,
      if (meta("window", "new") == "new") """ target="_blank" """ else "",
      if (text("ttl").nonEmpty) s""" title="${text("ttl")}" """ else "",
      if (text("alt").nonEmpty) s""" alt="${text("alt")}" """ else ""
    )

    val pictureHtml = s"""
      |<a class="picture" $href $title $target >
      |  <img src="/${packPath("ba", banner.ref, "p.gif")}" $alt />
      |</a>
    """.stripMargin

    val linkHtml = s"""
      |<a class="link" $href $title $target >
      |  ${text("txt")}
      |</a>
    """.stripMargin

    val html = banner.`type`.toInt match {
      case Banner.TYPE_PICTURE =>
        s"""
          |<div class="banner typ${banner.`type`}">
          |  $pictureHtml
          |  ${text("txt").nonEmpty ? linkHtml | ""}
          |</div>
        """.stripMargin
      case Banner.TYPE_TEXT =>
        s"""
          |<div class="banner typ${banner.`type`}">
          |  $linkHtml
          |</div>
        """.stripMargin
      case _ => ""
    }
    state.print(html)
  }
}

class BannerReuseTag extends BaseDirective with ReuseBanner with StateWithVars {
  override def execute(state: StateWithVars) {
    import state._
    reuse(state.paramAsObject[List[Banner]](pval("banners")).get, pnum("pos").toInt).foreach(banner =>
      outputTM(state, state.wrap(banner)))
  }
}

class BannerZoneTag extends BaseDirective with RenderBanner with StateWithVars {
  override def execute(state: StateWithVars) {
    import state._
    val domainId = state.systemVarAsObject[SiteState]("siteState").get.currentDomainId
    val banners = sc.get[BannerService].fetch(domainId, pval("opts").parseCSV.map(_.toInt), pnum("max").toInt,
      fill = pval("fill").isEmpty || pval("fill").isTrue)
    banners.foreach(render(_, state))
  }
}
