package com.coldcore.haala
package web
package facade.logic
package banner

import scala.beans.BeanProperty
import com.coldcore.misc.scala.StringOp._
import security.annotations.SecuredRole
import javax.servlet.http.HttpServletRequest
import com.coldcore.haala.core.NamedLog
import core.SearchHelper
import service.exceptions.{LimitExceededException, ObjectNotFoundException}
import service.SearchInput
import service.banner.BannerService
import service.banner.BannerService.{CreateIN, OptionIN, SearchInputX, UpdateIN}
import facade.annotations.{ErrorCR, LogClass, ValidatorClass}
import facade.validator.Validator
import facade.vo.CallResult
import facade.vo.cmd.{FilterVO, SearchVO}
import facade.logic.BaseFacade.{SortMapping, TargetedFilter, TargetedSearch}
import facade.vo.cmd.banner._
import domain.banner.Banner

/** Convert input and output data */
trait BannerConvert {
  self: BannerFacade =>

  implicit def addBannerVOtoIN(cmd: AddBannerVO) =
    CreateIN(cmd.`type`)

  implicit def updateBannerVOtoIN(cmd: UpdateBannerVO) =
    UpdateIN(cmd.active, cmd.url, cmd.reciprocal, cmd.newWindow, cmd.position, cmd.languages.safe.parseCSV, cmd.expires,
      listSiteableText(cmd.text), cmd.options.map(x => OptionIN(x.`type`, x.value)).toList,
      cmd.domains.safe.parseCSV)

  val bannerVO = (o: Banner) =>
    Map('id -> o.id, 'ref -> o.ref,
      'domains -> o.domains.map(_.domain),
      'url -> trimCut(o.url.safe, 200).convert("html")
    )
}

class BannerFacade extends BaseFacade with BannerConvert {
  @BeanProperty @LogClass lazy val LOG = new NamedLog("banner-facade", log)

  @BeanProperty @ValidatorClass var bannerValidator: Validator = _

  @SecuredRole("BANNER")
  @ErrorCR(on = classOf[LimitExceededException], key = "fcall.limit", log = "Banners limit reached for user {currentUsername}")
  def create(cmd: AddBannerVO, request: HttpServletRequest): CallResult = {
    val o = sc.get[BannerService].create(request.xuser.currentId, cmd)
    LOG.info("User "+request.xuser.currentUsername+" created banner #"+o.id)
    dataCR(o.ref)
  }

  @SecuredRole("BANNER")
  @ErrorCR(on = classOf[ObjectNotFoundException], key = "fcall.nofnd", log = "Banner #{bannerId} not found")
  def update(cmd: UpdateBannerVO, request: HttpServletRequest): CallResult = {
    sc.get[BannerService].update(cmd.bannerId.toLong, cmd)
    LOG.info("User "+request.xuser.currentUsername+" updated banner #"+cmd.bannerId)
    dataCR()
  }

  @SecuredRole("BANNER")
  def remove(cmd: RemoveBannerVO, request: HttpServletRequest): CallResult = {
    sc.get[BannerService].delete(cmd.bannerId.toLong)
    LOG.info("User "+request.xuser.currentUsername+" deleted banner #"+cmd.bannerId)
    dataCR()
  }

  private val searchOwned = (si: SearchInput, sh: SearchHelper) =>
    sc.get[BannerService].search(new SearchInputX(si) {
      //filter params: filter (FOO or empty)
      override val userId = Some(sh.request.xuser.currentId)
      override val filterValue = sh.filter(0).toLowerCase.option
    }).serializeAsMap(bannerVO)

  @SecuredRole("BANNER")
  def search(cmd: SearchVO, request: HttpServletRequest): CallResult =
    genericSearch(cmd, request,
      TargetedSearch(1, "bannerMyBanners", "bannerMyBanners",
        orderBy(cmd, SortMapping("ref")),
        searchOwned))

  @SecuredRole("BANNER")
  def filter(cmd: FilterVO, request: HttpServletRequest): CallResult = {
    genericSearchFilter(cmd, request, TargetedFilter(1, "bannerMyBanners"))
    dataCR()
  }

  @SecuredRole("BANNER")
  @ErrorCR(on = classOf[ObjectNotFoundException], key = "fcall.nofnd", log = "Banner #{bannerId} not found")
  def editFile(cmd: EditFileVO, request: HttpServletRequest): CallResult = {
    uploadedBodies(request).headOption match {
      case Some(body) =>
        sc.get[BannerService].updateFile(cmd.bannerId.toLong, body)
        LOG.info("User "+request.xuser.currentUsername+" updated file on banner #"+cmd.bannerId)
      case None =>
    }
    dataCR()
  }

}
