package com.coldcore.haala
package service

package banner {
  package test {

    class MyMock extends com.coldcore.haala.service.test.MyMock {
      val bannerService = mock[BannerService]

      serviceContainer.services =
        bannerService :: serviceContainer.services
    }

  }
}