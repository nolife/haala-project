package com.coldcore.haala
package web
package controller.aspect
package banner

import core.Dependencies
import javax.servlet.http.HttpServletRequest
import service.LabelsReader
import domain.banner.Banner
import com.coldcore.misc.scala.StringOp._

class PageMod extends PageAspect.Mod with Dependencies with LabelsReader {
  import PageAspect._

  override def createRecentTitle(request: HttpServletRequest): String =
    request.xattr.get[Banner]("banner") match {
      case Some(banner) =>
        val template = label("ttl.r"+request.xmisc.pageIndex, request.xsite.current).safe
        parametrize(template, "ref:"+banner.ref)
      case None => ""
    }

  override def createPageTitle(request: HttpServletRequest): String =
    request.xattr.get[Banner]("banner") match {
      case Some(banner) =>
        val template = label("ttl.p"+request.xmisc.pageIndex, request.xsite.current).safe
        parametrize(template, "ref:"+banner.ref)
      case None => ""
    }

  override def createMetaKeywords(request: HttpServletRequest): String =
    request.xattr.get[Banner]("banner") match {
      case Some(banner) =>
        val template = label("met.k"+request.xmisc.pageIndex, request.xsite.state.defaultSite).safe
        parametrize(template, "ref:"+banner.ref)
      case None => ""
    }

  override def createMetaDescription(request: HttpServletRequest): String =
    request.xattr.get[Banner]("banner") match {
      case Some(banner) =>
        val template = label("met.d"+request.xmisc.pageIndex, request.xsite.state.defaultSite).safe
        parametrize(template, "ref:"+banner.ref)
      case None => ""
    }

}
