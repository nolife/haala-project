package com.coldcore.haala
package web
package facade.validator
package banner

import com.coldcore.haala.core.NamedLog
import service.banner.BannerService
import facade.annotations.DatatypeInput
import facade.validator.Validator.{Context, Field}

class BannerValidator extends BaseValidator with CommonChecks {
  override lazy val LOG = new NamedLog("banner-validator", log)

  /** Check if a user owns a domain or a banner. */
  @DatatypeInput("owner")
  def checkOwner(field: Field, context: Context): Boolean =
    if (field.name == "domains") isDomainsOwner(field.strval, context) else isBannerOwner(field.strval, context)

  /** Check if a user owns a banner. */
  private def isBannerOwner(value: String, context: Context): Boolean =
    isOwnerOf(value, context, sc.get[BannerService].getById(_), "banner")

}
