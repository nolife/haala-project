package com.coldcore.haala
package service

import com.coldcore.haala.domain.User
import com.coldcore.haala.domain.banner.BannerPack

package object banner {
  class BannerUser(user: User) {
    def bannerPack = user.getPack[BannerPack](BannerPack.PACK)

    /** Method for traversing in JSP
      *  user.bannerPack.banners
      */
    def getBannerPack = bannerPack

    /** Method for passing regular user into JSP tags or traversing it
      *  user="${owner.unwrap}"   user.unwrap.address
      */
    def getUnwrap = user
  }

  implicit def bannerUser(user: User): BannerUser = new BannerUser(user)
}
