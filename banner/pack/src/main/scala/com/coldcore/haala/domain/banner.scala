package com.coldcore.haala
package domain
package banner

import scala.beans.BeanProperty
import com.coldcore.haala.domain.annotations.EqualsHashCode
import java.util.Date

object BannerPack {
  val PACK = "banner"
}

class BannerPack extends Pack {
  import BannerPack._
  pack = PACK
  @BeanProperty var banners: JList[Banner] = new JArrayList[Banner]
}

class Banner extends BaseObject with BasePackObject[BannerPack] with BaseOptionObject[BannerOption] {
  @BeanProperty @EqualsHashCode var ref: String = _
  @BeanProperty @EqualsHashCode var active: JInteger = 0
  @BeanProperty @EqualsHashCode var expires: JLong = _ //Midnight (00:00:00) of the next day (excluded)
  @BeanProperty @EqualsHashCode var url: String = _
  @BeanProperty @EqualsHashCode var reciprocal: String = _
  @BeanProperty @EqualsHashCode var `type`: JInteger = _
  @BeanProperty @EqualsHashCode var meta: String = _
  @BeanProperty var domains: JList[Domain] = new JArrayList[Domain]
}

object Banner {
  val TYPE_PICTURE = 1
  val TYPE_TEXT = 2
}

class BannerOption extends BaseOption("bannerBaOpt") {
  @BeanProperty var banner: Banner = _
}

object BannerOption {
  val TYPE_ZONE_1 = 1
  val TYPE_ZONE_2 = 2
  val TYPE_ZONE_3 = 3
  val TYPE_ZONE_4 = 4
  val TYPE_ZONE_5 = 5
  val TYPE_ZONE_6 = 6
  val TYPE_ZONE_7 = 7
  val TYPE_ZONE_8 = 8
  val TYPE_ZONE_9 = 9
  val TYPE_ZONE_10 = 10
  val TYPE_ZONE_11 = 11
  val TYPE_ZONE_12 = 12
  val TYPE_ZONE_13 = 13
  val TYPE_ZONE_14 = 14
  val TYPE_ZONE_15 = 15
  val TYPE_ZONE_16 = 16
  val TYPE_ZONE_17 = 17
  val TYPE_ZONE_18 = 18
  val TYPE_ZONE_19 = 19
  val TYPE_ZONE_20 = 20
  val TYPE_ZONE_21 = 21
  val TYPE_ZONE_22 = 22
  val TYPE_ZONE_23 = 23
  val TYPE_ZONE_24 = 24
  val TYPE_ZONE_25 = 25
  val TYPE_ZONE_26 = 26
  val TYPE_ZONE_27 = 27
  val TYPE_ZONE_28 = 28
  val TYPE_ZONE_29 = 29
  val TYPE_ZONE_30 = 30

  val OPTIONS_ZONE = List(TYPE_ZONE_1, TYPE_ZONE_2, TYPE_ZONE_3, TYPE_ZONE_4, TYPE_ZONE_5, TYPE_ZONE_6, TYPE_ZONE_7,
    TYPE_ZONE_8, TYPE_ZONE_9, TYPE_ZONE_10, TYPE_ZONE_11, TYPE_ZONE_12, TYPE_ZONE_13, TYPE_ZONE_14, TYPE_ZONE_15,
    TYPE_ZONE_16, TYPE_ZONE_17, TYPE_ZONE_18, TYPE_ZONE_19, TYPE_ZONE_20, TYPE_ZONE_21, TYPE_ZONE_22, TYPE_ZONE_23,
    TYPE_ZONE_24, TYPE_ZONE_25, TYPE_ZONE_26, TYPE_ZONE_27, TYPE_ZONE_28, TYPE_ZONE_29, TYPE_ZONE_30)
}
