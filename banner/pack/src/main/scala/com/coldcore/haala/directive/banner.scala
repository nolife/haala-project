package com.coldcore.haala
package directive.freemarker.banner

import com.coldcore.haala.core.Meta
import com.coldcore.haala.service.Dependencies
import com.coldcore.haala.directive.freemarker._
import com.coldcore.haala.service.banner.BannerService
import com.coldcore.haala.domain.banner.{Banner, BannerOption}
import com.coldcore.haala.service.banner.Util._

trait GetBanner extends GetOptionObject[BannerOption,Banner] with Dependencies {
  def getBanner(state: State): Option[Banner] = {
    import state._
    val pval = (x: String) => paramAsString(x).getOrElse("")
    val (id, ref) = (pval("bannerId"), pval("bannerRef"))

    paramAsObject[Banner]("banner").orElse { //banner object as param
      if (id.nonEmpty) sc.get[BannerService].getById(id.toLong) //by ID
      else if (ref.nonEmpty) sc.get[BannerService].getByRef(ref) //by REF
      else None
    }
  }

  override def getObject(state: State) = getBanner(state)
}

class BannerDirective extends BaseDirective with GetBanner {
  override def execute(state: State) = getBanner(state).foreach(banner => outputTM(state, state.wrap(banner)))
}

class BannerFilePathDirective extends BaseDirective with GetBanner {
  override def execute(state: State) = getBanner(state).foreach(banner =>
    if (banner.`type` == Banner.TYPE_PICTURE) output(state, packPath("ba", banner.ref, "p.gif")))
}

class BannerPropertyDirective extends BaseDirective with StateWithVars with GetBanner {

  class TextProcessor(state: StateWithVars, banner: Banner) extends Templated(state) {
    import state._
    val value = label(packLabel("ba", banner.ref, pval("property")), site)
    templatedValue(value, output(state, _: String))
  }

  class WindowProcessor(state: StateWithVars, banner: Banner) extends Templated(state) {
    val value = Meta(banner.meta)("window", "new")
    templatedValue(value, output(state, _: String))
  }

  class PositionProcessor(state: StateWithVars, banner: Banner) extends Templated(state) {
    val value = Meta(banner.meta)("position", "0")
    templatedValue(value, output(state, _: String))
  }

  class LanguagesProcessor(state: StateWithVars, banner: Banner) extends Templated(state) {
    val value = Meta(banner.meta)("languages", "")
    templatedValue(value, output(state, _: String))
  }

  override def execute(state: StateWithVars) = getBanner(state).foreach { banner =>
    import state._
    pval("property") match {
      case "window" => new WindowProcessor(state, banner)
      case "position" => new PositionProcessor(state, banner)
      case "languages" => new LanguagesProcessor(state, banner)
      case _ => new TextProcessor(state, banner)
    }
  }
}

trait BannerOptionsGroup extends OptionsGroup {
  override def onGroup(group: String): List[Int] = group match {
    case "zone" => BannerOption.OPTIONS_ZONE
    case _ => Nil
  }
}

trait NewBannerOption {
  def newOption = new BannerOption
}

class ListBannerOptionTypesDirective extends ListOptionTypesDirective with BannerOptionsGroup

class BannerOptionKeyDirective extends OptionKeyDirective[BannerOption] with NewBannerOption

class BannerOptionValueDirective extends OptionValueDirective[BannerOption,Banner] with GetBanner

class BannerHasOptionDirective extends HasOptionDirective[BannerOption,Banner] with GetBanner

class BannerOptionTextDirective extends OptionTextDirective[BannerOption,Banner] with GetBanner

class BannerOptionModeDirective extends OptionModeDirective[BannerOption] with NewBannerOption

