package com.coldcore.haala
package service
package banner

import core._
import core.banner.Params
import core.SiteUtil._
import FileService.Body
import exceptions.{LimitExceededException, ObjectNotFoundException}
import service.SolrService._
import Util._
import com.google.gson.Gson
import org.springframework.transaction.annotation.Transactional
import scala.beans.BeanProperty
import dao.GenericDAO
import com.coldcore.misc5.IdGenerator._
import domain.banner.{Banner, BannerOption}

import scala.collection.JavaConverters._

object BannerService {
  case class OptionIN(`type`: String, value: String)
  case class CreateIN(`type`: String)
  case class UpdateIN(active: String, url: String, reciprocal: String, newWindow: String, position: String,
                      languages: List[String], expires: String, text: List[SiteableText], options: List[OptionIN],
                      domains: List[String])

  class SearchInputX(from: Long, max: Int, orderBy: String = "") extends SearchInput(from, max, orderBy) {
    def this(in: SearchInput) = this(in.from, in.max, in.orderBy)

    val filterValue: Option[String] = None
    val domainId: Option[Long] = None
    val userId: Option[Long] = None
    val active: Option[Int] = None
    val options: List[Int] = Nil
  }
}

trait BannerService {
  import BannerService._

  def getById(id: Long): Option[Banner]
  def getByRef(ref: String): Option[Banner]
  def create(userId: Long, in: CreateIN): Banner
  def delete(id: Long)
  def update(id: Long, in: UpdateIN): Banner
  def prepareSolrDoc(o: Banner): SolrDoc
  def search(in: SearchInputX): SearchResult[Banner]
  def searchFast(in: SearchInput): SearchResult[Banner]
  def fetch(domId: Long, opts: List[Int], max: Int, site: SiteChain = SiteChain.empty, fill: Boolean = true): List[Banner]
  def updateFile(id: Long, body: Body)
}

@Transactional
class BannerServiceImpl extends BaseService with BannerService {
  import BannerService._

  @BeanProperty var bannerDao: GenericDAO[Banner] = _

  private def solrSort = SolrService.solrSort(packBannerPrefix, _: String)
  private def bannerType = packBannerType(sc)

  private def merge(o: BannerOption, in: OptionIN) {
    o.`type` = in.`type`.toInt
    o.value = in.value.safe //nullable field
  }

  /** Prepare Solr doc. */
  override def prepareSolrDoc(o: Banner): SolrDoc = {
    val terms =
      List(
        ObjectIdTerm(o.id, ObjectTypeTerm(bannerType)),
        packBannerTerm("ref", o.ref),
        packBannerTerm("act", o.active),
        packBannerTerm("usr", o.pack.user.id),
        ValueTerm("text", o.url)) :::
      //domains
      (List.empty[Term] /: o.domains)((a,b) => packBannerTerm("dom", b.id) :: a) :::
      //options
      (List.empty[Term] /: o.options)((a,b) => packBannerTerm("opt", b.`type`) :: a)
    SolrDoc(terms: _*)
  }

  /** Submit an object to Solr. */
  private def doSolrSubmit(o: Banner) =
    sc.get[SolrService].submitWT(prepareSolrDoc(o))

  /** Delete an object from Solr. */
  private def doSolrDelete(o: Banner) =
    sc.get[SolrService].deleteByIdsWT(ObjectIdTerm(o.id, ObjectTypeTerm(bannerType)))

  /** Get an object by its ID. */
  @Transactional (readOnly = true)
  override def getById(id: Long): Option[Banner] = bannerDao.findById(id)

  /** Get an object by a REF. */
  @Transactional (readOnly = true)
  override def getByRef(ref: String): Option[Banner] = bannerDao.findUniqueByProperty("ref", ref)

  /** Generate a unique ref. */
  private def generateUniqueRef: String =
    Iterator.fill(10000)(generate(8,8).toUpperCase)
      .find(ref => ref.matches("\\D+") && getByRef(ref).isEmpty) //non digit
      .getOrElse(throw new CoreException("Ref generator exhausted"))

  /** Create a banner. */
  @Transactional (readOnly = false)
  override def create(userId: Long, in: CreateIN): Banner = {
    val user = sc.get[UserService].getById(userId).getOrElse(throw new ObjectNotFoundException)

    //test that max inactive banners limit is not exceeded
    val params = Option(new Gson().fromJson(setting("BannerParams").safe, classOf[Params])).get
    val count = user.bannerPack.banners.count(_.active.isFalse)
    val limit = params.banner.parseCSVMap.getOrElse("inactBanners", "").toInt
    if (count >= limit) throw new LimitExceededException

    val o = new Banner
    o.`type` = in.`type`.toInt
    o.pack = user.bannerPack
    o.ref = generateUniqueRef

    o.pack.banners.add(o)
    bannerDao.saveOrUpdate(o)
    doSolrSubmit(o) //update Solr
    o
  }

  /** Delete a banner. */
  @Transactional (readOnly = false)
  override def delete(id: Long) = getById(id).foreach { o =>
    //delete banner's labels, files and an banner itself
    labelService.deleteByKeyLike(packLabel("ba", o.ref, "%"))
    sc.get[FileService].deleteByFolder(VirtualPath(packPath("ba", o.ref), "*"))
    o.pack.banners.remove(o)
    bannerDao.delete(o)
    doSolrDelete(o) //update Solr
  }

  @Transactional (readOnly = false)
  override def update(id: Long, in: UpdateIN): Banner = {
    val o = getById(id).getOrElse(throw new ObjectNotFoundException)
    o.url = in.url
    o.active = if (in.active.isTrue) 1 else 0
    o.reciprocal = in.reciprocal.safe
    o.expires = if (in.expires.safe != "") Timestamp(in.expires, "dd/MM/yyyy") else null

    o.meta = Meta(o.meta)
      .?(in.newWindow.isFalse, "window", "same")
      .?(in.position.safeInt > 0, "position", ""+in.position.toInt)
      .?(in.languages.safe.nonEmpty, "languages", in.languages.mkString(","))
      .serialize

    expandOrShrink(o.options, in.options.size, () => new BannerOption { banner = o })
    for ((op, voop) <- o.options zip in.options) merge(op, voop)

    val keyF = (k: String) => k match {
      case "alt" => packLabel("ba", o.ref, "alt")
      case "title" => packLabel("ba", o.ref, "ttl")
      case "text" => packLabel("ba", o.ref, "txt")
    }
    writeLabels(keyF, in.text: _*)

    val domains = in.domains.map(x => sc.get[DomainService].getById(x.toLong).get).asJava
    o.domains.clear
    o.domains.addAll(domains)

    bannerDao.saveOrUpdate(o)
    doSolrSubmit(o) //update Solr
    o
  }

  /** Fast search without filtering. */
  @Transactional (readOnly = true)
  override def searchFast(in: SearchInput): SearchResult[Banner] = {
    val query = "from "+classOf[Banner].getName+" "+in.orderByClause
    searchObjects(in.from, in.max, bannerDao, query)
  }

  /** Solr search. */
  @Transactional (readOnly = true)
  override def search(in: SearchInputX): SearchResult[Banner] = {
    val optTerms = for (op <- in.options) yield packBannerTerm("opt", op)
    val result = sc.get[SolrService].searchWT(
      SolrQuery(
        AndTerm(
          AndTerm(
            ObjectTypeTerm(bannerType),
            in.userId.map(packBannerTerm("usr", _)).getOrElse(NoopTerm),
            in.domainId.map(packBannerTerm("dom", _)).getOrElse(NoopTerm),
            in.active.map(packBannerTerm("act", _)).getOrElse(NoopTerm),
            in.filterValue.map(TextTerm).getOrElse(NoopTerm),
            in.options.nonEmpty ? AndTerm(optTerms: _*) | NoopTerm))),
      FlTerm("id"), StartTerm(in.from), RowsTerm(in.max), SortTerm((in.orderBy != "") ? solrSort(in.orderBy) | ""))
    sc.get[SolrService].fetchObjectsWT(result, bannerDao, classOf[Banner])
  }

  /** Fetch active banners for a domain and position those accordingly. */
  @Transactional (readOnly = true)
  override def fetch(domId: Long, opts: List[Int], max: Int, site: SiteChain = SiteChain.empty, fill: Boolean = true): List[Banner] = {
    val banners =
      search(new SearchInputX(0, 999) {
        override val domainId = Some(domId)
        override val active = Some(1)
        override val options = opts
      }).objects

    def position(o: Banner): Int =  Meta(o.meta)("position", "0").toInt
    def languages(o: Banner): List[String] = Meta(o.meta)("languages", "").parseCSV
    val lang = siteLocale(site, setting("Sites"))
    def language(o: Banner): Boolean = languages(o).isEmpty || site.isEmpty || languages(o).contains(lang)

    val split = banners.groupBy(position(_) > 0)
    val (fixed, float) = (split.getOrElse(true, Nil), split.getOrElse(false, Nil))

    val addedIds = new scala.collection.mutable.HashSet[Long]
    val added = (id: Long) => if (addedIds.contains(id)) true else { addedIds.add(id); false }
    def pickFixed(pos: Int): Option[Banner] = fixed.find(o => position(o) == pos && language(o))
    def pickAnyFloat: Banner = if (float.isEmpty) null else float.random
    def pickAny: Banner = if (banners.isEmpty) null else banners.random

    //pick fixed by position then any float which was not added yet then any banner
    (for (pos <- 1 to max) yield
      pickFixed(pos).getOrElse {
        Iterator.fill(100)(pickAnyFloat).find(x => x != null && language(x) && !added(x.id)).getOrElse(fill ? pickAny | null)
      }).filter(null!=).toList
  }

  /** Update banner's file (pictures) from a file body. */
  @Transactional (readOnly = false)
  override def updateFile(id: Long, body: Body) {
    val o = getById(id).getOrElse(throw new ObjectNotFoundException)

    //file is not attached to a banner, using naming convention
    o.`type`.toInt match {
      case Banner.TYPE_PICTURE =>
        val path = packPath("ba", o.ref, "p.gif")
        sc.get[FileService].getByPath(VirtualPath(path), SiteChain.pub) match {
          case Some(file) => sc.get[FileService].update(file.id, FileService.IN(VirtualPath(file.folder, file.name), Constants.SITE_PUBLIC), body)
          case None => sc.get[FileService].create(VirtualPath(path), Constants.SITE_PUBLIC, body)
        }
      case _ => throw new CoreException("Banner type "+o.`type`+" does not support files")
    }

    bannerDao.saveOrUpdate(o)
  }

}
