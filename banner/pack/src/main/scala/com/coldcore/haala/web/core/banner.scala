package com.coldcore.haala
package web.core
package banner

import com.coldcore.haala.service.DomainService
import javax.servlet.http.HttpServletRequest
import com.coldcore.haala.domain.banner.Banner

class BannerRequestExt(rx: BannerRequestX) extends CoreActions {
  private val request = rx.request

  /** Check if current domain supports supplied banner. */
  def isDomainSupportsBanner(banner: Banner): Boolean = isDomainSupports(banner, request)
}

case class BannerRequestX(override val request: HttpServletRequest) extends RequestX(request) {
  val xbanner = new BannerRequestExt(this)
}
