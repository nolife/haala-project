package com.coldcore.haala
package service
package banner

import com.coldcore.haala.domain.{Domain, User}
import com.coldcore.haala.domain.banner.BannerPack

/** Addition to user service. */
class UserMod extends UserService.Mod with Dependencies {
  override def delete(o: User) = o.bannerPack.banners.map(x => sc.get[BannerService].delete(x.id))
  override def register(o: User, domain: Domain) = addPack(o)
  override def addPack(o: User) = o.addPack(BannerPack.PACK) { new BannerPack }
}
