package com.coldcore.haala
package web
package controller.logic
package banner

import BaseController.Mod
import javax.servlet.http.{HttpServletRequest, HttpServletResponse}
import service.UserRoleCheck
import service.banner.BannerService

class BannerMod extends Mod with UserRoleCheck {
  override def process(ctrl: BaseController, request: HttpServletRequest, response: HttpServletResponse): Option[String] = {
    val (id, ref) = (request("id").safeLong, request("ref").toUpperCase)

    (if (id > 0) sc.get[BannerService].getById(id)
     else if (ref.nonEmpty) sc.get[BannerService].getByRef(ref)
     else None
    ).foreach { banner =>
      if (banner.pack.user.id == request.xuser.currentId || isUserAdmin) request.xattr + ("banner" -> banner)
    }

    None
  }
}

class MyBannersMod extends Mod {
  override def process(ctrl: BaseController, request: HttpServletRequest, response: HttpServletResponse): Option[String] = {
    ctrl.pushSearchState(request, "bannerMyBanners", "bannerMyBanners")
    None
  }
}
