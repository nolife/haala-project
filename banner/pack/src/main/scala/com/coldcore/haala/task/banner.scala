package com.coldcore.haala
package task
package banner

import com.coldcore.haala.service.SearchInput
import com.coldcore.haala.service.banner.Util._
import com.coldcore.haala.domain.banner.Banner
import com.coldcore.haala.service.banner.BannerService

/** On demand task to submit all banners into Solr. */
class SolrSubmitBannersTask extends BaseSolrSubmitTask[Banner] {
  override val name = "BannerSolrSubmitBannersTask"
  override val flagName = "BannerSolrSubmitBannersFlag"
  override def solrType: Int = packBannerType(sc)
  override def fastSearch(in: SearchInput) = sc.get[BannerService].searchFast(in)
  override def solrDoc(o: Banner) = sc.get[BannerService].prepareSolrDoc(o)
  override def goAhead: Boolean = super.goAhead && installed(sc)
}
