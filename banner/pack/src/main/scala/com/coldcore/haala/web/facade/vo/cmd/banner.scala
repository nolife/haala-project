package com.coldcore.haala.web.facade.vo.cmd
package banner

import scala.beans.BeanProperty
import com.coldcore.haala.web.facade.annotations.Input

class AddBannerVO {
  @BeanProperty @Input (required = true) var `type`: String = _
}

class UpdateBannerVO {
  @BeanProperty @Input (datatype = "owner") var bannerId: String = _
  @BeanProperty @Input var expires: String = _
  @BeanProperty @Input (required = true, transform = "url") var url: String = _
  @BeanProperty @Input (transform = "url") var reciprocal: String = _
  @BeanProperty @Input var position: String = _
  @BeanProperty @Input var newWindow: String = _
  @BeanProperty @Input var active: String = _
  @BeanProperty @Input var options: Array[OptionVO] = _
  @BeanProperty @Input var text: Array[TextVO] = _
  @BeanProperty @Input (transform = "lower") var languages: String = _
  @BeanProperty @Input (datatype = "owner") var domains: String = _
}

class RemoveBannerVO {
  @BeanProperty @Input (datatype = "owner") var bannerId: String = _
}

class TextVO {
  @BeanProperty @Input (datatype = "sites") var site: String = _

  @BeanProperty @Input var alt: String = _
  @BeanProperty @Input var title: String = _
  @BeanProperty @Input var text: String = _
}

class OptionVO {
  @BeanProperty var `type`: String = _
  @BeanProperty @Input var value: String = _
}

class EditFileVO {
  @BeanProperty @Input (datatype = "owner") var bannerId: String = _
}
