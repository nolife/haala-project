package com.coldcore.haala
package core
package banner

import scala.beans.BeanProperty

/** JSON setting BannerParams */
class Params {
  @BeanProperty var banner: String = _
}
