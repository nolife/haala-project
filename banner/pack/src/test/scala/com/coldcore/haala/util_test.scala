package com.coldcore.haala
package service
package banner

import org.junit.runner.RunWith
import org.scalatest.junit.JUnitRunner
import org.scalatest.{BeforeAndAfter, WordSpec}
import org.scalatest.mockito.MockitoSugar
import com.coldcore.haala.service.banner.Util._

@RunWith(classOf[JUnitRunner])
class UtilSpec extends WordSpec with MockitoSugar with BeforeAndAfter {

  "packLabel" should {
    "return correct label" in {
      assertResult("banner.ba.AAFF.%") { packLabel("ba", "AAFF", "%") }
    }
  }

  "packPath" should {
    "return correct file path" in {
      assertResult("banner/ba/AAFF/%") { packPath("ba", "AAFF", "%") }
    }
  }

}
