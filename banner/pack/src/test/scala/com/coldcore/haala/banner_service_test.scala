package com.coldcore.haala
package service
package banner

import domain.banner.{Banner, BannerPack}
import SolrService.SolrQuery
import org.scalatest.{BeforeAndAfter, FlatSpec, Matchers}
import org.scalatest.mockito.MockitoSugar
import org.mockito.Mockito._
import org.mockito.ArgumentMatchers._
import org.mockito.ArgumentCaptor
import dao.GenericDAO
import core.VirtualPath.Implicits._
import org.junit.runner.RunWith
import org.scalatest.junit.JUnitRunner

@RunWith(classOf[JUnitRunner])
class BannerServiceSpec extends FlatSpec with MockitoSugar with BeforeAndAfter with Matchers {
  var m: MyMock = _

  before {
    m = new MyMock
  }

  class MyMock extends com.coldcore.haala.service.test.MyMock {
    val bannerDao = mock[GenericDAO[Banner]]
    val service = new BannerServiceImpl |< { x =>
      x.serviceContainer = serviceContainer
      x.bannerDao = bannerDao
    }
  }

  it should "delete banners" in {
    val banner = new Banner { id = 12L; ref = "MOCK12"; pack = new BannerPack }
    when(m.bannerDao.findById(12)).thenReturn(Some(banner))
    m.service.delete(12)
    verify(m.labelService).deleteByKeyLike("banner.ba.MOCK12.%")
    verify(m.fileService).deleteByFolder("banner/ba/MOCK12/*")
    verify(m.bannerDao).delete(banner)
  }

  it should "fetch banners" in {
    val banner1 = new Banner { id = 1L; ref = "REF1"; meta = "languages=en" }
    val banner2 = new Banner { id = 2L; ref = "REF2"; meta = "languages=ru" }
    val banner3 = new Banner { id = 3L; ref = "REF3"; meta = "position=1" }
    val banner4 = new Banner { id = 4L; ref = "REF4"; }
    val banner5 = new Banner { id = 5L; ref = "REF5"; meta = "position=3" }

    when(m.settingService.querySystem("Sites")).thenReturn("xx1=xx1:en, xx2=xx1.xx2:ru|rs")

    //no banners
    val solrResult = new SearchResult[SolrService.SolrDoc](0, Nil)
    when(m.solrService.searchWT(any[SolrQuery], any)).thenReturn(solrResult)
    when(m.solrService.fetchObjectsWT(solrResult, m.bannerDao, classOf[Banner])).thenReturn(SearchResult(0, List.empty[Banner]))
    assertResult(Nil) { m.service.fetch(8, 1 :: Nil, 4) }

    //plenty of banners: not repeating fixed nor float
    when(m.solrService.fetchObjectsWT(solrResult, m.bannerDao, classOf[Banner])).thenReturn(
      SearchResult(0, banner2 :: banner5 :: banner1 :: banner4 :: banner3 :: Nil))
    var x = m.service.fetch(8, 1 :: Nil, 4)
    assertResult(4) { x.size }
    assertResult(("REF3", "REF5")) { (x(0).ref, x(2).ref) }
    (List.empty[String] /: x){ (a, b) => assert(!a.contains(b.ref)); b.ref :: a } //assert different rem.fileService
    assert(List("REF1", "REF2", "REF4").contains(x(1).ref))
    assert(List("REF1", "REF2", "REF4").contains(x(3).ref))

    //not enough banners, repeating fixed
    when(m.solrService.fetchObjectsWT(solrResult, m.bannerDao, classOf[Banner])).thenReturn(
      SearchResult(0, banner5 :: banner3 :: Nil))
    x = m.service.fetch(8, 1 :: Nil, 4)
    assertResult(4) { x.size }
    assertResult(("REF3", "REF5")) { (x(0).ref, x(2).ref) }

    //not enough banners, repeating float
    when(m.solrService.fetchObjectsWT(solrResult, m.bannerDao, classOf[Banner])).thenReturn(
      SearchResult(0, banner2 :: banner1 :: Nil))
    x = m.service.fetch(8, 1 :: Nil, 4)
    assertResult(4) { x.size }
    assertResult(Set("REF1", "REF2")) { Set(x(0).ref, x(1).ref) }

    //not enough banners, repeating float and fixed
    when(m.solrService.fetchObjectsWT(solrResult, m.bannerDao, classOf[Banner])).thenReturn(
      SearchResult(0, banner3 :: banner1 :: Nil))
    x = m.service.fetch(8, 1 :: Nil, 4)
    assertResult(4) { x.size }
    assertResult(("REF3", "REF1")) { (x(0).ref, x(1).ref) }
  }

}
