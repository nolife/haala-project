if (typeof dwr == 'undefined' || dwr.engine == undefined) throw new Error('You must include DWR engine before including this file');

(function() {
  if (dwr.engine._getObject("BannerF") == undefined) {
    var p;

    p = {};
    p._path = '/dwr';

    p.remove = function(p0, callback) {
      return dwr.engine._execute(p._path, 'BannerF', 'remove', arguments);
    };

    p.search = function(p0, callback) {
      return dwr.engine._execute(p._path, 'BannerF', 'search', arguments);
    };

    p.filter = function(p0, callback) {
      return dwr.engine._execute(p._path, 'BannerF', 'filter', arguments);
    };

    p.create = function(p0, callback) {
      return dwr.engine._execute(p._path, 'BannerF', 'create', arguments);
    };

    p.update = function(p0, callback) {
      return dwr.engine._execute(p._path, 'BannerF', 'update', arguments);
    };

    p.editFile = function(p0, callback) {
      return dwr.engine._execute(p._path, 'BannerF', 'editFile', arguments);
    };

    dwr.engine._setObject("BannerF", p);
  }
})();
