
(function($) {

  Haala.Misc.onDocReady(function() {
    initAddForm();
  });



  var initAddForm = function() {
    $("#page-form .submit button").button();

    var validateType = function() {
      return $("#page-form .type input:checked").val() ? null : "require";
    };

    var finp = Haala.Util.panelFormInput;
    var panel = $("#page-form").kdxPanel(
      $.extend(true, {}, Haala.KdxPanel.Integ.DefaultOptions, {
        input: [
          finp("type", { userFn: validateType })
        ]
      }));

    Haala.Util.formSubmitPanel(panel, function(/*json*/ container) {
      var command = { type: $("#page-form .type input:checked").val() };
      addBanner(command);
    });

    var addBanner = function(/*json*/ command) {
      Haala.AJAX.call("body", BannerF.create, command,
        function(data) { document.location = "edit.htm?ref="+data.result; return Haala.AJAX.DO_NOTHING; });
    };
  };

})(jQuery);
