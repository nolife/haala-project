<#import "/web/common.ftl" as c >
<#import "/web/common-htm.ftl" as htm >
<#include "/web/macros.ftl:"+currentSite >

<@htm.form id="page-form">
  <div class="form-panel">
    <div class="cell type">
      <label><@label "003"/></label>
      <div class="inline">
        <input type="radio" name="type" value="1"/><p><@label "banner.type1"/></p>
        <input type="radio" name="type" value="2"/><p><@label "banner.type2"/></p>
      </div>
      <div class="nofloat"></div>
      <p class="error"></p>
    </div>
    <div class="cell submit">
      <button><@label "btn.save"/></button>
    </div>
  </div>
</@htm.form>

<@htm.nodiv "quick-navigation-content">
  <a href="main.htm"><@label "004"/></a>
</@>
