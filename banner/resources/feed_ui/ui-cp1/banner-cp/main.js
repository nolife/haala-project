
(function($) {

  Haala.Misc.onDocReady(function() {
    initNavigation();
    initBannerList();
  });



  var initNavigation = function() {
    $("#context-navigation").kdxMenu();
  };




  var initBannerList = function() {
    var ivars = Haala.Misc.evalJSON("#page-ivars");

    var listModel = {
      sort: {
        items: [
          { element: $("#banner-list .head .ref"), field: "ref" }
        ]
      },
      loadItemsFn: function(/*json*/ model, /*fn*/ callback) {
        Haala.AquaList.Integ.loadItemsFn(BannerF.search, { target: 1 }, model, callback);
      },
      rowElementFn: function(/*json*/ vo, /*int*/ position, /*json*/ model) {
        var domains = "";
        if (vo) {
          for (var z = 0; z < vo.domains.length; z++)
            domains += ", "+vo.domains[z];
          if (domains) domains = domains.substring(2, domains.length);
        }

        return vo ?
          $("<div class='data'>" +
            "  <a href='edit.htm?id="+vo.id+"'>" +
            "    <p class='column-1'>"+vo.ref+"</p>" +
            "    <p class='column-2'>"+domains+"</p>" +
            "    <p class='column-3'>"+vo.url+"</p>" +
            "  </a>" +
            "</div>").aclick(function() {
                        document.location = "edit.htm?ref="+vo.ref;
                      }) :
          $("<div class='empty'></div>");
      }
    };
    var list = $("#banner-list .rows").aquaList(
      $.extend(true,
        Haala.AquaList.Integ.DefaultOptions,
        Haala.AquaList.Integ.parseState($("#banner-list").attr("data-state")),
        listModel));
    list.setPageSpread($("#banner-list .page-spread").pageSpread(Haala.PageSpread.Integ.DefaultOptions));

    $("#banner-list .filter .on").aclick(function() {
      var command = { target: 1, params: [$("#banner-list .filter input").val()] };
      Haala.AJAX.call("body", BannerF.filter, command,
        function(data) { list.page(1); });
    });
    $("#banner-list .filter .off").aclick(function() {
      var command = { target: 1, params: [] };
      Haala.AJAX.call("body", BannerF.filter, command,
        function(data) {
          $("#banner-list .filter input").val("");
          list.page(1);
        });
    });

  };

})(jQuery);
