<#import "/web/common.ftl" as c >
<#import "/web/common-htm.ftl" as htm >
<#include "/web/macros.ftl:"+currentSite >
<#include "/web/banner-macros.ftl:"+currentSite >

<#function x_hasDomain domainId>
  <#list banner.domains() as d>
    <#if d.id()==domainId><#return true></#if>
  </#list>
  <#return false>
</#function>

<#assign TYPE_PICTURE=1 TYPE_TEXT=2 />

<#if banner??>

  <@htm.form id="page-form" mode="upload">
    <div class="form-panel">
      <div class="cell domains">
        <label><@label "002"/></label>
        <@h.listUserDomains ; x >
          <p><input data-id="${x.id()}" type="checkbox" <#if x_hasDomain(x.id()) > checked="true" </#if> /><span>${x.domain()}</span></p>
        </@>
      </div>
      <div class="two-column">
        <div class="cell left status">
          <label><@label "008"/></label>
          <select>
            <option value="0" <#if banner.active()==0 > selected="true" </#if> ><@label "016"/></option>
            <option value="1" <#if banner.active()==1 > selected="true" </#if> ><@label "017"/></option>
          </select>
          <p class="error"></p>
        </div>
        <div class="cell right expires">
          <label><@label "009"/></label>
          <input type="text" maxlength="100" value="<@h.timestamp ts=banner.expires() format="dd/MM/yyyy" />" />
          <p class="error"></p>
        </div>
      </div>
      <div class="cell url">
        <label><@label "006"/></label>
        <input type="text" maxlength="500" value="${banner.url()!}"/>
        <p class="error"></p>
      </div>
      <div class="cell text">
        <label><@label key="003"/></label>
        <textarea><@bannerPack.bannerProperty property="txt" banner=banner /></textarea>
        <p class="error"></p>
      </div>
      <#if banner.type()==TYPE_PICTURE >
        <div class="cell alt">
          <label><@label "004"/></label>
          <textarea><@bannerPack.bannerProperty property="alt" banner=banner /></textarea>
          <p class="error"></p>
        </div>
        <div class="cell title">
          <label><@label "005"/></label>
          <textarea><@bannerPack.bannerProperty property="ttl" banner=banner /></textarea>
          <p class="error"></p>
        </div>
        <div class="file-upload">
          <div class="cell">
            <label><@label "011"/></label>
            <input name="dat1" type="file" size="20"/>
            <span data-clear="dat1" title="<@label "012"/>" ></span>
          </div>
        </div>
        <div class="cell picture">
          <img src=""/>
        </div>
      </#if>
      <#--<h3 class="cell">&nbsp;</h3>-->
      <div class="cell zone">
        <label><@label "007"/></label>
        <@bannerZoneOptionRow banner />
      </div>

      <div class="cell position">
        <@bannerPack.bannerProperty var="value" property="position" banner=banner />
        <label><@label "023"/></label>
        <input type="text" maxlength="10" <#if (value?number > 0) > value="${value}" </#if> />
        <p class="error"></p>
      </div>
      <div class="two-column">
        <div class="cell target">
          <@bannerPack.bannerProperty var="value" property="window" banner=banner />
          <label><@label "018"/></label>
          <select>
            <option value="0" <#if value!="new" > selected="true" </#if> ><@label "019"/></option>
            <option value="1" <#if value=="new" > selected="true" </#if> ><@label "020"/></option>
          </select>
          <p class="error"></p>
        </div>
      </div>
      <div class="cell language">
        <@bannerPack.bannerProperty var="value" property="languages" banner=banner />
        <label><@label "021"/></label>
        <@h.listInputSites ; site >
          <@h.siteLocale var="lang" key=site />
          <input type="checkbox" class="lang-${site}" data-lang="${lang}" <#if c.csvContains(lang, value!) > checked="true" </#if> />
          <div class="flag ${lang}"></div>
        </@>
        <div class="nofloat"></div>
        <p class="error"></p>
      </div>
      <div class="cell reciprocal">
        <label><@label "010"/></label>
        <input type="text" maxlength="500" value="${banner.reciprocal()!}" />
        <p class="error"></p>
      </div>
      <div class="cell submit">
        <button><@label "btn.save"/></button>
      </div>
    </div>
  </@>

  <@htm.nodiv "context-navigation-content">
    <a class="delete" href="#" title="<@label "014"/>"><@label "013"/></a>
  </@>

  <@htm.nodiv "quick-navigation-content">
    <a href="main.htm"><@label "022"/></a>
  </@>

  <@htm.nodiv "page-messages">
    <div data-key="confirm-delete"><@label "015"/></div>
  </@>

  <@htm.nodiv "page-ivars">
    { bannerId: ${banner.id()}, type: ${banner.type()}, filepath: "<@bannerPack.bannerFilePath banner=banner />" }
  </@>

<#else>

  <@label "banner.4"/>

</#if>
