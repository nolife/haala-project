<#import "/web/common.ftl" as c >
<#import "/web/common-htm.ftl" as htm >
<#include "/web/macros.ftl:"+currentSite >

<div id="banner-list" class="item-list" data-state="${perPage},${searchState!}">
  <div class="filter">
    <@h.searchFilterParam var="tmpval" index=1 />
    <input type="text" value="${tmpval!}">
    <span class="on" title="<@label "013"/>" ></span>
    <span class="off" title="<@label "014"/>" ></span>
  </div>
  <div class="head">
    <a href="#" class="column-1 ref"><@label key="002"/><span class="arrow"></span></a>
    <p class="column-2"><@label key="004"/></p>
    <p class="column-3"><@label key="003"/></p>
  </div>
  <div class="rows"></div>
  <div class="foot">
    <div class="page-spread"></div>
  </div>
</div>

<@htm.nodiv "context-navigation-content">
  <a class="add" href="add.htm" title="<@label "010"/>"><@label "008"/></a>
</@>

<#--
<div class="banners">
  <@bannerPack.bannerZone opts="1" max=4 />
</div>
-->
