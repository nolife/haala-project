
(function($) {

  Haala.Misc.onDocReady(function() {
    initEditForm();
    initNavigation();
  });



  var initNavigation = function() {
    var ivars = Haala.Misc.evalJSON("#page-ivars");
    var msgFn = function(/*string*/ msg, /*json*/ tokens) {
      return Haala.Misc.messageMapping("#page-messages", msg, tokens);
    };

    $("#context-navigation").kdxMenu({
      items: [
        { element: "#context-navigation .delete",
            gotoFn: function(/*json*/ item, /*json*/ model) {
              Haala.Messages.showYN(msgFn("confirm-delete"), "title-confirm", function() {
                Haala.AJAX.call("body", BannerF.remove, { bannerId: ivars.bannerId },
                  function() { document.location = "main.htm"; });
              });
            }}
      ]
    });
  };



  var initEditForm = function() {
    var ivars = Haala.Misc.evalJSON("#page-ivars");
    if (!ivars) return;

    $("#page-form .submit button").button();

    /* picture banner */
    if (ivars.type == 1) {
      var refreshPicture = function() {
        var img = $("#page-form .picture img");
        img.attr("src", "/bin.htm?path="+ivars.filepath+"&nc=1&salt="+Math.random());
      };
      refreshPicture();
    }

    var finp = Haala.Util.panelFormInput;
    var input = [
      finp("status"),
      finp("url",      { require: true }),
      finp("position", { regex: ["number"] }),
      finp("expires",  { regex: ["date"] }),
      finp("reciprocal"),
      finp("target")
    ];

    var panel = $("#page-form").kdxPanel(
      $.extend(true, {}, Haala.KdxPanel.Integ.DefaultOptions, {
        input: input
      }));

    var submitHandler = function() {
      if (panel.validate()) {
        var container = panel.collect();
        var command = Haala.Misc.collectInput(container,
                ["status", "expires", "url", "position", "reciprocal", "target"],
                ["active", "expires", "url", "position", "reciprocal", "newWindow"]);
        command.bannerId = ivars.bannerId;

        command.domains = "";
        $("#page-form .domains input:checked").each(function() {
          command.domains += ","+$(this).attr("data-id");
        });
        if (command.domains) command.domains = command.domains.substring(1);

        command.options = [];
        $("#page-form .zone input:checked").each(function() {
          command.options.push({ type: $(this).parent(".option").attr("data-type") });
        });

        command.text = [];
        command.text.push({
          site: Haala.Misc.systemVar("inputSites")[0], //not multilingual
          text: $("#page-form .text textarea").val(),
          alt: $("#page-form .alt textarea").val(),
          title: $("#page-form .title textarea").val()
        });

        command.languages = "";
        $("#page-form .language input:checked").each(function() {
          command.languages += ","+$(this).attr("data-lang");
        });
        if (command.languages) command.languages = command.languages.substring(1);

        editBanner(command);
      } else {
        Haala.Util.errorToViewport();
      }
      Haala.Code.stretch();
    };

    var uploadForm = initEmbeddedUploadForm(submitHandler);
    $("#page-form .submit button").click(function(event) {
      event.preventDefault();
      if (!uploadForm.hasFiles()) submitHandler(); //call submit handler without upload
      else {
        if (panel.validate()) uploadForm.upload(); //upload then call submit handler
        else Haala.Util.errorToViewport();
      }
      Haala.Code.stretch();
    });

    var editBanner = function(/*json*/ command) {
      Haala.AJAX.call("body", BannerF.update, command,
        function() { document.location = document.location.href; return Haala.AJAX.DO_NOTHING; });
    };

    $("#page-form .expires input").datepicker({ dateFormat: 'dd/mm/yy' });
  };

  var initEmbeddedUploadForm = function(/*fn*/ successFn) {
    var ivars = Haala.Misc.evalJSON("#page-ivars");

    var form = $("#page-form").uploadForm(
      $.extend(true, {}, Haala.UploadForm.Integ.DefaultOptions, {
        uploadSuccessFn: function(/*json*/ model) {
          Haala.AJAX.call(null, BannerF.editFile, { bannerId: ivars.bannerId },
            function() { successFn(); }); //using body will prevent successFn from making AJAX call
        }
      }));
    return form;
  };

})(jQuery);
