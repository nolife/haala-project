
<#macro bannerOptionRow banner group >
  <div class="option-row">
    <@bannerPack.listBannerOptionTypes banner=banner group=group ; type >
      <div class="option" data-type="${type}">
        <@bannerPack.bannerHasOption var="has" banner=banner type=type />
        <@bannerPack.bannerOptionValue var="oval" banner=banner type=type />
        <@bannerPack.bannerOptionMode var="mode" type=type />
        <input type="checkbox" <#if has > checked="true" </#if> />
        <p><@bannerPack.bannerOptionKey type=type /></p>
        <#if mode==1 || mode==2 >
          <input type="text" value="${oval}"/>
        </#if>
      </div>
      <#local typesNotEmpty=true />
    </@>
    <#if typesNotEmpty!false ><div class="nofloat"></div></#if>
  </div>
</#macro>

<#macro bannerZoneOptionRow banner >
  <div class="option-row">
    <@bannerPack.listBannerOptionTypes banner=banner group="zone" ; type >
      <div class="option" data-type="${type}">
        <@bannerPack.bannerHasOption var="has" banner=banner type=type />
        <input type="checkbox" <#if has > checked="true" </#if> />
        <p>${type}</p>
      </div>
    </@>
    <div class="nofloat"></div>
  </div>
</#macro>

