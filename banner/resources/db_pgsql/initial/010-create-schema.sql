-----------------------------------------------------------
--   TABLES
-----------------------------------------------------------

CREATE TABLE BANNER_banner_packs
(
  id bigint NOT NULL,
  CONSTRAINT BANNER_banner_packs_pkey PRIMARY KEY (id)
)
WITHOUT OIDS;




CREATE TABLE BANNER_banners
(
  id bigint NOT NULL,
  ref character varying(100) NOT NULL,
  active integer NOT NULL,
  url character varying(500),
  reciprocal character varying(500),
  "type" integer NOT NULL,
  expires bigint,
  meta character varying(500),
  pack_id bigint,
  CONSTRAINT BANNER_banners_pkey PRIMARY KEY (id)
)
WITHOUT OIDS;



CREATE TABLE BANNER_banner_options
(
  id bigint NOT NULL,
  "type" integer NOT NULL,
  "value" text,
  banner_id bigint,
  CONSTRAINT BANNER_banner_options_pkey PRIMARY KEY (id)
)
WITHOUT OIDS;



CREATE TABLE BANNER_banner_domain_map
(
  banner_id bigint NOT NULL,
  domain_id bigint NOT NULL
)
WITHOUT OIDS;










-----------------------------------------------------------
--   CONSTRAINTS
-----------------------------------------------------------

ALTER TABLE BANNER_banner_packs
  ADD CONSTRAINT BANNER_banner_pack_sub_fk FOREIGN KEY (id)
      REFERENCES packs (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION;



ALTER TABLE BANNER_banners
  ADD CONSTRAINT BANNER_banner_pack_fk FOREIGN KEY (pack_id)
      REFERENCES BANNER_banner_packs (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION;

ALTER TABLE BANNER_banners
  ADD CONSTRAINT BANNER_banners_ref_key UNIQUE(ref);



ALTER TABLE BANNER_banner_options
  ADD CONSTRAINT BANNER_banner_option_fk FOREIGN KEY (banner_id)
      REFERENCES BANNER_banners (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION;



ALTER TABLE BANNER_banner_domain_map
  ADD CONSTRAINT BANNER_domain_banner_fk FOREIGN KEY (banner_id)
      REFERENCES BANNER_banners (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION;

ALTER TABLE BANNER_banner_domain_map
  ADD CONSTRAINT BANNER_banner_domain_fk FOREIGN KEY (domain_id)
      REFERENCES domains (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION;









-----------------------------------------------------------
--   INDEXES
-----------------------------------------------------------

CREATE INDEX "BANNER_banners_ind1"
  ON BANNER_banners (ref);











-----------------------------------------------------------
--   SEQUENCES
-----------------------------------------------------------

CREATE SEQUENCE BANNER_banners_seq
  INCREMENT 1
  MINVALUE 1
  MAXVALUE 9223372036854775807
  START 1
  CACHE 1;



CREATE SEQUENCE BANNER_banner_options_seq
  INCREMENT 1
  MINVALUE 1
  MAXVALUE 9223372036854775807
  START 1
  CACHE 1;

