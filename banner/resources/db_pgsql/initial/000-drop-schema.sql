-----------------------------------------------------------
--   DROP
-----------------------------------------------------------

DROP TABLE BANNER_banner_domain_map CASCADE;

DROP TABLE BANNER_banner_options CASCADE;
DROP SEQUENCE BANNER_banner_options_seq;

DROP TABLE BANNER_banners CASCADE;
DROP SEQUENCE BANNER_banners_seq;

DROP TABLE BANNER_banner_packs CASCADE;

delete from packs where pack = 'banner';
