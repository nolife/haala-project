package com.coldcore.haala
package integration.test
package helper

import com.coldcore.haala.core.{CoreException, SLF4J, Log}
import com.coldcore.haala.core.HttpUtil._
import org.apache.http.HttpStatus
import com.google.gson.Gson
import scala.beans.BeanProperty

class Non200HttpCode extends CoreException

object HttpClient {
  class DwrReply {
    @BeanProperty var errorCodes: Array[Int] = _
    @BeanProperty var errorText: Array[String] = _
    @BeanProperty var failed: Boolean = _
  }
  class AnyDwrReply extends DwrReply { @BeanProperty var result: Any = _ }
  class StringDwrReply extends DwrReply { @BeanProperty var result: String = _ }
  case class Credentials(username: String, password: String)
}

class HttpClient extends DwrFacade {
  import HttpClient._
  import ContextHolder._
  val log: Log = new SLF4J

  var cookies: Map[String,String] = Map.empty
  var dwrSessionId: String = _
  var jSessionId: String = _
  var lastDwrReply: DwrReply = _
  var lastHttpCall: HttpCallSuccessful = _
  var ops = new HttpOps(this)
  var credentials: Credentials = _

  /** Reset variables */
  def reset {
    credentials = null
    cookies = Map.empty
    dwrSessionId = ""
    jSessionId = ""
  }

  /** Set up cookies and DWR operations */
  def setup {
    log.info("Setting up HTTP client")
    reset
    log.info(s"Base URL $baseUrl")
    retrieveCookies
    retrieveDwrSessionId

    log.info("Attempting change site operation")
    ops.changeSite()
    if (lastDwrReply.failed) throw new Exception("DWR reply failed, cannot continue")
    log.info("Successfully set up HTTP client")
  }

  def extractCookies(hcall: HttpCallSuccessful): Map[String,String] =
    hcall.rh.filter(_._1 == "Set-Cookie").map(_._2.takeWhile(';'!=).split("=")).map(x => x.head -> x.last).toMap

  def extractCookie(name: String, hcall: HttpCallSuccessful): String =
    extractCookies(hcall).find(_._1 == name).getOrElse(("",""))._2

  /** Set jSessionId and throw an exception if it eventually changes */
  def ensureSessionId(hcall: HttpCallSuccessful) {
    val jsid = extractCookie("JSESSIONID", hcall)
    if (jSessionId.safe == "" && jsid == "")
      throw new Exception("Session ID not set")
    if (jSessionId.safe == "" && jsid != "") {
      jSessionId = jsid
      log.info(s"Session ID set to $jSessionId")
    }
    if (jSessionId.safe != "" && jsid != "" && jSessionId != jsid)
      throw new Exception("Session ID changed "+jsid)
  }

  /** Required request headers */
  def requestHeaders: List[String] =
    "Set-Cookie" :: cookiesHeader ::
    "Referer" :: baseUrl ::
    "Host" :: httpHost :: Nil

  /** Set cookies and session ID */
  def retrieveCookies {
    val url = s"$baseUrl/blank.htm"
    httpGet(url, requestHeaders: _*) match {
      case x: HttpCallSuccessful if x.code == HttpStatus.SC_OK =>
        lastHttpCall = x
        cookies = extractCookies(x)
        log.info("Cookies: "+cookiesHeader)
        ensureSessionId(x)
      case x: HttpCallSuccessful =>
        lastHttpCall = x
        ensureSessionId(x)
        throw new Non200HttpCode
      case x: HttpCallFailed => throw x.error
    }
  }

  /** Make string content of a Cookie header */
  def cookiesHeader = cookies.map { case (k,v) => s"$k=$v" }.mkString("; ")

  def httpHost = context.config("base.url").dropWhile(':'!=).drop(3)

  def httpProtocol = context.config("base.url").takeWhile(':'!=)

  /** construct base URL by using properties */
  def baseUrl = httpProtocol+"://"+httpHost

  /** Set DWR session ID */
  def retrieveDwrSessionId {
    val facade = "__System.generateId"
    httpPost(s"$baseUrl/dwr/call/plaincall/$facade.dwr", dwrFacadeContent(facade, ""),
      "Content-Type" :: "text/javascript; charset=utf-8" :: requestHeaders: _*) match {
      case x: HttpCallSuccessful if x.code == HttpStatus.SC_OK =>
        lastHttpCall = x
        // dwr.engine.remote.handleCallback("0","0","hBiOctmzLh*YsWJKJUstco1V4Mk");
        dwrSessionId = x.body.substring(x.body.indexOf("handleCallback(")).split("\"")(5)
        if (dwrSessionId.size < 5) throw new Exception("DWR Session ID not set")
        log.info(s"DWR Session ID set to $dwrSessionId")
        cookies = cookies + ("DWRSESSIONID" -> dwrSessionId)
        ensureSessionId(x)
      case x: HttpCallSuccessful =>
        lastHttpCall = x
        ensureSessionId(x)
        throw new Non200HttpCode
      case x: HttpCallFailed => throw x.error
    }
  }

  def doGet(url: String) {
    log.info(s"GET $url")
    httpGet(url, requestHeaders: _*) match {
      case x: HttpCallSuccessful =>
        lastHttpCall = x
        ensureSessionId(x)
      case x: HttpCallFailed => throw x.error
    }
  }

}

/** Call DWR facades and create content to send to facades */
trait DwrFacade {
  self: HttpClient =>
  import HttpClient._

  /** Parse DWR facade reply */
  def parseDwrFacadeReply(body: String, clz: Class[_ <: DwrReply]) {
    // dwr.engine.remote.handleCallback("1","0",{errorCodes:[],errorsText:[],failed:false,result:null});
    val json = body.substring(body.indexOf("handleCallback(")).dropWhile('{'!=).takeWhile('}'!=)+"}"
    lastDwrReply = new Gson().fromJson(json, clz)
    if (lastDwrReply.failed) log.warn(s"DRW reply failed: $json")
    else log.info(s"DRW reply successful: $json")
  }

  /** Create full content for DWR call to use */
  def dwrFacadeContent(facade: String, content: String) =
    if (content.contains("scriptSessionId=")) content
    else
      s"""
        |c0-scriptName=${facade.split("\\.").head}
        |c0-methodName=${facade.split("\\.").last}
        |c0-id=0
        |${content.trim}
        |callCount=1
        |windowName=
        |nextReverseAjaxIndex=0
        |batchId=0
        |instanceId=0
        |page=%2F
        |scriptSessionId=$dwrSessionId/8hvU4Mk-UT3hV9OEx
      """.stripMargin.trim

  /** Create partial content for DWR call to send a single parameter */
  def dwrFacadeParamContent(param: String = ""): String = {
    /* MiscF.changeSite param content:
       c0-param0=string:$site */
    s"c0-param0=string:$param"
  }

  /** Create partial  content for DWR call to send a simple command fields */
  def dwrFacadeCommandContent(fields: Map[String,String]): String = {
    /* LoginF.login command content:
       c0-e1=string:7017
       c0-e2=string:${client.username}
       c0-e3=string:${client.password}
       c0-param0=Object_Object:{turing:reference:c0-e1, username:reference:c0-e2, password:reference:c0-e3} */
    var (e123, param0) = ("", "")
    fields.zipWithIndex.foreach { case ((k,v), n) =>
      e123 += s"c0-e${n+1}=string:$v\n"
      param0 += s"$k:reference:c0-e${n+1}, "
    }
    if (param0.nonEmpty) param0 = param0.dropRight(2)
    s"${e123}c0-param0=Object_Object:{$param0}"
  }

  def dwrFacadeCall(facade: String, content: String, clz: Class[_ <: DwrReply] = classOf[AnyDwrReply]) {
    log.info(s"Calling DWR facade $facade")
    httpPost(s"$baseUrl/dwr/call/plaincall/$facade.dwr", dwrFacadeContent(facade, content),
      "Content-Type" :: "text/javascript; charset=utf-8" :: requestHeaders: _*) match {
      case x: HttpCallSuccessful if x.code == HttpStatus.SC_OK =>
        lastHttpCall = x
        parseDwrFacadeReply(x.body, clz)
        ensureSessionId(x)
      case x: HttpCallSuccessful =>
        lastHttpCall = x
        ensureSessionId(x)
        throw new Non200HttpCode
      case x: HttpCallFailed => throw x.error
    }
  }

}

class HttpOps(client: HttpClient) {
  import HttpClient._
  import ContextHolder._
  val log: Log = new SLF4J

  def changeSite(site: String = "xx1") {
    log.info(s"Changing site to $site")
    client.dwrFacadeCall("MiscF.changeSite", client.dwrFacadeParamContent(site))
  }

  def generateCaptcha {
    log.info(s"Generating captcha")
    client.dwrFacadeCall("MiscF.turingPath", client.dwrFacadeParamContent(), classOf[StringDwrReply])
    log.info(s"Captcha picture URL: ${client.baseUrl}/${client.lastDwrReply.asInstanceOf[StringDwrReply].result}")
  }

  def login {
    log.info(s"Login as ${client.credentials.username} / ${client.credentials.password}")
    generateCaptcha
    val fields =
      Map("turing" -> "7017",
          "username" -> client.credentials.username,
          "password" -> client.credentials.password)
    client.dwrFacadeCall("LoginF.login", client.dwrFacadeCommandContent(fields), classOf[StringDwrReply])
    if (client.lastDwrReply.failed && client.lastDwrReply.errorCodes.contains(9))
      throw new Exception("Captcha ebabled, set 'Dev/NoCaptcha' setting to '1' to disable!")
  }

  def loginAdmin {
    client.credentials = Credentials(context.config("admin.username"), context.config("admin.password"))
    login
    if (client.lastDwrReply.failed) throw new Exception("Failed admin login")
  }

  def logout {
    log.info(s"Logout ${client.credentials.username}")
    client.dwrFacadeCall("LoginF.logout", client.dwrFacadeParamContent())
  }

  def getPage(page: String) {
    val url = s"${client.baseUrl}/${page.startsWith("/") ? page.drop(1) | page}"
    client.doGet(url)
  }

}
