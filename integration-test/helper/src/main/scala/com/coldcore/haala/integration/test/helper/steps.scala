package com.coldcore.haala
package integration.test
package helper

import cucumber.api.java.en._
import org.scalatest.Assertions
import com.coldcore.haala.integration.test.helper.HttpClient.Credentials

trait LoginSteps extends LoginStepsGiven {
  import ContextHolder._

  def httpClient = context.httpClient
}

trait LoginStepsGiven extends Assertions {
  self: LoginSteps =>

  @Given("^user credentials (.*) / (.*)$")
  def user_credentials(username: String, password: String) {
    httpClient.credentials = Credentials(username, password)
  }

  @Given("^login as (.*) / (.*)$")
  def login_as(username: String, password: String) {
    user_credentials(username, password)
    logged_in
  }

  @Given("^logged in$")
  def logged_in {
    httpClient.ops.login
    assertResult(false) { httpClient.lastDwrReply.failed }
  }

  @Given("^logged in as admin$")
  def logged_in_as_admin {
    httpClient.ops.loginAdmin
    assertResult(false) { httpClient.lastDwrReply.failed }
  }

  @Given("^logged out$")
  def logged_out {
    httpClient.ops.logout
    assertResult(false) { httpClient.lastDwrReply.failed }
  }

}

trait HttpSteps extends HttpStepsGiven with HttpStepsThen {
  import ContextHolder._

  def httpClient = context.httpClient
}

trait HttpStepsGiven extends Assertions {
  self: HttpSteps =>

  @Given("^http GET page (.*)$")
  def http_GET_page(page: String) {
    httpClient.ops.getPage(page)
  }

}

trait HttpStepsThen extends Assertions {
  self: HttpSteps =>

  @Then("^http response code (.*)$")
  def http_response_code(code: Int) {
    assertResult(code) { httpClient.lastHttpCall.code }
  }

  @Then("^http response is string (.*)$")
  def http_response_is_string(s: String) {
    assertResult(s) { httpClient.lastHttpCall.body }
  }

  @Then("^http response contains string (.*)$")
  def http_response_contains_string(s: String) {
    assertResult(true) { httpClient.lastHttpCall.body.toLowerCase.contains(s.toLowerCase) }
  }

}

trait FacadeSteps extends FacadeStepsThen {
  import ContextHolder._

  def httpClient = context.httpClient
}

trait FacadeStepsThen extends Assertions {
  self: FacadeSteps =>
  import HttpClient._

  private def dwrStringResult: String = {
    assertResult(classOf[StringDwrReply]) { httpClient.lastDwrReply.getClass }
    httpClient.lastDwrReply.asInstanceOf[StringDwrReply].result
  }

  @Then("^facade reply contains string (.*)$")
  def facade_reply_contains_string(s: String) {
    assertResult(true) { dwrStringResult.toLowerCase.contains(s.toLowerCase) }
  }

  @Then("^facade reply is string (.*)$")
  def facade_reply_is_string(s: String) {
    assertResult(s) { dwrStringResult }
  }

}

trait ApiSteps extends ApiStepsGiven {
  import ContextHolder._

  def httpClient = context.httpClient
}

trait ApiStepsGiven extends Assertions {
  self: ApiSteps =>

  @Given("^api dev features enabled$")
  def api_dev_features_enabled {
    httpClient.ops.getPage("/api/system-status")
    if (httpClient.lastHttpCall.code != 200)
      throw new Exception("API development features disabled, set 'Dev/ApiFeatures' setting to '1' to enable!")
  }

}

trait ConfigSteps extends ConfigStepsGiven with ConfigStepsThen

trait ConfigStepsGiven extends Assertions {
  self: ConfigSteps =>
  import ContextHolder._

  @Given("^set config property (.*) to value (.*)$")
  def set_config_property_to_value(key: String, value: String) {
    context.config.get(key) match {
      case Some(x) => context.config += key -> x
      case None => context.config += key -> value
    }
  }

  @Given("^set config property (.*) to string value (.*)$")
  def set_config_property_to_string_value(key: String, value: String) {
    context.config += key -> value
  }

  @Given("^set config property (.*) to property value (.*)$")
  def set_config_property_to_property_value(key: String, property: String) {
    context.config += key -> context.config(key)
  }

  @Given("^set config property (.*) to safe property value (.*)$")
  def set_config_property_to_safe_property_value(key: String, property: String) {
    context.config += key -> context.config.getOrElse(key, "")
  }

  @Given("^clear config property (.*)$")
  def clear_config_property(key: String) {
    context.config += key -> ""
  }

  @Given("^remove config property (.*)$")
  def remove_config_property(key: String) {
    context.config -= key
  }

}

trait ConfigStepsThen extends Assertions {
  self: ConfigSteps =>
  import ContextHolder._

  @Then("^config property (.*) exists$")
  def config_property_exists(key: String) {
    assertResult(true) { context.config.get(key).isDefined }
  }

  @Then("^config property (.*) does not exist$")
  def config_property_does_not_exist(key: String) {
    assertResult(false) { context.config.get(key).isDefined }
  }

  @Then("^config property (.*) is empty$")
  def config_property_is_empty(key: String) {
    assertResult("") { context.config.getOrElse(key, "") }
  }

  @Then("^config property (.*) is not empty$")
  def config_property_is_not_empty(key: String) {
    assertResult(true) { context.config.getOrElse(key, "") != "" }
  }

  @Then("^config property (.*) equals to string (.*)$")
  def config_property_equals_to_string(key: String, value: String) {
    assertResult(value) { context.config.getOrElse(key, "") }
  }

  @Then("^config property (.*) contains string (.*)$")
  def config_property_contains_string(key: String, value: String) {
    assertResult(true) { context.config.getOrElse(key, "").contains(value) }
  }

}
