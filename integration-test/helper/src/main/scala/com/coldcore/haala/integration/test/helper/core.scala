package com.coldcore.haala
package integration.test
package helper

import com.coldcore.misc5.CByte
import com.coldcore.haala.core.Constants._
import java.util.Properties
import java.io.StringReader
import com.coldcore.haala.core.{SLF4J, Log}
import scala.collection.JavaConverters._

object ContextHolder {
  var instance = new Context
  def context = instance
}

class Context {
  val log: Log = new SLF4J

  var config = new scala.collection.mutable.HashMap[String,String]
  var httpClient = new HttpClient

  def loadConfig(filename: String = "config.properties") {
    log.info(s"Loading config from $filename")
    val in = getClass.getResourceAsStream("/"+filename)
    val props = new Properties()
    props.load(new StringReader(new String(CByte.toByteArray(in), UTF8)))
    config ++= props.asScala
    log.info(s"Config has ${config.size} entries")
    log.info(config.map { case (k,v) => s"\t$k = $v\n" }.mkString)
  }
}
