package com.coldcore.haala
package domain

import annotations.{EqualsHashCode, Site, SiteGroup}
import java.io.Serializable
import javax.persistence._
import com.coldcore.misc.scala.ReflectUtil._
import beans.BeanProperty
import core.{Meta, Timestamp, VirtualPath}

/** With persistence annotation support (entity must provide a sequence generator) */
@MappedSuperclass
abstract class BaseObject extends Serializable {
  @Id @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "DEFAULT_GENERATOR")
  @BeanProperty @EqualsHashCode var id: JLong = _

  override def equals(a: Any): Boolean =
    Option(a).exists { _ =>
      try {
        val o = a.asInstanceOf[AnyRef]
        fieldsByAnnotation[EqualsHashCode](getClass).map(f => fieldValue(this, f) == fieldValue(o, f)).forall(true==)
      } catch { case _: Throwable => false }
    }

  override def hashCode: Int =
    fieldsByAnnotation[EqualsHashCode](getClass).foldLeft(0) { (r,f) =>
      Option(fieldValue(this, f)).map(v => 31*r+v.hashCode).getOrElse(r)
    }
}

/** Entry to store any data.
  * Performance: no database FK nor Indexes nor constraints.
  * Search and group: by REF and PACK name.
  */
class AbyssEntry extends BaseObject {
  @BeanProperty @EqualsHashCode var ref: String = _
  @BeanProperty @EqualsHashCode var pack: String = _
  @BeanProperty @EqualsHashCode var value: String = _
  @BeanProperty @EqualsHashCode var meta: String = _
  @BeanProperty @EqualsHashCode var created: JLong = Timestamp()
  @BeanProperty @EqualsHashCode var updated: JLong = Timestamp()
  @BeanProperty @EqualsHashCode var keep: JLong = _
}

class Address extends BaseObject {
  @BeanProperty @EqualsHashCode var line1: String = _
  @BeanProperty @EqualsHashCode var line2: String = _
  @BeanProperty @EqualsHashCode var line3: String = _
  @BeanProperty @EqualsHashCode var line4: String = _
  @BeanProperty @EqualsHashCode var line5: String = _
  @BeanProperty @EqualsHashCode var town: String = _
  @BeanProperty @EqualsHashCode var postcode: String = _
  @BeanProperty @EqualsHashCode var country: String = _
}

class EmailStorage extends BaseObject {
  @BeanProperty @EqualsHashCode var description: String = _
  @BeanProperty @EqualsHashCode var total: JInteger = 0
  @BeanProperty var items: JList[EmailStorageItem] = new JArrayList[EmailStorageItem]
}

class EmailStorageItem extends BaseObject {
  @BeanProperty @EqualsHashCode var value: String = _
  @BeanProperty @EqualsHashCode var status: JInteger = 0
  @BeanProperty var storage: EmailStorage = _
}

object EmailStorageItem {
  val STATUS_IDLE = 0
  val STATUS_PROCESSED = 1
  val STATUS_ERROR = 2
}

class Country extends BaseObject {
  @BeanProperty @EqualsHashCode var code: String = _

  def getTextKey = "loc.c"+id.asInstanceOf[Long].hex.toUpperCase
}

class Domain extends BaseObject {
  @BeanProperty @EqualsHashCode var domain: String = _
  @BeanProperty @EqualsHashCode var domainSite: String = _
  @BeanProperty @EqualsHashCode var `type`: String = _
  @BeanProperty @EqualsHashCode var meta: String = _

  def protocols: List[String] = Meta(meta)("protocol", "http").toLowerCase.parseCSV // https, http
  def aliases: List[String] = // example.org, www.example.org, foo.example.org
    domain :: Meta(meta)("cname", "").parseCSV.map(_+"."+domain)
}

object Domain {
  val TYPE_MAIN = "main" //main website (my-estate.eu)
  val TYPE_CONTROL_PANEL = "cpanel" //control panel website for agencies plus CMS (admin.my-estate.eu)
  val TYPE_OTHER = "other" //all other websites
}

class File extends BaseObject {
  @BeanProperty @EqualsHashCode var folder: String = _
  @BeanProperty @EqualsHashCode var name: String = _
  @BeanProperty var updated: JLong = Timestamp()
  @BeanProperty @EqualsHashCode var size: JLong = 0L
  @BeanProperty @EqualsHashCode @Site var site: String = _
  @BeanProperty @EqualsHashCode var meta: String = _

  @SiteGroup private val path: String = "" // @SiteGroup uses getter via reflection
  def getPath = VirtualPath(folder, name).path // ... (getter)
}

object File {
  val SUFFIX_DEFAULT = "d"
  val SUFFIX_ORIGINAL = "o"
}

class Label extends BaseObject {
  @BeanProperty @EqualsHashCode @SiteGroup var key: String = _
  @BeanProperty @EqualsHashCode var value: String = _
  @BeanProperty @EqualsHashCode @Site var site: String = _
}

class LocAddress extends BaseObject {
  @BeanProperty var country: Location = _
  @BeanProperty var region: Location = _
  @BeanProperty var subRegion: Location = _
  @BeanProperty var town: Location = _
  @BeanProperty var postcode: Postcode = _
}

class Location extends BaseObject {
  @BeanProperty @EqualsHashCode var `type`: JInteger = _
  @BeanProperty @EqualsHashCode var empty: JInteger = 0
  @BeanProperty @EqualsHashCode var index: JInteger = 0
  @BeanProperty var items: JList[Location] = new JArrayList[Location]
  @BeanProperty var postcodes: JList[Postcode] = new JArrayList[Postcode]
  @BeanProperty var parent: Location = _

  def getTextKey = "loc.t"+id.asInstanceOf[Long].hex.toUpperCase
}

object Location {
  val TYPE_COUNTRY = 1
  val TYPE_REGION = 2
  val TYPE_SUB_REGION = 3
  val TYPE_TOWN = 4
}

class GeoMap extends BaseObject {
  import GeoMap._
  @BeanProperty @EqualsHashCode var lat: JLong = NO_LAT_LNG // [-90..90] -33.4351709726486 x10,000,000,000,000,000
  @BeanProperty @EqualsHashCode var lng: JLong = NO_LAT_LNG // [-180..180] 152.0447568359375 x10,000,000,000,000,000
  @BeanProperty @EqualsHashCode var zoom: JInteger = _
  @BeanProperty @EqualsHashCode var lat2: JLong = NO_LAT_LNG
  @BeanProperty @EqualsHashCode var lng2: JLong = NO_LAT_LNG
}

object GeoMap {
  val NO_LAT_LNG = 200L*10000000000000000L
}

class Attachment extends BaseObject {
  @BeanProperty @EqualsHashCode var `type`: JInteger = _
  @BeanProperty @EqualsHashCode var position: JInteger = 0
  @BeanProperty var file: File = _
}

class Picture extends BaseObject {
  @BeanProperty @EqualsHashCode var `type`: JInteger = _
  @BeanProperty @EqualsHashCode var width: JInteger = _
  @BeanProperty @EqualsHashCode var height: JInteger = _
  @BeanProperty @EqualsHashCode var position: JInteger = 0
  @BeanProperty var file: File = _

  import Picture._
  def getLayout = {
    val w = width.asInstanceOf[Int]; val h = height.asInstanceOf[Int]
    if (w < h) LAYOUT_VERTICAL else if (w/h >= 2) LAYOUT_PANORAMA else LAYOUT_HORIZONTAL
  }
}

object Picture {
  val LAYOUT_HORIZONTAL = 1
  val LAYOUT_VERTICAL = 2
  val LAYOUT_PANORAMA = 3
}

class Postcode extends BaseObject {
  @BeanProperty @EqualsHashCode var `type`: JInteger = 1
  @BeanProperty @EqualsHashCode var value: String = _
  @BeanProperty @EqualsHashCode var index: JInteger = 0
  @BeanProperty var location: Location = _

  def getTextKey = "loc.p"+id.asInstanceOf[Long].hex.toUpperCase
}

object Postcode {
  val TYPE_REGULAR = 1
  val TYPE_GROUP = 2
}

class Role extends BaseObject {
  @BeanProperty @EqualsHashCode var role: String = _
  @BeanProperty @EqualsHashCode var description: String = _
}

object Role {
  val ANONYMOUS = "ANONYMOUS"
  val ADMIN = "ROLE_ADMIN"
  val USER = "ROLE_USER"
  val API_DEV = "API_DEV"

  def role(r: String): String = r.safe.toUpperCase match {
    case x if x.startsWith("ROLE_") => x
    case x @ (ANONYMOUS | "") => ANONYMOUS
    case API_DEV => API_DEV
    case x => "ROLE_"+x
  }
}

class Setting extends BaseObject {
  @BeanProperty @EqualsHashCode @SiteGroup var key: String = _
  @BeanProperty @EqualsHashCode var value: String = _
  @BeanProperty @EqualsHashCode @Site var site: String = _
}

class User extends BaseObject {
  @BeanProperty @EqualsHashCode var ref: String = _
  @BeanProperty @EqualsHashCode var username: String = _
  @BeanProperty @EqualsHashCode var password: String = _
  @BeanProperty @EqualsHashCode var salutation: JInteger = _
  @BeanProperty @EqualsHashCode var firstName: String = _
  @BeanProperty @EqualsHashCode var lastName: String = _
  @BeanProperty @EqualsHashCode var created: JLong = Timestamp()
  @BeanProperty @EqualsHashCode var activated: JLong = _
  @BeanProperty @EqualsHashCode var lastLogin: JLong = _
  @BeanProperty @EqualsHashCode var email: String = _
  @BeanProperty @EqualsHashCode var primaryNumber: String = _
  @BeanProperty @EqualsHashCode var ticket: String = _
  @BeanProperty @EqualsHashCode var token: String = _
  @BeanProperty @EqualsHashCode var credit: JLong = 0L //x100
  @BeanProperty @EqualsHashCode var meta: String = _
  @BeanProperty var address: Address = _
  @BeanProperty var roles: JList[Role] = new JArrayList[Role]
  @BeanProperty var domains: JList[Domain] = new JArrayList[Domain]
  @BeanProperty var packs: JList[Pack] = new JArrayList[Pack]

  def hasPack(pack: String): Boolean = packs.exists(_.pack == pack)
  def getPack[T <: Pack](pack: String): T = packs.find(_.pack == pack).get.asInstanceOf[T]
  def addPack[T <: Pack](pack: String)(f: => T) = if (!hasPack(pack)) packs.add(f)
}

object User {
  val SALUTATION_MR = 1
  val SALUTATION_MS = 2
  val SALUTATION_MRS = 3
  val SALUTATION_OTHER = 4
}

/** With persistence annotation support (entity subclass must provide a join column) */
@Entity
@Inheritance(strategy = InheritanceType.JOINED)
class Pack extends BaseObject {
  @BeanProperty @EqualsHashCode var pack: String = _
  @BeanProperty var user: User = _
}

/** Extend and use a in separate table (similar to BannerOption) */
abstract class BaseOption(val pkey: String) extends BaseObject {
  @BeanProperty @EqualsHashCode var `type`: JInteger = _
  @BeanProperty @EqualsHashCode var value: String = _

  val MODE_SIMPLE = 0 //option is either on or off
  val MODE_STRING_VALUE = 1 //option has text value
  val MODE_INT_VALUE = 2 //option has integer value

  /** Return option value-key or text-key */
  def getValueKey: String = getMode(`type`.asInstanceOf[Int]) match {
    case x if x > MODE_SIMPLE => s"$pkey.v${`type`}" // bannerBaOpt.v10
    case x => getTextKey // bannerBaOpt.t10
  }

  /** Return option text-key */
  def getTextKey: String = s"$pkey.t${`type`}" // bannerBaOpt.t10

  /** Map option to its mode */
  def getMode(): Int = getMode(`type`)

  /** Map option to its mode (static-like method) */
  def getMode(typ: Int): Int = typ match { //overwrite this to implement proper matching
    case _ => MODE_SIMPLE
  }

  /** Return option text-key (static-like method) */
  def getTextKeyByType(typ: Int): String = s"$pkey.t$typ" // bannerBaOpt.t10
}

/** Proprietary option rules:
  *   reside in a separate table "banner_options"
  *   mapped to "BannerOption"
  *   linked as "with BaseOptionObject[BannerOption]"
  */
trait BaseOptionObject[T <: BaseOption] {
  @BeanProperty var options: JList[T] = new JArrayList[T]
}

trait BasePackObject[P <: Pack] {
  @BeanProperty var pack: P = _
}
