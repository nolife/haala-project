package com.coldcore.haala
package domain

import annotations.EqualsHashCode
import org.scalatest.{BeforeAndAfter, WordSpec}
import java.util.Date

import com.coldcore.haala.core.Timestamp

import beans.BeanProperty
import org.junit.runner.RunWith
import org.scalatest.junit.JUnitRunner
import org.scalatest.mockito.MockitoSugar

@RunWith(classOf[JUnitRunner])
class DotDirectiveSpec extends WordSpec with MockitoSugar with BeforeAndAfter {

  class MyObject extends BaseObject {
    @BeanProperty @EqualsHashCode var name: String = _
    @BeanProperty var updated: JLong = Timestamp()
  }

  class OtherObject extends BaseObject {
    @BeanProperty @EqualsHashCode var name: String = _
    @BeanProperty var updated: JLong = Timestamp()
  }

  "equals" should {
    "compare objects" in {
      val (my1, my2) = (new MyObject, new MyObject)
      val pastDate = Timestamp.fromMills(System.currentTimeMillis-1000L*60L*60L*24L)
      assertResult(false) { my1 == null }
      assertResult(true) { my1 == my2 }
      my2.name = "name-2"
      assertResult(false) { my1 == my2 }
      my1.name = "name-1"
      assertResult(false) { my1 == my2 }
      my1.name = "name-2"
      assertResult(true) { my1 == my2 }
      my2.updated = pastDate
      assertResult(true) { my1 == my2 }
      my2.id = 10L
      assertResult(false) { my1 == my2 }
      my1.id = 11L
      assertResult(false) { my1 == my2 }
      my1.id = 10L
      assertResult(true) { my1 == my2 }
      assertResult(false) { my1 == new OtherObject { name = my1.name; updated = my1.updated }}
      assertResult(false) { my1 == "void" }
      assertResult(true) { my1 == my1 }
    }

    "compare objects with null fields" in {
      val (my1, my2) = (new MyObject { updated = null }, new MyObject { updated = null })
      assertResult(true) { my1 == my2 }
      my1.name = "a"; my2.name = "a"
      assertResult(true) { my1 == my2}
      my1.name = null; my2.name = "a"
      assertResult(false) { my1 == my2}
      my1.name = "a"; my2.name = null
      assertResult(false) { my1 == my2}
    }
  }

  "hashcode" should {
    "hash object" in {
      val my1 = new MyObject
      val pastDate = Timestamp.fromMills(System.currentTimeMillis-1000L*60L*60L*24L)
      assertResult(0) { my1.hashCode }
      my1.updated = pastDate
      assertResult(0) { my1.hashCode }
      my1.name = "name-1"
      assertResult(true) { my1.hashCode != 0 }
      my1.name = null; my1.id = 10L
      assertResult(true) { my1.hashCode != 0 }

      my1.name = "name-1"; my1.id = 10L
      assertResult(1721902203) { my1.hashCode }
    }
  }

}
