package com.coldcore.haala
package web.tag.declare
package domain

import beans.BeanProperty
import web.tag.logic.State
import web.tag.logic.domain.OnTypeTag

class OnTypeTagDlg extends TagDlg[OnTypeTag] {
  @BeanProperty var `type`: String  = _

  override def reset { `type` = null }

  import OnTypeTag._
  override def state: State = new ExtState { pctx = pageContext; attrs = Attrs(`type`) }
}
