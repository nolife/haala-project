package com.coldcore.haala
package web.tag.declare
package cms

import beans.BeanProperty
import web.tag.logic.State
import web.tag.logic.cms.{EnabledTag, EditTag}

class EditTagDlg extends TagDlg[EditTag] {
  @BeanProperty var data: String = _
  @BeanProperty var params: String = _

  override def reset { data = null; params = null }

  import EditTag._
  override def state: State = new ExtState { pctx = pageContext; attrs = Attrs(data, params) }
}


class EnabledTagDlg extends TagDlg[EnabledTag] {
  @BeanProperty var fn: String = _

  override def reset { fn = null }

  import EnabledTag._
  override def state: State = new ExtState { pctx = pageContext; attrs = Attrs(fn) }
}
