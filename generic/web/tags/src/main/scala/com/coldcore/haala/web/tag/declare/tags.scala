package com.coldcore.haala
package web.tag.declare

import org.springframework.web.servlet.tags.RequestContextAwareTag
import reflect.{ClassTag, classTag}
import beans.BeanProperty
import web.tag.logic._

/** Tag declared in TLD file which delegates calls to its target */
abstract class TagDlg[T <: BaseTag : ClassTag] extends RequestContextAwareTag {
  private var _state: State = _
  private var _target: T = _

  /** Reset variables to initial values */
  def reset {}

  /** Create state which will be passed into start and end methods */
  def state: State = new State { pctx = pageContext }

  private def target: T = {
    if (_target == null) _target = getRequestContext.getWebApplicationContext.getBean(classTag[T].runtimeClass).asInstanceOf[T]
    _target
  }

  override def doStartTagInternal: Int = {
    _state = state; _target = target
    _target.doStart(_state)
  }

  override def doEndTag: Int = {
    val r = _target.doEnd(_state)
    reset
    r
  }
}

abstract class PrintSetTagDlg[T <: BaseTag : ClassTag] extends TagDlg[T] {
  @BeanProperty var `var`: String = _
  @BeanProperty var scope: String = _
  @BeanProperty var action: String = _
  @BeanProperty var params: String = _
  @BeanProperty var site: String = _
  @BeanProperty var bydefault: Any = _

  override def reset { bydefault = null; scope = null; `var` = null; action = null; params = null; site = null }

  import BasePrintSetTag._
  override def state: State =
    new ExtState { pctx = pageContext; attrs = Attrs(`var`, scope, action, params, site, bydefault) }
}

class OnEntryTagDlg extends TagDlg[OnEntryTag] {
  @BeanProperty var value: String = _
  @BeanProperty var params: String = _

  override def reset { value = null; params = null }

  import OnEntryTag._
  override def state: State = new ExtState { pctx = pageContext; attrs = Attrs(value, params) }
}

class DotTagDlg extends TagDlg[DotTag]

class LayoutTagDlg extends TagDlg[LayoutTag] {
  @BeanProperty var path: String = _
  @BeanProperty var sfx: String = _

  override def reset { path = null; sfx = null }

  import LayoutTag._
  override def state: State = new ExtState { pctx = pageContext; attrs = Attrs(path, sfx) }
}

class OnPageTagDlg extends TagDlg[OnPageTag] {
  @BeanProperty var index: String = _

  override def reset { index = null }

  import OnPageTag._
  override def state: State = new ExtState { pctx = pageContext; attrs = Attrs(index) }
}

class UtilTagDlg extends PrintSetTagDlg[UtilTag] {
  @BeanProperty var value: AnyRef = _

  override def reset { value = null }

  import UtilTag._
  override def state: State = {
    val x = super.state.asInstanceOf[BasePrintSetTag.ExtState]
    new ExtState { pctx = x.pctx; attrs = x.attrs; vars = Vars(value) }
  }
}

class UrlTagDlg extends PrintSetTagDlg[UrlTag] {
  @BeanProperty var path: String = _
  @BeanProperty var nc: String = _
  @BeanProperty var salt: String = _

  override def reset { super.reset; nc = null; salt = null; path = null }

  import UrlTag._
  override def state: State = {
    val x = super.state.asInstanceOf[BasePrintSetTag.ExtState]
    new ExtState { pctx = x.pctx; attrs = x.attrs; vars = Vars(path, nc, salt) }
  }
}

class LabelTagDlg extends PrintSetTagDlg[LabelTag] {
  @BeanProperty var key: String = _
  @BeanProperty var tokens: String = _

  override def reset { super.reset; key = null; tokens = null }

  import LabelTag._
  override def state: State = {
    val x = super.state.asInstanceOf[BasePrintSetTag.ExtState]
    new ExtState { pctx = x.pctx; attrs = x.attrs; vars = Vars(key, tokens) }
  }
}

class LocationTagDlg extends PrintSetTagDlg[LocationTag] {
  @BeanProperty var obj: Any = _

  override def reset { super.reset; obj = null }

  import LocationTag._
  override def state: State = {
    val x = super.state.asInstanceOf[BasePrintSetTag.ExtState]
    new ExtState { pctx = x.pctx; attrs = x.attrs; vars = Vars(obj) }
  }
}

class SiteTagDlg extends PrintSetTagDlg[SiteTag]

class UseDynamicPageNameTagDlg extends TagDlg[UseDynamicPageNameTag]

class OnCtrlOptionTagDlg extends TagDlg[OnCtrlOptionTag] {
  @BeanProperty var option: String = _

  override def reset { option = null }

  import OnCtrlOptionTag._
  override def state: State = new ExtState { pctx = pageContext; attrs = Attrs(option) }
}

class FreemarkerLoadTagDlg extends TagDlg[FreemarkerLoadTag] {
  @BeanProperty var template: String = _

  override def reset { template = null }

  import FreemarkerLoadTag._
  override def state: State = new ExtState { pctx = pageContext; attrs = Attrs(template) }
}

class FreemarkerLoadOrElseTagDlg extends TagDlg[FreemarkerLoadOrElseTag] {
  @BeanProperty var template: String = _

  override def reset { template = null }

  import FreemarkerLoadTag._
  override def state: State = new ExtState { pctx = pageContext; attrs = Attrs(template) }
}
