package com.coldcore.haala
package web
package tag.freemarker

import freemarker.template.SimpleSequence
import directive.freemarker.{BaseDirective, State, StateWithVars}
import core._
import com.coldcore.haala.core.UrlMapping
import service._
import service.Dependencies
import domain.{Domain, User}
import com.google.gson.Gson

/** Those are extension of Freemarker directives which need a request to operate or need web core
  * objects or only provide value for web pages. Such directives are called "tags" similar to JSP "tags"
  * as those are used in Freemarker templates in favor of JSP pages.
  */

trait GetUser {
  def getUser(state: State): Option[User] = {
    import state._
    val pval = (x: String) => paramAsString(x).getOrElse("")
    val rx = systemVarAsObject[RequestX]("siteState").get
    val us = rx.sc.get[UserService]
    val (userId, userRef) = (pval("userId"), pval("userRef"))

    paramAsObject[User]("user").orElse { //user object as param
      if (userId.nonEmpty) us.getById(userId.toLong) //by ID
      else if (userRef.nonEmpty) us.getByRef(userRef) //by REF
      else rx.xuser.current //current user
    }.map { user => user.password = null; user } // hide password
  }
}

trait GetDomain {
  self: { val sc: ServiceContainer } =>
  
  def getDomain(state: State): Option[Domain] = {
    import state._
    val pval = (x: String) => paramAsString(x).getOrElse("")
    val domainId = pval("domainId")

    paramAsObject[Domain]("domain").orElse { //domain object as param
      if (domainId.nonEmpty) sc.get[DomainService].getById(domainId.toLong) //by ID
      else None
    }
  }
}

class ListRecentLinksTag extends BaseDirective with StateWithVars {
  override def execute(state: StateWithVars) {
    import state._
    val currentURI = vstr("currentURI")
    varAsObject[SimpleSequence]("recentLinks").foreach { xs =>
      xs.toList.foreach { x =>
        loopVars.update(0, wrap(x.asInstanceOf[RecentLink]))
        if (x.asInstanceOf[RecentLink].uri != currentURI) renderBody()
      }
    }

  }
}

class OnCmsEnabledTag extends BaseDirective with StateWithVars {
  override def execute(state: StateWithVars) {
    import state._
    val cmsState = systemVarAsObject[CmsState]("cmsState").get
    val b = pval("on") match {
      case "edit" => cmsState.editing
      case _ => cmsState.enabled
    }

    if (b) renderBody()
  }
}

class CmsEditDivTag extends BaseDirective with StateWithVars {
  override def execute(state: StateWithVars) {
    import state._
    val cmsState = systemVarAsObject[CmsState]("cmsState").get
    val b = cmsState.editing && pval("data") != ""
    if (b) {
      print(s"<div id='${pval("id")}' class='cms-holder ${pval("class")}' data-cms='${pval("data")}'>")
      renderBody()
      print("</div>")
    }
  }
}

/** CMS: list editable FTL pages (UrlMapping setting) */
class ListEditablePagesTag extends BaseDirective {
  override def execute(state: State) {
    import state._
    val xs =
      Option(new Gson().fromJson(setting("UrlMapping", resolveSite).safe,
        classOf[UrlMapping])).getOrElse(new UrlMapping).mapping.filter { x =>
        val hc = x.handlerConf
        hc != null && //to be editable it must have index, pageName and ftl-page set
          hc.index.safe.nonEmpty && hc.pageName.safe.nonEmpty && hc.options.safe.parseCSV.contains("ftl-page")
      }.sortBy(_.pattern)

    render(state, xs, (x: UrlMapping#Mapping) => wrap(x))
  }
}

class HtmFormTag extends BaseDirective with StateWithVars {
  override def execute(state: StateWithVars) {
    import state._
    val (mode, id, style) = (pval("mode"), pval("id"), pval("style"))

    print(s"<form id='${id or "pageForm"}' style='${style.safe}' action='#'" +
      s" onsubmit='return false;'${(mode == "upload") ? " enctype='multipart/form-data'" | ""}>")
    renderBody()
    print("</form>")
  }
}

class RequestParamValueTag extends BaseDirective with StateWithVars {
  override def execute(state: StateWithVars) {
    import state._
    val name = pval("name")
    val value = systemVarAsObject[RequestX]("requestX").get.request.getParameter(name)
    output(state, value)
  }
}

class SearchFilterParamTag extends BaseDirective with StateWithVars {
  override def execute(state: StateWithVars) {
    import state._
    val index = pnum("index").toInt-1
    val param = varAsObject[SearchCommand]("searchCommand").flatMap(_.filter.lift(index)).getOrElse("")
    output(state, param)
  }
}
