package com.coldcore.haala
package web
package tag.logic
package user

import com.coldcore.misc.scala.ReflectUtil._
import com.coldcore.misc.scala.StringOp._
import java.util.Date
import com.coldcore.haala.domain.User
import com.coldcore.haala.core.Timestamp
import javax.servlet.jsp.tagext.Tag
import service._
import scala.collection.JavaConverters._

/** Security tag checking if current user has required role. If the user is not in the role then
 *  tag body will be skipped. The tag operates on current user.
 *  Role:
 *      current - to test if a user is logged in
 *      super   - to test if a user is a super user under someone else's shell
 *      otherwise any role to test (eg. admin) with admin user qualifying for any role.
 */
object InRoleTag {
  case class Attrs(var role: String)

  class ExtState extends State { var attrs: Attrs = _ }
}

class InRoleTag extends BaseTag {
  import InRoleTag._

  override def onStart(in: State): Int = {
    val (requestX, state) = (in.requestX, in.asInstanceOf[ExtState])

    def hasRole(x: String) = x match {
      case "current" => requestX.xuser.state.hasCurrentUser
      case "super" => requestX.xuser.state.hasSuperUser
      case _ => isUserAdmin || isUserInRole(x)
    }

    val b = state.attrs.role.parseCSV match {
      case Seq("-", x @ _*) => !x.exists(hasRole) //none of the roles
      case Seq("+", x @ _*) => x.count(hasRole) == x.size //all of the roles (simple check by count)
      case x => x.exists(hasRole) //any of the roles
    }

    if (b) Tag.EVAL_BODY_INCLUDE else Tag.SKIP_BODY
  }
}

trait SetUser {
  def setUser[R <: { var ref: String; var user: User }](vars: R, state: State, sc: ServiceContainer): Option[User] = {
    if (vars.user == null && vars.ref.safe != "") vars.user = sc.get[UserService].getByRef(vars.ref).orNull
    if (vars.user == null) vars.user = state.requestX.xuser.current.orNull
    Option(vars.user)
  }
}

object OnPackTag {
  case class Attrs(var ref: String, var user: User, var pack: String)

  class ExtState extends State { var attrs: Attrs = _ }
}

class OnPackTag extends BaseTag with SetUser {
  import OnPackTag._

  override def onStart(in: State): Int = {
    val (requestX, state) = (in.requestX, in.asInstanceOf[ExtState])
    val attrs = state.attrs

    setUser(attrs, in, sc) match {
      case Some(user) =>
        val r =
          (false /: attrs.pack.parseCSV)((a,b) =>
            a || attrs.user.packs.exists { p => p.pack == b })
        r ? Tag.EVAL_BODY_INCLUDE | Tag.SKIP_BODY
      case None => Tag.SKIP_BODY
    }
  }
}

object UserTag {
  case class Vars(var ref: String, var user: User)

  class ExtState extends BasePrintSetTag.ExtState { var vars: Vars = _ }
}

class UserTag extends BasePrintSetTag with SetUser {
  import UserTag._

  /** List user domains' hosts. */
  private def domain_list(user: User): Array[String] =
    (if (isUserAdmin) sc.get[DomainService].getAll else user.domains.toList).map(_.domain).sorted.toArray

  /** List user domains' hosts as ID/domain pairs. */
  private def domain_map(user: User): Map[Long,String] =
    (if (isUserAdmin) sc.get[DomainService].getAll else user.domains.toList).map(x => x.id.toLong -> x.domain).toMap

  override def process(in: BasePrintSetTag.ExtState) {
    val (requestX, state) = (in.requestX, in.asInstanceOf[ExtState])
    val (action, vars) = (state.attrs.action, state.vars)

    val pval = (key: String) => paramValue(in, key)
    val out = (v: Any) => output(in, v)

    if (setUser(vars, in, sc).isDefined) {
      val (user, ref, site) = (vars.user, vars.user.ref, resolveSite(in))

      if (action == "credit")
        out(toX100(user.credit*100L))

      if (action == "object")
        out(user)

      if (action == "name" && pval("mode") == "full")
        out(sc.get[UserService].fullName(user, site))

      if (action == "text")
        out(label("u."+ref+"."+pval("key"), site).safe.convert(pval("format")))

      if (action == "date") {
        val x = fieldValue(user, pval("field")).asInstanceOf[Date]
        out(Option(x).map(Timestamp(_).toString("dd/MM/yyyy HH:mm")).orNull)
      }

      if (action == "domains")
        out(pval("mode") match {
          case "list" => domain_list(user)
          case "map" => domain_map(user).asJava
          case _ => null
        })
    }
  }
}

