package com.coldcore.haala
package web
package tag.logic

import javax.servlet.http.HttpServletRequest
import javax.servlet.jsp.tagext.Tag
import javax.servlet.jsp.PageContext
import com.coldcore.misc.scala.StringOp._
import com.coldcore.misc.scala.ReflectUtil._
import core.{CoreActions, Dependencies, RequestX}
import com.coldcore.haala.core.{CoreException, NamedLog, SiteChain, VirtualPath}
import com.coldcore.haala.core.GeneralUtil.generateRef
import service._
import service.MiscService.FreemarkerTemplateIN
import scala.collection.JavaConverters._

class State {
  var pctx: PageContext = _

  def request: HttpServletRequest = pctx.getRequest.asInstanceOf[HttpServletRequest]
  def requestX: RequestX = new RequestX(request) //use for performance
  def print(s: String) = pctx.getOut.print(s)
  def include(path: String) = pctx.include(path)
}

abstract class BaseTag extends CoreActions with Dependencies with SettingsReader with LabelsReader with UserRoleCheck {
  private lazy val LOG = new NamedLog("base-tag", log)

  private def catchError(state: State, body: => Int): Int =
    try {
      body
    } catch {
      case e: Throwable =>
        val (url, mth) = (state.request.xmisc.url, state.request.getMethod.toUpperCase)
        LOG.error("Error "+mth+" "+url, e)
        sc.get[MiscService].reportSystemError("[base-tag] Error "+mth+" "+url, e)
        throw e
    }

  final def doStart(state: State): Int = catchError(state, onStart(state))
  final def doEnd(state: State): Int = catchError(state, onEnd(state))

  def onStart(state: State): Int = Tag.SKIP_BODY
  def onEnd(state: State): Int = Tag.EVAL_PAGE

  /** Return value from 'params' property by its key. */
  def paramValue(params: String, key: String): String = params.safe.parseCSVMap(";").getOrElse(key, "")

  /** Get attribute value from page then request scope. */
  def getAttribute[T](state: State, name: String, value: T = null): T =
    Option(state.pctx.getAttribute(name).asInstanceOf[T]).getOrElse(state.requestX.xattr[T](name, value))
}

object BasePrintSetTag {
  case class Attrs(var `var`: String, var scope: String, var action: String, var params: String,
                   var site: String, var bydefault: Any)

  class ExtState extends State { var outputted = false; var attrs: Attrs = _ }

  case class ExecAction(action: String, params: String, exec: () => Any, enabled: Boolean = true)
}

abstract class BasePrintSetTag extends BaseTag {
  import BasePrintSetTag._

  /** Return value from 'params' property by its key. */
  def paramValue(state: ExtState, key: String): String = paramValue(state.attrs.params, key)

  /** Return current scope. */
  def resolveScope(state: ExtState): String =
    if (state.attrs.scope == "request") "request" else "page"

  /** Return current site. */
  def resolveSite(state: ExtState): SiteChain = SiteChain(state.attrs.site or state.requestX.xsite.current.value)

  def output(state: ExtState, v: Any) {
    val (pctx, requestX, attrs) = (state.pctx, state.requestX, state.attrs)
    val x = if (v == null && attrs.bydefault != null) attrs.bydefault else v

    if (attrs.`var` == null) state.print(Option(x).getOrElse("").toString)
    else
      if (resolveScope(state) == "request") {
        if (x == null) requestX.xattr - attrs.`var`
        else requestX.xattr + (attrs.`var` -> x)
      } else {
        if (x == null) pctx.removeAttribute(attrs.`var`)
        else pctx.setAttribute(attrs.`var`, x)
      }
    state.outputted = true
  }

  def process(state: ExtState)

  override def onStart(in: State): Int = {
    val state = in.asInstanceOf[ExtState]
    process(state)
    if (!state.outputted) output(state, null) //if 'process' method made no output, use default
    Tag.SKIP_BODY
  }

  /** Helper method to execute proper 'action' with proper parameters into output */
  def executeAction(state: ExtState, es: ExecAction*) {
    val pmatch = (e: ExecAction) =>
      e.params.safe == "" || e.params == "*" || !parseCSVMap(e.params).map { case (k, v) =>
        k == "" || k == "*" || v == "" || v == "*" || v == paramValue(state, k) // mode=array,as=*
      }.exists(false==)

    val e = es.filter(x => x.enabled && x.action == state.attrs.action) match { // action=size mode=array,as=*
      case xs if xs.isEmpty => throw new CoreException("Unknown action: "+state.attrs.action)
      case xs => xs.find(pmatch).getOrElse(throw new CoreException("No matching params for action: "+state.attrs.action))
    }
    output(state, e.exec())
  }

}

object OnEntryTag {
  case class Attrs(var value: String, var params: String)

  class ExtState extends State { var attrs: Attrs = _; var bodyIncluded = false }
}

class OnEntryTag extends BaseTag {
  import OnEntryTag._

  override def onStart(in: State): Int = {
    val (state, requestX) = (in.asInstanceOf[ExtState], in.requestX)
    val pval = (key: String) => paramValue(state.attrs.params, key)

    val entryId = pval("id") or "entry_id"
    val id = requestX.xattr[String](entryId, "")

    val b = state.attrs.value.parseCSV.exists(id==)
    if (b) {
      if (pval("keep").isFalse) requestX.xattr - entryId

      state.bodyIncluded = true
      Tag.EVAL_BODY_INCLUDE
    } else Tag.SKIP_BODY
  }
}

class DotTag extends BaseTag {
  override def onStart(state: State): Int = {
    state.print("/web/z/dot.gif")
    Tag.SKIP_BODY
  }
}

object LayoutTag {
  case class Attrs(var path: String, var sfx: String)

  class ExtState extends State { var attrs: Attrs = _ }
}

class LayoutTag extends BaseTag {
  import LayoutTag._

  override def onStart(state: State): Int = {
    val (attrs, requestX) = (state.asInstanceOf[ExtState].attrs, state.requestX)
    val page = requestX.xmisc.siteableJspPath(attrs.path, Option(attrs.sfx).getOrElse("xx"))
    state.include(page)
    Tag.SKIP_BODY
  }
}

object OnPageTag {
  case class Attrs(var index: String)

  class ExtState extends State { var attrs: Attrs = _ }
}

class OnPageTag extends BaseTag {
  import OnPageTag._

  override def onStart(state: State): Int = {
    val (attrs, requestX) = (state.asInstanceOf[ExtState].attrs, state.requestX)
    val pageIndex = requestX.xmisc.pageIndex

    val b = attrs.index.parseCSV match {
      case Seq("-", x @ _*) => !x.contains(pageIndex) //none of the pages
      case x => x.contains(pageIndex) //any of the pages
    }
    if (b) Tag.EVAL_BODY_INCLUDE else Tag.SKIP_BODY
  }
}

object UtilTag {
  case class Vars(var value: AnyRef)

  class ExtState extends BasePrintSetTag.ExtState { var vars: Vars = _ }
}

class UtilTag extends BasePrintSetTag {
  import UtilTag._

  override def process(in: BasePrintSetTag.ExtState) {
    val state = in.asInstanceOf[ExtState]
    val (action, value) = (state.attrs.action, state.vars.value)

    val pval = (key: String) => paramValue(in, key)
    val out = (v: Any) => output(in, v)

    val v = if (value != null) value.toString.trim else ""

    if (action == "array" && pval("data") == "csv")
      out(v.parseCSV(",").toArray)

    if (action == "count") // start ... start+value
      out((pval("start").toInt until pval("start").toInt+v.toInt).toArray)

//    if (action == "size") //collection, array, string
//      out(if (value.isInstanceOf[String] || value.isInstanceOf[Array[Any]])
//            value.asInstanceOf[{ def length: Int }].length else value.asInstanceOf[{ def size(): Int }].size)

    if (action == "size")
      out(value match {
        case x: String => x.size
        case x: Seq[_] => x.size
        case x: JCollection[_] => x.size
        case x: Array[_] => x.size
        case x: Map[_,_] => x.size
        case x: JMap[_,_] => x.size
        case _ => 0
      })

    if (action == "salt")
      out(generateRef("|hms|.|sym4|").toLowerCase)

    if (action == "blank")
      out(pval("is") match {
        case "html" => replaceTokens("<", ">", v, Seq.empty).trim == ""
        case _ => v == ""
      })

    if (action == "notBlank")
      out(pval("is") match {
        case "html" => replaceTokens("<", ">", v, Seq.empty).trim != ""
        case _ => v != ""
      })

    if (action == "tolast")
      out(if (v.indexOf(pval("of")) != -1) v.substring(0, v.lastIndexOf(pval("of"))) else "")

    if (action == "contains") {
      val p = pval("what")
      out((value != null || v != "") &&
        (if (pval("data") == "csv") v.parseCSV(",").exists(p==)
         else value match {
          case x: String => x.contains(p)
          case x: Seq[_] => x.contains(p)
          case x: JCollection[_] => x.contains(p)
          case x: Array[_] => x.contains(p)
          case _ => false
        }))
    }

    if (action == "formatArray") {
      val x: Seq[_] = value match {
        case x: String => v.parseCSV(",")
        case x: Seq[_] => x
        case x: JCollection[_] => x.asScala.toList
        case x: Array[_] => x
        case _ => Nil
      }
      out(pval("as") match {
        case "csv" => x.mkString(",")
        case "jsonArray" => x.asInstanceOf[Seq[AnyVal]].map("\""+_+"\"").mkString("[", ",", "]")
        case _ => x
      })
    }

    if (action == "formatText") {
      out(pval("as") match {
        case "html" => v.convert("html")
        case "currency" => sc.get[CurrencyService].getSign(v)
        case _ => v
      })
    }

    if (action == "formatNumber") {
      out(pval("as") match {
        case "hex" => v.safeLong.hex
        case "price" => toX100(v.safeLong)
        case _ => v
      })
    }

  }
}

object UrlTag {
  case class Vars(var path: String, var nc: String, var salt: String)

  class ExtState extends BasePrintSetTag.ExtState { var vars: Vars = _ }
}

class UrlTag extends BasePrintSetTag {
  import UrlTag._

  override def process(in: BasePrintSetTag.ExtState) {
    val (requestX, state) = (in.requestX, in.asInstanceOf[ExtState])
    val (path, nc, salt) = (state.vars.path, state.vars.nc, state.vars.salt)
    val ver = requestX.xmisc.resourceVersion
    val siteK = requestX.xsite.chainMap.keyByChain(resolveSite(in))

    val out = (v: Any) => output(in, v)

    out(path match {
      case x if x.safe == "" => ""
      case x =>
        "/"+x+"?site="+siteK+"&rv="+ver+(if (nc.safe != "") "&nc=1" else "")+
        (if (salt.safe != "") "&salt="+System.currentTimeMillis.hex else "")
    })
  }
}

object LabelTag {
  case class Vars(var key: String, var tokens: String)

  class ExtState extends BasePrintSetTag.ExtState { var vars: Vars = _ }
}

class LabelTag extends BasePrintSetTag {
  import LabelTag._

  override def process(in: BasePrintSetTag.ExtState) {
    val state = in.asInstanceOf[ExtState]
    val (site, curSite) = (resolveSite(state), state.requestX.xsite.current)
    val (key0, tokens) = (state.vars.key, state.vars.tokens.safe)
    val (mode, suffix, pindex) = (paramValue(state, "mode"), paramValue(state, "suffix"), paramValue(state, "pindex"))
    val tokenSp = (tokens+" ").head match { case ';' => ";"; case _ => "," }
    val bydefault = Option(state.attrs.bydefault.asInstanceOf[String])

    val pageIndex = state.request.xmisc.pageIndex
    val key = if (key0.contains(".") || pageIndex == "" || pindex == "0") key0 else pageIndex+"."+key0 // 001 -> 03022.001

    val value = mode match {
      case "seq" => label(key+suffix, site) nil label(key, site)
      case _ => label(key, site) nil bydefault.map(x => label(x, curSite) nil x).orNull
    }

    output(state, Option(value).map(x => parametrize(x, tokens.parseCSV(tokenSp): _*)).getOrElse(key))
  }
}

object LocationTag {
  case class Vars(var obj: Any)

  class ExtState extends BasePrintSetTag.ExtState { var vars: Vars = _ }
}

@deprecated(message = "Location should be removed, avoid using", since = "")
class LocationTag extends BasePrintSetTag {
  import LocationTag._

  override def process(in: BasePrintSetTag.ExtState) {
    val state = in.asInstanceOf[ExtState]
    val (action, obj) = (state.attrs.action, state.vars.obj)
    val ls = sc.get[LocationService]

    val pval = (key: String) => paramValue(in, key)
    val out = (v: Any) => output(in, v)

    if (action == "country")
      out(pval("mode") match {
        case "real" => ls.getRealCountries.toArray
        case "list" => ls.getCountries.toArray
        case _ => null
      })

    if (action == "text" && obj != null) {
      val v = invokeMethod(obj.asInstanceOf[AnyRef], "getTextKey").asInstanceOf[String]
      out(label(v, resolveSite(state)))
    }
  }
}

class SiteTag extends BasePrintSetTag {
  /** Return site's language, if no site supplied then revert to a current site. */
  private def language(requestX: RequestX, key: String): String = {
    val site = key or requestX.xsite.state.rootSite
    val rootSite = requestX.xsite.chainMap.rootSiteLinks(site)
    parsedSites.filter(_.key == rootSite).head.locale
  }

  /** Return list of root sites. */
  private def rootSites: Array[String] =
    parsedSites.map(_.key).toArray

  /** Return list of sites from 'chain' map in the same order as 'root' sites. */
  private def listSites(requestX: RequestX, chainMap: Map[String,SiteChain]): Array[String] = {
    val linkRoot = requestX.xsite.chainMap.rootSiteLinks
    (for (rootSite <- rootSites) yield chainMap.keys.filter(rootSite == linkRoot.get(_).get).head).toArray
  }

  override def process(in: BasePrintSetTag.ExtState) {
    val (requestX, site) = (in.requestX, resolveSite(in))
    val action = in.attrs.action

    val pval = (key: String) => paramValue(in, key)
    val out = (v: Any) => output(in, v)

    if (action == "my")
      out(pval("mode") match {
        case "def" => listSites(requestX, requestX.xsite.chainMap.mineSiteChain).head //'my' site matching default root
        case _ => requestX.xsite.chainMap.keyByChain(site) //'my' site
      })

    if (action == "list")
      out(pval("use") match {
        case "mine" => listSites(requestX, requestX.xsite.chainMap.mineSiteChain)
        case "xtra" => listSites(requestX, requestX.xsite.chainMap.xtraSiteChain)
        case _ => rootSites //same as action "input"
      })

    //these are the default sites (or mine sites for CMS) user can use to input multilingual text
    if (action == "input") {
      val x = pval("use") match {
        case "mine" => listSites(requestX, requestX.xsite.chainMap.mineSiteChain)
        case "xtra" => listSites(requestX, requestX.xsite.chainMap.xtraSiteChain)
        case _ => rootSites
      }
      out(pval("as") match {
        case "csv" => x.mkString(",")
        case "jsonArray" => x.map("\""+_+"\"").mkString("[", ",", "]")
        case _ => x
      })
    }

    if (action == "lang")
      out(language(requestX, pval("key")))

  }
}

class UseDynamicPageNameTag extends BaseTag {
  var originalName: String = _

  override def onStart(state: State): Int = {
    var requestX = state.requestX
    originalName = requestX.xattr[String]("pageName") // dynamic
    val dynamicName = requestX.xattr[String]("dynamicPageName", originalName) // other/mypage

    requestX.xattr + ("pageName" -> dynamicName)
    Tag.EVAL_BODY_INCLUDE
  }

  override def onEnd(state: State) = {
    state.requestX.xattr + ("pageName" -> originalName)
    Tag.EVAL_PAGE
  }
}

object OnCtrlOptionTag {
  case class Attrs(var option: String)

  class ExtState extends State { var attrs: Attrs = _ }
}

class OnCtrlOptionTag extends BaseTag {
  import OnCtrlOptionTag._

  override def onStart(state: State): Int = {
    val (attrs, requestX) = (state.asInstanceOf[ExtState].attrs, state.requestX)
    val options = requestX.xattr.ctrlOptions

    val b = attrs.option.parseCSV match {
      case Seq("-", x @ _*) => !x.exists(options.contains) //none of the options
      case Seq("+", x @ _*) => x.map(options.contains).count(true==) == x.size //all of the options (simple check by count)
      case x => x.exists(options.contains) //any of the options
    }

    if (b) Tag.EVAL_BODY_INCLUDE else Tag.SKIP_BODY
  }
}

object FreemarkerLoadTag {
  case class Attrs(var template: String)

  class ExtState extends State { var attrs: Attrs = _ }
}

class BaseFreemarkerLoadTag extends BaseTag {
  import FreemarkerLoadTag._

  override def onStart(state: State): Int = {
    val (attrs, requestX) = (state.asInstanceOf[ExtState].attrs, state.requestX)

    val system = Map( //system objects
      "requestX" -> requestX,
      "siteChainMap" -> requestX.xsite.chainMap,
      "ctrlOptions" -> requestX.xattr.ctrlOptions,
      "pageIndex" -> requestX.xattr[String]("pageIndex", ""),
      "siteState" -> requestX.xsite.state,
      "cmsState" -> requestX.xcms.state,
      "serviceContainer" -> requestX.sc
    )

    val directives = //wired directives
      (Map.empty[String, AnyRef] /: freemarkerDirectives.getAsMap)((r,x) => r + (x._1 -> x._2.asJava))

    val attributes = //pushed attributes (plus pushed by aspect)
      state.request.getAttributeNames.map { case k: String =>
        val v = state.request.getAttribute(k)
        k -> (v match {
          case x: Seq[_] => x.asJava
          case x: Map[_,_] => x.asJava
          case x => x
        })}

    val model = directives ++ Map("system" -> system.asJava) ++ attributes
    val x = sc.get[MiscService].freemarkerTemplate(FreemarkerTemplateIN(
        s"web/${attrs.template}:${requestX.xsite.current}", model)).getOrElse("")
    state.print(x)
    Tag.SKIP_BODY
  }
}

class FreemarkerLoadTag extends BaseFreemarkerLoadTag

class FreemarkerLoadOrElseTag extends BaseFreemarkerLoadTag {
  import FreemarkerLoadTag._

  override def onStart(state: State): Int = {
    val (template, requestX) = (state.asInstanceOf[ExtState].attrs.template, state.requestX)
    if (sc.get[FileService].getByPath(VirtualPath("web/"+template), requestX.xsite.current).isDefined) {
      state.pctx.setAttribute("loadOrElse", true)
      super.onStart(state)
    } else {
      state.pctx.setAttribute("loadOrElse", false)
      Tag.SKIP_BODY
    }
  }
}
