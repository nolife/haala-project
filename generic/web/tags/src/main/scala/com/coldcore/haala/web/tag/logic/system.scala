package com.coldcore.haala
package web
package tag.logic

import java.util.{Calendar, Date}
import com.coldcore.haala.core.{SiteChain, Timestamp}
import core.SearchCommand
import service.UserService

class SystemTag extends BasePrintSetTag {
  /** Create a date from a range. */
  private def date_rangeToDate(s: String, lastDate: Date = null): Date = {
    val (curDate, cal) = (Timestamp().toDate, Timestamp().toCalendar)
    cal.setTime(Option(lastDate).getOrElse(curDate))
    val (lastMonth, lastYear) = (cal.get(Calendar.MONTH), cal.get(Calendar.YEAR))

    cal.setTime(curDate)
    cal.set(Calendar.DAY_OF_MONTH, 1)
    cal.set(Calendar.MONTH, s.split("/")(1).toInt-1)
    if (cal.get(Calendar.MONTH) < lastMonth || cal.get(Calendar.YEAR) < lastYear) cal.add(Calendar.YEAR, 1)
    cal.set(Calendar.DAY_OF_MONTH,
      if (s.split("/")(0) == "L") cal.getActualMaximum(Calendar.DAY_OF_MONTH) else s.split("/")(0).toInt)
    cal.getTime
  }

  /** Return date range. */
  private def date_range(site: SiteChain, key: String, subkey: String): String = {
    val rang = setting(key, site).parseCSVMap.getOrElse(subkey, "")
    val dateFrom = if (rang != "") date_rangeToDate(rang.split("-")(0)) else null
    val dateTill = if (rang != "") date_rangeToDate(rang.split("-")(1), dateFrom) else null
    if (rang == "") "" else Timestamp(dateFrom).toString("dd/MM/yyyy")+" "+Timestamp(dateTill).toString("dd/MM/yyyy")
  }

  override def process(in: BasePrintSetTag.ExtState) {
    val (requestX, site) = (in.requestX, resolveSite(in))
    val action = in.attrs.action

    val pval = (key: String) => paramValue(in, key)
    val out = (v: Any) => output(in, v)

    if (action == "currency")
      out(pval("mode") match {
        case "sign" => setting("CurrencySigns").parseCSVMap(defaultCurrency) //default currency sign
        case "list" => setting("Currencies").parseCSV.toArray
        case _ => null
      })

    if (action == "user")
      out(pval("type") match {
        case "current" => requestX.xuser.current.orNull
        case "system" => sc.get[UserService].getSystemUser
        case _ => null
      })

    if (action == "setting")
      out(pval("type")+"/"+pval("as") match {
        case "/dateRange" => date_range(site, pval("key"), pval("subkey"))
        case "sys/dateRange" => date_range(SiteChain.sys, pval("key"), pval("subkey"))
        case "sys/" => setting(pval("key"))
        case _ => setting(pval("key"), site)
      })

    if (action == "brand")
      out(setting("Brand", site).parseCSVMap(pval("key")))

    if (action == "env")
      out(settingService.getEnv)

    if (action == "request" && pval("mode") == "param")
      out(in.request.getParameter(pval("key")))

    if (action == "limit")
      out(pval("mode") match {
        case "ipp" => settingService.getItemsPerPage(pval("key"))
        case _ => setting("ParamsLimit").parseCSVMap()(pval("key")).toInt
      })

    if (action == "winlin" && pval("mode") == "input")
      out(if (in.request.getHeader("User-Agent").safe.contains("windows")) pval("size")
        else pval("size").toInt-10) //Linux FireFox renders input wider

    if (action == "searchfilter") {
      val index = pval("index").toInt-1
      val params = requestX.xattr.get[SearchCommand]("searchCommand").map(_.filter).getOrElse(Nil)
      out(if (index < params.size) params(index) else "")
    }
  }
}
