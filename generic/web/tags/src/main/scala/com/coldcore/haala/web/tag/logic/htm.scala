package com.coldcore.haala
package web
package tag.logic
package htm

import javax.servlet.jsp.tagext.Tag

object FormTag {
  case class Attrs(var mode: String, var id: String, var style: String)

  class ExtState extends State { var attrs: Attrs = _ }
}

class FormTag extends BaseTag {
  import FormTag._

  override def onStart(in: State): Int = {
    val state = in.asInstanceOf[ExtState]
    val (mode, id, style) = (state.attrs.mode, state.attrs.id, state.attrs.style.safe)

    in.print(s"<form id='${id or "pageForm"}' style='${style.safe}' action='#'" +
      s" onsubmit='return false;'${(mode == "upload") ? " enctype='multipart/form-data'" | ""}>")

    Tag.EVAL_BODY_INCLUDE
  }

  override def onEnd(in: State): Int = {
    in.print("</form>")
    Tag.EVAL_PAGE
  }
}
