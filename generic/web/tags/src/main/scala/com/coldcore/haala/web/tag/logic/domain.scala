package com.coldcore.haala
package web
package tag.logic
package domain

import javax.servlet.jsp.tagext.Tag

object OnTypeTag {
  case class Attrs(var `type`: String)

  class ExtState extends State { var attrs: Attrs = _ }
}

class OnTypeTag extends BaseTag {
  import OnTypeTag._

  override def onStart(in: State): Int = {
    val (requestX, state) = (in.requestX, in.asInstanceOf[ExtState])

    if (state.attrs.`type`.parseCSV.contains(requestX.xsite.state.currentDomainType))
      Tag.EVAL_BODY_INCLUDE else Tag.SKIP_BODY
  }
}
