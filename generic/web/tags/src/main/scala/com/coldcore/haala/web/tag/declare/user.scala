package com.coldcore.haala
package web.tag.declare
package user

import beans.BeanProperty
import web.tag.logic.{BasePrintSetTag, BaseTag, State}
import web.tag.logic.user._
import com.coldcore.haala.domain.User

class InRoleTagDlg extends TagDlg[InRoleTag] {
  @BeanProperty var role: String = _

  override def reset { role = null }

  import InRoleTag._
  override def state: State = new ExtState { pctx = pageContext; attrs = Attrs(role) }
}

class OnPackTagDlg extends TagDlg[OnPackTag] {
  @BeanProperty var ref: String = _
  @BeanProperty var user: User = _
  @BeanProperty var pack: String = _

  override def reset { super.reset; ref = null; user = null; pack = null }

  import OnPackTag._
  override def state: State = new ExtState { pctx = pageContext; attrs = Attrs(ref, user, pack) }
}

class UserTagDlg extends PrintSetTagDlg[UserTag] {
  @BeanProperty var ref: String = _
  @BeanProperty var user: User = _

  override def reset { super.reset; ref = null; user = null }

  import UserTag._
  override def state: State = {
    val x = super.state.asInstanceOf[BasePrintSetTag.ExtState]
    new ExtState { pctx = x.pctx; attrs = x.attrs; vars = Vars(ref, user) }
  }
}

