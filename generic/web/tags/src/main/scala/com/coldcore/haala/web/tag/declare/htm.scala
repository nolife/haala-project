package com.coldcore.haala
package web.tag.declare
package htm

import beans.BeanProperty
import web.tag.logic.State
import web.tag.logic.htm._

class FormTagDlg extends TagDlg[FormTag] {
  @BeanProperty var mode: String = _
  @BeanProperty var style: String = _
  private var idx: String = _

  override def getId = idx
  override def setId(x: String) = idx = x

  override def reset { idx = null; mode = null; style = null }

  import FormTag._
  override def state: State = new ExtState { pctx = pageContext; attrs = Attrs(mode, idx, style) }
}
