package com.coldcore.haala
package web
package tag.logic
package cms

import javax.servlet.jsp.tagext.Tag

object EditTag {
  case class Attrs(var data: String, var params: String)

  class ExtState extends State { var attrs: Attrs = _ }
}

class EditTag extends BaseTag {
  import EditTag._

  /** Check if resources are editable and a tag is correctly filled. */
  def goAhead(state: ExtState): Boolean = state.requestX.xcms.state.editing && state.attrs.data.safe != ""

  override def onStart(in: State): Int = {
    val state = in.asInstanceOf[ExtState]
    val pval = (key: String) => paramValue(state.attrs.params, key)

    if (goAhead(state)) {
      state.print(s"<div id='${pval("id")}' class='cms-holder ${pval("class")}' data-cms='${state.attrs.data}'>")
      Tag.EVAL_BODY_INCLUDE
    } else Tag.SKIP_BODY
  }

  override def onEnd(in: State): Int = {
    if (goAhead(in.asInstanceOf[ExtState])) in.print("</div>")
    Tag.EVAL_PAGE
  }
}

object EnabledTag {
  case class Attrs(var fn: String)

  class ExtState extends State { var attrs: Attrs = _ }
}

class EnabledTag extends BaseTag {
  import EnabledTag._

  override def onStart(in: State): Int = {
    val (requestX, state) = (in.requestX, in.asInstanceOf[ExtState])

    val b = state.attrs.fn match {
      case "edit" => requestX.xcms.state.editing
      case _ => requestX.xcms.state.enabled
    }

    if (b) Tag.EVAL_BODY_INCLUDE else Tag.SKIP_BODY
  }
}

