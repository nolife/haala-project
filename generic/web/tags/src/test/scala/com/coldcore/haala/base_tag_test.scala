package com.coldcore.haala
package web
package tag

import logic.BasePrintSetTag
import org.scalatest.mockito.MockitoSugar
import org.mockito.Mockito._
import org.mockito.ArgumentMatchers._
import org.mockito.ArgumentCaptor
import org.scalatest._
import org.junit.runner.RunWith
import org.scalatest.junit.JUnitRunner
import tag.logic.BasePrintSetTag.{ExecAction, Attrs, ExtState}
import com.coldcore.haala.core.CoreException

@RunWith(classOf[JUnitRunner])
class BasePrintSetTagSpec extends WordSpec with MockitoSugar with BeforeAndAfter {
  var m: MyMock = _

  before {
    m = new MyMock
  }

  class MyMock extends com.coldcore.haala.web.test.MyMock {
    val tag = new BasePrintSetTag {
      def process(state: ExtState) {}
    } |< { x => x.serviceContainer = serviceContainer }

    val state = new ExtState |< { x =>
      x.pctx = pctx; x.attrs = Attrs("myvar", "", "size", "mode=array;as=int;format=json", "xx1", "na")}
  }

  "executeAction" should {
    "execute 'size' action" in {
      m.tag.executeAction(m.state,
        ExecAction("size", "*", () => { 10 }),
        ExecAction("has", "*", () => { false })
      )
      assertResult(10) { m.pctx.getAttribute("myvar") }
    }

    "fail when no action found" in {
      intercept[CoreException] { m.tag.executeAction(m.state, ExecAction("has", "*", () => { false })) }
    }

    "execute 'size' action with param matching" in {
      m.tag.executeAction(m.state,
        ExecAction("size", "mode=array", () => { 12 }),
        ExecAction("size", "*", () => { 10 })
      )
      assertResult(12) { m.pctx.getAttribute("myvar") }
    }

    "execute 'size' action with params matching" in {
      m.tag.executeAction(m.state,
        ExecAction("size", "mode=array,as=any", () => { 20 }),
        ExecAction("size", "mode=array,as=int", () => { 18 }),
        ExecAction("size", "mode=array,as=*", () => { 16 }),
        ExecAction("size", "mode=array", () => { 12 }),
        ExecAction("size", "*", () => { 10 })
      )
      assertResult(18) { m.pctx.getAttribute("myvar") }
    }

    "execute 'size' action with params matching (ignore disabled action)" in {
      m.tag.executeAction(m.state,
        ExecAction("size", "mode=array,as=any", () => { 20 }),
        ExecAction("size", "mode=array,as=int", () => { 18 }, false),
        ExecAction("size", "mode=array,as=*", () => { 16 }),
        ExecAction("size", "mode=array", () => { 12 }),
        ExecAction("size", "*", () => { 10 })
      )
      assertResult(16) { m.pctx.getAttribute("myvar") }
    }

    "execute 'size' action with param matching wildcard" in {
      m.tag.executeAction(m.state,
        ExecAction("size", "mode=array,as=any", () => { 20 }),
        ExecAction("size", "mode=array,as=*", () => { 16 }),
        ExecAction("size", "mode=array", () => { 12 }),
        ExecAction("size", "*", () => { 10 })
      )
      assertResult(16) { m.pctx.getAttribute("myvar") }
    }

    "execute 'size' action with params matching wildcards" in {
      m.tag.executeAction(m.state,
        ExecAction("size", "mode=array,as=any", () => { 20 }),
        ExecAction("size", "mode=array,as=*,format=*", () => { 18 }),
        ExecAction("size", "mode=array,as=*", () => { 16 }),
        ExecAction("size", "mode=array", () => { 12 }),
        ExecAction("size", "*", () => { 10 })
      )
      assertResult(18) { m.pctx.getAttribute("myvar") }
    }

    "fail when action found but no mathing params" in {
      intercept[CoreException] { m.tag.executeAction(m.state, ExecAction("size", "mode=array,as=any", () => { 20 })) }
    }
  }

}
