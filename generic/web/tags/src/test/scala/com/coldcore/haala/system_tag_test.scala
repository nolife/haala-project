package com.coldcore.haala
package web
package tag

import logic.{BasePrintSetTag, SystemTag}
import org.junit.runner.RunWith
import org.scalatest.{BeforeAndAfter, FlatSpec, Matchers}
import web.test.MyMock
import org.scalatest.mockito.MockitoSugar
import org.mockito.Mockito._
import org.mockito.ArgumentMatchers._
import org.mockito.ArgumentCaptor
import org.scalatest.junit.JUnitRunner
import tag.logic.BasePrintSetTag.Attrs

@RunWith(classOf[JUnitRunner])
class SystemTagSpec extends FlatSpec with MockitoSugar with BeforeAndAfter with Matchers {
  var m: MyMock = _

  before {
    m = new MyMock
  }

  it should "action env" in {
    val state = new BasePrintSetTag.ExtState { pctx = m.pctx; attrs = Attrs("myVar", "", "env", "", "mc1", null) }
    val tag = new SystemTag { serviceContainer = m.serviceContainer }
    when(m.settingService.getEnv).thenReturn("local")
    tag.onStart(state)
    assertResult("local") { m.pctx.getAttribute("myVar") }
  }

}
