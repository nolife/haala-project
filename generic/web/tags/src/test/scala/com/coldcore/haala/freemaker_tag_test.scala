package com.coldcore.haala
package web
package tag.freemarker

import org.scalatest.mockito.MockitoSugar
import org.mockito.Mockito._
import org.scalatest.{BeforeAndAfter, WordSpec}
import org.junit.runner.RunWith
import org.scalatest.junit.JUnitRunner
import service.test.MyMock
import com.coldcore.haala.core.SiteChain.Implicits._
import com.coldcore.haala.core.VirtualPath.Implicits._

@RunWith(classOf[JUnitRunner])
class HtmFormTagSpec extends WordSpec with MockitoSugar with BeforeAndAfter {
  var m: MyMock = _

  before {
    m = new MyMock
  }

  "directive" should {
    "print form tag" in {
      m.registerFile(11, "bar/template.ftl", ".sys", """<@mydir mode="upload" id="myForm" style="display:none">foo</@>""")

      val r = m.processFtl("bar/template.ftl", Map(
        "mydir" -> new HtmFormTag))

      assertResult("<form id='myForm' style='display:none' action='#' onsubmit='return false;' enctype='multipart/form-data'>foo</form>") { r }
    }
  }

}
