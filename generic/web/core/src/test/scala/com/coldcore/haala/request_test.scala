package com.coldcore.haala
package web
package core

import org.scalatest._
import web.test.MyMock
import org.scalatest.mockito.MockitoSugar
import org.mockito.Mockito._
import org.mockito.ArgumentMatchers._
import org.mockito.ArgumentCaptor
import org.springframework.core.io.Resource
import org.junit.runner.RunWith
import org.scalatest.junit.JUnitRunner
import domain.{Domain, Role, User}
import com.coldcore.haala.core.{SiteChainMap, SiteState}
import com.coldcore.haala.core.SiteChain.Implicits._

import scala.collection.JavaConverters._

@RunWith(classOf[JUnitRunner])
class RequestSpec extends FlatSpec with MockitoSugar with BeforeAndAfter with Matchers {
  var m: MyMock = _

  before {
    m = new MyMock
  }

  "xmisc" should "provide path to jsp by site" in {
    m.request.getSession.setAttribute(WebConstants.SITE_STATE,
      SiteState(
        currentDomainSite = "mh",
        currentSite = "", currentDomain = "", currentDomainId = 0, currentDomainType = "", currentDomainStyle = ""
      ))
    val resource = mock[Resource]
    when(m.resourceLoader.getResource(anyString)).thenReturn(resource)

    when(resource.exists).thenReturn(false)
    assertResult("/WEB-INF/jsp/pages/bb_none.jsp") { m.request.xmisc.siteableJspPath("pages/bb_none") }
    assertResult("/WEB-INF/jsp/bb_none-xx.jsp") { m.request.xmisc.siteableJspPath("bb_none", "xx") }

    when(resource.exists).thenReturn(true)
    assertResult("/WEB-INF/jsp/bb_none-mh.jsp") { m.request.xmisc.siteableJspPath("bb_none") }

    m.request.getSession.setAttribute(WebConstants.SITE_STATE,
      SiteState(
        currentDomainSite = "pp.mh",
        currentSite = "", currentDomain = "", currentDomainId = 0, currentDomainType = "", currentDomainStyle = ""
      ))
    assertResult("/WEB-INF/jsp/pages/bb_none-mh.jsp") { m.request.xmisc.siteableJspPath("pages/bb_none") }
  }

}

@RunWith(classOf[JUnitRunner])
class UserRequestExtSpec extends WordSpec with MockitoSugar with BeforeAndAfter {
  var m: MyMock = _

  before {
    m = new MyMock
  }

  class MyMock extends com.coldcore.haala.web.test.MyMock {
    val rx = new RequestX(request) |< { x => x.serviceContainer = serviceContainer }
    request.xattr.perm + (WebConstants.SITE_CHAIN_MAP -> siteChainMap, WebConstants.SITE_STATE -> siteState)
  }

  val siteState =
    SiteState(
      currentDomainSite = "", currentSite = "", currentDomain = "", currentDomainId = 0,
      currentDomainType = Domain.TYPE_CONTROL_PANEL, currentDomainStyle = "")

  val siteState2 =
    SiteState(
      currentDomainSite = "", currentSite = "", currentDomain = "", currentDomainId = 0,
      currentDomainType = "agency", currentDomainStyle = "")

  val siteChainMap =
    SiteChainMap(Map.empty, Map.empty, Map.empty, Map.empty)

  "isLoginForbidden" should {
    "forbid minions from control panel" in {
      val user = new User { id = 5L }
      when(m.securityService.isUserInRole(Role.ADMIN)).thenReturn(false)
      when(m.securityService.isUserInRole("ROLE_CPANEL")).thenReturn(false)
      assertResult(true) { m.rx.xuser.isLoginForbidden(user) }
    }

    "allow minions with domains to control panel" in {
      val user = new User { id = 5L; domains = List(new Domain).asJava }
      when(m.securityService.isUserInRole(Role.ADMIN)).thenReturn(false)
      when(m.securityService.isUserInRole("ROLE_CPANEL")).thenReturn(false)
      assertResult(false) { m.rx.xuser.isLoginForbidden(user) }
    }

    "allow admin to control panel" in {
      val user = new User { id = 5L }
      when(m.securityService.isUserInRole(Role.ADMIN)).thenReturn(true)
      when(m.securityService.isUserInRole("ROLE_CPANEL")).thenReturn(false)
      assertResult(false) { m.rx.xuser.isLoginForbidden(user) }
    }

    "allow cpanel role to control panel" in {
      val user = new User { id = 5L }
      when(m.securityService.isUserInRole(Role.ADMIN)).thenReturn(false)
      when(m.securityService.isUserInRole("ROLE_CPANEL")).thenReturn(true)
      assertResult(false) { m.rx.xuser.isLoginForbidden(user) }
    }

    "allow minions to other domains" in {
      m.request.xattr.perm + (WebConstants.SITE_STATE -> siteState2)
      val user = new User { id = 5L }
      when(m.securityService.isUserInRole(Role.ADMIN)).thenReturn(false)
      when(m.securityService.isUserInRole("ROLE_CPANEL")).thenReturn(false)
      assertResult(false) { m.rx.xuser.isLoginForbidden(user) }
    }
  }

}