package com.coldcore.haala
package web
package core

import java.util.Locale
import javax.servlet.http.Cookie

import com.coldcore.haala.core.Constants.UTF8
import com.coldcore.haala.core.{SiteChain, SiteState}
import com.coldcore.haala.core.SiteChain.Implicits._
import test.SetupMods
import domain.Domain
import org.scalatest.{BeforeAndAfter, FlatSpec, Matchers}
import org.scalatest.mockito.MockitoSugar
import org.mockito.Mockito._
import org.mockito.ArgumentMatchers._
import org.junit.runner.RunWith
import org.scalatest.junit.JUnitRunner

import scala.collection.JavaConverters._

@RunWith(classOf[JUnitRunner])
class RequestSetupModSpec extends FlatSpec with MockitoSugar with BeforeAndAfter with Matchers {
  var m: MyMock = _

  before {
    m = new MyMock
  }

  class MyMock extends com.coldcore.haala.web.test.MyMock {
    val mod = new RequestSetupMod

    request.setCharacterEncoding("ISO-8859-1")
    request.getSession.removeAttribute(WebConstants.USER_STATE)

    an [NoSuchElementException] should be thrownBy request.xuser.state
  }

  it should "call init and set encoding to utf8" in {
    m.mod.setup(m.request)
    m.request.getCharacterEncoding shouldBe UTF8
    m.request.xuser.state should not be null
  }

}

@RunWith(classOf[JUnitRunner])
class DomainSetupModSpec extends FlatSpec with MockitoSugar with BeforeAndAfter with Matchers {
  var m: MyMock = _

  before {
    m = new MyMock
  }

  class MyMock extends com.coldcore.haala.web.test.MyMock {
    val mod = new DomainSetupMod

    request.setServerName("www.example.org")
    request.getSession.removeAttribute(WebConstants.SITE_STATE)

    when(domainService.resolveDomain("www.example.org")).thenReturn(meDomain)

    an [NoSuchElementException] should be thrownBy request.xsite.state
  }

  val meDomain = new Domain {
    id = 1L; domain = "example.org"; domainSite = "me"; `type` = "main"; meta = "style=html5,layout-modern; resolve=site,currency" }

  it should "populate site-domain map with info about current domain" in {
    m.mod.setup(m.request)
    m.request.xsite.state shouldBe SiteState(
      currentDomain = "example.org",
      currentDomainId = 1,
      currentDomainSite = SiteChain("me"),
      currentDomainType = "main",
      currentDomainStyle = "html5,layout-modern",
      currentSite = SiteChain.empty
    )
  }

}

@RunWith(classOf[JUnitRunner])
class SiteSetupModSpec extends FlatSpec with MockitoSugar with BeforeAndAfter with Matchers {
  var m: MyMock = _

  before {
    m = new MyMock
  }

  class MyMock extends com.coldcore.haala.web.test.MyMock {
    val mod = new SiteSetupMod { override val staticBlock = m.staticBlock }

    siteState.currentSite = SiteChain.empty
    siteState.defaultSite = SiteChain.empty
    siteState.rootSite = ""
    request.getSession.removeAttribute(WebConstants.SITE_CHAIN_MAP)
    request.getSession.setAttribute(WebConstants.SITE_STATE, siteState)
    request.removeAttribute("currentSite")
    request.removeAttribute("rootSite")
    request.setRemoteAddr("0.0.0.0")

    when(domainService.resolveRootSite("example.org", Locale.ENGLISH)).thenReturn(SiteChain("xx1"))
    when(domainService.defaultSite("example.org")).thenReturn(SiteChain("xx1.me1"))
    when(settingService.querySystem("Sites")).thenReturn("xx1=xx1:en, xx2=xx1.xx2:ru")
    when(staticBlock.localeByIP("0.0.0.0")).thenReturn(null)

    an [NoSuchElementException] should be thrownBy request.xsite.chainMap
  }

  val siteState = SiteState(
    currentDomain = "example.org", currentDomainId = 1, currentDomainSite = SiteChain("me"),
    currentDomainType = "main", currentDomainStyle = "html5,layout-modern")

  it should "populate site-domain with info about current site" in {
    m.mod.setup(m.request)

    siteState.currentSite shouldBe SiteChain("xx1.me1")
    siteState.defaultSite shouldBe SiteChain("xx1.me1")
    siteState.rootSite shouldBe "xx1"

    val cm = m.request.xsite.chainMap
    cm.rootSiteChain shouldBe Map("xx1" -> "xx1.me1", "xx2" -> "xx1.me1.xx2.me2")
    cm.mineSiteChain shouldBe Map("me1" -> "xx1.me1", "me2" -> "xx1.me1.xx2.me2")
    cm.xtraSiteChain shouldBe Map("xx1j" -> "xx1j.me1j", "xx2j" -> "xx1j.me1j.xx2j.me2j",
                                  "xx1b" -> "xx1b.me1b", "xx2b" -> "xx1b.me1b.xx2b.me2b",
                                  "me1j" -> "xx1j.me1j", "me2j" -> "xx1j.me1j.xx2j.me2j",
                                  "me1b" -> "xx1b.me1b", "me2b" -> "xx1b.me1b.xx2b.me2b")
    cm.rootSiteLinks shouldBe Map("xx1"  -> "xx1", "xx2"  -> "xx2", "xx1j" -> "xx1", "xx2j" -> "xx2",
                                  "xx1b" -> "xx1", "xx2b" -> "xx2", "me1"  -> "xx1", "me2"  -> "xx2",
                                  "me1j" -> "xx1", "me2j" -> "xx2", "me1b" -> "xx1", "me2b" -> "xx2")

    m.request.getAttribute("currentSite") shouldBe "xx1.me1"
    m.request.getAttribute("rootSite") shouldBe "xx1"
  }

  it should "populate site-domain with info about current site (ru)" in {
    val ruLocale = new Locale("ru")
    m.request.setPreferredLocales(List(ruLocale).asJava)
    when(m.domainService.resolveRootSite("example.org", ruLocale)).thenReturn(SiteChain("xx1.xx2"))

    m.mod.setup(m.request)

    siteState.currentSite shouldBe SiteChain("xx1.me1.xx2.me2")
    siteState.defaultSite shouldBe SiteChain("xx1.me1")
    siteState.rootSite shouldBe "xx2"

    m.request.getAttribute("currentSite") shouldBe "xx1.me1.xx2.me2"
    m.request.getAttribute("rootSite") shouldBe "xx2"
  }

  it should "overwrite site if passed as request parameter" in {
    m.request.setParameter("osite", "me2")
    m.mod.setup(m.request)

    siteState.currentSite shouldBe SiteChain("xx1.me1.xx2.me2")
    siteState.defaultSite shouldBe SiteChain("xx1.me1")
    siteState.rootSite shouldBe "xx2"

    m.request.getAttribute("currentSite") shouldBe "xx1.me1.xx2.me2"
    m.request.getAttribute("rootSite") shouldBe "xx2"
  }

}

@RunWith(classOf[JUnitRunner])
class CurrencySetupModSpec extends FlatSpec with MockitoSugar with BeforeAndAfter with Matchers {
  var m: MyMock = _

  before {
    m = new MyMock
  }

  class MyMock extends com.coldcore.haala.web.test.MyMock with SetupMods {
    val mod = new CurrencySetupMod { override val staticBlock = m.staticBlock }
  }

  val prefs = new UserPreferences

  it should "set currency preference" in {
    m.runSetupMods("fr")
    prefs.currency = ""
    m.request.getSession.setAttribute(WebConstants.USER_PREFERENCES, prefs)

    m.mod.setup(m.request)
    prefs.currency shouldBe "EUR"
  }

  it should "use default currency" in {
    m.runSetupMods("ru")
    prefs.currency = ""
    m.request.getSession.setAttribute(WebConstants.USER_PREFERENCES, prefs)

    m.mod.setup(m.request)
    prefs.currency shouldBe "GBP"
  }

  it should "use cookie currency" in {
    m.runSetupMods()
    prefs.currency = ""
    m.request.getSession.setAttribute(WebConstants.USER_PREFERENCES, prefs)
    m.request.setCookies(new Cookie("system", "1,mc1,USD".encodeBase64))

    m.mod.setup(m.request)
    prefs.currency shouldBe "USD"
  }

}

@RunWith(classOf[JUnitRunner])
class ExtraSetupModSpec extends FlatSpec with MockitoSugar with BeforeAndAfter with Matchers {
  var m: MyMock = _

  before {
    m = new MyMock
  }

  class MyMock extends com.coldcore.haala.web.test.MyMock with SetupMods {
    val mod = new ExtraSetupMod
    runSetupMods()
  }

  it should "add controller options" in {
    m.mod.setup(m.request)
    m.request.xattr.ctrlOptions shouldBe List("html5", "layout-modern")
  }

}
