package com.coldcore.haala
package web
package core

import org.scalatest.{BeforeAndAfter, FlatSpec, Matchers, WordSpec}
import web.test.MyMock
import org.scalatest.mockito.MockitoSugar
import org.mockito.Mockito._
import org.mockito.ArgumentMatchers._
import org.mockito.ArgumentCaptor
import org.junit.runner.RunWith
import org.scalatest.junit.JUnitRunner
import com.coldcore.haala.service.SearchInput
import com.coldcore.haala.core.SiteChain.Implicits._

@RunWith(classOf[JUnitRunner])
class CoreActionsSpec extends WordSpec with MockitoSugar with BeforeAndAfter {
  var m: MyMock = _
  val ca = new CoreActions

  before {
    m = new MyMock
  }

  val searchCmd = SearchCommand(0, 10, "", "", 1, params = "a" :: "b" :: Nil)
  val si = SearchInput(0, 10, "id desc")

  def setupSearchBeans {
    val sh = SearchHelper(false, "objs", "xx1", m.request, 1, searchCmd)
    val sb = new SearchBeansMap
    sb += ("objs-searchResult" -> CachedSearchResult(si, sh, Map('a -> "b")))
    m.request.getSession.setAttribute(WebConstants.SEARCH_BEANS, sb)
  }

  "popCachedResult" should {
    "return cached object on match" in {
      setupSearchBeans
      val sh = SearchHelper(false, "objs", "xx1", m.request, 0, searchCmd.copy(params = Nil))
      assertResult(Some(Map('a -> "b"))) { ca.popCachedResult(si, sh) }
    }

    "not return cached object on refresh" in {
      setupSearchBeans
      val sh = SearchHelper(true, "objs", "xx1", m.request, 0, searchCmd.copy(params = Nil))
      assertResult(None) { ca.popCachedResult(si, sh) }
    }

    "not return cached object on mismatch" in {
      setupSearchBeans
      val si2 = SearchInput(10, 20, "val asc")
      val sh = SearchHelper(false, "objs", "xx1", m.request, 0, searchCmd.copy(params = Nil)) //match
      val sh2 = SearchHelper(false, "objs", "xx1.xx2", m.request, 0, searchCmd.copy(params = Nil))
      val sh3 = SearchHelper(false, "prds", "xx1", m.request, 0, searchCmd.copy(params = Nil))
      assertResult(None) { ca.popCachedResult(si2, sh) }
      assertResult(None) { ca.popCachedResult(si, sh2) }
      assertResult(None) { ca.popCachedResult(si, sh3) }
    }

    "return cached object on match (full comparison)" in {
      setupSearchBeans
      val sh = SearchHelper(false, "objs", "xx1", m.request, 1, searchCmd)
      assertResult(Some(Map('a -> "b"))) { ca.popCachedResult(si, sh, fullcomp = true) }
    }

    "not return cached object on mismatch (full comparison)" in {
      setupSearchBeans
      val sh2 = SearchHelper(false, "objs", "xx1", m.request, 0, searchCmd.copy(params = "a" :: "b" :: Nil))
      val sh3 = SearchHelper(false, "objs", "xx1", m.request, 1, searchCmd.copy(params = "b" :: "a" :: Nil))
      assertResult(None) { ca.popCachedResult(si, sh2, fullcomp = true) }
      assertResult(None) { ca.popCachedResult(si, sh3, fullcomp = true) }
    }

  }

}

@RunWith(classOf[JUnitRunner])
class AttrRequestExtSpec extends FlatSpec with MockitoSugar with BeforeAndAfter with Matchers {
  var m: MyMock = _

  before {
    m = new MyMock
  }

  class MyMock extends com.coldcore.haala.web.test.MyMock {
    val xattr = new AttrRequestExt(m.request)
    val perm = xattr.perm
    val session = request.getSession(true)
  }

  it should "set attribute" in {
    m.xattr.set("key-A", "val-A")
    m.request.getAttribute("key-A") shouldBe "val-A"

    m.xattr + ("key-B", "val-B")
    m.request.getAttribute("key-B") shouldBe "val-B"

    m.xattr + ("key-C" -> "val-C")
    m.request.getAttribute("key-C") shouldBe "val-C"

    m.xattr + ("key-D" -> "val-D", "key-E" -> "val-E")
    m.request.getAttribute("key-D") shouldBe "val-D"
    m.request.getAttribute("key-E") shouldBe "val-E"
  }

  it should "get attribute" in {
    m.request.setAttribute("key-A", "val-A")
    m.xattr.get[String]("key-A") shouldBe Some("val-A")

    m.request.setAttribute("key-B", "val-B")
    m.xattr[String]("key-B") shouldBe "val-B"

    m.xattr[String]("key-C", "val-C") shouldBe "val-C"
  }

  it should "remove attribute" in {
    m.request.setAttribute("key-A", "val-A")
    m.request.setAttribute("key-B", "val-B")
    m.request.setAttribute("key-C", "val-C")
    m.request.setAttribute("key-D", "val-D")

    m.xattr.rem("key-A")
    m.request.getAttribute("key-A") shouldBe null

    m.xattr - "key-B"
    m.request.getAttribute("key-B") shouldBe null

    m.xattr - ("key-C", "key-D")
    m.request.getAttribute("key-C") shouldBe null
    m.request.getAttribute("key-D") shouldBe null
  }

  it should "set session attribute" in {
    m.perm.set("key-A", "val-A")
    m.session.getAttribute("key-A") shouldBe "val-A"

    m.perm + ("key-B", "val-B")
    m.session.getAttribute("key-B") shouldBe "val-B"

    m.perm + ("key-C" -> "val-C")
    m.session.getAttribute("key-C") shouldBe "val-C"

    m.perm + ("key-D" -> "val-D", "key-E" -> "val-E")
    m.session.getAttribute("key-D") shouldBe "val-D"
    m.session.getAttribute("key-E") shouldBe "val-E"
  }

  it should "get session attribute" in {
    m.session.setAttribute("key-A", "val-A")
    m.perm.get[String]("key-A") shouldBe Some("val-A")

    m.session.setAttribute("key-B", "val-B")
    m.perm[String]("key-B") shouldBe "val-B"

    m.perm[String]("key-C", "val-C") shouldBe "val-C"
  }

  it should "remove session attribute" in {
    m.session.setAttribute("key-A", "val-A")
    m.session.setAttribute("key-B", "val-B")
    m.session.setAttribute("key-C", "val-C")
    m.session.setAttribute("key-D", "val-D")

    m.perm.rem("key-A")
    m.session.getAttribute("key-A") shouldBe null

    m.perm - "key-B"
    m.session.getAttribute("key-B") shouldBe null

    m.perm - ("key-C", "key-D")
    m.session.getAttribute("key-C") shouldBe null
    m.session.getAttribute("key-D") shouldBe null
  }

}
