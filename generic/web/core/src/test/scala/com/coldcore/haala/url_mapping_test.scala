package com.coldcore.haala
package web
package core

import org.scalatest.mockito.MockitoSugar
import org.mockito.Mockito._
import org.mockito.ArgumentMatchers._
import org.scalatest._
import org.junit.runner.RunWith
import org.scalatest.junit.JUnitRunner
import com.coldcore.haala.core.{SiteChainMap, SiteState}
import com.coldcore.haala.core.SiteChain.Implicits._

@RunWith(classOf[JUnitRunner])
class CustomUrlHandlerMappingSpec extends WordSpec with MockitoSugar with BeforeAndAfter {
  var m: MyMock = _

  before {
    m = new MyMock
  }

  class MyMock extends com.coldcore.haala.web.test.MyMock {
    val handler = new CustomUrlHandlerMapping |< { x => x.serviceContainer = serviceContainer }
    request.xattr.perm + (WebConstants.SITE_CHAIN_MAP -> siteChainMap, WebConstants.SITE_STATE -> siteState)
    when(settingService.query("UrlMapping", ".sys")).thenReturn(dummy_config_sys)
    when(settingService.query("UrlMapping", ".mc1")).thenReturn(dummy_config_mc)
    when(settingService.query("UrlMapping", ".xx1")).thenReturn(null)
    when(settingService.query("UrlMapping", ".mh1")).thenReturn(dummy_config_mh)
  }

  val siteState =
    SiteState(
      currentDomainSite = "", currentSite = "xx1.mc1",
      currentDomain = "", currentDomainId = 0, currentDomainType = "", currentDomainStyle = "")

  val siteChainMap =
    SiteChainMap(Map.empty, Map("mc1" -> "xx1.mc1", "mc2" -> "xx1.mc1.xx2.mc2"), Map.empty, Map.empty)

  private val dummy_config_sys = """
     |{
     |  "mapping": [
     |    { "pattern": "dwr/engine.js", "handler": "dwrCtrl" },
     |    { "pattern": "dwr/**", "handler": "dwrCtrl" },
     |
     |    { "pattern": "sitemap.xml", "handler": "sitemapCtrl" },
     |    { "pattern": "sitemap.htm", "handler": "sitemapCtrl" },
     |    { "pattern": "**/favicon.ico", "handler": "faviconCtrl" },
     |
     |    { "pattern": "/", "handler": "homeCtrl" },
     |    { "pattern": "blank.htm", "handler":  "blankCtrl" },
     |    { "pattern": "**/*", "handler":  "notfoundCtrl" }
     |  ],
     |  "unorderPrefix": "dwr"
     |}
     """.stripMargin.trim

  private val dummy_config_mc = """
     |{
     |  "mapping": [
     |    { "pattern": "my/account.htm", "handler": "myAccountCtrl" },
     |    { "pattern": "my/details.htm", "handler": "myDetailsCtrl" },
     |    { "pattern": "**/favicon.ico", "handler": "advFaviconCtrl" }
     |  ],
     |  "unorderPrefix": "ejb"
     |}
     """.stripMargin.trim

  private val dummy_config_mh = """
     |{
     |  "mapping": [
     |    { "pattern": "ejb/some.*", "handler": "val_A2" },
     |    { "pattern": "ejb/some/**", "handler": "val_A5" },
     |    { "pattern": "ejb/**", "handler": "val_A1" },
     |    { "pattern": "ejb/some", "handler": "val_A4" },
     |    { "pattern": "ejb/some.jar", "handler": "val_A3" },
     |
     |    { "pattern": "foo/**", "handler": "val_B1" },
     |    { "pattern": "foo/some/**", "handler": "val_B2" },
     |    { "pattern": "foo/some/bar", "handler": "val_B3" },
     |
     |    { "pattern": "bar/some/*.jpg", "handler": "val_C1" },
     |    { "pattern": "bar/**/*.jpg", "handler": "val_C2" }
     |  ],
     |  "unorderPrefix": "ejb"
     |}
     """.stripMargin.trim

  "loadUrlMapping" should {
    "return merged mppings" in {
      val c = CustomUrlHandlerMapping.loadUrlMapping("xx1.mc1", m.serviceContainer)
      assertResult(List("ejb", "dwr")) { c.unorderPrefix.split(",").toList }
      assertResult(10) { c.mapping.size }
      assertResult("/my/account.htm") { c.mapping.head.pattern }
      assertResult("/**/*") { c.mapping.last.pattern }
      assertResult((1, "advFaviconCtrl")) { val x = c.mapping.filter(_.pattern == "/**/favicon.ico" ); (x.size, x.head.handler) }
    }
  }

  "lookupHandlerMapping" should {
    val f = (pattern: String) => m.handler.lookupHandlerMapping(pattern, m.request).get.handler
    "return correct handler name (strict match)" in {
      siteState.currentSite = "xx1.mc1"
      assertResult("myAccountCtrl") { f("/my/account.htm") }
      assertResult("sitemapCtrl") { f("/sitemap.htm") }
      assertResult("homeCtrl") { f("/") }
    }
    "return correct handler name (pattern match)" in {
      assertResult("advFaviconCtrl") { f("/foo/bar/favicon.ico") }
      assertResult("dwrCtrl") { f("/dwr/nojs.js") }
      assertResult("notfoundCtrl") { f("/foo/bar") }
    }

    "return correct handler name (unordered pattern match)" in {
      siteState.currentSite = "xx1.mh1"
      assertResult("val_A4") { f("/ejb/some") }
      assertResult("val_A3") { f("/ejb/some.jar") }
      assertResult("val_A2") { f("/ejb/some.var") }
      assertResult("val_A5") { f("/ejb/some/any/my.var") }
      assertResult("val_A1") { f("/ejb/any") }
      assertResult("val_A1") { f("/ejb/any/some.var") }
    }
    "return correct handler name (ordered pattern match)" in {
      siteState.currentSite = "xx1.mh1"
      assertResult("val_B3") { f("/foo/some/bar") }
      assertResult("val_B1") { f("/foo/some/any") }
      assertResult("val_B1") { f("/foo/some") }
      assertResult("val_B1") { f("/foo/any") }

      assertResult("val_C1") { f("/bar/some/any.jpg") }
      assertResult("val_C2") { f("/bar/some/foo/any.jpg") }
      assertResult("val_C2") { f("/bar/rest/foo/any.jpg") }
      assertResult("val_C2") { f("/bar/rest/any.jpg") }
      assertResult("val_C2") { f("/bar/any.jpg") }
    }
  }

}
