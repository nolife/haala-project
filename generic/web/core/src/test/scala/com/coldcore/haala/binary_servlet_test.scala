package com.coldcore.haala
package web
package core

import org.scalatest.{BeforeAndAfter, WordSpec}
import java.io.{ByteArrayInputStream, InputStream}
import web.test.MyMock
import org.scalatest.mockito.MockitoSugar
import org.mockito.Mockito._
import org.mockito.ArgumentMatchers._
import org.mockito.ArgumentCaptor
import org.junit.runner.RunWith
import org.scalatest.junit.JUnitRunner
import com.coldcore.haala.service.test.ByteBody
import com.coldcore.haala.web.core.BinaryFile.{InputStreamData, StringData}
import java.io.{File => JFile}
import java.text.SimpleDateFormat

@RunWith(classOf[JUnitRunner])
class BinaryServletSpec extends WordSpec with MockitoSugar with BeforeAndAfter {
  var m: MyMock = _
  var servlet: BinaryServlet = _

  before {
    m = new MyMock
    servlet = new BinaryServlet { serviceContainer = m.serviceContainer }
    when(m.settingService.querySystem("MimeTypes")).thenReturn(mimeTypes)
  }

  val mimeTypes =
    """
      [
        { "ext": "gif", "type": "image/gif" },
        { "ext": "jpeg", "type": "image/jpeg" },
        { "ext": "jpg", "type": "image/jpeg" },
        { "ext": "png", "type": "image/png" },
        { "ext": "css", "type": "text/css" },
        { "ext": "htm", "type": "text/html; charset=UTF-8" },
        { "ext": "html", "type": "text/html; charset=UTF-8" },
        { "ext": "txt", "type": "text/plain; charset=UTF-8" },
        { "ext": "js", "type": "text/javascript; charset=UTF-8" },
        { "ext": "pdf", "type": "application/pdf" }
      ]
    """

  "forward404" should {
    "display default page if no 404 page set" in {
      servlet.service(m.request, m.response)
      assertResult(404) { m.response.getStatus }
      assertResult(" ") { m.response.getContentAsString }
      assertResult("text/html") { m.response.getContentType }
    }

    "forward to specific 404 page" in {
      servlet.page404 = "404.htm"
      servlet.service(m.request, m.response)
      assertResult("/404.htm") { m.response.getForwardedUrl }
    }
  }

  "service" should {
    "output text file to response" in {
      val timeout = System.currentTimeMillis+servlet.stayInCache
      val timeout2 = System.currentTimeMillis-servlet.stayInCache
      assert(timeout2 <= servlet.getLastModified(null))
      assert(timeout2+500 >= servlet.getLastModified(null))
      m.request.setAttribute(WebConstants.BINARY_FILE, BinaryFile("filename.txt", "1234567890"))
      servlet.service(m.request, m.response)
      assertResult(10) { m.response.getContentLength }
      assertResult("1234567890") { m.response.getContentAsString }
      assertResult("inline; filename=\"filename.txt\"") { m.response.getHeader("Content-Disposition") }
      assertResult("text/plain; charset=UTF-8") { m.response.getContentType }
      val sdf = new SimpleDateFormat("EEE, d MMM yyyy HH:mm:ss z") // Thu, 25 Aug 2016 12:27:49 GMT
      assert(timeout-1000 <= sdf.parse(m.response.getHeader("Expires")).getTime)
      assert(timeout+1000 >= sdf.parse(m.response.getHeader("Expires")).getTime)
    }

    "output text file to response (no cache)" in {
      val timeout = System.currentTimeMillis
      servlet.nocache = true
      assert(timeout <= servlet.getLastModified(null))
      assert(timeout+500 >= servlet.getLastModified(null))
      m.request.setAttribute(WebConstants.BINARY_FILE, BinaryFile("filename.txt", "1234567890"))
      servlet.service(m.request, m.response)
      val sdf = new SimpleDateFormat("EEE, d MMM yyyy HH:mm:ss z") // Thu, 25 Aug 2016 12:27:49 GMT
      assert(timeout-1000 <= sdf.parse(m.response.getHeader("Last-Modified")).getTime)
      assert(timeout+1000 >= sdf.parse(m.response.getHeader("Last-Modified")).getTime)
    }

    "output arbitrary file to response" in {
      m.request.setAttribute(WebConstants.BINARY_FILE, BinaryFile("filename.dat", "1234567890"))
      servlet.service(m.request, m.response)
      assertResult("attachment; filename=\"filename.dat\"") { m.response.getHeader("Content-Disposition") }
      assertResult("application/x-download") { m.response.getContentType }
    }

    "output file to response without headers (pure)" in {
      m.request.setAttribute(WebConstants.BINARY_FILE,
        new BinaryFile("filename.txt", new StringData("1234567890")) { pureOutput = true })
      servlet.service(m.request, m.response)
      assertResult(null) { m.response.getHeader("Content-Disposition") }
      assertResult(null) { m.response.getContentType }
      assertResult("1234567890") { m.response.getContentAsString }
    }

    "output text file to response with custom mime type" in {
      m.request.setAttribute(WebConstants.BINARY_FILE,
        new BinaryFile("filename.txt",
          new InputStreamData(new ByteArrayInputStream("1234567890".getBytes), 24)) { mimeType = "plain/x-download" })
      servlet.service(m.request, m.response)
      assertResult("attachment; filename=\"filename.txt\"") { m.response.getHeader("Content-Disposition") }
      assertResult("plain/x-download") { m.response.getContentType }
      assertResult(24) { m.response.getContentLength }
      assertResult("1234567890") { m.response.getContentAsString }
    }

    "forward to 404 if no data provided" in {
      class NullData extends BinaryFile.Data {
        override val has: Boolean = false
        override def stream: InputStream = null
      }
      m.request.setAttribute(WebConstants.BINARY_FILE, new BinaryFile("filename.txt", new NullData))
      servlet.service(m.request, m.response)
      assertResult(404) { m.response.getStatus }
    }
  }

}

@RunWith(classOf[JUnitRunner])
class BinaryFileSpec extends WordSpec with MockitoSugar with BeforeAndAfter {
  var m: MyMock = _

  before {
    m = new MyMock
  }

  "InputStreamData" should {
    "return length if available" in {
      assertResult(0) { BinaryFile("foo.txt", new ByteArrayInputStream("1234567890".getBytes)).data.length }
      assertResult(24) { BinaryFile("foo.txt", new ByteArrayInputStream("1234567890".getBytes), 24).data.length }
    }

    "tell if data available" in {
      assertResult(true) { BinaryFile("foo.txt", new ByteArrayInputStream("1234567890".getBytes)).data.has }
      assertResult(true) { BinaryFile("foo.txt", new ByteArrayInputStream("".getBytes)).data.has }
    }

    "stream data" in {
      assertResult(10) { BinaryFile("foo.txt", new ByteArrayInputStream("1234567890".getBytes)).data.stream.available }
    }
  }

  "BodyData" should {
    "return length if available" in {
      assertResult(0) { BinaryFile("foo.txt", new ByteBody("1234567890".getBytes)).data.length }
      assertResult(10) { BinaryFile("foo.txt", new ByteBody("1234567890".getBytes) { xSaved = true; xPath = "bar.txt" }).data.length }
    }

    "tell if data available" in {
      assertResult(true) { BinaryFile("foo.txt", new ByteBody("1234567890".getBytes) { xSaved = true; xPath = "bar.txt" }).data.has }
      assertResult(true) { BinaryFile("foo.txt", new ByteBody("".getBytes) { xSaved = true; xPath = "bar.txt" }).data.has }
      assertResult(false) { BinaryFile("foo.txt", new ByteBody("1234567890".getBytes)).data.has }
    }

    "stream data" in {
      assertResult(10) { BinaryFile("foo.txt", new ByteBody("1234567890".getBytes) { xSaved = true; xPath = "bar.txt" }).data.stream.available }
    }
  }

  "JFileData" should {
    "not return length for non existent file" in {
      assertResult(0) { BinaryFile("foo.txt", new JFile("unknown.dat")).data.length }
    }

    "tell data unavailable for non existent file" in {
      assertResult(false) { BinaryFile("foo.txt", new JFile("unknown.dat")).data.has }
    }
  }

  "factory" should {
    "create BinaryFile from various data sources" in {
      assertResult("foo.txt") { BinaryFile("foo.txt", "1234567890".getBytes).filename }
      assertResult("foo.txt") { BinaryFile("foo.txt", "1234567890").filename }
      assertResult("foo.txt") { BinaryFile("foo.txt", new JFile("unknown.dat")).filename }
      assertResult("foo.txt") { BinaryFile("foo.txt", new ByteArrayInputStream("1234567890".getBytes)).filename }
      assertResult("foo.txt") { BinaryFile("foo.txt", new ByteBody("1234567890".getBytes)).filename }
    }
  }
}
