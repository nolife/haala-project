package com.coldcore.haala
package web                        
package core

import org.springframework.web.multipart.{MultipartFile, MultipartHttpServletRequest}
import org.springframework.web.multipart.commons.{CommonsMultipartFile, CommonsMultipartResolver}
import org.springframework.web.servlet.ModelAndView
import org.springframework.web.servlet.handler.{AbstractUrlHandlerMapping, SimpleMappingExceptionResolver}
import org.springframework.web.context.support.WebApplicationContextUtils
import org.springframework.web.multipart.support.DefaultMultipartHttpServletRequest
import org.springframework.util.{AntPathMatcher, LinkedMultiValueMap, MultiValueMap, PathMatcher}
import org.apache.commons.fileupload.servlet.ServletFileUpload
import org.apache.commons.fileupload.disk.DiskFileItemFactory
import org.apache.commons.fileupload.{FileItem, ProgressListener}

import java.io.{BufferedInputStream, ByteArrayInputStream, FileInputStream, InputStream, File => JFile}
import java.util.{Comparator, Locale}
import javax.servlet.{ServletConfig, ServletException}
import javax.servlet.http._

import beans.BeanProperty
import annotation.tailrec
import collection.mutable.ListBuffer
import scala.collection.JavaConverters._
import com.google.gson.Gson
import com.coldcore.misc5.CByte._
import com.coldcore.haala.core._
import com.coldcore.haala.core.SiteUtil._
import com.coldcore.haala.core.GeneralUtil._
import com.coldcore.haala.core.Constants._
import domain.{Domain, Role, User}
import directive.freemarker.FreemarkerDirectives
import service._
import service.MiscService.FreemarkerTemplateIN
import service.FileService.Body
import CustomMultipartResolver.TemporaryFile
import net.sf.javainetlocator.InetAddressLocator

trait Dependencies extends service.Dependencies {
  @BeanProperty var resourceLoaderHolder: ResourceLoaderHolder = _
  @BeanProperty var applicationContextHolder: ApplicationContextHolder = _
  @BeanProperty var templatesCache: Cache[String] = _
  @BeanProperty var freemarkerDirectives: FreemarkerDirectives = _

  override val staticBlock = new StaticBlock
  override lazy val sb = staticBlock
}

trait Setup { // dependencies trait
  var setupMods: List[SetupMod] = _
  def setSetupMods(x: JList[SetupMod]) = setupMods = x.toList
}

trait SetupMod {
  def setup(request: HttpServletRequest)
}

object BinaryFile {
  trait Data {
    val (length, has) = (0L, true)
    def stream: InputStream
  }

  trait DataMaker[-A] {
    def make(a: A, length: Long): Data
  }

  implicit val ByteDataMaker: DataMaker[Array[Byte]] = new DataMaker[Array[Byte]] {
    override def make(data: Array[Byte], length: Long) = new ByteData(data)
  }

  implicit val StringDataMaker: DataMaker[String] = new DataMaker[String] {
    override def make(str: String, length: Long) = new StringData(str)
  }

  implicit val JFileDataMaker: DataMaker[JFile] = new DataMaker[JFile] {
    override def make(file: JFile, length: Long) = new JFileData(file)
  }

  implicit val BodyDataMaker: DataMaker[Body] = new DataMaker[Body] {
    override def make(body: Body, length: Long) = new BodyData(body)
  }

  implicit val InputStreamDataMaker: DataMaker[InputStream] = new DataMaker[InputStream] {
    override def make(in: InputStream, length: Long) = new InputStreamData(in, length)
  }

  class ByteData(data: Array[Byte]) extends Data {
    override val length: Long = data.length
    override def stream: InputStream = new ByteArrayInputStream(data)
  }

  class StringData(str: String) extends Data {
    override val length: Long = str.length
    override def stream: InputStream = new ByteArrayInputStream(str.bytesUTF8)
  }

  class JFileData(file: JFile) extends Data {
    override val has: Boolean = file.exists
    override val length: Long = has ? file.length | 0
    override def stream: InputStream = new BufferedInputStream(new FileInputStream(file))
  }

  class BodyData(body: Body) extends Data {
    override val has: Boolean = body.exists
    override val length: Long = has ? body.size | 0
    override def stream: InputStream = body.sourceStream
  }

  class InputStreamData(in: InputStream, override val length: Long = 0) extends Data {
    override def stream: InputStream = new BufferedInputStream(in)
  }

  def apply[A : DataMaker](filename: String, v: A, length: Long = 0): BinaryFile = {
    val m = implicitly[DataMaker[A]]
    new BinaryFile(filename, m.make(v, length))
  }
}

class BinaryFile(val filename: String, val data: BinaryFile.Data) {
  var (mimeType, action, pureOutput) = ("", "", false)
  def asBytes = withOpen(data.stream) { toByteArray }
  def asString = new String(asBytes, UTF8)
}

class BinaryServlet extends HttpServletTrg with Dependencies with SettingsReader {
  @BeanProperty val stayInCache = 1000L*60L*60L*24L*365L //1 year
  @BeanProperty var nocache: Boolean = _
  @BeanProperty var page404: String = _

  /** Date when a page was last modified (servlet's method). */
  override def getLastModified(request: HttpServletRequest) =
    (nocache || setting("Dev/NoCache").isTrue) ? //cache disabled (dev mode flag)
      System.currentTimeMillis | System.currentTimeMillis-stayInCache

  /** Set headers to forbid caching. */
  private def disableCaching(response: HttpServletResponse) {
    case class H[T](k: String, v: T)
    val mills = System.currentTimeMillis
    (H("Expires", 1L) :: H("Date", mills) :: H("Last-Modified", mills) :: H("ETag", mills+"ETag") ::
     H("Cache-Control", "no-cache, must-revalidate, no-store, max-age=0") :: H("Pragma", "no-cache") :: Nil).foreach {
      case H(k, v: Long) => response.setDateHeader(k, v)
      case H(k, v: String) => response.setHeader(k, v)
      case _ => // disable compiler non-exhaustive warning
    }
  }

  /** Set headers to allow caching. */
  private def enableCaching(response: HttpServletResponse) {
    response.setDateHeader("Expires", System.currentTimeMillis+stayInCache)
    response.setHeader("Cache-Control", "public, max-age="+(stayInCache/1000L))
    response.setHeader("Pragma", "") // container could set 'no-cache'
  }

  /** Forward to a 404 page. */
  private def forward404(request: HttpServletRequest, response: HttpServletResponse) =
    Option(page404) match {
      case Some(_) => request.getSession.getServletContext.getRequestDispatcher("/"+page404).forward(request, response)
      case None =>
        //default 404 page with no text
        response.setStatus(HttpServletResponse.SC_NOT_FOUND)
        response.setContentType("text/html")
        response.resetBuffer()
        response.getOutputStream.write(" ".getBytes)
    }

  class MimeType {
    @BeanProperty var ext: String = _
    @BeanProperty var `type`: String = _
  }

  /** All supported MIME types (default application/x-download). */
  private def fileMimeType(filename: String): String = {
    val ext = filename.contains(".") ? filename.split("\\.").last | ""
    val allMimeTypes = Option(new Gson().fromJson(setting("MimeTypes").safe,
      classOf[Array[MimeType]])).getOrElse(Array.empty[MimeType])
    allMimeTypes.find(_.ext == ext).getOrElse(new MimeType {`type` = "application/x-download"}).`type`
  }

  /** Return a MIME type based on a name or a type. */
  private def detectMimeType(o: BinaryFile): Option[String] =
    Option(o.mimeType match {
      case "" if !o.pureOutput => fileMimeType(o.filename)
      case x if x != "" => x
      case _ => null
    })

  /** Return an action (attachment/inline) based on a MIME type. */
  private def detectAction(o: BinaryFile): Option[String] =
    Option(o.action match {
      case "" if !o.pureOutput =>
        detectMimeType(o) match {
          case Some(m) if m.endsWith("x-download") => "attachment"
          case _ => "inline"
        }
      case x if x != "" => x
      case _ => null
    })

  /** Prepare response headers and write supplied data into it. */
  override def service(request: HttpServletRequest, response: HttpServletResponse) {
    val o = request.xattr.get[BinaryFile](WebConstants.BINARY_FILE).orNull
    if (o == null || !o.data.has) forward404(request, response) //forward to 404
    else {
      detectAction(o).foreach { x =>
        response.setHeader("Content-Disposition", s"""$x; filename="${o.filename}"""") }
      detectMimeType(o).foreach { x =>
        response.setContentType(x)
        if (o.data.length.toInt > 0) response.setContentLength(o.data.length.toInt)
      }

      //either forbid a browser to cache a file or tell it for how long to keep it in cache
      if (nocache || setting("Dev/NoCache").isTrue) disableCaching(response) //cache disabled (dev mode flag)
      else enableCaching(response)

      //write data into a response
      response.resetBuffer()
      withOpen(o.data.stream) { inputToOutput(_, response.getOutputStream) }
    }
  }

}

trait HttpServletTrg {
  def getLastModified(request: HttpServletRequest): Long
  def service(request: HttpServletRequest, response: HttpServletResponse)
}

class HttpServletDlg extends HttpServlet {
  var target: HttpServletTrg = _

  override def init(config: ServletConfig) {
    super.init(config)
    val wac = WebApplicationContextUtils.getRequiredWebApplicationContext(getServletContext)
    target = wac.getBean(getServletName, classOf[HttpServletTrg])
  }

  override def getLastModified(request: HttpServletRequest) = target.getLastModified(request)

  override def service(request: HttpServletRequest, response: HttpServletResponse) =
    try { target.service(request, response) } catch { case e: Throwable => throw new ServletException(e) }
}

class UploadBean {
  @BeanProperty var read: Long = _
  @BeanProperty var total: Long = _
  @BeanProperty var error: Int = _

  def serializeAsMap: Map[Symbol,Any] = Map('read -> read, 'total -> total, 'error -> error)
}

/** Resolve multipart requests to upload files. Declare as Spring bean with "multipartResolver" ID. */
object CustomMultipartResolver {
  /** Bean to store an uploaded file info and content. */
  case class TemporaryFile(name: String, field: String, size: Long, body: Body)
}

class CustomMultipartResolver extends CommonsMultipartResolver with Dependencies with SettingsReader {
  import CustomMultipartResolver._

  case class MultipartParsingResultHolder(multipartFiles: MultiValueMap[String,MultipartFile],
                                          multipartParameters: JMap[String,Array[String]])

  class UploadProgressListener(bean: UploadBean) extends ProgressListener {
    override def update(pBytesRead: Long, pContentLength: Long, pItems: Int) = bean.read = pBytesRead
  }

  private def parseFiles(fileItems: JList[FileItem], encoding: String): MultipartParsingResultHolder = {
    def r = super.parseFileItems(fileItems, encoding)
    MultipartParsingResultHolder(r.getMultipartFiles, r.getMultipartParameters)
  }

  /** Create a bean in a session to store an upload status. */
  private def prepareUploadBean(request: HttpServletRequest, t: Long): UploadBean = {
    val ubean = new UploadBean { total = t }
    request.xattr.perm + (WebConstants.UPLOAD_BEAN -> ubean)
    ubean
  }

  /** Create a multipart object with a progress listener. */
  private def multipartParsingResult(request: HttpServletRequest): MultipartParsingResultHolder = {
    val sfu = new ServletFileUpload(new DiskFileItemFactory)
    val lis = new UploadProgressListener(request.xattr.perm[UploadBean](WebConstants.UPLOAD_BEAN))
    val enc = determineEncoding(request)
    val upl = prepareFileUpload(enc)
    upl.setProgressListener(lis)
    parseFiles(sfu.parseRequest(request), enc)
  }

  /** Calculate a total size of all files about to upload. */
  private def totalSize(mpr: MultipartParsingResultHolder) =
    (0L /: mpr.multipartFiles.asScala.values.map(_.asScala.head.getSize))(_+_)

  /** Return a list with valid files. */
  private def filterFiles(mpr: MultipartParsingResultHolder) =
    mpr.multipartFiles.asScala.values.map(_.head).filter { file =>
      val name = file.asInstanceOf[CommonsMultipartFile].getFileItem.getName
      file.getSize > 0 || name != null && name.trim != ""
    }

  /** Create a file in a temporary directory to save an uploaded content. */
  @tailrec private def generateFile: Body = sc.get[FileService].body match {
    case x if !x.exists => x
    case _ => generateFile
  }

  /** Process an uploaded file creating a bean for it and saving its content to a temporary directory. */
  private def processFile(mf: CommonsMultipartFile): TemporaryFile = {
    val tf = TemporaryFile(mf.getFileItem.getName, mf.getName, mf.getSize, generateFile)
    tf.body.write(mf.getFileItem.getInputStream)
    tf
  }

  /** Process upload. */
  override def resolveMultipart(request: HttpServletRequest): MultipartHttpServletRequest = {
    val total = request.getContentLength.toLong //total request length
    val ubean = prepareUploadBean(request, total+1L) //create a status bean and add 1 to size (client waits on 99% for UPLOADED_TMP_FILES)
    val totalMB = math.round(total.toDouble/1024d/1024d) //total request size in MB
    val maxSize = configToBytes(setting("SystemParams").parseCSVMap()("uploadSize"))
    val maxSizeMB = math.round(maxSize.toDouble/1024d/1024d) //max size in MB

    request.xattr.perm - WebConstants.UPLOADED_TMP_FILES

    //check if size exceeded limit
    if (total > maxSize) {
      ubean.error = WebConstants.FACADE_ERROR_UPLOAD_OVERSIZE
      log.info("[custom-multipart-resolver] Attempt to upload "+totalMB+"MB+ exceeds limit of "+maxSizeMB+"MB")
      throw new WeakCoreException
    }

    //process upload
    val mpr = multipartParsingResult(request)
    val tfls = filterFiles(mpr).map(f => processFile(f.asInstanceOf[CommonsMultipartFile])).toList
      .sortWith((a,b) => a.field.compareTo(b.field) < 0)
    request.xattr.perm + (WebConstants.UPLOADED_TMP_FILES -> tfls)
    ubean.read = ubean.total

    //return a dummy object, everything was already processed
    new DefaultMultipartHttpServletRequest(request,
      new LinkedMultiValueMap[String,MultipartFile], new JHashMap[String,Array[String]], new JHashMap[String,String])
  }
}

object WebConstants {
  val COOKIE_EXPIRES = 60*60*24*30 //in seconds (1 month)

  val SEARCH_BEANS = "search-beans"
  val EX_SEARCH_BEANS = "ex-search-beans"
  val PARAMS_BEAN = "params-bean"
  val UPLOAD_BEAN = "upload-bean"
  val UPLOADED_TMP_FILES = "uploaded-tmp-files"
  val LAST_SECURED_URL = "last-secured-url"
  val RECENT_LINKS = "recent-links"
  val USER_STATE = "user-state"
  val USER_PREFERENCES = "user-preferences"
  val SITE_STATE = "site-state"
  val SITE_CHAIN_MAP = "site-chain-map"
  val CMS_STATE = "cms-state"
  val BINARY_FILE = "binary-file"

  /*** Facade errors used in java scripts and REST clients, must never be changed (reserved 0..200) ***/
  final val FACADE_ERROR_SYSTEM = 1
  final val FACADE_ERROR_SESSION_EXPIRED = 2
  final val FACADE_ERROR_OTHER = 3
  final val FACADE_ERROR_SECURITY_VIOLATED = 4
  final val FACADE_ERROR_REFRESH_ACTION = 5
  final val FACADE_ERROR_EMAIL_SERVER_DOWN = 6
  final val FACADE_ERROR_RAPID_SUBMIT = 7
  final val FACADE_ERROR_NO_COOKIES = 8
  final val FACADE_ERROR_INVALID_TURING = 9
  final val FACADE_ERROR_DOWNTIME = 10
  final val FACADE_ERROR_NO_UPLOAD = 11
  final val FACADE_ERROR_UPLOAD_OVERSIZE = 12

  final val FACADE_ERROR_INVALID_STATUS = 101
  final val FACADE_ERROR_NO_CREDIT = 102 //todo this is not yet used
  final val FACADE_ERROR_USER_AGREEMENT = 103

  final val FACADE_RESULT_OK = 0
}

class UserState {
  var (currentUserId, superUserId) = (0L, 0L)
  var currentUser: Option[User] = None
  var superUser: Option[User] = None
  var shellUsername = ""

  def hasCurrentUser: Boolean = currentUserId > 0
  def hasSuperUser: Boolean = superUserId > 0

  /** Remove domain objects so those could be re-fetched */
  def init {
    currentUser = None
    superUser = None
  }

  def enableSuperMode(user: User)(f: => Unit) = {
    if (!hasSuperUser) { //save the original user (first time only)
      superUserId = currentUserId
      superUser = currentUser
      f
    }

    shellUsername = user.username
    currentUserId = user.id
    currentUser = Some(user)
  }

  def disableSuperMode =
    if (hasSuperUser) {
      currentUserId = superUserId
      currentUser = superUser

      shellUsername = ""
      superUserId = 0
      superUser = None
    }
}

class UserPreferences {
  var currency = ""
}

class ExceptionResolver extends SimpleMappingExceptionResolver with Dependencies {
  @BeanProperty var errorView: String = _

  private lazy val LOG = new NamedLog("exception-resolver", log)

  override def resolveException(request: HttpServletRequest, response: HttpServletResponse,
                                handler: Any, ex: Exception): ModelAndView = {
    try { //load Freemarker error template into request attribute
      sc.get[MiscService]
        .freemarkerTemplate(FreemarkerTemplateIN(s"web/error.ftl:${request.xsite.current}", Map.empty))
        .foreach(request.setAttribute("errorContent", _))
    } catch { case _: Throwable => }

    val view = try { // /WEB-INF/jsp/error.jsp -> error -> /WEB-INF/jsp/error-mh.jsp
      request.xmisc.siteableJspPath(errorView.split("/").last.split("\\.").head)
    } catch { case _: Throwable => errorView }
    setDefaultErrorView(view)
    //LOG.error("Error occurred", ex)
    super.resolveException(request, response, handler, ex)
    //error will be handled by aspect
  }
}

class AttrRequestExt(rx: RequestX) {
  private val (request, session) = (rx.request, rx.request.getSession)
  private def instanceOf[T](x: Any) = x.asInstanceOf[T]
  private def notNull(x: Any) = x != null

  /** Permanent attributes (session attributes) */
  class Perm {
    def apply[T](name: String): T = get[T](name).get // perm[String]("foo") -> "bar" attribute
    def apply[T](name: String, default: => T): T = get[T](name).getOrElse(default) // ...

    def get[T] = (session.getAttribute _ andThen instanceOf[T] andThen Option.apply)(_)
    def set(name: String, value: Any) = session.setAttribute(name, value)
    def rem = session.removeAttribute _
    def has = (session.getAttribute _ andThen notNull)(_)
    def setOnce(name: String, value: Any) = if (!has(name)) set(name, value)

    // alias
    def +(kv: (String, Any)) = set(kv._1, kv._2)
    def +(kv: (String, Any)*) = kv.foreach(x => set(x._1, x._2))
    def +(name: String, value: Any) = set(name, value)
    def -(name: String) = rem(name)
    def -(name: String*) = name.foreach(rem)
  }

  val perm = new Perm

  def apply[T](name: String): T = get[T](name).get // perm[String]("foo") -> "bar" attribute
  def apply[T](name: String, default: => T): T = get[T](name).getOrElse(default) // ...

  /** Temporary attributes (request attributes) */
  def get[T] = (request.getAttribute _ andThen instanceOf[T] andThen Option.apply)(_)
  def set(name: String, value: Any) = request.setAttribute(name, value)
  def rem = request.removeAttribute _
  def has = (request.getAttribute _ andThen notNull)(_)
  def setOnce(name: String, value: Any) = if (!has(name)) set(name, value)

  // alias
  def +(kv: (String, Any)) = set(kv._1, kv._2)
  def +(kv: (String, Any)*) = kv.foreach(x => set(x._1, x._2))
  def +(name: String, value: Any) = set(name, value)
  def -(name: String) = rem(name)
  def -(name: String*) = name.foreach(rem)

  /** CTRL options */
  def addCtrlOption(option: String) = {
    val options = get[collection.mutable.ListBuffer[String]]("ctrl-options").get
    options -= option; options += option
  }
  def remCtrlOption(option: String) = {
    val options = get[collection.mutable.ListBuffer[String]]("ctrl-options").get
    options -= option
  }
  def ctrlOptions: List[String] = get[collection.mutable.ListBuffer[String]]("ctrl-options").get.toList

  /** Set a response code changed somewhere in request flow. */
  def pushResponseCode(code: Int) = set("response-code", code)
  def popResponseCode: Option[Int] = get[Int]("response-code")
}

class UserRequestExt(rx: RequestX) {
  def currentId: Long = state.currentUserId
  def superId: Long = state.superUserId

  def state: UserState = rx.xattr.perm[UserState](WebConstants.USER_STATE)
  def prefs: UserPreferences = rx.xattr.perm[UserPreferences](WebConstants.USER_PREFERENCES)

  private def load(id: Long): Option[User] = if (id > 0) rx.sc.get[UserService].getById(id) else None

  /** Return a current user. If a user is not in a request it will be loaded and stored first. */
  def current: Option[User] = state.currentUser orElse { val x = load(state.currentUserId); state.currentUser = x; x }

  /** Return a super user. If a user is not in a request it will be loaded and stored first. */
  def `super`: Option[User] = state.superUser orElse { val x = load(state.superUserId); state.superUser = x; x }

  /** Super user puts on a shell of another user. Back up her original beans and reset. */
  def shellOn(user: User) =
    state.enableSuperMode(user) {
      rx.xattr.perm + (WebConstants.EX_SEARCH_BEANS -> rx.xattr.perm[SearchBeansMap](WebConstants.SEARCH_BEANS)) }

  /** Super user takes off a shell of another user. Restore her original beans. */
  def shellOff {
    state.disableSuperMode
    rx.xattr.perm + (WebConstants.SEARCH_BEANS -> rx.xattr.perm[SearchBeansMap](WebConstants.EX_SEARCH_BEANS))
    rx.xattr.perm - WebConstants.EX_SEARCH_BEANS
  }

  def login(user: User) {
    rx.sc.get[SecurityService].pushPrincipal(user, Role.USER)
    rx.xuser.state.currentUserId = user.id
  }

  def logout: Option[User] = current.map { x =>
    rx.xinit.reset
    rx.sc.get[SecurityService].removePrincipal
    x
  }.orElse(None)

  /** Return 'true' if a user cannot login to current domain.
    * Method sets then clears user's principal.
    */
  def isLoginForbidden(user: User): Boolean =
    try {
      rx.sc.get[SecurityService].pushPrincipal(user, Role.USER)
      val domType = rx.xsite.state.currentDomainType
      !(rx.sc.get[SecurityService].isUserInRole(Role.ADMIN) ||
        domType == Domain.TYPE_CONTROL_PANEL && rx.sc.get[SecurityService].isUserInRole("ROLE_CPANEL") || //allowed to CPanel with role
        domType == Domain.TYPE_CONTROL_PANEL && user.domains.nonEmpty || //allowed to CPanel with domains
        domType != Domain.TYPE_CONTROL_PANEL) //allowed anywhere else
    } finally { rx.sc.get[SecurityService].removePrincipal }

  /** Return current domain agent or any user of current domain. */
  def domainOwner: Option[User] = {
    val domainId = rx.xsite.state.currentDomainId
    rx.sc.get[UserService].getAgentByDomain(domainId) orElse rx.sc.get[UserService].getByDomain(domainId)
  }

  /** Construct a user's full name. */
  def userFullName(user: User, site: SiteChain): String = {
    val s = rx.sc.get[LabelService].query("salut"+user.salutation, site)
    (if (user.salutation == User.SALUTATION_OTHER) "" else s+" ")+user.firstName+" "+user.lastName
  }

  /** Return a username. */
  def currentUsername: String = rx.xuser.current.map(_.username).getOrElse("?")
}

class SiteRequestExt(rx: RequestX) {
  def chainMap: SiteChainMap = rx.xattr.perm[SiteChainMap](WebConstants.SITE_CHAIN_MAP)
  def state: SiteState = rx.xattr.perm[SiteState](WebConstants.SITE_STATE)
  def current: SiteChain = state.currentSite
  def resolve(siteK: String): SiteChain = SiteChain(chainMap.chainByKey(siteK).value or current.value)

  /** Set a current site 'chain' and a root site to a session.
    *  Only if a site is valid to be stored as current.
    */
  def setCurrent(site: SiteChain) {
    val rootSite = rootSiteKey(site, rx.sc.get[SettingService].querySystem("Sites")) // xx1.mw1.xx2.mw2 -> xx2   xx1.ph1.zx1.mw1.xx2.ph2.zx2.mw2 -> xx2
    if (rootSite != "") { //avoid xx1b/xx1j as a current site
      state.currentSite = site
      state.rootSite = rootSite
    }
  }
}

class TuringRequestExt(rx: RequestX) {
  /** Inc turing attempt and return it or 0. */
  def incAttempt: Int = Option(get).map(_.attempted).getOrElse(0)

  /** Check if session contains valid turing optionally with its file present. */
  def isValid(file: Boolean = false): Boolean =
    Option(get).exists { turing =>
      val limit = rx.sc.get[SettingService].querySystem("SystemParams").parseCSVMap()("turRefresh").toInt
      val fpass = !file || rx.sc.get[FileService].getByPath(turing.path, SiteChain.pub).isDefined
      fpass && turing.attempt <= limit
    }

  /** Generate a turning and store it in a session. */
  def generate {
    val turing = rx.sc.get[TuringService].generate(rx.xsite.current)
    rx.xattr.perm + ("generated-turing" -> turing)
    rx.xattr + ("turingPath" -> turing.path)
  }

  def get: Turing = rx.xattr.perm.get[Turing]("generated-turing").orNull
}

class CookieRequestExt(rx: RequestX) {
  private val request = rx.request

  def read(name: String): String =
    (for (c <- request.getCookies.safe if name == c.getName) yield c.getValue).mkString("\n")

  def add(name: String, value: String) {
    val c = new Cookie(name, value)
    c.setMaxAge(WebConstants.COOKIE_EXPIRES)
    c.setPath("/")
    val cookies = rx.xattr[ListBuffer[Cookie]]("added-cookies")
    cookies.find(_.getName == name).map(cookies -= _)
    cookies += c
  }

  def added: List[Cookie] = rx.xattr[ListBuffer[Cookie]]("added-cookies").toList
}

class MiscRequestExt(rx: RequestX) {
  private val request = rx.request

  def resourceVersion: String = rx.sc.get[SettingService].getResourceVersion(rx.xsite.current.key)

  def searchBeansMap: SearchBeansMap = rx.xattr.perm[SearchBeansMap](WebConstants.SEARCH_BEANS)

  private def queryString = Option(request.getQueryString).map("?"+_).getOrElse("")
  def url: String = request.getRequestURL+queryString
  def uri: String = request.getRequestURI+queryString

  /** Each page has a unique index assigned, return page index. Helper method. */
  def pageIndex: String = rx.xattr[String]("pageIndex", "")

  /** Return siteable JSP path for current domain, properly prefixed and suffixed.
    *  Input: stripped 'path' to a resource, 'def-sfx' is specified then used as default suffix (eg. 'xx')
    *  my/account -> /WEB-INF/jsp/my/account-mc.jsp   /WEB-INF/jsp/my/account-xx.jsp   /WEB-INF/jsp/my/account.jsp
    */
  def siteableJspPath(path: String, defSfx: String = ""): String = {
    val loader = rx.resourceLoaderHolder.get
    val domainSite = rx.xsite.state.currentDomainSite
    val pfx = "/WEB-INF/jsp/"+path
    val sfx = domainSite.keys.filter(x => loader.getResource(pfx+"-"+x+".jsp").exists).lastOption.getOrElse(defSfx)
    pfx+(if (sfx != "") "-"+sfx else "")+".jsp"
  }

  /** Short form of querying for a label. */
  def label(key: String, site: SiteChain = rx.xsite.current): String = rx.sc.get[LabelService].query(key, site)

  /** Map with request parameters */
  def parameters: Map[String,String] = (for (name <- request.getParameterNames) yield name -> request(name)).toMap
}

class CmsRequestExt(rx: RequestX) {
  def state: CmsState = rx.xattr.perm[CmsState](WebConstants.CMS_STATE)

  /** Check if current user owns current domain. */
  private def isDomainOwner: Boolean =
    rx.sc.get[UserService].isDomainOwner(rx.xuser.currentId, rx.xsite.state.currentDomain)

  /** Enable CMS for current domain owner. Input: if 'pure-cms' is true then a user should be logged out
    *  after disabling it and optionally redirected back to CPanel domain.
    */
  def enable(pureCms: Boolean = false) =
    if (isDomainOwner && rx.sc.get[SecurityService].isUserInRole("ROLE_CMS") || rx.sc.get[SecurityService].isUserInRole(Role.ADMIN)) {
      state.enabled = true
      state.pureCms = pureCms
    }
  def disable = state.disable

  /** Enable resources edit for current domain owner. */
  def editOn = state.editing = state.enabled && (isDomainOwner || rx.sc.get[SecurityService].isUserInRole(Role.ADMIN))
  def editOff = state.editing = false
}

class SearchBeansMap extends collection.mutable.HashMap[String,Any] {
  def getAs[T](key: String): Option[T] = get(key).map(_.asInstanceOf[T])
}

class InitRequestExt(rx: RequestX) {
  /** Reset session and then populate it again. */
  def reset {
    (WebConstants.SEARCH_BEANS :: WebConstants.RECENT_LINKS :: WebConstants.LAST_SECURED_URL :: WebConstants.CMS_STATE ::
     WebConstants.USER_STATE :: WebConstants.USER_PREFERENCES :: Nil).foreach(rx.xattr.perm -)
    init
  }

  /** Populate request and session with required objects. */
  def init {
    val setPerm = rx.xattr.perm.setOnce _
    val set = rx.xattr.setOnce _

    setPerm(WebConstants.USER_STATE, new UserState)
    setPerm(WebConstants.USER_PREFERENCES, new UserPreferences)
    setPerm(WebConstants.SEARCH_BEANS, new SearchBeansMap)
    setPerm(WebConstants.CMS_STATE, new CmsState)
    set("added-cookies", new ListBuffer[Cookie])
    set("ctrl-options", new ListBuffer[String])

    rx.xuser.state.init
  }
}

class RequestX(val request: HttpServletRequest) {
  def resourceLoaderHolder = xattr[ResourceLoaderHolder]("resource-loader")
  def resourceLoaderHolder_= (x: ResourceLoaderHolder) = xattr + ("resource-loader" -> x)

  def serviceContainer = xattr[ServiceContainer]("service-container")
  def serviceContainer_= (x: ServiceContainer) = xattr + ("service-container" -> x)
  lazy val sc = serviceContainer

  def apply(name: String): String = request.getParameter(name).safe // request("foo") -> "bar" parameter

  def last: String = // .../page.htm?foo=bar -> page.htm   .../3I51B1R4   3I51B1R4?foo=bar -> 3I51B1R4
    request.getRequestURI.split("/").last.split("\\?").head

  val (xuser, xsite, xturing, xcookie, xmisc, xinit, xattr, xcms) = (
    new UserRequestExt(this), new SiteRequestExt(this), new TuringRequestExt(this), new CookieRequestExt(this),
    new MiscRequestExt(this), new InitRequestExt(this), new AttrRequestExt(this), new CmsRequestExt(this))
}

class CookieResponseExt(rx: ResponseX) {
  private val response = rx.response

  def add(name: String, value: String) {
    val c = new Cookie(name, value)
    c.setMaxAge(WebConstants.COOKIE_EXPIRES)
    c.setPath("/")
    response.addCookie(c)
  }
}

class ResponseX(val response: HttpServletResponse) {
  val xcookie = new CookieResponseExt(this)
}

class StaticBlock extends service.StaticBlock {
  def localeByIP(ip: String): Locale = InetAddressLocator.getLocale(ip)
}

/** Parameters saved in SearchBeansMap ("filter" is set separately). */
case class SearchCommand(from: Long, max: Int, sortBy: String, sortType: String, target: Int, params: List[String],
                         var filter: List[String] = Nil)

/** Container for parameters amd operations which could be useful in search methods */
case class SearchHelper(refresh: Boolean, saveKey: String, site: SiteChain, request: HttpServletRequest,
                        target: Int, cmd: SearchCommand) {
  /** Return a filter at given index to apply to search. */
  def filter(index: Int): String = if (index < cmd.filter.size) cmd.filter(index).safe.trim else ""

  /** Return a param at given index to apply to search. */
  def param(index: Int): String = if (index < cmd.params.size) cmd.params(index).safe.trim else ""

  /** Return ';' separated param as converted list. */
  def paramList[T](index: Int, f: String => T, default: T, sp: String = ";"): List[T] =
    param(index).split(sp).map { x => try { f(x) } catch { case _: Throwable => default }}.toList

  /** Return ';' separated param as converted tuple. */
  def paramTuple[T](index: Int, f: String => T, default: T, sp: String = ";"): (T,T) =
    paramList[T](index, f, default, sp).padTo(2, default) match { case Seq(a, b) => (a, b) }
}

case class CachedSearchResult(si: SearchInput, sh: SearchHelper, serializedResult: Map[Symbol,Any])

class CoreActions {
  /** Return uploaded files. After retrieval a session is cleared and can accumulate new files. */
  def uploadedFiles(request: HttpServletRequest): List[TemporaryFile] = {
    val tfs = request.xattr.perm.get[List[TemporaryFile]](WebConstants.UPLOADED_TMP_FILES).orNull
    request.xattr.perm - WebConstants.UPLOADED_TMP_FILES
    request.xattr.perm - WebConstants.UPLOAD_BEAN
    tfs.safe
  }

  /** Convenient method to return uploaded bodies. */
  def uploadedBodies(request: HttpServletRequest): List[Body] = uploadedFiles(request).map(_.body)

  /** Return result VO previously cached or none if not cached or refresh requested.
    * Full comparison also includes target and parameters.
    * Dev mode flag "Dev/NoCache" must not be applied for this method (controlled by business logic).
    */
  def popCachedResult(si: SearchInput, sh: SearchHelper, fullcomp: Boolean = false): Option[Map[Symbol,Any]] = {
    val o = sh.request.xmisc.searchBeansMap.getAs[CachedSearchResult](sh.saveKey+"-searchResult").orNull
    if (o == null || sh.refresh) None
    else {
      val xs = //do not compare 'target' as 'favs' do not rely on it
        (si.from, o.si.from) :: (si.max, o.si.max) :: (si.orderBy, o.si.orderBy) :: (sh.site, o.sh.site) :: Nil
      val fc = //compare 'target' and 'params' if full comparison set
        !fullcomp || sh.target == o.sh.target && sh.cmd.params == o.sh.cmd.params
      if (xs.forall { case (a, b) => a == b } && fc) Some(o.serializedResult) else None
    }
  }

  /** Save results into cache. */
  def pushCachedResult(si: SearchInput, sh: SearchHelper, result: Map[Symbol,Any]) =
    sh.request.xmisc.searchBeansMap += (sh.saveKey+"-searchResult" -> CachedSearchResult(si, sh, result))

  /** Use cached results or search and cache new results */
  def withCachedResult(si: SearchInput, sh: SearchHelper)(search: => Map[Symbol,Any]): Map[Symbol,Any] =
    popCachedResult(si, sh).getOrElse {
      val r = search
      pushCachedResult(si, sh, r)
      r
    }

  /** Check if current domain supports supplied object. */
  def isDomainSupports[A <: { def domains: JList[Domain] }](o: A, request: HttpServletRequest): Boolean =
    o.domains.contains(request.sc.get[DomainService].resolveDomain(request.xsite.state.currentDomain))

}

/** If URL matches several patterns, return the first one in order of appearance. */
class OrderedUrlHandlerMapping extends AbstractUrlHandlerMapping {
  @BeanProperty var mappings: Array[String] = _
  @BeanProperty var unordered: String = _
  private val pathMatcher: PathMatcher = new OrderedAntPathMatcher
  private var unpfx: List[String] = _ //unordered path prefix

  //match by position (unless has unordered prefix then fall back to default comparator)
  class OrderedAntPathMatcher extends AntPathMatcher {
    private val m = new Comparator[String] { override def compare(p1: String, p2: String): Int = -1 }
    override def getPatternComparator(path: String): Comparator[String] =
      if (unpfx.contains(path.split("/").headOption.getOrElse(""))) super.getPatternComparator(path) else m
  }

  override def initApplicationContext {
    super.initApplicationContext
    registerHandlers
    unpfx = unordered.safe.parseCSV
  }

  private def registerHandlers =
    for (m <- mappings) {
      val slash = (x: String) => x.startsWith("/") ? x | "/"+x // mypage.htm -> /mypage.htm
      m.split("\\|") match { case Array(url, handler) => registerHandler(slash(url.trim), handler.trim) }
    }

  override def getPathMatcher: PathMatcher = pathMatcher
}

/** Look up URL mapping in the database based on site. If URL matches several patterns, return the first
  * one in order of appearance. Declare as Spring bean with "urlMapping" ID.
  */
object CustomUrlHandlerMapping {
  /** Load URL mapping config (merge mappings using site chain plus SYS). */
  def loadUrlMapping(site: SiteChain, sc: ServiceContainer): UrlMapping = {
    val chain = site.keys.reverse :+ Constants.SITE_SYSTEM // xx1.mh1 -> mh1 xx1 sys
    val cx =
      for (siteK <- chain) yield
        Option(new Gson().fromJson(sc.get[SettingService].query("UrlMapping", SiteChain.key(siteK)).safe,
          classOf[UrlMapping])).getOrElse(new UrlMapping)

    val slash = (x: String) => x.startsWith("/") ? x | "/"+x // mypage.htm -> /mypage.htm

    val mmx = //list of merged pattern/handler
      (List.empty[UrlMapping#Mapping] /: cx){ (r, c) =>
        r ::: c.mapping.filter(a => !r.exists(x => slash(a.pattern) == slash(x.pattern))).toList }
    val mux = //list of merged unordered prefixes
      (List.empty[String] /: cx){ (r, c) => r ::: c.unorderPrefix.safe.parseCSV }
    new UrlMapping {
      mapping = mmx.map { x => x.pattern = slash(x.pattern); x }.toArray.asInstanceOf[Array[Mapping]]
      unorderPrefix = mux.mkString(",")
    }
  }
}

class CustomUrlHandlerMapping extends AbstractUrlHandlerMapping with Dependencies with Setup {
  import CustomUrlHandlerMapping._

  private lazy val LOG = new NamedLog("custom-url-handler-mapping", log)

  override def getHandlerInternal(request: HttpServletRequest): AnyRef = {
    try {
      request.serviceContainer = sc
      setupMods.foreach(_.setup(request)) //setup site now as initial aspect have not run yet
      val lookupPath = getUrlPathHelper.getLookupPathForRequest(request)
      lookupHandlerMapping(lookupPath, request) match {
        case Some(mapping) =>
          request.xattr + ("handler-mapping" -> mapping)
          val rawHandler = getApplicationContext.getBean(mapping.handler)
          validateHandler(rawHandler, request)
          //LOG.info(s"Mapping [$lookupPath] to "+mapping.handler)
          buildPathExposingHandler(rawHandler, lookupPath, lookupPath, null)
        case None =>
          throw new CoreException(s"No handler mapping found for [$lookupPath]")
      }
    } catch {
      case e: Throwable =>
        val (url, mth) = (request.xmisc.url, request.getMethod.toUpperCase)
        LOG.error("Error "+mth+" "+url, e)
        sc.get[MiscService].reportSystemError(s"[custom-url-handler-mapping] Error $mth $url", e)
        throw e
    }
  }

  def lookupHandlerMapping(urlPath: String, request: HttpServletRequest): Option[UrlMapping#Mapping] = {
    val cfg = loadUrlMapping(request.xsite.current, sc)
    val mappings = cfg.mapping.filter(!_.params.safe.parseCSV.contains("lock")) //ignore locked mappings
    mappings.find(_.pattern == urlPath) match {
      case x @ Some(directMatch) => x //direct match (as is)
      case None => //pattern match
        val antPathMatcher = new AntPathMatcher
        val matchedMappings = mappings.filter{x => antPathMatcher.`match`(x.pattern, urlPath)}
        //match in order of appearance (unless has unordered prefix then fall back to default comparator)
        matchedMappings.sortWith { (a, b) =>
          cfg.unorderPrefix.parseCSV.contains(urlPath.split("/").headOption.getOrElse("")) ?
            (antPathMatcher.getPatternComparator(urlPath).compare(a.pattern, b.pattern) > 0) | false
        }.headOption
    }
  }
}

case class RecentLink(@BeanProperty uri: String, @BeanProperty text: String, @BeanProperty url: String)

class CmsState {
  var (enabled, pureCms, editing) = (false, false, false)
  def disable = { enabled = false; pureCms = false; editing = false }
}

/** Parameters necessary for system operation */
class SystemCookie(val site: String, val currency: String)

object SystemCookie {
  def read(rx: RequestX): SystemCookie = {
    val x = rx.xcookie.read("system").decodeBase64.split(",")
    if (x(0) == "1") new SystemCookie(x(1), x(2)) else new SystemCookie("", "") // v1 - site/currency
  }
  def save(rx: RequestX) {
    val (site, currency) = (rx.xsite.chainMap.keyByChain(rx.xsite.state.currentSite), rx.xuser.prefs.currency)
    rx.xcookie.add("system", ("1,"+site+","+currency).encodeBase64) // v1 - site/currency
  }
}

object Aspect {

  trait Mod {
    def preHandle(request: HttpServletRequest)
  }

  def preHandle(request: HttpServletRequest, setup: List[SetupMod], mods: List[Mod]) {
    setup.foreach(_.setup(request))
    mods.foreach(_.preHandle(request)) // process mods
    SystemCookie.save(request)
  }

  private def pushAttributes(request: HttpServletRequest) {
    val (xattr, xsite, xuser, xmisc) = (request.xattr, request.xsite, request.xuser, request.xmisc)

    Map(
      "currentSite" -> xsite.current.value,
      "rootSite" -> xsite.state.rootSite,
      "mySite" -> xsite.chainMap.keyByChain(xsite.current),

      "currentUserId" -> xuser.currentId,
      "superUserId" -> xuser.superId,
      "shellUsername" -> xuser.state.shellUsername,
      "selectedCurrency" -> xuser.prefs.currency,

      "currentURI" -> request.getRequestURI,
      "currentURL" -> xmisc.url,
      "resourceVersion" -> xmisc.resourceVersion,

      "requestParams" -> xmisc.parameters
    ).foreach { xattr + }
  }

  def postHandle(request: HttpServletRequest, response: Option[HttpServletResponse] = None) {
    pushAttributes(request) //push vital attributes for JSP rendering

    response.foreach { r =>
      for (c <- request.xcookie.added) r.addCookie(c) //cookies scheduled to add

      //session ID cookie with every response for integration tests
      Option(request.getSession(false)).map(_.getId).foreach { sessionId =>
        if (request.sc.get[SettingService].querySystem("Dev/ApiFeatures").isTrue &&
          !r.getHeaders("Set-Cookie").exists(_.startsWith("JSESSIONID=")))
          r.addHeader("Set-Cookie", "JSESSIONID="+sessionId+"; Path=/; HttpOnly")
      }

      //set a response code if requested during flow
      r.setStatus(request.xattr.popResponseCode.getOrElse(HttpServletResponse.SC_OK))
      r.setIntHeader("Retry-After", 3600)
    }
  }

}
