package com.coldcore.haala
package web
package test

import java.util.Locale
import javax.servlet.http.HttpServletRequest

import com.coldcore.haala.core.SiteChain
import com.coldcore.haala.domain.Domain
import org.springframework.mock.web.{MockHttpServletRequest, MockHttpServletResponse, MockPageContext}
import core._
import org.mockito.Mockito.when
import scala.collection.JavaConverters._

class MyMock extends com.coldcore.haala.service.test.MyMock {
  override val m = this

  val pctx = new MockPageContext
  val (pageRequest, pageResponse) = (pctx.getRequest, pctx.getRequest)
  pageRequest.asInstanceOf[HttpServletRequest].serviceContainer = serviceContainer
  pageRequest.asInstanceOf[HttpServletRequest].resourceLoaderHolder = resourceLoaderHolder

  override val staticBlock = mock[StaticBlock]

  val (request, response) = (new MockHttpServletRequest, new MockHttpServletResponse)
  request.serviceContainer = serviceContainer
  request.resourceLoaderHolder = resourceLoaderHolder
  request.xinit.init
}

trait SetupMods {
  self: MyMock =>

  val mcDomain = new Domain {
    id = 1L; domain = "example.org"; domainSite = "mc"; `type` = "main"; meta = "style=html5,layout-modern; resolve=site,currency" }

  val (enLocale, ruLocale, frLocale) = (new Locale("en"), new Locale("ru"), new Locale("fr"))

  def runSetupMods(language: String = "en") {
    val (locale, chain, currency) =
      if (language == "ru") (ruLocale, SiteChain("xx1.xx2"), "RUB")
      else if (language == "fr") (frLocale, SiteChain("xx1"), "EUR")
      else (enLocale, SiteChain("xx1"), "GBP")

    request.setPreferredLocales(List(locale).asJava)
    when(settingService.querySystem("Sites")).thenReturn("xx1=xx1:en, xx2=xx1.xx2:ru")

    request.setServerName("www.example.org")
    when(domainService.resolveDomain("www.example.org")).thenReturn(mcDomain)
    when(domainService.resolveRootSite("example.org", locale)).thenReturn(chain)
    when(domainService.defaultSite("example.org")).thenReturn(SiteChain("xx1.mc1"))

    request.setRemoteAddr("0.0.0.0")
    when(staticBlock.localeByIP("0.0.0.0")).thenReturn(null)

    when(settingService.querySystem("Currencies")).thenReturn("GBP, EUR, USD")
    when(settingService.getDefaultCurrency).thenReturn("GBP")
    when(currencyService.resolveCurrency("example.org", locale)).thenReturn(currency)

    new RequestSetupMod().setup(request)

    request.getSession.removeAttribute(WebConstants.SITE_STATE) // execute condition
    new DomainSetupMod().setup(request)

    request.xsite.state.currentSite = SiteChain.empty // execute condition
    new SiteSetupMod { override val staticBlock = m.staticBlock }.setup(request)

    request.xuser.prefs.currency = "" // execute condition
    new CurrencySetupMod { override val staticBlock = m.staticBlock }.setup(request)

    new ExtraSetupMod().setup(request)
  }

}