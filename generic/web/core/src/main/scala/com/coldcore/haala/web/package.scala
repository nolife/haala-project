package com.coldcore.haala

import javax.servlet.http.{HttpServletResponse, HttpServletRequest}
import web.core._

package object web {
  implicit def toRequestX(request: HttpServletRequest) = new RequestX(request)
  implicit def toResponseX(response: HttpServletResponse) = new ResponseX(response)
}
