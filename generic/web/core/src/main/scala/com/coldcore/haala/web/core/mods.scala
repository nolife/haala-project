package com.coldcore.haala
package web
package core

import com.coldcore.haala.core.{Constants, Meta, SiteChain, SiteState}
import com.coldcore.haala.core.SiteUtil._
import com.coldcore.haala.core.Constants.UTF8
import javax.servlet.http.{HttpServletRequest, HttpServletResponse}
import service._

/** First mod to run to initialise request. */
class RequestSetupMod extends SetupMod {
  override def setup(request: HttpServletRequest) = {
    request.setCharacterEncoding(UTF8)
    request.xinit.init
  }
}

/** Populate a site-domain map with info about current domain.
 *  Application server is configured to create a separate session for each alias, each domain has its own session and
 *  does not share its session with other aliases on the same web host - no need to check if user switched domains.
 */
class DomainSetupMod extends SetupMod {

  private def setupDomain(rx: RequestX) =
    if (!rx.xattr.perm.has(WebConstants.SITE_STATE)) {
      val domainEntity = rx.sc.get[DomainService].resolveDomain(rx.request.getServerName) //resolve domain entity by name

      val state = SiteState(
        currentDomain = domainEntity.domain,
        currentDomainId = domainEntity.id,
        currentDomainSite = SiteChain(domainEntity.domainSite),
        currentDomainType = domainEntity.`type`,
        currentDomainStyle = Meta(domainEntity.meta)("style", "")
      )

      rx.xattr.perm + (WebConstants.SITE_STATE -> state)
    }

  override def setup(request: HttpServletRequest) =
    setupDomain(request)
}

/** Populate a site-domain map with info about current site. */
class SiteSetupMod extends SetupMod with Dependencies {

  private def setupSite(rx: RequestX) {
    val state = rx.xsite.state
    if (rx.xsite.current.isEmpty) {
      val domain = state.currentDomain
      val domainSite = state.currentDomainSite // ph.zx.mw
      val locales = List(rx.request.getLocale, sb.localeByIP(rx.request.getRemoteAddr)).filter(null!=)
      val curSite = mixRootPureSites(rx.sc.get[DomainService].resolveRootSite(domain, locales: _*), domainSite) // xx1.xx2+ph.zx.mw -> xx1.ph1.zx1.mw1.xx2.ph2.zx2.mw2
      val defSite = rx.sc.get[DomainService].defaultSite(domain) // xx1.ph1.zx1.mw1.xx2.ph2.zx2.mw2
      val rootSites = stripRootSites(rx.sc.get[SettingService].querySystem("Sites").parseCSVMap) // xx2=xx1.xx2
      val rootSite = rootSiteKey(curSite, rootSites) // xx1.ph1.zx1.mw1.xx2.ph2.zx2.mw2 -> xx2

      state.currentSite = curSite
      state.defaultSite = defSite
      state.rootSite = rootSite

      val cmap = createChainMap(domainSite, rootSites, Constants.SITE_EXTRAS) // root, mine, extra etc
      rx.xattr.perm + (WebConstants.SITE_CHAIN_MAP -> cmap)

      // restore a site if stored in a cookie
      val scookie = SystemCookie.read(rx)
      if (scookie.site != "") rx.xsite.setCurrent(rx.xsite.chainMap.chainByKey(scookie.site))
    }

    // if site overwrite passed as a request parameter - try to change it
    val mySite = rx.request("osite")
    mySite on { rx.xsite.setCurrent(rx.xsite.chainMap.chainByKey(mySite)); "" }

    rx.xattr + ("currentSite" -> state.currentSite.value, "rootSite" -> state.rootSite)
  }

  override def setup(request: HttpServletRequest) =
    setupSite(request)
}

class CurrencySetupMod extends SetupMod with Dependencies {

  private def setupCurrency(rx: RequestX) =
    if (rx.xuser.prefs.currency == "") {
      val scookie = SystemCookie.read(rx)
      val locales = List(rx.request.getLocale, sb.localeByIP(rx.request.getRemoteAddr)).filter(null!=)
      val resolvedCur = rx.sc.get[CurrencyService].resolveCurrency(rx.xsite.state.currentDomain, locales: _*)
      val allCur = rx.sc.get[SettingService].querySystem("Currencies").parseCSV

      // try to restore a currency from a cookie then resolve by domain then use default
      rx.xuser.prefs.currency =
        if (allCur.contains(scookie.currency)) scookie.currency
        else if (allCur.contains(resolvedCur)) resolvedCur
        else rx.sc.get[SettingService].getDefaultCurrency
    }

  override def setup(request: HttpServletRequest) =
    setupCurrency(request)
}

/** Last mod to run to setup extra info. */
class ExtraSetupMod extends SetupMod {
  override def setup(request: HttpServletRequest) = {
    // add domain style to controller options to use in pages
    request.xsite.state.currentDomainStyle.parseCSV.foreach(request.xattr.addCtrlOption)
  }
}
