package com.coldcore.haala
package web
package controller

import logic.LabelsController
import org.scalatest.mockito.MockitoSugar
import org.mockito.Mockito._
import org.mockito.ArgumentMatchers._
import org.mockito.ArgumentCaptor
import org.scalatest._
import org.junit.runner.RunWith
import org.scalatest.junit.JUnitRunner
import com.coldcore.haala.core.SiteChain.Implicits._
import com.coldcore.haala.core.VirtualPath.Implicits._
import test.SetupMods
import service.{Cache, MiscServiceImpl}
import core.{BinaryFile, WebConstants}

@RunWith(classOf[JUnitRunner])
class LabelsControllerSpec extends WordSpec with MockitoSugar with BeforeAndAfter {
  var m: MyMock = _

  before {
    m = new MyMock
  }

  class MyMock extends com.coldcore.haala.web.test.MyMock with SetupMods {
    serviceContainer.services = //use real misc service
      (new MiscServiceImpl |< { x =>
        x.serviceContainer = serviceContainer
        x.velocityEngine = vmEngine
      }) :: serviceContainer.services

    val templatesCache = mock[Cache[String]]
    val controller = new LabelsController |< { x => x.serviceContainer = serviceContainer; x.templatesCache = templatesCache }

    runSetupMods()

    when(settingService.getResourceVersion(anyString)).thenReturn("1.2")
    when(templatesCache.get(anyString)).thenReturn(Some("1234567890"))
  }

  implicit class Inliner(x: String) { def inline = x.replaceAll("\\r?\\n|\\r", " ").replaceAll("\\s+", " ") }

  val labels_js_vm =
    """
      |Haala.getLabel().model.box = {
      |#foreach ($e in $map.entrySet())
      |  "$e.key": "$e.value",
      |#end
      |  "null": ""
      |}
    """.stripMargin

  "handleRequest" should {
    "output empty content if no site key set" in {
      m.controller.handleRequest(m.request, m.response)
      assertResult(None) { m.request.xattr.get[BinaryFile](WebConstants.BINARY_FILE) }
    }

    "output empty content if not xtra site key set" in {
      m.request.setParameter("site", "xx1")
      m.controller.handleRequest(m.request, m.response)
      assertResult(None) { m.request.xattr.get[BinaryFile](WebConstants.BINARY_FILE) }
    }

    "output content if xtra site key set" in {
      m.request.setParameter("site", "mc1j")
      m.controller.handleRequest(m.request, m.response)
      assertResult("1234567890") { m.request.xattr.get[BinaryFile](WebConstants.BINARY_FILE).get.asString }
    }

    "output content if xtra site key set 2" in {
      m.request.setParameter("site", "mc1j")
      m.controller.handleRequest(m.request, m.response)
      assertResult("1234567890") { m.request.xattr.get[BinaryFile](WebConstants.BINARY_FILE).get.asString }
    }

    "construct labels JS file" in {
      when(m.labelService.getForSite("xx1j.mc1j")).thenReturn(Map("key A" -> "val A", "key B" -> "val B"))
      when(m.settingService.getForSiteLike("RegEx/%", ".sys")).thenReturn(Map("key C" -> "val C"))
      when(m.templatesCache.get(anyString)).thenReturn(None)
      m.request.setParameter("site", "mc1j")
      m.registerFile(1, "web/labels.js.vm", "xx1j.mc1j", labels_js_vm)
      m.controller.handleRequest(m.request, m.response)
      val expected =
        s"""
            Haala.getLabel().model.box = {
              "key A": "val A",
              "key B": "val B",
              "key C": "val C",
              "null": ""
            }
         """
      assertResult(expected.inline) { m.request.xattr.get[BinaryFile](WebConstants.BINARY_FILE).get.asString.inline }
    }

  }

}
