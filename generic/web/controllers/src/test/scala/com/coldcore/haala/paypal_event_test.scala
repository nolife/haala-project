package com.coldcore.haala
package web
package controller.logic
package payment

import core.BinaryFile
import com.coldcore.haala.core.CoreException
import com.coldcore.haala.core.GeneralUtil.digest
import com.coldcore.haala.domain.AbyssEntry
import service.{AbyssService, PaypalHandler}
import javax.servlet.http.HttpServletResponse
import org.scalatest.mockito.MockitoSugar
import org.mockito.Mockito._
import org.scalatest._
import org.junit.runner.RunWith
import org.mockito.ArgumentMatchers._
import org.scalatest.junit.JUnitRunner

@RunWith(classOf[JUnitRunner])
class PaypalEventControllerSpec extends FlatSpec with MockitoSugar with BeforeAndAfter with Matchers {
  var m: MyMock = _

  before {
    m = new MyMock
  }

  class MyMock extends com.coldcore.haala.web.test.MyMock {
    val controller = new Base3rdController {
      serviceContainer = m.serviceContainer
      mod = mock[PaypalEventMod]
    }
  }

  def setResponseCode = m.response.setStatus(m.request.xattr.popResponseCode.getOrElse(HttpServletResponse.SC_OK))

  it should "respond with code 200 on success" in {
    when(m.controller.mod.process(m.controller, m.request, m.response)).thenReturn(BinaryFile("", "0"))

    m.controller.handleRequest(m.request, m.response)
    setResponseCode

    m.response.getStatus shouldBe 200
    verify(m.controller.mod).process(m.controller, m.request, m.response)
  }

  it should "respond with code 503 on error" in {
    when(m.controller.mod.process(m.controller, m.request, m.response)).thenThrow(new CoreException)

    m.controller.handleRequest(m.request, m.response)
    setResponseCode

    m.response.getStatus shouldBe 503
    verify(m.controller.mod).process(m.controller, m.request, m.response)
  }

}

@RunWith(classOf[JUnitRunner])
class PaypalEventModSpec extends FlatSpec with MockitoSugar with BeforeAndAfter with Matchers {
  var m: MyMock = _

  before {
    m = new MyMock
  }

  class MyMock extends com.coldcore.haala.web.test.MyMock {
    val mod = new PaypalEventMod { serviceContainer = m.serviceContainer }
    val controller = mock[Base3rdController]
  }

  it should "ignore request with no content" in {
    m.mod.process(m.controller, m.request, m.response)

    verify(m.abyssService, times(0)).createNow(any[AbyssService.CreateIN])
    verify(m.queueService, times(0)).pushUnique(anyString, anyString, anyString)
  }

  it should "save content and put it into queue" in {
    m.request.setContent("FOO".bytesUTF8)
    doAnswer(invocation => {
      new AbyssEntry { id = 3L }
    }).when(m.abyssService).createNow(any[AbyssService.CreateIN])

    m.mod.process(m.controller, m.request, m.response)

    verify(m.abyssService).createNow(any[AbyssService.CreateIN])
    verify(m.queueService).pushUnique("PaymentEvent", "handler=paypal;abyss=3", digest("FOO", "MD5"))
  }

  it should "save content and throw exception if queue fails" in {
    m.request.setContent("FOO".bytesUTF8)
    when(m.queueService.pushUnique(anyString, anyString, anyString)).thenThrow(new CoreException)
    doAnswer(invocation => {
      new AbyssEntry { id = 3L }
    }).when(m.abyssService).createNow(any[AbyssService.CreateIN])

    a [CoreException] should be thrownBy m.mod.process(m.controller, m.request, m.response)
  }

}
