package com.coldcore.haala
package web
package controller

import logic.JsController
import org.scalatest.mockito.MockitoSugar
import org.mockito.Mockito._
import org.mockito.ArgumentMatchers._
import org.mockito.ArgumentCaptor
import org.scalatest._
import org.junit.runner.RunWith
import org.scalatest.junit.JUnitRunner
import com.coldcore.haala.core.SiteChain.Implicits._
import com.coldcore.haala.core.VirtualPath.Implicits._
import test.SetupMods
import service.Cache
import core.{BinaryFile, WebConstants}

@RunWith(classOf[JUnitRunner])
class JsControllerSpec extends WordSpec with MockitoSugar with BeforeAndAfter {
  var m: MyMock = _

  before {
    m = new MyMock
  }

  class MyMock extends com.coldcore.haala.web.test.MyMock with SetupMods {
    val templatesCache = mock[Cache[String]]
    val controller = new JsController |< { x => x.serviceContainer = serviceContainer; x.templatesCache = templatesCache }
    val controllerSpy = spy(controller)

    runSetupMods()
    request.setParameter("site", "mc1")

    when(settingService.getResourceVersion(anyString)).thenReturn("1.2")
    when(settingService.query("MainURL", "xx1.mc1")).thenReturn("http://example.org")
    when(templatesCache.get(anyString)).thenReturn(None)
  }

  implicit class Inliner(x: String) { def inline = x.replaceAll("\\r?\\n|\\r", " ").replaceAll("\\s+", " ") }

  val simple_js =
    """
      |var Haala = Haala || {};
      |Haala.Code = {
      |  init: function() { },
      |  ready: function() { }
      |};
    """.stripMargin

  val commented_js =
    """
      |/* strip::comments */
      |var Haala = Haala || {};
      |/* initialisation */
      |Haala.Code = {
      |  init: function() { }, /* called first */
      |  ready: function() { } /* called last */
      |};
    """.stripMargin

  val inject_js =
    """
      |var Haala = Haala || {};
      |Haala.Code = {
      |  init: function() {
      |    var root = "${MainURL}";
      |    var domain = "${DomainAPI}";
      |  },
      |  ready: function() { }
      |};
    """.stripMargin

  "handleRequest" should {
    "construct simple JS file" in {
      m.request.setParameter("path", "simple.js")
      doReturn(simple_js, Nil: _*).when(m.controllerSpy).readWebResource("simple.js", "xx1.mc1")

      m.controllerSpy.handleRequest(m.request, m.response)
      assertResult(simple_js) { m.request.xattr.get[BinaryFile](WebConstants.BINARY_FILE).get.asString }
    }

    "strip comments in JS file" in {
      m.request.setParameter("path", "commented.js")
      doReturn(commented_js, Nil: _*).when(m.controllerSpy).readWebResource("commented.js", "xx1.mc1")

      m.controllerSpy.handleRequest(m.request, m.response)
      val expected =
        """
          var Haala = Haala || {};
          Haala.Code = {
            init: function() { },
            ready: function() { }
          };
        """
      assertResult(expected.inline) { m.request.xattr.get[BinaryFile](WebConstants.BINARY_FILE).get.asString.inline }
    }

    "inject variables into JS file" in {
      m.request.setParameter("path", "inject.js")
      doReturn(inject_js, Nil: _*).when(m.controllerSpy).readWebResource("inject.js", "xx1.mc1")

      m.controllerSpy.handleRequest(m.request, m.response)
      val expected =
        """
          var Haala = Haala || {};
          Haala.Code = {
            init: function() {
              var root = "http://example.org";
              var domain = "example.org";
            },
            ready: function() { }
          };
        """
      assertResult(expected.inline) { m.request.xattr.get[BinaryFile](WebConstants.BINARY_FILE).get.asString.inline }
    }

  }

}
