package com.coldcore.haala
package web
package controller

import logic.WebResourceReader
import org.scalatest.mockito.MockitoSugar
import org.mockito.Mockito._
import org.mockito.ArgumentMatchers._
import org.scalatest._
import org.junit.runner.RunWith
import org.scalatest.junit.JUnitRunner
import org.springframework.core.io.Resource
import java.io.ByteArrayInputStream

import com.coldcore.haala.core.{SiteChain, VirtualPath}
import com.coldcore.haala.core.SiteChain.Implicits._
import com.coldcore.haala.core.VirtualPath.Implicits._

@RunWith(classOf[JUnitRunner])
class WebResourceReaderSpec extends WordSpec with MockitoSugar with BeforeAndAfter {
  var m: MyMock = _

  before {
    m = new MyMock
  }

  class MyMock extends com.coldcore.haala.web.test.MyMock {
    class CssReader extends WebResourceReader {
      override val webResourceType = "css"
      val sc = m.serviceContainer
      val resourceLoaderHolder = m.resourceLoaderHolder
    }
    val reader = new CssReader
  }

  implicit class Inliner(x: String) { def inline = x.replaceAll("\\r?\\n|\\r", " ").replaceAll("\\s+", " ") }

  val simple_css =
    """
      |body { width:920px; }
      |#left-column, #right-column { display:none; }
      |#content { margin-left:0; }
    """.stripMargin

  val mainA_css = // one import
    """
      |/* css::import
      |   { path = foo; css = partE, partF }
      |*/
      |#content { margin-left:0; }
    """.stripMargin

  val mainB_css = // double import
    """
      |/* css::import
      |   { path = foo; css = partE, partF }
      |*/
      |#content { margin-left:0; }
      |/* css::import
      |   { path = foo; css = partE }
      |*/
    """.stripMargin

  val mainC_css = // import and nested import (from part)
    """
      |/* css::import
      |   { path = foo; css = partE, partG }
      |*/
      |#content { margin-left:0; }
    """.stripMargin

  val partE_css = // no import
    """
      |.part-e a { color:green; }
    """.stripMargin

  val partF_css = // no import
    """
      |.part-f a { color:red; }
    """.stripMargin

  val partG_css = //import
    """
      |.part-g a { color:blue; }
      |/* css::import
      |   { path = foo; css = partH }
      |*/
    """.stripMargin

  val partH_css = //no import
    """
      |.part-h a { color:black; }
    """.stripMargin

  "readWebResource" should {
    "read file from DB" in {
      m.registerFile(1, "foo/simple.css", "xx1", simple_css)
      val content = m.reader.readWebResource("foo/simple.css", "xx1")
      assertResult(simple_css) { content }
      verify(m.fileService).getByPath("foo/simple.css", "xx1")
    }

    "read file from DB other folder" in {
      when(m.fileService.getByPath("foo/simple.css", "xx1")).thenReturn(None)
      m.registerFile(1, "web/foo/simple.css", "xx1", simple_css)
      val content = m.reader.readWebResource("foo/simple.css", "xx1")
      assertResult(simple_css) { content }
      verify(m.fileService).getByPath("foo/simple.css", "xx1")
      verify(m.fileService).getByPath("web/foo/simple.css", "xx1")
    }

    "read file from WAR" in {
      val resource = mock[Resource]
      when(m.fileService.getByPath(any[VirtualPath], any[SiteChain])).thenReturn(None)
      when(m.resourceLoader.getResource("foo/simple.css")).thenReturn(resource)
      when(resource.exists()).thenReturn(true)
      when(resource.getInputStream).thenReturn(new ByteArrayInputStream(simple_css.getBytes))

      val content = m.reader.readWebResource("foo/simple.css", "xx1")
      assertResult(simple_css) { content }
      verify(m.fileService).getByPath("foo/simple.css", "xx1")
      verify(m.resourceLoader).getResource("foo/simple.css")
    }
  }

  "glueWebResource" should {
    "glue several files together" in {
      m.registerFile(1, "foo/mainA.css", "xx1", mainA_css)
      m.registerFile(2, "foo/mainB.css", "xx1", mainB_css)
      m.registerFile(3, "foo/mainC.css", "xx1", mainC_css)
      m.registerFile(4, "foo/partE.css", "xx1", partE_css)
      m.registerFile(5, "foo/partF.css", "xx1", partF_css)
      m.registerFile(6, "foo/partG.css", "xx1", partG_css)
      m.registerFile(7, "foo/partH.css", "xx1", partH_css)

      var content = m.reader.readWebResource("foo/mainA.css", "xx1")
      var glued = m.reader.glueWebResource("xx1")(content, m.reader.noopOnEach)
      var expected =
        """
          .part-e a { color:green; }
          .part-f a { color:red; }
          #content { margin-left:0; }
        """
      assertResult(expected.inline) { glued.inline }

      content = m.reader.readWebResource("foo/mainB.css", "xx1")
      glued = m.reader.glueWebResource("xx1")(content, m.reader.noopOnEach)
      expected =
        """
          .part-e a { color:green; }
          .part-f a { color:red; }
          #content { margin-left:0; }
          .part-e a { color:green; }
        """
      assertResult(expected.inline) { glued.inline }

      content = m.reader.readWebResource("foo/mainC.css", "xx1")
      glued = m.reader.glueWebResource("xx1")(content, m.reader.noopOnEach)
      expected =
        """
          .part-e a { color:green; }
          .part-g a { color:blue; }
          .part-h a { color:black; }
          #content { margin-left:0; }
        """
      assertResult(expected.inline) { glued.inline }
    }

  }

}
