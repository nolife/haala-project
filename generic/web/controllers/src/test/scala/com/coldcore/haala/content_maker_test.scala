package com.coldcore.haala
package web
package controller

import logic.{ContentMaker, WebResourceReader}
import org.scalatest.mockito.MockitoSugar
import org.mockito.Mockito._
import org.mockito.ArgumentMatchers._
import org.scalatest._
import org.junit.runner.RunWith
import org.scalatest.junit.JUnitRunner
import org.springframework.core.io.Resource
import java.io.ByteArrayInputStream

import com.coldcore.haala.core.SiteChain
import com.coldcore.haala.core.SiteChain.Implicits._
import com.coldcore.haala.service.Cache

@RunWith(classOf[JUnitRunner])
class ContentMakerSpec extends FlatSpec with MockitoSugar with BeforeAndAfter with Matchers {
  var m: MyMock = _

  before {
    m = new MyMock
  }

  class MyMock extends com.coldcore.haala.web.test.MyMock {
    val cache = mock[Cache[String]]
    class Maker extends ContentMaker { def templatesCache: Cache[String] = cache }

    val maker = new Maker

    when(cache.get(anyString)).thenReturn(None)
    when(cache.get("my-key")).thenReturn(Some("my-content"))
  }

  it should "load content from cache if found" in {
    var c = ""
    m.maker.makeContent("my-key", "foo") { content =>
      c = content
    }
    c shouldBe "my-content"
  }

  it should "make content if not found in cache" in {
    var c = ""
    m.maker.makeContent("bar", "foo") { content =>
      c = content
    }
    c shouldBe "foo"
  }

  it should "make content only once and use cache after" in {
    m.maker.makeContent("bar", "foo") { content => }
    verify(m.cache).set("bar", "foo")
    when(m.cache.get("bar")).thenReturn(Some("foo"))

    var c = ""
    m.maker.makeContent("bar", throw new Exception("evaluated")) { content =>
      c = content
    }
    c shouldBe "foo"
  }

}
