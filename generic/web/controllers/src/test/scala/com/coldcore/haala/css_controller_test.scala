package com.coldcore.haala
package web
package controller

import logic.CssController
import org.scalatest.mockito.MockitoSugar
import org.mockito.Mockito._
import org.mockito.ArgumentMatchers._
import org.mockito.ArgumentCaptor
import org.scalatest._
import org.junit.runner.RunWith
import org.scalatest.junit.JUnitRunner
import com.coldcore.haala.core.SiteChain.Implicits._
import com.coldcore.haala.core.VirtualPath.Implicits._
import test.SetupMods
import service.Cache
import core.{BinaryFile, WebConstants}

@RunWith(classOf[JUnitRunner])
class CssControllerSpec extends WordSpec with MockitoSugar with BeforeAndAfter {
  var m: MyMock = _

  before {
    m = new MyMock
  }

  class MyMock extends com.coldcore.haala.web.test.MyMock with SetupMods {
    val templatesCache = mock[Cache[String]]
    val controller = new CssController |< { x => x.serviceContainer = serviceContainer; x.templatesCache = templatesCache }
    val controllerSpy = spy(controller)

    runSetupMods()
    request.setParameter("site", "mc1")

    when(settingService.querySystem("CssCtrlExtensions")).thenReturn("css, jpg, jpeg, gif, png")
    when(settingService.getResourceVersion(anyString)).thenReturn("1.2")
    when(templatesCache.get(anyString)).thenReturn(None)
  }

  implicit class Inliner(x: String) { def inline = x.replaceAll("\\r?\\n|\\r", " ").replaceAll("\\s+", " ") }

  val simple_css =
    """
      |body { width:920px; }
      |#content { margin-left:0; color:#AA0000; background-color:#3333CC; }
    """.stripMargin

  val import_class_css =
    """
      |.page-spread * { display:inline-block; }
      |.page-spread a .next { /* css::class { css = theme; class = .ui-icon, .ui-icon-circle-arrow-e } */ }
      |.page-spread a .prev { /* css::class { css = theme; class = .ui-icon, .unknown, .ui-icon-circle-arrow-w } */ }
    """.stripMargin

  val theme_css =
    """
      |.ui-icon {
      |	  display: block;
      |	  text-indent: -99999px;
      |	  overflow: hidden;
      |	  background-repeat: no-repeat;
      |}
      |.ui-icon-circle-arrow-e { background-position: -112px -192px; }
      |.ui-icon-circle-arrow-s { background-position: -128px -192px; }
      |.ui-icon-circle-arrow-w { background-position: -144px -192px; }
      |.ui-icon-circle-arrow-n { background-position: -160px -192px; }
    """.stripMargin

  val version_css =
    """
      |@import "/theme.css";
      |#change-site .en { background:url(icon-flag-en.gif) no-repeat; }
      |#change-site .ru { background:url( 'bar/icon-flag-ru.gif' ) no-repeat; }
      |#change-site .es { background:url(/flag-es.GIF); }
      |#change-site .au { background:url(/web/flag-au.JPG); }
    """.stripMargin

  val glue_css =
    """
      |/* css::import
      |   { path = my/foo; css = version }
      |*/
      |.ui-icon { background-image: url("ui-icons_0078ae_256x240.png"); }
    """.stripMargin

  val commented_css =
    """
      |/* strip::comments */
      |/* main layout */
      |#mainmenu { background:#ddddaa; margin:10px 0 10px 0; height:50px; }
      |/* object not found DIV */
      |#no-object { padding-top:50px; text-align:center; }
      |#no-object button { margin-top:40px; }
    """.stripMargin

  val noPalette = () => m.registerFile(1, "web/palette.prop", "xx1.mc1", "")

  "handleRequest" should {
    "construct simple CSS file" in {
      m.request.setParameter("path", "simple.css")
      doReturn(simple_css, Nil: _*).when(m.controllerSpy).readWebResource("simple.css", "xx1.mc1")
      noPalette()

      m.controllerSpy.handleRequest(m.request, m.response)
      assertResult(simple_css) { m.request.xattr.get[BinaryFile](WebConstants.BINARY_FILE).get.asString }
    }

    "apply palette to CSS file" in {
      m.request.setParameter("path", "simple.css")
      doReturn(simple_css, Nil: _*).when(m.controllerSpy).readWebResource("simple.css", "xx1.mc1")

      val palette_props =
        """
          |AA0000=3333CC
          |3333CC=AA0000
          |FF0000=CC0000
        """.stripMargin
      m.registerFile(1, "web/palette.prop", "xx1.mc1", palette_props)

      m.controllerSpy.handleRequest(m.request, m.response)
      val expected =
        """
          body { width:920px; }
          #content { margin-left:0; color:#3333CC; background-color:#AA0000; }
        """
      assertResult(expected.inline) { m.request.xattr.get[BinaryFile](WebConstants.BINARY_FILE).get.asString.inline }
    }

    "import classes from other CSS files" in {
      m.request.setParameter("path", "import_class.css")
      doReturn(import_class_css, Nil: _*).when(m.controllerSpy).readWebResource("import_class.css", "xx1.mc1")
      doReturn(theme_css, Nil: _*).when(m.controllerSpy).readWebResource("theme.css", "xx1.mc1")
      noPalette()

      m.controllerSpy.handleRequest(m.request, m.response)
      val expected =
        """
          .page-spread * { display:inline-block; }
          .page-spread a .next {
            display: block;
            text-indent: -99999px;
            overflow: hidden;
            background-repeat: no-repeat;
            ;
            background-position: -112px -192px;
          }
          .page-spread a .prev {
            display: block;
            text-indent: -99999px;
            overflow: hidden;
            background-repeat: no-repeat;
            ; ;
            background-position: -144px -192px;
          }
        """
      assertResult(expected.inline) { m.request.xattr.get[BinaryFile](WebConstants.BINARY_FILE).get.asString.inline }
    }

    "prepend non-absolute and append site with version to external links" in {
      m.request.setParameter("path", "abc/foo/version.css")
      doReturn(version_css, Nil: _*).when(m.controllerSpy).readWebResource("abc/foo/version.css", "xx1.mc1")
      noPalette()

      m.controllerSpy.handleRequest(m.request, m.response)
      val expected =
        """
          @import "/theme.css?site=mc1&rv=1.2";
          #change-site .en { background:url(/abc/foo/icon-flag-en.gif?site=mc1&rv=1.2) no-repeat; }
          #change-site .ru { background:url( '/abc/foo/bar/icon-flag-ru.gif?site=mc1&rv=1.2' ) no-repeat; }
          #change-site .es { background:url(/flag-es.GIF?site=mc1&rv=1.2); }
          #change-site .au { background:url(/web/flag-au.JPG?site=mc1&rv=1.2); }
        """
      assertResult(expected.inline) { m.request.xattr.get[BinaryFile](WebConstants.BINARY_FILE).get.asString.inline }
    }

    "prepend non-absolute and append site with version to external links (glued css as root)" in {
      m.request.setParameter("path", "glue.css")
      doReturn(glue_css, Nil: _*).when(m.controllerSpy).readWebResource("glue.css", "xx1.mc1")
      doReturn(version_css, Nil: _*).when(m.controllerSpy).readWebResource("my/foo/version.css", "xx1.mc1")
      noPalette()

      m.controllerSpy.handleRequest(m.request, m.response)
      val expected =
        """
          @import "/theme.css?site=mc1&rv=1.2";
          #change-site .en { background:url(/my/foo/icon-flag-en.gif?site=mc1&rv=1.2) no-repeat; }
          #change-site .ru { background:url( '/my/foo/bar/icon-flag-ru.gif?site=mc1&rv=1.2' ) no-repeat; }
          #change-site .es { background:url(/flag-es.GIF?site=mc1&rv=1.2); }
          #change-site .au { background:url(/web/flag-au.JPG?site=mc1&rv=1.2); }
          .ui-icon { background-image: url("/ui-icons_0078ae_256x240.png?site=mc1&rv=1.2"); }
        """
      assertResult(expected.inline) { m.request.xattr.get[BinaryFile](WebConstants.BINARY_FILE).get.asString.inline }
    }

    "prepend non-absolute and append site with version to external links (glued css as not root)" in {
      m.request.setParameter("path", "deepblue/glue.css")
      doReturn(glue_css, Nil: _*).when(m.controllerSpy).readWebResource("deepblue/glue.css", "xx1.mc1")
      doReturn(version_css, Nil: _*).when(m.controllerSpy).readWebResource("my/foo/version.css", "xx1.mc1")
      noPalette()

      m.controllerSpy.handleRequest(m.request, m.response)
      val expected =
        """
          @import "/theme.css?site=mc1&rv=1.2";
          #change-site .en { background:url(/my/foo/icon-flag-en.gif?site=mc1&rv=1.2) no-repeat; }
          #change-site .ru { background:url( '/my/foo/bar/icon-flag-ru.gif?site=mc1&rv=1.2' ) no-repeat; }
          #change-site .es { background:url(/flag-es.GIF?site=mc1&rv=1.2); }
          #change-site .au { background:url(/web/flag-au.JPG?site=mc1&rv=1.2); }
          .ui-icon { background-image: url("/deepblue/ui-icons_0078ae_256x240.png?site=mc1&rv=1.2"); }
        """
      assertResult(expected.inline) { m.request.xattr.get[BinaryFile](WebConstants.BINARY_FILE).get.asString.inline }
    }

    "strip comments in CSS file" in {
      m.request.setParameter("path", "commented.css")
      doReturn(commented_css, Nil: _*).when(m.controllerSpy).readWebResource("commented.css", "xx1.mc1")
      noPalette()

      m.controllerSpy.handleRequest(m.request, m.response)
      val expected =
        """
          #mainmenu { background:#ddddaa; margin:10px 0 10px 0; height:50px; }
          #no-object { padding-top:50px; text-align:center; }
          #no-object button { margin-top:40px; }
        """
      assertResult(expected.inline) { m.request.xattr.get[BinaryFile](WebConstants.BINARY_FILE).get.asString.inline }
    }

  }

}
