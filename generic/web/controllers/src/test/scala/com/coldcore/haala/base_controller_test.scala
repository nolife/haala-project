package com.coldcore.haala
package web
package controller

import org.scalatest.{BeforeAndAfter, WordSpec}
import logic.{BaseController, DynamicController}
import org.scalatest.mockito.MockitoSugar
import org.mockito.Mockito._
import org.mockito.ArgumentMatchers._
import org.mockito.ArgumentCaptor
import core.SearchCommand
import com.coldcore.haala.core.{SiteChain, UrlMapping}
import com.coldcore.haala.core.SiteChain.Implicits._
import test.SetupMods
import org.junit.runner.RunWith
import org.scalatest.junit.JUnitRunner

@RunWith(classOf[JUnitRunner])
class BaseControllerSpec extends WordSpec with MockitoSugar with BeforeAndAfter {
  var m: MyMock = _

  before {
    m = new MyMock
  }

  class MyMock extends com.coldcore.haala.web.test.MyMock with SetupMods {
    runSetupMods()
  }

  "config" should {
    "return combined configuration based on page index" in {
      val ctrl = new BaseController { index = "00055"; mod = "myMod"; facade = "script1, script2"; pageName = "my/account"
        serviceContainer = m.serviceContainer }
      val urlMapping = new UrlMapping
      m.request.xinit.init
      m.request.xattr + ("handler-mapping" -> new urlMapping.Mapping)

      var conf = ctrl.config(m.request)
      assertResult("00055") { conf.index }
      assertResult("script1, script2") { conf.facade }
      assertResult("myMod") { conf.mod }

      ctrl.index = "00056"
      m.request.xattr + ("handler-mapping" -> (new urlMapping.Mapping |< { x => x.handlerConf =
        new urlMapping.HandlerConf { index = "00056"; mod = "turingMod"; facade = "my/Script, esatte/MessageF"; pageName = "my/acc2" }}))
      conf = ctrl.config(m.request)
      assertResult("00056") { conf.index }
      assertResult("my/Script, esatte/MessageF") { conf.facade }
      assertResult("my/acc2") { conf.pageName }
      assertResult("turingMod") { conf.mod }

      ctrl.index = "00057"
      m.request.xattr + ("handler-mapping", new urlMapping.Mapping { handlerConf =
        new urlMapping.HandlerConf { index = "00057"; mod = "" }})
      conf = ctrl.config(m.request)
      assertResult("00057") { conf.index }
      assertResult("script1, script2") { conf.facade }
      assertResult("my/account") { conf.pageName }
      assertResult("") { conf.mod }
    }
  }

  "pushSearchState" should {
    "set correct search state string into request" in {
      val ctrl = new BaseController { serviceContainer = m.serviceContainer }
      val myBean = SearchCommand(from = 0, max = 0, sortBy = "id", sortType = "asc", target = 1,
        params = List("prm1", "prm2", "prm3"), filter = List("prmA", "prmB", "prmC"))

      m.request.xinit.init

      val getAttribute = (name: String) => m.request.xattr.get(name)
      when(m.settingService.getItemsPerPage(anyString, any[SiteChain])).thenReturn(10)

      ctrl.pushSearchState(m.request, "myBean", "my-per-page")
      assertResult(None) { getAttribute("searchState") }
      assertResult(None) { getAttribute("searchCommand") }
      assertResult(10) { getAttribute("perPage").get }

      m.request.xmisc.searchBeansMap += ("myBean" -> myBean)

      ctrl.pushSearchState(m.request, "myBean", "mock-per-page")
      assertResult("10,1,,0,0") { getAttribute("searchState").get }
      assertResult(List("prmA", "prmB", "prmC")) { getAttribute("searchCommand").get.asInstanceOf[SearchCommand].filter }

      ctrl.pushSearchState(m.request, "myBean", "mock-per-page", 1, List("prm3", "prm1", "prm2"), 0)
      assertResult("10,1,,0,0") { getAttribute("searchState").get }
      ctrl.pushSearchState(m.request, "myBean", "mock-per-page", 1, List("prm1", "prm2", "prm3"), 0)
      assertResult("10,1,id,asc,0") { getAttribute("searchState").get }
      ctrl.pushSearchState(m.request, "myBean", "mock-per-page", 1, List("prm1", "prm2", "prm3"), 3)
      assertResult("10,3,id,asc,1") { getAttribute("searchState").get }
    }
  }

  "handleRequest" should {
    "be thread safe and not change variables" in {
      val ctrl = new BaseController { index = "00055"; facade = "script1, script2"; pageName = "my/account"
        serviceContainer = m.serviceContainer }
      val urlMapping = new UrlMapping
      m.request.xinit.init
      val xattr = m.request.xattr
      xattr + ("handler-mapping" -> new urlMapping.Mapping)

      ctrl.handleRequest(m.request, m.response)
      assertResult("00055") { ctrl.index }
      assertResult("script1, script2") { ctrl.facade }
      assertResult("my/account") { ctrl.pageName }
      assertResult("00055") { xattr.get("pageIndex").get }
      assertResult(List("script1", "script2")) { xattr.get("pageFacade").get.asInstanceOf[Array[String]].toList }
      assertResult("my/account") { xattr.get("pageName").get }

      xattr + ("pageIndex" -> "00056")
      xattr + ("handler-mapping" -> new urlMapping.Mapping { handlerConf =
        new urlMapping.HandlerConf { index = "00056"; facade = "my/Script, esatte/MessageF"; pageName = "my/acc2" }})
      ctrl.handleRequest(m.request, m.response)
      assertResult("00055") { ctrl.index }
      assertResult("script1, script2") { ctrl.facade }
      assertResult("my/account") { ctrl.pageName }
      assertResult("00056") { xattr.get("pageIndex").get }
      assertResult(List("my/Script", "esatte/MessageF")) { xattr.get("pageFacade").get.asInstanceOf[Array[String]].toList }
      assertResult("my/acc2") { xattr.get("pageName").get }
    }
  }

}

@RunWith(classOf[JUnitRunner])
class DynamicControllerSpec extends WordSpec with MockitoSugar with BeforeAndAfter {
  var m: MyMock = _

  before {
    m = new MyMock
  }

  class MyMock extends com.coldcore.haala.web.test.MyMock with SetupMods {
    runSetupMods()
  }

  "handleRequest" should {
    "use original controller page name" in {
      val ctrl = new DynamicController { index = "00055"; facade = "script1"; pageName = "dynamic"
        serviceContainer = m.serviceContainer }
      val urlMapping = new UrlMapping
      m.request.xinit.init
      val xattr = m.request.xattr

      xattr + ("handler-mapping" -> new urlMapping.Mapping { handlerConf =
        new urlMapping.HandlerConf { index = "00056"; facade = "script2"; pageName = "my/acc2" }})

      ctrl.handleRequest(m.request, m.response)
      assertResult("dynamic") { ctrl.pageName }
      assertResult("dynamic") { xattr.get("pageName").get }
      assertResult("my/acc2") { xattr.get("dynamicPageName").get }
    }

    "404 if controller config cannot be loaded" in {
      val ctrl = new DynamicController { index = "00055"; facade = "script1"; pageName = "dynamic"
        serviceContainer = m.serviceContainer }
      val urlMapping = new UrlMapping
      m.request.xinit.init
      val xattr = m.request.xattr
      xattr + ("handler-mapping" -> new urlMapping.Mapping)

      ctrl.handleRequest(m.request, m.response)
      assertResult("dynamic") { ctrl.pageName }
      assertResult(None) { xattr.get("pageName") }
      assertResult(None) { xattr.get("dynamicPageName") }
    }
  }

}
