package com.coldcore.haala
package web
package controller

import logic.SitemapController
import org.scalatest.mockito.MockitoSugar
import org.mockito.Mockito._
import org.mockito.ArgumentMatchers._
import org.scalatest._
import org.junit.runner.RunWith
import org.scalatest.junit.JUnitRunner
import com.coldcore.haala.core.Timestamp
import com.coldcore.haala.core.SiteChain.Implicits._
import com.coldcore.haala.core.VirtualPath.Implicits._
import test.SetupMods
import service.{Cache, MiscServiceImpl}
import core.{BinaryFile, WebConstants}

@RunWith(classOf[JUnitRunner])
class SitemapControllerSpec extends WordSpec with MockitoSugar with BeforeAndAfter {
  var m: MyMock = _

  before {
    m = new MyMock
  }

  class MyMock extends com.coldcore.haala.web.test.MyMock with SetupMods {
    serviceContainer.services = //use real misc service
      (new MiscServiceImpl |< { x =>
        x.serviceContainer = serviceContainer
        x.velocityEngine = vmEngine
      }) :: serviceContainer.services

    val templatesCache = mock[Cache[String]]
    val controller = new SitemapController |< { x =>
      x.serviceContainer = serviceContainer
      x.templatesCache = templatesCache
    }
    val controllerSpy = spy(controller)

    runSetupMods()
    request.setParameter("site", "mc1")

    when(settingService.query("MainURL", "xx1.mc1")).thenReturn("http://example.org")
    when(templatesCache.get(anyString)).thenReturn(None)
    when(settingService.query("UrlMapping", ".mc1")).thenReturn(urlMapping_mc1)
  }

  implicit class Inliner(x: String) { def inline = x.replaceAll("\\r?\\n|\\r", " ").replaceAll("\\s+", " ") }

  val sitemap_csv =
    "portfolio.htm, services.htm, projects.htm"

  val urlMapping_mc1 = """
    {
      "mapping": [
        { "pattern": "/", "handler": "dynamicCtrl" },
        { "pattern": "/about/about-us.htm", "handler": "dynamicCtrl", params: "sitemap, my" },
        { "pattern": "contact.htm", "handler": "dynamicCtrl" }
      ]
    }
    """

  val sitemap_xml_vm =
    """
      |<urlset>
      |  <url>
      |    <loc>$MainURL/</loc>
      |    <lastmod>$LastMod</lastmod>
      |  </url>
      |  #foreach ($p in $urls)
      |    <url>
      |      <loc>$MainURL/$p</loc>
      |      <lastmod>$LastMod</lastmod>
      |    </url>
      |  #end
      |</urlset>
    """.stripMargin

  val sitemap_htm_vm =
    """
      |<html>
      |  <head><title>Sitemap</title></head>
      |  <body>
      |    <h3>Generated on $LastMod</h3>
      |    <a href="/">Homepage</a><br>
      |    #foreach ($p in $urls)
      |      <a href="/$p">$p</a><br>
      |    #end
      |  </body>
      |</html>
    """.stripMargin

  "handleRequest" should {
    "construct sitemap XML file" in {
      m.request.setRequestURI("/sitemap.xml")
      doReturn(sitemap_csv, Nil: _*).when(m.controllerSpy).readWebResource("etc/sitemap.csv", "xx1.mc1")
      m.registerFile(1, "web/sitemap.xml.vm", "xx1.mc1", sitemap_xml_vm)

      m.controllerSpy.handleRequest(m.request, m.response)
      val lastmod = Timestamp.fromMills(System.currentTimeMillis-1000L*60L*60L*24L).toString("yyyy-MM-dd")
      val expected =
        s"""
          <urlset>
            <url>
              <loc>http://example.org/</loc>
              <lastmod>$lastmod</lastmod>
            </url>
            <url>
              <loc>http://example.org/portfolio.htm</loc>
              <lastmod>$lastmod</lastmod>
            </url>
              <url>
              <loc>http://example.org/services.htm</loc>
              <lastmod>$lastmod</lastmod>
            </url>
            <url>
              <loc>http://example.org/projects.htm</loc>
              <lastmod>$lastmod</lastmod>
            </url>
            <url>
              <loc>http://example.org/about/about-us.htm</loc>
              <lastmod>$lastmod</lastmod>
            </url>
          </urlset>
        """
      assertResult(expected.inline) { m.request.xattr.get[BinaryFile](WebConstants.BINARY_FILE).get.asString.inline }
    }

    "construct sitemap HTML file" in {
      m.request.setRequestURI("/sitemap.htm")
      doReturn(sitemap_csv, Nil: _*).when(m.controllerSpy).readWebResource("etc/sitemap.csv", "xx1.mc1")
      m.registerFile(1, "web/sitemap.htm.vm", "xx1.mc1", sitemap_htm_vm)

      m.controllerSpy.handleRequest(m.request, m.response)
      val lastmod = Timestamp.fromMills(System.currentTimeMillis-1000L*60L*60L*24L).toString("yyyy-MM-dd")
      val expected =
        s"""
          <html>
            <head><title>Sitemap</title></head>
            <body>
              <h3>Generated on $lastmod</h3>
              <a href="/">Homepage</a><br>
              <a href="/portfolio.htm">portfolio.htm</a><br>
              <a href="/services.htm">services.htm</a><br>
              <a href="/projects.htm">projects.htm</a><br>
              <a href="/about/about-us.htm">about/about-us.htm</a><br>
            </body>
          </html>
        """
      assertResult(expected.inline) { m.request.xattr.get[BinaryFile](WebConstants.BINARY_FILE).get.asString.inline }
    }

  }

}
