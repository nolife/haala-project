package com.coldcore.haala
package web
package controller.logic
package spam

import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse

import BaseController.Mod
import service.{SpamService, UserRoleCheck}

class StorageMod extends Mod with UserRoleCheck {
  override def process(ctrl: BaseController, request: HttpServletRequest, response: HttpServletResponse): Option[String] = {
    val id = request("id").safeLong
    if (isUserAdmin) sc.get[SpamService].getStorageById(id).foreach(storage => request.xattr + ("storage" -> storage))
    None
  }
}
