package com.coldcore.haala
package web
package controller.logic

import scala.collection.JavaConverters._
import javax.servlet.http.{HttpServletRequest, HttpServletResponse}
import org.springframework.web.servlet.{HandlerAdapter, ModelAndView}
import org.springframework.web.bind.ServletRequestDataBinder
import org.springframework.web.multipart.support.ByteArrayMultipartFileEditor
import org.springframework.web.bind.annotation.{InitBinder, RequestMapping}
import com.coldcore.misc.scala.StringOp._
import com.coldcore.misc5.CByte._
import java.io._
import java.util.Properties

import com.google.gson.Gson
import com.coldcore.haala.core._
import java.util.regex.Pattern
import java.net.URLDecoder

import beans.BeanProperty
import service._
import service.FileService.Body
import service.MiscService.VelocityTemplateIN
import core._
import core.{Dependencies, SearchCommand}
import core.BinaryFile.{BodyData, ByteData, StringData}
import controller.annotations.AopController
import com.coldcore.haala.domain.File._
import com.coldcore.haala.core.Constants.UTF8

object BaseController {
  trait Mod extends Dependencies {
    def process(ctrl: BaseController, request: HttpServletRequest, response: HttpServletResponse): Option[String]
  }
}

/** Trait to create proper interface-based proxy for controller mixins */
trait ControllerProxy {
  @RequestMapping
  def handleRequest(request: HttpServletRequest, response: HttpServletResponse): ModelAndView
}

trait Implicits {
  implicit def string2modelAndView(x: String): ModelAndView = new ModelAndView(x)
}

@AopController
class BaseController extends ControllerProxy with Dependencies with SettingsReader with UserRoleCheck with Implicits {
  import BaseController._

  private lazy val LOG = new NamedLog("base-controller", log)

  @BeanProperty var downtime = "1" // default: page goes down when system goes down
  @BeanProperty var view: String = _
  @BeanProperty var downtimeView: String = _
  @BeanProperty var notfoundView: String = _
  @BeanProperty var pageName: String = _
  @BeanProperty var facade = ""
  @BeanProperty var index = ""
  @BeanProperty var mod = ""

  /** Return combined configuration for a controller (DB parameters take precedence over hardcoded). */
  def config(request: HttpServletRequest): UrlMapping#HandlerConf = {
    val hm = request.xattr[UrlMapping#Mapping]("handler-mapping")
    val urlMapping = new UrlMapping
    val hc = Option(hm.handlerConf).getOrElse(new urlMapping.HandlerConf)
    new urlMapping.HandlerConf |< { x =>
      x.index = hc.index or request.xattr[String]("pageIndex", index) //may change by DynamicController
      x.mod = hc.mod nil mod
      x.facade = hc.facade nil facade
      x.pageName = hc.pageName nil pageName
      x.downtime = hc.downtime nil downtime
      x.options = hc.options.safe
    }
  }

  def redirectPage(view: String = view): ModelAndView = "redirect:"+view

  def goPage(request: HttpServletRequest, view: String = view): ModelAndView = {
    val (conf, xattr) = (config(request), request.xattr)
    xattr + ("pageIndex" -> conf.index, "pageFacade" -> conf.facade.parseCSV.toArray)
    conf.options.parseCSV.map(xattr.addCtrlOption)

    // if DynamicController handled request then use original page name (dynamic) otherwise use configured
    xattr + ("pageName", xattr.has("dynamicPageName") ? pageName | conf.pageName)

    view
  }

  def goDowntimePage(request: HttpServletRequest): ModelAndView = {
    request.xuser.logout.foreach(user => LOG.info("User "+user.username+" kicked out (downtime)"))
    request.xattr.pushResponseCode(HttpServletResponse.SC_SERVICE_UNAVAILABLE)
    downtimeView
  }

  override def handleRequest(request: HttpServletRequest, response: HttpServletResponse): ModelAndView = {
    val conf = config(request)
    if (setting("DowntimeFlag").isTrue && conf.downtime.isTrue && !isUserAdmin) goDowntimePage(request)
    else conf.mod.parseCSV match {
      case Seq() => goPage(request)
      case xs =>
        val v = (for (x <- xs) yield
          applicationContextHolder.get.getBean(x).asInstanceOf[Mod].process(this, request, response)).
          filter(_.isDefined).lastOption.getOrElse(Some(view)).get
        goPage(request, v)
    }
  }

  /** Extract a command and push search and filter as request attribute to include into a page (for JS to use)
    * searchBeanKey   - SearchBeanKey key in session to extract SearchCommand
    * ppKey           - per page setting key
    * target/params   - if match to saved SearchCommand then include sort
    * page            - if greater than 0 then set as current page with refresh otherwise use SearchCommand
    */
  def pushSearchState(request: HttpServletRequest, searchBeanKey: String, ppKey: String,
                      target: Int = 0, params: Seq[String] = Seq.empty, page: Int = 0) {
    val perPage = settingService.getItemsPerPage(ppKey, request.xsite.current)
    val sbean = request.xmisc.searchBeansMap.getAs[SearchCommand](searchBeanKey).orNull
    val xattr = request.xattr

    if (sbean != null) {
      val refresh = if (page > 0) 1 else 0
      val curPage = if (page > 0) page else (sbean.from/perPage).toInt+1

      val state =
        if ((target <= 0 || target == sbean.target) && sbean.params == params)
          perPage+","+curPage+","+sbean.sortBy+","+sbean.sortType+","+refresh
        else perPage+","+curPage+",,0,"+refresh

      xattr + ("searchState" -> state, "searchCommand" -> sbean)
    }

    xattr + ("perPage" -> perPage)
  }
}

/** Handles arbitrary pages. */
@AopController
class DynamicController extends BaseController {
  override def handleRequest(request: HttpServletRequest, response: HttpServletResponse): ModelAndView = {
    //dynamic page must have explicit handler configuration or it does not exist
    val hm = request.xattr[UrlMapping#Mapping]("handler-mapping")
    Option(hm.handlerConf) match {
      case Some(hc) =>
        val xattr = request.xattr
        val uri = request.getRequestURI

        //get page index from handler configuration (01enquiry) OR generate (foo.com/bar/mypage.htm -> 00mypage)
        val pageIndex = hc.index or ("00"+uri.split("/").last.split("\\.").head)
        xattr + ("pageIndex" -> pageIndex) //change controller page index as "config" method depends on it

        //get page name from handler configuration (other/enquiry) OR generate (00mypage -> mypage)
        xattr + ("dynamicPageName", ((hc.pageName or pageName) != pageName) ? hc.pageName | pageIndex.substring(2))
        super.handleRequest(request, response)
      case None =>
        notfoundView
    }
  }
}

/** Redirects to page.  */
@AopController
class RedirectController extends BaseController {
  override def goPage(request: HttpServletRequest, view: String): ModelAndView =
    redirectPage(config(request).pageName)
}

@AopController
class EmptyController extends ControllerProxy with Implicits {
  @BeanProperty var view: String = _

  override def handleRequest(request: HttpServletRequest, response: HttpServletResponse): ModelAndView = view
}

@AopController
class ErrorController extends ControllerProxy {
  override def handleRequest(request: HttpServletRequest, response: HttpServletResponse): ModelAndView =
    throw request.xattr[Exception]("javax.servlet.error.exception", new NoTraceException("Unknown render error, check logs")) //view will be set by ExceptionResolver
}

/** Return request "path" parameter or guess it from URL.  */
trait RequestPath extends Implicits {
  val requestPath = (request: HttpServletRequest) => {
    val x = request("path") or request.getRequestURI.substring(1)
    VirtualPath(URLDecoder.decode(x, UTF8))
  }
}

trait BinaryOutput {
  def output(file: BinaryFile, request: HttpServletRequest) = request.xattr + (WebConstants.BINARY_FILE -> file)
  def output(filename: String, body: Body, request: HttpServletRequest): Unit = output(BinaryFile(filename, body), request)
  def output(filename: String, content: String, request: HttpServletRequest): Unit = output(BinaryFile(filename, content), request)
}

@AopController
class BinaryController extends ControllerProxy with Dependencies with RequestPath with SettingsReader with UserRoleCheck with BinaryOutput with Implicits {
  @BeanProperty var view: String = _
  @BeanProperty var viewNC: String = _

  override def handleRequest(request: HttpServletRequest, response: HttpServletResponse): ModelAndView = {
    val (siteK, sfx, cms) = (request("site"), request("sfx"), request("cms").isTrue)
    val ndotSiteK = siteK.startsWith(".") ? siteK.substring(1) | siteK
    val inmap = () => request.xsite.chainMap.chainByKey(ndotSiteK).nonEmpty

    val site = //CMS: if site chain cannot be resolved but user owns a domain then use site key as is
      if (cms && !inmap() && (isUserAdmin || sc.get[UserService].isSiteOwner(request.xuser.currentId, ndotSiteK))) SiteChain(siteK) //user owns a domain
      else request.xsite.resolve(siteK)

    //developer instruction to replace all images with blanks
    def noImage(b: Body): Option[Array[Byte]] =
      if (setting("Dev/NoImages").isTrue) //images disabled (dev mode flag)
        try {
          val size = sc.get[PictureService].imageSize(b)
          val jpeg = sc.get[PictureService].blankJpeg(size._1, size._2)
          Some(jpeg)
        } catch { case _: Throwable => None }
      else None

    sc.get[FileService].getByPath(requestPath(request), site) match {
      case Some(f) =>
        val b = sc.get[FileService].getBody(f, (if (sfx == SUFFIX_ORIGINAL) "" else sfx) or SUFFIX_DEFAULT) // forbid access to original content
        output(new BinaryFile(request("name") or requestPath(request).filename,
                              noImage(b).map(new ByteData(_)).getOrElse(new BodyData(b))), request)
      case None =>
    }

    request("nc") on viewNC or view // view without or with cache
  }
}

class FaviconController extends BinaryController {
  override def handleRequest(request: HttpServletRequest, response: HttpServletResponse): ModelAndView = {
    sc.get[FileService].getByPath(VirtualPath("web/etc/favicon.ico"), request.xsite.current)
      .foreach(f => output("favicon.ico", sc.get[FileService].getBody(f), request))
    view
  }
}

@AopController
class LabelsController extends ControllerProxy with Dependencies with ContentMaker with BinaryOutput with Implicits {
  @BeanProperty var view: String = _

  private def prepareContent(fname: String, site: SiteChain): String = {
    val regexSlashes = (m: Map[String,String]) => m.map { case (k,v) => k -> v.replace("\\", "\\\\") }

    val model: Map[String,AnyRef] = Map("map" ->
      (sc.get[LabelService].getForSite(site) ++
       regexSlashes(sc.get[SettingService].getForSiteLike("RegEx/%", SiteChain.sys))).asJava)
    sc.get[MiscService].velocityTemplate(VelocityTemplateIN("web/"+fname+".vm:"+site, model)).get
  }

  override def handleRequest(request: HttpServletRequest, response: HttpServletResponse): ModelAndView = {
    val siteK = request("site")
    val (site, rv, fname) = (request.xsite.resolve(siteK), request.xmisc.resourceVersion, "labels.js")
    try {
      request.xsite.chainMap.assertXtraChainByKey(siteK) // allow extra chains only: mw1j mw2b
      makeContent("lbl/"+fname+"/"+site+"/"+rv, prepareContent(fname, site))(output("labels_"+siteK+"_"+rv+".js", _, request))
    } catch { case _: CoreException => }
    view
  }
}

/** Read JS or CSS file from WAR or DB and glue several files together. */
trait WebResourceReader {
  self: {
    val sc: ServiceContainer
    def resourceLoaderHolder: ResourceLoaderHolder
  } =>

  val webResourceType: String // either "js" or "css"

  private def readFromDB(path: VirtualPath, site: SiteChain): Option[String] =
    sc.get[FileService].getByPath(path, site).map(f =>
      withOpen(sc.get[FileService].getBody(f).sourceStream) { r => new String(toByteArray(r), UTF8) })

  private def readFromWar(path: VirtualPath): Option[String] =
    resourceLoaderHolder.get.getResource(path.path) match {
      case rs if rs.exists =>
        val x = withOpen(new BufferedInputStream(rs.getInputStream)) { r => new String(toByteArray(r), UTF8) }
        Some(x)
      case _ => None
    }

  def readWebResource(path: VirtualPath, site: SiteChain): String =
    readFromDB(path, site) orElse readFromDB(VirtualPath("web/"+path), site) orElse readFromWar(path) getOrElse ""

  val noopOnEach = (path: String, x: String) => x

  /** Embed CSS if "css::import" declaration found (for JS files "js" token must be used instead of "css").
    *  css::import { path = pages/common; css = one, two, three } { path = ; css = foo, bar }
    *  eg. pages/common/one.css will be read and embedded into the content
    */
  def glueWebResource(site: SiteChain)(content: String, onEach: (String, String) => String): String = {
    val f = (v: String) => {
      val pmap = v.parseCSVMap(";")
      val (p, c) = (pmap("path"), pmap(webResourceType).parseCSV)
      val path = p on p+"/"
      c.map(x => readWebResource(VirtualPath(path+x+"."+webResourceType), site)).map(onEach(path, _)).mkString("\n\n")
    }

    val (pfx, sfx) = (s"/* $webResourceType::import", "*/")
    val (qpfx, qsfx) = (Pattern.quote(pfx), Pattern.quote(sfx))
    if (!content.contains(pfx)) onEach("", content) //no declaration
    else (onEach("", content) /: (1 to 100)) { (s, _) => //process declarations in imported files
      findTokens(qpfx, qsfx, s).headOption match {
        case None => s
        case Some(part) =>
          val x = findTokens("\\{", "\\}", part).map(f).mkString("\n\n")
          s.substring(0, s.indexOf(pfx))+x+s.substring(s.indexOf(sfx, s.indexOf(pfx))+sfx.length)
      }
    }
  }

}

trait ContentMaker {
  self: { def templatesCache: Cache[String] } =>

  def makeContent(cacheKey: String, make: => String)(after: String => Unit) {
    val template = templatesCache.get(cacheKey).getOrElse("")
    val content = template or make
    if (content != "") {
      if (template == "") templatesCache.set(cacheKey, content)
      after(content)
    }
  }
}

@AopController
class CssController extends ControllerProxy with Dependencies with WebResourceReader with RequestPath with ContentMaker with BinaryOutput with Implicits {
  @BeanProperty var view: String = _
  override val webResourceType = "css"

  /** Insert class content from other CSS if "css::class" declaration found.
   *  css::class { css = foo/bar; class = .one, .two, .three } { css = extra; class = .foo, .bar }
   *  eg. foo/bar.css will be read, import declarations processed, internal of class .one { ... }
   *  will be inserted into the content:
   *  .my { ... insert } -> .one { border:0px; } -> .my { ... border:0px; }
   */
  private def cssClass(site: SiteChain, root: VirtualPath)(css: String): String = {
    val f = (v: String) => {
      val pmap = v.parseCSVMap(";")
      val (path, c) = (VirtualPath(pmap("css")+".css"), pmap("class").parseCSV)
      val s = glueWebResource(site)(readWebResource(path, site), prependNonAbsolute(root)) //embed other CSS
      c.map(x => findTokens(Pattern.quote("\n"+x+" {"), "}", s).mkString(";\n")).mkString(";\n")
    }

    val (pfx, sfx) = ("/* css::class", "*/")
    val (qpfx, qsfx) = (Pattern.quote(pfx), Pattern.quote(sfx))
    (css /: findTokens(qpfx, qsfx, css)) { (s, a) =>
      val x = findTokens("\\{", "\\}", a).map(f).mkString("\n\n")
      s.substring(0, s.indexOf(pfx))+x+s.substring(s.indexOf(sfx, s.indexOf(pfx))+sfx.length)
    }
  }

  private val validExtension = (content: String, extensions: List[String]) =>
    extensions.flatMap(ext => s"(?i)\\.$ext".r.findAllIn(content)).toSet

  private def appendSiteAndVersion(siteK: String, rv: String)(css: String): String = {
    val extensions = sc.get[SettingService].querySystem("CssCtrlExtensions").parseCSV
    (css /: validExtension(css, extensions))((c, x) => c.replace(x, x+"?site="+siteK+"&rv="+rv))
  }

  /** url(foo/bar.gif) -> url(/abc/foo/bar.gif) */
  private def prependNonAbsolute(root: VirtualPath)(path: String, css: String): String = {
    val extensions = sc.get[SettingService].querySystem("CssCtrlExtensions").parseCSV
    val p = "/"+(path or (root.folder on root.folder+"/"))
    val regex = """(\(\s*["|']?\s*)(\w[\w\/\.-]+)(\s*["|']?\s*\))""".r // ( "foo/bar.gif" )
    val xs = findTokens("\\(", "\\)", css).map("("+_+")")
    (css /: xs) { (content, x) =>
      regex.findFirstMatchIn(x).map(_.subgroups) match {
        case Some(List(a, b, c)) if validExtension(b, extensions).nonEmpty => content.replace(x, a+p+b+c)
        case _ => content
      }
    }
  }

  private def stripComments(css: String): String = {
    val (pfx, sfx) = ("/*", "*/")
    val (qpfx, qsfx) = (Pattern.quote(pfx), Pattern.quote(sfx))
    if (!css.contains("/* strip::comments")) css
    else (css /: findTokens(qpfx, qsfx, css))((s, t) => s.replace(pfx+t+sfx, ""))
  }

  private def applyPalette(site: SiteChain)(css: String): String = {
    val f = sc.get[FileService].getByPath(VirtualPath("web/palette.prop"), site).get
    val p = new Properties
    withOpen(sc.get[FileService].getBody(f).sourceStream) { p.load }

    var s = css
    val pp = p.toMap.asInstanceOf[Map[String,String]] // FF0000=CC0000 CC0000=FF0000
    for ((k,v) <- pp if v != "") s = s.replace("#"+k, "#"+k+"^") // #FF0000 #CC0000 -> #FF0000^ #CC0000^
    for ((k,v) <- pp) s = s.replace("#"+k+"^", "#"+v) // #FF0000^ #CC0000^ -> #CC0000 #FF0000
    s
  }

  private def prepareCss(request: HttpServletRequest): String = {
    val siteK = request("site")
    val (site, rv) = (request.xsite.resolve(siteK), request.xmisc.resourceVersion)
    val path = requestPath(request) // abc/foo/my.css

    readWebResource(path, site) |> (glueWebResource(site)(_, prependNonAbsolute(path))) |>
      cssClass(site, path) |> appendSiteAndVersion(siteK, rv) |> applyPalette(site) |> stripComments or "/*empty*/"
  }

  override def handleRequest(request: HttpServletRequest, response: HttpServletResponse): ModelAndView = {
    val siteK = request("site")
    val (site, rv) = (request.xsite.resolve(siteK), request.xmisc.resourceVersion)
    makeContent("css/"+requestPath(request)+"/"+site+"/"+rv, prepareCss(request))(output("tmp_"+siteK+"_"+rv+".css", _, request))
    view
  }
}

@AopController
class JsController extends ControllerProxy with Dependencies with WebResourceReader with RequestPath with SettingsReader with ContentMaker with BinaryOutput with Implicits {
  @BeanProperty var view: String = _
  override val webResourceType = "js"

  private def stripComments(js: String): String = {
    val (pfx, sfx) = ("/*", "*/")
    val (qpfx, qsfx) = (Pattern.quote(pfx), Pattern.quote(sfx))
    if (!js.contains("/* strip::comments")) js
    else (js /: findTokens(qpfx, qsfx, js))((s, t) => s.replace(pfx+t+sfx, ""))
  }

  private def currentDomainApi(request: HttpServletRequest): String = {
    val arr = request.xsite.state.currentDomain.split("\\.")
    if (arr.size > 1) arr(arr.size-2)+"."+arr.last else arr.last // ect.my-site.com -> my-site.com OR localhost
  }

  /** Replace special variables with their values. */
  private def injectVariables(site: SiteChain, request: HttpServletRequest)(js: String): String =
    js.replace("${MainURL}", setting("MainURL", site)).
       replace("${DomainAPI}", currentDomainApi(request))

  private def prepareJs(request: HttpServletRequest): String = {
    val siteK = request("site")
    val site = request.xsite.resolve(siteK)
    val path = requestPath(request)

    readWebResource(path, site) |> (glueWebResource(site)(_, noopOnEach)) |> injectVariables(site, request) |>
      stripComments or "/*empty*/"
  }

  override def handleRequest(request: HttpServletRequest, response: HttpServletResponse): ModelAndView = {
    val siteK = request("site")
    val (site, rv) = (request.xsite.resolve(siteK), request.xmisc.resourceVersion)
    makeContent("js/"+requestPath(request)+"/"+site+"/"+rv, prepareJs(request))(output("tmp_"+siteK+"_"+rv+".js", _, request))
    view
  }
}

object SitemapController {
  trait Mod {
    def appendToModel(request: HttpServletRequest): Map[String,AnyRef]
  }
}

@AopController
class SitemapController extends ControllerProxy with Dependencies with WebResourceReader with SettingsReader with ContentMaker with BinaryOutput with Implicits {
  import SitemapController._

  @BeanProperty var view: String = _
  override val webResourceType = "etc"

  var mods = List.empty[Mod]
  def setMods(x: JList[Mod]) = mods = x.toList

  private def lastModified: String =
    Timestamp.fromMills(System.currentTimeMillis-1000L*60L*60L*24L).toString("yyyy-MM-dd")

  private def prepareContent(request: HttpServletRequest, fname: String, site: SiteChain): String = {
    val urls0 = readWebResource(VirtualPath("etc/sitemap.csv"), site).parseCSV
    val urlMapping = //check only last UrlMapping
      Option(new Gson().fromJson(setting("UrlMapping", SiteChain.key(site.key)).safe,
        classOf[UrlMapping])).getOrElse(new UrlMapping)
    val urls =
      urls0 ++ //hardcoded in "sitemap.csv"
      urlMapping.mapping.filter { x => //page mappings with "sitemap" param
        !x.pattern.exists(urls0.contains) && x.pattern != "/" && !x.pattern.contains("*") &&
         x.params.safe.parseCSV.contains("sitemap")
      }.map(x => x.pattern.startsWith("/") ? x.pattern.substring(1) | x.pattern)

    val model0: Map[String,AnyRef] =
      Map("MainURL" -> setting("MainURL", site), "LastMod" -> lastModified, "urls" -> urls.toArray)
    val model: Map[String,AnyRef] = (model0 /: mods)((a, b) => a ++ b.appendToModel(request)) //process mods
    sc.get[MiscService].velocityTemplate(VelocityTemplateIN("web/"+fname+".vm:"+site, model)).get
  }

  override def handleRequest(request: HttpServletRequest, response: HttpServletResponse): ModelAndView = {
    val (rv, site, fname) = (request.xmisc.resourceVersion, request.xsite.current, request.getRequestURI.substring(1))
    makeContent(fname+"/"+site+"/"+rv, prepareContent(request, fname, site))(output(fname, _, request))
    view
  }
}

object Base3rdController {
  trait Mod extends Dependencies {
    def process(ctrl: Base3rdController, request: HttpServletRequest, response: HttpServletResponse): BinaryFile
  }
}

trait Base3rdControllerProxy extends ControllerProxy {
  @InitBinder
  def initBinder(binder: ServletRequestDataBinder)
}

@AopController
class Base3rdController extends Base3rdControllerProxy with Dependencies with BinaryOutput with Implicits {
  import Base3rdController._

  @BeanProperty var view: String = _
  @BeanProperty var mod: Mod = _

  override def initBinder(binder: ServletRequestDataBinder) =
    binder.registerCustomEditor(classOf[Array[Byte]], new ByteArrayMultipartFileEditor)

  def goPage(request: HttpServletRequest): ModelAndView = view

  def makeBinaryFile(content: String): BinaryFile =
    new BinaryFile("", new StringData(content)) { pureOutput = true }

  protected def handleResult(request: HttpServletRequest, bfile: BinaryFile): ModelAndView = {
    output(bfile, request)
    goPage(request)
  }

  override def handleRequest(request: HttpServletRequest, response: HttpServletResponse): ModelAndView =
    try {
      handleResult(request, Option(mod).map(_.process(this, request, response)).getOrElse(makeBinaryFile("")))
    } catch {
      case e: Throwable =>
        log.error("Base3rdController error", e)
        request.xattr.pushResponseCode(HttpServletResponse.SC_SERVICE_UNAVAILABLE)
        handleResult(request, makeBinaryFile("error"))
    }
}

/** Handle ControllerProxy controllers. Declare as Spring bean. */
class CustomControllerHandlerAdapter extends HandlerAdapter {
  override def supports(handler: AnyRef): Boolean = handler.isInstanceOf[ControllerProxy]

  override def getLastModified(request: HttpServletRequest, handler: AnyRef): Long = -1

  override def handle(request: HttpServletRequest, response: HttpServletResponse, handler: AnyRef): ModelAndView =
    handler.asInstanceOf[ControllerProxy].handleRequest(request, response)
}
