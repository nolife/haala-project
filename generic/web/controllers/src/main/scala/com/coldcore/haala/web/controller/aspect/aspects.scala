package com.coldcore.haala
package web
package controller.aspect

import org.aspectj.lang.ProceedingJoinPoint
import org.aspectj.lang.annotation.{Around, Aspect, Pointcut}
import beans.BeanProperty
import collection.mutable.ListBuffer
import core.{Dependencies, RecentLink, Setup, WebConstants}
import core.Aspect._
import com.coldcore.haala.core._
import com.coldcore.haala.core.GeneralUtil._
import com.coldcore.misc.scala.ReflectUtil._
import com.coldcore.misc.scala.StringOp.{findTokens, parametrize}
import service._
import org.springframework.security.web.savedrequest.HttpSessionRequestCache
import org.springframework.web.servlet.ModelAndView
import javax.servlet.http.{HttpServletRequest, HttpServletResponse, HttpSession}
import java.io.{ByteArrayOutputStream, ObjectOutputStream}
import java.util.regex.Pattern
import security.annotations.SecuredRole
import domain.Role

@Aspect
abstract class AroundControllerAspect extends Dependencies {
  @BeanProperty var order: Int = _

  def around(pjp: ProceedingJoinPoint, request: HttpServletRequest, response: HttpServletResponse): AnyRef

  @Pointcut("@within(com.coldcore.haala.web.controller.annotations.AopController)")
  def controllerMethods = {}

  @Around("controllerMethods()")
  def aroundControllerMethods(pjp: ProceedingJoinPoint) = around(pjp, requestArg(pjp).get, responseArg(pjp).get)

  private def requestArg(pjp: ProceedingJoinPoint): Option[HttpServletRequest] =
    pjp.getArgs.find(_.isInstanceOf[HttpServletRequest]).asInstanceOf[Option[HttpServletRequest]]

  private def responseArg(pjp: ProceedingJoinPoint): Option[HttpServletResponse] =
    pjp.getArgs.find(_.isInstanceOf[HttpServletResponse]).asInstanceOf[Option[HttpServletResponse]]
}

/** Catch exception and forward to exception resolver. Must be the first in a chain. */
class ErrorHandlingAspect extends AroundControllerAspect {
  private lazy val LOG = new NamedLog("[controller.error-handling-aspect]", log)

  override def around(pjp: ProceedingJoinPoint, request: HttpServletRequest, response: HttpServletResponse): AnyRef =
    try {
      pjp.proceed
    } catch {
      case e: WeakCoreException => throw e
      case e: Throwable =>
        val (url, mth) = (request.xmisc.url, request.getMethod.toUpperCase)
        LOG.error("Error "+mth+" "+url, e)
        sc.get[MiscService].reportSystemError("[controller.error-handling-aspect] Error "+mth+" "+url, e)
        throw e
    }
}

/** Log total request execution time */
class LogAspect extends AroundControllerAspect {
  private lazy val LOG = new NamedLog("controller.log-aspect", log)

  override def around(pjp: ProceedingJoinPoint, request: HttpServletRequest, response: HttpServletResponse): AnyRef = {
    val start = System.currentTimeMillis
    val o = pjp.proceed
    LOG.info(request.xmisc.url+" executed in "+millsToSec(System.currentTimeMillis-start)+"s")
    o
  }
}

class DebugAspect extends AroundControllerAspect {
  private lazy val LOG = new NamedLog("controller.debug-aspect", log)

  private def sessionWeight(session: HttpSession) {
    val out = new ByteArrayOutputStream
    for (x <- session.getAttributeNames)
      try {
        new ObjectOutputStream(out).writeObject(x)
      } catch { case _: Throwable => LOG.warn("Failed to serialize session attribute "+x) }
    LOG.info("Session "+session.getId+" weights "+out.size+"b")
  }

  override def around(pjp: ProceedingJoinPoint, request: HttpServletRequest, response: HttpServletResponse): AnyRef = {
    val o = pjp.proceed
    sessionWeight(request.getSession)
    o
  }
}

/** Ensure initial attributes are set. */
class InitialAspect extends AroundControllerAspect with Setup {
  @BeanProperty var statistics: Statistics = _

  var mods = List.empty[Mod]
  def setMods(x: JList[Mod]) = mods = x.toList

  override def around(pjp: ProceedingJoinPoint, request: HttpServletRequest, response: HttpServletResponse): AnyRef = {
    request.serviceContainer = sc
    request.resourceLoaderHolder = resourceLoaderHolder
    request.getSession(true)

    val cache = new HttpSessionRequestCache
    val req = cache.getRequest(request, response)
    val lastUrl = Option(req).map(_.getRedirectUrl).getOrElse("")

    //save current URL to restore after signing in if user is not autheticated
    if (lastUrl != "") {
      request.xattr.perm + (WebConstants.LAST_SECURED_URL -> lastUrl)
      cache.removeRequest(request, response)
    }

    preHandle(request, setupMods, mods)
    val result = pjp.proceed
    postHandle(request, Some(response))

    statistics.requests.incrementAndGet
    result
  }
}

object PageAspect {
  trait Mod {
    def createRecentTitle(request: HttpServletRequest): String
    def createPageTitle(request: HttpServletRequest): String
    def createMetaKeywords(request: HttpServletRequest): String
    def createMetaDescription(request: HttpServletRequest): String
  }

  /** Set request attribute as HTML one liner. */
  def lineAttribute(request: HttpServletRequest, name: String, value: String) =
    request.xattr + (name -> value.convert("html-nlb"))
}

/** Set up recent pages, a page title and its meta info. */
class PageAspect extends AroundControllerAspect with SettingsReader with LabelsReader {
  import PageAspect._

  var mods = List.empty[Mod]
  def setMods(x: JList[Mod]) = mods = x.toList

  private def setupRecentPages(request: HttpServletRequest) {
    val (site, pageIdx)  = (request.xsite.current, request.xmisc.pageIndex)
    request.xattr.perm.setOnce(WebConstants.RECENT_LINKS, new ListBuffer[RecentLink])
    val recent = request.xattr.perm[ListBuffer[RecentLink]](WebConstants.RECENT_LINKS)

    //proceed with indexed pages
    if (pageIdx.nonEmpty)
      mods.map(_.createRecentTitle(request)).find(""!=) getOrElse label("ttl.r"+pageIdx, site).safe match {
        case title if title != "" && !title.contains("{") => //titles with no {variable} - configurable?
          val uri = request.getRequestURI //unique link ID - only one link with the same URI can exist
          val max = settingService.getItemsPerPage("recent")

          recent.filter(_.uri == uri).foreach(recent -= _) //remove existing link
          if (recent.size > max) recent -= recent.last //truncate if a list is over its limit
          RecentLink(uri, title, request.xmisc.uri) +=: recent
        case _ =>
      }

    request.xattr + ("recentLinks" -> recent.toArray)
  }

  private def setPageText(request: HttpServletRequest, name: String, keyPfx: String, mf: Mod => String) {
    val (site, pageIdx) = (request.xsite.current, request.xmisc.pageIndex)
    if (pageIdx.nonEmpty) //proceed with indexed pages only
      mods.map(mf).find(""!=)
        .orElse(Option(label(keyPfx+pageIdx, site).safe)
        .filterNot(x => x.contains("{") || x == "")).foreach(x => lineAttribute(request, name, x))
  }

  private def setupPageTitle(request: HttpServletRequest) {
    val site = request.xsite.current
    lineAttribute(request, "pageTitle", label("site.ttl", site).safe)
    setPageText(request, "pageTitle", "ttl.p", _.createPageTitle(request))
  }

  private def setupMetaKeywords(request: HttpServletRequest) {
    val site = request.xsite.current
    lineAttribute(request, "metaKeywords", label("meta.key", site).safe)
    setPageText(request, "metaKeywords", "met.k", _.createMetaKeywords(request))
  }

  private def setupMetaDescription(request: HttpServletRequest) =
    setPageText(request, "metaDescription", "met.d", _.createMetaDescription(request))

  override def around(pjp: ProceedingJoinPoint, request: HttpServletRequest, response: HttpServletResponse): AnyRef = {
    val result = pjp.proceed
    setupRecentPages(request)
    setupPageTitle(request)
    setupMetaKeywords(request)
    setupMetaDescription(request)
    result
  }
}

/** Check if requested method can be accessed with user roles */
class SecurityAspect extends AroundControllerAspect with SettingsReader with UserRoleCheck {
  private def checkMethodAccess(mth: String, target: AnyRef): Symbol = {
    val m = methodByName(target.getClass, mth).get
    val securedRole = m.getAnnotation(classOf[SecuredRole])

    // all controllers allow anonymous access by default unless restricted to a role
    val role = Option(securedRole).isDefined ? Role.role(securedRole.value) | Role.ANONYMOUS
    if (role == Role.ANONYMOUS) 'pass
    else if (role == Role.API_DEV) setting("Dev/ApiFeatures").isTrue ? 'pass | 'fail
    else if (isUserAdmin || isUserInRole(role)) 'pass
    else 'fail
  }

  override def around(pjp: ProceedingJoinPoint, request: HttpServletRequest, response: HttpServletResponse): AnyRef =
    checkMethodAccess(pjp.getSignature.getName, pjp.getTarget) match {
      case 'pass => pjp.proceed
      case 'fail =>
        request.xattr.pushResponseCode(HttpServletResponse.SC_FORBIDDEN)
        new ModelAndView("forward:/403.htm")
      case _ => throw new IllegalStateException("Unknown access")
    }
}

/** Replace tokens with request parameters
  * REF {requestParam.ref} -> REF TD123FOO
  */
class RequestParamPageMod extends PageAspect.Mod with Dependencies with LabelsReader {

  private def process(request: HttpServletRequest): String = {
    val pfx = "requestParam."
    val template = label("ttl.r"+request.xmisc.pageIndex, request.xsite.current).safe
    if (template.contains("{"+pfx)) {
      val xs = findTokens(Pattern.quote("{"+pfx), "\\}", template).map(x => s"$pfx$x:${request.getParameter(x)}")
      parametrize(template, xs: _*)
    } else ""
  }

  override def createRecentTitle(request: HttpServletRequest): String = process(request)
  override def createPageTitle(request: HttpServletRequest): String = process(request)
  override def createMetaKeywords(request: HttpServletRequest): String = ""
  override def createMetaDescription(request: HttpServletRequest): String = ""
}
