package com.coldcore.haala
package web
package controller.logic
package payment

import javax.servlet.http.{HttpServletRequest, HttpServletResponse}
import BaseController.Mod
import com.coldcore.haala.core.Constants.UTF8
import com.coldcore.haala.core.GeneralUtil.digest
import com.coldcore.haala.core.{NamedLog, Timestamp}
import core.BinaryFile
import service._
import scala.tools.nsc.io.Streamable

class PaymentCheckoutMod extends Mod {
  override def process(ctrl: BaseController, request: HttpServletRequest, response: HttpServletResponse): Option[String] = {
    val ref = request("ref") or request.last // .../pay.htm?ref=3I51B1R4   .../3I51B1R4   3I51B1R4?foo=bar -> 3I51B1R4
    if (ref.nonEmpty) sc.get[PaymentService].searchByRef(ref).foreach { entry =>
      if (entry.active) request.xattr + ("paymentEntry" -> entry)
    }
    None
  }
}

class MyPaymentsEntriesMod extends Mod {
  override def process(ctrl: BaseController, request: HttpServletRequest, response: HttpServletResponse): Option[String] = {
    ctrl.pushSearchState(request, "myPayments", "myPayments")
    None
  }
}

class PaymentEntryMod extends Mod with UserRoleCheck {
  override def process(ctrl: BaseController, request: HttpServletRequest, response: HttpServletResponse): Option[String] = {
    val id = request("id").safeLong
    if (isUserAdmin) sc.get[PaymentService].getById(id).foreach(entry => request.xattr + ("paymentEntry" -> entry))
    None
  }
}

/** Forward inbound events to paypal handler */
class PaypalEventMod extends Base3rdController.Mod {
  private lazy val LOG = new NamedLog("paypal-event-mod", log)

  override def process(ctrl: Base3rdController, request: HttpServletRequest, response: HttpServletResponse): BinaryFile = {
    val content = new String(Streamable.bytes(request.getInputStream), UTF8)
    if (content != "") {
      // save event content for 30 days
      val abyss = sc.get[AbyssService].createNow(AbyssService.CreateIN(
        ref = "", pack = "", keep = Timestamp().addDays(30),
        vm = AbyssService.ValueMetaIN(value = content, meta = "vendor=paypal;type=event;tmp=y")))
      LOG.info("Inbound event #"+abyss.id+" saved for 30 days")

      sc.get[QueueService].pushUnique("PaymentEvent", "handler=paypal;abyss="+abyss.id, digest(content, "MD5"))

    } else {
      LOG.warn("Request has no content, ignoring")
    }

    ctrl.makeBinaryFile("0")
  }
}
