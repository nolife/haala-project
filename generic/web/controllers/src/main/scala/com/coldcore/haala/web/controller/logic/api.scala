package com.coldcore.haala
package web
package controller.logic
package api

import javax.servlet.http.{HttpServletRequest, HttpServletResponse}

import com.coldcore.haala.core.NamedLog
import com.coldcore.haala.security.annotations.SecuredRole
import com.coldcore.haala.web.controller.annotations.AopController
import org.springframework.web.servlet.ModelAndView

import scala.beans.BeanProperty
import com.coldcore.haala.web.core.BinaryFile
import com.google.gson.Gson

@AopController
class BaseApiDevController extends Base3rdController {
  @SecuredRole("API_DEV")
  override def handleRequest(request: HttpServletRequest, response: HttpServletResponse): ModelAndView =
    super.handleRequest(request, response)
}

class SystemStatusMod extends Base3rdController.Mod {
  private lazy val LOG = new NamedLog("api.system-status-mod", log)

  class ReplyJson {
    @BeanProperty var timestamp: Long = _
  }

  override def process(ctrl: Base3rdController, request: HttpServletRequest, response: HttpServletResponse): BinaryFile = {
    val reply = new ReplyJson { timestamp = System.currentTimeMillis }
    val bfile = ctrl.makeBinaryFile(new Gson().toJson(reply, classOf[ReplyJson]))
    bfile.mimeType = "application/json; charset=UTF-8"
    bfile.action = "inline"
    bfile
  }
}

