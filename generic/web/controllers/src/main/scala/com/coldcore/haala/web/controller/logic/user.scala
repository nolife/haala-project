package com.coldcore.haala
package web
package controller.logic
package user

import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse
import org.springframework.web.servlet.ModelAndView
import BaseController.Mod
import com.coldcore.haala.core.NamedLog
import controller.annotations.AopController
import beans.BeanProperty
import service.exceptions.{ObjectExpiredException, ObjectNotFoundException}
import service.{UserRoleCheck, UserService}
import core.Dependencies

class ActivateMod extends Mod {
  private lazy val LOG = new NamedLog("user.activate-ctrl-mod", log)

  override def process(ctrl: BaseController, request: HttpServletRequest, response: HttpServletResponse): Option[String] = {
    val ref = request("ref").toUpperCase
    if (ref.nonEmpty) sc.get[UserService].getByRef(ref) match {
      case Some(user) =>
        sc.get[UserService].activate(user.id)
        request.xattr + ("user" -> user)
        LOG.info("Activated user "+user.username)
      case None => LOG.warn("User not found, ref "+ref)
    }
    None
  }
}

class MyUsersMod extends Mod {
  override def process(ctrl: BaseController, request: HttpServletRequest, response: HttpServletResponse): Option[String] = {
    ctrl.pushSearchState(request, "myUsers", "myUsers")
    None
  }
}

class UserMod extends Mod with UserRoleCheck {
  override def process(ctrl: BaseController, request: HttpServletRequest, response: HttpServletResponse): Option[String] = {
    val id = request("id").safeLong
    if (isUserAdmin) sc.get[UserService].getById(id).foreach(user => request.xattr + ("user" -> user))
    None
  }
}

@AopController
class SsoController extends ControllerProxy with Dependencies with Implicits {
  private lazy val LOG = new NamedLog("user.sso-controller", log)

  @BeanProperty var view: String = _

  /** Execute actions after successful login before redirecting. */
  private def afterLogin(request: HttpServletRequest, params: Seq[String]) {
    if (params.contains("encms")) request.xcms.enable(true) //enable CMS with 'pure-cms' parameter
  }

  override def handleRequest(request: HttpServletRequest, response: HttpServletResponse): ModelAndView = {
    val ticket = request("tck")
    val params = request.getParameterValues("prm").toList

    try {
      sc.get[UserService].loginSso(ticket) match {
        case Some(user) =>
          request.xinit.reset

          if (request.xuser.isLoginForbidden(user)) {
            //forbidden to login to this domain
            LOG.info("User "+user.username+" kicked (forbidden)")
          } else {
            //login successful
            request.xuser.login(user)
            LOG.info("User "+user.username+" login successful ("+request.getRemoteHost+")")
            if (params.nonEmpty) afterLogin(request, params)
          }

        case None => LOG.info("Ticket "+ticket+" login failed")
      }
    } catch {
      case _: ObjectExpiredException => LOG.info("Ticket "+ticket+" expired")
      case _: ObjectNotFoundException => LOG.info("User not found, ticket "+ticket)
    }

    "redirect:"+view
  }
}
