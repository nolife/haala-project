package com.coldcore.haala
package web
package controller.logic
package message

import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse
import BaseController.Mod
import com.coldcore.haala.core.Meta
import service._

class FeedbackMod extends Mod {
  override def process(ctrl: BaseController, request: HttpServletRequest, response: HttpServletResponse): Option[String] = {
    request.xuser.current.foreach { user =>
      request.xattr + ("email" -> user.email, "name" -> sc.get[UserService].fullName(user, request.xsite.current))
    }
    request.xturing.generate
    None
  }
}

class MessageMod extends Mod with UserRoleCheck {
  override def process(ctrl: BaseController, request: HttpServletRequest, response: HttpServletResponse): Option[String] = {
    val ref = request("ref")
    if (ref.nonEmpty) sc.get[MessageService].searchByRef(ref).foreach { message =>
      if (message.user.id == request.xuser.currentId || isUserAdmin) {
        sc.get[MessageService].read(message.abyss.id)
        request.xattr + ("messageEntry" -> message)
      }
    }
    None
  }
}

class MyMessagesMod extends Mod with SettingsReader {
  override def process(ctrl: BaseController, request: HttpServletRequest, response: HttpServletResponse): Option[String] = {
    val trg = request("trg").or("1").toInt
    val user = trg match {
      case 1 => sc.get[UserService].getById(request.xuser.currentId).get
      case _ => sc.get[UserService].getSystemUser
    }
    val forward = Meta(user.meta)("msgfwd", "y").isTrue
    request.xattr + ("trg" -> trg, "fwdEmail" -> (if (forward) "y" else "n"))
    ctrl.pushSearchState(request, if (trg == 1) "myMessages" else "sysMessages", "myMessages")
    None
  }
}
