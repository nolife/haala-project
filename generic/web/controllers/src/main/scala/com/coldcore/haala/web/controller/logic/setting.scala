package com.coldcore.haala
package web.controller.logic
package setting

import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse
import org.springframework.web.servlet.ModelAndView
import BaseController.Mod

class MySettingsMod extends Mod {
  override def process(ctrl: BaseController, request: HttpServletRequest, response: HttpServletResponse): Option[String] = {
    ctrl.pushSearchState(request, "mySettings", "mySettings")
    None
  }
}
