package com.coldcore.haala
package web.controller.logic
package credit

import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse
import BaseController.Mod

class CreditMod extends Mod {
  override def process(ctrl: BaseController, request: HttpServletRequest, response: HttpServletResponse): Option[String] = {
    ctrl.pushSearchState(request, "myCredit", "myCredit")
    None
  }
}
