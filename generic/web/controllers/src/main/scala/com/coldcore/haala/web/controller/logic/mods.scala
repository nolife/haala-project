package com.coldcore.haala
package web
package controller.logic

import javax.servlet.http.{HttpServletRequest, HttpServletResponse}
import BaseController.Mod

class NotFoundMod extends Mod {
  override def process(ctrl: BaseController, request: HttpServletRequest, response: HttpServletResponse): Option[String] = {
    request.xattr.pushResponseCode(HttpServletResponse.SC_NOT_FOUND)
    None
  }
}

class ForbiddenMod extends Mod {
  override def process(ctrl: BaseController, request: HttpServletRequest, response: HttpServletResponse): Option[String] = {
    request.xattr.pushResponseCode(HttpServletResponse.SC_FORBIDDEN)
    None
  }
}

class TuringMod extends Mod {
  override def process(ctrl: BaseController, request: HttpServletRequest, response: HttpServletResponse): Option[String] = {
    request.xturing.generate
    None
  }
}
