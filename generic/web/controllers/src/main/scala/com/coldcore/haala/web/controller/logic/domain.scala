package com.coldcore.haala
package web
package controller.logic
package domain

import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse

import BaseController.Mod
import service.{DomainService, UserRoleCheck}

class DomainMod extends Mod with UserRoleCheck {
  override def process(ctrl: BaseController, request: HttpServletRequest, response: HttpServletResponse): Option[String] = {
    val id = request("id").safeLong
    val domains = request.xuser.current.get.domains

    //get a domain by ID or if a user has just one domain then select it
    val domain = if (id > 0) sc.get[DomainService].getById(id).orNull
                 else if (domains.size == 1) domains.head
                 else null
    if (domain != null && (domains.contains(domain) || isUserAdmin)) request.xattr + ("domain" -> domain)

    None
  }
}
