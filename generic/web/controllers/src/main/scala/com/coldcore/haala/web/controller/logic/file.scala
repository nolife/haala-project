package com.coldcore.haala
package web.controller.logic
package file

import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse
import org.springframework.web.servlet.ModelAndView
import web.controller.logic.BaseController.Mod

class MyFilesMod extends Mod {
  override def process(ctrl: BaseController, request: HttpServletRequest, response: HttpServletResponse): Option[String] = {
    ctrl.pushSearchState(request, "myFiles", "myFiles")
    None
  }
}
