package com.coldcore.haala
package web
package facade.logic

import beans.BeanProperty
import security.annotations.SecuredRole
import javax.servlet.http.HttpServletRequest

import com.coldcore.haala.core.NamedLog
import core.SearchHelper
import facade.vo.cmd.spam._
import facade.annotations.{ErrorCR, ErrorCRs, LogClass, ValidatorClass}
import facade.validator.Validator
import facade.vo.CallResult
import facade.vo.cmd._
import domain.EmailStorage
import service.SpamService.{ByDescriptionSearchInput, CreateStorageIN, StartTaskIN, UpdateStorageIN}
import service.exceptions.{ObjectExistsException, ObjectNotFoundException}
import service.{SearchInput, SpamService}
import BaseFacade.TargetedSearch

/** Convert input and output data */
trait SpamConvert {
  self: SpamFacade =>

  implicit def updateStorageVOtoIN(cmd: UpdateStorageVO) = UpdateStorageIN(cmd.description, cmd.append.safe)
  implicit def addStorageVOtoIN(cmd: AddStorageVO) = CreateStorageIN(cmd.description)
  implicit def startTaskVOtoIN(cmd: StartTaskVO) = StartTaskIN(cmd.storageId.toLong, cmd.from, cmd.subject, cmd.text)

  val storageVO = (o: EmailStorage) =>
    Map('id -> o.id, 'description -> o.description, 'total -> o.total)
}

class SpamFacade extends BaseFacade with SpamConvert {
  @BeanProperty @LogClass lazy val LOG = new NamedLog("spam-facade", log)

  @BeanProperty @ValidatorClass var baseValidator: Validator = _

  @SecuredRole("ADMIN")
  def createStorage(cmd: AddStorageVO, request: HttpServletRequest): CallResult = {
    val o = sc.get[SpamService].createStorage(cmd)
    LOG.info("User "+request.xuser.currentUsername+" created storage #"+o.id)
    dataCR(o.id)
  }

  @SecuredRole("ADMIN")
  @ErrorCR(on = classOf[ObjectNotFoundException], key = "fcall.nofnd", log = "Storage #{storageId} not found")
  def updateStorage(cmd: UpdateStorageVO, request: HttpServletRequest): CallResult = {
    sc.get[SpamService].updateStorage(cmd.storageId.toLong, cmd)
    LOG.info("User "+request.xuser.currentUsername+" updated storage #"+cmd.storageId)
    dataCR()
  }

  @SecuredRole("ADMIN")
  def removeStorage(cmd: RemoveStorageVO, request: HttpServletRequest): CallResult = {
    sc.get[SpamService].deleteStorage(cmd.storageId.toLong)
    LOG.info("User "+request.xuser.currentUsername+" deleted storage #"+cmd.storageId)
    dataCR()
  }

  @SecuredRole("ADMIN")
  @ErrorCRs(Array(
    new ErrorCR(on = classOf[ObjectNotFoundException], key = "fcall.nofnd", log = "Storage #{storageId} not found"),
    new ErrorCR(on = classOf[ObjectExistsException],   key = "fcall.exist", log = "Started task already exists")
  ))
  def startTask(cmd: StartTaskVO, request: HttpServletRequest): CallResult = {
    sc.get[SpamService].startTask(cmd)
    LOG.info("User "+request.xuser.currentUsername+" started task on storage #"+cmd.storageId)
    dataCR()
  }

  @SecuredRole("ADMIN")
  def stopTask(noprm: String, request: HttpServletRequest): CallResult = {
    sc.get[SpamService].stopTask
    LOG.info("User "+request.xuser.currentUsername+" stopped current task")
    dataCR()
  }

  private val searchAllStorages = (si: SearchInput, sh: SearchHelper) =>
    sc.get[SpamService].searchStorages(si).serializeAsMap(storageVO)

  private val searchByDescription = (si: SearchInput, sh: SearchHelper) => {
    //params: description (any text)
    val input = new ByDescriptionSearchInput(si) {
      override val description = sh.param(0)
    }
    sc.get[SpamService].searchStoragesByDescription(input).serializeAsMap(storageVO)
  }

  @SecuredRole("ADMIN")
  def searchStorages(cmd: SearchVO, request: HttpServletRequest): CallResult =
    genericSearch(cmd, request,
      TargetedSearch(1, "", "", orderBy(cmd), searchAllStorages),   //displaying in endless table, no need to save pages/command
      TargetedSearch(2, "", "", orderBy(cmd), searchByDescription)) //...

}
