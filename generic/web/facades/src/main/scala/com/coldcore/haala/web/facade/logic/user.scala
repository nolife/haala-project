package com.coldcore.haala
package web
package facade.logic

import web.facade.annotations.{ErrorCR, ErrorCRs, LogClass, ValidatorClass}
import web.facade.validator.Validator

import beans.BeanProperty
import web.facade.vo.CallResult
import web.facade.vo.cmd._
import web.facade.vo.cmd.user._
import web.core.WebConstants._
import security.annotations.SecuredRole
import service.exceptions._
import service.CreditService.CreditUserIN
import com.coldcore.haala.service.UserService.{AddressIN, IN, UpdatePropsIN}
import web.facade.logic.BaseFacade.{SortMapping, TargetedSearch}
import web.core.SearchHelper
import com.coldcore.misc.scala.StringOp._
import domain.User
import javax.servlet.http.HttpServletRequest

import com.coldcore.haala.core.NamedLog
import service._

import concurrent.ExecutionContext.Implicits.global
import concurrent.Future

/** Convert input and output data */
trait UserConvert {
  self: UserFacade =>

  class BaseUserX(cmd: BaseUserVO) {
    def toIN(username: String, password: String): IN =
      IN(username, password, cmd.email, cmd.firstName, cmd.lastName,
        cmd.primaryNumber, cmd.salutation,
        Option(cmd.address).map(a => AddressIN(a.line1, a.line2, a.town, a.postcode, a.country)).orNull)

    def toIN: IN = toIN(cmd.username, cmd.password)
  }

  implicit def editPropsVOtoIN(cmd: EditPropsVO) =
    UpdatePropsIN(cmd.username, cmd.disabled, cmd.activated,
      cmd.domains.safe.parseCSV, cmd.roles.parseCSV)

  implicit def baseUserX(cmd: BaseUserVO) = new BaseUserX(cmd)
  implicit def creditVOtoIN(cmd: CreditVO) = CreditUserIN("", "message="+cmd.message, "", cmd.amount.toLong)

  val userVO = (o: User) =>
    Map('id -> o.id, 'username -> o.username, 'email -> o.email,
      'name -> trimCut(o.firstName+" "+o.lastName, 100).convert("html-nlb"))
}

class UserFacade extends BaseFacade with UserConvert {
  @BeanProperty @LogClass lazy val LOG = new NamedLog("user-facade", log)

  @BeanProperty @ValidatorClass var userValidator: Validator = _

  @SecuredRole("ANONYMOUS")
  def usernameTaken(username: String, request: HttpServletRequest): CallResult = {
    LOG.info("User "+request.xuser.currentUsername+" queried username "+username)
    dataCR(sc.get[UserService].getByUsername(username.safe.trim).isDefined)
  }

  @SecuredRole("ANONYMOUS")
  @ErrorCRs(Array(
    new ErrorCR(on = classOf[UsernameExistsException], key = "x.exusr", log = "Username {username} already exists"),
    new ErrorCR(on = classOf[EmailExistsException],    key = "x.exeml", log = "Email {email} already exists")
  ))
  def register(cmd: RegisterVO, request: HttpServletRequest): CallResult = {
    val domain = sc.get[DomainService].getById(request.xsite.state.currentDomainId).get
    val user = sc.get[UserService].register(cmd.toIN, domain)

    Future { //may fail if email server is down, user does not need to know about this at this point
      try { sc.get[EmailService].remindPassword(user.username, request.xsite.current)
      } catch {
        case _: EmailServerDownException =>
        case _: InvalidEmailException =>
        case e: Throwable => LOG.error("Error (thread)", e)
      }
    }

    LOG.info("User "+user.username+" registered and activation link sent")
    dataCR()
  }

  @SecuredRole("ANONYMOUS")
  @ErrorCRs(Array(
    new ErrorCR(on = classOf[EmailServerDownException], code = FACADE_ERROR_EMAIL_SERVER_DOWN),
    new ErrorCR(on = classOf[InvalidEmailException],    code = FACADE_ERROR_EMAIL_SERVER_DOWN),
    new ErrorCR(on = classOf[ObjectNotFoundException], key = "x.nuser", log = "User {value} not found")
  ))
  def remindPassword(cmd: ReminderVO, request: HttpServletRequest): CallResult = {
    val user = sc.get[EmailService].remindPassword(cmd.value, request.xsite.current)
    LOG.info("User "+request.xuser.currentUsername+" invoked reminder for "+cmd.value)
    dataCR(user.ref)
  }

  @SecuredRole("ANONYMOUS")
  @ErrorCR(on = classOf[ObjectNotFoundException], key = "x.nuser", log = "User ref {ref} not found")
  def acceptAgreement(cmd: AcceptAgreementVO, request: HttpServletRequest): CallResult = {
    val domain = sc.get[DomainService].getById(request.xsite.state.currentDomainId).get
    sc.get[UserService].acceptAgreement(cmd.ref, domain)
    LOG.info("User ref "+cmd.ref+" accepted agreement")
    dataCR()
  }

  @SecuredRole("USER")
  @ErrorCR(on = classOf[EmailExistsException], key = "fcall.exeml", log = "Email {email} already exists")
  def editDetails(cmd: EditDetailsVO, request: HttpServletRequest): CallResult = {
    val user = request.xuser.current.get
    sc.get[UserService].update(user.id, cmd.toIN(user.username, user.password))
    LOG.info("User "+request.xuser.currentUsername+" updated details")
    dataCR()
  }

  @SecuredRole("ADMIN")
  @ErrorCRs(Array(
    new ErrorCR(on = classOf[ObjectNotFoundException],  key = "fcall.nofnd", log = "User #{userId} or entry {cmd.key} not found"),
    new ErrorCR(on = classOf[MismatchException],        key = "fcall.enmis", log = "Entry {cmd.key} mismatch"),
    new ErrorCR(on = classOf[ObjectProcessedException], key = "fcall.procd", log = "Entry {cmd.key} already processed")
  ))
  def credit(cmd: CreditVO, request: HttpServletRequest): CallResult = {
    val r = sc.get[CreditService].creditUser(cmd.userId.toLong, cmd)
    LOG.info("Admin "+request.xuser.currentUsername+" credited user #"+cmd.userId+" for "+cmd.amount)
    dataCR()
  }

  @SecuredRole("USER")
  def changePassword(cmd: ChangePasswordVO, request: HttpServletRequest): CallResult = {
    sc.get[UserService].changePassword(request.xuser.current.get.id, cmd.password)
    LOG.info("User "+request.xuser.currentUsername+" changed password")
    dataCR()
  }

  @SecuredRole("ADMIN")
  @ErrorCRs(Array(
    new ErrorCR(on = classOf[UsernameExistsException], key = "x.exusr",     log = "Username {username} already exists"),
    new ErrorCR(on = classOf[ObjectNotFoundException], key = "fcall.nofnd", log = "User #{userId} not found")
  ))
  def editProps(cmd: EditPropsVO, request: HttpServletRequest): CallResult = {
    sc.get[UserService].updateProps(cmd.userId.toLong, cmd)
    LOG.info("User "+request.xuser.currentUsername+" updated user #"+cmd.userId)
    dataCR()
  }

  private val searchAll = (si: SearchInput, sh: SearchHelper) =>
    sc.get[UserService].search(si).serializeAsMap(userVO)

  @SecuredRole("ADMIN")
  def search(cmd: SearchVO, request: HttpServletRequest): CallResult =
    genericSearch(cmd, request,
      TargetedSearch(1, "myUsers", "myUsers",
        orderBy(cmd, SortMapping("usr", "username"), SortMapping("eml", "email")),
        searchAll))
}
