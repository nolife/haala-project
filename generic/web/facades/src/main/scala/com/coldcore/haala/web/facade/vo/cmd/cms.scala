package com.coldcore.haala.web.facade.vo.cmd
package cms

import com.coldcore.haala.web.facade.annotations.Input
import beans.BeanProperty

class LoadDomainVO {
  @BeanProperty @Input (datatype = "owner") var domainId: String = _
}

class DomainGeneralVO {
  @BeanProperty @Input (datatype = "owner") var site: String = _
  @BeanProperty @Input var title: String = _
}

class EditDomainVO {
  @BeanProperty @Input (datatype = "owner") var domainId: String = _
  @BeanProperty @Input var keywords: String = _
  @BeanProperty @Input var insideHead: String = _
  @BeanProperty @Input var beforeBody: String = _
  @BeanProperty @Input var info: Array[DomainGeneralVO] = _
}

class PageTitlelVO {
  @BeanProperty @Input (datatype = "owner") var site: String = _
  @BeanProperty @Input var title: String = _
}

class EditPageLabelsVO {
  @BeanProperty @Input (datatype = "owner") var domainId: String = _
  @BeanProperty @Input (required = true) var index: String = _
  @BeanProperty @Input var keywords: String = _
  @BeanProperty @Input var description: String = _
  @BeanProperty @Input var title: Array[PageTitlelVO] = _
}

class EditFileVO {
  @BeanProperty @Input (datatype = "owner") var site: String = _
  @BeanProperty var path: String = _
  @BeanProperty var params: Array[String] = _
}

class EditTextVO {
  @BeanProperty var key: String = _
  @BeanProperty @Input var values: Array[ValueVO] = _
}

class IncVersionVO {
  @BeanProperty @Input (datatype = "owner") var site: String = _
}

class SwitchOnVO {
  @BeanProperty @Input (datatype = "owner") var domainId: String = _
}

class ValueVO {
  @BeanProperty @Input (datatype = "owner") var site: String = _
  @BeanProperty @Input var value: String = _
}

class AddPageMappingVO {
  @BeanProperty @Input (datatype = "owner") var domainId: String = _
  @BeanProperty @Input (required = true) var pattern: String = _
  @BeanProperty @Input (required = true) var index: String = _
  @BeanProperty @Input (required = true) var pageName: String = _
  @BeanProperty @Input (required = true) var handler: String = _
  @BeanProperty @Input var params: String = _
  @BeanProperty @Input var options: String = _
  @BeanProperty @Input var mod: String = _
  @BeanProperty @Input var downtime: String = _
  @BeanProperty @Input var facade: String = _
}

class EditPageMappingVO extends AddPageMappingVO {
  @BeanProperty var oindex: String = _
}

class RemovePageMappingVO {
  @BeanProperty @Input (datatype = "owner") var domainId: String = _
  @BeanProperty var index: String = _
}

class EditLabelVO {
  @BeanProperty @Input (datatype = "owner") var labelId: String = _
  @BeanProperty @Input (required = true) var key: String = _
  @BeanProperty @Input var value: String = _
  @BeanProperty @Input (required = true, datatype = "owner", transform = "lower") var site: String = _
}

class AddLabelVO {
  @BeanProperty @Input (required = true) var key: String = _
  @BeanProperty @Input var value: String = _
  @BeanProperty @Input (required = true, datatype = "owner", transform = "lower") var site: String = _
}

class RemoveLabelVO {
  @BeanProperty @Input (datatype = "owner") var labelId: String = _
}

class LoadLabelVO {
  @BeanProperty @Input (datatype = "owner") var labelId: String = _
}

class SearchLabelsVO extends SearchVO {
  @BeanProperty @Input (datatype = "owner") var domainId: String = _
}

class EditSettingVO {
  @BeanProperty @Input (datatype = "owner") var settingId: String = _
  @BeanProperty @Input (required = true) var key: String = _
  @BeanProperty @Input var value: String = _
  @BeanProperty @Input (required = true, datatype = "owner", transform = "lower") var site: String = _
}

class AddSettingVO {
  @BeanProperty @Input (required = true) var key: String = _
  @BeanProperty @Input var value: String = _
  @BeanProperty @Input (required = true, datatype = "owner", transform = "lower") var site: String = _
}

class RemoveSettingVO {
  @BeanProperty @Input (datatype = "owner") var settingId: String = _
}

class LoadSettingVO {
  @BeanProperty @Input (datatype = "owner") var settingId: String = _
}

class SearchSettingsVO extends SearchVO {
  @BeanProperty @Input (datatype = "owner") var domainId: String = _
}

class SearchFilesVO extends SearchVO {
  @BeanProperty @Input (datatype = "owner") var domainId: String = _
}

class LoadFileVO {
  @BeanProperty @Input (datatype = "owner") var fileId: String = _
}

class RemoveFileVO {
  @BeanProperty @Input (datatype = "owner") var fileId: String = _
}

class AddFileVO {
  @BeanProperty @Input (required = true) var folder: String = _
  @BeanProperty @Input (required = true, datatype = "owner", transform = "lower") var site: String = _
}

class EditTextFileVO {
  @BeanProperty @Input (datatype = "owner") var site: String = _
  @BeanProperty var path: String = _
  @BeanProperty @Input var value: String = _
}

class BackupFileVO {
  @BeanProperty @Input (datatype = "owner") var site: String = _
  @BeanProperty var path: String = _
  @BeanProperty var sfx: String = _
}

class LoadFilePathVO {
  @BeanProperty @Input (datatype = "owner") var site: String = _
  @BeanProperty var path: String = _
}
