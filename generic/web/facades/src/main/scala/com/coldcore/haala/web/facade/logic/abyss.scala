package com.coldcore.haala
package web
package facade.logic

import facade.annotations.{ErrorCR, LogClass, ValidatorClass}
import facade.validator.Validator
import facade.vo.CallResult
import facade.vo.cmd._
import facade.vo.cmd.abyss.{EditVO, AddVO}

import beans.BeanProperty
import service.exceptions.ObjectNotFoundException
import security.annotations.SecuredRole
import service.AbyssService._
import java.text.SimpleDateFormat

import javax.servlet.http.HttpServletRequest
import domain.AbyssEntry
import service.{AbyssService, SearchInput}
import core.SearchHelper
import com.coldcore.haala.core.{NamedLog, Timestamp}
import com.coldcore.misc.scala.StringOp._
import BaseFacade.{SortMapping, TargetedSearch}

/** Convert input and output data */
trait AbyssConvert {
  self: AbyssFacade =>

  implicit def editX(cmd: EditVO) = ValueMetaIN(cmd.value, cmd.meta)
  implicit def addX(cmd: AddVO) = ValueMetaIN(cmd.value, cmd.meta)

  val abyssEntryVO = (o: AbyssEntry) => {
    val sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm")
    Map('id -> o.id, 'ref -> o.ref, 'pack -> o.pack,
      'keep -> sdf.format(Timestamp(o.keep).toDate),
      'updated -> sdf.format(Timestamp(o.updated).toDate),
      'created -> sdf.format(Timestamp(o.created).toDate),
      'value -> trimCut(o.value, 100).convert("html-nlb"), 'meta -> trimCut(o.meta, 100).convert("html-nlb"))
  }

  val abyssEntryVO_full = (o: AbyssEntry) =>
    abyssEntryVO(o) + ('value -> o.value) + ('meta -> o.meta)
}

class AbyssFacade extends BaseFacade with AbyssConvert {
  @BeanProperty @LogClass lazy val LOG = new NamedLog("Abyss-facade", log)

  @BeanProperty @ValidatorClass var baseValidator: Validator = _

  @SecuredRole("ADMIN")
  @ErrorCR(on = classOf[ObjectNotFoundException], key = "fcall.nofnd", log = "Abyss {parentId} not found")
  def add(cmd: AddVO, request: HttpServletRequest): CallResult = {
    val entry = sc.get[AbyssService].create(cmd.parentId.toLong, cmd) // create from parent entry
    LOG.info("Admin "+request.xuser.currentUsername+" added abyss #"+entry.id)
    dataCR()
  }

  @SecuredRole("ADMIN")
  @ErrorCR(on = classOf[ObjectNotFoundException], key = "fcall.nofnd", log = "Abyss {entryId} not found")
  def edit(cmd: EditVO, request: HttpServletRequest): CallResult = {
    sc.get[AbyssService].update(cmd.entryId.toLong, cmd)
    LOG.info("Admin "+request.xuser.currentUsername+" updated abyss #"+cmd.entryId)
    dataCR()
  }

  @SecuredRole("ADMIN")
  @ErrorCR(on = classOf[ObjectNotFoundException], key = "fcall.nofnd", log = "Abyss {itemId} not found")
  def load(cmd: LoadVO, request: HttpServletRequest): CallResult = {
    val entry = sc.get[AbyssService].getById(cmd.itemId.toLong).getOrElse(throw new ObjectNotFoundException)
    LOG.info("Admin "+request.xuser.currentUsername+" loaded abyss #"+cmd.itemId)
    dataCR(abyssEntryVO_full(entry))
  }

  private val searchAll = (si: SearchInput, sh: SearchHelper) =>
    sc.get[AbyssService].search(new SearchInputX(si)).serializeAsMap(abyssEntryVO)

  private val searchByRef = (si: SearchInput, sh: SearchHelper) =>
    sc.get[AbyssService].search(new SearchInputX(si) { //params: ref (any text), pack (any text)
      override val ref = sh.param(0).option
      override val pack = sh.param(1).option
    }).serializeAsMap(abyssEntryVO)

  @SecuredRole("ADMIN")
  def search(cmd: SearchVO, request: HttpServletRequest): CallResult =
    genericSearch(cmd, request,
      TargetedSearch(1, "myAbyss", "myAbyss", orderBy(cmd), searchAll),
      TargetedSearch(2, "", "", orderBy({ cmd.sortBy = "+id"; cmd }, SortMapping("id")), searchByRef))

}
