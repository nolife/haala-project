package com.coldcore.haala
package web
package facade.logic

import beans.BeanProperty
import service.{MessageService, SearchInput, UserService}
import service.MessageService.{FeedbackIN, MessageEntry, SearchInputX}
import security.annotations.SecuredRole
import facade.vo.CallResult
import facade.vo.cmd._
import facade.vo.cmd.message.FeedbackVO
import facade.logic.BaseFacade.{SortMapping, TargetedSearch}
import facade.annotations.{LogClass, ValidatorClass}
import facade.validator.Validator
import core.{RequestX, SearchHelper}
import com.coldcore.haala.core.{CoreException, NamedLog, Timestamp}
import com.coldcore.misc.scala.StringOp._
import javax.servlet.http.HttpServletRequest

/** Convert input and output data */
trait MessageConvert {
  self: MessageFacade =>

  def feedbackVOtoIN(cmd: FeedbackVO, rx: RequestX) = {
    val sender = if (rx.xuser.currentId > 0) Some(rx.xuser.currentId) else None
    FeedbackIN(cmd.text, sender, cmd.name, cmd.email, rx.xsite.state.currentDomain, rx.xsite.current)
  }

  val messageEntryVO = (rx: RequestX) => (o: MessageEntry) =>
    Map('id -> o.abyss.id, 'ref -> o.abyss.ref, 'read -> o.read,
      'subject -> parametrize(
        rx.xmisc.label("msg.sub"+o.abyss.pack.on("_")+o.subject), // msg.sub1   msg.sub_mypack1
         "domain:"+o.meta("domain", "")),
      'created -> Timestamp(o.abyss.created).toString("dd/MM/yyyy HH:mm"))
}

class MessageFacade extends BaseFacade with MessageConvert {
  @BeanProperty @LogClass lazy val LOG = new NamedLog("message-facade", log)

  @BeanProperty @ValidatorClass var baseValidator: Validator = _

  @SecuredRole("ANONYMOUS")
  def feedback(cmd: FeedbackVO, request: HttpServletRequest): CallResult = {
    val recipient = request.xuser.domainOwner.map(_.id).getOrElse(sc.get[UserService].getSystemUser.id).toLong
    sc.get[MessageService].feedback(recipient, feedbackVOtoIN(cmd, request))
    LOG.info("User "+request.xuser.currentUsername+" submitted feedback for domain "+request.xsite.state.currentDomain)
    dataCR()
  }

  private val searchOwned = (si: SearchInput, sh: SearchHelper) =>
    sc.get[MessageService].search(new SearchInputX(si) {
      override val userId = Some(sh.request.xuser.currentId)
    }).serializeAsMap(messageEntryVO(sh.request))

  private val searchSystem = (si: SearchInput, sh: SearchHelper) =>
    sc.get[MessageService].search(new SearchInputX(si) {
      override val userId = Some(sc.get[UserService].getSystemUser.id)
    }).serializeAsMap(messageEntryVO(sh.request))

  @SecuredRole("USER")
  def search(cmd: SearchVO, request: HttpServletRequest): CallResult =
    genericSearch(cmd, request,
      TargetedSearch(1, "myMessages", "myMessages",
        orderBy(cmd, SortMapping("read"), SortMapping("date", "created"), SortMapping("subject")),
        searchOwned))

  @SecuredRole("ADMIN")
  def asearch(cmd: SearchVO, request: HttpServletRequest): CallResult =
    genericSearch(cmd, request,
      TargetedSearch(2, "myMessages", "sysMessages",
        orderBy(cmd, SortMapping("read"), SortMapping("date", "created"), SortMapping("subject")),
        searchSystem))

  private def genericSetup(cmd: TargetVO, request: HttpServletRequest, userId: Long): CallResult = {
    cmd.target.toInt match {
      case 1 => sc.get[UserService].updateMeta(userId, "msgfwd=y")
      case 2 => sc.get[UserService].updateMeta(userId, "msgfwd=n")
      case _ => throw new CoreException("Unknown target: "+cmd.target.toInt)
    }
    LOG.info("User "+request.xuser.currentUsername+" setup user #"+userId+" using target "+cmd.target)
    dataCR()
  }

  private def genericDelete(cmd: TargetVO, request: HttpServletRequest, userId: Long): CallResult = {
    cmd.target.toInt match {
      case 1 => sc.get[MessageService].clean(userId)
      case _ => throw new CoreException("Unknown target: "+cmd.target.toInt)
    }
    LOG.info("User "+request.xuser.currentUsername+" deleted messages of user #"+userId+" using target "+cmd.target)
    dataCR()
  }

  @SecuredRole("USER")
  def setup(cmd: TargetVO, request: HttpServletRequest): CallResult =
    genericSetup(cmd, request, request.xuser.currentId)

  @SecuredRole("ADMIN")
  def asetup(cmd: TargetVO, request: HttpServletRequest): CallResult =
    genericSetup(cmd, request, sc.get[UserService].getSystemUser.id)

  @SecuredRole("USER")
  def remove(cmd: TargetVO, request: HttpServletRequest): CallResult =
    genericDelete(cmd, request, request.xuser.currentId)

  @SecuredRole("ADMIN")
  def aremove(cmd: TargetVO, request: HttpServletRequest): CallResult =
    genericDelete(cmd, request, sc.get[UserService].getSystemUser.id)
}
