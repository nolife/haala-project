package com.coldcore.haala
package web
package facade.logic

import beans.BeanProperty
import security.annotations.SecuredRole
import javax.servlet.http.HttpServletRequest
import com.coldcore.misc.scala.StringOp._
import com.coldcore.haala.core._
import core.{SearchHelper, WebConstants}
import core.CustomMultipartResolver.TemporaryFile
import domain._
import service._
import service.exceptions.{InvalidFormatException, MismatchException, ObjectExistsException, ObjectNotFoundException}
import service.LabelService.{IN => LabelIN, SearchInputX => LabelSearchInputX}
import service.SettingService.{IN => SettingIN, SearchInputX => SettingSearchInputX}
import service.FileService.{SearchInputX => FileSearchInputX}
import service.CmsService.PageMappingIN
import BaseFacade.{SortMapping, TargetedFilter, TargetedSearch}
import facade.annotations.{ErrorCR, ErrorCRs, LogClass, ValidatorClass}
import facade.validator.Validator
import facade.vo.CallResult
import facade.vo.cmd._
import facade.vo.cmd.cms._

/** Convert input and output data */
trait CmsConvert {
  self: CmsFacade =>

  implicit def addOrEditPageMappingVOtoIN[T <: AddPageMappingVO](cmd: T) =
    PageMappingIN(cmd.index, cmd.pattern, cmd.pageName, cmd.params, cmd.options,
      cmd.mod, cmd.facade, cmd.downtime, cmd.handler)

  implicit def editLabelVOtoIN(cmd: EditLabelVO) = LabelIN(cmd.key, cmd.value, cmd.site)
  implicit def addLabelVOtoIN(cmd: AddLabelVO) = LabelIN(cmd.key, cmd.value, cmd.site)

  implicit def editSettingVOtoIN(cmd: EditSettingVO) = SettingIN(cmd.key, cmd.value, cmd.site)
  implicit def addSettingVOtoIN(cmd: AddSettingVO) = SettingIN(cmd.key, cmd.value, cmd.site)

  /** Convert a label with all its values sharing the same key into VO.
    *  Inputs: label's key and a map with site=value pairs.
    *  k {s1=v1 s2=v2} -> {:key k :values [{:site s1 :value v1} {:site s2 :value v2}]}
    */
  val labelsVO = (k: String, m: Map[String,String]) =>
    Map('key -> k, 'values -> m.map { case (s,v) => Map('site -> s, 'value -> v) })

  /** Convert picture files sharing the same path into VO. */
  val picFilesVO = (p: String, m: Map[String,File]) =>
    Map('path -> p,
        'items -> m.map { case (s,f) =>
          val size = sc.get[PictureService].imageSize(f)
          Map('site -> s, 'size -> Map('width -> size._1, 'height -> size._2))
        })

  /** Convert files sharing the same path into VO. */
  val filesVO = (p: String, m: Map[String,File]) =>
    Map('path -> p,
        'items -> m.map { case (s,f) => Map('site -> s) })

  val domainVO = (o: Domain) =>
    Map('id -> o.id, 'domain -> o.domain)

  val labelVO = (o: Label) =>
    Map('id -> o.id, 'key -> o.key, 'site -> o.site,
        'value -> trimCut(o.value, 200).convert("html-nlb"))

  val settingVO = (o: Setting) =>
    Map('id -> o.id, 'key -> o.key, 'site -> o.site,
        'value -> trimCut(o.value, 200).convert("html-nlb"))

  val fileVO = (o: File) =>
    Map('id -> o.id, 'folder -> o.folder, 'name -> o.name, 'site -> o.site, 'path -> o.getPath, 'meta -> o.meta)

  val pictureVO = (o: File) => {
    val size = sc.get[PictureService].imageSize(o)
    Map('id -> o.id, 'folder -> o.folder, 'name -> o.name, 'site -> o.site,
        'path -> o.getPath, 'size -> Map('width -> size._1, 'height -> size._2))
  }
}

class CmsFacade extends BaseFacade with CmsConvert {
  @BeanProperty @LogClass lazy val LOG = new NamedLog("cms-facade", log)

  @BeanProperty @ValidatorClass var cmsValidator: Validator = _

  /*** Current website on-the-spot edit ("chainMap" contains values of the website) ***/

  @SecuredRole("CMS")
  @ErrorCR(on = classOf[ObjectNotFoundException], key = "fcall.nofnd", log = "Label {itemId} not found")
  def loadText(cmd: LoadVO, request: HttpServletRequest): CallResult = {
    val r = sc.get[CmsService].loadLabel(cmd.itemId, cmd.params.safe.headOption.getOrElse(""), request.xsite.chainMap)
    LOG.info("User "+request.xuser.currentUsername+" loaded label "+cmd.itemId)
    dataCR(labelsVO(cmd.itemId, r))
  }

  @SecuredRole("CMS")
  @ErrorCR(on = classOf[ObjectNotFoundException], key = "fcall.nofnd", log = "Label {key} not found")
  def editText(cmd: EditTextVO, request: HttpServletRequest): CallResult = {
    val sat = (for (x <- cmd.values) yield SiteableText(x.site, Map(Symbol(cmd.key) -> x.value))).toList
    sc.get[CmsService].updateLabel(sat, request.xsite.chainMap)
    LOG.info("User "+request.xuser.currentUsername+" updated label "+cmd.key)
    dataCR()
  }

  @SecuredRole("CMS")
  @ErrorCR(on = classOf[ObjectNotFoundException], key = "fcall.nofnd", log = "Picture {itemId} not found")
  def loadPic(cmd: LoadVO, request: HttpServletRequest): CallResult = {
    val m = sc.get[CmsService].loadFile(VirtualPath(cmd.itemId), request.xsite.chainMap)
    LOG.info("User "+request.xuser.currentUsername+" loaded picture "+cmd.itemId)
    dataCR(picFilesVO(cmd.itemId, m))
  }

  @SecuredRole("CMS")
  @ErrorCRs(Array(
    new ErrorCR(on = classOf[ObjectNotFoundException], key = "fcall.nofnd", log = "Picture {path} not found"),
    new ErrorCR(on = classOf[InvalidFormatException], key = "fcall.uppic", log = "Invalid format uploaded for picture {path}"),
    new ErrorCR(on = classOf[MismatchException], key = "fcall.picsz", log = "Invalid size for picture {path}")
  ))
  def editPic(cmd: EditFileVO, request: HttpServletRequest): CallResult = {
    uploadedBodies(request).headOption.foreach { body =>
      val size = sc.get[PictureService].imageSize(body) //ensure it is a picture and size matches
      if (cmd.params.safe.headOption.getOrElse("") == "same-size")
        sc.get[FileService].getByPath(VirtualPath(cmd.path)).headOption.foreach(x =>
          if (size != sc.get[PictureService].imageSize(x)) throw new MismatchException())

      sc.get[CmsService].updateFile(VirtualPath(cmd.path), cmd.site, body, request.xsite.chainMap)
      LOG.info("User "+request.xuser.currentUsername+" updated picture "+cmd.path)
    }
    dataCR()
  }

  @SecuredRole("CMS")
  @ErrorCR(on = classOf[ObjectNotFoundException], key = "fcall.nofnd", log = "File {itemId} not found")
  def loadFile(cmd: LoadVO, request: HttpServletRequest): CallResult = {
    val m = sc.get[CmsService].loadFile(VirtualPath(cmd.itemId), request.xsite.chainMap)
    LOG.info("User "+request.xuser.currentUsername+" loaded file "+cmd.itemId)
    dataCR(filesVO(cmd.itemId, m))
  }

  @SecuredRole("CMS")
  @ErrorCRs(Array(
    new ErrorCR(on = classOf[ObjectNotFoundException], key = "fcall.nofnd", log = "File {path} not found"),
    new ErrorCR(on = classOf[MismatchException], key = "fcall.frmis", log = "Invalid format for file {path}")
  ))
  def editFile(cmd: EditFileVO, request: HttpServletRequest): CallResult = {
    val tfs = request.xattr.perm.get[List[TemporaryFile]](WebConstants.UPLOADED_TMP_FILES).orNull
    uploadedBodies(request).headOption.foreach { body =>
      if (cmd.params.safe.headOption.getOrElse("") == "same-format")
        sc.get[FileService].getByPath(VirtualPath(cmd.path)).headOption.foreach(x =>
          if (x.name.split("\\.").last != tfs.headOption.get.name.split("\\.").last) throw new MismatchException())

      sc.get[CmsService].updateFile(VirtualPath(cmd.path), cmd.site, body, request.xsite.chainMap)
      LOG.info("User "+request.xuser.currentUsername+" updated file "+cmd.path)
    }
    dataCR()
  }

  /*** Right-side menu operations ***/

  @SecuredRole("CMS")
  def status(cmd: TargetVO, request: HttpServletRequest): CallResult = {
    val username = request.xuser.currentUsername

    cmd.target.toInt match {
      case 1 => request.xcms.editOn
      case 2 => request.xcms.editOff
      case 3 =>
        val pure = request.xcms.state.pureCms
        request.xcms.disable //reset CMS state
        if (pure) request.xuser.logout.foreach(user => LOG.info("User "+username+" logged out")) //logout when 'pure-cms' parameter set
    }

    LOG.info("User "+username+" submitted status #"+cmd.target)
    dataCR()
  }

  @SecuredRole("CMS")
  def incVersion(cmd: IncVersionVO, request: HttpServletRequest): CallResult = {
    sc.get[CmsService].incVerion(cmd.site)
    LOG.info("User "+request.xuser.currentUsername+" increased version of "+cmd.site)
    dataCR()
  }

  @SecuredRole("CMS")
  @ErrorCR(on = classOf[ObjectNotFoundException], key = "fcall.nofnd", log = "Domain {domainId} not found")
  def switchOn(cmd: SwitchOnVO, request: HttpServletRequest): CallResult = {
    val domain = sc.get[DomainService].getById(cmd.domainId.toLong).getOrElse(throw new ObjectNotFoundException)
    val sameDomain = domain.domain == request.xsite.state.currentDomain
    LOG.info("User "+request.xuser.currentUsername+" switched on domain "+domain.domain)
    dataCR(
      if (sameDomain) { request.xcms.enable(); "" }
      else domain.protocols.head+"://"+domain.domain+"/sso.htm?tck="+sc.get[UserService].genTicket(request.xuser.currentId)+"&prm=encms")
  }

  /*** Domain ***/

  @SecuredRole("CMS")
  @ErrorCR(on = classOf[ObjectNotFoundException], key = "fcall.nofnd", log = "Domain {domainId} not found")
  def loadDomain(cmd: LoadDomainVO, request: HttpServletRequest): CallResult = {
    val o = sc.get[DomainService].getById(cmd.domainId.toLong).getOrElse(throw new ObjectNotFoundException)
    LOG.info("User "+request.xuser.currentUsername+" loaded domain #"+cmd.domainId)
    dataCR(domainVO(o))
  }

  @SecuredRole("CMS")
  @ErrorCR(on = classOf[ObjectNotFoundException], key = "fcall.nofnd", log = "Domain {domainId} not found")
  def editDomain(cmd: EditDomainVO, request: HttpServletRequest): CallResult = {
    sc.get[CmsService].updateDomain(cmd.domainId.toLong,
      siteableText(cmd, 'keywords :: 'insideHead :: 'beforeBody :: Nil, request.xsite.current.key),
      listSiteableText(cmd.info))
    LOG.info("User "+request.xuser.currentUsername+" updated domain #"+cmd.domainId)
    dataCR()
  }

  /*** Page mappings ***/

  @SecuredRole("CMS")
  @ErrorCRs(Array(
    new ErrorCR(on = classOf[ObjectNotFoundException], key = "fcall.nofnd", log = "Domain {domainId} or page mapping {oindex} not found"),
    new ErrorCR(on = classOf[ObjectExistsException], key = "fcall.exists", log = "Page mapping {index} already exists")
  ))
  def editPageMapping(cmd: EditPageMappingVO, request: HttpServletRequest): CallResult = {
    sc.get[CmsService].updatePageMapping(cmd.domainId.toLong, cmd.oindex, cmd)
    LOG.info(s"User ${request.xuser.currentUsername} updated page mapping ${cmd.oindex} in domain #${cmd.domainId}")
    dataCR()
  }

  @SecuredRole("CMS")
  @ErrorCRs(Array(
    new ErrorCR(on = classOf[ObjectNotFoundException], key = "fcall.nofnd", log = "Domain {domainId} or page mappings not found"),
    new ErrorCR(on = classOf[ObjectExistsException], key = "fcall.exists", log = "Page mapping {index} already exists")
  ))
  def addPageMapping(cmd: AddPageMappingVO, request: HttpServletRequest): CallResult = {
    sc.get[CmsService].addPageMapping(cmd.domainId.toLong, cmd)
    LOG.info(s"User ${request.xuser.currentUsername} added page mapping ${cmd.index} in domain #${cmd.domainId}")
    dataCR()
  }

  @SecuredRole("CMS")
  @ErrorCR(on = classOf[ObjectNotFoundException], key = "fcall.nofnd", log = "Domain {domainId} or page mappings not found")
  def removePageMapping(cmd: RemovePageMappingVO, request: HttpServletRequest): CallResult = {
    sc.get[CmsService].deletePageMapping(cmd.domainId.toLong, cmd.index)
    LOG.info(s"User ${request.xuser.currentUsername} deleted page mapping ${cmd.index} in domain #${cmd.domainId}")
    dataCR()
  }

  /*** Labels ***/

  val searchLabelsBySite = (siteK: List[String]) => (si: SearchInput, sh: SearchHelper) =>
    labelService.search(new LabelSearchInputX(si) {
      //filter params: filter (FOO or empty)
      override val filterValue = sh.filter(0).toLowerCase
      override val siteKeys = siteK
    }).serializeAsMap(labelVO)

  val searchAllLabels = (si: SearchInput, sh: SearchHelper) =>
    labelService.search(new LabelSearchInputX(si) {
      //filter params: filter (FOO or empty)
      override val filterValue = sh.filter(0).toLowerCase
    }).serializeAsMap(labelVO)

  @SecuredRole("CMS")
  def searchLabels(cmd: SearchLabelsVO, request: HttpServletRequest): CallResult =
    if (cmd.target == "1" && !isUserAdmin) //only for admin (delegated by LabelF)
      errorCR(WebConstants.FACADE_ERROR_SECURITY_VIOLATED)
    else {
      val siteKeys = sc.get[DomainService].getById(cmd.domainId.safeLong) match {
        case Some(o) =>
            val domainSites = sc.get[DomainService].listSites(o) // wis1 wis2
            domainSites ::: (for { d <- domainSites; x <- Constants.SITE_EXTRAS } yield d+x) // wis1, wis2, wis1j, wis2b ...
        case None => List("?") //non existent site if domain not found
      }
      genericSearch(cmd, request,
        TargetedSearch(1, "myLabels", "myLabels", orderBy(cmd, SortMapping("key")), searchAllLabels),
        TargetedSearch(2, "cmsLabels", "cmsLabels", orderBy(cmd, SortMapping("key")), searchLabelsBySite(siteKeys)))
    }

  @SecuredRole("CMS")
  def filterLabels(cmd: FilterVO, request: HttpServletRequest): CallResult = {
    genericSearchFilter(cmd, request,
      TargetedFilter(1, "myLabels"),
      TargetedFilter(2, "cmsLabels"))
    dataCR()
  }

  @SecuredRole("CMS")
  def loadLabel(cmd: LoadLabelVO, request: HttpServletRequest): CallResult =
    label(cmd.labelId.toLong) match {
      case Some(x) =>
        LOG.info("User "+request.xuser.currentUsername+" loaded label #"+cmd.labelId)
        dataCR(Map('id -> x.id, 'key -> x.key, 'site -> x.site, 'value -> x.value))
      case None =>
        LOG.info("Label #"+cmd.labelId+" not found")
        errorCR("fcall.nofnd", request)
    }

  @SecuredRole("CMS")
  @ErrorCR(on = classOf[ObjectNotFoundException], key = "fcall.nofnd", log = "Label {labelId} not found")
  def editLabel(cmd: EditLabelVO, request: HttpServletRequest): CallResult = {
    labelService.update(cmd.labelId.toLong, cmd)
    LOG.info("User "+request.xuser.currentUsername+" updated label #"+cmd.labelId)
    dataCR()
  }

  @SecuredRole("CMS")
  @ErrorCR(on = classOf[ObjectExistsException], key = "fcall.exist", log = "Label {key} already exists")
  def addLabel(cmd: AddLabelVO, request: HttpServletRequest): CallResult = {
    val o = labelService.create(cmd)
    LOG.info("User "+request.xuser.currentUsername+" created label #"+o.id)
    dataCR(Map('id -> o.id, 'key -> o.key, 'site -> o.site, 'value -> o.value))
  }

  @SecuredRole("CMS")
  def removeLabel(cmd: RemoveLabelVO, request: HttpServletRequest): CallResult = {
    labelService.delete(cmd.labelId.toLong)
    LOG.info("User "+request.xuser.currentUsername+" deleted label #"+cmd.labelId)
    dataCR()
  }

  @SecuredRole("CMS")
  @ErrorCR(on = classOf[ObjectNotFoundException], key = "fcall.nofnd", log = "Domain {domainId} not found")
  def editPageLabels(cmd: EditPageLabelsVO, request: HttpServletRequest): CallResult = {
    sc.get[CmsService].updatePageLabels(cmd.domainId.toLong, cmd.index,
      siteableText(cmd, 'keywords :: 'description :: Nil, request.xsite.current.key),
      listSiteableText(cmd.title))
    LOG.info("User "+request.xuser.currentUsername+" updated page labels "+cmd.index+" in domain #"+cmd.domainId)
    dataCR()
  }

  /*** Setting ***/

  val searchSettingsBySite = (siteK: List[String]) => (si: SearchInput, sh: SearchHelper) =>
    settingService.search(new SettingSearchInputX(si) {
      //filter params: key (FOO or empty), value (BAR or empty)
      override val filterKey = sh.filter(0).toLowerCase
      override val filterValue = sh.filter(1).toLowerCase
      override val siteKeys = siteK
    }).serializeAsMap(settingVO)

  private val searchAllSettings = (si: SearchInput, sh: SearchHelper) =>
    settingService.search(new SettingSearchInputX(si) {
      //filter params: key (FOO or empty), value (BAR or empty)
      override val filterKey = sh.filter(0).toLowerCase
      override val filterValue = sh.filter(1).toLowerCase
    }).serializeAsMap(settingVO)

  @SecuredRole("CMS")
  def searchSettings(cmd: SearchSettingsVO, request: HttpServletRequest): CallResult =
    if (cmd.target == "1" && !isUserAdmin) //only for admin (delegated by SettingF)
      errorCR(WebConstants.FACADE_ERROR_SECURITY_VIOLATED)
    else {
      val siteKeys = sc.get[DomainService].getById(cmd.domainId.safeLong) match {
        case Some(o) => sc.get[DomainService].listSites(o)
        case None => List("?") //non existent site if domain not found
      }
      genericSearch(cmd, request,
        TargetedSearch(1, "mySettings", "mySettings", orderBy(cmd, SortMapping("key"), SortMapping("sit", "site")), searchAllSettings),
        TargetedSearch(2, "cmsSettings", "cmsSettings", orderBy(cmd, SortMapping("key"), SortMapping("sit", "site")), searchSettingsBySite(siteKeys)))
    }

  @SecuredRole("CMS")
  def filterSettings(cmd: FilterVO, request: HttpServletRequest): CallResult = {
    genericSearchFilter(cmd, request,
      TargetedFilter(1, "mySettings"),
      TargetedFilter(2, "cmsSettings"))
    dataCR()
  }

  @SecuredRole("CMS")
  def loadSetting(cmd: LoadSettingVO, request: HttpServletRequest): CallResult =
    setting(cmd.settingId.toLong) match {
      case Some(x) =>
        LOG.info("User "+request.xuser.currentUsername+" loaded setting #"+cmd.settingId)
        dataCR(Map('id -> x.id, 'key -> x.key, 'site -> x.site, 'value -> x.value))
      case None =>
        LOG.info("Setting #"+cmd.settingId+" not found")
        errorCR("fcall.nofnd", request)
    }

  @SecuredRole("CMS")
  @ErrorCR(on = classOf[ObjectNotFoundException], key = "fcall.nofnd", log = "Setting #{settingId} not found")
  def editSetting(cmd: EditSettingVO, request: HttpServletRequest): CallResult = {
    settingService.update(cmd.settingId.toLong, cmd)
    LOG.info("User "+request.xuser.currentUsername+" updated setting #"+cmd.settingId)
    dataCR()
  }

  @SecuredRole("CMS")
  @ErrorCR(on = classOf[ObjectExistsException], key = "fcall.exist", log = "Setting {key} already exists")
  def addSetting(cmd: AddSettingVO, request: HttpServletRequest): CallResult = {
    val o = settingService.create(cmd)
    LOG.info("User "+request.xuser.currentUsername+" created setting #"+o.id)
    dataCR(Map('id -> o.id, 'key -> o.key, 'site -> o.site, 'value -> o.value))
  }

  @SecuredRole("CMS")
  def removeSetting(cmd: RemoveSettingVO, request: HttpServletRequest): CallResult = {
    settingService.delete(cmd.settingId.toLong)
    LOG.info("User "+request.xuser.currentUsername+" deleted setting #"+cmd.settingId)
    dataCR()
  }

  /*** File ***/

  val searchFilesBySite = (siteK: List[String]) => (si: SearchInput, sh: SearchHelper) =>
    sc.get[FileService].search(new FileSearchInputX(si) {
      //filter params: value (BAR or empty)
      override val filterValue = sh.filter(0).toLowerCase
      override val siteKeys = siteK
    }).serializeAsMap(fileVO)

  /** Return pictures in the web folder */
  val searchWebPicturesBySite = (siteK: List[String]) => (si: SearchInput, sh: SearchHelper) =>
    sc.get[FileService].search(new FileSearchInputX(si) {
      //filter params: value (BAR or empty)
      override val filterValue = sh.filter(0).toLowerCase
      override val queryAnd = "(of_nam:*.gif OR of_nam:*.png OR of_nam:*.jpg) AND of_fld:web*"
      override val siteKeys = siteK
    }).serializeAsMap(fileVO)

  private val searchAllFiles = (si: SearchInput, sh: SearchHelper) =>
    sc.get[FileService].search(new FileSearchInputX(si) {
      //filter params: value (FOO or empty)
      override val filterValue = sh.filter(0).toLowerCase
    }).serializeAsMap(fileVO)

  @SecuredRole("CMS")
  def searchFiles(cmd: SearchFilesVO, request: HttpServletRequest): CallResult =
    if (cmd.target == "1" && !isUserAdmin) //only for admin
      errorCR(WebConstants.FACADE_ERROR_SECURITY_VIOLATED)
    else {
      val siteKeys = sc.get[DomainService].getById(cmd.domainId.safeLong) match {
        case Some(o) => sc.get[DomainService].listSites(o)
        case None => List("?") //non existent site if domain not found
      }
      genericSearch(cmd, request,
        TargetedSearch(1, "myFiles", "myFiles", orderBy(cmd, SortMapping("fld")), searchAllFiles),
        TargetedSearch(2, "cmsFiles", "cmsFiles", orderBy(cmd, SortMapping("fld")), searchFilesBySite(siteKeys)),
        TargetedSearch(3, "cmsPictures", "cmsPictures", orderBy(cmd, SortMapping("fld")), searchWebPicturesBySite(siteKeys)))
    }

  @SecuredRole("CMS")
  def filterFiles(cmd: FilterVO, request: HttpServletRequest): CallResult = {
    genericSearchFilter(cmd, request,
      TargetedFilter(1, "myFiles"),
      TargetedFilter(2, "cmsFiles"))
    dataCR()
  }

  @SecuredRole("CMS")
  @ErrorCR(on = classOf[ObjectNotFoundException], key = "fcall.nofnd", log = "File #{fileId} not found")
  def loadFile2(cmd: LoadFileVO, request: HttpServletRequest): CallResult =
    sc.get[FileService].getById(cmd.fileId.toLong).map { o =>
      LOG.info("User "+request.xuser.currentUsername+" loaded file #"+cmd.fileId)
      dataCR(fileVO(o))
    }.getOrElse(throw new ObjectNotFoundException)

  @SecuredRole("CMS")
  @ErrorCR(on = classOf[ObjectNotFoundException], key = "fcall.nofnd", log = "File {path} not found")
  def loadFilePath(cmd: LoadFilePathVO, request: HttpServletRequest): CallResult =
    sc.get[FileService].getByPath(VirtualPath(cmd.path), SiteChain(cmd.site)).map { o =>
      LOG.info("User "+request.xuser.currentUsername+" loaded file "+cmd.path)
      dataCR(fileVO(o))
    }.getOrElse(throw new ObjectNotFoundException)

  @SecuredRole("CMS")
  @ErrorCR(on = classOf[InvalidFormatException], key = "fcall.fruns", log = "Invalid picture format #{fileId}")
  def loadPicture(cmd: LoadFileVO, request: HttpServletRequest): CallResult =
    sc.get[FileService].getById(cmd.fileId.toLong) match {
      case Some(x) =>
        LOG.info("User "+request.xuser.currentUsername+" loaded file #"+cmd.fileId)
        dataCR(pictureVO(x))
      case None =>
        LOG.info("File #"+cmd.fileId+" not found")
        errorCR("fcall.nofnd", request)
    }

  @SecuredRole("CMS")
  def removeFile(cmd: RemoveFileVO, request: HttpServletRequest): CallResult = {
    sc.get[FileService].delete(cmd.fileId.toLong)
    LOG.info("User "+request.xuser.currentUsername+" deleted file #"+cmd.fileId)
    dataCR()
  }

  @SecuredRole("CMS")
  def addFile(cmd: AddFileVO, request: HttpServletRequest): CallResult = {
    val tfs = request.xattr.perm.get[List[TemporaryFile]](WebConstants.UPLOADED_TMP_FILES).orNull
    uploadedBodies(request).headOption match {
      case Some(body) =>
        val path = cmd.folder+"/"+tfs.headOption.get.name
        try {
          val o = sc.get[FileService].create(VirtualPath(path), cmd.site, body)
          LOG.info("User "+request.xuser.currentUsername+" created file "+path)
          dataCR(fileVO(o))
        } catch {
          case e: ObjectExistsException =>
            LOG.info(s"File $path already exists")
            errorCR("fcall.exist", request)
        }
      case None =>
        errorCR(WebConstants.FACADE_ERROR_NO_UPLOAD)
    }
  }

  @SecuredRole("CMS")
  @ErrorCR(on = classOf[ObjectNotFoundException], key = "fcall.nofnd", log = "File {path} not found")
  def editFile2(cmd: EditFileVO, request: HttpServletRequest): CallResult = {
    uploadedBodies(request).headOption match {
      case Some(body) =>
        sc.get[CmsService].updateFile(VirtualPath(cmd.path), cmd.site, body)
        LOG.info("User "+request.xuser.currentUsername+" updated file "+cmd.path)
      case None =>
    }
    dataCR()
  }

  @SecuredRole("CMS")
  @ErrorCR(on = classOf[ObjectNotFoundException], key = "fcall.nofnd", log = "File {path} not found")
  def editTextFile(cmd: EditTextFileVO, request: HttpServletRequest): CallResult = {
    val body = sc.get[FileService].body
    body.write(cmd.value.bytesUTF8)
    sc.get[CmsService].updateFile(VirtualPath(cmd.path), cmd.site, body, cd = true)
    LOG.info("User "+request.xuser.currentUsername+" updated text file "+cmd.path)
    dataCR()
  }

  @SecuredRole("CMS")
  @ErrorCRs(Array(
    new ErrorCR(on = classOf[ObjectNotFoundException], key = "fcall.nofnd", log = "File #{fileId} not found"),
    new ErrorCR(on = classOf[InvalidFormatException], key = "fcall.fruns", log = "Invalid text file format #{fileId}")
  ))
  def loadTextFile(cmd: LoadFileVO, request: HttpServletRequest): CallResult = {
    val (o, text) = sc.get[CmsService].loadTextFile(cmd.fileId.toLong)
    LOG.info("User "+request.xuser.currentUsername+" loaded text file #"+cmd.fileId)
    dataCR(fileVO(o)+('value -> text))
  }

  @SecuredRole("CMS")
  @ErrorCR(on = classOf[ObjectNotFoundException], key = "fcall.nofnd", log = "File {path} not found")
  def backupFile(cmd: BackupFileVO, request: HttpServletRequest): CallResult =
    sc.get[FileService].getByPath(VirtualPath(cmd.path), SiteChain(cmd.site)).map { o =>
      (1 to 3).map("up"+_).find(cmd.sfx==).orElse(throw new CoreException(s"Illegal backup suffix ${cmd.sfx}"))
      val b = sc.get[FileService].copyBody(o)
      sc.get[FileService].attachBody(o.id, cmd.sfx, b)
      LOG.info("User "+request.xuser.currentUsername+" backed file "+cmd.path+s" as ${cmd.sfx}")
      dataCR()
    }.getOrElse(throw new ObjectNotFoundException)

}
