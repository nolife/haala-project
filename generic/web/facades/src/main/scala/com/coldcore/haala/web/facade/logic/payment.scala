package com.coldcore.haala
package web
package facade.logic

import beans.BeanProperty
import facade.annotations.{ErrorCR, ErrorCRs, LogClass, ValidatorClass}
import facade.validator.Validator
import facade.vo.CallResult
import facade.vo.cmd._
import facade.vo.cmd.payment._
import service.exceptions.{InvalidStatusException, NotSupportedException, ObjectNotFoundException, ObjectProcessedException}
import service.PaymentService._
import service.{PaymentService, SearchInput, UserService}
import core.SearchHelper
import com.coldcore.haala.core.{NamedLog, Timestamp}
import com.coldcore.misc.scala.StringOp._
import BaseFacade.{SortMapping, TargetedFilter, TargetedSearch}
import security.annotations.SecuredRole
import javax.servlet.http.HttpServletRequest

/** Convert input and output data */
trait PaymentConvert {
  self: PaymentFacade =>

  implicit def createPaymentVOtoIN(cmd: CreatePaymentVO) =
    CreatePaymentIN("", cmd.ref, cmd.message.safe, "", PaymentIN(toX100(cmd.amount), cmd.currency))

  val paymentEntryVO = (o: PaymentEntry) =>
    Map('id -> o.abyss.id, 'ref -> o.abyss.ref, 'pack -> o.abyss.pack, 'username -> o.username, 'url -> o.url,
        'currency -> o.currency, 'amount -> toX100(o.amount), 'active -> o.active,
        'expire -> Timestamp(o.expire).toString("dd/MM/yyyy HH:mm"),
        'updated -> Timestamp(o.abyss.updated).toString("dd/MM/yyyy HH:mm"),
        'created -> Timestamp(o.abyss.created).toString("dd/MM/yyyy HH:mm"))
}

class PaymentFacade extends BaseFacade with PaymentConvert {
  @BeanProperty @LogClass lazy val LOG = new NamedLog("payment-facade", log)

  @BeanProperty @ValidatorClass var baseValidator: Validator = _

  @SecuredRole("ADMIN")
  @ErrorCR(on = classOf[ObjectNotFoundException], key = "fcall.nofnd", log = "User {username} not found")
  def create(cmd: CreatePaymentVO, request: HttpServletRequest): CallResult = {
    val user = sc.get[UserService].getByUsername(cmd.username).getOrElse(throw new ObjectNotFoundException)
    val entry = sc.get[PaymentService].create(user.id, cmd)
    LOG.info("Admin "+request.xuser.currentUsername+" created payment "+entry.ref)
    dataCR(paymentEntryVO(entry))
  }

  @SecuredRole("ADMIN")
  @ErrorCRs(Array(
    new ErrorCR(on = classOf[ObjectNotFoundException],  key = "fcall.nofnd", log = "Payment {itemId} not found"),
    new ErrorCR(on = classOf[InvalidStatusException],   key = "fcall.insta", log = "Payment {itemId} not pushed"),
    new ErrorCR(on = classOf[NotSupportedException],    key = "fcall.nosup", log = "Payment {itemId} not pushed"),
    new ErrorCR(on = classOf[ObjectProcessedException], key = "fcall.procd", log = "Payment {itemId} already processed")
  ))
  def push(cmd: LoadVO, request: HttpServletRequest): CallResult = {
    sc.get[PaymentService].push(cmd.itemId.toLong)
    LOG.info("Admin "+request.xuser.currentUsername+" pushed payment #"+cmd.itemId)
    dataCR()
  }

  private val searchAll = (si: SearchInput, sh: SearchHelper) =>
    sc.get[PaymentService].search(new SearchInputX(si) {
      //filter params: ref (FOO or empty), pack (BAR or empty), username (FOO or empty)
      override val ref = sh.filter(0).toUpperCase.option
      override val pack = sh.filter(1).toLowerCase.option
      override val username = sh.filter(2).toLowerCase.option
    }).serializeAsMap(paymentEntryVO)

  @SecuredRole("ADMIN")
  def search(cmd: SearchVO, request: HttpServletRequest): CallResult =
    genericSearch(cmd, request,
      TargetedSearch(1, "myPayments", "myPayments",
        orderBy(cmd, SortMapping("ref"), SortMapping("pac", "pack"), SortMapping("usr", "username")),
        searchAll))

  @SecuredRole("ADMIN")
  def filter(cmd: FilterVO, request: HttpServletRequest): CallResult = {
    genericSearchFilter(cmd, request, TargetedFilter(1, "myPayments"))
    dataCR()
  }

}
