package com.coldcore.haala
package web
package facade.logic

import web.facade.vo.cmd._
import web.facade.vo.CallResult
import com.coldcore.haala.core.CoreException
import com.coldcore.misc.scala.ReflectUtil._
import web.facade.annotations.Input
import web.core.{CoreActions, SearchCommand, SearchHelper}
import javax.servlet.http.HttpServletRequest

import com.coldcore.haala.service._
import core.Dependencies

object BaseFacade {
  /** Generic search argument
    * target    - unique search ID
    * ppKey     - per page setting key
    * saveKey   - SearchBeansMap key in session ("" - do not save and do not apply a filter)
    * orderBy   - sort applicable to database or search engine
    * search    - search function
    */
  case class TargetedSearch(target: Int, ppKey: String, saveKey: String, orderBy: String,
                            search: (SearchInput, SearchHelper) => Any)

  /** Generic filter argument
    * target    - unique search ID  
    * saveKey   - SearchBeansMap key in session
    */
  case class TargetedFilter(target: Int, saveKey: String)

  /** Mapping between user input and actual database fields to order by (sit -> site, key -> key, typ -> type). */
  case class SortMapping(field: String, dbField: String, ascending: Boolean = true)
  object SortMapping {
    def apply(field: String) = new SortMapping(field, field)
  }
}

class BaseFacade extends CoreActions with Dependencies with SettingsReader with LabelsReader with UserRoleCheck {
  import BaseFacade._

  implicit class StringX(s: String) {
    def isAscending = s.safe == "1" || s.safe == "asc"
    def toDbOrder = new StringX(s).isAscending ? "asc" | "desc"
  }

  implicit class BooleanX(b: Boolean) {
    def toDbOrder = b ? "asc" | "desc"
  }

  /** Return call result with an error. */
  def errorCR(key: String, request: HttpServletRequest): CallResult = new CallResult().pushError(request.xmisc.label(key))

  /** Return call result with an error. */
  def errorCR(code: Int): CallResult = new CallResult().pushError(code)

  /** Return call result with data. */
  def dataCR(data: Any = null): CallResult = new CallResult(data)

  /** Replace command sort fields with proper DB columns to order by.
    * Adding "+" OR "-" on fields converts to ASC or DESC for columns (in this case cmd.sortType is ignored)
    *   +sit -> site asc   sit+ -> site asc   -sit -> site desc
    *   +sit -key -> site asc, key desc   +sit, key- -> site asc, key desc
    */
  def orderBy(cmd: SearchVO, sortby: SortMapping*): String = {
    val strip = (x: String) => (for (c <- x; if c != '+' && c != '-') yield c).mkString
    val isAsc = (x: String) => !x.matches("(-.+)|(.+-)") && (x.matches("(\\+.+)|(.+\\+)") || cmd.sortType.isAscending)
    val xs = for (a <- cmd.sortBy.safe.parseCSV(", "); b <- sortby; if strip(a) == b.field) yield SortMapping(b.field, b.dbField, isAsc(a))
    val dsort = SortMapping("id", "id", ascending = false) // default sort by ID desc
    (if (xs.exists(_.field == "id")) xs else xs :+ dsort).map(x => x.dbField+" "+x.ascending.toDbOrder).mkString(", ")
  }

  /** Call a proper search function. */
  def genericSearch(cmd: SearchVO, request: HttpServletRequest, ts: TargetedSearch*): CallResult = {
    import cmd._
    val t = ts.find(_.target == target.toInt).getOrElse(throw new CoreException("Unknown target: "+target)) // find by unique search ID
    val (site, sbeans, prms) = (request.xsite.current, request.xmisc.searchBeansMap, params.safe.toList)
    val (maxItems, cmdItems) = (settingService.getItemsPerPage(t.ppKey, site), max.safeInt)
    val filter = sbeans.getAs[SearchCommand](t.saveKey).map(_.filter).getOrElse(Nil)
    val searchCmd = SearchCommand(from.safeLong, max.safeInt, sortBy.safe, sortType.safe, target.toInt, prms, filter)
    val si = SearchInput(from.safeLong, (cmdItems > 0 && cmdItems <= maxItems) ? cmdItems | maxItems, t.orderBy)
    val sh = SearchHelper(refresh == "1", t.saveKey, site, request, target.toInt, searchCmd)

    if (t.saveKey.nonEmpty) sbeans += (t.saveKey -> searchCmd)
    dataCR(t.search(si, sh))
  }

  /** Save parameters supplied in a command to apply during the next and consequent search requests (command must be saved beforehand) */
  def genericSearchFilter(cmd: FilterVO, request: HttpServletRequest, ts: TargetedFilter*) {
    import cmd._
    val t = ts.find(_.target == target.toInt).getOrElse(throw new CoreException("Unknown target: "+target)) // find by unique search ID
    request.xmisc.searchBeansMap.getAs[SearchCommand](t.saveKey).foreach(_.filter = params.safe.toList )
  }

  /** Construct a SiteableText from a supplied object (VO).
    * VO must have a "site" field containing a site key and the rest of the fields considered as text mappings.
    * Text mappings could be filtered via "keys" argument.
    * In case if "site" filed is missing in VO then "dsite" argument will be used or it will revert to the
    * first configured site key (eg. xx1).
    */
  def siteableText(vo: AnyRef, keys: Seq[Symbol] = Seq.empty, dsite: String = ""): SiteableText = {
    val text =
      (for (f <- fieldsByAnnotation[Input](vo.getClass)
              if f.getName != "site" && (keys.isEmpty || keys.contains(Symbol(f.getName)))) yield
        Symbol(f.getName) -> fieldValue(vo, f).asInstanceOf[String]).toMap

    val site = fieldByName(vo.getClass, "site") match {
      case Some(f) => fieldValue(vo, f).asInstanceOf[String].or(dsite)
      case None if dsite.nonEmpty => dsite
      case None => parsedSites.head.key
    }
    SiteableText(site, text)
  }

  def listSiteableText(vos: Seq[AnyRef]): List[SiteableText] = vos.map(vo => siteableText(vo)).toList
}
