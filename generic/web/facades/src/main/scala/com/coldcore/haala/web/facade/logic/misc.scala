package com.coldcore.haala
package web
package facade.logic

import web.facade.annotations.{ValidatorClass, IgnoreDowntime}
import web.facade.validator.Validator
import web.facade.vo.CallResult
import web.facade.vo.cmd._
import security.annotations.SecuredRole
import beans.BeanProperty
import web.core._
import javax.servlet.http.HttpServletRequest
import com.coldcore.haala.core.{NamedLog, SiteChain}

object MiscFacade {
  trait FavsMod {
    def searchFavs(cmd: SearchVO, request: HttpServletRequest): CallResult
  }
}

class MiscFacade extends BaseFacade {
  import MiscFacade._

  private lazy val LOG = new NamedLog("[misc-facade]", log)

  @BeanProperty @ValidatorClass var baseValidator: Validator = _

  var favsMods = List.empty[FavsMod]
  def setFavsMods(x: JList[FavsMod]) = favsMods = x.toList

  @IgnoreDowntime
  @SecuredRole("ANONYMOUS")
  def changeSite(siteK: String, request: HttpServletRequest): CallResult = {
    request.xsite.chainMap.assertRootChainByKey(siteK)
    val site = request.xsite.chainMap.chainByKey(siteK)
    if (site.nonEmpty) {
      request.xsite.setCurrent(site)
      LOG.info("User "+request.xuser.currentUsername+" changed site to "+site)
    }
    dataCR()
  }

  @IgnoreDowntime
  @SecuredRole("ANONYMOUS")
  def changeCurrency(currency: String, request: HttpServletRequest): CallResult = {
    val clist = setting("Currencies")
    val ocur = currency.safe.toUpperCase
    val ncur = if (clist.parseCSV.contains(ocur)) ocur else defaultCurrency
    request.xuser.prefs.currency = ncur
    LOG.info("User "+request.xuser.currentUsername+" changed currency to "+ncur)
    dataCR()
  }

  @IgnoreDowntime
  @SecuredRole("ANONYMOUS")
  def uploadStatus(noprm: String, request: HttpServletRequest): CallResult =
    request.xattr.perm.get[UploadBean](WebConstants.UPLOAD_BEAN) match {
      case Some(ubean) =>
        if (ubean.error != 0) {
          request.xattr.perm - WebConstants.UPLOAD_BEAN
          errorCR(ubean.error)
        } else {
          val (r, t) = (ubean.read, ubean.total)
          if (r == t) request.xattr.perm - WebConstants.UPLOAD_BEAN
          dataCR(ubean.serializeAsMap +
            ('perc -> (if (r == t) 100 else if (t <= 0) 0 else math.round(r.toDouble*100d/t.toDouble))))
        }
      case None => errorCR(WebConstants.FACADE_ERROR_NO_UPLOAD)
    }

  @SecuredRole("ANONYMOUS")
  def params(cmd: ParamsVO, request: HttpServletRequest): CallResult = {
    request.xattr.perm + (WebConstants.PARAMS_BEAN -> cmd)
    dataCR()
  }

  @SecuredRole("ANONYMOUS")
  def favs(cmd: SearchVO, request: HttpServletRequest): CallResult =
    favsMods.map(x => x.searchFavs(cmd, request)).find(null!=).getOrElse(dataCR())

  @SecuredRole("ANONYMOUS")
  def turingPath(noprm: String, request: HttpServletRequest): CallResult = {
    if (!request.xturing.isValid(true)) request.xturing.generate
    dataCR(request.xturing.get.path)
  }
}
