package com.coldcore.haala
package web
package facade.logic

import web.facade.annotations.ValidatorClass
import web.facade.validator.Validator

import beans.BeanProperty
import web.facade.vo.CallResult
import web.facade.vo.cmd._
import web.facade.vo.cmd.cms._
import security.annotations.SecuredRole
import javax.servlet.http.HttpServletRequest

import com.coldcore.haala.core.NamedLog

class SettingFacade extends BaseFacade {
  private lazy val LOG = new NamedLog("setting-facade", log)

  @BeanProperty @ValidatorClass var cmsValidator: Validator = _
  @BeanProperty var cmsFacade: CmsFacade = _

  @SecuredRole("ADMIN")
  def load(cmd: LoadSettingVO, request: HttpServletRequest): CallResult =
    cmsFacade.loadSetting(cmd, request)

  @SecuredRole("ADMIN")
  def edit(cmd: EditSettingVO, request: HttpServletRequest): CallResult =
    cmsFacade.editSetting(cmd, request)

  @SecuredRole("ADMIN")
  def add(cmd: AddSettingVO, request: HttpServletRequest): CallResult =
    cmsFacade.addSetting(cmd, request)

  @SecuredRole("ADMIN")
  def remove(cmd: RemoveSettingVO, request: HttpServletRequest): CallResult =
    cmsFacade.removeSetting(cmd, request)
  @SecuredRole("ADMIN")
  def search(cmd: SearchSettingsVO, request: HttpServletRequest): CallResult =
    cmsFacade.searchSettings(cmd, request)

  @SecuredRole("ADMIN")
  def filter(cmd: FilterVO, request: HttpServletRequest): CallResult =
    cmsFacade.filterSettings(cmd, request)
}
