package com.coldcore.haala.web.facade.vo.cmd
package abyss

import beans.BeanProperty
import com.coldcore.haala.web.facade.annotations.Input

class EditVO {
  @BeanProperty var entryId: String = _
  @BeanProperty @Input var meta: String = _
  @BeanProperty @Input var value: String = _
}

class AddVO {
  @BeanProperty var parentId: String = _
  @BeanProperty @Input var meta: String = _
  @BeanProperty @Input var value: String = _
}
