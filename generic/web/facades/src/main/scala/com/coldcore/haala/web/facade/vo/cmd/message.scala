package com.coldcore.haala.web.facade.vo.cmd
package message

import com.coldcore.haala.web.facade.annotations.Input
import beans.BeanProperty

class FeedbackVO {
  @BeanProperty @Input (required = true) var name: String = _
  @BeanProperty @Input (required = true, datatype = "email", transform = "lower") var email: String = _
  @BeanProperty @Input (required = true) var text: String = _
  @BeanProperty @Input (required = true, datatype = "turing", transform = "lower") var turing: String = _
}

class SupportEmailVO {
  @BeanProperty @Input (required = true, datatype = "email", transform = "lower") var email: String = _
  @BeanProperty @Input (required = true) var text: String = _
}

class SupportUserVO {
  @BeanProperty var userId: String = _
  @BeanProperty @Input (required = true) var text: String = _
}

