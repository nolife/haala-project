package com.coldcore.haala
package web
package facade.logic

import com.coldcore.haala.web.facade.annotations._
import web.facade.validator.Validator

import beans.BeanProperty
import web.facade.vo.CallResult
import web.facade.vo.cmd.user._
import service.exceptions._
import web.core.WebConstants._
import security.annotations.SecuredRole
import javax.servlet.http.HttpServletRequest

import service._
import facade.vo.cmd.TokenVO
import com.coldcore.haala.domain.Role
import com.coldcore.haala.core.NamedLog

import concurrent.ExecutionContext.Implicits.global
import concurrent.Future

/** Convert input and output data */
trait LoginConvert {
  self: LoginFacade =>

  implicit def login2login(cmd: Login2VO): LoginVO = {
    val ncmd = new LoginVO()
    ncmd.username = cmd.value
    ncmd.password = cmd.password
    ncmd.turing = cmd.turing
    ncmd
  }
}

class LoginFacade extends BaseFacade with LoginConvert {
  @BeanProperty @LogClass lazy val LOG = new NamedLog("login-facade", log)

  @BeanProperty @ValidatorClass var userValidator: Validator = _
  @BeanProperty var userFacade: UserFacade = _

  @IgnoreDowntime
  @SecuredRole("ANONYMOUS")
  @ErrorCRs(Array(
    new ErrorCR(on = classOf[ObjectNotFoundException],   key = "x.inval", log = "User {username} not found"),
    new ErrorCR(on = classOf[UserNotActivatedException], key = "x.nactv", log = "User {username} not active"),
    new ErrorCR(on = classOf[UserDisabledException],     key = "x.disab", log = "User {username} is disabled"),
    new ErrorCR(on = classOf[UserNoAgreementException], result = "{e.ref}", code = FACADE_ERROR_USER_AGREEMENT, log = "User {username} not accepted agreement")
  ))
  def login(cmd: LoginVO, request: HttpServletRequest): CallResult = {
    val domain = sc.get[DomainService].getById(request.xsite.state.currentDomainId).get
    sc.get[UserService].login(cmd.username, cmd.password, domain) match {
      case Some(user) =>
        val url = request.xattr.perm[String](LAST_SECURED_URL, "") or
                  sc.get[SettingService].query("LandingPage", request.xsite.current)
        request.xinit.reset

        if (request.xuser.isLoginForbidden(user)) {
          //forbidden to login to this domain
          LOG.info("User " + cmd.username + " kicked (forbidden)")
          errorCR("x.forbd", request)
        } else {
          //login successful
          request.xuser.login(user)
          LOG.info("User " + cmd.username + " login successful (" + request.getRemoteHost + ")")
          dataCR(url)
        }
      case None =>
        LOG.info("User " + cmd.username + " login failed (" + request.getRemoteHost + ")")
        errorCR("x.inval", request)
    }
  }

  @IgnoreDowntime
  @SecuredRole("ANONYMOUS")
  @ErrorCRs(Array(
    new ErrorCR(on = classOf[ObjectNotFoundException],   key = "x.inval", log = "User {value} not found"),
    new ErrorCR(on = classOf[UserNotActivatedException], key = "x.nactv", log = "User {value} not active"),
    new ErrorCR(on = classOf[UserDisabledException],     key = "x.disab", log = "User {value} is disabled"),
    new ErrorCR(on = classOf[UserNoAgreementException], result = "{e.ref}", code = FACADE_ERROR_USER_AGREEMENT, log = "User {value} not accepted agreement")
  ))
  def login2(cmd: Login2VO, request: HttpServletRequest): CallResult =
    login(cmd, request)

  @IgnoreDowntime
  @SecuredRole("ANONYMOUS")
  def logout(noprm: String, request: HttpServletRequest): CallResult =
    request.xuser.logout match {
      case Some(user) =>
        LOG.info("User "+user.username+" logged out")
        dataCR()
      case None => dataCR()
    }

  @SecuredRole("ADMIN")
  def shellOn(cmd: ShellVO, request: HttpServletRequest): CallResult =
    sc.get[UserService].getById(cmd.userId.toLong) match {
      case Some(user) =>
        LOG.info("Admin "+request.xuser.currentUsername+" put on "+user.username+" shell")
        request.xuser.shellOn(user)
        securityService.pushPrincipal(user, Role.ADMIN, Role.USER)
        dataCR()
      case None =>
        LOG.info("User #"+cmd.userId+" not found")
        errorCR("fcall.nofnd", request)
    }

  @SecuredRole("ADMIN")
  def shellOff(noprm: String, request: HttpServletRequest): CallResult =
    request.xuser.`super` match {
      case Some(user) =>
        val exUserId = request.xuser.currentId
        LOG.info("Admin "+user.username+" took off "+request.xuser.currentUsername+" shell")
        request.xuser.shellOff
        securityService.pushPrincipal(user, Role.USER)
        dataCR(exUserId)
      case None => dataCR()
    }

  @SecuredRole("ANONYMOUS")
  def remindPassword(cmd: ReminderVO, request: HttpServletRequest): CallResult =
    userFacade.remindPassword(cmd, request)

  @SecuredRole("ANONYMOUS")
  @ErrorCRs(Array(
    new ErrorCR(on = classOf[ExternalFailException], key = "fcall.extfl", log = "External resource failed"),
    new ErrorCR(on = classOf[UserDisabledException], key = "x.disab",     log = "User ref {ref} is disabled"),
    new ErrorCR(on = classOf[UserNoAgreementException], result = "{e.ref}", code = FACADE_ERROR_USER_AGREEMENT, log = "User ref {ref} not accepted agreement")
  ))
  def fbLogin(cmd: TokenVO, request: HttpServletRequest): CallResult = {
    val fb = sc.get[FacebookService]
    val fbUser = fb.getUser(cmd.token).getOrElse(throw new ExternalFailException)
    val domain = sc.get[DomainService].getById(request.xsite.state.currentDomainId).get
    val user =
      (try { fb.login(fbUser, domain) } catch {
        case e: ObjectNotFoundException => //register and login
          fb.register(FacebookService.RegisterIN(fbUser, fb.generateUsername(fbUser), ""))

          Future { //may fail if email server is down, user does not need to know about this at this point
            try {
              sc.get[EmailService].remindPassword(fbUser.email, request.xsite.current)
            } catch {
              case _: EmailServerDownException =>
              case _: InvalidEmailException =>
              case e: Throwable => LOG.error("Error (thread)", e)
            }
          }

          fb.login(fbUser, domain)
      }).get

    val url = request.xattr.perm[String](LAST_SECURED_URL, "") or
              sc.get[SettingService].query("LandingPage", request.xsite.current)
    request.xinit.reset

    if (request.xuser.isLoginForbidden(user)) {
      //forbidden to login to this domain
      LOG.info("User "+user.username+" kicked (forbidden)")
      errorCR("x.forbd", request)
    } else {
      //login successful
      request.xuser.login(user)
      LOG.info("User "+user.username+" login successful via FB ("+request.getRemoteHost+")")
      dataCR(url)
    }
  }

}
