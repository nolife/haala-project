package com.coldcore.haala
package web
package facade.logic

import facade.annotations.ValidatorClass
import facade.validator.Validator
import facade.vo.CallResult
import facade.vo.cmd.LoadVO
import security.annotations.SecuredRole

import beans.BeanProperty
import javax.servlet.http.HttpServletRequest
import com.coldcore.haala.core.{Constants, Meta, NamedLog}
import service.SettingService

/** Convert input and output data */
object NodeFacade {

  val sendEmailVO = (config: Meta) =>
    Map('from -> config("emailFrom", "?"), 'to -> config("emailTo", "?"))

}

class NodeFacade extends BaseFacade {
  import NodeFacade._

  private lazy val LOG = new NamedLog("[node-facade]", log)

  @BeanProperty @ValidatorClass var baseValidator: Validator = _

  @SecuredRole("ADMIN")
  def sendEmail(cmd: LoadVO, request: HttpServletRequest): CallResult = {
    val config = Meta(request.sc.get[SettingService].querySystem("NodeTestTask"), ";,")
    sc.get[SettingService].write("NodeTest/"+cmd.itemId, "email=y", Constants.SITE_SYSTEM)
    LOG.info("Admin "+request.xuser.currentUsername+" requested test email for node "+cmd.itemId)
    dataCR(sendEmailVO(config))
  }

}
