package com.coldcore.haala
package web
package facade.logic

import web.facade.annotations.{ErrorCR, LogClass, ValidatorClass}
import web.facade.validator.Validator

import beans.BeanProperty
import web.facade.vo.CallResult
import web.facade.vo.cmd._
import security.annotations.SecuredRole
import domain.{Location, Postcode}
import javax.servlet.http.HttpServletRequest

import com.coldcore.haala.core.NamedLog
import service.LocationService
import service.exceptions.ExternalFailException

/** Convert input and output data */
trait LocationConvert {
  self: LocationFacade =>

  def locationVO(o: Location, txt: String): Map[Symbol,Any] =
    Map('id -> o.id, 'type -> o.`type`, 'empty -> o.empty, 'txt -> txt.convert("html"))

  def postcodeVO(o: Postcode, txt: String): Map[Symbol,Any] =
    Map('id -> o.id, 'type -> o.`type`, 'value -> o.value, 'txt -> txt.convert("html"))
}

class LocationFacade extends BaseFacade with LocationConvert {
  @BeanProperty @LogClass lazy val LOG = new NamedLog("location-facade", log)

  @BeanProperty @ValidatorClass var baseValidator: Validator = _

  private def skipEmptyLocations(lst: List[Location]): List[Location] = lst match {
    case x :: Nil if x.empty.isTrue => skipEmptyLocations(x.items.toList)
    case _ => lst
  }

  @SecuredRole("ANONYMOUS")
  def locations(cmd: LocationsVO, request: HttpServletRequest): CallResult = {
    val pid = cmd.parentId.safeLong
    val lst = if (pid > 0) sc.get[LocationService].getById(pid).get.items.toList else sc.get[LocationService].getCountries
    val r = for (loc <- skipEmptyLocations(lst)) yield locationVO(loc, request.xmisc.label(loc.getTextKey))
    dataCR(r)
  }

  @SecuredRole("ANONYMOUS")
  def postcodes(cmd: PostcodesVO, request: HttpServletRequest): CallResult = {
    val lid = cmd.locationId.safeLong
    val chain = sc.get[LocationService].parentChain(lid)
    val r = chain.reverse.view.collectFirst {
      case x if sc.get[LocationService].getPostcodesByLocationId(x.id).nonEmpty =>
        sc.get[LocationService].getPostcodesByLocationId(x.id)
    }.getOrElse(Nil).map(x => postcodeVO(x, request.xmisc.label(x.getTextKey)))
    dataCR(r)
  }

  @SecuredRole("AGENT")
  @ErrorCR(on = classOf[ExternalFailException], key = "fcall.extfl", log = "External resource failed")
  def geocode(cmd: GeocodeAddressVO, request: HttpServletRequest): CallResult = {
    val token = (x: String) => (x != "") ? (", "+x) | ""
    val address =
      token(cmd.line1)+token(cmd.line2)+token(cmd.line3)+token(cmd.line4)+token(cmd.line5)+
      token(cmd.town)+token(cmd.postcode)+token(cmd.country) |> (x => x.startsWith(",") ? x.substring(1).trim | x.trim)
    val r = sc.get[LocationService].geocode(address)
    dataCR(r.serializeAsMap)
  }
}
