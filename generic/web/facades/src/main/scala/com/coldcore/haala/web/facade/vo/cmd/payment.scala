package com.coldcore.haala.web.facade.vo.cmd
package payment

import beans.BeanProperty
import com.coldcore.haala.web.facade.annotations.Input

class CreatePaymentVO {
  @BeanProperty @Input (required = true, transform = "upper") var ref: String = _
  @BeanProperty @Input (required = true, datatype = "username", transform = "lower") var username: String = _
  @BeanProperty @Input (required = true, datatype = "float", transform = "float") var amount: String = _
  @BeanProperty @Input (required = true, transform = "upper") var currency: String = _
  @BeanProperty @Input var message: String = _
}
