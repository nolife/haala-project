package com.coldcore.haala.web.facade.vo.cmd

import com.coldcore.haala.web.facade.annotations.Input
import beans.BeanProperty

class AddressVO {
  @BeanProperty @Input (required = true) var line1: String = _
  @BeanProperty @Input var line2: String = _
  @BeanProperty @Input var line3: String = _
  @BeanProperty @Input var line4: String = _
  @BeanProperty @Input var line5: String = _
  @BeanProperty @Input (required = true) var town: String = _
  @BeanProperty @Input (required = true, transform = "upper") var postcode: String = _
  @BeanProperty @Input (required = true, transform = "upper") var country: String = _
}

class GeocodeAddressVO {
  @BeanProperty @Input var line1: String = _
  @BeanProperty @Input var line2: String = _
  @BeanProperty @Input var line3: String = _
  @BeanProperty @Input var line4: String = _
  @BeanProperty @Input var line5: String = _
  @BeanProperty @Input var town: String = _
  @BeanProperty @Input (transform = "upper") var postcode: String = _
  @BeanProperty @Input (transform = "upper") var country: String = _
}

class FilterVO {
  @BeanProperty var target: String = _
  @BeanProperty var params: Array[String] = _
}

class LoadVO {
  @BeanProperty var itemId: String = _
  @BeanProperty var params: Array[String] = _
}

class LocAddressVO {
  @BeanProperty @Input var country: String = _
  @BeanProperty @Input var region: String = _
  @BeanProperty @Input var subRegion: String = _
  @BeanProperty @Input var town: String = _
}

class LocationsVO {
  @BeanProperty var parentId: String = _
}

class PostcodesVO {
  @BeanProperty var locationId: String = _
}

class MapVO {
  @BeanProperty @Input var lat: String = _
  @BeanProperty @Input var lng: String = _
  @BeanProperty @Input var zoom: String = _
  @BeanProperty @Input var lat2: String = _
  @BeanProperty @Input var lng2: String = _
}

class ParamsVO {
  @BeanProperty var keys: Array[String] = _
  @BeanProperty var values: Array[String] = _
}

class SearchVO {
  @BeanProperty var from: String = _
  @BeanProperty var max: String = _
  @BeanProperty var sortBy: String = _
  @BeanProperty var sortType: String = _
  @BeanProperty var refresh: String = _
  @BeanProperty var target: String = _
  @BeanProperty var params: Array[String] = _
  @BeanProperty var hint: String = _
}

class TargetVO {
  @BeanProperty var target: String = _
}

class TokenVO {
  @BeanProperty var token: String = _
}

class DummyVO {

}
