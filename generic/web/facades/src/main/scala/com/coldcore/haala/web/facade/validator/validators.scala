package com.coldcore.haala
package web
package facade.validator

import com.coldcore.misc.scala.ReflectUtil._
import com.coldcore.haala.core.{CoreException, NamedLog}
import java.lang.reflect.{Field => JField}
import domain.Pack
import facade.vo.CallResult
import facade.vo.cmd.user.{ChangePasswordVO, EditDetailsVO}
import facade.annotations.{DatatypeInput, Input, TransformInput}
import core.{WebConstants, Dependencies}
import javax.servlet.http.HttpServletRequest
import service._
import Validator._

object Validator {
  type FieldValidator = (Field, Context) => Boolean // field, value, context
  val ignoreFieldValidator: FieldValidator = (_, _) => true

  case class Field(o: AnyRef, field: JField, value: Any) {
    val name: String = field.getName
    lazy val strval: String = value.asInstanceOf[String] // value as string

    def this(o: AnyRef, field: JField) = this(o, field, fieldValue(o, field))
    
    /** Re-get field with new value */
    def reget: Field = new Field(o, field)

    /** Re-set field value in the object */
    def reset(v: AnyRef): Field = { resetField(o, field, v); Field(o, field, v) } // with new value
  }

  class Context(val obj: AnyRef, val cr: CallResult, val request: HttpServletRequest) {
    private var fieldValidators = Map.empty[String, FieldValidator]
    def fieldValidator(field: String, f: FieldValidator) = fieldValidators = fieldValidators + (field -> f)
    def fieldValidator(field: String): Option[FieldValidator] = fieldValidators.get(field)
  }
  object Context {
    def apply(obj: AnyRef, cr: CallResult, request: HttpServletRequest) = new Context(obj, cr, request)
  }
}

trait Validator {
  def validate(context: Context): Boolean
}

trait Transform {
  self: BaseValidator =>

  /** Uppercase value. */
  @TransformInput("upper")
  def transformUpper(field: Field, context: Context): String =
    field.strval.toUpperCase

  /** Lowercase value. */
  @TransformInput("lower")
  def transformLower(field: Field, context: Context): String =
    field.strval.toLowerCase

  /** Format a value as URL. */
  @TransformInput("url")
  def transformUrl(field: Field, context: Context): String = {
    val v = field.strval.toLowerCase
    var s = if (v.startsWith("http://") || v.startsWith("https://")) field.strval else "http://"+field.strval
    s = if (s.endsWith("/")) s.substring(0, s.length-1) else s
    if (s.length < 10) "" else s // http://a.b
  }

  /** Format a value to be parsed as float. */
  @TransformInput("float")
  def transformFloat(field: Field, context: Context): String =
    field.strval.replace(",", ".")

  /** Format a value to be parsed as boolean (0 or 1). */
  @TransformInput("bool")
  def transformBool(field: Field, context: Context): String =
  field.strval.toLowerCase.equalsAny("0", "false", "no", "n") ? "0" | "1"
}

trait Datatype {
  self: BaseValidator =>

  /** Value must be one of the root sites (xx1, xx2). */
  @DatatypeInput("sites")
  def checkSites(field: Field, context: Context): Boolean =
    if (parsedSites.exists(_.key == field.strval)) true
    else { LOG.warn(s"Sites check failed: ${field.strval}"); false }

  /** Value must match a stored turing value. */
  @DatatypeInput("turing")
  def checkTuring(field: Field, context: Context): Boolean = {
    val an = field.field.getAnnotation(classOf[Input])
    if (an.datatype != "turing") true
    else if (setting("SystemParams").parseCSVMap.getOrElse("noCaptcha", "").isTrue) true //captcha disabled
    else if (setting("Dev/NoCaptcha").isTrue) true //captcha disabled (dev mode flag)
    else {
      val request = context.request
      request.xturing.incAttempt
      if (request.xturing.isValid()) {
        //proceed as normal
        val turingValue = request.xturing.get.value
        if (isUserAdmin || field.strval == turingValue) true
        else {
          LOG.warn(s"Turing $turingValue does not match input: ${field.strval}")
          context.cr.pushError(WebConstants.FACADE_ERROR_INVALID_TURING)
          false
        }
      } else {
        //no turing or its max attempts exceeded - refresh
        request.xturing.generate
        context.cr.pushError(WebConstants.FACADE_ERROR_REFRESH_ACTION)
        false
      }
    }
  }
}

class BaseValidator extends Validator with Dependencies with Transform with Datatype with SettingsReader with LabelsReader with UserRoleCheck {
  lazy val LOG = new NamedLog("base-validator", log)

  /** Validate an object - entry method. */
  override def validate(context: Context): Boolean =
    validateInner(context.obj, context)

  /** Actual validation. */
  private def validateInner(o: AnyRef, context: Context): Boolean = {
    o == null || validateArray(o, context) && validateInputs(o, context)
  }

  /** Re-validate items in an array object. */
  private def validateArray(o: AnyRef, context: Context): Boolean =
    o match {
      case x: Array[AnyRef] => x.map(validateInner(_, context)).forall(true==)
      case _ => true
    }

  /** Validate all input annotated fields. */
  private def validateInputs(o: AnyRef, context: Context): Boolean =
    (for (field <- fieldsByAnnotation[Input](o.getClass)) yield
      if (field.getType == classOf[String]) validateString(new Field(o, field), context) //string validation
      else if (field.getType == classOf[Array[String]]) validateStringArray(new Field(o, field), context) //string array validation
      else validateInner(fieldValue(o, field).asInstanceOf[AnyRef], context)).forall(true==) //descent into complex value

  /** Run transformation/validation on a string value. */
  private def validateString(in: Field, context: Context): Boolean = {
    //trim field's value in an object
    val value = in.value.asInstanceOf[String].safe.trim
    val field = in.reset(value)

    val defaultFieldValidator: FieldValidator = (_, _) =>
      if (isRequiredString(field, context)) { //required field?
        if (value.isEmpty) true //non-required empty value does not need transformation nor validation
        else {
          val v = transformField(field, context) //transform a value and proceed with the transformed result
          datatypeField(field.reset(v), context) //validate by data type
        }
      } else false

    context.fieldValidator(field.name).getOrElse(defaultFieldValidator)(field.reget, context)
  }

  /** Run transformation/validation on a string array. */
  private def validateStringArray(in: Field, context: Context): Boolean = {
    //trim field's array values in an object
    val value = in.value.asInstanceOf[Array[String]].safe.map(_.trim).filter("" !=)
    val field = in.reset(value)

    val defaultFieldValidator: FieldValidator = (_, _) =>
      if (isRequiredArray(field, context)) { //required field?
        if (value.isEmpty) true //non-required empty array does not need transformation nor validation
        else {
          val transformed = value.map(v => field.copy(value = v)).map(transformField(_, context)) //transform a value and proceed with the transformed result
          field.reset(transformed)
          transformed.map(v => field.copy(value = v)).forall(datatypeField(_, context)) //validate by data type
        }
      } else false

    context.fieldValidator(field.name).getOrElse(defaultFieldValidator)(field.reget, context)
  }

  /** Transform a field's value in an object. Call validator's transform method and return a transformed value. */
  private def transformField(field: Field, context: Context): String = {
    val an = field.field.getAnnotation(classOf[Input])
    if (an.transform == "") field.strval
    else {
      val mth =
        methodsByAnnotation[TransformInput](getClass)
          .find(_.getAnnotation(classOf[TransformInput]).value == an.transform)
          .getOrElse(throw new NoSuchMethodException(s"Transformer for input ${an.transform} not found in $getClass"))
      mth.invoke(this, field, context).asInstanceOf[String]
    }
  }

  /** Required value must not be empty. */
  private def isRequiredString(field: Field, context: Context): Boolean = {
    val an = field.field.getAnnotation(classOf[Input])
    if (an.required && field.strval == "") {
      LOG.warn("Required field "+field.name+" is empty")
      context.cr.pushSystemError
      false
    } else true
  }

  /** Required array must not be empty. */
  private def isRequiredArray(field: Field, context: Context): Boolean = {
    val an = field.field.getAnnotation(classOf[Input])
    if (an.required && field.value.asInstanceOf[Array[_]].isEmpty) {
      LOG.warn(s"Required field ${field.name} is empty array")
      context.cr.pushSystemError
      false
    } else true
  }

  /** Validates a property against its data type. Call data type validator check method or validate on regex. */
  private def datatypeField(field: Field, context: Context): Boolean = {
    val an = field.field.getAnnotation(classOf[Input])
    if (an.datatype == "") true
    else {
      val b =
        methodsByAnnotation[DatatypeInput](getClass)
          .find(_.getAnnotation(classOf[DatatypeInput]).value == an.datatype) match {
          case Some(x) => x.invoke(this, field, context).asInstanceOf[Boolean]
          case _ => regexField(field) //no method? check by regex
        }
      if (b) true else { context.cr.pushSystemError; false }
    }
  }

  /** Run validation of a property value against property data type regex.
   *  DB must contain space separated regular expressions for a data type.
   */
  private def regexField(field: Field): Boolean = {
    val an = field.field.getAnnotation(classOf[Input])
    val rkey = "RegEx/"+an.datatype
    val rval = setting(rkey)

    if (rval.safe == "") throw new CoreException("No regex for datatype "+an.datatype)
    if (rval.split(" ").filter(""!=).forall(field.strval.matches)) true
    else {
      LOG.warn(s"Field $field.name failed regex check: ${field.strval}")
      false
    }
  }

}

class CmsValidator extends BaseValidator {
  override lazy val LOG = new NamedLog("cms-validator", log)

  /** Check if a user owns domain's site or a domain. */
  @DatatypeInput("owner")
  def checkOwner(field: Field, context: Context): Boolean =
    field.name match {
      case "site" => isSiteOwner(field.strval, context)
      case "labelId" => isLabelOwner(field.strval, context)
      case "settingId" => isSettingOwner(field.strval, context)
      case "fileId" => isFileOwner(field.strval, context)
      case _ => isDomainOwner(field.strval, context)
    }

  /** Check if a user owns a site. */
  private def isSiteOwner(value: String, context: Context): Boolean = {
    val userId = context.request.xuser.currentId
    if (isUserAdmin || sc.get[UserService].isSiteOwner(userId, value)) true
    else {
      LOG.warn("Owner #"+userId+" check failed for site #"+value)
      false
    }
  }

  /** Check if a user owns a label. */
  private def isLabelOwner(value: String, context: Context): Boolean =
    label(value.safeLong) match {
      case Some(x) => isSiteOwner(x.site, context)
      case None =>
        LOG.warn("Owner #"+context.request.xuser.currentId+" check failed for label #"+value+" (not found)")
        false
    }

  /** Check if a user owns a setting. */
  private def isSettingOwner(value: String, context: Context): Boolean =
    setting(value.safeLong) match {
      case Some(x) => isSiteOwner(x.site, context)
      case None =>
        LOG.warn("Owner #"+context.request.xuser.currentId+" check failed for setting #"+value+" (not found)")
        false
    }

  /** Check if a user owns a file. */
  private def isFileOwner(value: String, context: Context): Boolean =
    sc.get[FileService].getById(value.safeLong) match {
      case Some(x) => isSiteOwner(x.site, context)
      case None =>
        LOG.warn("Owner #"+context.request.xuser.currentId+" check failed for file #"+value+" (not found)")
        false
    }

  /** Check if a user owns a domain. */
  private def isDomainOwner(value: String, context: Context): Boolean = {
    val userId = context.request.xuser.currentId
    if (isUserAdmin || sc.get[UserService].isDomainOwner(userId, value.safeLong)) true
    else {
      LOG.warn("Owner #"+userId+" check failed for domain #"+value)
      false
    }
  }
}

class UserValidator extends BaseValidator {
  override lazy val LOG = new NamedLog("user-validator", log)

  override def validate(context: Context): Boolean = context.obj match {
    case _: ChangePasswordVO => onChangePassword(context)
    case _: EditDetailsVO => onEditDetails(context)
    case _ => super.validate(context)
  }

  /** Check if a user provided correct password in a command. */
  private def onChangePassword(context: Context): Boolean = {
    val o = context.obj.asInstanceOf[ChangePasswordVO]
    context.fieldValidator("oldPassword", (_, _) => {
      val request = context.request
      val (suser, cuser) = (request.xuser.`super`.orNull, request.xuser.current.get)
      if (cuser.password == o.oldPassword || //user password matches
        suser != null && suser.password == o.oldPassword) //or super user password matches, if present
        true
      else {
        LOG.warn("Invalid old password provided")
        context.cr.pushError(label("fcall.erpas", request.xsite.current))
        false
      }
    })
    super.validate(context)
  }

  /** Check if a user provided correct password in a command. */
  private def onEditDetails(context: Context): Boolean = {
    val o = context.obj.asInstanceOf[EditDetailsVO]
    context.fieldValidator("password", (_, _) => {
      val request = context.request
      val (suser, cuser) = (request.xuser.`super`.orNull, request.xuser.current.get)
      if (cuser.password == o.password || //user password matches
        suser != null && suser.password == o.password) //or super user password matches, if present
        true
      else {
        LOG.warn("Invalid password provided")
        context.cr.pushError(label("fcall.erpas", request.xsite.current))
        false
      }
    })
    context.fieldValidator("username", ignoreFieldValidator) // does not change
    super.validate(context)
  }
}

/** Common checks used in packs */
trait CommonChecks {
  self: BaseValidator =>

  /** Check if a user owns all provided domains. */
  def isDomainsOwner(value: String, context: Context): Boolean = {
    val userId = context.request.xuser.currentId
    val b = value.parseCSV.forall(x => sc.get[UserService].isDomainOwner(userId, x.safeLong))
    if (isUserAdmin || b) true
    else {
      LOG.warn("Owner #"+userId+" check failed for domain #"+value)
      false
    }
  }

  /** Check if a user owns all provided domain objects. */
  def isOwnerOf(value: String, context: Context,
                findById: Long => Option[{ def pack: Pack }], logtype: String): Boolean = {
    val userId = context.request.xuser.currentId
    val b = value.parseCSV.forall(x => findById(x.safeLong).map(_.pack.user.id).getOrElse(0) == userId)
    if (isUserAdmin || b) true
    else {
      LOG.warn(s"Owner #$userId check failed for $logtype #$value")
      false
    }
  }

}
