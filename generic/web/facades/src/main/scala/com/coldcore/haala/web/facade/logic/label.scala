package com.coldcore.haala
package web
package facade.logic

import web.facade.vo.cmd.cms._
import web.facade.annotations.ValidatorClass
import web.facade.validator.Validator

import beans.BeanProperty
import web.facade.vo.CallResult
import web.facade.vo.cmd._
import security.annotations.SecuredRole
import javax.servlet.http.HttpServletRequest

import com.coldcore.haala.core.NamedLog

class LabelFacade extends BaseFacade {
  private lazy val LOG = new NamedLog("label-facade", log)

  @BeanProperty @ValidatorClass var cmsValidator: Validator = _
  @BeanProperty var cmsFacade: CmsFacade = _

  @SecuredRole("ADMIN")
  def load(cmd: LoadLabelVO, request: HttpServletRequest): CallResult =
    cmsFacade.loadLabel(cmd, request)

  @SecuredRole("ADMIN")
  def edit(cmd: EditLabelVO, request: HttpServletRequest): CallResult =
    cmsFacade.editLabel(cmd, request)

  @SecuredRole("ADMIN")
  def add(cmd: AddLabelVO, request: HttpServletRequest): CallResult =
    cmsFacade.addLabel(cmd, request)

  @SecuredRole("ADMIN")
  def remove(cmd: RemoveLabelVO, request: HttpServletRequest): CallResult =
    cmsFacade.removeLabel(cmd, request)

  @SecuredRole("ADMIN")
  def search(cmd: SearchLabelsVO, request: HttpServletRequest): CallResult =
    cmsFacade.searchLabels(cmd, request)

  @SecuredRole("ADMIN")
  def filter(cmd: FilterVO, request: HttpServletRequest): CallResult =
    cmsFacade.filterLabels(cmd, request)
}
