package com.coldcore.haala
package web
package facade.aspect

import org.aspectj.lang.ProceedingJoinPoint
import org.aspectj.lang.annotation.{Around, Aspect, Pointcut}

import beans.BeanProperty
import com.coldcore.haala.core.GeneralUtil._
import com.coldcore.haala.core._
import com.coldcore.misc.scala.ReflectUtil._
import com.coldcore.misc.scala.StringOp._
import core.Aspect._
import core.{Dependencies, Setup, WebConstants}
import domain.Role
import facade.annotations._
import facade.vo.CallResult
import facade.validator.Validator
import service.{DomainService, MiscService, SettingsReader, UserRoleCheck}
import service.exceptions.ServiceException
import security.annotations._
import scala.collection.JavaConverters._
import java.lang.reflect.InvocationTargetException
import java.util.regex.Pattern

import javax.servlet.http.HttpServletRequest

@Aspect
abstract class AroundFacadeAspect extends Dependencies {
  @BeanProperty var order: Int = _

  def around(pjp: ProceedingJoinPoint, request: HttpServletRequest): AnyRef

  @Pointcut("@within(com.coldcore.haala.web.facade.annotations.AopFacade)")
  def facadeMethods = {}

  @Around("facadeMethods()")
  def aroundFacadeMethods(pjp: ProceedingJoinPoint) = around(pjp, requestArg(pjp).get)

  private def requestArg(pjp: ProceedingJoinPoint): Option[HttpServletRequest] =
    pjp.getArgs.find(_.isInstanceOf[HttpServletRequest]).asInstanceOf[Option[HttpServletRequest]]
}

/** Ensure initial attributes are set. */
class InitialAspect extends AroundFacadeAspect with Setup {
  @BeanProperty var statistics: Statistics = _

  var mods = List.empty[Mod]
  def setMods(x: JList[Mod]) = mods = x.toList

  override def around(pjp: ProceedingJoinPoint, request: HttpServletRequest): AnyRef = {
    request.serviceContainer = sc
    request.resourceLoaderHolder = resourceLoaderHolder
    request.getSession(true)

    preHandle(request, setupMods, mods)
    val result = pjp.proceed
    postHandle(request)

    statistics.requests.incrementAndGet
    result
  }
}

/** Log total request execution time */
class LogAspect extends AroundFacadeAspect {
  private lazy val LOG = new NamedLog("facade.log-aspect", log)

  private def formatCall(pjp: ProceedingJoinPoint): String =
    pjp.getSignature.getDeclaringTypeName+"::"+pjp.getSignature.getName

  override def around(pjp: ProceedingJoinPoint, request: HttpServletRequest): AnyRef = {
    val start = System.currentTimeMillis
    val o = pjp.proceed
    LOG.info(formatCall(pjp)+" executed in "+millsToSec(System.currentTimeMillis-start)+"s")
    o
  }
}

/** Validate incoming arguments by using a validator from the requested class */
class ValidatorAspect extends AroundFacadeAspect {
  private def findValidator(target: AnyRef): Option[Validator] = {
    val o = fieldByAnnotation[TargetClass](target.getClass).
      map(f => fieldValue(target, f).asInstanceOf[AnyRef]).getOrElse(target) //delegate, has inner target field
    fieldByAnnotation[ValidatorClass](o.getClass).map(x => fieldValue(o, x).asInstanceOf[Validator])
  }

  override def around(pjp: ProceedingJoinPoint, request: HttpServletRequest): AnyRef = {
    val target = pjp.getTarget
    val (validator, cr) = (findValidator(target), new CallResult)
    val b =
      (for (arg <- pjp.getArgs if arg != null && !arg.isInstanceOf[String]) yield
        validator.map(_.validate(Validator.Context(arg, cr, request)))
          .getOrElse(throw new CoreException("No validator found in "+target.getClass))).forall(true==)

    if (b) pjp.proceed else cr
  }
}

/** Check if a method can be accessed by request from foreign domain */
class CrossDomainAspect extends AroundFacadeAspect {
  private lazy val LOG = new NamedLog("facade.cross-domain-aspect", log)

  private def checkMethodAccess(mth: String, target: AnyRef): Boolean = {
    val an = methodByName(target.getClass, mth).get.getAnnotation(classOf[CrossDomain])
    an != null && Access.byValue(an.value) == Access.ALWAYS
  }

  /** http://www.my-site.com:8080/login/bar.htm -> www.my-site.com */
  private def refererDomain(request: HttpServletRequest): String = {
    val referer = request.getHeader("referer").safe.toLowerCase.trim
    (if (referer.contains("://")) referer.split("://")(1) else referer).split("/").head.split(":").head
  }

  override def around(pjp: ProceedingJoinPoint, request: HttpServletRequest): AnyRef = {
    val curDomain = request.xsite.state.currentDomain
    val refDomain = refererDomain(request) or "?"
    val resDomain = request.sc.get[DomainService].resolveDomain(refDomain).domain // resolve referer
    val (mth, target) = (pjp.getSignature.getName, pjp.getTarget)

    if (refDomain == curDomain || // referer is the same as base domain
        resDomain == curDomain && resDomain != "*" || // referer resolved to the same base domain 
        checkMethodAccess(mth, target)) // cross domain explicitly allowed 
      pjp.proceed
    else {
      LOG.warn(s"Cross domain call from $refDomain is forbidden to access $curDomain method $mth in ${target.getClass}")
      new CallResult().pushSystemError
    }
  }
}

/** Forbid access if the system is in downtime mode */
class DowntimeAspect extends AroundFacadeAspect with SettingsReader with UserRoleCheck {
  private def isIgnoreDowntime(mth: String, target: AnyRef): Boolean =
    methodByName(target.getClass, mth).get.isAnnotationPresent(classOf[IgnoreDowntime])

  override def around(pjp: ProceedingJoinPoint, request: HttpServletRequest): AnyRef = {
    val down = setting("DowntimeFlag").isTrue
    if (!down || isUserAdmin || isIgnoreDowntime(pjp.getSignature.getName, pjp.getTarget))
      pjp.proceed
    else
      new CallResult().pushError(WebConstants.FACADE_ERROR_DOWNTIME)
  }
}

/** Catch exception wrapping those into proper response. Must be the first in a chain. */
class ErrorHandlingAspect extends AroundFacadeAspect {
  private lazy val LOG = new NamedLog("facade.error-handling-aspect", log)

  private def formatCall(pjp: ProceedingJoinPoint): String =
    pjp.getSignature.getDeclaringTypeName+"::"+pjp.getSignature.getName

  override def around(pjp: ProceedingJoinPoint, request: HttpServletRequest): AnyRef =
    try {
      pjp.proceed
    } catch {
      case e: ServiceException =>
        LOG.error("Unhandled service exception", e)
        sc.get[MiscService].reportSystemError("[facade.error-handling-aspect] Unhandled service exception", e)
        new CallResult().pushSystemError
      case e: Throwable =>
        LOG.error("Error "+formatCall(pjp), e)
        sc.get[MiscService].reportSystemError("[facade.error-handling-aspect] Error "+formatCall(pjp), e)
        new CallResult().pushSystemError
    }
}

/** If the supplied "target" argument has TargetClass annotated field then return its value (delegate). */
trait ResolveTarget {
  def resolveTarget(target: AnyRef): AnyRef =
    fieldByAnnotation[TargetClass](target.getClass) match {
      case Some(f) => fieldValue(target, f).asInstanceOf[AnyRef] //delegate, has inner target field
      case None => target //not a delegate
    }
}

/** Check if requested method can be accessed with user roles */
class SecurityAspect extends AroundFacadeAspect with UserRoleCheck with ResolveTarget {
  private def checkMethodAccess(mth: String, target: AnyRef, request: HttpServletRequest): Symbol = {
    val o = resolveTarget(target)
    val m = methodByName(o.getClass, mth).get
    val securedRole = m.getAnnotation(classOf[SecuredRole])
    if (securedRole == null)
      throw new SecurityException("No secured annotation on method "+mth+" in "+target.getClass)

    val role = Role.role(securedRole.value)
    if (role == Role.ANONYMOUS) 'pass
    else if (request.xuser.current.isEmpty || securityService.isPrincipalMissing) 'nouser
    else if (isUserAdmin || isUserInRole(role)) 'pass
    else 'fail
  }

  override def around(pjp: ProceedingJoinPoint, request: HttpServletRequest): AnyRef =
    checkMethodAccess(pjp.getSignature.getName, pjp.getTarget, request) match {
      case 'pass => pjp.proceed
      case 'fail => new CallResult().pushError(WebConstants.FACADE_ERROR_SECURITY_VIOLATED)
      case 'nouser => new CallResult().pushError(WebConstants.FACADE_ERROR_SESSION_EXPIRED)
      case _ => throw new IllegalStateException("Unknown access")
    }
}

/** Traverse response converting Scala into Java */
class ConverterAspect extends AroundFacadeAspect {
  private def output(in: Any): Any = in match {
    case x: Map[_,_] =>
      x.asInstanceOf[Map[Any,Any]].map {
        case (k: Symbol,v) => k.name -> output(v)
        case (k,v) => k.toString -> output(v)
      }.asJava
    case x: Seq[_] => x.map(output).toArray
    case x => x
  }

  override def around(pjp: ProceedingJoinPoint, request: HttpServletRequest): AnyRef = {
    val o = pjp.proceed
    if (!o.isInstanceOf[CallResult]) new CallResult
    else {
      val cr = o.asInstanceOf[CallResult]
      cr.result = output(cr.result)
      cr
    }
  }
}

/** Invoke requested method or delegated method directly. Must be the last in a chain. */
class InvokerAspect extends AroundFacadeAspect with ResolveTarget {
  override def around(pjp: ProceedingJoinPoint, request: HttpServletRequest): AnyRef =
    fieldByAnnotation[TargetClass](pjp.getTarget.getClass) match {
      case Some(f) => //delegate, has inner target field
        val o = fieldValue(pjp.getTarget, f).asInstanceOf[AnyRef]
        try {
          invokeMethod(o, pjp.getSignature.getName, pjp.getArgs.toList: _*).asInstanceOf[AnyRef]
        } catch { case e: InvocationTargetException => throw e.getTargetException }
      case None => pjp.proceed //not a delegate
    }
}

/** Create erroneous CallResult if a facade throws exception matching ErrorCR annotations. */
class ErroneousAspect extends AroundFacadeAspect with ResolveTarget {
  private lazy val LOG = new NamedLog("facade.erroneous-aspect", log)

  private def errorCRs(mth: String, target: AnyRef): List[ErrorCR] = {
    val o = resolveTarget(target)
    val m = methodByName(o.getClass, mth).get
    Option(m.getAnnotation(classOf[ErrorCRs])).map(_.value.toList)
      .getOrElse(Option(m.getAnnotation(classOf[ErrorCR])).toList)
  }

  private def injectParams(text: String, e: Throwable, args: List[AnyRef], request: HttpServletRequest): String = {
    import Pattern._
    var s = text
    val f = (a: String, xs: List[AnyRef]) =>
      findTokens(quote(s"{$a"), quote("}"), s).foreach { token =>
        xs.find(x => fieldByName(x.getClass, token).isDefined).foreach { x =>
          s = s.replace(s"{$a$token}", fieldValue(x, token).toString)
        }
      }
    // invoke getters on exception {e.ref}, on arguments {a.username}, on both {ref} {username}
    f("e.", e :: Nil)
    f("a.", args)
    f("", e :: args)

    s = s.replace("{currentUsername}", request.xuser.currentUsername)
    s
  }

  private def printLog(target: AnyRef, text: String) = {
    val o = resolveTarget(target)
    fieldByAnnotation[LogClass](o.getClass) match {
      case Some(f) => fieldValue(o, f).asInstanceOf[SLF4J].info(text)
      case _ => LOG.warn(s"No log in ${o.getClass}, message: $text")
    }
  }

  private def createCR(request: HttpServletRequest, e: Throwable, args: List[AnyRef], err: ErrorCR): CallResult = {
    val inject = (text: String) => injectParams(text, e, args, request)
    new CallResult(inject(err.result))
      .pushError(err.code)
      .pushError(if (err.key.isEmpty) "" else inject(request.xmisc.label(err.key) nil err.key))
  }

  private def handle(target: AnyRef, request: HttpServletRequest, e: Throwable, args: List[AnyRef])(err: ErrorCR): CallResult = {
    val inject = (text: String) => injectParams(text, e, args, request)
    if (err.log.nonEmpty) printLog(target, inject(err.log))
    createCR(request, e, args, err)
  }

  override def around(pjp: ProceedingJoinPoint, request: HttpServletRequest): AnyRef =
    try {
      pjp.proceed
    } catch {
      case e: Throwable =>
        errorCRs(pjp.getSignature.getName, pjp.getTarget)
          .find(x => x.on.isInstance(e))
          .map(handle(pjp.getTarget, request, e, pjp.getArgs.toList))
          .getOrElse(throw e)
    }
}
