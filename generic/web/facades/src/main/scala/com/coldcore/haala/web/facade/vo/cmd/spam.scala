package com.coldcore.haala.web.facade.vo.cmd
package spam

import com.coldcore.haala.web.facade.annotations.Input
import beans.BeanProperty

class AddStorageVO {
  @BeanProperty @Input (required = true) var description: String = _
}

class UpdateStorageVO {
  @BeanProperty @Input var storageId: String = _
  @BeanProperty @Input var description: String = _
  @BeanProperty @Input var append: String = _
}

class RemoveStorageVO {
  @BeanProperty @Input var storageId: String = _
}

class StartTaskVO {
  @BeanProperty @Input var storageId: String = _
  @BeanProperty @Input var from: String = _
  @BeanProperty @Input var subject: String = _
  @BeanProperty @Input var text: String = _
}
