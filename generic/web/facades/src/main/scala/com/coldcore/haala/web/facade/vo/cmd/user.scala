package com.coldcore.haala.web.facade.vo.cmd
package user

import com.coldcore.haala.web.facade.annotations.Input
import beans.BeanProperty

class BaseUserVO {
  @BeanProperty @Input (required = true, datatype = "username", transform = "lower") var username: String = _
  @BeanProperty @Input (required = true, datatype = "password") var password: String = _
  @BeanProperty @Input (required = true, datatype = "email", transform = "lower") var email: String = _
  @BeanProperty @Input (required = true) var salutation: String = _
  @BeanProperty @Input (required = true) var firstName: String = _
  @BeanProperty @Input (required = true) var lastName: String = _
  @BeanProperty @Input (required = true, transform = "upper") var primaryNumber: String = _
  @BeanProperty @Input var address: AddressVO = _
}

class ChangePasswordVO {
  @BeanProperty @Input (required = true) var oldPassword: String = _
  @BeanProperty @Input (required = true, datatype = "password") var password: String = _
}

class CreditVO {
  @BeanProperty var userId: String = _
  @BeanProperty @Input (required = true, datatype = "number") var amount: String = _
  @BeanProperty @Input var message: String = _
}

class EditDetailsVO extends BaseUserVO

class LoginVO { // with username
  @BeanProperty @Input (required = true, datatype = "username", transform = "lower") var username: String = _
  @BeanProperty @Input (required = true, datatype = "password") var password: String = _
  @BeanProperty @Input (required = true, datatype = "turing", transform = "lower") var turing: String = _
}

class Login2VO { // with username or email
  @BeanProperty @Input (required = true, transform = "lower") var value: String = _
  @BeanProperty @Input (required = true, datatype = "password") var password: String = _
  @BeanProperty @Input (required = true, datatype = "turing", transform = "lower") var turing: String = _
}

class RegisterVO extends BaseUserVO {
  @BeanProperty @Input (required = true, datatype = "turing", transform = "lower") var turing: String = _
  @BeanProperty var params: Array[String] = _
}

class ReminderVO {
  @BeanProperty @Input (required = true, transform = "lower") var value: String = _
  @BeanProperty @Input (required = true, datatype = "turing", transform = "lower") var turing: String = _
}

class ShellVO {
  @BeanProperty var userId: String = _
}

class AcceptAgreementVO {
  @BeanProperty @Input (transform = "upper") var ref: String = _
  @BeanProperty @Input (required = true, datatype = "turing", transform = "lower") var turing: String = _
}

class EditPropsVO {
  @BeanProperty var userId: String = _
  @BeanProperty @Input (required = true, datatype = "username", transform = "lower") var username: String = _
  @BeanProperty @Input (transform = "bool") var disabled: String = _
  @BeanProperty @Input (datatype = "date") var activated: String = _
  @BeanProperty @Input var domains: String = _
  @BeanProperty @Input var roles: String = _
}
