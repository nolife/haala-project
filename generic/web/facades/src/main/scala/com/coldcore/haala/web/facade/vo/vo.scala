package com.coldcore.haala
package web.facade.vo

import com.coldcore.haala.web.core.WebConstants
import beans.{BooleanBeanProperty, BeanProperty}

class CallResult {
  @BeanProperty var errorsText = new JArrayList[String]
  @BeanProperty var errorCodes = new JArrayList[JInteger]
  @BeanProperty var result: Any = _
  @BooleanBeanProperty var failed: Boolean = _

  def this(x: Any) = { this(); result = x }

  def pushError(code: Int, text: String): CallResult = {
    if (text != null && !text.isEmpty) errorsText.add(text)
    if (code > 0) errorCodes.add(code)
    failed = true
    this
  }

  def pushError(text: String): CallResult = pushError(WebConstants.FACADE_ERROR_OTHER, text)
  def pushError(code: Int): CallResult = pushError(code, null)
  def pushSystemError: CallResult = pushError(WebConstants.FACADE_ERROR_SYSTEM)
}

