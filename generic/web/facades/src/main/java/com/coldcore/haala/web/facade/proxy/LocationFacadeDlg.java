package com.coldcore.haala.web.facade.proxy;

import com.coldcore.haala.web.facade.annotations.AopFacade;
import com.coldcore.haala.web.facade.annotations.TargetClass;
import com.coldcore.haala.web.facade.logic.LocationFacade;
import com.coldcore.haala.web.facade.vo.cmd.*;

import javax.servlet.http.HttpServletRequest;

/** AOP proxy delegate */
@AopFacade
public class LocationFacadeDlg {

  @TargetClass private LocationFacade locationFacade;

  public LocationFacade getLocationFacade() {
    return locationFacade;
  }

  public void setLocationFacade(LocationFacade locationFacade) {
    this.locationFacade = locationFacade;
  }

  /*** Stub methods. Invoked by 'InvokerAspect' directly on the 'target'. ***/
  public Object locations(LocationsVO cmd, HttpServletRequest request) { return null; }
  public Object postcodes(PostcodesVO cmd, HttpServletRequest request) { return null; }
  public Object geocode(GeocodeAddressVO cmd, HttpServletRequest request) { return null; }
}