package com.coldcore.haala.web.facade.proxy;

import com.coldcore.haala.web.facade.annotations.AopFacade;
import com.coldcore.haala.web.facade.annotations.TargetClass;
import com.coldcore.haala.web.facade.logic.SpamFacade;
import com.coldcore.haala.web.facade.vo.cmd.*;
import com.coldcore.haala.web.facade.vo.cmd.spam.*;

import javax.servlet.http.HttpServletRequest;

/** AOP proxy delegate */
@AopFacade
public class SpamFacadeDlg {

  @TargetClass private SpamFacade spamFacade;

  public SpamFacade getSpamFacade() {
    return spamFacade;
  }

  public void setSpamFacade(SpamFacade spamFacade) {
    this.spamFacade = spamFacade;
  }

  /*** Stub methods. Invoked by 'InvokerAspect' directly on the 'target'. ***/
  public Object searchStorages(SearchVO cmd, HttpServletRequest request) { return null; }
  public Object createStorage(AddStorageVO cmd, HttpServletRequest request) { return null; }
  public Object updateStorage(UpdateStorageVO cmd, HttpServletRequest request) { return null; }
  public Object removeStorage(RemoveStorageVO cmd, HttpServletRequest request) { return null; }
  public Object startTask(StartTaskVO cmd, HttpServletRequest request) { return null; }
  public Object stopTask(String noprm, HttpServletRequest request) { return null; }
}
