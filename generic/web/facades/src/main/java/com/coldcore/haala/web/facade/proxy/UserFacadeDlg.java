package com.coldcore.haala.web.facade.proxy;

import com.coldcore.haala.web.facade.annotations.AopFacade;
import com.coldcore.haala.web.facade.annotations.TargetClass;
import com.coldcore.haala.web.facade.logic.UserFacade;
import com.coldcore.haala.web.facade.vo.cmd.*;
import com.coldcore.haala.web.facade.vo.cmd.user.*;

import javax.servlet.http.HttpServletRequest;

/** AOP proxy delegate */
@AopFacade
public class UserFacadeDlg {

  @TargetClass private UserFacade userFacade;

  public UserFacade getUserFacade() {
    return userFacade;
  }

  public void setUserFacade(UserFacade userFacade) {
    this.userFacade = userFacade;
  }

  /*** Stub methods. Invoked by 'InvokerAspect' directly on the 'target'. ***/
  public Object usernameTaken(String username, HttpServletRequest request) { return null; }
  public Object register(RegisterVO cmd, HttpServletRequest request) { return null; }
  public Object remindPassword(ReminderVO cmd, HttpServletRequest request) { return null; }
  public Object editDetails(EditDetailsVO cmd, HttpServletRequest request) { return null; }
  public Object credit(CreditVO cmd, HttpServletRequest request) { return null; }
  public Object changePassword(ChangePasswordVO cmd, HttpServletRequest request) { return null; }
  public Object search(SearchVO cmd, HttpServletRequest request) { return null; }
  public Object acceptAgreement(AcceptAgreementVO cmd, HttpServletRequest request) { return null; }
  public Object editProps(EditPropsVO cmd, HttpServletRequest request) { return null; }
}