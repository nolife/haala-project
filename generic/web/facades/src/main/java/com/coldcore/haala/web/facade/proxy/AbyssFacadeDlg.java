package com.coldcore.haala.web.facade.proxy;

import com.coldcore.haala.web.facade.annotations.AopFacade;
import com.coldcore.haala.web.facade.annotations.TargetClass;
import com.coldcore.haala.web.facade.logic.AbyssFacade;
import com.coldcore.haala.web.facade.vo.cmd.*;
import com.coldcore.haala.web.facade.vo.cmd.abyss.*;

import javax.servlet.http.HttpServletRequest;

/** AOP proxy delegate */
@AopFacade
public class AbyssFacadeDlg {

  @TargetClass private AbyssFacade abyssFacade;

  public AbyssFacade getAbyssFacade() {
    return abyssFacade;
  }

  public void setAbyssFacade(AbyssFacade abyssFacade) {
    this.abyssFacade = abyssFacade;
  }

  /*** Stub methods. Invoked by 'InvokerAspect' directly on the 'target'. ***/
  public Object add(AddVO cmd, HttpServletRequest request) { return null; }
  public Object edit(EditVO cmd, HttpServletRequest request) { return null; }
  public Object load(LoadVO cmd, HttpServletRequest request) { return null; }
  public Object search(SearchVO cmd, HttpServletRequest request) { return null; }
}
