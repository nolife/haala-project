package com.coldcore.haala.web.facade.proxy;

import com.coldcore.haala.web.facade.annotations.AopFacade;
import com.coldcore.haala.web.facade.annotations.TargetClass;
import com.coldcore.haala.web.facade.logic.CmsFacade;
import com.coldcore.haala.web.facade.vo.cmd.*;
import com.coldcore.haala.web.facade.vo.cmd.cms.*;

import javax.servlet.http.HttpServletRequest;

/** AOP proxy delegate */
@AopFacade
public class CmsFacadeDlg {

  @TargetClass private CmsFacade cmsFacade;

  public CmsFacade getCmsFacade() {
    return cmsFacade;
  }

  public void setCmsFacade(CmsFacade cmsFacade) {
    this.cmsFacade = cmsFacade;
  }

  /*** Stub methods. Invoked by 'InvokerAspect' directly on the 'target'. ***/
  public Object loadText(LoadVO cmd, HttpServletRequest request) { return null; }
  public Object editText(EditTextVO cmd, HttpServletRequest request) { return null; }
  public Object loadPic(LoadVO cmd, HttpServletRequest request) { return null; }
  public Object editPic(EditFileVO cmd, HttpServletRequest request) { return null; }
  public Object loadFile(LoadVO cmd, HttpServletRequest request) { return null; }
  public Object editFile(EditFileVO cmd, HttpServletRequest request) { return null; }
  public Object status(TargetVO cmd, HttpServletRequest request) { return null; }
  public Object incVersion(IncVersionVO cmd, HttpServletRequest request) { return null; }
  public Object switchOn(SwitchOnVO cmd, HttpServletRequest request) { return null; }

  public Object loadDomain(LoadDomainVO cmd, HttpServletRequest request) { return null; }
  public Object editDomain(EditDomainVO cmd, HttpServletRequest request) { return null; }

  public Object editPageMapping(EditPageMappingVO cmd, HttpServletRequest request) { return null; }
  public Object addPageMapping(AddPageMappingVO cmd, HttpServletRequest request) { return null; }
  public Object removePageMapping(RemovePageMappingVO cmd, HttpServletRequest request) { return null; }
  public Object editPageLabels(EditPageLabelsVO cmd, HttpServletRequest request) { return null; }

  public Object searchLabels(SearchLabelsVO cmd, HttpServletRequest request) { return null; }
  public Object filterLabels(FilterVO cmd, HttpServletRequest request) { return null; }
  public Object loadLabel(LoadLabelVO cmd, HttpServletRequest request) { return null; }
  public Object editLabel(EditLabelVO cmd, HttpServletRequest request) { return null; }
  public Object addLabel(AddLabelVO cmd, HttpServletRequest request) { return null; }
  public Object removeLabel(RemoveLabelVO cmd, HttpServletRequest request) { return null; }

  public Object loadSetting(LoadSettingVO cmd, HttpServletRequest request) { return null; }
  public Object editSetting(EditSettingVO cmd, HttpServletRequest request) { return null; }
  public Object addSetting(AddSettingVO cmd, HttpServletRequest request) { return null; }
  public Object removeSetting(RemoveSettingVO cmd, HttpServletRequest request) { return null; }
  public Object searchSettings(SearchSettingsVO cmd, HttpServletRequest request) { return null; }
  public Object filterSettings(FilterVO cmd, HttpServletRequest request) { return null; }

  public Object searchFiles(SearchFilesVO cmd, HttpServletRequest request) { return null; }
  public Object filterFiles(FilterVO cmd, HttpServletRequest request) { return null; }
  public Object loadFile2(LoadFileVO cmd, HttpServletRequest request) { return null; }
  public Object loadFilePath(LoadFilePathVO cmd, HttpServletRequest request) { return null; }
  public Object loadPicture(LoadFileVO cmd, HttpServletRequest request) { return null; }
  public Object removeFile(RemoveFileVO cmd, HttpServletRequest request) { return null; }
  public Object addFile(AddFileVO cmd, HttpServletRequest request) { return null; }
  public Object editFile2(EditFileVO cmd, HttpServletRequest request) { return null; }
  public Object editTextFile(EditTextFileVO cmd, HttpServletRequest request) { return null; }
  public Object loadTextFile(LoadFileVO cmd, HttpServletRequest request) { return null; }
  public Object backupFile(BackupFileVO cmd, HttpServletRequest request) { return null; }
}
