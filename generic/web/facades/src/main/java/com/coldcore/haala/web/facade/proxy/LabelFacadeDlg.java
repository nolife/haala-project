package com.coldcore.haala.web.facade.proxy;

import com.coldcore.haala.web.facade.annotations.AopFacade;
import com.coldcore.haala.web.facade.annotations.TargetClass;
import com.coldcore.haala.web.facade.logic.LabelFacade;
import com.coldcore.haala.web.facade.vo.cmd.*;
import com.coldcore.haala.web.facade.vo.cmd.cms.*;

import javax.servlet.http.HttpServletRequest;

/** AOP proxy delegate */
@AopFacade
public class LabelFacadeDlg {

  @TargetClass private LabelFacade labelFacade;

  public LabelFacade getLabelFacade() {
    return labelFacade;
  }

  public void setLabelFacade(LabelFacade labelFacade) {
    this.labelFacade = labelFacade;
  }

  /*** Stub methods. Invoked by 'InvokerAspect' directly on the 'target'. ***/
  public Object load(LoadLabelVO cmd, HttpServletRequest request) { return null; }
  public Object edit(EditLabelVO cmd, HttpServletRequest request) { return null; }
  public Object add(AddLabelVO cmd, HttpServletRequest request) { return null; }
  public Object remove(RemoveLabelVO cmd, HttpServletRequest request) { return null; }
  public Object search(SearchLabelsVO cmd, HttpServletRequest request) { return null; }
  public Object filter(FilterVO cmd, HttpServletRequest request) { return null; }
}