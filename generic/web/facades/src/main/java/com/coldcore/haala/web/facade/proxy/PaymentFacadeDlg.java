package com.coldcore.haala.web.facade.proxy;

import com.coldcore.haala.web.facade.annotations.AopFacade;
import com.coldcore.haala.web.facade.annotations.TargetClass;
import com.coldcore.haala.web.facade.logic.PaymentFacade;
import com.coldcore.haala.web.facade.vo.cmd.*;
import com.coldcore.haala.web.facade.vo.cmd.payment.*;

import javax.servlet.http.HttpServletRequest;

/** AOP proxy delegate */
@AopFacade
public class PaymentFacadeDlg {

  @TargetClass private PaymentFacade paymentFacade;

  public PaymentFacade getPaymentFacade() {
    return paymentFacade;
  }

  public void setPaymentFacade(PaymentFacade paymentFacade) {
    this.paymentFacade = paymentFacade;
  }

  /*** Stub methods. Invoked by 'InvokerAspect' directly on the 'target'. ***/
  public Object create(CreatePaymentVO cmd, HttpServletRequest request) { return null; }
  public Object push(LoadVO cmd, HttpServletRequest request) { return null; }
  public Object search(SearchVO cmd, HttpServletRequest request) { return null; }
  public Object filter(FilterVO cmd, HttpServletRequest request) { return null; }
}
