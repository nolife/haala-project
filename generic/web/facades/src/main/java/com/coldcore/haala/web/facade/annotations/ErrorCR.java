package com.coldcore.haala.web.facade.annotations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/** Create an erroneous CallResult for supplied "on" exception.
 *  Exceptions can be declared though this annotation on a facade method and handled by an aspect.
 */
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
public @interface ErrorCR {
    String key() default "";
    int code() default 0;
    String log() default "";
    Class<? extends Throwable> on();
    String result() default "";
}
