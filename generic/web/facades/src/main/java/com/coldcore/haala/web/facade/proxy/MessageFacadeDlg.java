package com.coldcore.haala.web.facade.proxy;

import com.coldcore.haala.web.facade.annotations.AopFacade;
import com.coldcore.haala.web.facade.annotations.TargetClass;
import com.coldcore.haala.web.facade.logic.MessageFacade;
import com.coldcore.haala.web.facade.vo.cmd.*;
import com.coldcore.haala.web.facade.vo.cmd.message.*;

import javax.servlet.http.HttpServletRequest;

/** AOP proxy delegate */
@AopFacade
public class MessageFacadeDlg {

  @TargetClass private MessageFacade messageFacade;

  public MessageFacade getMessageFacade() {
    return messageFacade;
  }

  public void setMessageFacade(MessageFacade messageFacade) {
    this.messageFacade = messageFacade;
  }

  /*** Stub methods. Invoked by 'InvokerAspect' directly on the 'target'. ***/
  public Object feedback(FeedbackVO cmd, HttpServletRequest request) { return null; }
  public Object search(SearchVO cmd, HttpServletRequest request) { return null; }
  public Object asearch(SearchVO cmd, HttpServletRequest request) { return null; }
  public Object setup(TargetVO cmd, HttpServletRequest request) { return null; }
  public Object asetup(TargetVO cmd, HttpServletRequest request) { return null; }
  public Object remove(TargetVO cmd, HttpServletRequest request) { return null; }
  public Object aremove(TargetVO cmd, HttpServletRequest request) { return null; }
}