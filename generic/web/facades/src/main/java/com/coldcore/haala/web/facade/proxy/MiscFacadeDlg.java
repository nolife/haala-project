package com.coldcore.haala.web.facade.proxy;

import com.coldcore.haala.web.facade.annotations.AopFacade;
import com.coldcore.haala.web.facade.annotations.TargetClass;
import com.coldcore.haala.web.facade.logic.MiscFacade;
import com.coldcore.haala.web.facade.vo.cmd.*;

import javax.servlet.http.HttpServletRequest;

/** AOP proxy delegate */
@AopFacade
public class MiscFacadeDlg {

  @TargetClass private MiscFacade miscFacade;

  public MiscFacade getMiscFacade() {
    return miscFacade;
  }

  public void setMiscFacade(MiscFacade miscFacade) {
    this.miscFacade = miscFacade;
  }

  /*** Stub methods. Invoked by 'InvokerAspect' directly on the 'target'. ***/
  public Object changeSite(String site, HttpServletRequest request) { return null; }
  public Object changeCurrency(String currency, HttpServletRequest request) { return null; }
  public Object uploadStatus(String noprm, HttpServletRequest request) { return null; }
  public Object params(ParamsVO cmd, HttpServletRequest request) { return null; }
  public Object favs(SearchVO cmd, HttpServletRequest request) { return null; }
  public Object turingPath(String noprm, HttpServletRequest request) { return null; }
}
