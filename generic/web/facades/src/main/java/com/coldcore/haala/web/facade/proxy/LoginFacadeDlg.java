package com.coldcore.haala.web.facade.proxy;

import com.coldcore.haala.web.facade.annotations.AopFacade;
import com.coldcore.haala.web.facade.annotations.TargetClass;
import com.coldcore.haala.web.facade.logic.LoginFacade;
import com.coldcore.haala.web.facade.vo.cmd.*;
import com.coldcore.haala.web.facade.vo.cmd.user.*;

import javax.servlet.http.HttpServletRequest;

/** AOP proxy delegate */
@AopFacade
public class LoginFacadeDlg {

  @TargetClass private LoginFacade loginFacade;

  public LoginFacade getLoginFacade() {
    return loginFacade;
  }

  public void setLoginFacade(LoginFacade loginFacade) {
    this.loginFacade = loginFacade;
  }

  /*** Stub methods. Invoked by 'InvokerAspect' directly on the 'target'. ***/
  public Object login(LoginVO cmd, HttpServletRequest request) { return null; }
  public Object login2(Login2VO cmd, HttpServletRequest request) { return null; }
  public Object logout(String noprm, HttpServletRequest request) { return null; }
  public Object shellOn(ShellVO cmd, HttpServletRequest request) { return null; }
  public Object shellOff(String noprm, HttpServletRequest request) { return null; }
  public Object remindPassword(ReminderVO cmd, HttpServletRequest request) { return null; }
  public Object fbLogin(TokenVO cmd, HttpServletRequest request) { return null; }
}
