package com.coldcore.haala.web.facade.proxy;

import com.coldcore.haala.web.facade.annotations.AopFacade;
import com.coldcore.haala.web.facade.annotations.TargetClass;
import com.coldcore.haala.web.facade.logic.SettingFacade;
import com.coldcore.haala.web.facade.vo.cmd.*;
import com.coldcore.haala.web.facade.vo.cmd.cms.*;

import javax.servlet.http.HttpServletRequest;

/** AOP proxy delegate */
@AopFacade
public class SettingFacadeDlg {

  @TargetClass private SettingFacade settingFacade;

  public SettingFacade getSettingFacade() {
    return settingFacade;
  }

  public void setSettingFacade(SettingFacade settingFacade) {
    this.settingFacade = settingFacade;
  }

  /*** Stub methods. Invoked by 'InvokerAspect' directly on the 'target'. ***/
  public Object load(LoadSettingVO cmd, HttpServletRequest request) { return null; }
  public Object edit(EditSettingVO cmd, HttpServletRequest request) { return null; }
  public Object add(AddSettingVO cmd, HttpServletRequest request) { return null; }
  public Object remove(RemoveSettingVO cmd, HttpServletRequest request) { return null; }
  public Object search(SearchSettingsVO cmd, HttpServletRequest request) { return null; }
  public Object filter(FilterVO cmd, HttpServletRequest request) { return null; }
}
