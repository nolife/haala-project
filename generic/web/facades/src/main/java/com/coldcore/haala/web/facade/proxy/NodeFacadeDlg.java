package com.coldcore.haala.web.facade.proxy;

import com.coldcore.haala.web.facade.annotations.AopFacade;
import com.coldcore.haala.web.facade.annotations.TargetClass;
import com.coldcore.haala.web.facade.logic.NodeFacade;
import com.coldcore.haala.web.facade.vo.cmd.LoadVO;

import javax.servlet.http.HttpServletRequest;

/** AOP proxy delegate */
@AopFacade
public class NodeFacadeDlg {

  @TargetClass private NodeFacade nodeFacade;

  public NodeFacade getNodeFacade() {
    return nodeFacade;
  }

  public void setNodeFacade(NodeFacade nodeFacade) {
    this.nodeFacade = nodeFacade;
  }

  /*** Stub methods. Invoked by 'InvokerAspect' directly on the 'target'. ***/
  public Object sendEmail(LoadVO cmd, HttpServletRequest request) { return null; }
}
