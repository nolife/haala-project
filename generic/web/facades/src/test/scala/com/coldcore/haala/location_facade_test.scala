package com.coldcore.haala
package web
package facade.logic

import org.scalatest.{BeforeAndAfter, FlatSpec, Matchers}
import org.scalatest.mockito.MockitoSugar
import org.mockito.Mockito._
import org.mockito.ArgumentMatchers._
import org.mockito.ArgumentCaptor
import facade.vo.cmd.GeocodeAddressVO
import org.junit.runner.RunWith
import org.scalatest.junit.JUnitRunner
import service.LocationService.GeocodeResult

@RunWith(classOf[JUnitRunner])
class LocationFacadeSpec extends FlatSpec with MockitoSugar with BeforeAndAfter with Matchers {
  var m: MyMock = _

  before {
    m = new MyMock
  }

  class MyMock extends com.coldcore.haala.web.test.MyMock {
    val facade = new LocationFacade |< { x => x.serviceContainer = serviceContainer }
  }

  it should "geocode" in {
    val cmd = new GeocodeAddressVO { line1 = "12 Hydepark Corner"; line2 = ""; line3 = ""; line4 = ""; line5 = "";
      postcode = "W1K 7TY"; town = "London"; country = "GB" }
    when(m.locationService.geocode("12 Hydepark Corner, London, W1K 7TY, GB")).thenReturn(
      GeocodeResult(374229181000000000L, -1220854212000000000L))
    val cr = m.facade.geocode(cmd, m.request)
    assertResult(false) { cr.failed }
    assertResult(Map('lat -> "37.4229181000000000", 'lng -> "-122.0854212000000000", 'type -> 0)) { cr.result }
  }

}
