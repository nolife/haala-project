package com.coldcore.haala
package web
package facade

import org.scalatest.{BeforeAndAfter, FlatSpec, Matchers}
import annotations.{DatatypeInput, Input, TransformInput}
import domain.{User, Label, Setting, File, Pack, BaseObject, BasePackObject}
import core.WebConstants
import vo.cmd.user.{ChangePasswordVO, EditDetailsVO}
import vo.CallResult
import beans.BeanProperty
import validator.{BaseValidator, CmsValidator, CommonChecks, UserValidator}
import validator.Validator._
import org.junit.runner.RunWith
import org.scalatest.mockito.MockitoSugar
import org.mockito.Mockito._
import org.mockito.ArgumentMatchers._
import org.mockito.ArgumentCaptor
import org.scalatest.junit.JUnitRunner
import com.coldcore.haala.core.SiteChain.Implicits._
import com.coldcore.haala.core.SiteState
import test.MyMock

@RunWith(classOf[JUnitRunner])
class BaseValidatorSpec extends FlatSpec with MockitoSugar with BeforeAndAfter with Matchers {
  var m: MyMock = _

  before {
    m = new MyMock
  }

  class Missile {
    @BeanProperty @Input (required = true, transform = "float") var fuel = " 100,23 " // simple 100.23
    @BeanProperty @Input (required = true, transform = "bool") var explosive = " yes " // boolean true
    @BeanProperty @Input (datatype = "numbs") var numbers = " 123 " // regex [0-9]{1,10}
    @BeanProperty @Input (required = true, transform = "upper") var uppers = Array(" foo ", " bar ", " ") //transform array to uppercase and remove empty items
    @BeanProperty @Input var wings = Array(new Wing{color = " white "}, new Wing{color = " red "}) //descent into objects
    @BeanProperty @Input var module: Module = _ //for extended validator
    var voltage = " High Voltage " //not annotated field will not be touched
  }
  
  class Wing {
    @BeanProperty @Input var color = " black "
  }

  class Module {
    @BeanProperty @Input (transform = "to01", datatype = "digits01") var switch = " yes "
  }

  class ExtendedValidator extends BaseValidator {
    override def validate(context: Context): Boolean = context.obj match {
      case _: Missile =>
        context.fieldValidator("fuel", ignoreFieldValidator)
        context.fieldValidator("uppers", ignoreFieldValidator)
        super.validate(context)
      case _ =>
        super.validate(context)
    }

    @TransformInput("to01")
    def transformTo01(field: Field, context: Context): String =
      if (field.strval == "yes") "1" else if (field.strval == "no") "0" else field.strval

    @DatatypeInput("digits01")
    def checkDigits01(field: Field, context: Context): Boolean =
      field.strval == "0" || field.strval == "1"
  }

  it should "transform and validate fields in an object" in {
    val ss = m.settingService
    val (v, ev, cr, o, md) =
      (new BaseValidator { serviceContainer = m.serviceContainer },
        new ExtendedValidator { serviceContainer = m.serviceContainer },
        new CallResult, new Missile, new Module)

    when(ss.querySystem("RegEx/numbs")).thenReturn("[0-9]{1,10}")

    def f = v.validate(Context(o, cr, m.request))
    def ef = ev.validate(Context(o, cr, m.request))

    // transform

    f shouldBe true
    o.fuel shouldBe "100.23"
    o.explosive shouldBe "1"
    o.voltage shouldBe " High Voltage "
    (o.wings(0).color, o.wings(1).color) shouldBe ("white", "red")
    (o.uppers(0), o.uppers(1)) shouldBe ("FOO", "BAR")

    // validation

    o.numbers = "abc" //invalid value
    f shouldBe false
    o.numbers = "123" //ensure validated once again
    f shouldBe true

    o.fuel = ""//required field
    f shouldBe false
    ef shouldBe true
    o.fuel = "100" //ensure validated once again
    f shouldBe true

    o.uppers = Array() //required field (array)
    f shouldBe false
    ef shouldBe true
    o.uppers = Array("", "", "")
    f shouldBe false
    ef shouldBe true
    o.uppers = Array("", "foo", "") //ensure validated once again
    f shouldBe true

    o.module = md //extended checks
    intercept[NoSuchMethodException] { f } // transformTo01
    ef shouldBe true
    md.switch = "no" //valid value
    ef shouldBe true
    md.switch = "maybe" //invalid value
    ef shouldBe false

    // nulls and empty values

    o.module = null
    f shouldBe true
    o.module shouldBe null
    o.numbers = ""
    f shouldBe true
    o.numbers shouldBe ""
    o.numbers = null
    f shouldBe true
    o.numbers shouldBe ""
  }

}

@RunWith(classOf[JUnitRunner])
class UserValidatorSpec extends FlatSpec with MockitoSugar with BeforeAndAfter with Matchers {
  var m: MyMock = _

  before {
    m = new MyMock
  }

  class MyMock extends com.coldcore.haala.web.test.MyMock {
    val validator = new UserValidator |< { x => x.serviceContainer = serviceContainer }

    request.xuser.state.currentUser = Some(new User { password = "secret" })
    request.xattr.perm + (WebConstants.SITE_STATE -> siteState)

    when(labelService.query("fcall.erpas", "xx1.mc1")).thenReturn("Invalid password!")
    when(settingService.querySystem("RegEx/password")).thenReturn(".{5,100}")
    when(settingService.querySystem("RegEx/username")).thenReturn("[a-zA-Z][0-9a-zA-Z_]{4,99}")
    when(settingService.querySystem("RegEx/email")).thenReturn("([a-zA-Z0-9_\\-\\.]+)@((\\[[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.)|(([a-zA-Z0-9\\-]+\\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\\]?) .{5,100}")
    when(settingService.querySystem("RegEx/name")).thenReturn(".{1,15}")
  }

  val siteState =
    SiteState(
      currentDomainSite = "mc", currentSite = "xx1.mc1",
      currentDomain = "", currentDomainId = 0, currentDomainType = "", currentDomainStyle = "")

  val editVO =
    new EditDetailsVO { password = "secret"; username = "myname"; email = "foo@foo.foo"
                        salutation = "mr"; firstName = "firstName"; lastName = "lastName"; primaryNumber = "020" }

  def validate(cmd: AnyRef): Boolean = {
    val cr = new CallResult
    m.validator.validate(Context(cmd, cr, m.request))
    !cr.failed
  }

  "change password" should "fail on invalid original password" in {
    val cr = new CallResult
    m.validator.validate(Context(new ChangePasswordVO { oldPassword = "foo"; password = "bar" }, cr, m.request))
    (cr.failed, cr.errorsText(0)) shouldBe (true, "Invalid password!")

    validate(new ChangePasswordVO { oldPassword = "secret"; password = "myPass" }) shouldBe true
  }

  "change password" should "fail on invalid or empty new password" in {
    validate(new ChangePasswordVO { oldPassword = "secret" }) shouldBe false
    validate(new ChangePasswordVO { oldPassword = "secret"; password = "bar" }) shouldBe false
  }

  "edit details" should "check password is correct" in {
    val cr = new CallResult
    m.validator.validate(Context(new EditDetailsVO { password = "myPass" }, cr, m.request))
    (cr.failed, cr.errorsText(0)) shouldBe (true, "Invalid password!")

    validate(editVO) shouldBe true
  }

  "edit details" should "validate email" in {
    val x = (email: String) => { editVO.email = email; validate(editVO) }

    x(" a@b.cc       ") shouldBe true
    x(" a-z@b-z.cc   ") shouldBe true
    x(" a-z@b.co.uk  ") shouldBe true
    x(" info@dot.net ") shouldBe true

    x(" foo             ") shouldBe false
    x(" foo@bar         ") shouldBe false
    x(" a@b.c           ") shouldBe false
    x(" info@dot.london ") shouldBe false
  }

}

@RunWith(classOf[JUnitRunner])
class CmsValidatorSpec extends FlatSpec with MockitoSugar with BeforeAndAfter with Matchers {
  var m: MyMock = _

  before {
    m = new MyMock
  }

  class MyMock extends com.coldcore.haala.web.test.MyMock {
    val validator = new CmsValidator |< { x => x.serviceContainer = serviceContainer }

    request.xuser.state.currentUserId = 11

    when(labelService.getById(4)).thenReturn(Some(new Label { site = "mc1" }))
    when(settingService.getById(4)).thenReturn(Some(new Setting { site = "mc1" }))
    when(fileService.getById(4)).thenReturn(Some(new File { site = "mc1" }))

    when(userService.isSiteOwner(11, "mc1")).thenReturn(false, true) // fail, pass
    when(userService.isDomainOwner(11, 4)).thenReturn(false, true)   // ...
  }

  def validate(cmd: AnyRef): Boolean = {
    val cr = new CallResult
    m.validator.validate(Context(cmd, cr, m.request))
    !cr.failed
  }

  it should "check owner for site" in {
    class VO { @BeanProperty @Input (datatype = "owner") var site: String = "mc1" }
    validate(new VO) shouldBe false
    validate(new VO) shouldBe true
  }

  it should "check owner for label" in {
    class VO { @BeanProperty @Input (datatype = "owner") var labelId: String = "4" }
    validate(new VO) shouldBe false
    validate(new VO) shouldBe true
  }

  it should "check owner for setting" in {
    class VO { @BeanProperty @Input (datatype = "owner") var settingId: String = "4" }
    validate(new VO) shouldBe false
    validate(new VO) shouldBe true
  }

  it should "check owner for file" in {
    class VO { @BeanProperty @Input (datatype = "owner") var fileId: String = "4" }
    validate(new VO) shouldBe false
    validate(new VO) shouldBe true
  }

  it should "check owner for domain" in {
    class VO { @BeanProperty @Input (datatype = "owner") var domainId: String = "4" }
    validate(new VO) shouldBe false
    validate(new VO) shouldBe true
  }

}

@RunWith(classOf[JUnitRunner])
class CommonChecksSpec extends FlatSpec with MockitoSugar with BeforeAndAfter with Matchers {
  var m: MyMock = _

  before {
    m = new MyMock
  }

  class MyMock extends com.coldcore.haala.web.test.MyMock {
    class CustomValidator extends BaseValidator with CommonChecks
    val validator = new CustomValidator |< { x => x.serviceContainer = serviceContainer }

    request.xuser.state.currentUserId = 11

    when(userService.isDomainOwner(11, 4)).thenReturn(false, true) // fail, pass
    when(userService.isDomainOwner(11, 5)).thenReturn(false, true) // ...
  }

  "isDomainsOwner" should "check if user owns domain" in {
    val context = Context(null, null, m.request)
    m.validator.isDomainsOwner("4", context) shouldBe false
    m.validator.isDomainsOwner("4", context) shouldBe true
    m.validator.isDomainsOwner("4, 5", context) shouldBe false
    m.validator.isDomainsOwner("4, 5", context) shouldBe true
  }

  "isOwnerOf" should "check if user owns entity object" in {
    val context = Context(null, null, m.request)
    val u = new User

    class MyPack extends Pack { pack = "my" }
    class Entity extends BaseObject with BasePackObject[MyPack] { pack = new MyPack { user = u }}

    val findById = (id: Long) => if (id == 4 || id == 5) Some(new Entity) else None

    u.id = 12L
    m.validator.isOwnerOf("4", context, findById, "entity") shouldBe false

    u.id = 11L
    m.validator.isOwnerOf("4", context, findById, "entity") shouldBe true

    m.validator.isOwnerOf("4, 5", context, findById, "entity") shouldBe true
    m.validator.isOwnerOf("4, 6", context, findById, "entity") shouldBe false
  }

}
