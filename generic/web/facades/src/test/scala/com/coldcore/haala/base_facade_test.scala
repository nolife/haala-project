package com.coldcore.haala
package web
package facade

import org.junit.runner.RunWith
import org.scalatest.junit.JUnitRunner
import org.scalatest.{BeforeAndAfter, WordSpec}
import org.scalatest.mockito.MockitoSugar
import com.coldcore.haala.web.facade.logic.BaseFacade
import com.coldcore.haala.web.facade.vo.cmd.SearchVO
import com.coldcore.haala.web.facade.logic.BaseFacade.SortMapping

@RunWith(classOf[JUnitRunner])
class BaseFacadeSpec extends WordSpec with MockitoSugar with BeforeAndAfter {
  var m: MyMock = _

  before {
    m = new MyMock
  }

  class MyMock extends com.coldcore.haala.web.test.MyMock {
    val facade = new BaseFacade
  }

  "orderBy" should {
    "construct proper mapping to database fields" in {
      val mapping = SortMapping("sit", "site") :: SortMapping("key") :: SortMapping("typ", "type", ascending = false) :: Nil
      assertResult("site desc, id desc") { m.facade.orderBy(new SearchVO { sortBy = "sit"; sortType = "desc" }, mapping: _*) }
      assertResult("id desc") { m.facade.orderBy(new SearchVO { sortBy = "unk"; sortType = "asc" }, mapping: _*) }
    }
    "support + - for asc and desc order" in {
      val mapping = SortMapping("sit", "site") :: SortMapping("key") :: Nil
      assertResult("site asc, key desc, id desc") { m.facade.orderBy(new SearchVO { sortBy = "+sit, -key" }, mapping: _*) }
      assertResult("site asc, key desc, id desc") { m.facade.orderBy(new SearchVO { sortBy = "sit+, key-" }, mapping: _*) }
      assertResult("site desc, key asc, id desc") { m.facade.orderBy(new SearchVO { sortBy = "-sit, key+" }, mapping: _*) }
      assertResult("site desc, key asc, id desc") { m.facade.orderBy(new SearchVO { sortBy = "-sit  key+" }, mapping: _*) }
    }
    "support + - ordering with sortType present" in {
      val mapping = SortMapping("sit", "site") :: SortMapping("key") :: Nil
      assertResult("site desc, key asc, id desc") { m.facade.orderBy(new SearchVO { sortBy = "-sit key"; sortType = "asc" }, mapping: _*) }
      assertResult("site desc, key desc, id desc") { m.facade.orderBy(new SearchVO { sortBy = "-sit key"; sortType = "0" }, mapping: _*) }
    }
  }

}
