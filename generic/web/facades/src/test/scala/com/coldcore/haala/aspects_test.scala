package com.coldcore.haala
package web
package facade

import javax.servlet.http.HttpServletRequest

import service.exceptions.{InvalidEmailException, ObjectNotFoundException, UserDisabledException, UserNoAgreementException}
import core.WebConstants
import facade.annotations.{ErrorCR, ErrorCRs, LogClass, TargetClass}
import facade.aspect.ErroneousAspect
import facade.vo.CallResult
import facade.vo.cmd.user.LoginVO
import org.aspectj.lang.{ProceedingJoinPoint, Signature}
import org.junit.runner.RunWith
import org.scalatest._
import org.scalatest.junit.JUnitRunner
import org.scalatest.mockito.MockitoSugar
import org.mockito.Mockito._
import org.mockito.ArgumentMatchers._
import WebConstants._
import com.coldcore.haala.core.{NamedLog, SLF4J, SiteState}
import com.coldcore.haala.core.SiteChain.Implicits._
import scala.beans.BeanProperty


@RunWith(classOf[JUnitRunner])
class ErrorResponseAspectSpec extends FlatSpec with MockitoSugar with BeforeAndAfter with Matchers {
  var m: MyMock = _

  before {
    m = new MyMock
  }

  class DummyFacadeDlg(target: DummyFacade) {
    @BeanProperty @TargetClass val dummyFacade: DummyFacade = target
  }

  class DummyFacade(log: SLF4J) {
    @BeanProperty @LogClass val LOG = new NamedLog("login-facade", log)

    @ErrorCRs(Array(
      new ErrorCR(on = classOf[ObjectNotFoundException], key = "x.nousr", log = "User {username} not found"),
      new ErrorCR(on = classOf[UserDisabledException], result = "{e.ref}", key = "User {a.username} disabled!", log = "User {username} is disabled"),
      new ErrorCR(on = classOf[UserNoAgreementException], result = "{ref}", code = FACADE_ERROR_USER_AGREEMENT, log = "User {username} not accepted terms")
    ))
    def login(cmd: LoginVO, request: HttpServletRequest): CallResult = ???

    @ErrorCR(on = classOf[ObjectNotFoundException], key = "Not logged in!")
    def logout(cmd: String, request: HttpServletRequest): CallResult = ???
  }

  class NamedSignature(val name: String) extends Signature {
    override def getName: String = name
    override def getModifiers: Int = ???
    override def getDeclaringTypeName: String = ???
    override def toShortString: String = ???
    override def toLongString: String = ???
    override def getDeclaringType: JClass[_] = ???
  }

  class MyMock extends com.coldcore.haala.web.test.MyMock {
    val log = mock[SLF4J]
    val aspect = new ErroneousAspect
    val facade = new DummyFacade(log)
    val facadeDlg = new DummyFacadeDlg(facade)

    // call login method as username "Wilson"
    val pjp = mock[ProceedingJoinPoint]
    when(pjp.getSignature).thenReturn(new NamedSignature("login"))
    when(pjp.getTarget).thenReturn(facade, facade)
    when(pjp.getArgs).thenReturn(Array(new LoginVO { username = "Wilson" }), null)
    when(pjp.proceed).thenReturn(new CallResult, null)

    val siteState = SiteState(
      currentDomainSite = "", currentSite = "xx1.mc1",
      currentDomain = "", currentDomainId = 0, currentDomainType = "", currentDomainStyle = "")
    request.xattr.perm + (SITE_STATE -> siteState)

    when(labelService.query("x.nousr", "xx1.mc1")).thenReturn("User {username} not found!")
  }

  it should "create CallResult matching ErrorCR exception (error text queried)" in {
    when(m.pjp.proceed).thenThrow(new ObjectNotFoundException)

    val cr = m.aspect.around(m.pjp, m.request).asInstanceOf[CallResult]
    cr.errorsText should contain ("User Wilson not found!")
  }

  it should "create CallResult matching ErrorCR exception (error text with result)" in {
    when(m.pjp.proceed).thenThrow(new UserDisabledException("disabled/ABC"))

    val cr = m.aspect.around(m.pjp, m.request).asInstanceOf[CallResult]
    cr.errorsText should contain ("User Wilson disabled!")
    cr.result shouldBe "disabled/ABC"
  }

  it should "create CallResult matching ErrorCR exception (error code with result)" in {
    when(m.pjp.proceed).thenThrow(new UserNoAgreementException("terms/ABC"))

    val cr = m.aspect.around(m.pjp, m.request).asInstanceOf[CallResult]
    cr.errorCodes should contain (FACADE_ERROR_USER_AGREEMENT)
    cr.result shouldBe "terms/ABC"
  }

  it should "create CallResult on a single ErrorCR annotation" in {
    when(m.pjp.getSignature).thenReturn(new NamedSignature("logout"))
    when(m.pjp.proceed).thenThrow(new ObjectNotFoundException)

    val cr = m.aspect.around(m.pjp, m.request).asInstanceOf[CallResult]
    cr.errorsText should contain ("Not logged in!")
  }

  it should "do not handle non matching ErrorCR exceptions" in {
    when(m.pjp.proceed).thenThrow(new InvalidEmailException)

    an [InvalidEmailException] should be thrownBy m.aspect.around(m.pjp, m.request)
  }

  it should "log ErrorCR error message" in {
    when(m.pjp.proceed).thenThrow(new ObjectNotFoundException)

    m.aspect.around(m.pjp, m.request)
    verify(m.log).info("[login-facade] User Wilson not found")
  }

  it should "create CallResult matching ErrorCR exception via a delegate" in {
    when(m.pjp.getTarget).thenReturn(m.facadeDlg, m.facadeDlg)
    when(m.pjp.proceed).thenThrow(new ObjectNotFoundException)

    val cr = m.aspect.around(m.pjp, m.request).asInstanceOf[CallResult]
    cr.errorsText should contain ("User Wilson not found!")
  }

}
