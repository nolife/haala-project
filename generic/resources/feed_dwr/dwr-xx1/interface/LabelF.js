if (typeof dwr == 'undefined' || dwr.engine == undefined) throw new Error('You must include DWR engine before including this file');

(function() {
  if (dwr.engine._getObject("LabelF") == undefined) {
    var p;

    p = {};
    p._path = '/dwr';

    p.add = function(p0, callback) {
      return dwr.engine._execute(p._path, 'LabelF', 'add', arguments);
    };

    p.remove = function(p0, callback) {
      return dwr.engine._execute(p._path, 'LabelF', 'remove', arguments);
    };

    p.load = function(p0, callback) {
      return dwr.engine._execute(p._path, 'LabelF', 'load', arguments);
    };

    p.search = function(p0, callback) {
      return dwr.engine._execute(p._path, 'LabelF', 'search', arguments);
    };

    p.filter = function(p0, callback) {
      return dwr.engine._execute(p._path, 'LabelF', 'filter', arguments);
    };

    p.edit = function(p0, callback) {
      return dwr.engine._execute(p._path, 'LabelF', 'edit', arguments);
    };

    dwr.engine._setObject("LabelF", p);
  }
})();
