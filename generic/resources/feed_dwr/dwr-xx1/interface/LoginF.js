if (typeof dwr == 'undefined' || dwr.engine == undefined) throw new Error('You must include DWR engine before including this file');

(function() {
  if (dwr.engine._getObject("LoginF") == undefined) {
    var p;

    p = {};
    p._path = '/dwr';

    p.login = function(p0, callback) {
      return dwr.engine._execute(p._path, 'LoginF', 'login', arguments);
    };

    p.login2 = function(p0, callback) {
      return dwr.engine._execute(p._path, 'LoginF', 'login2', arguments);
    };

    p.logout = function(p0, callback) {
      return dwr.engine._execute(p._path, 'LoginF', 'logout', arguments);
    };

    p.remindPassword = function(p0, callback) {
      return dwr.engine._execute(p._path, 'LoginF', 'remindPassword', arguments);
    };

    p.shellOn = function(p0, callback) {
      return dwr.engine._execute(p._path, 'LoginF', 'shellOn', arguments);
    };

    p.shellOff = function(p0, callback) {
      return dwr.engine._execute(p._path, 'LoginF', 'shellOff', arguments);
    };

    p.fbLogin = function(p0, callback) {
      return dwr.engine._execute(p._path, 'LoginF', 'fbLogin', arguments);
    };

    dwr.engine._setObject("LoginF", p);
  }
})();
