if (typeof dwr == 'undefined' || dwr.engine == undefined) throw new Error('You must include DWR engine before including this file');

(function() {
  if (dwr.engine._getObject("AbyssF") == undefined) {
    var p;

    p = {};
    p._path = '/dwr';

    p.add = function(p0, callback) {
      return dwr.engine._execute(p._path, 'AbyssF', 'add', arguments);
    };

    p.edit = function(p0, callback) {
      return dwr.engine._execute(p._path, 'AbyssF', 'edit', arguments);
    };

    p.load = function(p0, callback) {
      return dwr.engine._execute(p._path, 'AbyssF', 'load', arguments);
    };

    p.search = function(p0, callback) {
      return dwr.engine._execute(p._path, 'AbyssF', 'search', arguments);
    };

    dwr.engine._setObject("AbyssF", p);
  }
})();
