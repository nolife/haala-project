if (typeof dwr == 'undefined' || dwr.engine == undefined) throw new Error('You must include DWR engine before including this file');

(function() {
  if (dwr.engine._getObject("UserF") == undefined) {
    var p;

    p = {};
    p._path = '/dwr';

    p.register = function(p0, callback) {
      return dwr.engine._execute(p._path, 'UserF', 'register', arguments);
    };

    p.search = function(p0, callback) {
      return dwr.engine._execute(p._path, 'UserF', 'search', arguments);
    };

    p.remindPassword = function(p0, callback) {
      return dwr.engine._execute(p._path, 'UserF', 'remindPassword', arguments);
    };

    p.usernameTaken = function(p0, callback) {
      return dwr.engine._execute(p._path, 'UserF', 'usernameTaken', arguments);
    };

    p.editDetails = function(p0, callback) {
      return dwr.engine._execute(p._path, 'UserF', 'editDetails', arguments);
    };

    p.credit = function(p0, callback) {
      return dwr.engine._execute(p._path, 'UserF', 'credit', arguments);
    };

    p.changePassword = function(p0, callback) {
      return dwr.engine._execute(p._path, 'UserF', 'changePassword', arguments);
    };

    p.acceptAgreement = function(p0, callback) {
      return dwr.engine._execute(p._path, 'UserF', 'acceptAgreement', arguments);
    };

    p.editProps = function(p0, callback) {
      return dwr.engine._execute(p._path, 'UserF', 'editProps', arguments);
    };

    dwr.engine._setObject("UserF", p);
  }
})();
