if (typeof dwr == 'undefined' || dwr.engine == undefined) throw new Error('You must include DWR engine before including this file');

(function() {
  if (dwr.engine._getObject("SettingF") == undefined) {
    var p;

    p = {};
    p._path = '/dwr';

    p.add = function(p0, callback) {
      return dwr.engine._execute(p._path, 'SettingF', 'add', arguments);
    };

    p.remove = function(p0, callback) {
      return dwr.engine._execute(p._path, 'SettingF', 'remove', arguments);
    };

    p.load = function(p0, callback) {
      return dwr.engine._execute(p._path, 'SettingF', 'load', arguments);
    };

    p.search = function(p0, callback) {
      return dwr.engine._execute(p._path, 'SettingF', 'search', arguments);
    };

    p.filter = function(p0, callback) {
      return dwr.engine._execute(p._path, 'SettingF', 'filter', arguments);
    };

    p.edit = function(p0, callback) {
      return dwr.engine._execute(p._path, 'SettingF', 'edit', arguments);
    };

    dwr.engine._setObject("SettingF", p);
  }
})();
