if (typeof dwr == 'undefined' || dwr.engine == undefined) throw new Error('You must include DWR engine before including this file');

(function() {
  if (dwr.engine._getObject("MessageF") == undefined) {
    var p;

    p = {};
    p._path = '/dwr';

    p.remove = function(p0, callback) {
      return dwr.engine._execute(p._path, 'MessageF', 'remove', arguments);
    };

    p.setup = function(p0, callback) {
      return dwr.engine._execute(p._path, 'MessageF', 'setup', arguments);
    };

    p.search = function(p0, callback) {
      return dwr.engine._execute(p._path, 'MessageF', 'search', arguments);
    };

    p.asearch = function(p0, callback) {
      return dwr.engine._execute(p._path, 'MessageF', 'asearch', arguments);
    };

    p.feedback = function(p0, callback) {
      return dwr.engine._execute(p._path, 'MessageF', 'feedback', arguments);
    };

    p.asetup = function(p0, callback) {
      return dwr.engine._execute(p._path, 'MessageF', 'asetup', arguments);
    };

    p.aremove = function(p0, callback) {
      return dwr.engine._execute(p._path, 'MessageF', 'aremove', arguments);
    };

    dwr.engine._setObject("MessageF", p);
  }
})();
