if (typeof dwr == 'undefined' || dwr.engine == undefined) throw new Error('You must include DWR engine before including this file');

(function() {
  if (dwr.engine._getObject("LocationF") == undefined) {
    var p;

    p = {};
    p._path = '/dwr';

    p.locations = function(p0, callback) {
      return dwr.engine._execute(p._path, 'LocationF', 'locations', arguments);
    };

    p.geocode = function(p0, callback) {
      return dwr.engine._execute(p._path, 'LocationF', 'geocode', arguments);
    };

    dwr.engine._setObject("LocationF", p);
  }
})();
