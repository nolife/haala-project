if (typeof dwr == 'undefined' || dwr.engine == undefined) throw new Error('You must include DWR engine before including this file');

(function() {
  if (dwr.engine._getObject("CmsF") == undefined) {
    var p;

    p = {};
    p._path = '/dwr';

    p.status = function(p0, callback) {
      return dwr.engine._execute(p._path, 'CmsF', 'status', arguments);
    };

    p.loadText = function(p0, callback) {
      return dwr.engine._execute(p._path, 'CmsF', 'loadText', arguments);
    };

    p.editText = function(p0, callback) {
      return dwr.engine._execute(p._path, 'CmsF', 'editText', arguments);
    };

    p.loadPic = function(p0, callback) {
      return dwr.engine._execute(p._path, 'CmsF', 'loadPic', arguments);
    };

    p.editPic = function(p0, callback) {
      return dwr.engine._execute(p._path, 'CmsF', 'editPic', arguments);
    };

    p.loadFile = function(p0, callback) {
      return dwr.engine._execute(p._path, 'CmsF', 'loadFile', arguments);
    };

    p.editFile = function(p0, callback) {
      return dwr.engine._execute(p._path, 'CmsF', 'editFile', arguments);
    };

    p.incVersion = function(p0, callback) {
      return dwr.engine._execute(p._path, 'CmsF', 'incVersion', arguments);
    };

    p.switchOn = function(p0, callback) {
      return dwr.engine._execute(p._path, 'CmsF', 'switchOn', arguments);
    };

    p.loadDomain = function(p0, callback) {
      return dwr.engine._execute(p._path, 'CmsF', 'loadDomain', arguments);
    };

    p.editDomain = function(p0, callback) {
      return dwr.engine._execute(p._path, 'CmsF', 'editDomain', arguments);
    };

    p.editPageMapping = function(p0, callback) {
      return dwr.engine._execute(p._path, 'CmsF', 'editPageMapping', arguments);
    };

    p.addPageMapping = function(p0, callback) {
      return dwr.engine._execute(p._path, 'CmsF', 'addPageMapping', arguments);
    };

    p.removePageMapping = function(p0, callback) {
      return dwr.engine._execute(p._path, 'CmsF', 'removePageMapping', arguments);
    };

    p.searchLabels = function(p0, callback) {
      return dwr.engine._execute(p._path, 'CmsF', 'searchLabels', arguments);
    };

    p.filterLabels = function(p0, callback) {
      return dwr.engine._execute(p._path, 'CmsF', 'filterLabels', arguments);
    };

    p.loadLabel = function(p0, callback) {
      return dwr.engine._execute(p._path, 'CmsF', 'loadLabel', arguments);
    };

    p.editLabel = function(p0, callback) {
      return dwr.engine._execute(p._path, 'CmsF', 'editLabel', arguments);
    };

    p.addLabel = function(p0, callback) {
      return dwr.engine._execute(p._path, 'CmsF', 'addLabel', arguments);
    };

    p.removeLabel = function(p0, callback) {
      return dwr.engine._execute(p._path, 'CmsF', 'removeLabel', arguments);
    };

    p.editPageLabels = function(p0, callback) {
      return dwr.engine._execute(p._path, 'CmsF', 'editPageLabels', arguments);
    };

    p.loadSetting = function(p0, callback) {
      return dwr.engine._execute(p._path, 'CmsF', 'loadSetting', arguments);
    };

    p.editSetting = function(p0, callback) {
      return dwr.engine._execute(p._path, 'CmsF', 'editSetting', arguments);
    };

    p.addSetting = function(p0, callback) {
      return dwr.engine._execute(p._path, 'CmsF', 'addSetting', arguments);
    };

    p.removeSetting = function(p0, callback) {
      return dwr.engine._execute(p._path, 'CmsF', 'removeSetting', arguments);
    };

    p.searchSettings = function(p0, callback) {
      return dwr.engine._execute(p._path, 'CmsF', 'searchSettings', arguments);
    };

    p.filterSettings = function(p0, callback) {
      return dwr.engine._execute(p._path, 'CmsF', 'filterSettings', arguments);
    };

    p.searchFiles = function(p0, callback) {
      return dwr.engine._execute(p._path, 'CmsF', 'searchFiles', arguments);
    };

    p.filterFiles = function(p0, callback) {
      return dwr.engine._execute(p._path, 'CmsF', 'filterFiles', arguments);
    };

    p.loadFile2 = function(p0, callback) {
      return dwr.engine._execute(p._path, 'CmsF', 'loadFile2', arguments);
    };

    p.loadFilePath = function(p0, callback) {
      return dwr.engine._execute(p._path, 'CmsF', 'loadFilePath', arguments);
    };

    p.loadPicture = function(p0, callback) {
      return dwr.engine._execute(p._path, 'CmsF', 'loadPicture', arguments);
    };

    p.removeFile = function(p0, callback) {
      return dwr.engine._execute(p._path, 'CmsF', 'removeFile', arguments);
    };

    p.addFile = function(p0, callback) {
      return dwr.engine._execute(p._path, 'CmsF', 'addFile', arguments);
    };

    p.editFile2 = function(p0, callback) {
      return dwr.engine._execute(p._path, 'CmsF', 'editFile2', arguments);
    };

    p.editTextFile = function(p0, callback) {
      return dwr.engine._execute(p._path, 'CmsF', 'editTextFile', arguments);
    };

    p.loadTextFile = function(p0, callback) {
      return dwr.engine._execute(p._path, 'CmsF', 'loadTextFile', arguments);
    };

    p.backupFile = function(p0, callback) {
      return dwr.engine._execute(p._path, 'CmsF', 'backupFile', arguments);
    };

    dwr.engine._setObject("CmsF", p);
  }
})();
