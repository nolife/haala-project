if (typeof dwr == 'undefined' || dwr.engine == undefined) throw new Error('You must include DWR engine before including this file');

(function() {
  if (dwr.engine._getObject("NodeF") == undefined) {
    var p;

    p = {};
    p._path = '/dwr';

    p.sendEmail = function(p0, callback) {
      return dwr.engine._execute(p._path, 'NodeF', 'sendEmail', arguments);
    };

    dwr.engine._setObject("NodeF", p);
  }
})();
