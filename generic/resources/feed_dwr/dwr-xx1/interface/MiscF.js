if (typeof dwr == 'undefined' || dwr.engine == undefined) throw new Error('You must include DWR engine before including this file');

(function() {
  if (dwr.engine._getObject("MiscF") == undefined) {
    var p;

    p = {};
    p._path = '/dwr';

    p.params = function(p0, callback) {
      return dwr.engine._execute(p._path, 'MiscF', 'params', arguments);
    };

    p.changeSite = function(p0, callback) {
      return dwr.engine._execute(p._path, 'MiscF', 'changeSite', arguments);
    };

    p.changeCurrency = function(p0, callback) {
      return dwr.engine._execute(p._path, 'MiscF', 'changeCurrency', arguments);
    };

    p.uploadStatus = function(p0, callback) {
      return dwr.engine._execute(p._path, 'MiscF', 'uploadStatus', arguments);
    };

    p.turingPath = function(p0, callback) {
      return dwr.engine._execute(p._path, 'MiscF', 'turingPath', arguments);
    };

    p.favs = function(p0, callback) {
      return dwr.engine._execute(p._path, 'MiscF', 'favs', arguments);
    };

    dwr.engine._setObject("MiscF", p);
  }
})();
