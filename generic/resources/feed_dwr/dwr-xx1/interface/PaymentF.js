if (typeof dwr == 'undefined' || dwr.engine == undefined) throw new Error('You must include DWR engine before including this file');

(function() {
  if (dwr.engine._getObject("PaymentF") == undefined) {
    var p;

    p = {};
    p._path = '/dwr';

    p.create = function(p0, callback) {
      return dwr.engine._execute(p._path, 'PaymentF', 'create', arguments);
    };

    p.push = function(p0, callback) {
      return dwr.engine._execute(p._path, 'PaymentF', 'push', arguments);
    };

    p.search = function(p0, callback) {
      return dwr.engine._execute(p._path, 'PaymentF', 'search', arguments);
    };

    p.filter = function(p0, callback) {
      return dwr.engine._execute(p._path, 'PaymentF', 'filter', arguments);
    };

    dwr.engine._setObject("PaymentF", p);
  }
})();
