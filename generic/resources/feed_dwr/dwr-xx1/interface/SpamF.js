if (typeof dwr == 'undefined' || dwr.engine == undefined) throw new Error('You must include DWR engine before including this file');

(function() {
  if (dwr.engine._getObject("SpamF") == undefined) {
    var p;

    p = {};
    p._path = '/dwr';

    p.searchStorages = function(p0, callback) {
      return dwr.engine._execute(p._path, 'SpamF', 'searchStorages', arguments);
    };

    p.createStorage = function(p0, callback) {
      return dwr.engine._execute(p._path, 'SpamF', 'createStorage', arguments);
    };

    p.updateStorage = function(p0, callback) {
      return dwr.engine._execute(p._path, 'SpamF', 'updateStorage', arguments);
    };

    p.removeStorage = function(p0, callback) {
      return dwr.engine._execute(p._path, 'SpamF', 'removeStorage', arguments);
    };

    p.startTask = function(p0, callback) {
      return dwr.engine._execute(p._path, 'SpamF', 'startTask', arguments);
    };

    p.stopTask = function(p0, callback) {
      return dwr.engine._execute(p._path, 'SpamF', 'stopTask', arguments);
    };

    dwr.engine._setObject("SpamF", p);
  }
})();
