/*
  Deprecated. Upgrade version 0.90 to 0.91.
*/

-- Add a new index column and order location countries

alter table locations add column index integer;
update locations set index = 0;
alter table locations alter column index set NOT NULL;

CREATE OR REPLACE FUNCTION tmp_func() RETURNS integer AS $$
DECLARE
    v_row locations%ROWTYPE;
    v_cnt integer := 0;
BEGIN
    for v_row in select * from locations where type=1 order by id
    loop
      v_cnt := v_cnt+1;
      if (v_row.id = 4073) then -- (85) 4064 Romania, (86) gap for Russia, (87) 4073 Samoa
          v_cnt := v_cnt+1;
      end if;
      update locations set index = v_cnt where id = v_row.id;
    end loop;
    return 0;
END;
$$ LANGUAGE plpgsql;

select tmp_func();
DROP FUNCTION tmp_func();

update locations set index = 86 where id = 5437;
