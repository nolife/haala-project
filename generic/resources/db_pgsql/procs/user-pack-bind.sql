/*
  Deprecated. Can be done in the Control Panel.

  Binds a user to a pack.
  Safe to run on users with already assigned packs (does not assign twice).
  Fails if no user/pack exist.
*/

CREATE OR REPLACE FUNCTION tmp_func(p_username varchar, p_pack varchar) RETURNS integer AS $$
DECLARE
    v_tmp bigint := 0;
    v_user_id bigint := 0;
    v_pack_type integer := 0;
    v_pack_tbl varchar := NULL;
BEGIN
    select into v_user_id id from users where username=p_username;
    if not found then
      raise exception 'User % not found', p_username;
    end if;

    if p_pack = 'estate' then
      v_pack_type = 1;
      v_pack_tbl = 'EST_ESTATE_PACKS';
    end if;
    if p_pack = 'minishop' then
      v_pack_type = 2;
      v_pack_tbl = 'MIS_MINISHOP_PACKS';
    end if;
    if v_pack_tbl is NULL then
      raise exception 'Pack % not found', p_pack;
    end if;

    --assign pack to this user
    select into v_tmp user_id from packs where user_id = v_user_id and type = v_pack_type;
    if not found then
      select into v_tmp nextval('PACKS_SEQ');
      INSERT INTO PACKS(ID, TYPE, user_id) VALUES (v_tmp, v_pack_type, v_user_id);
      execute 'INSERT INTO ' || v_pack_tbl || '(ID) VALUES ($1)' using v_tmp;
    end if;

    return 0;
END;
$$ LANGUAGE plpgsql;

select tmp_func('someuser', 'somepack'); -- dummy parameters

DROP FUNCTION tmp_func(varchar, varchar);
