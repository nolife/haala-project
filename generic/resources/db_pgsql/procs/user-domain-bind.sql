/*
  Deprecated. Can be done in the Control Panel.

  Binds a user to a domain and assigns CMS role to her.
  Safe to run on users with already assigned domains/roles (does not assign twice).
  Fails if no user/domain exist.
*/

CREATE OR REPLACE FUNCTION tmp_func(p_username varchar, p_domain varchar) RETURNS integer AS $$
DECLARE
    v_tmp bigint := 0;
    v_user_id bigint := 0;
    v_domain_id bigint := 0;
    v_role_cms bigint := 4; -- CMS role
BEGIN
    select into v_user_id id from users where username=p_username;
    if not found then
      raise exception 'User % not found', p_username;
    end if;

    select into v_domain_id id from domains where domain=p_domain;
    if not found then
      raise exception 'Domain % not found', p_domain;
    end if;

    -- add CMS role
    select into v_tmp user_id from USER_ROLE_MAP where user_id = v_user_id and role_id = v_role_cms;
    if not found then
      INSERT INTO USER_ROLE_MAP(USER_ID, ROLE_ID) VALUES (v_user_id, 4); 
    end if;

    -- bind a user to her domain
    select into v_tmp user_id from USER_DOMAIN_MAP where user_id = v_user_id and domain_id = v_domain_id;
    if not found then
      INSERT INTO USER_DOMAIN_MAP(USER_ID, DOMAIN_ID) VALUES (v_user_id, v_domain_id); 
    end if;

    return 0;
END;
$$ LANGUAGE plpgsql;

select tmp_func('someuser', 'somedomain.my-estate.eu'); -- dummy parameters

DROP FUNCTION tmp_func(varchar, varchar);
