-----------------------------------------------------------
--   DROP
-----------------------------------------------------------

  DROP TABLE settings CASCADE;
  DROP SEQUENCE settings_seq;

  DROP TABLE labels CASCADE;
  DROP SEQUENCE labels_seq;

  DROP TABLE abyss_entries CASCADE;
  DROP SEQUENCE abyss_entries_seq;

  DROP TABLE countries CASCADE;
  DROP SEQUENCE countries_seq;

  DROP TABLE pictures CASCADE;
  DROP SEQUENCE pictures_seq;

  DROP TABLE attachments CASCADE;
  DROP SEQUENCE attachments_seq;

  DROP TABLE files CASCADE;
  DROP SEQUENCE files_seq;

  DROP TABLE loc_addresses CASCADE;
  DROP SEQUENCE loc_addresses_seq;

  DROP TABLE locations CASCADE;
  DROP SEQUENCE locations_seq;

  DROP TABLE postcodes CASCADE;
  DROP SEQUENCE postcodes_seq;

  DROP TABLE packs CASCADE;
  DROP SEQUENCE packs_seq;

  DROP TABLE user_role_map CASCADE;

  DROP TABLE user_domain_map CASCADE;

  DROP TABLE roles CASCADE;
  DROP SEQUENCE roles_seq;

  DROP TABLE users CASCADE;
  DROP SEQUENCE users_seq;

  DROP TABLE addresses CASCADE;
  DROP SEQUENCE addresses_seq;

  DROP TABLE maps CASCADE;
  DROP SEQUENCE maps_seq;

  DROP TABLE domains CASCADE;
  DROP SEQUENCE domains_seq;

  DROP TABLE email_storages CASCADE;
  DROP SEQUENCE email_storages_seq;

  DROP TABLE email_storage_items CASCADE;
  DROP SEQUENCE email_storage_items_seq;

  DROP FUNCTION timestamp_fn();
