-----------------------------------------------------------
--   SETTINGS
-----------------------------------------------------------


insert into SETTINGS (id, key, value, site) values (nextval('SETTINGS_SEQ'), 'FsPath', '/haala.fs', 'sys');
insert into SETTINGS (id, key, value, site) values (nextval('SETTINGS_SEQ'), 'ItemsPerPage', 'def=10, recent=5, favs=5', 'sys');
insert into SETTINGS (id, key, value, site) values (nextval('SETTINGS_SEQ'), 'SystemParams', 'batch=50, uploadSize=10, ssoTimeout=3m, turRefresh=3', 'sys');
insert into SETTINGS (id, key, value, site) values (nextval('SETTINGS_SEQ'), 'ParamsLimit', 'genFrag=30, upload=10', 'sys');
insert into SETTINGS (id, key, value, site) values (nextval('SETTINGS_SEQ'), 'Sites', 'xx1=xx1:en, xx2=xx1.xx2:ru', 'sys');
insert into SETTINGS (id, key, value, site) values (nextval('SETTINGS_SEQ'), 'ResourceVersion', '1', 'sys');
insert into SETTINGS (id, key, value, site) values (nextval('SETTINGS_SEQ'), 'ResourceVersion', '0', 'pub');
insert into SETTINGS (id, key, value, site) values (nextval('SETTINGS_SEQ'), 'SolrURL', 'http://localhost/solr/haala', 'sys');
insert into SETTINGS (id, key, value, site) values (nextval('SETTINGS_SEQ'), 'Environment', 'env=local', 'sys');
insert into SETTINGS (id, key, value, site) values (nextval('SETTINGS_SEQ'), 'LandingPage', 'my/account.htm', 'pub');

insert into SETTINGS (id, key, value, site) values (nextval('SETTINGS_SEQ'), 'Currencies', 'GBP, EUR, USD', 'sys');
insert into SETTINGS (id, key, value, site) values (nextval('SETTINGS_SEQ'), 'CurrencySigns', 'GBP=£, EUR=€, USD=$, AUD=AU$, ZAR=R, CAD=CA$', 'sys');
insert into SETTINGS (id, key, value, site) values (nextval('SETTINGS_SEQ'), 'CurrencyRates', 'GBP=1.000000, EUR=1.151622, USD=1.609393', 'sys');
insert into SETTINGS (id, key, value, site) values (nextval('SETTINGS_SEQ'), 'CurrencyRatesRSS', 'http://localhost/external/GBP.xml', 'sys');
insert into SETTINGS (id, key, value, site) values (nextval('SETTINGS_SEQ'), 'CssCtrlExtensions', 'css, jpg, jpeg, gif, png', 'sys');

insert into SETTINGS (id, key, value, site) values (nextval('SETTINGS_SEQ'), 'ForwardErrorsTask', 'sleep=15', 'sys');
insert into SETTINGS (id, key, value, site) values (nextval('SETTINGS_SEQ'), 'ForwardMessagesTask', 'sleep=5, items=25', 'sys');
insert into SETTINGS (id, key, value, site) values (nextval('SETTINGS_SEQ'), 'FeedTask', 'sleep=1', 'sys');
insert into SETTINGS (id, key, value, site) values (nextval('SETTINGS_SEQ'), 'CleanTuringTask', 'sleep=5, wait=3', 'sys');
insert into SETTINGS (id, key, value, site) values (nextval('SETTINGS_SEQ'), 'CleanTmpTask', 'sleep=1h, wait=1d', 'sys');
insert into SETTINGS (id, key, value, site) values (nextval('SETTINGS_SEQ'), 'CleanFilesTask', 'sleep=12h, wait=1h', 'sys');
insert into SETTINGS (id, key, value, site) values (nextval('SETTINGS_SEQ'), 'CleanAbyssTask', 'sleep=12h', 'sys');
insert into SETTINGS (id, key, value, site) values (nextval('SETTINGS_SEQ'), 'CleanFeedTask', 'sleep=0, wait=1d', 'sys');
insert into SETTINGS (id, key, value, site) values (nextval('SETTINGS_SEQ'), 'CleanUsersTask', 'sleep=1h, wait=1d', 'sys');
insert into SETTINGS (id, key, value, site) values (nextval('SETTINGS_SEQ'), 'CleanCacheTask', 'sleep=5, wipe=1d', 'sys');
insert into SETTINGS (id, key, value, site) values (nextval('SETTINGS_SEQ'), 'CleanActiveCacheTask', 'sleep=5, wipe=10', 'sys');
insert into SETTINGS (id, key, value, site) values (nextval('SETTINGS_SEQ'), 'CurrencyRatesTask', 'sleep=0', 'sys'); -- Deprecated, feed no longer available
insert into SETTINGS (id, key, value, site) values (nextval('SETTINGS_SEQ'), 'AddMissingPacksTask', 'sleep=1', 'sys');
insert into SETTINGS (id, key, value, site) values (nextval('SETTINGS_SEQ'), 'SolrOptimizeTask', 'sleep=4h', 'sys');
insert into SETTINGS (id, key, value, site) values (nextval('SETTINGS_SEQ'), 'SolrSubmitLabelsTask', 'sleep=1', 'sys');
insert into SETTINGS (id, key, value, site) values (nextval('SETTINGS_SEQ'), 'SolrSubmitFilesTask', 'sleep=1', 'sys');
insert into SETTINGS (id, key, value, site) values (nextval('SETTINGS_SEQ'), 'SolrSubmitAbyssTask', 'sleep=1', 'sys');
insert into SETTINGS (id, key, value, site) values (nextval('SETTINGS_SEQ'), 'SolrSubmitUsersTask', 'sleep=1', 'sys');
insert into SETTINGS (id, key, value, site) values (nextval('SETTINGS_SEQ'), 'SpamEmailsTask', 'sleep=5, items=10', 'sys');
insert into SETTINGS (id, key, value, site) values (nextval('SETTINGS_SEQ'), 'RefreshFilesTask', 'sleep=1', 'sys');
insert into SETTINGS (id, key, value, site) values (nextval('SETTINGS_SEQ'), 'PaymentEventTask', 'sleep=1', 'sys');
insert into SETTINGS (id, key, value, site) values (nextval('SETTINGS_SEQ'), 'NodeHeartbeatTask', 'sleep=1', 'sys');
insert into SETTINGS (id, key, value, site) values (nextval('SETTINGS_SEQ'), 'NodeMaintenanceTask', 'sleep=1, dead=5m', 'sys');
insert into SETTINGS (id, key, value, site) values (nextval('SETTINGS_SEQ'), 'NodeSummaryTask', 'sleep=1h', 'sys');
insert into SETTINGS (id, key, value, site) values (nextval('SETTINGS_SEQ'), 'NodeTestTask', 'sleep=1, emailFrom=root@EXAMPLE.ORG, emailTo=support@EXAMPLE.ORG', 'sys');

insert into SETTINGS (id, key, value, site) values (nextval('SETTINGS_SEQ'), 'SkipDebitFlag', '0', 'sys');
insert into SETTINGS (id, key, value, site) values (nextval('SETTINGS_SEQ'), 'DowntimeFlag', '0', 'sys');
insert into SETTINGS (id, key, value, site) values (nextval('SETTINGS_SEQ'), 'SuspendFlag', '0', 'sys');
insert into SETTINGS (id, key, value, site) values (nextval('SETTINGS_SEQ'), 'AddMissingPacksFlag', '0', 'sys');
insert into SETTINGS (id, key, value, site) values (nextval('SETTINGS_SEQ'), 'SolrSubmitLabelsFlag', '0', 'sys');
insert into SETTINGS (id, key, value, site) values (nextval('SETTINGS_SEQ'), 'SolrSubmitFilesFlag', '0', 'sys');
insert into SETTINGS (id, key, value, site) values (nextval('SETTINGS_SEQ'), 'SolrSubmitAbyssFlag', '0', 'sys');
insert into SETTINGS (id, key, value, site) values (nextval('SETTINGS_SEQ'), 'SolrSubmitUsersFlag', '0', 'sys');
insert into SETTINGS (id, key, value, site) values (nextval('SETTINGS_SEQ'), 'SpamEmailsFlag', '0', 'sys');
insert into SETTINGS (id, key, value, site) values (nextval('SETTINGS_SEQ'), 'RefreshFilesFlag', '0', 'sys');

insert into SETTINGS (id, key, value, site) values (nextval('SETTINGS_SEQ'), 'SupportEmail', 'support@EXAMPLE.ORG', 'pub');
insert into SETTINGS (id, key, value, site) values (nextval('SETTINGS_SEQ'), 'MainURL', 'http://EXAMPLE.ORG', 'pub');
insert into SETTINGS (id, key, value, site) values (nextval('SETTINGS_SEQ'), 'Brand', 'name=EXAMPLE.ORG, web=EXAMPLE.ORG, info=info@EXAMPLE.ORG', 'pub');
insert into SETTINGS (id, key, value, site) values (nextval('SETTINGS_SEQ'), 'TuringParams', '', 'pub');

insert into SETTINGS (id, key, value, site) values (nextval('SETTINGS_SEQ'), 'RegEx/username', '[a-zA-Z][0-9a-zA-Z_]{4,99}', 'sys');
insert into SETTINGS (id, key, value, site) values (nextval('SETTINGS_SEQ'), 'RegEx/password', '.{5,100}', 'sys');
insert into SETTINGS (id, key, value, site) values (nextval('SETTINGS_SEQ'), 'RegEx/email', '([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?) .{5,100}', 'sys');
insert into SETTINGS (id, key, value, site) values (nextval('SETTINGS_SEQ'), 'RegEx/name', '.{1,15}', 'sys');
insert into SETTINGS (id, key, value, site) values (nextval('SETTINGS_SEQ'), 'RegEx/date', '(0?[1-9]|[12][0-9]|3[01])[\/\-](0?[1-9]|1[012])[\/\-]\d{4}', 'sys');
insert into SETTINGS (id, key, value, site) values (nextval('SETTINGS_SEQ'), 'RegEx/number', '[0-9]{1,10}', 'sys');
insert into SETTINGS (id, key, value, site) values (nextval('SETTINGS_SEQ'), 'RegEx/float', '[0-9]{1,10}[,.][0-9]{1,2}|[0-9]{1,10}', 'sys');



-----------------------------------------------------------
--   USER AND RELATED
-----------------------------------------------------------


INSERT INTO ROLES(ID, ROLE, DESCRIPTION) VALUES (1, 'ROLE_USER', 'User');
INSERT INTO ROLES(ID, ROLE, DESCRIPTION) VALUES (2, 'ROLE_ADMIN', 'Admin');
INSERT INTO ROLES(ID, ROLE, DESCRIPTION) VALUES (3, 'ROLE_AGENT', 'Agent');
INSERT INTO ROLES(ID, ROLE, DESCRIPTION) VALUES (4, 'ROLE_CMS', 'CMS');
INSERT INTO ROLES(ID, ROLE, DESCRIPTION) VALUES (6, 'ROLE_CPANEL', 'Control panel');

alter SEQUENCE ROLES_SEQ restart with 7;



INSERT INTO DOMAINS(ID, DOMAIN, DOMAIN_SITE, TYPE, META) VALUES (1, '*', 'me', 'main', 'style=html5,layout-modern; resolve=site,currency');

alter SEQUENCE DOMAINS_SEQ restart with 2;



INSERT INTO ADDRESSES(ID, LINE_1, LINE_2, TOWN, POSTCODE, COUNTRY) VALUES (1, '15 Black Temmple Road', NULL, 'Shadowmoon Valley', 'SMV BT3', 'GB');
INSERT INTO ADDRESSES(ID, LINE_1, LINE_2, TOWN, POSTCODE, COUNTRY) VALUES (2, '2 Scryer Square', NULL, 'Shattrath City', 'SCT SS1', 'GB');

alter SEQUENCE ADDRESSES_SEQ restart with 3;



INSERT INTO USERS(ID, REF, USERNAME, PASSWORD, CREATED, ACTIVATED, EMAIL, SALUTATION, FIRST_NAME, LAST_NAME, ADDRESS_ID, PRIMARY_NUMBER, CREDIT)
  VALUES (1, '*', '*', '*', 20080926195906000, null, 'root@EXAMPLE.ORG', 4, '*', '*', null, '*', 0);
INSERT INTO USERS(ID, REF, USERNAME, PASSWORD, CREATED, ACTIVATED, EMAIL, SALUTATION, FIRST_NAME, LAST_NAME, ADDRESS_ID, PRIMARY_NUMBER, CREDIT)
  VALUES (2, 'Z8JKH84', 'admin', 'admin', 20080926195639000, 20080926195639000, 'admin@EXAMPLE.ORG', 1, 'Craven ', 'Drok', 2, '0735463344', 0);
INSERT INTO USERS(ID, REF, USERNAME, PASSWORD, CREATED, ACTIVATED, EMAIL, SALUTATION, FIRST_NAME, LAST_NAME, ADDRESS_ID, PRIMARY_NUMBER, CREDIT)
  VALUES (3, 'VLZG5ZJ', 'tester', 'tester', 20080926194845000, 20080926194845000, 'tester@EXAMPLE.ORG', 2, 'Belgrom', 'Rockmaul', 1, '0203384464', 0);

alter SEQUENCE USERS_SEQ restart with 4;



INSERT INTO USER_ROLE_MAP(USER_ID, ROLE_ID) VALUES (2, 2);

