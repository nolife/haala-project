-----------------------------------------------------------
--   TABLES
-----------------------------------------------------------


CREATE TABLE addresses
(
  id bigint NOT NULL,
  line_1 character varying(100) NOT NULL,
  line_2 character varying(100),
  line_3 character varying(100),
  line_4 character varying(100),
  line_5 character varying(100),
  town character varying(100) NOT NULL,
  postcode character varying(100) NOT NULL,
  country character varying(100) NOT NULL,
  CONSTRAINT addresses_pkey PRIMARY KEY (id)
)
WITHOUT OIDS;



CREATE TABLE geo_maps
(
  id bigint NOT NULL,
  lat bigint NOT NULL,
  lng bigint NOT NULL,
  lat_2 bigint,
  lng_2 bigint,
  zoom integer,
  CONSTRAINT geo_maps_pkey PRIMARY KEY (id)
)
WITHOUT OIDS;



CREATE TABLE packs
(
  id bigint NOT NULL,
  pack character varying(100) NOT NULL,
  user_id bigint,
  CONSTRAINT packs_pkey PRIMARY KEY (id)
)
WITHOUT OIDS;



CREATE TABLE countries
(
  id bigint NOT NULL,
  code character varying(100) NOT NULL,
  CONSTRAINT countries_pkey PRIMARY KEY (id)
)
WITHOUT OIDS;



CREATE TABLE files
(
  id bigint NOT NULL,
  folder character varying(250) NOT NULL,
  "name" character varying(100) NOT NULL,
  updated bigint NOT NULL,
  size integer NOT NULL,
  site character varying(100) NOT NULL,
  meta character varying(500),
  CONSTRAINT files_pkey PRIMARY KEY (id)
)
WITHOUT OIDS;



CREATE TABLE labels
(
  id bigint NOT NULL,
  "key" character varying(250) NOT NULL,
  "value" text,
  site character varying(100) NOT NULL,
  CONSTRAINT labels_pkey PRIMARY KEY (id)
)
WITHOUT OIDS;



CREATE TABLE loc_addresses
(
  id bigint NOT NULL,
  country_id bigint,
  region_id bigint,
  sub_region_id bigint,
  town_id bigint,
  postcode_id bigint,
  CONSTRAINT loc_addresses_pkey PRIMARY KEY (id)
)
WITHOUT OIDS;



CREATE TABLE locations
(
  id bigint NOT NULL,
  "type" integer NOT NULL,
  empty integer NOT NULL,
  parent_id bigint,
  CONSTRAINT locations_pkey PRIMARY KEY (id)
)
WITHOUT OIDS;



CREATE TABLE postcodes
(
  id bigint NOT NULL,
  "type" integer NOT NULL,
  "value" text NOT NULL,
  index integer NOT NULL,
  location_id bigint,
  CONSTRAINT postcodes_pkey PRIMARY KEY (id)
)
WITHOUT OIDS;



CREATE TABLE abyss_entries
(
  id bigint NOT NULL,
  ref character varying(100) NOT NULL,
  pack character varying(100),
  "value" text,
  meta character varying(500),
  created bigint NOT NULL,
  updated bigint NOT NULL,
  keep bigint NOT NULL,
  CONSTRAINT abyss_entries_pkey PRIMARY KEY (id)
)
WITHOUT OIDS;



CREATE TABLE pictures
(
  id bigint NOT NULL,
  height integer NOT NULL,
  width integer NOT NULL,
  "type" integer NOT NULL,
  "position" integer NOT NULL,
  file_id bigint,
  CONSTRAINT pictures_pkey PRIMARY KEY (id)
)
WITHOUT OIDS;



CREATE TABLE attachments
(
  id bigint NOT NULL,
  "type" integer NOT NULL,
  "position" integer NOT NULL,
  file_id bigint,
  CONSTRAINT attachments_pkey PRIMARY KEY (id)
)
WITHOUT OIDS;



CREATE TABLE roles
(
  id bigint NOT NULL,
  "role" character varying(100) NOT NULL,
  description character varying(100),
  CONSTRAINT roles_pkey PRIMARY KEY (id)
)
WITHOUT OIDS;



CREATE TABLE domains
(
  id bigint NOT NULL,
  "domain" character varying(100) NOT NULL,
  domain_site character varying(100) NOT NULL,
  "type" character varying(100) NOT NULL,
  meta character varying(500),
  CONSTRAINT domains_pkey PRIMARY KEY (id)
)
WITHOUT OIDS;



CREATE TABLE settings
(
  id bigint NOT NULL,
  "key" character varying(250) NOT NULL,
  "value" text,
  site character varying(100) NOT NULL,
  CONSTRAINT settings_pkey PRIMARY KEY (id)
)
WITHOUT OIDS;



CREATE TABLE user_role_map
(
  user_id bigint NOT NULL,
  role_id bigint NOT NULL
)
WITHOUT OIDS;



CREATE TABLE user_domain_map
(
  user_id bigint NOT NULL,
  domain_id bigint NOT NULL
)
WITHOUT OIDS;



CREATE TABLE users
(
  id bigint NOT NULL,
  ref character varying(100) NOT NULL,
  username character varying(100) NOT NULL,
  "password" character varying(100) NOT NULL,
  created bigint NOT NULL,
  last_login bigint,
  activated bigint,
  email character varying(100) NOT NULL,
  salutation integer NOT NULL,
  first_name character varying(100) NOT NULL,
  last_name character varying(100) NOT NULL,
  primary_number character varying(100) NOT NULL,
  ticket character varying(100),
  token character varying(100),
  meta character varying(500),
  credit bigint NOT NULL,
  address_id bigint,
  CONSTRAINT users_pkey PRIMARY KEY (id)
)
WITHOUT OIDS;



CREATE TABLE email_storages
(
  id bigint NOT NULL,
  description character varying(100) NOT NULL,
  total integer NOT NULL,
  CONSTRAINT email_storages_pkey PRIMARY KEY (id)
)
WITHOUT OIDS;



CREATE TABLE email_storage_items
(
  id bigint NOT NULL,
  "value" character varying(100) NOT NULL,
  status integer NOT NULL,
  storage_id bigint,
  CONSTRAINT email_storage_items_pkey PRIMARY KEY (id)
)
WITHOUT OIDS;














-----------------------------------------------------------
--   CONSTRAINTS
-----------------------------------------------------------


ALTER TABLE countries
  ADD CONSTRAINT countries_code_key UNIQUE(code);



ALTER TABLE labels
  ADD CONSTRAINT labels_key_site_key UNIQUE("key", site);



ALTER TABLE files
  ADD CONSTRAINT files_path_site_key UNIQUE(folder, "name", site);



ALTER TABLE settings
  ADD CONSTRAINT settings_key_site_key UNIQUE("key", site);



ALTER TABLE packs
  ADD CONSTRAINT pack_user_fk FOREIGN KEY (user_id)
      REFERENCES users (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION;



ALTER TABLE loc_addresses
  ADD CONSTRAINT address_location_fk1 FOREIGN KEY (country_id)
      REFERENCES locations (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION;

ALTER TABLE loc_addresses
  ADD CONSTRAINT address_location_fk2 FOREIGN KEY (region_id)
      REFERENCES locations (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION;

ALTER TABLE loc_addresses
  ADD CONSTRAINT address_location_fk3 FOREIGN KEY (sub_region_id)
      REFERENCES locations (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION;

ALTER TABLE loc_addresses
  ADD CONSTRAINT address_location_fk4 FOREIGN KEY (town_id)
      REFERENCES locations (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION;

ALTER TABLE loc_addresses
  ADD CONSTRAINT loc_address_postcode_fk FOREIGN KEY (postcode_id)
      REFERENCES postcodes (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION;



ALTER TABLE locations
  ADD CONSTRAINT location_self_fk FOREIGN KEY (parent_id)
      REFERENCES locations (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION;



ALTER TABLE postcodes
  ADD CONSTRAINT location_postcode_fk FOREIGN KEY (location_id)
      REFERENCES locations (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION;



ALTER TABLE pictures
  ADD CONSTRAINT picture_file_fk FOREIGN KEY (file_id)
      REFERENCES files (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION;



ALTER TABLE attachments
  ADD CONSTRAINT attachment_file_fk FOREIGN KEY (file_id)
      REFERENCES files (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION;



ALTER TABLE roles
  ADD CONSTRAINT roles_role_key UNIQUE("role");



ALTER TABLE domains
  ADD CONSTRAINT domains_host_key UNIQUE("domain");



ALTER TABLE user_role_map
  ADD CONSTRAINT role_user_fk FOREIGN KEY (user_id)
      REFERENCES users (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION;

ALTER TABLE user_role_map
  ADD CONSTRAINT user_role_fk FOREIGN KEY (role_id)
      REFERENCES roles (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION;



ALTER TABLE user_domain_map
  ADD CONSTRAINT domain_user_fk FOREIGN KEY (user_id)
      REFERENCES users (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION;

ALTER TABLE user_domain_map
  ADD CONSTRAINT user_domain_fk FOREIGN KEY (domain_id)
      REFERENCES domains (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION;



ALTER TABLE users
  ADD CONSTRAINT user_address_fk FOREIGN KEY (address_id)
      REFERENCES addresses (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION;

ALTER TABLE users
  ADD CONSTRAINT users_email_key UNIQUE(email);

ALTER TABLE users
  ADD CONSTRAINT users_ref_key UNIQUE(ref);

ALTER TABLE users
  ADD CONSTRAINT users_username_key UNIQUE(username);



ALTER TABLE email_storage_items
  ADD CONSTRAINT email_storage_item_fk FOREIGN KEY (storage_id)
      REFERENCES email_storages (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION;









-----------------------------------------------------------
--   INDEXES
-----------------------------------------------------------


CREATE INDEX "settings_ind1"
   ON settings ("key");

CREATE INDEX "settings_ind2"
   ON settings ("key", site);



CREATE INDEX "labels_ind1"
   ON labels ("key");

CREATE INDEX "labels_ind2"
   ON labels (site);

CREATE INDEX "labels_ind3"
   ON labels ("key", site);



CREATE INDEX "files_ind1"
   ON files (folder, "name", site);

CREATE INDEX "files_ind2"
   ON files (folder);

CREATE INDEX "files_ind3"
   ON files (folder, "name");



CREATE INDEX "domains_ind1"
   ON domains ("domain");



CREATE INDEX "users_ind1"
  ON users (username);













-----------------------------------------------------------
--   SEQUENCES
-----------------------------------------------------------


CREATE SEQUENCE addresses_seq
  INCREMENT 1
  MINVALUE 1
  MAXVALUE 9223372036854775807
  START 1
  CACHE 1;



CREATE SEQUENCE geo_maps_seq
  INCREMENT 1
  MINVALUE 1
  MAXVALUE 9223372036854775807
  START 1
  CACHE 1;



CREATE SEQUENCE packs_seq
  INCREMENT 1
  MINVALUE 1
  MAXVALUE 9223372036854775807
  START 1
  CACHE 1;



CREATE SEQUENCE countries_seq
  INCREMENT 1
  MINVALUE 1
  MAXVALUE 9223372036854775807
  START 1
  CACHE 1;



CREATE SEQUENCE files_seq
  INCREMENT 1
  MINVALUE 1
  MAXVALUE 9223372036854775807
  START 1
  CACHE 1;



CREATE SEQUENCE labels_seq
  INCREMENT 1
  MINVALUE 1
  MAXVALUE 9223372036854775807
  START 1
  CACHE 1;



CREATE SEQUENCE loc_addresses_seq
  INCREMENT 1
  MINVALUE 1
  MAXVALUE 9223372036854775807
  START 1
  CACHE 1;



CREATE SEQUENCE locations_seq
  INCREMENT 1
  MINVALUE 1
  MAXVALUE 9223372036854775807
  START 1
  CACHE 1;



CREATE SEQUENCE postcodes_seq
  INCREMENT 1
  MINVALUE 1
  MAXVALUE 9223372036854775807
  START 1
  CACHE 1;



CREATE SEQUENCE abyss_entries_seq
  INCREMENT 1
  MINVALUE 1
  MAXVALUE 9223372036854775807
  START 1
  CACHE 1;



CREATE SEQUENCE pictures_seq
  INCREMENT 1
  MINVALUE 1
  MAXVALUE 9223372036854775807
  START 1
  CACHE 1;



CREATE SEQUENCE attachments_seq
  INCREMENT 1
  MINVALUE 1
  MAXVALUE 9223372036854775807
  START 1
  CACHE 1;



CREATE SEQUENCE roles_seq
  INCREMENT 1
  MINVALUE 1
  MAXVALUE 9223372036854775807
  START 1
  CACHE 1;



CREATE SEQUENCE domains_seq
  INCREMENT 1
  MINVALUE 1
  MAXVALUE 9223372036854775807
  START 1
  CACHE 1;



CREATE SEQUENCE settings_seq
  INCREMENT 1
  MINVALUE 1
  MAXVALUE 9223372036854775807
  START 1
  CACHE 1;



CREATE SEQUENCE users_seq
  INCREMENT 1
  MINVALUE 1
  MAXVALUE 9223372036854775807
  START 1
  CACHE 1;



CREATE SEQUENCE email_storages_seq
  INCREMENT 1
  MINVALUE 1
  MAXVALUE 9223372036854775807
  START 1
  CACHE 1;



CREATE SEQUENCE email_storage_items_seq
  INCREMENT 1
  MINVALUE 1
  MAXVALUE 9223372036854775807
  START 1
  CACHE 1;







-----------------------------------------------------------
--   FUNCTIONS
-----------------------------------------------------------

CREATE OR REPLACE FUNCTION timestamp_fn() RETURNS timestamp AS $$
DECLARE
    ts timestamp without time zone;
BEGIN
    select current_timestamp into ts;
    return ts;
END;
$$ LANGUAGE plpgsql;
