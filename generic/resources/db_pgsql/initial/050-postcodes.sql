-----------------------------------------------------------
--   POSTCODES (London)
-----------------------------------------------------------


insert into postcodes (id, type, value, index, location_id) values (1, '2', '', 1, 1175);   /* Greater London */
insert into postcodes (id, type, value, index, location_id) values (2, '2', 'WC,EC', 2, 1175);   /* Central London */
insert into postcodes (id, type, value, index, location_id) values (3, '2', 'N', 3, 1175);   /* North London */
insert into postcodes (id, type, value, index, location_id) values (4, '2', 'S', 4, 1175);   /* South London */
insert into postcodes (id, type, value, index, location_id) values (5, '2', 'E', 5, 1175);   /* East London */
insert into postcodes (id, type, value, index, location_id) values (6, '2', 'W', 6, 1175);   /* West London */
insert into postcodes (id, type, value, index, location_id) values (7, '1', 'SW4', 1, 1175);   /* Abbeville Village - SW4 */
insert into postcodes (id, type, value, index, location_id) values (8, '1', 'GU1', 2, 1175);   /* Abbotswood - GU1 */
insert into postcodes (id, type, value, index, location_id) values (9, '1', 'RH5', 3, 1175);   /* Abinger Hammer - RH5 */
insert into postcodes (id, type, value, index, location_id) values (10, '1', 'W3', 4, 1175);   /* Acton - W3 */
insert into postcodes (id, type, value, index, location_id) values (11, '1', 'W4', 5, 1175);   /* Acton Green - W4 */
insert into postcodes (id, type, value, index, location_id) values (12, '1', 'KT15', 6, 1175);   /* Addlestone - KT15 */
insert into postcodes (id, type, value, index, location_id) values (13, '1', 'GU5', 7, 1175);   /* Albury - GU5 */
insert into postcodes (id, type, value, index, location_id) values (14, '1', 'GU12', 8, 1175);   /* Aldershot - GU12 */
insert into postcodes (id, type, value, index, location_id) values (15, '1', 'E1', 9, 1175);   /* Aldgate - E1 */
insert into postcodes (id, type, value, index, location_id) values (16, '1', 'N22', 10, 1175);   /* Alexandra Park - N22 */
insert into postcodes (id, type, value, index, location_id) values (17, '1', 'GU6', 11, 1175);   /* Alfold - GU6 */
insert into postcodes (id, type, value, index, location_id) values (18, '1', 'GU6', 12, 1175);   /* Alfold Crossways - GU6 */
insert into postcodes (id, type, value, index, location_id) values (19, '1', 'HA0', 13, 1175);   /* Alperton - HA0 */
insert into postcodes (id, type, value, index, location_id) values (20, '1', 'GU34', 14, 1175);   /* Alton - GU34 */
insert into postcodes (id, type, value, index, location_id) values (21, '1', 'N1', 15, 1175);   /* Angel - N1 */
insert into postcodes (id, type, value, index, location_id) values (22, '1', 'N19', 16, 1175);   /* Archway - N19 */
insert into postcodes (id, type, value, index, location_id) values (23, '1', 'N5', 17, 1175);   /* Arsenal - N5 */
insert into postcodes (id, type, value, index, location_id) values (24, '1', 'W2', 18, 1175);   /* Artesian Village - W2 */
insert into postcodes (id, type, value, index, location_id) values (25, '1', 'SL5', 19, 1175);   /* Ascot - SL5 */
insert into postcodes (id, type, value, index, location_id) values (26, '1', 'GU12', 20, 1175);   /* Ash - GU12 */
insert into postcodes (id, type, value, index, location_id) values (27, '1', 'GU12', 21, 1175);   /* Ash Green - GU12 */
insert into postcodes (id, type, value, index, location_id) values (28, '1', 'GU12', 22, 1175);   /* Ash Vale - GU12 */
insert into postcodes (id, type, value, index, location_id) values (29, '1', 'KT12', 23, 1175);   /* Ashley Park - KT12 */
insert into postcodes (id, type, value, index, location_id) values (30, '1', 'NW1,W1', 24, 1175);   /* Baker Street - NW1 / W1 */
insert into postcodes (id, type, value, index, location_id) values (31, '1', 'SW12', 25, 1175);   /* Balham - SW12 */
insert into postcodes (id, type, value, index, location_id) values (32, '1', 'EC1', 26, 1175);   /* Barbican - EC1 */
insert into postcodes (id, type, value, index, location_id) values (33, '1', 'IG11', 27, 1175);   /* Barking - IG11 */
insert into postcodes (id, type, value, index, location_id) values (34, '1', 'SW13', 28, 1175);   /* Barnes - SW13 */
insert into postcodes (id, type, value, index, location_id) values (35, '1', 'N1', 29, 1175);   /* Barnsbury - N1 */
insert into postcodes (id, type, value, index, location_id) values (36, '1', 'W6,W14', 30, 1175);   /* Barons Court - W6 / W14 */
insert into postcodes (id, type, value, index, location_id) values (37, '1', 'GU24', 31, 1175);   /* Barrowhill - GU24 */
insert into postcodes (id, type, value, index, location_id) values (38, '1', 'SW11', 32, 1175);   /* Battersea - SW11 */
insert into postcodes (id, type, value, index, location_id) values (39, '1', 'SW8', 33, 1175);   /* Battersea Park - SW8 */
insert into postcodes (id, type, value, index, location_id) values (40, '1', 'SW11', 34, 1175);   /* Battersea Square - SW11 */
insert into postcodes (id, type, value, index, location_id) values (41, '1', 'W2', 35, 1175);   /* Bayswater - W2 */
insert into postcodes (id, type, value, index, location_id) values (42, '1', 'E6', 36, 1175);   /* Beckton - E6 */
insert into postcodes (id, type, value, index, location_id) values (43, '1', 'SW12', 37, 1175);   /* Bedford Hill - SW12 */
insert into postcodes (id, type, value, index, location_id) values (44, '1', 'W4', 38, 1175);   /* Bedford Park - W4 */
insert into postcodes (id, type, value, index, location_id) values (45, '1', 'SW1', 39, 1175);   /* Belgravia - SW1 */
insert into postcodes (id, type, value, index, location_id) values (46, '1', 'SW17,SW18', 40, 1175);   /* Bellevue Village - SW17 / SW18 */
insert into postcodes (id, type, value, index, location_id) values (47, '1', 'NW3', 41, 1175);   /* Belsize Park - NW3 */
insert into postcodes (id, type, value, index, location_id) values (48, '1', 'SE1,SE16', 42, 1175);   /* Bermondsey - SE1 / SE16 */
insert into postcodes (id, type, value, index, location_id) values (49, '1', 'KT5', 43, 1175);   /* Berrylands - KT5 */
insert into postcodes (id, type, value, index, location_id) values (50, '1', 'E2', 44, 1175);   /* Bethnal Green - E2 */
insert into postcodes (id, type, value, index, location_id) values (51, '1', 'SW11', 45, 1175);   /* Between the Commons - SW11 */
insert into postcodes (id, type, value, index, location_id) values (52, '1', 'RH14', 46, 1175);   /* Billingshurst - RH14 */
insert into postcodes (id, type, value, index, location_id) values (53, '1', 'SW6', 47, 1175);   /* Bishop's Park - SW6 */
insert into postcodes (id, type, value, index, location_id) values (54, '1', 'GU24', 48, 1175);   /* Bisley - GU24 */
insert into postcodes (id, type, value, index, location_id) values (55, '1', 'SE3', 49, 1175);   /* Blackheath - SE3 */
insert into postcodes (id, type, value, index, location_id) values (56, '1', 'GU4', 50, 1175);   /* Blackheath Lane - GU4 */
insert into postcodes (id, type, value, index, location_id) values (57, '1', 'WC1', 51, 1175);   /* Bloomsbury - WC1 */
insert into postcodes (id, type, value, index, location_id) values (58, '1', 'SE1,SE17', 52, 1175);   /* Borough - SE1 / SE17 */
insert into postcodes (id, type, value, index, location_id) values (59, '1', 'W7,W5', 53, 1175);   /* Boston Manor - W7 / W5 */
insert into postcodes (id, type, value, index, location_id) values (60, '1', 'N11,N22', 54, 1175);   /* Bounds Green - N11 / N22 */
insert into postcodes (id, type, value, index, location_id) values (61, '1', 'E3', 55, 1175);   /* Bow - E3 */
insert into postcodes (id, type, value, index, location_id) values (62, '1', 'N13', 56, 1175);   /* Bowes Park - N13 */
insert into postcodes (id, type, value, index, location_id) values (63, '1', 'GU8', 57, 1175);   /* Bowlhead Green - GU8 */
insert into postcodes (id, type, value, index, location_id) values (64, '1', 'W6', 58, 1175);   /* Brackenbury Village - W6 */
insert into postcodes (id, type, value, index, location_id) values (65, '1', 'GU5', 59, 1175);   /* Bramley - GU5 */
insert into postcodes (id, type, value, index, location_id) values (66, '1', 'NW11', 60, 1175);   /* Brent Cross - NW11 */
insert into postcodes (id, type, value, index, location_id) values (67, '1', 'TW8', 61, 1175);   /* Brentford - TW8 */
insert into postcodes (id, type, value, index, location_id) values (68, '1', 'SW9', 62, 1175);   /* Brixton - SW9 */
insert into postcodes (id, type, value, index, location_id) values (69, '1', 'SW2', 63, 1175);   /* Brixton Hill - SW2 */
insert into postcodes (id, type, value, index, location_id) values (70, '1', 'SE4', 64, 1175);   /* Brockley - SE4 */
insert into postcodes (id, type, value, index, location_id) values (71, '1', 'SW2', 65, 1175);   /* Brockwell Park - SW2 */
insert into postcodes (id, type, value, index, location_id) values (72, '1', 'NW2,NW6', 66, 1175);   /* Brondesbury - NW2 / NW6 */
insert into postcodes (id, type, value, index, location_id) values (73, '1', 'GU8', 67, 1175);   /* Brook - GU8 */
insert into postcodes (id, type, value, index, location_id) values (74, '1', 'W6', 68, 1175);   /* Brook Green - W6 */
insert into postcodes (id, type, value, index, location_id) values (75, '1', 'KT13', 69, 1175);   /* Brooklands - KT13 */
insert into postcodes (id, type, value, index, location_id) values (76, '1', 'GU24', 70, 1175);   /* Brookwood - GU24 */
insert into postcodes (id, type, value, index, location_id) values (77, '1', 'KT11,KT12', 71, 1175);   /* Burhill - KT11 / KT12 */
insert into postcodes (id, type, value, index, location_id) values (78, '1', 'GU23', 72, 1175);   /* Burntcommon - GU23 */
insert into postcodes (id, type, value, index, location_id) values (79, '1', 'GU4', 73, 1175);   /* Burpham - GU4 */
insert into postcodes (id, type, value, index, location_id) values (80, '1', 'KT12', 74, 1175);   /* Burwood Park - KT12 */
insert into postcodes (id, type, value, index, location_id) values (81, '1', 'GU7', 75, 1175);   /* Busbridge - GU7 */
insert into postcodes (id, type, value, index, location_id) values (82, '1', 'GU1', 76, 1175);   /* Bushy Hill - GU1 */
insert into postcodes (id, type, value, index, location_id) values (83, '1', 'KT14', 77, 1175);   /* Byfleet - KT14 */
insert into postcodes (id, type, value, index, location_id) values (84, '1', 'N1', 78, 1175);   /* Caledonian Road - N1 */
insert into postcodes (id, type, value, index, location_id) values (85, '1', 'GU15', 79, 1175);   /* Camberley - GU15 */
insert into postcodes (id, type, value, index, location_id) values (86, '1', 'SE9,SE5', 80, 1175);   /* Camberwell - SE9 / SE5 */
insert into postcodes (id, type, value, index, location_id) values (87, '1', 'SE5', 81, 1175);   /* Camberwell Green - SE5 */
insert into postcodes (id, type, value, index, location_id) values (88, '1', 'NW1', 82, 1175);   /* Camden - NW1 */
insert into postcodes (id, type, value, index, location_id) values (89, '1', 'NW1', 83, 1175);   /* Camden Town - NW1 */
insert into postcodes (id, type, value, index, location_id) values (90, '1', 'GU27', 84, 1175);   /* Camelsdale - GU27 */
insert into postcodes (id, type, value, index, location_id) values (91, '1', 'SE16', 85, 1175);   /* Canada Water - SE16 */
insert into postcodes (id, type, value, index, location_id) values (92, '1', 'E14', 86, 1175);   /* Canary Wharf - E14 */
insert into postcodes (id, type, value, index, location_id) values (93, '1', 'E16', 87, 1175);   /* Canning Town - E16 */
insert into postcodes (id, type, value, index, location_id) values (94, '1', 'N1', 88, 1175);   /* Canonbury - N1 */
insert into postcodes (id, type, value, index, location_id) values (95, '1', 'SW13', 89, 1175);   /* Castelnau - SW13 */
insert into postcodes (id, type, value, index, location_id) values (96, '1', 'GU24', 90, 1175);   /* Castle Green - GU24 */
insert into postcodes (id, type, value, index, location_id) values (97, '1', 'NW1', 91, 1175);   /* Chalk Farm - NW1 */
insert into postcodes (id, type, value, index, location_id) values (98, '1', 'WC2', 92, 1175);   /* Charing Cross - WC2 */
insert into postcodes (id, type, value, index, location_id) values (99, '1', 'GU7', 93, 1175);   /* Charterhouse - GU7 */
insert into postcodes (id, type, value, index, location_id) values (100, '1', 'SW3,SW10', 94, 1175);   /* Chelsea - SW3 / SW10 */
insert into postcodes (id, type, value, index, location_id) values (101, '1', 'KT16', 95, 1175);   /* Chertsey - KT16 */
insert into postcodes (id, type, value, index, location_id) values (102, '1', 'KT9', 96, 1175);   /* Chessington - KT9 */
insert into postcodes (id, type, value, index, location_id) values (103, '1', 'GU8', 97, 1175);   /* Chiddingfold - GU8 */
insert into postcodes (id, type, value, index, location_id) values (104, '1', 'NW2,NW11', 98, 1175);   /* Child's Hill - NW2 / NW11 */
insert into postcodes (id, type, value, index, location_id) values (105, '1', 'GU4', 99, 1175);   /* Chilworth - GU4 */
insert into postcodes (id, type, value, index, location_id) values (106, '1', 'W4', 100, 1175);   /* Chiswick - W4 */
insert into postcodes (id, type, value, index, location_id) values (107, '1', 'W4', 101, 1175);   /* Chiswick Mall - W4 */
insert into postcodes (id, type, value, index, location_id) values (108, '1', 'GU24', 102, 1175);   /* Chobham - GU24 */
insert into postcodes (id, type, value, index, location_id) values (109, '1', 'NW3,NW7', 103, 1175);   /* Church End - NW3 / NW7 */
insert into postcodes (id, type, value, index, location_id) values (110, '1', 'EC2', 104, 1175);   /* City - EC2 */
insert into postcodes (id, type, value, index, location_id) values (111, '1', 'SW4', 105, 1175);   /* Clapham - SW4 */
insert into postcodes (id, type, value, index, location_id) values (112, '1', 'SW4', 106, 1175);   /* Clapham Common North Side - SW4 */
insert into postcodes (id, type, value, index, location_id) values (113, '1', 'SW4,11', 107, 1175);   /* Clapham Common Westside - SW4/11 */
insert into postcodes (id, type, value, index, location_id) values (114, '1', 'SW4', 108, 1175);   /* Clapham High Street - SW4 */
insert into postcodes (id, type, value, index, location_id) values (115, '1', 'SW11', 109, 1175);   /* Clapham Junction - SW11 */
insert into postcodes (id, type, value, index, location_id) values (116, '1', 'SW4', 110, 1175);   /* Clapham North - SW4 */
insert into postcodes (id, type, value, index, location_id) values (117, '1', 'SW4', 111, 1175);   /* Clapham Old Town - SW4 */
insert into postcodes (id, type, value, index, location_id) values (118, '1', 'SW4,SW2', 112, 1175);   /* Clapham Park - SW4 / SW2 */
insert into postcodes (id, type, value, index, location_id) values (119, '1', 'SW12', 113, 1175);   /* Clapham South - SW12 */
insert into postcodes (id, type, value, index, location_id) values (120, '1', 'E5', 114, 1175);   /* Clapton - E5 */
insert into postcodes (id, type, value, index, location_id) values (121, '1', 'W11', 115, 1175);   /* Clarendon Cross - W11 */
insert into postcodes (id, type, value, index, location_id) values (122, '1', 'KT10', 116, 1175);   /* Claygate - KT10 */
insert into postcodes (id, type, value, index, location_id) values (123, '1', 'EC1', 117, 1175);   /* Clerkenwell - EC1 */
insert into postcodes (id, type, value, index, location_id) values (124, '1', 'KT11', 118, 1175);   /* Cobham - KT11 */
insert into postcodes (id, type, value, index, location_id) values (125, '1', 'SW19', 119, 1175);   /* Collier's Wood - SW19 */
insert into postcodes (id, type, value, index, location_id) values (126, '1', 'N10', 120, 1175);   /* Colney Hatch - N10 */
insert into postcodes (id, type, value, index, location_id) values (127, '1', 'GU3', 121, 1175);   /* Compton - GU3 */
insert into postcodes (id, type, value, index, location_id) values (128, '1', 'W2', 122, 1175);   /* Connaught Square - W2 */
insert into postcodes (id, type, value, index, location_id) values (129, '1', 'W2', 123, 1175);   /* Connaught Village - W2 */
insert into postcodes (id, type, value, index, location_id) values (130, '1', 'KT2', 124, 1175);   /* Coombe - KT2 */
insert into postcodes (id, type, value, index, location_id) values (131, '1', 'SW19', 125, 1175);   /* Copse Hill - SW19 */
insert into postcodes (id, type, value, index, location_id) values (132, '1', 'W4', 126, 1175);   /* Corney Reach - W4 */
insert into postcodes (id, type, value, index, location_id) values (133, '1', 'SW19', 127, 1175);   /* Cottenham Park - SW19 */
insert into postcodes (id, type, value, index, location_id) values (134, '1', 'WC2', 128, 1175);   /* Covent Garden - WC2 */
insert into postcodes (id, type, value, index, location_id) values (135, '1', 'W6', 129, 1175);   /* Crabtree Estate - W6 */
insert into postcodes (id, type, value, index, location_id) values (136, '1', 'GU6', 130, 1175);   /* Cranleigh - GU6 */
insert into postcodes (id, type, value, index, location_id) values (137, '1', 'N10', 131, 1175);   /* Cranley Gardens - N10 */
insert into postcodes (id, type, value, index, location_id) values (138, '1', 'NW2', 132, 1175);   /* Cricklewood - NW2 */
insert into postcodes (id, type, value, index, location_id) values (139, '1', 'N8', 133, 1175);   /* Crouch End - N8 */
insert into postcodes (id, type, value, index, location_id) values (140, '1', 'CR0', 134, 1175);   /* Croydon - CR0 */
insert into postcodes (id, type, value, index, location_id) values (141, '1', 'SE19', 135, 1175);   /* Crystal Palace - SE19 */
insert into postcodes (id, type, value, index, location_id) values (142, '1', 'E8', 136, 1175);   /* Dalston - E8 */
insert into postcodes (id, type, value, index, location_id) values (143, '1', 'NW5', 137, 1175);   /* Dartmouth Park - NW5 */
insert into postcodes (id, type, value, index, location_id) values (144, '1', 'N1', 138, 1175);   /* De Beauvoir Town - N1 */
insert into postcodes (id, type, value, index, location_id) values (145, '1', 'SE5', 139, 1175);   /* Denmark Hill - SE5 */
insert into postcodes (id, type, value, index, location_id) values (146, '1', 'SE8', 140, 1175);   /* Deptford - SE8 */
insert into postcodes (id, type, value, index, location_id) values (147, '1', 'SW18', 141, 1175);   /* Diamond Conservation Area - SW18 */
insert into postcodes (id, type, value, index, location_id) values (148, '1', 'E14', 142, 1175);   /* Docklands - E14 */
insert into postcodes (id, type, value, index, location_id) values (149, '1', 'NW2', 143, 1175);   /* Dollis Hill - NW2 */
insert into postcodes (id, type, value, index, location_id) values (150, '1', 'GU24', 144, 1175);   /* Donkey Town - GU24 */
insert into postcodes (id, type, value, index, location_id) values (151, '1', 'KT11', 145, 1175);   /* Downside - KT11 */
insert into postcodes (id, type, value, index, location_id) values (152, '1', 'SE21,SE22', 146, 1175);   /* Dulwich - SE21 / SE22 */
insert into postcodes (id, type, value, index, location_id) values (153, '1', 'SE21', 147, 1175);   /* Dulwich Village - SE21 */
insert into postcodes (id, type, value, index, location_id) values (154, '1', 'GU8', 148, 1175);   /* Dunsfold - GU8 */
insert into postcodes (id, type, value, index, location_id) values (155, '1', 'W5', 149, 1175);   /* Ealing - W5 */
insert into postcodes (id, type, value, index, location_id) values (156, '1', 'W5', 150, 1175);   /* Ealing Broadway - W5 */
insert into postcodes (id, type, value, index, location_id) values (157, '1', 'W5', 151, 1175);   /* Ealing Common - W5 */
insert into postcodes (id, type, value, index, location_id) values (158, '1', 'SW5', 152, 1175);   /* Earls Court - SW5 */
insert into postcodes (id, type, value, index, location_id) values (159, '1', 'SW18', 153, 1175);   /* Earlsfield - SW18 */
insert into postcodes (id, type, value, index, location_id) values (160, '1', 'GU7', 154, 1175);   /* Eashing - GU7 */
insert into postcodes (id, type, value, index, location_id) values (161, '1', 'W3', 155, 1175);   /* East Acton - W3 */
insert into postcodes (id, type, value, index, location_id) values (162, '1', 'N1', 156, 1175);   /* East Canonbury - N1 */
insert into postcodes (id, type, value, index, location_id) values (163, '1', 'GU4', 157, 1175);   /* East Clandon - GU4 */
insert into postcodes (id, type, value, index, location_id) values (164, '1', 'CR0', 158, 1175);   /* East Croydon - CR0 */
insert into postcodes (id, type, value, index, location_id) values (165, '1', 'SE22', 159, 1175);   /* East Dulwich - SE22 */
insert into postcodes (id, type, value, index, location_id) values (166, '1', 'N2', 160, 1175);   /* East Finchley - N2 */
insert into postcodes (id, type, value, index, location_id) values (167, '1', 'SW18', 161, 1175);   /* East Hill - SW18 */
insert into postcodes (id, type, value, index, location_id) values (168, '1', 'KT24', 162, 1175);   /* East Horsley - KT24 */
insert into postcodes (id, type, value, index, location_id) values (169, '1', 'KT8', 163, 1175);   /* East Molesey - KT8 */
insert into postcodes (id, type, value, index, location_id) values (170, '1', 'SW15', 164, 1175);   /* East Putney - SW15 */
insert into postcodes (id, type, value, index, location_id) values (171, '1', 'SW14', 165, 1175);   /* East Sheen - SW14 */
insert into postcodes (id, type, value, index, location_id) values (172, '1', 'TW1', 166, 1175);   /* East Twickenham - TW1 */
insert into postcodes (id, type, value, index, location_id) values (173, '1', 'HA5', 167, 1175);   /* Eastcote - HA5 */
insert into postcodes (id, type, value, index, location_id) values (174, '1', 'N9', 168, 1175);   /* Edmonton - N9 */
insert into postcodes (id, type, value, index, location_id) values (175, '1', 'KT24', 169, 1175);   /* Effingham - KT24 */
insert into postcodes (id, type, value, index, location_id) values (176, '1', 'RH12', 170, 1175);   /* Ellen's Green - RH12 */
insert into postcodes (id, type, value, index, location_id) values (177, '1', 'GU8', 171, 1175);   /* Elstead - GU8 */
insert into postcodes (id, type, value, index, location_id) values (178, '1', 'KT10', 172, 1175);   /* Esher - KT10 */
insert into postcodes (id, type, value, index, location_id) values (179, '1', 'NW1', 173, 1175);   /* Euston - NW1 */
insert into postcodes (id, type, value, index, location_id) values (180, '1', 'GU6', 174, 1175);   /* Ewhurst - GU6 */
insert into postcodes (id, type, value, index, location_id) values (181, '1', 'GU6', 175, 1175);   /* Ewhurst Green - GU6 */
insert into postcodes (id, type, value, index, location_id) values (182, '1', 'GU2', 176, 1175);   /* Fairlands - GU2 */
insert into postcodes (id, type, value, index, location_id) values (183, '1', 'KT11', 177, 1175);   /* Fairmile - KT11 */
insert into postcodes (id, type, value, index, location_id) values (184, '1', 'N12,N3', 178, 1175);   /* Fallow Corner - N12 / N3 */
insert into postcodes (id, type, value, index, location_id) values (185, '1', 'GU5', 179, 1175);   /* Farley Green - GU5 */
insert into postcodes (id, type, value, index, location_id) values (186, '1', 'GU7', 180, 1175);   /* Farncombe - GU7 */
insert into postcodes (id, type, value, index, location_id) values (187, '1', 'GU34,GU9', 181, 1175);   /* Farnham - GU34 / GU9 */
insert into postcodes (id, type, value, index, location_id) values (188, '1', 'EC1', 182, 1175);   /* Farringdon - EC1 */
insert into postcodes (id, type, value, index, location_id) values (189, '1', 'TW13', 183, 1175);   /* Feltham - TW13 */
insert into postcodes (id, type, value, index, location_id) values (190, '1', 'KT22', 184, 1175);   /* Fetcham - KT22 */
insert into postcodes (id, type, value, index, location_id) values (191, '1', 'N3,N12', 185, 1175);   /* Finchley - N3 / N12 */
insert into postcodes (id, type, value, index, location_id) values (192, '1', 'EC1', 186, 1175);   /* Finsbury - EC1 */
insert into postcodes (id, type, value, index, location_id) values (193, '1', 'N4,N15', 187, 1175);   /* Finsbury Park - N4 / N15 */
insert into postcodes (id, type, value, index, location_id) values (194, '1', 'W1', 188, 1175);   /* Fitzrovia - W1 */
insert into postcodes (id, type, value, index, location_id) values (195, '1', 'GU3', 189, 1175);   /* Flexford - GU3 */
insert into postcodes (id, type, value, index, location_id) values (196, '1', 'SE23', 190, 1175);   /* Forest Hill - SE23 */
insert into postcodes (id, type, value, index, location_id) values (197, '1', 'N2,N10', 191, 1175);   /* Fortis Green - N2 / N10 */
insert into postcodes (id, type, value, index, location_id) values (198, '1', 'NW6', 192, 1175);   /* Fortune Green - NW6 */
insert into postcodes (id, type, value, index, location_id) values (199, '1', 'GU3', 193, 1175);   /* Fox Corner - GU3 */
insert into postcodes (id, type, value, index, location_id) values (200, '1', 'N11,N12', 194, 1175);   /* Friern Barnet - N11 / N12 */
insert into postcodes (id, type, value, index, location_id) values (201, '1', 'SW6', 195, 1175);   /* Fulham - SW6 */
insert into postcodes (id, type, value, index, location_id) values (202, '1', 'SW6', 196, 1175);   /* Fulham Broadway - SW6 */
insert into postcodes (id, type, value, index, location_id) values (203, '1', 'SW16,SW17', 197, 1175);   /* Furzedown - SW16 / SW17 */
insert into postcodes (id, type, value, index, location_id) values (204, '1', 'E16', 198, 1175);   /* Gallions Reach - E16 */
insert into postcodes (id, type, value, index, location_id) values (205, '1', 'KT7', 199, 1175);   /* Giggs Hill - KT7 */
insert into postcodes (id, type, value, index, location_id) values (206, '1', 'W4', 200, 1175);   /* Glebe Estate - W4 */
insert into postcodes (id, type, value, index, location_id) values (207, '1', 'W2', 201, 1175);   /* Gloucester Square - W2 */
insert into postcodes (id, type, value, index, location_id) values (208, '1', 'GU7', 202, 1175);   /* Godalming - GU7 */
insert into postcodes (id, type, value, index, location_id) values (209, '1', 'NW11', 203, 1175);   /* Golders Green - NW11 */
insert into postcodes (id, type, value, index, location_id) values (210, '1', 'GU21', 204, 1175);   /* Goldsworth Park - GU21 */
insert into postcodes (id, type, value, index, location_id) values (211, '1', 'GU5', 205, 1175);   /* Gomshall - GU5 */
insert into postcodes (id, type, value, index, location_id) values (212, '1', 'NW5', 206, 1175);   /* Gospel Oak - NW5 */
insert into postcodes (id, type, value, index, location_id) values (213, '1', 'GU27', 207, 1175);   /* Grayswood - GU27 */
insert into postcodes (id, type, value, index, location_id) values (214, '1', 'UB6', 208, 1175);   /* Greenford - UB6 */
insert into postcodes (id, type, value, index, location_id) values (215, '1', 'SE10,SE3', 209, 1175);   /* Greenwich - SE10 / SE3 */
insert into postcodes (id, type, value, index, location_id) values (216, '1', 'W4', 210, 1175);   /* Grove Park - W4 */
insert into postcodes (id, type, value, index, location_id) values (217, '1', 'GU1', 211, 1175);   /* Guildford - GU1 */
insert into postcodes (id, type, value, index, location_id) values (218, '1', 'W3,W4', 212, 1175);   /* Gunnersbury - W3 / W4 */
insert into postcodes (id, type, value, index, location_id) values (219, '1', 'E8,E9', 213, 1175);   /* Hackney - E8 / E9 */
insert into postcodes (id, type, value, index, location_id) values (220, '1', 'E2', 214, 1175);   /* Haggerston - E2 */
insert into postcodes (id, type, value, index, location_id) values (221, '1', 'TW10', 215, 1175);   /* Ham - TW10 */
insert into postcodes (id, type, value, index, location_id) values (222, '1', 'GU8', 216, 1175);   /* Hambledon - GU8 */
insert into postcodes (id, type, value, index, location_id) values (223, '1', 'W6', 217, 1175);   /* Hammersmith - W6 */
insert into postcodes (id, type, value, index, location_id) values (224, '1', 'NW3', 218, 1175);   /* Hampstead - NW3 */
insert into postcodes (id, type, value, index, location_id) values (225, '1', 'NW11', 219, 1175);   /* Hampstead Garden Suburb - NW11 */
insert into postcodes (id, type, value, index, location_id) values (226, '1', 'TW12', 220, 1175);   /* Hampton - TW12 */
insert into postcodes (id, type, value, index, location_id) values (227, '1', 'KT8', 221, 1175);   /* Hampton Court - KT8 */
insert into postcodes (id, type, value, index, location_id) values (228, '1', 'KT1', 222, 1175);   /* Hampton Wick - KT1 */
insert into postcodes (id, type, value, index, location_id) values (229, '1', 'W5', 223, 1175);   /* Hanger Hill - W5 */
insert into postcodes (id, type, value, index, location_id) values (230, '1', 'W7', 224, 1175);   /* Hanwell - W7 */
insert into postcodes (id, type, value, index, location_id) values (231, '1', 'NW10', 225, 1175);   /* Harlesden - NW10 */
insert into postcodes (id, type, value, index, location_id) values (232, '1', 'N8', 226, 1175);   /* Harringay - N8 */
insert into postcodes (id, type, value, index, location_id) values (233, '1', 'HA2', 227, 1175);   /* Harrow - HA2 */
insert into postcodes (id, type, value, index, location_id) values (234, '1', 'HA1', 228, 1175);   /* Harrow on the Hill - HA1 */
insert into postcodes (id, type, value, index, location_id) values (235, '1', 'HA7', 229, 1175);   /* Harrow Weald - HA7 */
insert into postcodes (id, type, value, index, location_id) values (236, '1', 'GU8', 230, 1175);   /* Hascombe - GU8 */
insert into postcodes (id, type, value, index, location_id) values (237, '1', 'GU27', 231, 1175);   /* Haselmere Town - GU27 */
insert into postcodes (id, type, value, index, location_id) values (238, '1', 'GU27,GU30', 232, 1175);   /* Haslemere - GU27 / GU30 */
insert into postcodes (id, type, value, index, location_id) values (239, '1', 'HA5,WD23', 233, 1175);   /* Hatch End - HA5 / WD23 */
insert into postcodes (id, type, value, index, location_id) values (240, '1', 'GU4', 234, 1175);   /* Hatchlands - GU4 */
insert into postcodes (id, type, value, index, location_id) values (241, '1', 'UB3', 235, 1175);   /* Hayes - UB3 */
insert into postcodes (id, type, value, index, location_id) values (242, '1', 'SW17', 236, 1175);   /* Heaver Estate - SW17 */
insert into postcodes (id, type, value, index, location_id) values (243, '1', 'NW4,NW7', 237, 1175);   /* Hendon - NW4 / NW7 */
insert into postcodes (id, type, value, index, location_id) values (244, '1', 'GU3', 238, 1175);   /* Henley Park - GU3 */
insert into postcodes (id, type, value, index, location_id) values (245, '1', 'SE24', 239, 1175);   /* Herne Hill - SE24 */
insert into postcodes (id, type, value, index, location_id) values (246, '1', 'KT10,KT12', 240, 1175);   /* Hersham - KT10 / KT12 */
insert into postcodes (id, type, value, index, location_id) values (247, '1', 'TW5', 241, 1175);   /* Heston - TW5 */
insert into postcodes (id, type, value, index, location_id) values (248, '1', 'W8', 242, 1175);   /* High Street Kensington - W8 */
insert into postcodes (id, type, value, index, location_id) values (249, '1', 'N5', 243, 1175);   /* Highbury - N5 */
insert into postcodes (id, type, value, index, location_id) values (250, '1', 'N5', 244, 1175);   /* Highbury and Islington - N5 */
insert into postcodes (id, type, value, index, location_id) values (251, '1', 'N6', 245, 1175);   /* Highgate - N6 */
insert into postcodes (id, type, value, index, location_id) values (252, '1', 'W8', 246, 1175);   /* Hillgate Village - W8 */
insert into postcodes (id, type, value, index, location_id) values (253, '1', 'N7', 247, 1175);   /* Hillmarton Conservation Area - N7 */
insert into postcodes (id, type, value, index, location_id) values (254, '1', 'KT10', 248, 1175);   /* Hinchley Wood - KT10 */
insert into postcodes (id, type, value, index, location_id) values (255, '1', 'GU26', 249, 1175);   /* Hindhead - GU26 */
insert into postcodes (id, type, value, index, location_id) values (256, '1', 'GU22', 250, 1175);   /* Hockering - GU22 */
insert into postcodes (id, type, value, index, location_id) values (257, '1', 'NW4', 251, 1175);   /* Holders Hill - NW4 */
insert into postcodes (id, type, value, index, location_id) values (258, '1', 'W11,W14', 252, 1175);   /* Holland Park - W11 / W14 */
insert into postcodes (id, type, value, index, location_id) values (259, '1', 'N7', 253, 1175);   /* Holloway - N7 */
insert into postcodes (id, type, value, index, location_id) values (260, '1', 'GU5', 254, 1175);   /* Holmbury St Mary - GU5 */
insert into postcodes (id, type, value, index, location_id) values (261, '1', 'SE23,SE6', 255, 1175);   /* Honor Oak Park - SE23 / SE6 */
insert into postcodes (id, type, value, index, location_id) values (262, '1', 'GU22', 256, 1175);   /* Hook Heath - GU22 */
insert into postcodes (id, type, value, index, location_id) values (263, '1', 'N8', 257, 1175);   /* Hornsey - N8 */
insert into postcodes (id, type, value, index, location_id) values (264, '1', 'GU21', 258, 1175);   /* Horsell - GU21 */
insert into postcodes (id, type, value, index, location_id) values (265, '1', 'TW3,TW4', 259, 1175);   /* Hounslow - TW3 / TW4 */
insert into postcodes (id, type, value, index, location_id) values (266, '1', 'N1', 260, 1175);   /* Hoxton - N1 */
insert into postcodes (id, type, value, index, location_id) values (267, '1', 'SW6', 261, 1175);   /* Hurlingham - SW6 */
insert into postcodes (id, type, value, index, location_id) values (268, '1', 'SW12', 262, 1175);   /* Hyde Farm Estate - SW12 */
insert into postcodes (id, type, value, index, location_id) values (269, '1', 'W2', 263, 1175);   /* Hyde Park Estate - W2 */
insert into postcodes (id, type, value, index, location_id) values (270, '1', 'W2', 264, 1175);   /* Hyde Park Square - W2 */
insert into postcodes (id, type, value, index, location_id) values (271, '1', 'GU8', 265, 1175);   /* Hydestile - GU8 */
insert into postcodes (id, type, value, index, location_id) values (272, '1', 'UB10', 266, 1175);   /* Ickenham - UB10 */
insert into postcodes (id, type, value, index, location_id) values (273, '1', 'IG11', 267, 1175);   /* Ilford - IG11 */
insert into postcodes (id, type, value, index, location_id) values (274, '1', 'SW6', 268, 1175);   /* Imperial Wharf - SW6 */
insert into postcodes (id, type, value, index, location_id) values (275, '1', 'E14', 269, 1175);   /* Isle Of Dogs - E14 */
insert into postcodes (id, type, value, index, location_id) values (276, '1', 'TW7', 270, 1175);   /* Isleworth - TW7 */
insert into postcodes (id, type, value, index, location_id) values (277, '1', 'N1', 271, 1175);   /* Islington - N1 */
insert into postcodes (id, type, value, index, location_id) values (278, '1', 'GU4', 272, 1175);   /* Jacobs Well - GU4 */
insert into postcodes (id, type, value, index, location_id) values (279, '1', 'SE11,SW8', 273, 1175);   /* Kennington - SE11 / SW8 */
insert into postcodes (id, type, value, index, location_id) values (280, '1', 'NW10', 274, 1175);   /* Kensal Green - NW10 */
insert into postcodes (id, type, value, index, location_id) values (281, '1', 'NW6', 275, 1175);   /* Kensal Rise - NW6 */
insert into postcodes (id, type, value, index, location_id) values (282, '1', 'NW6,NW10', 276, 1175);   /* Kensal Town - NW6 / NW10 */
insert into postcodes (id, type, value, index, location_id) values (283, '1', 'W8', 277, 1175);   /* Kensington - W8 */
insert into postcodes (id, type, value, index, location_id) values (284, '1', 'NW5', 278, 1175);   /* Kentish Town - NW5 */
insert into postcodes (id, type, value, index, location_id) values (285, '1', 'HA3', 279, 1175);   /* Kenton - HA3 */
insert into postcodes (id, type, value, index, location_id) values (286, '1', 'SW5', 280, 1175);   /* Kenway Village - SW5 */
insert into postcodes (id, type, value, index, location_id) values (287, '1', 'TW9', 281, 1175);   /* Kew - TW9 */
insert into postcodes (id, type, value, index, location_id) values (288, '1', 'TW9', 282, 1175);   /* Kew Green - TW9 */
insert into postcodes (id, type, value, index, location_id) values (289, '1', 'NW6', 283, 1175);   /* Kilburn - NW6 */
insert into postcodes (id, type, value, index, location_id) values (290, '1', 'N1', 284, 1175);   /* King's Cross - N1 */
insert into postcodes (id, type, value, index, location_id) values (291, '1', 'SW3', 285, 1175);   /* Kings Road - SW3 */
insert into postcodes (id, type, value, index, location_id) values (292, '1', 'KT1,KT2', 286, 1175);   /* Kingston - KT1 / KT2 */
insert into postcodes (id, type, value, index, location_id) values (293, '1', 'KT2', 287, 1175);   /* Kingston Hill - KT2 */
insert into postcodes (id, type, value, index, location_id) values (294, '1', 'SW15', 288, 1175);   /* Kingston Vale - SW15 */
insert into postcodes (id, type, value, index, location_id) values (295, '1', 'RH14', 289, 1175);   /* Kirdford - RH14 */
insert into postcodes (id, type, value, index, location_id) values (296, '1', 'GU21', 290, 1175);   /* Knaphill - GU21 */
insert into postcodes (id, type, value, index, location_id) values (297, '1', 'SW3,SW7', 291, 1175);   /* Knightsbridge - SW3 / SW7 */
insert into postcodes (id, type, value, index, location_id) values (298, '1', 'W11,W10', 292, 1175);   /* Ladbroke Grove - W11 / W10 */
insert into postcodes (id, type, value, index, location_id) values (299, '1', 'W2', 293, 1175);   /* Lancaster Gate - W2 */
insert into postcodes (id, type, value, index, location_id) values (300, '1', 'KT22', 294, 1175);   /* Leatherhead - KT22 */
insert into postcodes (id, type, value, index, location_id) values (301, '1', 'SE13,SE4', 295, 1175);   /* Lewisham - SE13 / SE4 */
insert into postcodes (id, type, value, index, location_id) values (302, '1', 'E14', 296, 1175);   /* Limehouse - E14 */
insert into postcodes (id, type, value, index, location_id) values (303, '1', 'NW1,NW8', 297, 1175);   /* Lisson Grove - NW1 / NW8 */
insert into postcodes (id, type, value, index, location_id) values (304, '1', 'SW13', 298, 1175);   /* Little Chelsea - SW13 */
insert into postcodes (id, type, value, index, location_id) values (305, '1', 'W5', 299, 1175);   /* Little Ealing - W5 */
insert into postcodes (id, type, value, index, location_id) values (306, '1', 'SW11', 300, 1175);   /* Little India - SW11 */
insert into postcodes (id, type, value, index, location_id) values (307, '1', 'W2,W9', 301, 1175);   /* Little Venice - W2 / W9 */
insert into postcodes (id, type, value, index, location_id) values (308, '1', 'SE1', 302, 1175);   /* London Bridge - SE1 */
insert into postcodes (id, type, value, index, location_id) values (309, '1', 'E8', 303, 1175);   /* London Fields - E8 */
insert into postcodes (id, type, value, index, location_id) values (310, '1', 'KT6,KT7', 304, 1175);   /* Long Ditton - KT6 / KT7 */
insert into postcodes (id, type, value, index, location_id) values (311, '1', 'KT16', 305, 1175);   /* Longcross - KT16 */
insert into postcodes (id, type, value, index, location_id) values (312, '1', 'SW10', 306, 1175);   /* Lots Road - SW10 */
insert into postcodes (id, type, value, index, location_id) values (313, '1', 'E5', 307, 1175);   /* Lower Clapton - E5 */
insert into postcodes (id, type, value, index, location_id) values (314, '1', 'KT10', 308, 1175);   /* Lower Green - KT10 */
insert into postcodes (id, type, value, index, location_id) values (315, '1', 'N7', 309, 1175);   /* Lower Holloway - N7 */
insert into postcodes (id, type, value, index, location_id) values (316, '1', 'TW16', 310, 1175);   /* Lower Sunbury - TW16 */
insert into postcodes (id, type, value, index, location_id) values (317, '1', 'GU8', 311, 1175);   /* Loxhill - GU8 */
insert into postcodes (id, type, value, index, location_id) values (318, '1', 'KT16', 312, 1175);   /* Lyne - KT16 */
insert into postcodes (id, type, value, index, location_id) values (319, '1', 'SW18', 313, 1175);   /* Magdalen Estate - SW18 */
insert into postcodes (id, type, value, index, location_id) values (320, '1', 'W9', 314, 1175);   /* Maida Hill - W9 */
insert into postcodes (id, type, value, index, location_id) values (321, '1', 'W9', 315, 1175);   /* Maida Vale - W9 */
insert into postcodes (id, type, value, index, location_id) values (322, '1', 'SL6', 316, 1175);   /* Maidenhead - SL6 */
insert into postcodes (id, type, value, index, location_id) values (323, '1', 'NW2', 317, 1175);   /* Mapesbury Estate - NW2 */
insert into postcodes (id, type, value, index, location_id) values (324, '1', 'W1', 318, 1175);   /* Marylebone - W1 */
insert into postcodes (id, type, value, index, location_id) values (325, '1', 'GU22', 319, 1175);   /* Maybury - GU22 */
insert into postcodes (id, type, value, index, location_id) values (326, '1', 'W1', 320, 1175);   /* Mayfair - W1 */
insert into postcodes (id, type, value, index, location_id) values (327, '1', 'GU22', 321, 1175);   /* Mayford - GU22 */
insert into postcodes (id, type, value, index, location_id) values (328, '1', 'GU1', 322, 1175);   /* Merrow - GU1 */
insert into postcodes (id, type, value, index, location_id) values (329, '1', 'SW19', 323, 1175);   /* Merton - SW19 */
insert into postcodes (id, type, value, index, location_id) values (330, '1', 'N5', 324, 1175);   /* Mildmay - N5 */
insert into postcodes (id, type, value, index, location_id) values (331, '1', 'E3', 325, 1175);   /* Mile End - E3 */
insert into postcodes (id, type, value, index, location_id) values (332, '1', 'GU8', 326, 1175);   /* Milford - GU8 */
insert into postcodes (id, type, value, index, location_id) values (333, '1', 'NW7', 327, 1175);   /* Mill Hill - NW7 */
insert into postcodes (id, type, value, index, location_id) values (334, '1', 'NW7', 328, 1175);   /* Mill Hill East - NW7 */
insert into postcodes (id, type, value, index, location_id) values (335, '1', 'CR4', 329, 1175);   /* Mitcham - CR4 */
insert into postcodes (id, type, value, index, location_id) values (336, '1', 'HA6', 330, 1175);   /* Moor Park - HA6 */
insert into postcodes (id, type, value, index, location_id) values (337, '1', 'SW6', 331, 1175);   /* Moore Park Estate - SW6 */
insert into postcodes (id, type, value, index, location_id) values (338, '1', 'EC2', 332, 1175);   /* Moorgate - EC2 */
insert into postcodes (id, type, value, index, location_id) values (339, '1', 'SM4', 333, 1175);   /* Morden - SM4 */
insert into postcodes (id, type, value, index, location_id) values (340, '1', 'SM4', 334, 1175);   /* Morden Park - SM4 */
insert into postcodes (id, type, value, index, location_id) values (341, '1', 'NW1,N1', 335, 1175);   /* Mornington Crescent - NW1 / N1 */
insert into postcodes (id, type, value, index, location_id) values (342, '1', 'SW14', 336, 1175);   /* Mortlake - SW14 */
insert into postcodes (id, type, value, index, location_id) values (343, '1', 'GU7', 337, 1175);   /* Munstead - GU7 */
insert into postcodes (id, type, value, index, location_id) values (344, '1', 'SW6', 338, 1175);   /* Munster Village - SW6 */
insert into postcodes (id, type, value, index, location_id) values (345, '1', 'N10', 339, 1175);   /* Muswell Hill - N10 */
insert into postcodes (id, type, value, index, location_id) values (346, '1', 'SE14', 340, 1175);   /* New Cross - SE14 */
insert into postcodes (id, type, value, index, location_id) values (347, '1', 'KT15', 341, 1175);   /* New Haw - KT15 */
insert into postcodes (id, type, value, index, location_id) values (348, '1', 'KT3', 342, 1175);   /* New Malden - KT3 */
insert into postcodes (id, type, value, index, location_id) values (349, '1', 'N11', 343, 1175);   /* New Southgate - N11 */
insert into postcodes (id, type, value, index, location_id) values (350, '1', 'SW12', 344, 1175);   /* Nightingale Triangle - SW12 */
insert into postcodes (id, type, value, index, location_id) values (351, '1', 'SW8', 345, 1175);   /* Nine Elms - SW8 */
insert into postcodes (id, type, value, index, location_id) values (352, '1', 'SE27,SW16', 346, 1175);   /* Norbury - SE27 / SW16 */
insert into postcodes (id, type, value, index, location_id) values (353, '1', 'GU3', 347, 1175);   /* Normandy - GU3 */
insert into postcodes (id, type, value, index, location_id) values (354, '1', 'W3', 348, 1175);   /* North Acton - W3 */
insert into postcodes (id, type, value, index, location_id) values (355, '1', 'W3,W5', 349, 1175);   /* North Ealing - W3 / W5 */
insert into postcodes (id, type, value, index, location_id) values (356, '1', 'N12', 350, 1175);   /* North Finchley - N12 */
insert into postcodes (id, type, value, index, location_id) values (357, '1', 'HA1', 351, 1175);   /* North Harrow - HA1 */
insert into postcodes (id, type, value, index, location_id) values (358, '1', 'W10', 352, 1175);   /* North Kensington - W10 */
insert into postcodes (id, type, value, index, location_id) values (359, '1', 'KT2', 353, 1175);   /* North Kingston - KT2 */
insert into postcodes (id, type, value, index, location_id) values (360, '1', 'TW9', 354, 1175);   /* North Sheen - TW9 */
insert into postcodes (id, type, value, index, location_id) values (361, '1', 'W13,W5', 355, 1175);   /* Northfields - W13 / W5 */
insert into postcodes (id, type, value, index, location_id) values (362, '1', 'UB5', 356, 1175);   /* Northolt - UB5 */
insert into postcodes (id, type, value, index, location_id) values (363, '1', 'HA6,WD19', 357, 1175);   /* Northwood - HA6 / WD19 */
insert into postcodes (id, type, value, index, location_id) values (364, '1', 'HA6', 358, 1175);   /* Northwood Hills - HA6 */
insert into postcodes (id, type, value, index, location_id) values (365, '1', 'SE19', 359, 1175);   /* Norwood - SE19 */
insert into postcodes (id, type, value, index, location_id) values (366, '1', 'W11', 360, 1175);   /* Notting Hill - W11 */
insert into postcodes (id, type, value, index, location_id) values (367, '1', 'W11,W2', 361, 1175);   /* Notting Hill Gate - W11 / W2 */
insert into postcodes (id, type, value, index, location_id) values (368, '1', 'SE15', 362, 1175);   /* Nunhead - SE15 */
insert into postcodes (id, type, value, index, location_id) values (369, '1', 'KT13', 363, 1175);   /* Oatlands Park - KT13 */
insert into postcodes (id, type, value, index, location_id) values (370, '1', 'GU7', 364, 1175);   /* Ockford Ridge - GU7 */
insert into postcodes (id, type, value, index, location_id) values (371, '1', 'GU23', 365, 1175);   /* Ockham - GU23 */
insert into postcodes (id, type, value, index, location_id) values (372, '1', 'RM15', 366, 1175);   /* Okendon - RM15 */
insert into postcodes (id, type, value, index, location_id) values (373, '1', 'TW7', 367, 1175);   /* Old Isleworth - TW7 */
insert into postcodes (id, type, value, index, location_id) values (374, '1', 'EC1', 368, 1175);   /* Old Street - EC1 */
insert into postcodes (id, type, value, index, location_id) values (375, '1', 'GU22', 369, 1175);   /* Old Woking - GU22 */
insert into postcodes (id, type, value, index, location_id) values (376, '1', 'W14', 370, 1175);   /* Olympia - W14 */
insert into postcodes (id, type, value, index, location_id) values (377, '1', 'GU2', 371, 1175);   /* Onslow Village - GU2 */
insert into postcodes (id, type, value, index, location_id) values (378, '1', 'TW7,TW5', 372, 1175);   /* Osterley - TW7 / TW5 */
insert into postcodes (id, type, value, index, location_id) values (379, '1', 'KT16', 373, 1175);   /* Ottershaw - KT16 */
insert into postcodes (id, type, value, index, location_id) values (380, '1', 'SW9', 374, 1175);   /* Oval - SW9 */
insert into postcodes (id, type, value, index, location_id) values (381, '1', 'KT22', 375, 1175);   /* Oxshott - KT22 */
insert into postcodes (id, type, value, index, location_id) values (382, '1', 'W2', 376, 1175);   /* Paddington - W2 */
insert into postcodes (id, type, value, index, location_id) values (383, '1', 'N13', 377, 1175);   /* Palmers Green - N13 */
insert into postcodes (id, type, value, index, location_id) values (384, '1', 'GU2', 378, 1175);   /* Park Barn - GU2 */
insert into postcodes (id, type, value, index, location_id) values (385, '1', 'NW10', 379, 1175);   /* Park Royal - NW10 */
insert into postcodes (id, type, value, index, location_id) values (386, '1', 'SW6', 380, 1175);   /* Parsons Green - SW6 */
insert into postcodes (id, type, value, index, location_id) values (387, '1', 'GU5', 381, 1175);   /* Peaslake - GU5 */
insert into postcodes (id, type, value, index, location_id) values (388, '1', 'SE15,SE5', 382, 1175);   /* Peckham - SE15 / SE5 */
insert into postcodes (id, type, value, index, location_id) values (389, '1', 'SE15', 383, 1175);   /* Peckham Rye - SE15 */
insert into postcodes (id, type, value, index, location_id) values (390, '1', 'GU24', 384, 1175);   /* Penny Pot - GU24 */
insert into postcodes (id, type, value, index, location_id) values (391, '1', 'UB6', 385, 1175);   /* Perivale - UB6 */
insert into postcodes (id, type, value, index, location_id) values (392, '1', 'SW6', 386, 1175);   /* Peterborough Estate - SW6 */
insert into postcodes (id, type, value, index, location_id) values (393, '1', 'TW10', 387, 1175);   /* Petersham - TW10 */
insert into postcodes (id, type, value, index, location_id) values (394, '1', 'W8', 388, 1175);   /* Phillimore Estate - W8 */
insert into postcodes (id, type, value, index, location_id) values (395, '1', 'W1', 389, 1175);   /* Piccadilly Circus - W1 */
insert into postcodes (id, type, value, index, location_id) values (396, '1', 'SW1', 390, 1175);   /* Pimlico - SW1 */
insert into postcodes (id, type, value, index, location_id) values (397, '1', 'HA5', 391, 1175);   /* Pinner - HA5 */
insert into postcodes (id, type, value, index, location_id) values (398, '1', 'HA5', 392, 1175);   /* Pinner Green - HA5 */
insert into postcodes (id, type, value, index, location_id) values (399, '1', 'GU24', 393, 1175);   /* Pirbright - GU24 */
insert into postcodes (id, type, value, index, location_id) values (400, '1', 'GU3', 394, 1175);   /* Pitch Place - GU3 */
insert into postcodes (id, type, value, index, location_id) values (401, '1', 'W5', 395, 1175);   /* Pitshanger Lane - W5 */
insert into postcodes (id, type, value, index, location_id) values (402, '1', 'W3', 396, 1175);   /* Poet's Corner - W3 */
insert into postcodes (id, type, value, index, location_id) values (403, '1', 'KT11', 397, 1175);   /* Pointers Green - KT11 */
insert into postcodes (id, type, value, index, location_id) values (404, '1', 'E14', 398, 1175);   /* Poplar - E14 */
insert into postcodes (id, type, value, index, location_id) values (405, '1', 'W1', 399, 1175);   /* Portman Estate - W1 */
insert into postcodes (id, type, value, index, location_id) values (406, '1', 'W11', 400, 1175);   /* Portobello - W11 */
insert into postcodes (id, type, value, index, location_id) values (407, '1', 'NW1,NW3', 401, 1175);   /* Primrose Hill - NW1 / NW3 */
insert into postcodes (id, type, value, index, location_id) values (408, '1', 'SW8,SW11', 402, 1175);   /* Prince of Wales Drive - SW8 / SW11 */
insert into postcodes (id, type, value, index, location_id) values (409, '1', 'SW15', 403, 1175);   /* Putney - SW15 */
insert into postcodes (id, type, value, index, location_id) values (410, '1', 'SW15', 404, 1175);   /* Putney Heath - SW15 */
insert into postcodes (id, type, value, index, location_id) values (411, '1', 'GU3', 405, 1175);   /* Puttenham - GU3 */
insert into postcodes (id, type, value, index, location_id) values (412, '1', 'GU22', 406, 1175);   /* Pyrford - GU22 */
insert into postcodes (id, type, value, index, location_id) values (413, '1', 'NW6', 407, 1175);   /* Queen's Park - NW6 */
insert into postcodes (id, type, value, index, location_id) values (414, '1', 'W2', 408, 1175);   /* Queensway - W2 */
insert into postcodes (id, type, value, index, location_id) values (415, '1', 'W6', 409, 1175);   /* Ravenscourt Park - W6 */
insert into postcodes (id, type, value, index, location_id) values (416, '1', 'HA2', 410, 1175);   /* Rayners Lane - HA2 */
insert into postcodes (id, type, value, index, location_id) values (417, '1', 'SW20', 411, 1175);   /* Raynes Park - SW20 */
insert into postcodes (id, type, value, index, location_id) values (418, '1', 'NW1', 412, 1175);   /* Regent's Park - NW1 */
insert into postcodes (id, type, value, index, location_id) values (419, '1', 'TW10,TW9', 413, 1175);   /* Richmond - TW10 / TW9 */
insert into postcodes (id, type, value, index, location_id) values (420, '1', 'TW10', 414, 1175);   /* Richmond Green - TW10 */
insert into postcodes (id, type, value, index, location_id) values (421, '1', 'TW10', 415, 1175);   /* Richmond Hill - TW10 */
insert into postcodes (id, type, value, index, location_id) values (422, '1', 'WD3', 416, 1175);   /* Rickmansworth - WD3 */
insert into postcodes (id, type, value, index, location_id) values (423, '1', 'GU23', 417, 1175);   /* Ripley - GU23 */
insert into postcodes (id, type, value, index, location_id) values (424, '1', 'SW15', 418, 1175);   /* Roehampton - SW15 */
insert into postcodes (id, type, value, index, location_id) values (425, '1', 'RM1', 419, 1175);   /* Romford - RM1 */
insert into postcodes (id, type, value, index, location_id) values (426, '1', 'SE16', 420, 1175);   /* Rotherhithe - SE16 */
insert into postcodes (id, type, value, index, location_id) values (427, '1', 'KT15', 421, 1175);   /* Row Hill - KT15 */
insert into postcodes (id, type, value, index, location_id) values (428, '1', 'KT15', 422, 1175);   /* Row Town - KT15 */
insert into postcodes (id, type, value, index, location_id) values (429, '1', 'GU5', 423, 1175);   /* Rowly - GU5 */
insert into postcodes (id, type, value, index, location_id) values (430, '1', 'E16', 424, 1175);   /* Royal Docks - E16 */
insert into postcodes (id, type, value, index, location_id) values (431, '1', 'W2', 425, 1175);   /* Royal Oak - W2 */
insert into postcodes (id, type, value, index, location_id) values (432, '1', 'RH12', 426, 1175);   /* Rudgwick - RH12 */
insert into postcodes (id, type, value, index, location_id) values (433, '1', 'HA4', 427, 1175);   /* Ruislip - HA4 */
insert into postcodes (id, type, value, index, location_id) values (434, '1', 'HA4', 428, 1175);   /* Ruislip Gardens - HA4 */
insert into postcodes (id, type, value, index, location_id) values (435, '1', 'HA4', 429, 1175);   /* Ruislip Manor - HA4 */
insert into postcodes (id, type, value, index, location_id) values (436, '1', 'SW6', 430, 1175);   /* Sands End - SW6 */
insert into postcodes (id, type, value, index, location_id) values (437, '1', 'GU10', 431, 1175);   /* Seale - GU10 */
insert into postcodes (id, type, value, index, location_id) values (438, '1', 'CR7', 432, 1175);   /* Selhurst - CR7 */
insert into postcodes (id, type, value, index, location_id) values (439, '1', 'GU23', 433, 1175);   /* Send - GU23 */
insert into postcodes (id, type, value, index, location_id) values (440, '1', 'GU7', 434, 1175);   /* Shackleford - GU7 */
insert into postcodes (id, type, value, index, location_id) values (441, '1', 'E8', 435, 1175);   /* Shacklewell - E8 */
insert into postcodes (id, type, value, index, location_id) values (442, '1', 'SE1', 436, 1175);   /* Shad Thames - SE1 */
insert into postcodes (id, type, value, index, location_id) values (443, '1', 'E1', 437, 1175);   /* Shadwell - E1 */
insert into postcodes (id, type, value, index, location_id) values (444, '1', 'SW11', 438, 1175);   /* Shaftesbury Estate - SW11 */
insert into postcodes (id, type, value, index, location_id) values (445, '1', 'GU4', 439, 1175);   /* Shalford - GU4 */
insert into postcodes (id, type, value, index, location_id) values (446, '1', 'GU5', 440, 1175);   /* Shamley Green - GU5 */
insert into postcodes (id, type, value, index, location_id) values (447, '1', 'GU22', 441, 1175);   /* Sheerwater - GU22 */
insert into postcodes (id, type, value, index, location_id) values (448, '1', 'WD7', 442, 1175);   /* Shenley - WD7 */
insert into postcodes (id, type, value, index, location_id) values (449, '1', 'W12', 443, 1175);   /* Shepherd's Bush - W12 */
insert into postcodes (id, type, value, index, location_id) values (450, '1', 'TW17', 444, 1175);   /* Shepperton - TW17 */
insert into postcodes (id, type, value, index, location_id) values (451, '1', 'GU5', 445, 1175);   /* Shere - GU5 */
insert into postcodes (id, type, value, index, location_id) values (452, '1', 'GU8', 446, 1175);   /* Shillinglee  - GU8 */
insert into postcodes (id, type, value, index, location_id) values (453, '1', 'E2', 447, 1175);   /* Shoreditch - E2 */
insert into postcodes (id, type, value, index, location_id) values (454, '1', 'GU27', 448, 1175);   /* Shottermill - GU27 */
insert into postcodes (id, type, value, index, location_id) values (455, '1', 'E16', 449, 1175);   /* Silvertown - E16 */
insert into postcodes (id, type, value, index, location_id) values (456, '1', 'SW1', 450, 1175);   /* Sloane Square - SW1 */
insert into postcodes (id, type, value, index, location_id) values (457, '1', 'SL1', 451, 1175);   /* Slough - SL1 */
insert into postcodes (id, type, value, index, location_id) values (458, '1', 'W1', 452, 1175);   /* Soho - W1 */
insert into postcodes (id, type, value, index, location_id) values (459, '1', 'NW1', 453, 1175);   /* Somers Town - NW1 */
insert into postcodes (id, type, value, index, location_id) values (460, '1', 'W3', 454, 1175);   /* South Acton - W3 */
insert into postcodes (id, type, value, index, location_id) values (461, '1', 'W5', 455, 1175);   /* South Ealing - W5 */
insert into postcodes (id, type, value, index, location_id) values (462, '1', 'E9', 456, 1175);   /* South Hackney - E9 */
insert into postcodes (id, type, value, index, location_id) values (463, '1', 'NW6', 457, 1175);   /* South Hampstead - NW6 */
insert into postcodes (id, type, value, index, location_id) values (464, '1', 'HA1', 458, 1175);   /* South Harrow - HA1 */
insert into postcodes (id, type, value, index, location_id) values (465, '1', 'SW7,SW5', 459, 1175);   /* South Kensington - SW7 / SW5 */
insert into postcodes (id, type, value, index, location_id) values (466, '1', 'CR7,SE20', 460, 1175);   /* South Norwood - CR7 / SE20 */
insert into postcodes (id, type, value, index, location_id) values (467, '1', 'SW6', 461, 1175);   /* South Park - SW6 */
insert into postcodes (id, type, value, index, location_id) values (468, '1', 'HA4', 462, 1175);   /* South Ruislip - HA4 */
insert into postcodes (id, type, value, index, location_id) values (469, '1', 'SW20', 463, 1175);   /* South Wimbledon - SW20 */
insert into postcodes (id, type, value, index, location_id) values (470, '1', 'E18', 464, 1175);   /* South Woodford - E18 */
insert into postcodes (id, type, value, index, location_id) values (471, '1', 'KT6', 465, 1175);   /* Southborough - KT6 */
insert into postcodes (id, type, value, index, location_id) values (472, '1', 'SW18', 466, 1175);   /* Southfields - SW18 */
insert into postcodes (id, type, value, index, location_id) values (473, '1', 'N14', 467, 1175);   /* Southgate - N14 */
insert into postcodes (id, type, value, index, location_id) values (474, '1', 'SE1,SE16', 468, 1175);   /* Southwark - SE1 / SE16 */
insert into postcodes (id, type, value, index, location_id) values (475, '1', 'SW18', 469, 1175);   /* Spencer Park - SW18 */
insert into postcodes (id, type, value, index, location_id) values (476, '1', 'E1', 470, 1175);   /* Spitalfields - E1 */
insert into postcodes (id, type, value, index, location_id) values (477, '1', 'AL2', 471, 1175);   /* St Albans - AL2 */
insert into postcodes (id, type, value, index, location_id) values (478, '1', 'KT13', 472, 1175);   /* St George's Hill - KT13 */
insert into postcodes (id, type, value, index, location_id) values (479, '1', 'SW1', 473, 1175);   /* St James's - SW1 */
insert into postcodes (id, type, value, index, location_id) values (480, '1', 'SW11', 474, 1175);   /* St John's Hill - SW11 */
insert into postcodes (id, type, value, index, location_id) values (481, '1', 'NW8', 475, 1175);   /* St John's Wood - NW8 */
insert into postcodes (id, type, value, index, location_id) values (482, '1', 'GU22', 476, 1175);   /* St Johns - GU22 */
insert into postcodes (id, type, value, index, location_id) values (483, '1', 'TW1', 477, 1175);   /* St Margarets - TW1 */
insert into postcodes (id, type, value, index, location_id) values (484, '1', 'N1', 478, 1175);   /* St Pancras - N1 */
insert into postcodes (id, type, value, index, location_id) values (485, '1', 'EC4', 479, 1175);   /* St Pauls - EC4 */
insert into postcodes (id, type, value, index, location_id) values (486, '1', 'W6', 480, 1175);   /* Stamford Brook - W6 */
insert into postcodes (id, type, value, index, location_id) values (487, '1', 'N16', 481, 1175);   /* Stamford Hill - N16 */
insert into postcodes (id, type, value, index, location_id) values (488, '1', 'HA7', 482, 1175);   /* Stanmore - HA7 */
insert into postcodes (id, type, value, index, location_id) values (489, '1', 'E1', 483, 1175);   /* Stepney - E1 */
insert into postcodes (id, type, value, index, location_id) values (490, '1', 'SW9', 484, 1175);   /* Stockwell - SW9 */
insert into postcodes (id, type, value, index, location_id) values (491, '1', 'KT11', 485, 1175);   /* Stoke D'Abernon - KT11 */
insert into postcodes (id, type, value, index, location_id) values (492, '1', 'N16', 486, 1175);   /* Stoke Newington - N16 */
insert into postcodes (id, type, value, index, location_id) values (493, '1', 'GU2', 487, 1175);   /* Stoughton - GU2 */
insert into postcodes (id, type, value, index, location_id) values (494, '1', 'W4', 488, 1175);   /* Strand on the Green - W4 */
insert into postcodes (id, type, value, index, location_id) values (495, '1', 'E15', 489, 1175);   /* Stratford - E15 */
insert into postcodes (id, type, value, index, location_id) values (496, '1', 'TW2', 490, 1175);   /* Strawberry Hill - TW2 */
insert into postcodes (id, type, value, index, location_id) values (497, '1', 'SW16,SW2', 491, 1175);   /* Streatham - SW16 / SW2 */
insert into postcodes (id, type, value, index, location_id) values (498, '1', 'SW16', 492, 1175);   /* Streatham Common - SW16 */
insert into postcodes (id, type, value, index, location_id) values (499, '1', 'SW2', 493, 1175);   /* Streatham Hill - SW2 */
insert into postcodes (id, type, value, index, location_id) values (500, '1', 'SW16,SW17', 494, 1175);   /* Streatham Park - SW16 / SW17 */
insert into postcodes (id, type, value, index, location_id) values (501, '1', 'SW16', 495, 1175);   /* Streatham Vale - SW16 */
insert into postcodes (id, type, value, index, location_id) values (502, '1', 'N4,N8', 496, 1175);   /* Stroud Green - N4 / N8 */
insert into postcodes (id, type, value, index, location_id) values (503, '1', 'HA1', 497, 1175);   /* Sudbury - HA1 */
insert into postcodes (id, type, value, index, location_id) values (504, '1', 'SW19', 498, 1175);   /* Summerstown - SW19 */
insert into postcodes (id, type, value, index, location_id) values (505, '1', 'TW16', 499, 1175);   /* Sunbury - TW16 */
insert into postcodes (id, type, value, index, location_id) values (506, '1', 'KT6,KT5', 500, 1175);   /* Surbiton - KT6 / KT5 */
insert into postcodes (id, type, value, index, location_id) values (507, '1', 'GU4', 501, 1175);   /* Sutton Green - GU4 */
insert into postcodes (id, type, value, index, location_id) values (508, '1', 'NW3', 502, 1175);   /* Swiss Cottage - NW3 */
insert into postcodes (id, type, value, index, location_id) values (509, '1', 'SE26,BR3', 503, 1175);   /* Sydenham - SE26 / BR3 */
insert into postcodes (id, type, value, index, location_id) values (510, '1', 'TW11', 504, 1175);   /* Teddington - TW11 */
insert into postcodes (id, type, value, index, location_id) values (511, '1', 'SE14', 505, 1175);   /* Telegraph Hill - SE14 */
insert into postcodes (id, type, value, index, location_id) values (512, '1', 'SW2', 506, 1175);   /* Telford Park - SW2 */
insert into postcodes (id, type, value, index, location_id) values (513, '1', 'NW11', 507, 1175);   /* Temple Fortune - NW11 */
insert into postcodes (id, type, value, index, location_id) values (514, '1', 'KT6,KT7', 508, 1175);   /* Thames Ditton - KT6 / KT7 */
insert into postcodes (id, type, value, index, location_id) values (515, '1', 'SW1', 509, 1175);   /* The Hawkins - SW1 */
insert into postcodes (id, type, value, index, location_id) values (516, '1', 'KT24', 510, 1175);   /* The Horsleys - KT24 */
insert into postcodes (id, type, value, index, location_id) values (517, '1', 'KT7,KT8', 511, 1175);   /* The Moleseys - KT7 / KT8 */
insert into postcodes (id, type, value, index, location_id) values (518, '1', 'SW11', 512, 1175);   /* The Sisters - SW11 */
insert into postcodes (id, type, value, index, location_id) values (519, '1', 'WC1', 513, 1175);   /* The Strand - WC1 */
insert into postcodes (id, type, value, index, location_id) values (520, '1', 'SW18', 514, 1175);   /* The Toastrack - SW18 */
insert into postcodes (id, type, value, index, location_id) values (521, '1', 'SW18', 515, 1175);   /* The Tonsleys - SW18 */
insert into postcodes (id, type, value, index, location_id) values (522, '1', 'GU5', 516, 1175);   /* Thorncombe Street - GU5 */
insert into postcodes (id, type, value, index, location_id) values (523, '1', 'N7', 517, 1175);   /* Tollington Park - N7 */
insert into postcodes (id, type, value, index, location_id) values (524, '1', 'KT6', 518, 1175);   /* Tolworth - KT6 */
insert into postcodes (id, type, value, index, location_id) values (525, '1', 'SW17', 519, 1175);   /* Tooting - SW17 */
insert into postcodes (id, type, value, index, location_id) values (526, '1', 'SW17', 520, 1175);   /* Tooting Bec - SW17 */
insert into postcodes (id, type, value, index, location_id) values (527, '1', 'SW17', 521, 1175);   /* Tooting Graveney - SW17 */
insert into postcodes (id, type, value, index, location_id) values (528, '1', 'N17', 522, 1175);   /* Tottenham - N17 */
insert into postcodes (id, type, value, index, location_id) values (529, '1', 'N20', 523, 1175);   /* Totteridge - N20 */
insert into postcodes (id, type, value, index, location_id) values (530, '1', 'E14', 524, 1175);   /* Tower Hamlets - E14 */
insert into postcodes (id, type, value, index, location_id) values (531, '1', 'EC3', 525, 1175);   /* Tower Hill - EC3 */
insert into postcodes (id, type, value, index, location_id) values (532, '1', 'N19', 526, 1175);   /* Tufnell Park - N19 */
insert into postcodes (id, type, value, index, location_id) values (533, '1', 'SW2,SE27', 527, 1175);   /* Tulse Hill - SW2 / SE27 */
insert into postcodes (id, type, value, index, location_id) values (534, '1', 'W4', 528, 1175);   /* Turnham Green - W4 */
insert into postcodes (id, type, value, index, location_id) values (535, '1', 'N8', 529, 1175);   /* Turnpike Lane - N8 */
insert into postcodes (id, type, value, index, location_id) values (536, '1', 'TW1,TW2', 530, 1175);   /* Twickenham - TW1 / TW2 */
insert into postcodes (id, type, value, index, location_id) values (537, '1', 'E5', 531, 1175);   /* Upper Clapton - E5 */
insert into postcodes (id, type, value, index, location_id) values (538, '1', 'N19', 532, 1175);   /* Upper Holloway - N19 */
insert into postcodes (id, type, value, index, location_id) values (539, '1', 'SE19', 533, 1175);   /* Upper Norwood - SE19 */
insert into postcodes (id, type, value, index, location_id) values (540, '1', 'SE26', 534, 1175);   /* Upper Sydenham - SE26 */
insert into postcodes (id, type, value, index, location_id) values (541, '1', 'SW17', 535, 1175);   /* Upper Tooting - SW17 */
insert into postcodes (id, type, value, index, location_id) values (542, '1', 'SW8', 536, 1175);   /* Vauxhall - SW8 */
insert into postcodes (id, type, value, index, location_id) values (543, '1', 'SW1', 537, 1175);   /* Victoria - SW1 */
insert into postcodes (id, type, value, index, location_id) values (544, '1', 'E9', 538, 1175);   /* Victoria Park - E9 */
insert into postcodes (id, type, value, index, location_id) values (545, '1', 'GU25', 539, 1175);   /* Virginia Water - GU25 */
insert into postcodes (id, type, value, index, location_id) values (546, '1', 'W5', 540, 1175);   /* Walpole Park - W5 */
insert into postcodes (id, type, value, index, location_id) values (547, '1', 'E18', 541, 1175);   /* Walthamstow - E18 */
insert into postcodes (id, type, value, index, location_id) values (548, '1', 'KT12,KT13', 542, 1175);   /* Walton-on-Thames - KT12 / KT13 */
insert into postcodes (id, type, value, index, location_id) values (549, '1', 'GU3', 543, 1175);   /* Wanborough - GU3 */
insert into postcodes (id, type, value, index, location_id) values (550, '1', 'SW18', 544, 1175);   /* Wandsworth - SW18 */
insert into postcodes (id, type, value, index, location_id) values (551, '1', 'SW12,SW17', 545, 1175);   /* Wandsworth Common - SW12 / SW17 */
insert into postcodes (id, type, value, index, location_id) values (552, '1', 'SW18', 546, 1175);   /* Wandsworth Town - SW18 */
insert into postcodes (id, type, value, index, location_id) values (553, '1', 'E1', 547, 1175);   /* Wapping - E1 */
insert into postcodes (id, type, value, index, location_id) values (554, '1', 'SE1', 548, 1175);   /* Waterloo - SE1 */
insert into postcodes (id, type, value, index, location_id) values (555, '1', 'HA3', 549, 1175);   /* Wealdstone - HA3 */
insert into postcodes (id, type, value, index, location_id) values (556, '1', 'HA9', 550, 1175);   /* Wembley - HA9 */
insert into postcodes (id, type, value, index, location_id) values (557, '1', 'W12', 551, 1175);   /* Wendell Park - W12 */
insert into postcodes (id, type, value, index, location_id) values (558, '1', 'N1', 552, 1175);   /* Wenlock Basin - N1 */
insert into postcodes (id, type, value, index, location_id) values (559, '1', 'W3', 553, 1175);   /* West Acton - W3 */
insert into postcodes (id, type, value, index, location_id) values (560, '1', 'SW6', 554, 1175);   /* West Brompton - SW6 */
insert into postcodes (id, type, value, index, location_id) values (561, '1', 'KT14', 555, 1175);   /* West Byfleet - KT14 */
insert into postcodes (id, type, value, index, location_id) values (562, '1', 'GU4', 556, 1175);   /* West Clandon - GU4 */
insert into postcodes (id, type, value, index, location_id) values (563, '1', 'UB7', 557, 1175);   /* West Drayton - UB7 */
insert into postcodes (id, type, value, index, location_id) values (564, '1', 'SE21', 558, 1175);   /* West Dulwich - SE21 */
insert into postcodes (id, type, value, index, location_id) values (565, '1', 'W13', 559, 1175);   /* West Ealing - W13 */
insert into postcodes (id, type, value, index, location_id) values (566, '1', 'W1', 560, 1175);   /* West End - W1 */
insert into postcodes (id, type, value, index, location_id) values (567, '1', 'KT10', 561, 1175);   /* West End village - KT10 */
insert into postcodes (id, type, value, index, location_id) values (568, '1', 'N3', 562, 1175);   /* West Finchley - N3 */
insert into postcodes (id, type, value, index, location_id) values (569, '1', 'NW6', 563, 1175);   /* West Hampstead - NW6 */
insert into postcodes (id, type, value, index, location_id) values (570, '1', 'HA1', 564, 1175);   /* West Harrow - HA1 */
insert into postcodes (id, type, value, index, location_id) values (571, '1', 'SW15,SW18', 565, 1175);   /* West Hill - SW15 / SW18 */
insert into postcodes (id, type, value, index, location_id) values (572, '1', 'KT24', 566, 1175);   /* West Horsley - KT24 */
insert into postcodes (id, type, value, index, location_id) values (573, '1', 'W14', 567, 1175);   /* West Kensington - W14 */
insert into postcodes (id, type, value, index, location_id) values (574, '1', 'NW6', 568, 1175);   /* West Kilburn - NW6 */
insert into postcodes (id, type, value, index, location_id) values (575, '1', 'KT8', 569, 1175);   /* West Molesey - KT8 */
insert into postcodes (id, type, value, index, location_id) values (576, '1', 'SE27', 570, 1175);   /* West Norwood - SE27 */
insert into postcodes (id, type, value, index, location_id) values (577, '1', 'SW15', 571, 1175);   /* West Putney - SW15 */
insert into postcodes (id, type, value, index, location_id) values (578, '1', 'HA4', 572, 1175);   /* West Ruislip - HA4 */
insert into postcodes (id, type, value, index, location_id) values (579, '1', 'SW19', 573, 1175);   /* West Wimbledon - SW19 */
insert into postcodes (id, type, value, index, location_id) values (580, '1', 'W11', 574, 1175);   /* Westbourne Green - W11 */
insert into postcodes (id, type, value, index, location_id) values (581, '1', 'W11,W2', 575, 1175);   /* Westbourne Grove - W11 / W2 */
insert into postcodes (id, type, value, index, location_id) values (582, '1', 'W2', 576, 1175);   /* Westbourne Park - W2 */
insert into postcodes (id, type, value, index, location_id) values (583, '1', 'E14', 577, 1175);   /* Westferry - E14 */
insert into postcodes (id, type, value, index, location_id) values (584, '1', 'GU22', 578, 1175);   /* Westfield - GU22 */
insert into postcodes (id, type, value, index, location_id) values (585, '1', 'SW1', 579, 1175);   /* Westminster - SW1 */
insert into postcodes (id, type, value, index, location_id) values (586, '1', 'KT7', 580, 1175);   /* Weston Green - KT7 */
insert into postcodes (id, type, value, index, location_id) values (587, '1', 'KT13,KT11', 581, 1175);   /* Weybridge - KT13 / KT11 */
insert into postcodes (id, type, value, index, location_id) values (588, '1', 'GU8', 582, 1175);   /* Wheelerstreet - GU8 */
insert into postcodes (id, type, value, index, location_id) values (589, '1', 'N20', 583, 1175);   /* Whetstone - N20 */
insert into postcodes (id, type, value, index, location_id) values (590, '1', 'W12', 584, 1175);   /* White City - W12 */
insert into postcodes (id, type, value, index, location_id) values (591, '1', 'E1', 585, 1175);   /* Whitechapel - E1 */
insert into postcodes (id, type, value, index, location_id) values (592, '1', 'N19', 586, 1175);   /* Whitehall Park - N19 */
insert into postcodes (id, type, value, index, location_id) values (593, '1', 'KT12', 587, 1175);   /* Whiteley Village - KT12 */
insert into postcodes (id, type, value, index, location_id) values (594, '1', 'TW2', 588, 1175);   /* Whitton - TW2 */
insert into postcodes (id, type, value, index, location_id) values (595, '1', 'NW10', 589, 1175);   /* Willesden - NW10 */
insert into postcodes (id, type, value, index, location_id) values (596, '1', 'NW10', 590, 1175);   /* Willesden Green - NW10 */
insert into postcodes (id, type, value, index, location_id) values (597, '1', 'SW19', 591, 1175);   /* Wimbledon - SW19 */
insert into postcodes (id, type, value, index, location_id) values (598, '1', 'SW19', 592, 1175);   /* Wimbledon Common - SW19 */
insert into postcodes (id, type, value, index, location_id) values (599, '1', 'SW19', 593, 1175);   /* Wimbledon Park - SW19 */
insert into postcodes (id, type, value, index, location_id) values (600, '1', 'SW19', 594, 1175);   /* Wimbledon Village - SW19 */
insert into postcodes (id, type, value, index, location_id) values (601, '1', 'N21,EN4', 595, 1175);   /* Winchmore Hill - N21 / EN4 */
insert into postcodes (id, type, value, index, location_id) values (602, '1', 'GU20', 596, 1175);   /* Windlesham - GU20 */
insert into postcodes (id, type, value, index, location_id) values (603, '1', 'SL4', 597, 1175);   /* Windsor - SL4 */
insert into postcodes (id, type, value, index, location_id) values (604, '1', 'GU23', 598, 1175);   /* Wisley - GU23 */
insert into postcodes (id, type, value, index, location_id) values (605, '1', 'GU8', 599, 1175);   /* Witley - GU8 */
insert into postcodes (id, type, value, index, location_id) values (606, '1', 'GU22', 600, 1175);   /* Woking - GU22 */
insert into postcodes (id, type, value, index, location_id) values (607, '1', 'GU5', 601, 1175);   /* Wonersh - GU5 */
insert into postcodes (id, type, value, index, location_id) values (608, '1', 'N22', 602, 1175);   /* Wood Green - N22 */
insert into postcodes (id, type, value, index, location_id) values (609, '1', 'GU3', 603, 1175);   /* Wood Street Village - GU3 */
insert into postcodes (id, type, value, index, location_id) values (610, '1', 'KT15', 604, 1175);   /* Woodham - KT15 */
insert into postcodes (id, type, value, index, location_id) values (611, '1', 'N12', 605, 1175);   /* Woodside Park - N12 */
insert into postcodes (id, type, value, index, location_id) values (612, '1', 'SE18', 606, 1175);   /* Woolwich - SE18 */
insert into postcodes (id, type, value, index, location_id) values (613, '1', 'KT4', 607, 1175);   /* Worcester Park - KT4 */
insert into postcodes (id, type, value, index, location_id) values (614, '1', 'GU8', 608, 1175);   /* Wormley - GU8 */
insert into postcodes (id, type, value, index, location_id) values (615, '1', 'GU3', 609, 1175);   /* Worplesdon - GU3 */


DROP SEQUENCE POSTCODES_SEQ;
CREATE SEQUENCE POSTCODES_SEQ
    INCREMENT 1
    START 616
    MINVALUE 1
    MAXVALUE 9223372036854775807
    CACHE 1;
