
/* IE fix for missing array.indexOf */
if (!Array.prototype.indexOf)
  Array.prototype.indexOf = function(obj, start) {
    for (var i = (start || 0), j = this.length; i < j; i++)
      if (this[i] === obj) return i;
    return -1;
  };


/* Fix for "contains" as it was deprecated and renamed to "includes" */
if (!String.prototype.contains)
  String.prototype.contains = String.prototype.includes;


(function($) {

  $(document).ready(function() {
    /* Site code before */
    if (Haala.Code.init) Haala.Code.init();

    /* jQuery pages */
    $.each(Haala.getStore().get("doc:ready", []), function(n, f) {
      try {
        f();
      } catch (e) {
        $.error(e);
      }
    });

    /* Site code after */
    if (Haala.Code.ready) Haala.Code.ready();
  });

})(jQuery);
