
/* strip::comments */

/* js::import

   { path = web/haala;
     js = store, misc, ajax, storage, label, messages, input-validator, ui-validator, code,

          favourite-bar, kdx-menu, kdx-panel, card-slide,
          quick-navigation, aqua-list, page-spread, frag-input, availability-grid, upload-form,
          pagination, sortcolumn, listing, table-builder, formpanel, form-builder }

   { path = web/endorsed/qtip2-20170510;
     js = jquery.qtip.min }

   { path = web/endorsed/context-menu-1.01;
     js = jquery.contextMenu }

*/
