/**
 * jQuery
 * Depends on: misc
 */

var Haala = Haala || {};

(function($) {

  /* plugin */

  $.fn.quickNavigation = function(/*json*/ options) {
    if (!options) options = {};
    options.element = this;
    return $(this).data("quick-navigation") || new Haala.QuickNavigation(options);
  };

  /* logic */

  Haala.QuickNavigation = function(/*json*/ options) {
    this.init(options);
  };

  Haala.QuickNavigation.Settings = {
    contentElement: null
  };

  Haala.QuickNavigation.prototype = {

    init: function(/*json*/ options) {
      this.configure(options);
    },

    hasItems: function() {
      return this.model.items.length > 0;
    },

    configure: function(/*json*/ options) {
      var settings = $.extend(true, {}, Haala.QuickNavigation.Settings, options || {});
      var model = {};
      model.element = $(settings.element);

      var configureItem = function(/*selector*/ item) {
        var e = $(item);
        var external = e.attr("target") == "_blank";

        model.items.push({ element: e, external: external });

        if (!external)
          e.aclick(function() {
            document.location = e.attr("href");
          });
      };

      var ce = $(settings.contentElement);
      if (ce.length) ce.children().appendTo(model.element);

      model.items = [];
      model.element.find("a").each(function(n, item) { configureItem(item); });

      model.element.data("quick-navigation", this);
      this.model = model;
    }

  };

})(jQuery);
