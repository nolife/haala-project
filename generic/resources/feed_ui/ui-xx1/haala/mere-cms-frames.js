
/* strip::comments */

/* CMS frames to edit text, pictures, files.
 * Depends on:
 *  mere-cms-core OR mere-cms-nocore
 */

(function($) {

  Haala.MereCms.TextFrame = function(/*json*/ options) {
    this.init(options);
  };

  Haala.MereCms.TextFrame.Settings = {
  };

  Haala.MereCms.TextFrame.prototype = {

    init: function(/*json*/ options) {
      this.configure(options);
    },

    configure: function(/*json*/ options) {
      var settings = $.extend(true, {}, Haala.MereCms.TextFrame.Settings, options || {});
      var model = settings.state;

      var id = "cms-text-frame";
      if (!$("#"+id).length)
        $("body").append(
          "<div id='"+id+"'>" +
            "<div class='text'>" +
              "<textarea></textarea>" +
            "</div>" +
          "</div>"
        );
      model.element = $("#"+id);

      var mf = settings.message.mappingFn;
      if (model.roots && model.sites) model.lang = mf("lang-"+model.roots[model.sites.indexOf(model.site)]);
      model.textElement = model.element.find("textarea");

      /* rich edit */
      var opts = model.opts || "";
      var tinymceOptions = null;
      if (opts.indexOf("rf") != -1) {
        /* full */
        tinymceOptions = {
          plugins: "autolink lists table image hr link advlist contextmenu media fullscreen paste code directionality visualchars nonbreaking anchor charmap textcolor",
          toolbar1: "code | undo redo | bold italic underline strikethrough | alignleft aligncenter alignright alignjustify | formatselect fontselect fontsizeselect",
          toolbar2: "table | bullist numlist | outdent indent | forecolor backcolor | link unlink anchor image media | charmap hr nonbreaking | ltr rtl | fullscreen",
          menubar: true
        };
      } else if (opts.indexOf("rn") != -1) {
        /* normal */
        tinymceOptions = {
          plugins: "autolink lists table image hr link advlist contextmenu media fullscreen paste code directionality",
          toolbar1: "code | undo redo | bold italic underline strikethrough | alignleft aligncenter alignright alignjustify | formatselect fontselect fontsizeselect",
          toolbar2: "table | bullist numlist | outdent indent | forecolor backcolor | link unlink anchor image media | charmap hr nonbreaking | ltr rtl | fullscreen"
        };
      } else if (opts.indexOf("rl") != -1) {
        /* limited */
        tinymceOptions = {
          plugins: "autolink code",
          toolbar1: "code | undo redo | bold italic underline strikethrough | alignleft aligncenter alignright alignjustify | bullist numlist | outdent indent"
        };
      } else {
        /* disabled */
        tinymceOptions = {
          force_br_newlines : true, force_p_newlines : false, forced_root_block : ''
        };
      }

      /* create rich edit */
      if (!model.plainText)
        Haala.Misc.tinymce4(model.textElement,
          $.extend(true, {
            height: 500-180,
            plugins: "autolink code",
            toolbar1: "code | undo redo"
          }, tinymceOptions));

      var buttons = [];
      buttons.push({ text: mf("save"), click: function() { save(); }});
      if (model.enableDelete) buttons.push({ text: mf("delete"), click: function() { remove(); }});
      buttons.push({ text: mf("cancel"), click: function() { $(this).dialog("close"); }});

      model.element.dialog({
        autoOpen: false, resizable: true, zIndex: 50, width: 700, height: 500,
        buttons: buttons,
        title: mf("text-frame", { lang: model.lang, key: model.itemId }),
        close: function() {
          Haala.MereCms.showAnchors();
          /* reset rich edit (recreated it on each frame call) */
          if (!model.plainText)
            model.textElement.tinymce().remove();
        }
      });

      var load = function() {
        model.textElement.val("");

        settings.loadFn({ itemId: model.itemId, params: [model.siteSuffix] }, model,
          function(/*json*/ rvo) {
            /* server may return many values (value per site) for the same label key OR just a single value  */
            if (!rvo.result.values) model.textElement.val(rvo.result.value); //single
            else $.each(rvo.result.values, function(n, vo) { //many
              if (model.site+model.siteSuffix == vo.site) model.textElement.val(vo.value);
            });
            model.element.dialog("open");
          },
          Haala.MereCms.showAnchors);
      };
      setTimeout(load, 10);

      var save = function() {
        var cmd = {
          key: model.itemId,
          values: [{ site: model.site+model.siteSuffix, value: model.textElement.val() }]
        };
        settings.saveFn(cmd, model,
          function() { model.element.dialog("close"); },
          function() {});
      };

      var remove = function() {
        var cmd = {
          key: model.itemId,
          values: [{ site: model.site+model.siteSuffix }]
        };
        var msg = mf("text-frame-delete", { key: model.itemId, site: model.site+model.siteSuffix });
        Haala.Messages.showYN(msg, mf("dialog-confirm-title"), function() {
          settings.deleteFn(cmd, model,
              function() { model.element.dialog("close"); },
              function() {});
        });
      };

      Haala.MereCms.hideAnchors();

      this.model = model;
    }

  };



  Haala.MereCms.PictureFrame = function(/*json*/ options) {
    this.init(options);
  };

  Haala.MereCms.PictureFrame.Settings = {
  };

  Haala.MereCms.PictureFrame.prototype = {

    init: function(/*json*/ options) {
      this.configure(options);
    },

    configure: function(/*json*/ options) {
      var settings = $.extend(true, {}, Haala.MereCms.PictureFrame.Settings, options || {});
      var model = settings.state;

      var id = "cms-picture-frame";
      if (!$("#"+id).length)
        $("body").append(
          "<div id='"+id+"'>" +
            "<div class='canvas'>" +
              "<img src=''/>" +
            "</div>" +
            "<form id='cms-picture-upload-form' class='upload' action='#' onsubmit='return false;' enctype='multipart/form-data'>" +
              "<input name='dat1' type='file'/>" +
            "</form>" +
          "</div>"
        );
      model.element = $("#"+id);

      var mf = settings.message.mappingFn;
      if (model.roots && model.sites) model.lang = mf("lang-"+model.roots[model.sites.indexOf(model.site)]);
      model.canvasElement = model.element.find(".canvas");

      var buttons = [];
      buttons.push({ text: mf("upload"), click: function() { save(); }});
      if (model.enableDelete) buttons.push({ text: mf("delete"), click: function() { remove(); }});
      buttons.push({ text: mf("cancel"), click: function() { $(this).dialog("close"); }});

      model.element.dialog({
        autoOpen: false, resizable: false, zIndex: 50, width: 600, height: 400,
        buttons: buttons,
        close: Haala.MereCms.showAnchors
      });

      var load = function() {
        model.canvasElement.find("img").attr("src", "");

        settings.loadFn({ itemId: model.itemId }, model,
          function(/*json*/ rvo) {
            /* server may return many values (value per site) for the same picture OR just a single value */
            var xvo = null;
            if (!rvo.result.items) xvo = rvo.result; //single
            else $.each(rvo.result.items, function(n, vo) { //many
              if (model.site == vo.site) xvo = vo;
            });

            var f = function() {
              var isize = xvo.size;
              var iw = isize.width, ih = isize.height;
              var cw = model.canvasElement.width(), ch = model.canvasElement.height();

              //select the best fit
              var fw = iw, fh = ih;
              if (cw < iw || ch < ih) {
                var ratio = iw/ih;
                var xh = cw/ratio, xp = cw*xh; //fit by width
                var yw = ch*ratio, yp = yw*ch; //fit by height
                if (xp > yp && ch >= xh) { fw = cw; fh = xh; }
                else if (yp > xp && cw >= yw) { fw = yw; fh = ch; }
                else if (ch >= xh) { fw = cw; fh = xh; }
                else { fw = yw; fh = ch; }
              }

              var img = model.canvasElement.find("img");
              img.css({ width: fw+"px", height: fh+"px", margin: ((ch-fh)/2)+"px 0 0 "+((cw-fw)/2)+"px" });
              img.attr("src", "/"+rvo.result.path+"?site="+xvo.site+"&cms=1&nc=1&salt="+Math.random());
            };
            setTimeout(f, 200);

            var size = xvo.size;
            model.element.dialog("option", "title", mf("picture-frame", { size: size.width+"x"+size.height, lang: model.lang }));
            model.element.dialog("open");
          },
          Haala.MereCms.showAnchors);
      };
      setTimeout(load, 10);

      var save = function() {
        var params = [];
        if ((model.opts || "").indexOf("a") == -1) params.push("same-size");
        settings.saveFn({ path: model.itemId, site: model.site, params: params }, model,
          function() { model.element.dialog("close"); },
          function() {});
      };

      var remove = function() {
        var cmd = { key: model.itemId, site: model.site };
        var name = model.itemId.substring(model.itemId.lastIndexOf("/")+1);
        var msg = mf("picture-frame-delete", { name: name, site: model.site });
        Haala.Messages.showYN(msg, mf("dialog-confirm-title"), function() {
          settings.deleteFn(cmd, model,
              function() { model.element.dialog("close"); },
              function() {});
        });
      };

      Haala.MereCms.hideAnchors();

      this.model = model;
    }

  };


  Haala.MereCms.FileFrame = function(/*json*/ options) {
    this.init(options);
  };

  Haala.MereCms.FileFrame.Settings = {
  };

  Haala.MereCms.FileFrame.prototype = {

    init: function(/*json*/ options) {
      this.configure(options);
    },

    configure: function(/*json*/ options) {
      var settings = $.extend(true, {}, Haala.MereCms.FileFrame.Settings, options || {});
      var model = settings.state;
      var mf = settings.message.mappingFn;

      var id = "cms-file-frame";
      if (!$("#"+id).length)
        $("body").append(
          "<div id='"+id+"'>" +
            "<div class='link'>" +
              "<a href='' target='_blank'>"+mf("file-open")+"</a>" +
            "</div>" +
            "<form id='cms-file-upload-form' class='upload' action='#' onsubmit='return false;' enctype='multipart/form-data'>" +
              "<input name='dat1' type='file'/>" +
            "</form>" +
          "</div>"
        );
      model.element = $("#"+id);

      var buttons = [];
      buttons.push({ text: mf("upload"), click: function() { save(); }});
      if (model.enableDelete) buttons.push({ text: mf("delete"), click: function() { remove(); }});
      buttons.push({ text: mf("cancel"), click: function() { $(this).dialog("close"); }});

      model.element.dialog({
        autoOpen: false, resizable: false, zIndex: 50,
        width: 600, height: 400,
        buttons: buttons,
        title: mf("file-frame"),
        close: Haala.MereCms.showAnchors
      });

      model.linkElement = model.element.find(".link");

      var load = function() {
        model.linkElement.find("a").attr("href", "");

        settings.loadFn({ itemId: model.itemId }, model,
          function(/*json*/ rvo) {
            var link = model.linkElement.find("a");
            /* server may return many values (value per site) for the same file OR just a single value */
            if (!rvo.result.items) { //single
              link.attr("href", "/bin.htm?path="+rvo.result.path+"&site="+rvo.result.site+"&cms=1&nc=1&salt="+Math.random());
            } else $.each(rvo.result.items, function(n, vo) { //many
              if (model.site == vo.site)
                link.attr("href", "/bin.htm?path="+rvo.result.path+"&site="+vo.site+"&cms=1&nc=1&salt="+Math.random());
            });
            model.element.dialog("open");
          },
          Haala.MereCms.showAnchors);
      };
      setTimeout(load, 10);

      var save = function() {
        var params = [];
        if ((model.opts || "").indexOf("a") == -1) params.push("same-format");
        settings.saveFn({ path: model.itemId, site: model.site, params: params }, model,
          function() { model.element.dialog("close"); },
          function() {});
      };

      var remove = function() {
        var cmd = { key: model.itemId, site: model.site };
        var name = model.itemId.substring(model.itemId.lastIndexOf("/")+1);
        var msg = mf("file-frame-delete", { name: name, site: model.site });
        Haala.Messages.showYN(msg, mf("dialog-confirm-title"), function() {
          settings.deleteFn(cmd, model,
              function() { model.element.dialog("close"); },
              function() {});
        });
      };

      Haala.MereCms.hideAnchors();

      this.model = model;
    }

  };

})(jQuery);

