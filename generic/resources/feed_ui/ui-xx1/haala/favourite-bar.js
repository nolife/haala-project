/**
 * jQuery
 * Depends on: misc
 *
 * Can be used in any pack.
 * Not a pure plug-in as it has hardcoded AJAX calls.
 */

var Haala = Haala || {};

(function($) {

  /* plugin */
  $.fn.favouriteBar = function(/*json*/ options) {
    if (!options) options = {};
    options.element = this;
    return $(this).data("favourite-bar") || new Haala.FavouriteBar(options);
  };

  /* logic */

  Haala.FavouriteBar = function(/*json*/ options) {
    this.init(options);
  };

  Haala.FavouriteBar.Settings = {
    itemName: "favs", // cookie name or storage item name
    currentRef: null, max: 5, hide: true,
    cardElementFn: function(/*json*/ vo, /*json*/ model) { return null; },
    renderedFn: function(/*json*/ model) { }, //deprecated in favour of listeners
    initFn: function(/*json*/ model) { },
    storage: Haala.Cookie // anything from "storage.js"
  };

  Haala.FavouriteBar.prototype = {

    init: function(/*json*/ options) {
      this.configure(options);
      var model = this.model;
      model.initFn(model);
    },

    /* public */
    addRef: function(/*string*/ ref) {
      var model = this.model;
      if (model.runner.busy || !model.element.length) return;

      if (model.refs.indexOf(ref) != -1) model.refs.splice(model.refs.indexOf(ref), 1);
      model.refs.unshift(ref);
      this.save();
      this.load(true);
    },

    /* public */
    hasRef: function(/*string*/ ref) {
      var model = this.model;
      if (model.runner.busy || !model.element.length) return false;
      return model.refs.indexOf(ref) != -1
    },

    /* public */
    removeRef: function(/*string*/ ref) {
      var model = this.model;
      if (model.runner.busy || !model.element.length) return;

      if (model.refs.indexOf(ref) != -1) model.refs.splice(model.refs.indexOf(ref), 1);
      this.save();
      this.load(true);
    },

    /* public */
    removeRefs: function() {
      var model = this.model;
      if (model.runner.busy || !model.element.length) return;

      model.refs = [];
      this.save();
      this.load(true);
    },

    load: function(/*bool*/ refresh) {
      var model = this.model;
      if (model.runner.busy || !model.element.length) return;
      model.runner.busy = true;
      var THIS = this;

      var x = model.storage.read(model.itemName) || "";
      model.refs = x.split(",");

      var itemsLoaded = function(/*int*/ total, /*[json]*/ vos) {
        var le = model.element.find(".list");
        le.empty();
          
        /* sort as in ref array */
        var refs = [], items = [];
        model.refs.forEach(function(ref) {
          vos.forEach(function(vo) {
            if (vo.ref == ref) {
              var e = model.cardElementFn(vo, model);
              if (e) {
                e.appendTo(le);
                items.push({ element: e, ref: vo.ref });
                refs.push(vo.ref);
              }
            }
          });
        });
        model.refs = refs;
        model.items = items;

        var ae = model.element.find(".add").hide();
        var re = model.element.find(".remove").hide();
        var ra = model.element.find(".remove-all").hide();
        if (total) ra.show();
        if (model.currentRef) {
          /* move the current ref to the top of the list and array */
          $.each(model.items, function(n, item) {
            if (model.currentRef == item.ref) {
              le.prepend(item.element);
              model.refs.splice(model.refs.indexOf(item.ref), 1);
              model.refs.unshift(item.ref);
            }
          });
          /* show add/remove link */
          if (model.refs.indexOf(model.currentRef) == -1) {
            ae.show().unbind("click").click(function(event) {
              event.preventDefault();
              THIS.addRef(model.currentRef);
            });
          } else {
            re.show().unbind("click").click(function(event) {
              event.preventDefault();
              THIS.removeRef(model.currentRef);
            });
          }
        }

        if (model.hide) {
          if (!total && !model.currentRef) model.element.hide();
          else model.element.show();
        }
        model.runner.busy = false;
        THIS.save();
        model.renderedFn(model);
        model.listener.fire("rendered");
      };

      Haala.AJAX.call(null, MiscF.favs, { target: 0, params: [x], refresh: refresh ? 1 : 0, hint: model.hint },
        function(data) {
          itemsLoaded(parseInt(data.result.total), data.result.objects);
        }, null, Haala.AJAX.DO_NOTHING );
    },

    save: function() {
      var model = this.model;
      if (model.runner.busy || !model.element.length) return;

      if (!model.refs.length) model.storage.remove(model.itemName);
      else {
        var x = "";
        $.each(model.refs, function(n, v) {
          if (n < model.max) x += (n ? "," : "")+v
        });
        model.storage.write(model.itemName, x);
      }
    },

    configure: function(/*json*/ options) {
      var settings = $.extend(true, {}, Haala.FavouriteBar.Settings, options || {});
      var model = {
        max: settings.max, hide: settings.hide, currentRef: settings.currentRef, itemName: settings.itemName, hint: settings.hint,
        cardElementFn: settings.cardElementFn, renderedFn: settings.renderedFn, initFn: settings.initFn,
        storage: settings.storage
      };
      model.element = $(settings.element);
      var THIS = this;

      if (model.element.length) {
        model.element.find(".add").hide();
        model.element.find(".remove").hide();
        model.element.find(".remove-all").click(function(event) {
          event.preventDefault();
          THIS.removeRefs();
        }).hide();

        setTimeout(function() { THIS.load(); }, 10);
      }

      model.runner = {
        busy: false
      };

      Haala.Misc.eventListener(model, this);

      model.element.data("favourite-bar", this);
      this.model = model;
    }

  };

})(jQuery);
