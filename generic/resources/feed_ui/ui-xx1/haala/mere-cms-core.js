
/* strip::comments */

/**
 * jQuery
 * Depends on:
 *  misc
 *  UI
 *  qtip
 *  contextMenu
 *  tinymce
 *  mere-cms-frames
 *
 *  This is on-the-spot CMS, which allows to edit resources annotated with "data-cms" tags. A "data-cms" tag
 *  is parsed and an anchor DIV appears for the resource it annotates. It could be a text, picture or file.
 *  By clicking on an anchor a pop-up frame appears allowing to view and change the resource. Rich click brings
 *  context menu with more options.
 *
 *  The pop-up frames used by the plugin were extracted into separate "mere-cms-frames" JS file so they can be
 *  re-used in other CMS-like pages without "data-cms" annotations. There is "mere-cms-nocore" JS file, which
 *  provides dependencies those frames need.
 */

var Haala = Haala || {};

(function($) {

  Haala.MereCms = {

    Settings: {
      message: {
        mappingFn: function(/*string*/ msg, /*json*/ tokens) { return msg; }
      },
      control: {
        editFn: function(/*json*/ model) {},
        doneFn: function(/*json*/ model) {},
        versionFn: function(/*json*/ model) {},
        exitFn: function(/*json*/ model) {}
      },
      state: {
        edit: false, //highlight editable content or not
        mySite: "", //current site   eg. mh2
        mydefSite: "", //default site   eg. mh1
        sites: [], //all input sites   eg. mh1 mh2   (same order as roots)
        roots: [] //all root input sites   eg. xx1 xx2   (can be converted to labels)
      },
      stateFn: function(/*json*/ state) {},
      anchor: {
        blinkDelay: 1000
      },
      textFrame: {
        loadFn: function(/*json*/ cmd, /*json*/ model, /*fn*/ callback, /*fn*/ fail) {},
        saveFn: function(/*json*/ cmd, /*json*/ model, /*fn*/ callback, /*fn*/ fail) {}
      },
      pictureFrame: {
        loadFn: function(/*json*/ cmd, /*json*/ model, /*fn*/ callback, /*fn*/ fail) {},
        saveFn: function(/*json*/ cmd, /*json*/ model, /*fn*/ callback, /*fn*/ fail) {}
      },
      fileFrame: {
        loadFn: function(/*json*/ cmd, /*json*/ model, /*fn*/ callback, /*fn*/ fail) {},
        saveFn: function(/*json*/ cmd, /*json*/ model, /*fn*/ callback, /*fn*/ fail) {}
      }
    },

    init: function(/*json*/ options) {
      var settings = $.extend(true, {}, Haala.MereCms.Settings, options || {});
      settings.stateFn(settings.state);

      //control
      new Haala.MereCms.Control($.extend(true, {}, settings.control, {
        message: { mappingFn: settings.message.mappingFn },
        state: settings.state
      }));

      //anchors
      if (settings.state.edit) {
        Haala.MereCms.anchorOptions = { //need to save this for "appended" fn
          message: { mappingFn: settings.message.mappingFn },
          state: settings.state,
          textFn: function(/*json*/ state) {
            new Haala.MereCms.TextFrame($.extend(true, {}, settings.textFrame, {
              message: { mappingFn: settings.message.mappingFn },
              state: state
            }));
          },
          pictureFn: function(/*json*/ state) {
            new Haala.MereCms.PictureFrame($.extend(true, {}, settings.pictureFrame, {
              message: { mappingFn: settings.message.mappingFn },
              state: state
            }));
          },
          fileFn: function(/*json*/ state) {
            new Haala.MereCms.FileFrame($.extend(true, {}, settings.fileFrame, {
              message: { mappingFn: settings.message.mappingFn },
              state: state
            }));
          }
        };
        $("*[data-cms]").each(function() {
          $(this).mereCmsAnchor(Haala.MereCms.anchorOptions);
        });

        $(document).mousedown(function(/*event*/ event) {
          var anchorClick = false;
          var e = $(event.target);
          while (e.length && !anchorClick) {
            anchorClick |= e.hasClass("cms-anchor") || e.hasClass("contextMenu");
            e = e.parent();
          }
          if (!anchorClick && !Haala.MereCms.anchorsHidden) {
            Haala.MereCms.hideAnchors();
            setTimeout(function() { Haala.MereCms.showAnchors(); }, settings.anchor.blinkDelay);
          }
        });

        Haala.MereCms.enabled = settings.state.edit;
        Haala.MereCms.showAnchors();
      }
    },

    confirm: function(/*json*/ params) {
      var id = "cms-dialog";
      if (!$("#"+id).length)
        $("body").append("<div id='"+id+"'></div>");

      $("#"+id).html(params.text);
      $("#"+id).dialog({
        title: params.title,
        buttons: [
          { text: params.buttonLabels[0],
            click: function() {
              $(this).dialog("close");
              params.confirmFn();
            }},
          { text: params.buttonLabels[1],
            click: function() {
              $(this).dialog("close");
            }}]
      });
    },

    anchorUserPictures: function(/*string*/ pfx) {
      $("img[src^='/"+pfx+"']").each(function() {
        $(this).attr("data-cms", "1:"+$(this).attr("src").substring(1).split("?")[0]+":a");
      });
      Haala.MereCms.appended();
    },

    appended: function() {
      $("*[data-cms]").each(function() {
        $(this).mereCmsAnchor(Haala.MereCms.anchorOptions);
      });
      if (Haala.MereCms.enabled && !Haala.MereCms.anchorsHidden) Haala.MereCms.showAnchors();
    },

    showAnchors: function() {
      Haala.MereCms.anchorsHidden = false;
      $("*[data-cms]").each(function() {
        $(this).mereCmsAnchor().show();
      });
    },

    hideAnchors: function() {
      Haala.MereCms.anchorsHidden = true;
      $("*[data-cms]").each(function() {
        $(this).mereCmsAnchor().hide();
      });
    }
  };



  Haala.MereCms.Control = function(/*json*/ options) {
    this.init(options);
  };

  Haala.MereCms.Control.Settings = {
  };

  Haala.MereCms.Control.prototype = {

    init: function(/*json*/ options) {
      this.configure(options);
    },

    configure: function(/*json*/ options) {
      var settings = $.extend(true, {}, Haala.MereCms.Control.Settings, options || {});
      var model = settings.state;

      var id = "cms-control";
      var html =
        "<div id='"+id+"'>" +
          (model.edit ? "<div class='done'></div>" : "<div class='edit'></div>") +
          "<div class='version'></div>" +
          "<div class='exit'></div>" +
        "</div>";
      $("body").prepend(html);
      model.element = $("#"+id);

      var elemType = function(/*element*/ e) {
        var x = null;
        if (e.hasClass("edit")) x = "edit";
        if (e.hasClass("done")) x = "done";
        if (e.hasClass("version")) x = "version";
        if (e.hasClass("exit")) x = "exit";
        return x;
      };

      model.element.find("div").qtip({
        content: {
          text: function() { return settings.message.mappingFn("tooltip-"+elemType($(this))); }
        },
        position: {
          my: "left center", at: "right center"
        },
        style: {
          widget: true
        }
      });

      model.element.find("div").aclick(function(event) {
        var e = $(event.target);
        switch (elemType(e)) {
          case "edit":
            settings.editFn(model);
            break;
          case "done":
            settings.doneFn(model);
            break;
          case "version":
            var mf = settings.message.mappingFn;
            Haala.MereCms.confirm({
              text: mf("confirm-version"),
              title: mf("dialog-confirm-title"),
              buttonLabels: [mf("dialog-yes"), mf("dialog-no")],
              confirmFn: function() { settings.versionFn(model); }
            });
            break;
          case "exit":
            settings.exitFn(model);
            break;
        }
      });

      this.model = model;
    }

  };


  $.fn.mereCmsAnchor = function(/*json*/ options) {
    if (!options) options = {};
    options.element = this;
    return $(this).data("mere-cms-anchor") || new Haala.MereCms.Anchor(options);
  };

  Haala.MereCms.Anchor = function(/*json*/ options) {
    this.init(options);
  };

  Haala.MereCms.Anchor.Settings = {
  };

  Haala.MereCms.Anchor.prototype = {

    init: function(/*json*/ options) {
      this.configure(options);
    },

    show: function() {
      var offset = this.model.element.offset();
      if (offset.top >= 0 && offset.left >= 0) //do not show for hidden elements
        this.model.anchorElement.css({ top: offset.top+"px", left: offset.left+"px" }).show();
    },

    hide: function() {
      this.model.anchorElement.hide();
    },

    configure: function(/*json*/ options) {
      var settings = $.extend(true, {}, Haala.MereCms.Anchor.Settings, options || {});
      var model = settings.state;
      model.element = $(settings.element);

      var anchorClass = model.element.attr("data-cms-class") || "";
      var anchor =
        $("<div class='cms-anchor "+anchorClass+"'>" +
            "<div class='icon'></div>" +
            "<div class='text'></div>" +
          "</div>");
      $("body").append(anchor);
      model.anchorElement = anchor;

      var gotoFrame = function(/*string*/ data, /*string*/ site) {
        // mode:key:opts[:suffix]
        var parr = data.split(":");
        var mode = parseInt(parr[0][0]); // 2t -> 2   1 - picture, 2/3 - text, 4 - file
        var itemId = parr[1];
        var opts = parr.length > 2 ? parr[2].split(",") : [];
        switch (mode) {
          case 1:
            // mode:key:opts
            settings.pictureFn({
              itemId: itemId, opts: opts,
              sites: model.sites, roots: model.roots, site: site || model.mySite
            });
            break;
          case 4:
            // mode:key:opts
            settings.fileFn({
              itemId: itemId, opts: opts,
              sites: model.sites, roots: model.roots, site: model.mydefSite //use only the default site
            });
            break;
          default:
            // mode:key:opts[:suffix]
            settings.textFn({
              itemId: itemId, opts: opts,
              sites: model.sites, roots: model.roots,
              site: parr[0] == 3 ? model.mydefSite : site || model.mySite, //mode 3 is not multilingual - use the default site
              siteSuffix: parr.length > 3 ? parr[3] : ""
            });
        }
      };

      var addContextMenu = function(/*element*/ e, /*string*/ data) {
        e.contextMenu({ menu: "cms-anchor-menu" },
          function(action, el, pos) {
            if (action == "edit") gotoFrame(data);
            if ((action+"     ").substring(0, 5) == "root-") {
              var root = action.substring(5);
              var site = model.sites[model.roots.indexOf(root)];
              gotoFrame(data, site);
            }
          });
      };

      var collectionBehaviour = function(/*string*/ data, /*string*/ mod) {
        // label;mode:key:opts:suffix[;mode:key:opts:suffix...]
        var barr = data.split(";");
        if (!mod) mod = "x";

        var text = settings.message.mappingFn("anchor-"+barr[0]);
        model.anchorElement.find(".text").html(text);

        model.anchorElement.append("<div class='drop'></div>");
        var drop = model.anchorElement.find(".drop");

        $.each(barr, function(z, v) {
          if (z > 0) { //skip label
            // mode:key:opts:suffix
            var parr = v.split(":");
            var txt = "";
            if (mod == "x") txt = settings.message.mappingFn("anchor-"+parr[0]); // eg. Keywords
            if (parr[1]) {
              if (mod == "n") txt = "#"+z; // #1 .. #n
              if (mod == "n0") txt = "#"+(z-1); // #0 .. #n
              if (mod == "kv") txt = "#"+Math.floor((z+1)/2); // #1 #1 .. #n #n
              if (mod == "kv0") txt = "#"+Math.floor((z-1)/2); // #0 #0 .. #n #n
            }
            var cls = "cell mod-"+mod+" "+(z%2==0?"odd":"even");
            var cell = $("<div class='"+cls+"'>"+(parr[1] ? txt : "&nbsp;")+"</div>");
            drop.append(cell);

            if (parr[1]) cell.data("cms-data", v);
          }
        });

        var cells = model.anchorElement.find(".cell");
        $.each(cells, function(n, e) {
          e = $(e);
          if (e.data("cms-data")) {
            e.aclick(function() {
              gotoFrame(e.data("cms-data"));
            });
            addContextMenu(e, e.data("cms-data"));
          }
        });

        var zIndex = parseInt(model.anchorElement.css("z-index") || 100);
        model.anchorElement.hover(
          function() {
            $(this).css("z-index", zIndex+1);
            drop.show();
          },
          function() {
            drop.hide();
            $(this).css("z-index", zIndex);
          }
        );
      };

      var multikeyBehaviour = function(/*string*/ data) {
        // mode:key,key,key:opts:suffix
        var parr = data.split(":");
        var ndata = parr[0]; // mode:key[,key...]:opts:suffix -> label;mode:key:opts:suffix[;mode:key:opts:suffix...]
        var opts = parr.length > 2 ? parr[2].split(",") : [];
        var sfx = parr.length > 3 ? parr[3] : "";
        $.each(parr[1].split(","), function(z, v) {
          ndata += ";"+parr[0]+":"+v+(sfx ? ":"+sfx : "");
        });
        var mod = null;
        $.each(["n", "n0", "kv", "kv0"], function(z, v) { //all available mods
          if (opts.indexOf(v) != -1) mod = v;
        });
        collectionBehaviour(ndata, mod);
      };

      var defaultBehaviour = function(/*string*/ data) {
        // mode:key:opts:suffix
        var e = model.anchorElement;
        e.aclick(function() {
          gotoFrame(data);
        });
        addContextMenu(e, data);
      };

      //configure editable piece
      var data = model.element.attr("data-cms");
      if (data.indexOf(";") != -1) {
        // label;mode:key:opts:suffix[;mode:key:suffix...]
        collectionBehaviour(data);
      } else {
        // mode:key:opts:suffix
        var parr = data.split(":");
        var text = settings.message.mappingFn("anchor-"+parr[0]);
        model.anchorElement.find(".text").html(text);
        var f = parr[1].indexOf(",") != -1 ? multikeyBehaviour : defaultBehaviour;
        f(data);
      }

      model.element.data("mere-cms-anchor", this);
      this.model = model;
    }

  };

})(jQuery);

/* Integration */

(function($) {

  Haala.MereCms.Integ = {};

  Haala.MereCms.Integ.DefaultOptions = {
    state: {
      edit: false
    },
    message: {
      mappingFn: function(/*string*/ msg, /*json*/ tokens) {
        return Haala.Misc.messageMapping("#cms-messages", msg, tokens);
      }
    },
    stateFn: function(/*json*/ state) {
      state.mySite = Haala.Misc.systemVar("mySite");
      state.mydefSite = Haala.Misc.systemVar("mydefSite");
      state.edit = Haala.Misc.systemVar("cmsEdit") == "1";
      state.sites = Haala.Misc.systemVar("mineInputSites");
      state.roots = Haala.Misc.systemVar("inputSites");
    },
    control: {
      editFn: function(/*json*/ model) {
        Haala.AJAX.call("body", CmsF.status, { target: 1 },
                function() { document.location = document.location.href; return Haala.AJAX.DO_NOTHING; });
      },
      doneFn: function(/*json*/ model) {
        Haala.AJAX.call("body", CmsF.status, { target: 2 },
                function() { document.location = document.location.href; return Haala.AJAX.DO_NOTHING; });
      },
      versionFn: function(/*json*/ model) {
        Haala.AJAX.call("body", CmsF.incVersion, { site: model.mydefSite },
                function() { document.location = document.location.href; return Haala.AJAX.DO_NOTHING; });
      },
      exitFn: function(/*json*/ model) {
        Haala.AJAX.call("body", CmsF.status, { target: 3 },
                function() { document.location = "/"; return Haala.AJAX.DO_NOTHING; });
      }
    },
    textFrame: {
      loadFn: function(/*json*/ cmd, /*json*/ model, /*fn*/ callback, /*fn*/ fail) {
        Haala.AJAX.call("body", CmsF.loadText, cmd, callback, fail);
      },
      saveFn: function(/*json*/ cmd, /*json*/ model, /*fn*/ callback, /*fn*/ fail) {
        Haala.AJAX.call("body", CmsF.editText, cmd, callback, fail);
      }
    },
    pictureFrame: {
      loadFn: function(/*json*/ cmd, /*json*/ model, /*fn*/ callback, /*fn*/ fail) {
        Haala.AJAX.call("body", CmsF.loadPic, cmd, callback, fail);
      },
      saveFn: function(/*json*/ cmd, /*json*/ model, /*fn*/ callback, /*fn*/ fail) {
        var form = $("#cms-picture-upload-form").uploadForm(
          $.extend(true, {}, Haala.UploadForm.Integ.DefaultOptions, {
            uploadSuccessFn: function(/*json*/ model) {
              Haala.AJAX.call("body", CmsF.editPic, cmd, callback, fail);
            },
            destroyFn: function(/*json*/ model) { form.destroy(); }
          }));
        form.upload();
      }
    },
    fileFrame: {
      loadFn: function(/*json*/ cmd, /*json*/ model, /*fn*/ callback, /*fn*/ fail) {
        Haala.AJAX.call("body", CmsF.loadFile, cmd, callback, fail);
      },
      saveFn: function(/*json*/ cmd, /*json*/ model, /*fn*/ callback, /*fn*/ fail) {
        var form = $("#cms-file-upload-form").uploadForm(
          $.extend(true, {}, Haala.UploadForm.Integ.DefaultOptions, {
            uploadSuccessFn: function(/*json*/ model) {
              Haala.AJAX.call("body", CmsF.editFile, cmd, callback, fail);
            },
            destroyFn: function(/*json*/ model) { form.destroy(); }
          }));
        form.upload();
      }
    }
  };

  Haala.MereCms.Integ.init = function() {
    new Haala.MereCms.init(Haala.MereCms.Integ.DefaultOptions);
    if (Haala.MereCms.enabled) Haala.MereCms.anchorUserPictures("web/my/");
  };

})(jQuery);
