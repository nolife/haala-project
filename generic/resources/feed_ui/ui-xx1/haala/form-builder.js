/**
 * jQuery
 * Wire together plugins to make a submittable form with validation
 */

;var Haala = Haala || {}

;(function ( $, window, document, undefined ) {

  Haala.FormBuilder = function(/*{}*/ options) {
    this.init(options || {})
  }

  Haala.FormBuilder.prototype = {

    supportedPlugins: ["formpanel"],
    supportedValidators: [Haala.InputValidator, Haala.UIValidator],
    plugins: {},
    scrollToError: false, // scroll to first visible validation error
    submittable: true, // submit form when "enter" is pressed, form must have submit button

    init: function(/*{}*/ options) {
      this.scrollToError = options.scrollToError
      if (options.submittable == false) this.submittable = false
    },

    pluginData: function(pluginName) {
      return this.plugins[pluginName] ? this.plugins[pluginName].data("plugin_" + pluginName) : null
    },

    plugin: function(/*plugin*/ plugin) {
      var THIS = this,
          set = false
      this.supportedPlugins.forEach(function(pluginName) {
        var data = plugin.data("plugin_" + pluginName)
        if (data != undefined) {
          THIS.plugins[pluginName] = plugin
          set = true
        }
      })
      if (!set) $.error( 'Unsupported plugin '+plugin )
      return this
    },

    validate: function(validator) {
      var THIS = this,
          set = false
      this.supportedValidators.forEach(function(v) {
        if (validator instanceof v) {
          THIS.validator = validator
          set = true
        }
      })
      if (!set) $.error( 'Unsupported validator '+validator )
      return this
    },

    build: function() {
      var pluginFP = this.plugins["formpanel"],
          pdataFP  = this.pluginData("formpanel"),
          validator = this.validator

      if (pluginFP) {

        pdataFP.options.validateFields = function(model, /*[{}]*/ fields, /*{}*/ values, callback) {
          var valid = true

          if (validator) {
            var arr = fields.map(function(field) { // append extra properties to validate hash to use by validator
              field.validate.value = values[field.name]
              field.validate.tagName = field.$element.is(":checkbox") ? "CHECKBOX" : field.$element[0].tagName
              return field.validate
            })
            valid = validator.validateInput(arr)
          }

          if (!valid && this.scrollToError)
            pdataFP.$element.find(".error:visible:first").toViewport()

          callback(valid)
        }

        pdataFP.options.onReset = function(model, /*[{}]*/ fields) {
          if (validator) {
            var arr = fields.map(function(field) {
              return field.validate || {}
            })
            validator.resetInput(arr)
          }
        }

        if (this.submittable)
          pdataFP.$element.submit(function(event) {
            event.preventDefault()
            pluginFP.formpanel("submit")
          })

      }

    }

  }

})( jQuery, window, document );
