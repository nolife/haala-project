/**
 * jQuery
 * Depends on: nothing
 */

var Haala = Haala || {};

(function($) {

  Haala.getStore = function() {
    if (!Haala.Store.Instance) new Haala.Store();
    return Haala.Store.Instance;
  };

  Haala.Store = function(/*json*/ options) {
    this.init(options);
  };

  Haala.Store.Instance = null;

  Haala.Store.Settings = {
  };

  Haala.Store.prototype = {

    init: function(/*json*/ options) {
      this.configure(options);
    },

    /** Get value by its key or return a default */
    get: function(key, def) {
      return this.model[key] || def || null;
    },

    /** Set value */
    set: function(key, val) {
      this.model[key] = val;
    },

    /** Remove value by its key */
    remove: function(key) {
      delete this.model[key];
    },

    /** Push value into array determined by key */
    push: function(key, val) {
      var x = this.model[key];
      if (!x) {
        x = [];
        this.model[key] = x;
      }
      x.push(val);
    },

    configure: function(/*json*/ options) {
      var settings = $.extend(true, {}, Haala.Store.Settings, options || {});
      var model = {};

      this.model = model;
      Haala.Store.Instance = this;
    }

  };

})(jQuery);
