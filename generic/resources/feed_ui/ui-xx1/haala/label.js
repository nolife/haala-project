/**
 * jQuery
 * Depends on: nothing
 */

var Haala = Haala || {};

(function($) {

  Haala.getLabel = function() {
    if (!Haala.Label.Instance) new Haala.Label();
    return Haala.Label.Instance;
  };

  Haala.Label = function(/*json*/ options) {
    this.init(options);
  };

  Haala.Label.Instance = null;

  Haala.Label.Settings = {
  };

  Haala.Label.prototype = {

    init: function(/*json*/ options) {
      this.configure(options);
    },

    /** Parametrize a label with tokens */
    parametrize: function(/*string*/ value, /*json*/ tokens) {
      if (tokens) $.each(tokens, function(k, v) {
        value = value.replace("{"+k+"}", v);
      });
      return value;
    },

    /** Get parametrized label by its key */
    get: function(/*string*/ key, /*json*/ tokens) {
      return this.getOrElse(key, key, tokens);
    },

    /** Get parametrized label by its key or return default non-parametrized value */
    getOrElse: function(/*string*/ key, /*string*/ val, /*json*/ tokens) {
      return this.model.box[key] ? this.parametrize(this.model.box[key], tokens) : val;
    },

    configure: function(/*json*/ options) {
      var settings = $.extend(true, {}, Haala.Label.Settings, options || {});
      var model = {}; /* model.box populated externally from labels.js.vm */

      this.model = model;
      Haala.Label.Instance = this;
    }

  };

})(jQuery);
