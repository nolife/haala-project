/**
 * jQuery
 * Depends on: nothing
 */

var Haala = Haala || {};

(function($) {

  /* plugin */

  $.fn.kdxMenu = function(/*json*/ options) {
    if (!options) options = {};
    options.element = this;
    return $(this).data("kdx-menu") || new Haala.KdxMenu(options);
  };

  /* logic */

  Haala.KdxMenu = function(/*json*/ options) {
    this.init(options);
  };

  Haala.KdxMenu.Settings = {
    scanItems: true,
    dropdown: {
      offset: { x: 0, y: 0, anchor: "bottom-left" },
      offsetFn: function(/*json*/ dd, /*json*/ model) {
        var pe = dd.parentItem.element;
        var psize = { x: pe.offset().left, y: pe.offset().top, w: pe.width(), h: pe.height() };
        return dd.offset.anchor == "top-right" ? { x: psize.x+psize.w+dd.offset.x, y: psize.y+dd.offset.y } :
                                                 { x: psize.x+dd.offset.x, y: psize.y+psize.h+dd.offset.y }
      },
      expandFn: function(/*json*/ dd, /*json*/ model) { dd.element.slideDown(200); },
      collapseFn: function(/*json*/ dd, /*json*/ model) { dd.element.slideUp(200); },
      collapseDelay: 500
    },
    item: {
      gotoFn: function(/*json*/ item, /*json*/ model) { document.location = item.href; },
      clickFn: function(/*json*/ item, /*json*/ model) {
        item.element.animate({ opacity: .5 }, 140).animate({ opacity: 1 }, 140,
          function() {
            try { item.gotoFn(item, model); }
            catch (e) { $.error("Cannot open "+item.href+"\n"+e); }
          });
      }
    },
    css: { highlight: "highlight", expand: "expand" }
  };

  Haala.KdxMenu.prototype = {

    init: function(/*json*/ options) {
      this.configure(options);
    },

    configure: function(/*json*/ options) {
      var href = function(/*element*/ e) {
        var x = e.attr("data-href") || e.attr("href") || null;
        if (!x) e.find("a").each(function() { x = this.attr("href"); return false; });
        return x;
      };

      var addScanItems = function(/*element*/ e, /*[json]*/ items) {
        if (settings.scanItems)
          e.find("a").each(function() {
            var elem = $(this), h = href(elem), same = false;
            $.each(items, function(n, item) { same |= elem[0] == $(item.element)[0]; });
            if (!same && h && h[0] != "#") items.push({ element: elem });
          });
      };

      var disableClicks = function(/*element*/ e) {
        e.click(function(event) { event.preventDefault(); });
        e.find("a").each(function() { $(this).click(function(event) { event.preventDefault(); }); });
      };

      var expand = function(/*json*/ item) {
        collapseSiblingsExcept(item);
        var dd = item.dropdown;
        if (dd) {
          var coords = dd.offsetFn(dd, model);
          dd.element.css({ position: "absolute", left: coords.x+"px", top: coords.y+"px" });
          if (!item.element.hasClass(model.css.expand)) item.element.addClass(model.css.expand);
          dd.expandFn(dd, model);
        }
      };

      var collapse = function(/*json*/ item) {
        var currentDropdown = function() {
          var collectAll = function(/*json[]*/ items) {
            return $.map(items, function(item) {
              return item.dropdown ? $.merge([item.dropdown], collectAll(item.dropdown.items)) : []
            });
          };
          var xs = $.grep(collectAll(model.items), function(x) { return x.mouseover; });
          return xs.length > 0 ? xs[0] : null;
        };

        var currentDropdownItem = function(/*json*/ dd) {
          var xs = $.grep(dd.items, function(x) { return x.mouseover; });
          return xs.length > 0 ? xs[0] : null;
        };

        var currentTopItem = function() {
          var xs = $.grep(model.items, function(item) { return item.mouseover; });
          return xs.length > 0 ? xs[0] : null;
        };

        var dd = item.dropdown;
        if (dd) setTimeout(function() {
          var dd = currentDropdown();
          var ditem = dd ? currentDropdownItem(dd) : null;
          collapseSiblingsExcept(ditem ? ditem : dd ? dd.items[0] : currentTopItem());
        }, dd.collapseDelay);
      };

      var collapseNow = function(/*json*/ item) {
        item.mouseover = false;
        var dd = item.dropdown;
        if (dd && dd.element.is(":visible")) {
          dd.mouseover = false;
          item.element.removeClass(model.css.expand);
          dd.collapseFn(dd, model);
          $.each(dd.items, function(n, ditem) { collapseNow(ditem) })
        }
      };

      var collapseSiblingsExcept = function(/*json*/ exceptItem) {
        var level = exceptItem && exceptItem.level ? exceptItem.level : 0;
        var items = level > 0 ? exceptItem.parentItem.dropdown.items : model.items;
        $.each(items, function(n, item) {
          if (item != exceptItem && item.level == level) collapseNow(item);
        });
      };

      var currentURL = function() {
        var x = document.location.href;
        x = x.substring(x.indexOf("/", 10)); // http://domain.com/foo/list.htm?trg=1 -> /foo/list.htm?trg=1 ...
        x = x.indexOf("?") == -1 ? x : x.substring(0, x.indexOf("?")); // ... /foo/list.htm
        return x;
      };

      var configureItem = function(/*json*/ item, /*json*/ parentItem) {
        var e = $(item.element);
        item.element = e;
        if (!e.length) return; //no such element

        if (parentItem) item.parentItem = parentItem;
        item.level = item.parentItem ? item.parentItem.level+1 : 0;
        item.href = item.href || href(e);
        disableClicks(e);

        item.clickFn = item.clickedFn || settings.item.clickFn;
        item.gotoFn = item.gotoFn || settings.item.gotoFn;
        if (item.href || item.clickable) e.click(function() { item.clickFn(item, model); });

        //highlight items matching current page
        var highlight = function(/*json*/ item) {
          if (!item.element.hasClass(model.css.highlight)) item.element.addClass(model.css.highlight);
          if (item.parentItem) highlight(item.parentItem);
        };

        var url = currentURL();
        if (!item.highlightOnURL) item.highlightOnURL = [];
        if (url == item.href || item.highlightOnURL.indexOf(url) != -1) highlight(item);
      };

      var configureDropdown = function(/*json*/ item) {
        if (item.dropdown) {
          item.dropdown = $.extend(true, {}, settings.dropdown, item.dropdown);
          var dd = item.dropdown;
          var dde = $(dd.element);
          if (!dde.length) return; //no such element

          dd.element = dde;
          dd.parentItem = item;
          dd.level = item.level+1;

          item.element.hover(
            function() { item.mouseover = true; expand(item); },
            function() { item.mouseover = false; collapse(item); }
          );

          dde.hover(
            function() { dd.mouseover = true; },
            function() { dd.mouseover = false; collapse(item); }
          );

          dd.items = dd.items || [];
          addScanItems(dd.element, dd.items);
          $.each(dd.items, function(dn, ditem) {
            configureItem(ditem, item);
            if (ditem.dropdown) configureDropdown(ditem);
          });
        }
      };

      var settings = $.extend(true, {}, Haala.KdxMenu.Settings, options || {});
      var model = {};
      model.css = $.extend(true, {}, settings.css, model.css);
      model.element = $(settings.element);

      if (model.element.length) {
        model.items = settings.items || [];
        addScanItems(model.element, model.items);
        $.each(model.items, function(n, item) {
          configureItem(item);
          if (item.dropdown) configureDropdown(item);
        });
      }

      model.element.data("kdx-menu", this);
      this.model = model;
    }

  };

})(jQuery);
