/**
 * jQuery pagination plugin
 * 1 ... 23 24 [25] 26 27 ... 85
 */

;(function ( $, window, document, undefined ) {

    var pluginName = 'pagination'

    // Create the plugin constructor
    function Plugin ( element, options ) {
        this.element = element
        this._name = pluginName
        this._defaults = $.fn.pagination.defaults
        this.options = $.extend( {}, this._defaults, options )
        this.init()
    }

    // Avoid Plugin.prototype conflicts
    $.extend(Plugin.prototype, {

        // Initialization logic
        init: function () {
            this.buildCache()
            this.bindEvents()

            var options = this.options
            this.model = {
              spread: options.spread,
              totalPages: options.totalPages,
              currentPage: options.currentPage,
              nextprev: options.nextprev
            }

            this.$element.addClass(options.css)
            this.render(this.model.totalPages, this.model.currentPage)
        },

        // Remove plugin instance completely
        destroy: function() {
            this.unbindEvents()
            this.$element.removeData()
        },

        // Cache DOM nodes for performance
        buildCache: function () {
            this.$element = $(this.element)
        },

        // Bind events that trigger methods
        bindEvents: function() {
            var plugin = this
        },

        // Unbind events that trigger methods
        unbindEvents: function() {
            this.$element.off('.'+this._name)
        },

        render: function(/*num*/ totalPages, /*num*/ currentPage) {
          var plugin = this,
              model  = this.model

          model.totalPages = totalPages
          model.currentPage = currentPage

          plugin.$element.empty()

          model.items = []
          var total   = model.totalPages,
              current = model.currentPage,
              spread  = model.spread
          current = current < 1 ? 1 : current > total ? total : current

          function addItem(/*int*/ n, /*str*/ html, /*str*/ type) {
            var e = n ? $("<a href='#"+n+"'>"+(html || n)+"</a>") : $(html)
            if (n == current) e.addClass("current")
            e.appendTo(plugin.$element)
            model.items.push({ element: e, num: n, type: type || "page" })

            if (n)
              e.on('click'+'.'+plugin._name, function(event) {
                event.preventDefault()
                plugin.notifyGotoPage.call(plugin, n)
              })
          }

          /* [prev] [first] [gap] [spread] [page] [spread] [gap] [last] [next] */
          if (total > 1) {
            var b
            switch (model.nextprev) {
              case "never": b = false; break
              case "always": b = true; break
              default: b = current > 1
            }
            if (b) addItem(Math.max(current-1, 1), "<span class='prev'></span>", "prev")

            var gap = "<span class='gap'></span>"
            var n
            addItem(1)
            if (total <= spread*2+5) {
              /* no gaps: 1 2 3 4 5 [6] 7 8 9 10 11 */
              for (n = 2; n <= total-1; n++) addItem(n)
            } else if (current < spread*2) {
              /* bigger right spread: 1 [2] 3 4 5 6 7 8 9 ... 15 */
              for (n = 2; n <= spread*2+3; n++) addItem(n)
              addItem(0, gap, "right gap") //right gap
            } else if (current > total-spread*2) {
              /*bigger left spread: 1 ... 7 8 9 10 11 12 13 [14] 15 */
              addItem(0, gap, "left gap") //left gap
              for (n = total-spread*2-2; n <= total-1; n++) addItem(n)
            } else {
              /* moving spread: 1 ... 5 6 7 [8] 9 10 11 ... 15   1 2 3 4 5 [6] 7 8 9 ... 15   1 [2] 3 4 5 ... 15 */
              var start = current-spread > 2 ? current-spread : 3
              var end = current+spread < total-2 ? current+spread : total-2
              if (start == 3) addItem(2); else addItem(0, gap, "left gap") //left gap (or unfold second page)
              for (n = start; n <= end; n++) addItem(n)
              if (end >= total-2) addItem(total-1); else addItem(0, gap, "right gap") //right gap (or unfold pre last page)
            }
            addItem(total)

            switch (model.nextprev) {
              case "never": b = false; break
              case "always": b = true; break
              default: b = current < total
            }
            if (b) addItem(Math.min(current+1, total), "<span class='next'></span>", "next")
          }

          plugin.notifyRender()
        },

        notifyRender: function() {
            var fn = this.options.onRender

            if ( typeof fn === 'function' ) {
                fn.call(this.element, this.model)
            }
        },

        notifyGotoPage: function(/*num*/ page) {
            var fn     = this.options.changePage,
                plugin = this,
                model  = this.model

            if ( typeof fn === 'function' ) {
              fn.call(this.element, model, page,
                function(/*bool\optional*/ success, /*num\optional*/ currentPage, /*num\optional*/ totalPages) {
                  if (success || success == undefined)
                    plugin.render(totalPages ? totalPages : model.totalPages, currentPage ? currentPage : page)
                })
            }
        }

    })

    var methods = {
        page: function (/*num*/ n) {
          var plugin = this.data("plugin_" + pluginName),
              model  = plugin.model
          plugin.render(model.totalPages, n)
        },

        update: function (/*num|optional*/ currentPage, /*num|optional*/ totalPages) {
          var plugin = this.data("plugin_" + pluginName),
              model  = plugin.model
          plugin.render(totalPages || model.totalPages, currentPage || model.currentPage)
        }
    }

    // Plugin wrapper around constructor
    $.fn.pagination = function ( options ) {
      if ( methods[options] ) {
        return methods[options].apply( this, Array.prototype.slice.call( arguments, 1 ))
      } else if ( typeof options === 'object' || !options ) {
        this.each(function() {
          if ( !$.data( this, "plugin_" + pluginName ) ) {
            $.data( this, "plugin_" + pluginName, new Plugin( this, options ) )
          }
        })
        return this
      } else {
        $.error( 'Method '+ options+' does not exist on jQuery.'+pluginName )
      }
    }

    $.fn.pagination.defaults = {
        onRender: null, // fn
        changePage: null, // fn
        spread: 3, // show 3 pages to the left and to the right from current page
        css: "pagination", // CSS class applied to element
        nextprev: "default", // show "next" and "previous" navigation: default | always | never
        totalPages: 0, // total pages
        currentPage: 1 // current page
    }

})( jQuery, window, document );
