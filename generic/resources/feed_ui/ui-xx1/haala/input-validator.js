/**
 * jQuery
 * Depends on:
 *   misc
 */

;var Haala = Haala || {}

;(function ( $, window, document, undefined ) {

  /* logic */

  Haala.InputValidator = function(/*json*/ options) {
    this.init(options)
  }

  Haala.InputValidator.Settings = {
    regexMappingFn: function(/*string*/ name, /*str*/ value) {
      return [name]
    }
  }

  Haala.InputValidator.prototype = {

    init: function(/*json*/ options) {
      this.settings = $.extend(true, {}, Haala.InputValidator.Settings, options || {})
    },

    /** return fail reason or empty */
    validateValue: function(/*str*/ value, /*{}*/ vld) {
      var THIS = this

      //required test
      if (vld.require && !value) return "require"

      //min-max length test
      if (vld.length && value) {
        var min = vld.length[0], max = vld.length.length > 0 ? vld.length[1] : 0
        if (min && value.length < min) return "min-length"
        if (max && value.length > max) return "max-length"
      }

      //regex test
      if (vld.regex && value) {
        var b = true
        $.each(vld.regex, function(n, r) {
          var regex = THIS.settings.regexMappingFn(r)
          for (var z = 0; z < regex.length; z++)
            b &= value.match(regex[z]) != null
        })
        if (!b) return "regex"
      }

      //user defined
      if (vld.userFn) {
        var reason = vld.userFn(value)
        if (reason != null) return reason
      }

      return ""
    },

    validateInput: function(/*[{}]*/ arr) {
      this.resetInput(arr)

      var THIS = this,
          valid = true
      arr.forEach(function(input) {

        if (input.validate != undefined) {
          // old format { element, validate: { require: true, fail: message }}
          var value = Haala.Misc.inputValue(input.element),
              reason = THIS.validateValue(value, input.validate)
          if (reason) input.validate.fail = reason
        } else {
          // new format { value, require: true, fail: message }
          var value = input.value,
              reason = THIS.validateValue(value, input)
          if (reason) input.fail = reason
        }

        valid &= !reason
      })
      return valid
    },

    resetInput: function(/*[{}]*/ arr) {
      arr.forEach(function(input) {

        if (input.validate != undefined) {
          // old format { element, validate: { require: true, fail: message }}
          delete input.validate.fail
        } else {
          // new format { value, require: true, fail: message }
          delete input.fail
        }

      })
    }

  }

})( jQuery, window, document );

/* Integration */

(function($) {

  Haala.InputValidator.Integ = {
    turingLength: 5,
    regexMappingFn: function(/*str*/ name) {
      var x = [];
      var rvals = Haala.Misc.messageMapping(null, "RegEx/"+name).split(" "); // RegEx/email
      for (var z = 0; z < rvals.length; z++) {
        var a = rvals[z];
        if (a != "RegEx/"+name && a != "") x.push("^"+a+"$"); //regular expression found
      }
      return x;
    },
    turingUserFn: function(/*str*/ value) {
      return value.length != Haala.InputValidator.Integ.turingLength ? "notur" : null;
    }
  };

})(jQuery);
