/**
 * jQuery
 * Depends on:
 *   misc
 *   Haala.UIValidator or Haala.InputValidator (optional)
 */

var Haala = Haala || {};

(function($) {

  /* plugin */
  $.fn.kdxPanel = function(/*json*/ options) {
    if (!options) options = {};
    options.element = this;
    return $(this).data("kdx-panel") || new Haala.KdxPanel(options);
  };

  /* logic */

  Haala.KdxPanel = function(/*json*/ options) {
    this.init(options);
  };

  Haala.KdxPanel.Settings = {
    validator: Haala.UIValidator ? new Haala.UIValidator() :
               Haala.InputValidator ? new Haala.InputValidator() : null
  };

  Haala.KdxPanel.prototype = {

    init: function(/*json*/ options) {
      this.configure(options);
    },

    collect: function() {
      var model = this.model, container = {};
      $.each(model.input, function(n, input) {
        var key = input.key;
        if (key) {
          var value = Haala.Misc.inputValue(input.element);
          if (value) container[key] = value;
        }
      });
      return container;
    },

    validate: function(/*string*/ action) {
      var model = this.model, vld = model.validator;
      if (vld)
        switch (action) {
          case "reset": return vld.resetInput(model.input);
          default: return vld.validateInput(model.input);
        }
      return true;
    },

    configure: function(/*json*/ options) {
      var configureInput = function(/*json*/ input) {
        input.element = $(input.element);
      };

      var settings = $.extend(true, {}, Haala.KdxPanel.Settings, options || {});
      var model = {};
      model.validator = model.validator || settings.validator;
      model.element = $(settings.element);

      if (model.element.length) {
        model.input = settings.input || [];
        $.each(model.input, function(n, input) {
          configureInput(input);
        });
      }

      model.element.data("kdx-panel", this);
      this.model = model;
    }

  };

})(jQuery);

/* Integration */

(function($) {

  Haala.KdxPanel.Integ = {};

  Haala.KdxPanel.Integ.DefaultOptions = {
    validator: new Haala.UIValidator({
      messageMappingFn: Haala.UIValidator.Integ.messageMappingFn,
      validator: new Haala.InputValidator({
        regexMappingFn: Haala.InputValidator.Integ.regexMappingFn
      })
    })
  };

})(jQuery);
