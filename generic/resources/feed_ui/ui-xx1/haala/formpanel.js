/**
 * jQuery form plugin
 */

;(function ( $, window, document, undefined ) {

    var pluginName = 'formpanel'

    // Create the plugin constructor
    function Plugin ( element, options ) {
        this.element = element
        this._name = pluginName
        this._defaults = $.fn.formpanel.defaults
        this.options = $.extend( {}, this._defaults, options )
        this.init()
    }

    // Avoid Plugin.prototype conflicts
    $.extend(Plugin.prototype, {

        // Initialization logic
        init: function () {
            this.buildCache()
            this.bindEvents()

            var options = this.options
            this.model = {
              spread: options.spread,
              totalPages: options.totalPages,
              currentPage: options.currentPage,
              nextprev: options.nextprev
            }
        },

        // Remove plugin instance completely
        destroy: function() {
            this.unbindEvents()
            this.$element.removeData()
        },

        // Cache DOM nodes for performance
        buildCache: function () {
            this.$element = $(this.element)
            this.fields = []

            var plugin = this

            $.each(this.options.fields, function(n, vo) {
              var e = (typeof vo.selector === 'string') ? plugin.$element.find(vo.selector) : $(vo.selector)
              var field = { name: vo.name, $element: e }
              plugin.fields.push(field)

              if (vo.validate) {
                field.validate = vo.validate
                var $valid = e.parent().find(".error")
                if ($valid.length) field.validate.$valid = $valid
              }
            })
        },

        // Bind events that trigger methods
        bindEvents: function() {
            var plugin = this
        },

        // Unbind events that trigger methods
        unbindEvents: function() {
            this.$element.off('.'+this._name)
        },

        fieldValue: function(/*selector*/ selector) {
          var e = $(selector),
              value = null
          if (e.length)
            switch (e[0].tagName) {
              case "SELECT":
              case "TEXTAREA":
              case "INPUT":
                if (e.is(":checkbox")) value = e.is(":checked") ? "on" : null
                else value = $.trim(e.val())
                break
            }
          return value
        },

        collect: function(/*{}*/ container) {
          var plugin = this
          $.each(this.fields, function(n, field) {
            container[field.name] = plugin.fieldValue(field.$element)
          })
        },

        validate: function(/*fn*/ validateCallback) {
          var values = {}
          this.collect(values)
          var fields = this.fields.filter(function(field) {
            return field.validate != undefined
          })

          delete this.model.validated

          this.notifyValidate(fields, values, validateCallback)
        },

        submit: function() {
          var plugin = this,
              values = {}
          plugin.validate(function() {
            plugin.collect(values)
            if (plugin.model.validated) plugin.notifySubmit.call(plugin, values)
          })
        },

        notifyValidate: function(/*[{}]*/ fields, /*{}*/ values, /*fn*/ validateCallback) {
            var fn = this.options.validateFields,
                plugin = this

            if ( typeof fn === 'function' ) {
                fn.call(this.element, this.model, fields, values,
                  function(/*bool\optional*/ success) {
                    if (success || success == undefined) plugin.model.validated = true
                    plugin.notifyValidateAfter.call(plugin, fields, values)
                    if (validateCallback) validateCallback()
                  })
            }
        },

        notifyValidateAfter: function(/*[{}]*/ fields, /*{}*/ values) {
            var fn = this.options.onValidateAfter,
                plugin = this

            if ( typeof fn === 'function' ) {
                fn.call(this.element, this.model, fields, values)
            }
        },

        notifySubmit: function(/*{}*/ values) {
            var fn = this.options.onSubmit,
                plugin = this

            if ( typeof fn === 'function' ) {
                fn.call(this.element, this.model, values)
            }
        },

        notifyReset: function() {
            var fn = this.options.onReset,
                plugin = this

            if ( typeof fn === 'function' ) {
                fn.call(this.element, this.model, this.fields)
            }
        }

    })

    var methods = {
        validate: function () {
          var plugin = this.data("plugin_" + pluginName)
          plugin.validate()
        },

        collect: function (/*{}*/ container) {
          var plugin = this.data("plugin_" + pluginName)
          plugin.collect(container)
        },

        submit: function () {
          var plugin = this.data("plugin_" + pluginName)
          plugin.submit()
        },

        reset: function () {
          var plugin = this.data("plugin_" + pluginName)
          plugin.notifyReset()
        },

        /** extend model */
        model: function (/*{}*/ m) {
          var plugin = this.data("plugin_" + pluginName)
          $.extend(true, plugin.model, m)
        }
    }

    // Plugin wrapper around constructor
    $.fn.formpanel = function ( options ) {
      if ( methods[options] ) {
        return methods[options].apply( this, Array.prototype.slice.call( arguments, 1 ))
      } else if ( typeof options === 'object' || !options ) {
        this.each(function() {
          if ( !$.data( this, "plugin_" + pluginName ) ) {
            $.data( this, "plugin_" + pluginName, new Plugin( this, options ) )
          }
        })
        return this
      } else {
        $.error( 'Method '+ options+' does not exist on jQuery.'+pluginName )
      }
    }

    $.fn.formpanel.defaults = {
        validateFields: null, // fn
        onValidateAfter: null, // fn
        onSubmit: null, // fn
        onReset: null, // fn
        fields: [] // form fields as { name, selector, validate }
                   // name - field name, relative column selector or element, validate data
    }

})( jQuery, window, document );
