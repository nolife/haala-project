/**
 * jQuery
 * Depends on: nothing
 */

var Haala = Haala || {};

(function($) {

  /* plugin */

  $.fn.availabilityGrid = function(/*json*/ options) {
    if (!options) options = {};
    options.element = this;
    return $(this).data("availability-grid") || new Haala.AvailabilityGrid(options);
  };

  /* logic */

  Haala.AvailabilityGrid = function(/*json*/ options) {
    this.init(options);
  };

  Haala.AvailabilityGrid.Settings = {
    mutable: true, highlight: true,
    typeSeq: ["1","3","2","4","5"],
    message: {
      mappingFn: function(/*string*/ msg, /*json*/ tokens) { return msg; }
    }
  };

  Haala.AvailabilityGrid.prototype = {

    init: function(/*json*/ options) {
      this.configure(options);
    },

    parseInput: function(/*string*/ mstr) {
      var cells = [];
      var rarr = []; //rows: [mmyyyy] 052009 062009 072009

      //mstr: [mmyyyy][days][fday(1..7))]:[type(=count)] 052009315:3:2=15:4:1=14,062009301:5=30, ...
      var parseV2 = function() {
        var cind = 0;
        var xarr = mstr.split(",");
        for (var n = 0; n < xarr.length; n++) {
          var mrow = xarr[n];
          var arr = mrow.split(":");
          var wd = arr[0].substring(8); //[day(1..7)] 1 2 3 4 5 6 7 1 2 3 4 5 6 7 1
          var dt = arr[0].substring(0,6);
          rarr.push(dt);
          for (var dc = 1; dc <= arr[0].substring(6,8); dc++) {
            cells.push({ day: wd, date: (dc<10?"0":"")+dc+dt });
            if (++wd > 7) wd = 1;
          }
          for (var z = 1; z < arr.length; z++) {
            var tc = arr[z].split("=");
            var cn = tc.length == 2 ? tc[1] : 1;
            for (var j = 0; j < cn; j++) {
              cells[cind].type = tc[0];
              cells[cind++].otype = tc[0];
            }
          }
        }
      };
      parseV2();

      //cols: [day(1..7)] 1 2 3 4 5 6 7 1 2 3 4 5 6 7 1
      var carr = [];
      for (var z = 1; z <= 37; z++) {
        var n = z;
        while (n > 7) n -= 7;
        carr.push(n);
      }

      var grid = [];
      for (var j = 0; j < rarr.length; j++) {
        grid[j] = [];
        for (var i = 0; i < carr.length; i++)
          grid[j][i] = null;
      }

      for (var n = 0; n < cells.length; n++) {
        var cell = cells[n];
        var row = rarr.indexOf(cell.date.substring(2));
        var ind = -1;
        for (var z = 0; z < carr.length; z++)
          if (grid[row][z]) ind = z;
        for (var z = ind+1; z < carr.length; z++)
          if (carr[z] == cell.day && !grid[row][z]) {
            grid[row][z] = cell;
            cell.i = z;
            cell.j = row;
            break;
          }
      }

      var model = this.model;
      var msgFn = model.messageMappingFn;
      var prefix = "grid-";
      var rttl = [], cttl = []; //rows and columns title
      for (var z = 0; z < rarr.length; z++) {
        var v = rarr[z];
        rttl.push(msgFn(prefix+"sm"+(v[0] == "0" ? v.substring(1,2) : v.substring(0,2)))+" "+v.substring(4));
      }
      for (var z = 0; z < carr.length; z++) {
        var v = carr[z];
        cttl.push(msgFn(prefix+"xd"+v));
      }
      model.state = { grid: grid, weekdayColumns: carr, mmyyyyRows: rarr, titleRows: rttl, titleColumns: cttl };
    },

    render: function() {
      var model = this.model;
      var grid = model.state.grid, weekdayColumns = model.state.weekdayColumns;
      var rttl = model.state.titleRows, cttl = model.state.titleColumns;

      /* build the table */
      var html = "";
      html += "<table cellpadding='0' cellspacing='0'>";
      for (var i = -1; i < cttl.length; i++) {
        if (i == -1) html += "<td></td>";
        else html += "<td class='column-title"+(weekdayColumns[i] > 5 ? " we" : "")+"'" +
                     " data-index='"+i+"'>"+cttl[i]+"</td>";
      }
      for (var j = 0; j < grid.length; j++) {
        html += "<tr>";
        for (var i = -1; i < grid[j].length; i++) {
          if (i == -1) html += "<td class='row-title' data-index='"+j+"'>"+rttl[j]+"</td>";
          else {
            var cell = grid[j][i];
            if (!cell) html += "<td></td>";
            else {
              var x = cell.date.substring(0,2);
              html += "<td class='day typ"+cell.type+(cell.day > 5 ? " we" : "")+"'" +
                      " rc='"+j+"_"+i+"'>"+(x[0] == "0" ? x[1] : x)+"</td>";
            }
          }
        }
        html += "</tr>";
      }
      html += "</table>";

      var table = $(html).appendTo(model.element);

      /* highlight selected day and process click on it */
      var tdays = table.find(".day");
      if (model.highlight)
        tdays.hover(
          function() {
            var rc = $(this).attr("rc").split("_");
            $(this).removeClass("highlight").addClass("highlight");
            table.find(".row-title[data-index="+rc[0]+"]").removeClass("highlight").addClass("highlight");
            table.find(".column-title[data-index="+rc[1]+"]").removeClass("highlight").addClass("highlight");
          },
          function() {
            var rc = $(this).attr("rc").split("_");
            $(this).removeClass("highlight");
            table.find(".row-title[data-index="+rc[0]+"]").removeClass("highlight");
            table.find(".column-title[data-index="+rc[1]+"]").removeClass("highlight");
          });
      if (model.mutable)
        tdays.click(function() {
          var seq = model.typeSeq;
          var ce = $(this);
          var rc = ce.attr("rc").split("_");
          var cell = grid[rc[0]][rc[1]];
          cell.type = seq.indexOf(cell.type) < seq.length-1 ? seq[seq.indexOf(cell.type)+1] : seq[0];
          for (var z = 0; z < seq.length; z++)
            ce.removeClass("typ"+seq[z]);
          ce.addClass("typ"+cell.type);
        });

      /* highlight selected row and process click on it */
      var trows = table.find(".row-title");
      if (model.highlight)
        trows.hover(
          function() {
            $(this).removeClass("highlight").addClass("highlight");
          },
          function() {
            $(this).removeClass("highlight");
          });
      if (model.mutable)
        trows.click(function() {
          var seq = model.typeSeq;
          var j = $(this).attr("data-index")
          var cells = grid[j];
          var type = -1;
          for (var i = 0; i < cells.length; i++) {
            var cell = cells[i];
            if (cell && type == -1) {
              type = cell.type;
              type = seq.indexOf(cell.type) < seq.length-1 ? seq[seq.indexOf(cell.type)+1] : seq[0];
            }
            if (cell) {
              cell.type = type;
              var ce = table.find(".day[rc="+j+"_"+i+"]");
              for (var z = 0; z < seq.length; z++)
                ce.removeClass("typ"+seq[z]);
              ce.addClass("typ"+cell.type);
             }
          }
      });
    },

    updatedCells: function() {
      var cells = [];
      var grid = this.model.state.grid || [[0]];
      for (var j = 0; j < grid.length; j++)
        for (var i = 0; i < grid[j].length; i++) {
          var cell = grid[j][i];
          if (cell && cell.type != cell.otype) cells.push(cell);
        }
      return cells;
    },

    createOutput: function() {
      if (this.updatedCells().length == 0) return null;
      var x = ""; //[mmyyyy]:[type(=count)] 052009:3:2=15:4:1=14,062009:5=30, ...
      var grid = this.model.state.grid, mmyyyyRows = this.model.state.mmyyyyRows;
      for (var j = 0; j < grid.length; j++) {
        var type = -1;
        var count = 0;
        var mstr = ","+mmyyyyRows[j];
        for (var i = 0; i < grid[j].length; i++) {
          var cell = grid[j][i];
          if (cell) {
            if (type == cell.type) count++;
            else if (type == -1) {
              type = cell.type;
              count = 1;
            } else {
              mstr += ":"+type+(count > 1 ? "="+count : "");
              type = cell.type;
              count = 1;
            }
          }
        }
        mstr += ":"+type+(count > 1 ? "="+count : "");
        x += mstr;
      }
      if (x) x = x.substring(1);
      return x;
    },

    configure: function(/*json*/ options) {
      var settings = $.extend(true, {}, Haala.AvailabilityGrid.Settings, options || {});
      var model = {
        typeSeq: settings.typeSeq, mutable: settings.mutable, highlight: settings.highlight,
        messageMappingFn: settings.message.mappingFn
      };
      model.element = $(settings.element);

      if (model.element.length) {

      }

      model.element.data("availability-grid", this);
      this.model = model;
    }

  };

})(jQuery);

/* Integration */

(function($) {

  Haala.AvailabilityGrid.Integ = {};

  Haala.AvailabilityGrid.Integ.DefaultOptions = {
    message: {
      mappingFn: function(/*string*/ msg, /*json*/ tokens) {
        var x = msg;
        x = Haala.Misc.replaceAll(x, "grid-", "cal.");
        return Haala.Misc.messageMapping(null, x, tokens);
      }
    }
  };

})(jQuery);
