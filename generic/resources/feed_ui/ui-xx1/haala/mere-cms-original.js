
/* strip::comments */

/**
 * jQuery
 * Depends on:
 *  misc
 *  UI
 *  qtip
 *  contextMenu
 *  tinymce
 */

var Haala = Haala || {};

(function($) {

  Haala.MereCms = {

    Settings: {
      message: {
        mappingFn: function(/*string*/ msg, /*json*/ tokens) { return msg; }
      },
      control: {
        editFn: function(/*json*/ model) {},
        doneFn: function(/*json*/ model) {},
        versionFn: function(/*json*/ model) {},
        exitFn: function(/*json*/ model) {}
      },
      state: {
        edit: false, //highligh editable content or not
        mySite: "", //current site   eg. mh2
        mydefSite: "", //default site   eg. mh1
        sites: [], //all input sites   eg. mh1 mh2   (same order as roots)
        roots: [] //all root input sites   eg. xx1 xx2   (can be converted to labels)
      },
      stateFn: function(/*json*/ state) {},
      anchor: {
        blinkDelay: 1000
      },
      textFrame: {
        loadFn: function(/*json*/ cmd, /*json*/ model, /*fn*/ callback, /*fn*/ fail) {},
        saveFn: function(/*json*/ cmd, /*json*/ model, /*fn*/ callback, /*fn*/ fail) {}
      },
      pictureFrame: {
        loadFn: function(/*json*/ cmd, /*json*/ model, /*fn*/ callback, /*fn*/ fail) {},
        saveFn: function(/*json*/ cmd, /*json*/ model, /*fn*/ callback, /*fn*/ fail) {}
      },
      fileFrame: {
        loadFn: function(/*json*/ cmd, /*json*/ model, /*fn*/ callback, /*fn*/ fail) {},
        saveFn: function(/*json*/ cmd, /*json*/ model, /*fn*/ callback, /*fn*/ fail) {}
      }
    },

    init: function(/*json*/ options) {
      var settings = $.extend(true, {}, Haala.MereCms.Settings, options || {});
      settings.stateFn(settings.state);

      //control
      new Haala.MereCms.Control($.extend(true, {}, settings.control, {
        message: { mappingFn: settings.message.mappingFn },
        state: settings.state
      }));

      //anchors
      if (settings.state.edit) {
        Haala.MereCms.anchorOptions = { //need to save this for "appended" fn
          message: { mappingFn: settings.message.mappingFn },
          state: settings.state,
          textFn: function(/*json*/ state) {
            new Haala.MereCms.TextFrame($.extend(true, {}, settings.textFrame, {
              message: { mappingFn: settings.message.mappingFn },
              state: state
            }));
          },
          pictureFn: function(/*json*/ state) {
            new Haala.MereCms.PictureFrame($.extend(true, {}, settings.pictureFrame, {
              message: { mappingFn: settings.message.mappingFn },
              state: state
            }));
          },
          fileFn: function(/*json*/ state) {
            new Haala.MereCms.FileFrame($.extend(true, {}, settings.fileFrame, {
              message: { mappingFn: settings.message.mappingFn },
              state: state
            }));
          }
        };
        $("*[data-cms]").each(function() {
          $(this).mereCmsAnchor(Haala.MereCms.anchorOptions);
        });

        $(document).mousedown(function(/*event*/ event) {
          var anchorClick = false;
          var e = $(event.target);
          while (e.length && !anchorClick) {
            anchorClick |= e.hasClass("cms-anchor") || e.hasClass("contextMenu");
            e = e.parent();
          }
          if (!anchorClick && !Haala.MereCms.anchorsHidden) {
            Haala.MereCms.hideAnchors();
            setTimeout(function() { Haala.MereCms.showAnchors(); }, settings.anchor.blinkDelay);
          }
        });

        Haala.MereCms.enabled = settings.state.edit;
        Haala.MereCms.showAnchors();
      }
    },

    confirm: function(/*json*/ params) {
      var id = "cms-dialog";
      if (!$("#"+id).length)
        $("body").append("<div id='"+id+"'></div>");

      $("#"+id).html(params.text);
      $("#"+id).dialog({
        title: params.title,
        buttons: [
          { text: params.buttonLabels[0],
            click: function() {
              $(this).dialog("close");
              params.confirmFn();
            }},
          { text: params.buttonLabels[1],
            click: function() {
              $(this).dialog("close");
            }}]
      });
    },

    anchorUserPictures: function(/*string*/ pfx) {
      $("img[src^='/"+pfx+"']").each(function() {
        $(this).attr("data-cms", "1:"+$(this).attr("src").substring(1).split("?")[0]+":a");
      });
      Haala.MereCms.appended();
    },

    appended: function() {
      $("*[data-cms]").each(function() {
        $(this).mereCmsAnchor(Haala.MereCms.anchorOptions);
      });
      if (Haala.MereCms.enabled && !Haala.MereCms.anchorsHidden) Haala.MereCms.showAnchors();
    },

    showAnchors: function() {
      Haala.MereCms.anchorsHidden = false;
      $("*[data-cms]").each(function() {
        $(this).mereCmsAnchor().show();
      });
    },

    hideAnchors: function() {
      Haala.MereCms.anchorsHidden = true;
      $("*[data-cms]").each(function() {
        $(this).mereCmsAnchor().hide();
      });
    }
  };



  Haala.MereCms.Control = function(/*json*/ options) {
    this.init(options);
  };

  Haala.MereCms.Control.Settings = {
  };

  Haala.MereCms.Control.prototype = {

    init: function(/*json*/ options) {
      this.configure(options);
    },

    configure: function(/*json*/ options) {
      var settings = $.extend(true, {}, Haala.MereCms.Control.Settings, options || {});
      var model = settings.state;

      var id = "cms-control";
      var html =
        "<div id='"+id+"'>" +
          (model.edit ? "<div class='done'></div>" : "<div class='edit'></div>") +
          "<div class='version'></div>" +
          "<div class='exit'></div>" +
        "</div>";
      $("body").prepend(html);
      model.element = $("#"+id);

      var elemType = function(/*element*/ e) {
        var x = null;
        if (e.hasClass("edit")) x = "edit";
        if (e.hasClass("done")) x = "done";
        if (e.hasClass("version")) x = "version";
        if (e.hasClass("exit")) x = "exit";
        return x;
      };

      model.element.find("div").qtip({
        content: {
          text: function() { return settings.message.mappingFn("tooltip-"+elemType($(this))); }
        },
        position: {
          my: "left center", at: "right center"
        },
        style: {
          widget: true
        }
      });

      model.element.find("div").click(function() {
        var e = $(this);
        e.doClick(function() {
          switch (elemType(e)) {
            case "edit":
              settings.editFn(model);
              break;
            case "done":
              settings.doneFn(model);
              break;
            case "version":
              var mf = settings.message.mappingFn;
              Haala.MereCms.confirm({
                text: mf("confirm-version"),
                title: mf("dialog-confirm-title"),
                buttonLabels: [mf("dialog-yes"), mf("dialog-no")],
                confirmFn: function() { settings.versionFn(model); }
              });
              break;
            case "exit":
              settings.exitFn(model);
              break;
          }
        });
      });

      this.model = model;
    }

  };


  $.fn.mereCmsAnchor = function(/*json*/ options) {
    if (!options) options = {};
    options.element = this;
    return $(this).data("mere-cms-anchor") || new Haala.MereCms.Anchor(options);
  };

  Haala.MereCms.Anchor = function(/*json*/ options) {
    this.init(options);
  };

  Haala.MereCms.Anchor.Settings = {
  };

  Haala.MereCms.Anchor.prototype = {

    init: function(/*json*/ options) {
      this.configure(options);
    },

    show: function() {
      var offset = this.model.element.offset();
      if (offset.top > 0 && offset.left > 0) //do not show for hidden elements
        this.model.anchorElement.css({ top: offset.top+"px", left: offset.left+"px" }).show();
    },

    hide: function() {
      this.model.anchorElement.hide();
    },

    configure: function(/*json*/ options) {
      var settings = $.extend(true, {}, Haala.MereCms.Anchor.Settings, options || {});
      var model = settings.state;
      model.element = $(settings.element);

      var anchorClass = model.element.attr("data-cms-class") || "";
      var anchor =
        $("<div class='cms-anchor "+anchorClass+"'>" +
            "<div class='icon'></div>" +
            "<div class='text'></div>" +
          "</div>");
      $("body").append(anchor);
      model.anchorElement = anchor;

      var gotoFrame = function(/*string*/ data, /*string*/ site) {
        // mode:key:opts[:suffix]
        var parr = data.split(":");
        var mode = parseInt(parr[0][0]); // 2t -> 2   1 - picture, 2/3 - text, 4 - file
        var itemId = parr[1];
        var opts = parr.length > 2 ? parr[2].split(",") : []
        switch (mode) {
          case 1:
            // mode:key:opts
            settings.pictureFn({
              itemId: itemId, opts: opts,
              sites: model.sites, roots: model.roots, site: site || model.mySite
            });
            break;
          case 4:
            // mode:key:opts
            settings.fileFn({
              itemId: itemId, opts: opts,
              sites: model.sites, roots: model.roots, site: model.mydefSite //use only the default site
            });
            break;
          default:
            // mode:key:opts[:suffix]
            settings.textFn({
              itemId: itemId, opts: opts,
              sites: model.sites, roots: model.roots,
              site: parr[0] == 3 ? model.mydefSite : site || model.mySite, //mode 3 is not multilingual - use the default site
              siteSuffix: parr.length > 3 ? parr[3] : ""
            });
        }
      };

      var addContextMenu = function(/*element*/ e, /*string*/ data) {
        e.contextMenu({ menu: "cms-anchor-menu" },
          function(action, el, pos) {
            if (action == "edit") gotoFrame(data, site);
            if ((action+"     ").substring(0, 5) == "root-") {
              var root = action.substring(5);
              var site = model.sites[model.roots.indexOf(root)];
              gotoFrame(data, site);
            }
          });
      };

      var collectionBehaviour = function(/*string*/ data, /*string*/ mod) {
        // label;mode:key:opts:suffix[;mode:key:opts:suffix...]
        var barr = data.split(";");
        if (!mod) mod = "x";

        var text = settings.message.mappingFn("anchor-"+barr[0]);
        model.anchorElement.find(".text").html(text);

        model.anchorElement.append("<div class='drop'></div>");
        var drop = model.anchorElement.find(".drop");

        $.each(barr, function(z, v) {
          if (z > 0) { //skip label
            // mode:key:opts:suffix
            var parr = v.split(":");
            var txt = "";
            if (mod == "x") txt = settings.message.mappingFn("anchor-"+parr[0]); // eg. Keywords
            if (parr[1]) {
              if (mod == "n") txt = "#"+z; // #1 .. #n
              if (mod == "n0") txt = "#"+(z-1); // #0 .. #n
              if (mod == "kv") txt = "#"+Math.floor((z+1)/2); // #1 #1 .. #n #n
              if (mod == "kv0") txt = "#"+Math.floor((z-1)/2); // #0 #0 .. #n #n
            }
            var cls = "cell mod-"+mod+" "+(z%2==0?"odd":"even");
            var cell = $("<div class='"+cls+"'>"+(parr[1] ? txt : "&nbsp;")+"</div>");
            drop.append(cell);

            if (parr[1]) cell.data("cms-data", v);
          }
        });

        var cells = model.anchorElement.find(".cell");
        $.each(cells, function(n, e) {
          e = $(e);
          if (e.data("cms-data")) {
            e.click(function() {
              e.doClick(function() { gotoFrame(e.data("cms-data")); });
            });
            addContextMenu(e, e.data("cms-data"));
          }
        });

        var zIndex = parseInt(model.anchorElement.css("z-index") || 100);
        model.anchorElement.hover(
          function() {
            $(this).css("z-index", zIndex+1);
            drop.show();
          },
          function() {
            drop.hide();
            $(this).css("z-index", zIndex);
          }
        );
      };

      var multikeyBehaviour = function(/*string*/ data) {
        // mode:key,key,key:opts:suffix
        var parr = data.split(":");
        var ndata = parr[0]; // mode:key[,key...]:opts:suffix -> label;mode:key:opts:suffix[;mode:key:opts:suffix...]
        var opts = parr.length > 2 ? parr[2].split(",") : [];
        var sfx = parr.length > 3 ? parr[3] : "";
        $.each(parr[1].split(","), function(z, v) {
          ndata += ";"+parr[0]+":"+v+(sfx ? ":"+sfx : "");
        });
        var mod = null;
        $.each(["n", "n0", "kv", "kv0"], function(z, v) { //all available mods
          if (opts.indexOf(v) != -1) mod = v;
        });
        collectionBehaviour(ndata, mod);
      };

      var defaultBehaviour = function(/*string*/ data) {
        // mode:key:opts:suffix
        var e = model.anchorElement;
        e.click(function() {
          $(this).doClick(function() { gotoFrame(data); });
        });
        addContextMenu(e, data);
      };

      //configure editable piece
      var data = model.element.attr("data-cms");
      if (data.indexOf(";") != -1) {
        // label;mode:key:opts:suffix[;mode:key:suffix...]
        collectionBehaviour(data);
      } else {
        // mode:key:opts:suffix
        var parr = data.split(":");
        var text = settings.message.mappingFn("anchor-"+parr[0]);
        model.anchorElement.find(".text").html(text);
        var f = parr[1].indexOf(",") != -1 ? multikeyBehaviour : defaultBehaviour;
        f(data);
      }

      model.element.data("mere-cms-anchor", this);
      this.model = model;
    }

  };


  Haala.MereCms.TextFrame = function(/*json*/ options) {
    this.init(options);
  };

  Haala.MereCms.TextFrame.Settings = {
  };

  Haala.MereCms.TextFrame.prototype = {

    init: function(/*json*/ options) {
      this.configure(options);
    },

    configure: function(/*json*/ options) {
      var settings = $.extend(true, {}, Haala.MereCms.TextFrame.Settings, options || {});
      var model = settings.state;

      var id = "cms-text-frame";
      $("body").append(
        "<div id='"+id+"'>" +
          "<div class='text'>" +
            "<textarea></textarea>" +
          "</div>" +
        "</div>"
      );
      model.element = $("#"+id);

      var mf = settings.message.mappingFn;
      model.lang = mf("lang-"+model.roots[model.sites.indexOf(model.site)]);
      model.textElement = model.element.find("textarea");

      /* rich edit */
      var opts = model.opts;
      var tinymceOptions = null;
      if (opts.indexOf("rf") != -1) {
        /* full */
        tinymceOptions = {
          plugins: "inlinepopups,autolink,lists,table,advimage,advhr,advlink,advlist,contextmenu,media,directionality,fullscreen,paste,layer,style,visualchars,nonbreaking",
          theme_advanced_buttons1: "code,|,undo,redo,|,bold,italic,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull,|,formatselect,fontselect,fontsizeselect",
          theme_advanced_buttons2: "bullist,numlist,|,outdent,indent,|,forecolor,backcolor,|,link,unlink,anchor,image,media,|,sub,sup,|,charmap,advhr,nonbreaking,|,ltr,rtl,|,cleanup,|,fullscreen",
          theme_advanced_buttons3: "tablecontrols,|,insertlayer,moveforward,movebackward,absolute,|,styleprops  ,|,removeformat,visualaid,visualchars"
        };
      } else if (opts.indexOf("rn") != -1) {
        /* normal */
        tinymceOptions = {
          plugins: "inlinepopups,autolink,lists,table,advimage,advhr,advlink,advlist,contextmenu,media,directionality,fullscreen,paste",
          theme_advanced_buttons1: "code,|,undo,redo,|,bold,italic,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull,|,formatselect,fontselect,fontsizeselect",
          theme_advanced_buttons2: "bullist,numlist,|,outdent,indent,|,forecolor,backcolor,|,link,unlink,image,media,|,sub,sup,|,charmap,advhr,|,ltr,rtl,|,cleanup,|,fullscreen",
          theme_advanced_buttons3: "tablecontrols"
        };
      } else if (opts.indexOf("rl") != -1) {
        /* limited */
        tinymceOptions = {
          theme_advanced_buttons1: "code,|,undo,redo,|,bold,italic,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull,|,bullist,numlist,|,outdent,indent"
        };
      } else {
        /* disabled */
        tinymceOptions = {
          force_br_newlines : true, force_p_newlines : false, forced_root_block : '',
          theme_advanced_path : false
        };
      }
      Haala.Misc.tinymce3(model.textElement,
        $.extend(true, {
          width: 600-30, height: 450-115,
          theme_advanced_buttons1: "code,|,undo,redo" ,
          skin: "o2k7", skin_variant: "silver", plugins: "inlinepopups,autolink"
        }, tinymceOptions));

      /* Hack:
         Resize TinyMCE elements to fit the dialog. Firebug shows that two elements need to be resized:
         wrapping table and its internal iframe.
            $("#cms-text-frame table.mceLayout").width("400px");
            $("#cms-text-frame iframe").width("400px");

         Table contains toolbars which may grow. Its height must be subtracted from iframe height, otherwise
         vertical scrollbar appears on dialog.
            $("#cms-text-frame table.mceLayout tr.mceFirst").height();
            $("#cms-text-frame table.mceLayout tr.mceLast").height();

         calculated height: iframe (200) = table (300) - top toolbar (80) - bottom toolbar (20)
         dialog needs 105px height gap for controls
       */
      var tinymceResize = function() {
        var table = $("#"+id+" table.mceLayout"), iframe =$("#"+id+" iframe");
        var toolbar1 = $("#"+id+" table.mceLayout tr.mceFirst"), toolbar2 = $("#"+id+" table.mceLayout tr.mceLast");
        var width = model.element.dialog('option','width'), height = model.element.dialog('option','height');
        table.width((width-30)+"px"); table.height((height-105)+"px");
        iframe.width((width-30)+"px"); iframe.height((height-105-toolbar1.height()-toolbar2.height())+"px");
      };

      model.element.dialog({
        autoOpen: false, resizable: true, zIndex: 50, width: 600, height: 450,
        buttons: [
          { text: mf("save"), click: function() { save(); }},
          { text: mf("cancel"), click: function() { $(this).dialog("close"); }}],
        title: mf("text-frame", { lang: model.lang }),
        resizeStop: tinymceResize,
        close: function() {
          Haala.MereCms.showAnchors();
          /* reset rich edit (recreated it on each frame call) */
          model.textElement.tinymce().remove();
        }
      });

      var load = function() {
        model.textElement.val("");

        settings.loadFn({ itemId: model.itemId, params: [model.siteSuffix] }, model,
          function(/*json*/ rvo) {
            $.each(rvo.result.values, function(n, vo) {
              var site = vo.site.substring(0, vo.site.length-model.siteSuffix.length);
              if (model.site == site) model.textElement.val(vo.value);
            });

            model.element.dialog("open");
          },
          Haala.MereCms.showAnchors);
      };
      setTimeout(load, 10);

      var save = function() {
        var cmd = {
          key: model.itemId,
          values: [{ site: model.site+model.siteSuffix, value: model.textElement.val() }]
        };
        settings.saveFn(cmd, model,
          function() { model.element.dialog("close"); },
          function() {});
      };

      Haala.MereCms.hideAnchors();

      this.model = model;
    }

  };



  Haala.MereCms.PictureFrame = function(/*json*/ options) {
    this.init(options);
  };

  Haala.MereCms.PictureFrame.Settings = {
  };

  Haala.MereCms.PictureFrame.prototype = {

    init: function(/*json*/ options) {
      this.configure(options);
    },

    configure: function(/*json*/ options) {
      var settings = $.extend(true, {}, Haala.MereCms.PictureFrame.Settings, options || {});
      var model = settings.state;

      var id = "cms-picture-frame";
      if (!$("#"+id).length)
        $("body").append(
          "<div id='"+id+"'>" +
            "<div class='canvas'>" +
              "<img src=''/>" +
            "</div>" +
            "<form id='cms-picture-upload-form' class='upload' action='#' onsubmit='return false;' enctype='multipart/form-data'>" +
              "<input name='dat1' type='file'/>" +
            "</form>" +
          "</div>"
        );
      model.element = $("#"+id);

      var mf = settings.message.mappingFn;
      model.lang = mf("lang-"+model.roots[model.sites.indexOf(model.site)]);
      model.canvasElement = model.element.find(".canvas");

      model.element.dialog({
        autoOpen: false, resizable: false, zIndex: 50, width: 600, height: 400,
        buttons: [
          { text: mf("upload"), click: function() { save(); }},
          { text: mf("cancel"), click: function() { $(this).dialog("close"); }}],
        close: Haala.MereCms.showAnchors
      });

      var load = function() {
        model.canvasElement.find("img").attr("src", "");

        settings.loadFn({ itemId: model.itemId }, model,
          function(/*json*/ rvo) {
            var f = function() {
              $.each(rvo.result.items, function(n, vo) {
                if (model.site == vo.site) {

                    var isize = vo.size;
                    var iw = isize.width, ih = isize.height;
                    var cw = model.canvasElement.width(), ch = model.canvasElement.height();

                    //select the best fit
                    var fw = iw, fh = ih;
                    if (cw < iw || ch < ih) {
                      var ratio = iw/ih;
                      var xh = cw/ratio, xp = cw*xh; //fit by width
                      var yw = ch*ratio, yp = yw*ch; //fit by height
                      if (xp > yp && ch >= xh) { fw = cw; fh = xh; }
                      else if (yp > xp && cw >= yw) { fw = yw; fh = ch; }
                      else if (ch >= xh) { fw = cw; fh = xh; }
                      else { fw = yw; fh = ch; }
                    }

                    var img = model.canvasElement.find("img");
                    img.css({ width: fw+"px", height: fh+"px", margin: ((ch-fh)/2)+"px 0 0 "+((cw-fw)/2)+"px" });
                    img.attr("src", "/"+rvo.result.path+"?site="+vo.site+"&nc=1&salt="+Math.random());

                }
              });
            };
            setTimeout(f, 200);

            var size = rvo.result.items[0].size;
            model.element.dialog("option", "title", mf("picture-frame", { size: size.width+"x"+size.height, lang: model.lang }));
            model.element.dialog("open");
          },
          Haala.MereCms.showAnchors);
      };
      setTimeout(load, 10);

      var save = function() {
        var params = [];
        if (model.opts.indexOf("a") == -1) params.push("same-size");
        settings.saveFn({ path: model.itemId, site: model.site, params: params }, model,
          function() { model.element.dialog("close"); },
          function() {});
      };

      Haala.MereCms.hideAnchors();

      this.model = model;
    }

  };


  Haala.MereCms.FileFrame = function(/*json*/ options) {
    this.init(options);
  };

  Haala.MereCms.FileFrame.Settings = {
  };

  Haala.MereCms.FileFrame.prototype = {

    init: function(/*json*/ options) {
      this.configure(options);
    },

    configure: function(/*json*/ options) {
      var settings = $.extend(true, {}, Haala.MereCms.FileFrame.Settings, options || {});
      var model = settings.state;
      var mf = settings.message.mappingFn;

      var id = "cms-file-frame";
      if (!$("#"+id).length)
        $("body").append(
          "<div id='"+id+"'>" +
            "<div class='link'>" +
              "<a href='' target='_blank'>"+mf("file-open")+"</a>" +
            "</div>" +
            "<form id='cms-file-upload-form' class='upload' action='#' onsubmit='return false;' enctype='multipart/form-data'>" +
              "<input name='dat1' type='file'/>" +
            "</form>" +
          "</div>"
        );
      model.element = $("#"+id);

      model.element.dialog({
        autoOpen: false, resizable: false, zIndex: 50,
        width: 600, height: 400,
        buttons: [
          { text: mf("upload"), click: function() { save(); }},
          { text: mf("cancel"), click: function() { $(this).dialog("close"); }}],
        title: mf("file-frame"),
        close: Haala.MereCms.showAnchors
      });

      model.linkElement = model.element.find(".link");

      var load = function() {
        model.linkElement.find("a").attr("href", "");

        settings.loadFn({ itemId: model.itemId }, model,
          function(/*json*/ rvo) {
            $.each(rvo.result.items, function(n, vo) {
              if (model.site == vo.site) {
                var link = model.linkElement.find("a");
                link.attr("href", "/bin.htm?path="+rvo.result.path+"&site="+vo.site+"&nc=1&salt="+Math.random());
              }
            });
            model.element.dialog("open");
          },
          Haala.MereCms.showAnchors);
      };
      setTimeout(load, 10);

      var save = function() {
        var params = [];
        if (model.opts.indexOf("a") == -1) params.push("same-format");
        settings.saveFn({ path: model.itemId, site: model.site, params: params }, model,
          function() { model.element.dialog("close"); },
          function() {});
      };

      Haala.MereCms.hideAnchors();

      this.model = model;
    }

  };

})(jQuery);

/* Integration */

(function($) {

  Haala.MereCms.Integ = {};

  Haala.MereCms.Integ.DefaultOptions = {
    state: {
      edit: false
    },
    message: {
      mappingFn: function(/*string*/ msg, /*json*/ tokens) {
        return Haala.Misc.messageMapping("#cms-messages", msg, tokens);
      }
    },
    stateFn: function(/*json*/ state) {
      state.mySite = Haala.Misc.systemVar("mySite");
      state.mydefSite = Haala.Misc.systemVar("mydefSite");
      state.edit = Haala.Misc.systemVar("cmsEdit") == "1";
      state.sites = Haala.Misc.systemVar("mineInputSites");
      state.roots = Haala.Misc.systemVar("inputSites");
    },
    control: {
      editFn: function(/*json*/ model) {
        Haala.AJAX.call("body", CmsF.status, { target: 1 },
                function() { document.location = document.location.href; return Haala.AJAX.DO_NOTHING; });
      },
      doneFn: function(/*json*/ model) {
        Haala.AJAX.call("body", CmsF.status, { target: 2 },
                function() { document.location = document.location.href; return Haala.AJAX.DO_NOTHING; });
      },
      versionFn: function(/*json*/ model) {
        Haala.AJAX.call("body", CmsF.incVersion, { site: model.state.mydefSite },
                function() { document.location = document.location.href; return Haala.AJAX.DO_NOTHING; });
      },
      exitFn: function(/*json*/ model) {
        Haala.AJAX.call("body", CmsF.status, { target: 3 },
                function() { document.location = "/"; return Haala.AJAX.DO_NOTHING; });
      }
    },
    textFrame: {
      loadFn: function(/*json*/ cmd, /*json*/ model, /*fn*/ callback, /*fn*/ fail) {
        Haala.AJAX.call("body", CmsF.loadText, cmd, callback, fail);
      },
      saveFn: function(/*json*/ cmd, /*json*/ model, /*fn*/ callback, /*fn*/ fail) {
        Haala.AJAX.call("body", CmsF.editText, cmd, callback, fail);
      }
    },
    pictureFrame: {
      loadFn: function(/*json*/ cmd, /*json*/ model, /*fn*/ callback, /*fn*/ fail) {
        Haala.AJAX.call("body", CmsF.loadPic, cmd, callback, fail);
      },
      saveFn: function(/*json*/ cmd, /*json*/ model, /*fn*/ callback, /*fn*/ fail) {
        var form = $("#cms-picture-upload-form").uploadForm(
          $.extend(true, {}, Haala.UploadForm.Integ.DefaultOptions, {
            uploadSuccessFn: function(/*json*/ model) {
              Haala.AJAX.call("body", CmsF.editPic, cmd, callback, fail);
            }
          }));
        form.upload();
        form.destroy();
      }
    },
    fileFrame: {
      loadFn: function(/*json*/ cmd, /*json*/ model, /*fn*/ callback, /*fn*/ fail) {
        Haala.AJAX.call("body", CmsF.loadFile, cmd, callback, fail);
      },
      saveFn: function(/*json*/ cmd, /*json*/ model, /*fn*/ callback, /*fn*/ fail) {
        var form = $("#cms-file-upload-form").uploadForm(
          $.extend(true, {}, Haala.UploadForm.Integ.DefaultOptions, {
            uploadSuccessFn: function(/*json*/ model) {
              Haala.AJAX.call("body", CmsF.editFile, cmd, callback, fail);
            }
          }));
        form.upload();
        form.destroy();
      }
    }
  };

  Haala.MereCms.Integ.init = function() {
    new Haala.MereCms.init(Haala.MereCms.Integ.DefaultOptions);
    if (Haala.MereCms.enabled) Haala.MereCms.anchorUserPictures("web/my/");
  };

})(jQuery);
