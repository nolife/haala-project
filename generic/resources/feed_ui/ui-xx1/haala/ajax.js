/**
 * jQuery
 * Depends on:
 *   messages
 */

var Haala = Haala || {};

(function($) {

  Haala.AJAX = {

    DO_NOTHING: 1, /* if returned from onSuccess or onFail will halt further operations */
    RELEASE_LOCK: 2, /* release lock on fail or success */
    CLOSE_FRAME: 4, /* close message frame on fail or success (decrease counter) */
    HANDLE_ERROR: 8, /* handle error on fail */
    ACQURE_LOCK: 16, /* acquire lock before call */
    SHOW_FRAME: 32, /* show message frame on call (increase counter) */

    /** Make AJAX call */
    call: function(/*selector*/ lock, /*fn*/ target, /*json*/ command, /*fn*/ onSuccess, /*fn*/ onFail, /*int*/ opts) {
      var ajax = Haala.AJAX;
      opts = opts ? opts : ajax.ACQURE_LOCK|ajax.SHOW_FRAME|ajax.RELEASE_LOCK|ajax.CLOSE_FRAME|ajax.HANDLE_ERROR;
      lock = $(lock);
      if (!(opts & ajax.ACQURE_LOCK) || ajax.aquireLock(lock)) {
        if (opts & ajax.SHOW_FRAME) Haala.Messages.showAJAX();

        var fail = function(data) {
          var x = parseInt(opts) >= 0 ? opts : ajax.RELEASE_LOCK|ajax.CLOSE_FRAME|ajax.HANDLE_ERROR;
          x = onFail ? onFail(data) || x : x;
          if (x & ajax.RELEASE_LOCK) ajax.releaseLock(lock);
          if (x & ajax.CLOSE_FRAME) Haala.Messages.closeAJAX();
          if (x & ajax.HANDLE_ERROR) ajax.handleError(data);
        };

        var success = function(data) {
          var x = opts ? opts : ajax.RELEASE_LOCK|ajax.CLOSE_FRAME;
          x = onSuccess ? onSuccess(data) || x : x;
          if (x & ajax.RELEASE_LOCK) ajax.releaseLock(lock);
          if (x & ajax.CLOSE_FRAME) Haala.Messages.closeAJAX();
        };

        target(command, {
          callback: function(data) { data.failed ?  fail(data) : success(data); },
          errorHandler: function(error, ex) {
            var data = { errorCodes: [], errorsText: [] };
            if (ex.name == "dwr.engine.timeout") data.errorCodes.push(13);
            else data.errorsText.push(error);
            fail(data);
          },
          timeout: 60000
        });

      }
    },

    /** Set lock on a selector */
    aquireLock: function(/*selector*/ sel) {
      sel = $(sel);
      if (sel.data("ajax-call-lock") == "y") return false;
      sel.data("ajax-call-lock", "y");
      return true;
    },

    /** Remove lock from a selector */
    releaseLock: function(/*selector*/ sel) {
      sel = $(sel);
      sel.data("ajax-call-lock", "n");
    },

    /** Default error handling */
    handleError: function(/*json*/ data) {
      var arr = data.errorCodes || [];

      /* special code to refresh the page */
      if (arr.indexOf(5) != -1) {
        document.location = document.location.href;
        return;
      }

      /* special code to redirect to downtime */
      if (arr.indexOf(10) != -1) {
        document.location = "/downtime.htm";
        return;
      }

      /* error text provided */
      if (data.errorsText.length) {
        Haala.Messages.showOK(data.errorsText[0], "title-error");
        return;
      }

      /* fetch error text by code */
      for (var z = 0; z < arr.length; z++) {
        var key = "fcall.e"+arr[z];
        var fctext = Haala.getLabel().get(key);
        if (fctext && fctext != key) {
          Haala.Messages.showOK(fctext, "title-error");
          return;
        }
      }

      /* unknown error */
      Haala.Messages.showOK("fcall.e", "title-error");
    }

  };

})(jQuery);
