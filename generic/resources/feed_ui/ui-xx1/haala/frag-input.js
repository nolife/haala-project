/**
 * jQuery
 * Depends on: misc
 */

var Haala = Haala || {};

(function($) {

  /* plugin */

  $.fn.fragInput = function(/*json*/ options) {
    if (!options) options = {};
    options.element = this;
    return $(this).data("frag-input") || new Haala.FragInput(options);
  };

  /* logic */

  Haala.FragInput = function(/*json*/ options) {
    this.init(options);
  };

  Haala.FragInput.Settings = {
    showPerRow: 3, scanInput: true,
    renderedFn: function(/*json*/ model) { }, //deprecated in favour of listeners
    initFn: function(/*json*/ model) { }
  };

  Haala.FragInput.prototype = {

    init: function(/*json*/ options) {
      this.configure(options);
      var model = this.model;
      model.initFn(model);
    },

    refresh: function(/*int*/ row) {
      var model = this.model;
      row = row || 0, valued = true, fireEvent = false;
      $.each(model.input, function(n, input) {
        if (input.element.is(":visible") || input.element.val()) row = input.row;
        if (input.element.length && row == input.row) valued &= input.element.val() != "";
      });
      if (valued && row) row++;
      if (row != model.visibleRow) {
        $.each(model.input, function(n, input) {
          if (input.row <= row && !input.element.is(":visible")) input.element.show();
        });
        fireEvent = true;
      }
      model.visibleRow = row;
      if (fireEvent) {
        model.renderedFn(model);
        model.listener.fire("rendered");
      }
    },

    showFirstRow: function() {
      this.refresh(1);
    },

    configure: function(/*json*/ options) {
      var settings = $.extend(true, {}, Haala.FragInput.Settings, options || {});
      var model = {
        showPerRow: settings.showPerRow, visibleRow: 0, renderedFn: settings.renderedFn, initFn: settings.initFn,
        userData: settings.userData
      };
      model.element = $(settings.element);
      var THIS = this;

      var addScanInput = function(/*element*/ e, /*[json]*/ input) {
        if (settings.scanInput)
          e.children("input").each(function() {
            var elem = $(this), same = false;
            $.each(input, function(n, inp) { same |= elem[0] == $(inp.element)[0]; });
            if (!same) input.push({ element: elem });
          });
      };

      var configureInput = function(/*int*/ n, /*json*/ input) {
        var e = $(input.element);
        input.element = e;
        if (!e.length) return; //no such element

        input.row = Math.floor(n/model.showPerRow)+1;

        e.keyup(function() { THIS.refresh(); });
      };

      if (model.element.length) {
        model.input = settings.input || [];
        addScanInput(model.element, model.input);
        $.each(model.input, function(n, input) {
          configureInput(n, input);
        });

        setTimeout(function() { THIS.refresh(); }, 10);
      }

      Haala.Misc.eventListener(model, this);

      model.element.data("frag-input", this);
      this.model = model;
    }

  };

})(jQuery);

