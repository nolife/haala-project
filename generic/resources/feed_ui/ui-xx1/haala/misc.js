
var Haala = Haala || {};

(function($) {

  /** Scroll up or down until a selector is visible */
  $.fn.toViewport = function(/*int*/ offset) {
    return this.each(function() {
      offset = offset || 100;
      var etop = $(this).offset().top;
      var wtop = $(window).scrollTop();
      var wheight = $(window).height();
      if (wtop > etop-offset || wtop+wheight < etop+offset) $(window).scrollTop(etop-offset);
    });
  };

  /** Animate a selector as mouse click effect */
  $.fn.doClick = function(/*fn*/ afterFn, /*event*/ event) {
    this.animate({ opacity: .5 }, 140).animate({ opacity: 1 }, 140, function() { afterFn(event); });
  };

  /** On mouse click animate as in doClick and prevent a default event */
  $.fn.aclick = function(/*fn*/ afterFn) {
    return this.click(function(event) {
      event.preventDefault();
      $(this).doClick(afterFn, event);
    });
  };

  Haala.Misc = {

    /** Parametrize label with tokens */
    parametrize: function(/*string*/ value, /*json*/ tokens) {
      if (tokens) $.each(tokens, function(k, v) {
        value = value.replace("{"+k+"}", v);
      });
      return value;
    },

    /** Convert selector HTML into JSON */
    evalJSON: function(/*selector*/ x) {
      return eval("("+$(x).html()+")");
    },

    /** Return value of system variable extracted from page tag */
    systemVar: function(/*string*/ name, /*string*/ format) {
      var json = Haala.Misc.evalJSON("#sys-ivars");
      var value = json[name];
      if (value)
        switch (format) {
          case "cvs-to-array":
            value = value.split(",");
            break;
        }
      return value;
    },

    /** Use on kdxPanel.collect to extract input */
    collectInput: function(/*json*/ container, /*[string]*/ names, /*[string]*/ newNames) {
      var item = {};
      $.each(names, function(n, name) {
        item[newNames ? newNames[n] : name] = container[name];
      });
      return item;
    },

    /** Use on kdxPanel.collect to extract siteable input */
    collectSiteableInput: function(/*json*/ container, /*[string]*/ names, /*[string]*/ newNames, /*[string]*/ sites) {
      var items = [];
      sites = sites || Haala.Misc.systemVar("inputSites");
      $.each(sites, function(z, site) {
        var item = { site: site };
        items.push(item);
        $.each(names, function(n, name) {
          item[newNames ? newNames[n] : name] = container[name+"_"+site];
        });
      });
      return items;
    },

    /** Return parametrized message label.
      * Value extracted from a selector or labels object.
      */
    messageMapping: function(/*selector*/ sel, /*string*/ msg, /*json*/ tokens) {
      var msg =
        $((sel || "body")+" *[data-key='"+msg+"']").html() ||
        Haala.getLabel().getOrElse(msg, null) ||
        $("#sys-messages"+" *[data-key='"+msg+"']").html() ||
        msg;
      msg = Haala.Misc.parametrize(msg, tokens);
      return msg;
    },

    /** Replace all occurrence of string "a" with string "b" */
    replaceAll: function(/*string*/ text, /*string*/ a, /*string*/ b) {
      a = a.replace(/([.*+?^=!:${}()|\[\]\/\\])/g, "\\$1");
      return text.replace(new RegExp(a, "g"), b);
    },

    /** Initialize qtip */
    tooltips: function(/*selector*/ sel) {
      var e = $(sel || "body");
      e.find("*[title]").each(function() {
        $(this).attr("tooltip", $(this).attr("title"))
      }).removeAttr("title");
      e.find("*[tooltip]").qtip({
        content: {
          text: function() { return $(this).attr("tooltip"); }
        },
        position: {
          viewport: $(window)
        },
        style: {
          classes: "ui-tooltip-shadow"
        }
      });
    },

    /** Return outer selector HTML */
    outerHTML: function(/*selector*/ sel) {
      return $("<div/>").append($(sel).clone()).remove().html();
    },

    /** Swap two HTML nodes */
    swapNodes: function(/*node*/ a, /*node*/ b) {
      var aparent = a.parentNode;
      var asibling = a.nextSibling === b ? a : a.nextSibling;
      b.parentNode.insertBefore(a, b);
      aparent.insertBefore(b, asibling);
    },

    /** Load remote script */
    loadScript: function(/*string*/ url,/*fn*/ callback) {
      if (Haala.getStore().get("loaded_"+url)) callback();
      else $.getScript(url, function() {
        Haala.getStore().set("loaded_"+url, true);
        callback();
      });
    },

    /** Re-load remote script */
    reloadScript: function(/*string*/ url,/*fn*/ callback) {
      Haala.getStore().remove("loaded_"+url);
      Haala.Misc.loadScript(url, callback);
    },

    /** Init TinyMCE v3 jQuery plugin on a selector */
    tinymce3: function(/*selector*/ sel, /*json*/ options) {
      Haala.Misc.loadScript("/web/endorsed/tinymce-3.4.7/jquery.tinymce.js", function() {
        $(sel).tinymce($.extend(true, {
            script_url: "/web/endorsed/tinymce-3.4.7/tiny_mce.js",
            theme: "advanced" ,
            plugins: "",
            theme_advanced_toolbar_align : "left",
            theme_advanced_toolbar_location: "top",
            theme_advanced_statusbar_location : "bottom",
            theme_advanced_buttons1: "undo,redo,|,bold,italic,underline,strikethrough",
            theme_advanced_buttons2: "",
            theme_advanced_buttons3: "",
            editor_deselector: "noRichText",
            language: Haala.Misc.systemVar("lang")
          }, options || {}));
      });
    },

    /** Init TinyMCE v4 jQuery plugin on a selector */
    tinymce4: function(/*selector*/ sel, /*json*/ options) {
      Haala.Misc.loadScript("/web/endorsed/tinymce-4.9.1/jquery.tinymce.min.js", function() {
        $(sel).tinymce($.extend(true, {
            script_url: "/web/endorsed/tinymce-4.9.1/tinymce.min.js",
            theme: "modern",
            plugins: [],
            toolbar1: "undo redo | bold italic underline strikethrough",
            menubar: false,
            toolbar_items_size: 'small',
            language: Haala.Misc.systemVar("lang"),
            relative_urls : false,
            remove_script_host : false,
            convert_urls : false
          }, options || {}));
      });
    },

    /** Load Google Maps API */
    googleMapsAPI: function(/*str*/ apiKey, /*fn*/ callback) {
      /* Step 1: Load up the google core API */
      Haala.Misc.loadScript("http://www.google.com/jsapi", function() {
        /* Step 2: Once the core is loaded, use the google loader to bring in the maps module.
           The callback function contains my map init and population code. */
        google.load('maps', '3', { callback: callback, other_params:'key='+apiKey+'&language='+Haala.Misc.systemVar("lang")});
      });
    },

    /** Extract parameter value from URL-like string */
    getParameter: function(/*string*/ s, /*string*/ name) {
      var xs = Haala.Misc.getParameters(s, name);
      return xs.length > 0 ? xs[0] : null;
    },

    /** Extract parameters value from URL-like string */
    getParameters: function(/*string*/ s, /*string*/ name) {
      s = s || "";
      if (s.indexOf("?") != -1) s = $.trim((s+" ").substring(s.indexOf("?")+1, s.length));
      if (s.indexOf("#") != -1) s = s.substring(0, s.indexOf("#"));
      var xs = s.split("&"), x = [];
      for (var z = 0; z < xs.length; z++)
        if (xs[z].indexOf(name+"=") == 0) x.push(xs[z].substring(xs[z].indexOf("=")+1));
      return x;
    },

    /** Return value of input */
    inputValue: function(/*selector*/ input) {
      input = $(input);
      var value = null;
      if (input.length)
        switch (input[0].tagName) {
          case "SELECT":
          case "TEXTAREA":
          case "INPUT":
            if (input.is(":checkbox")) value = input.is(":checked") ? "on" : null;
            else value = $.trim(input.val());
            break;
        }
      return value;
    },

    /** Turn a string into camel case by removing dashes and underscores.
     *  e.g. first-name  -->  firstName
     */
    camelCase: function(/*str*/ str) {
      var x = "", cap = false;
      for (var z = 0; z < str.length; z++ ) {
        var c = str[z];
        if (c == "-" || c == "_" || c == " ") cap = true;
        else {
          if (cap) c = c.toUpperCase();
          x += c;
          cap = false;
        }
      }
      return x;
    },

    /** Push a function into the queue which executes on document ready event. */
    onDocReady: function(/*fn*/ f) {
      Haala.getStore().push("doc:ready", f);
    },

    /** Add listener functionality to an object.
     *  Listeners react on events fired by the object and receive parameters (which also includes the object and
     *  the event fired) as a single argument.
     */
    eventListener: function(/*object*/ obj, /*object*/ mthobj) {
      obj.listener = { listeners: [] };
      obj.listener.add = function(/*str*/ event, /*fn*/ callback) {
        obj.listener.listeners.push({ event: event, callback: callback });
      };
      obj.listener.fire = function(/*str*/ event, /*json*/ params) {
        params = $.extend(true, params || {}, { event: event, target: obj });
        $.each(obj.listener.listeners, function() {
          if (this.event == event || this.event == "*") this.callback(params);
        });
      };

      /* attach methods to method-object if specified */
      if (mthobj) mthobj.addListener = function(/*str*/ event, /*fn*/ callback) { obj.listener.add(event, callback); };
    },

    /** Copy file inputs from one form into another.
      * Steps 1. include file inputs into a common form which has other inputs (text, checkboxes etc)
      *       2. on submit: validate all input errors
      *       3. on submit: copy the files into a hidden upload form and upload
      *       4. on upload form success: call a facade with common form inputs, files should be there waiting
      */
    copyFormFiles: function(/*selector*/ srcForm, /*element*/ trgForm) {
      var orgElem = $(srcForm),
          trgElem = $(trgForm)
      trgElem.find("input[type=file]").each(function() {
        $(this).remove()
      })    
      $(srcForm).find("input[type=file]").each(function() {
        var clone = $(this).clone()
        $(this).after(clone).appendTo(trgElem)
      })
    }

  };

})(jQuery);
