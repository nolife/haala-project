
var Haala = Haala || {};

/** Client-side swappable storage implementations: read/write/remove */

(function($) {

  Haala.Cookie = {

    write: function(/*str*/ name, /*str*/ value, /*num|optional*/ seconds, /*str|optional*/ path) {
        path = path || "/"
        seconds = seconds || 60*60*24*30 // 1 month
        var date = new Date()
        date.setTime(date.getTime()+seconds*1000)
        var expires = "; expires="+date.toGMTString()
        document.cookie = name+"="+value+expires+"; path="+path
    },

    read: function(/*str*/ name) {
        var nameEQ = name+"="
        var ca = document.cookie.split(';')
        for (var i = 0; i < ca.length; i++) {
          var c = ca[i]
          while (c.charAt(0) == ' ') c = c.substring(1, c.length);
          if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length, c.length)
        }
        return ""
    },

    remove: function(/*str*/ name, /*str|optional*/ path) {
        path = path || "/"
        document.cookie = name+"=; expires=Thu, 01-Jan-70 00:00:01 GMT"+";path="+path
    }

  }

  Haala.LocalStorage = {

      write: function(/*str*/ key, /*str*/ value) {
          localStorage.setItem(key, value)
      },

      read: function(/*str*/ key) {
          return localStorage.getItem(key)
      },

      remove: function(/*str*/ key) {
          localStorage.removeItem(key)
      }

  }

  Haala.ExpirableLocalStorage = {

    /** write an item to the local storage with expiry metadata */
    write: function(/*str*/ key, /*str*/ value, /*num|optional*/ seconds) {
        // remove all expired items on each write
        Haala.ExpirableLocalStorage._removeExpired()

        // save item expiry metadata (removing the item to replace its metadata)
        Haala.ExpirableLocalStorage.remove(key)
        var expiry = Haala.ExpirableLocalStorage._expiryHash(key, seconds),
            arr = /*[{key,timestamp,seconds}]*/ Haala.ExpirableLocalStorage._expirable()
        arr.push(expiry)
        Haala.ExpirableLocalStorage._expirable(arr)

        localStorage.setItem(key, value)
    },

    /** read an item from the local storage and extend its expiry */
    read: function(/*str*/ key) {
        var arr = /*[{key,timestamp,seconds}]*/ Haala.ExpirableLocalStorage._expirable()
        arr.forEach(function(data) {
            if (data.key == key) {
                var expiry = Haala.ExpirableLocalStorage._expiryHash(key, data.seconds)
                data.timestamp = expiry.timestamp
            }
        })
        return localStorage.getItem(key)
    },

    /** remove an item from the local storage with its expiry metadata */
    remove: function(/*str*/ key) {
        var arr = /*[{key,timestamp,seconds}]*/ Haala.ExpirableLocalStorage._expirable(),
            index = -1
        arr.forEach(function(data, n) {
            if (data.key == key) {
                index = n
            }
        })
        if (index > -1) {
            arr.splice(index, 1)
            Haala.ExpirableLocalStorage._expirable(arr)
        }
        localStorage.removeItem(key)
    },

    /** remove all expired items */
    _removeExpired: function() {
        // get all expired items
        var arr = /*[{key,timestamp,seconds}]*/ Haala.ExpirableLocalStorage._expirable(),
            now = new Date().getTime(),
            expiredItems = arr.filter(function(data) {
                return now > data.timestamp
            })

        // remove expired items one by one
        expiredItems.forEach(function(data) {
            Haala.ExpirableLocalStorage.remove(data.key)
        })
     },

    /** getter and setter for the expirable array */
    _expirable: function(/*[{key,timestamp,seconds}]*/ arr) {
        if (arr) { // setter
            localStorage.setItem("expirable", JSON.stringify(arr))
        } else { // getter
            return JSON.parse(localStorage.getItem("expirable")) || []
        }
    },

    /* create a new expiry metadata hash */
    _expiryHash: function(/*str*/ key, /*num|optional*/ seconds) {
        seconds = seconds || 60*60*24*30 // 1 month
        var date = new Date()
        date.setTime(date.getTime()+seconds*1000)
        return { key: key, timestamp: date.getTime(), seconds: seconds }
    }
  }

})(jQuery);
