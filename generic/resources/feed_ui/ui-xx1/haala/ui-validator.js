/**
 * jQuery
 * Depends on:
 *   Haala.InputValidator (can be substituted)
 */

;var Haala = Haala || {}

;(function ( $, window, document, undefined ) {

  /* logic */

  Haala.UIValidator = function(/*json*/ options) {
    this.init(options)
  }

  Haala.UIValidator.Settings = {
    validator: new Haala.InputValidator(),
    messageMappingFn: function(/*str*/ reason, /*{}*/ vld, /*str*/ tagName) {
      return { template: reason, params: [] }
    }
  }

  Haala.UIValidator.prototype = {

    init: function(/*json*/ options) {
      this.settings = $.extend(true, {}, Haala.UIValidator.Settings, options || {})
    },

    parametrize: function(/*string*/ template, /*[string[*/ params) {
      $.each(params, function(n, keyval) {
        var key = $.trim(keyval.substring(0, keyval.indexOf(":")))
        var val = $.trim(keyval.substring(keyval.indexOf(":")+1))
        template = template.replace("{"+key+"}", val)
      })
      return template
    },

    validateInput: function(/*[{}]*/ arr) {
      this.resetInput(arr)
      var b = this.settings.validator.validateInput(arr)

      var THIS = this
      arr.forEach(function(input) {

        if (input.validate != undefined) {
          // old format { element, validate: { require: true, fail: message }}
          if (input.validate.fail) {
            var reason = input.validate.fail,
                tagName = input.element.is(":checkbox") ? "CHECKBOX" : input.element[0].tagName,
                template = THIS.settings.messageMappingFn(reason, input.validate, tagName) || reason,
                message = THIS.parametrize(template.template, template.params)
            $(input.validate.element).html(message).show()
          }
        } else {
          // new format { $valid, value, tagName, require: true, fail: message }
          if (input.fail) {
            var reason = input.fail,
                tagName = input.tagName,
                template = THIS.settings.messageMappingFn(reason, input, tagName) || reason,
                message = THIS.parametrize(template.template, template.params)
            input.$valid.html(message).show()
          } 
        }

      })

      return b
    },

    resetInput: function(/*[{}]*/ arr) {
      this.settings.validator.resetInput(arr)

      arr.forEach(function(input) {

        if (input.validate != undefined) {
          // old format { element, validate: { require: true, fail: message }}
          $(input.validate.element).hide().html("")
        } else {
          // new format { $valid, value, tagName, require: true, fail: message }
          if (input.$valid) input.$valid.hide().html("")
        }

      })
    }
  }

})( jQuery, window, document );

/* Integration */

(function($) {

  Haala.UIValidator.Integ = {
    messageMappingFn: function(/*str*/ reason, /*{}*/ vld, /*str*/ tagName) {
      var msgFn = Haala.Misc.messageMapping;
      var x = { template: reason, params: [] };
      switch (reason) {
        case "regex":
          var msg = msgFn(null, "valid.re/"+vld.regex[0]);
          x.template = msg.indexOf("valid.re/") == -1 ? msg : msgFn(null, "valid.erinp");
          break;
        case "min-length":
          x.template = msgFn(null, "valid.shtxt");
          x.params = ["min:"+vld.length[0]];
          break;
        case "require":
          if (tagName == "CHECKBOX") x.template = msgFn(null, "valid.nochk");
          else x.template = msgFn(null, "valid.notxt");
          break;
        default:
          x.template = msgFn(null, reason.indexOf(" ") == -1 ? "valid."+reason : reason);
          break;
      }
      return x;
    },
    validator: new Haala.InputValidator({
      regexMappingFn: Haala.InputValidator.Integ.regexMappingFn
    })
  };

})(jQuery);
