
var Haala = Haala || {};

(function($) {

  Haala.Messages = {

    showOK: function(/*string*/ text, /*string*/ title, /*fn*/ onCloseFn) {
      new Haala.Messages.Ok({ title: title, text: text, onCloseFn: onCloseFn,
        message: { mappingFn: Haala.Messages.Integ.messageMappingFn }});
    },

    showYN: function(/*string*/ text, /*string*/ title, /*fn*/ onYesFn, /*fn*/ onNoFn) {
      new Haala.Messages.YesNo({ title: title, text: text, onYesFn: onYesFn, onNoFn: onNoFn,
        message: { mappingFn: Haala.Messages.Integ.messageMappingFn }});
    },

    ajaxFrame: function() {
      if (!Haala.getStore().get("ajax-frame")) {
        var frame = new Haala.Messages.AJAX({});
        Haala.getStore().set("ajax-frame", frame);
      }
      return Haala.getStore().get("ajax-frame");
    },

    showAJAX: function() {
      Haala.Messages.ajaxFrame().increase();
    },

    closeAJAX: function() {
      Haala.Messages.ajaxFrame().decrease();
    }

  };

  Haala.Messages.AJAX = function(/*json*/ options) {
    this.init(options);
  };

  Haala.Messages.AJAX.Settings = {
  };

  Haala.Messages.AJAX.prototype = {

    init: function(/*json*/ options) {
      this.configure(options);
    },

    refresh: function() {
      var model = this.model;
      var counter = model.counter, visible = model.element.is(":visible");
      if (counter && !visible) {
        model.element.fadeIn(400);
        //$.each(Haala.getStorage().get("_closeableFrames", []), function(n, frame) {
        //  if (frame) frame.close();
        //});
      }
      if (!counter && visible) model.element.fadeOut(400);
    },

    increase: function() {
      this.model.counter++;
      this.refresh();
    },

    decrease: function() {
      if (this.model.counter) this.model.counter--;
      this.refresh();
    },

    configure: function(/*json*/ options) {
      var settings = $.extend(true, {}, Haala.Messages.AJAX.Settings, options || {});
      var model = { counter:0 };
      model.element = $("#ajax-frame");
      this.model = model;
    }

  };

  Haala.Messages.Ok = function(/*json*/ options) {
    this.init(options);
  };

  Haala.Messages.Ok.Settings = {
    message: {
      mappingFn: function(/*string*/ msg, /*json*/ tokens) { return msg; }
    },
    onCloseFn: function(/*json*/ model) { }
  };

  Haala.Messages.Ok.prototype = {

    init: function(/*json*/ options) {
      this.configure(options);
    },

    configure: function(/*json*/ options) {
      var settings = $.extend(true, {}, Haala.Messages.Ok.Settings, options || {});
      var model = {};
      var msgFn = settings.message.mappingFn;

      var id = "ok-message";
      if (!$("#"+id).length) {
        $("body").append("<div id='"+id+"'></div>");
        //Haala.getStorage().addToArray("_closeableFrames", this);
      }
      model.element = $("#"+id);

      var active = true;
      model.element.html(msgFn(settings.text));
      model.element.dialog({
        zIndex: 70,
        buttons: [{ text: msgFn("button-ok"), click: function() { $(this).dialog("close"); }}],
        title: msgFn(settings.title),
        close: function() {
          if (active) {
            active = false;
            settings.onCloseFn(model);
          }
        }
      });

      this.model = model;
    },

    close: function() {
      this.model.element.dialog("close");
    }

  };

  Haala.Messages.YesNo = function(/*json*/ options) {
    this.init(options);
  };

  Haala.Messages.YesNo.Settings = {
    message: {
      mappingFn: function(/*string*/ msg, /*json*/ tokens) { return msg; }
    },
    onYesFn: function(/*json*/ model) { },
    onNoFn: function(/*json*/ model) { }
  };

  Haala.Messages.YesNo.prototype = {

    init: function(/*json*/ options) {
      this.configure(options);
    },

    configure: function(/*json*/ options) {
      var settings = $.extend(true, {}, Haala.Messages.YesNo.Settings, options || {});
      var model = {};
      var msgFn = settings.message.mappingFn;

      var id = "yn-message";
      if (!$("#"+id).length) {
        $("body").append("<div id='"+id+"'></div>");
        //Haala.getStorage().addToArray("_closeableFrames", this);
      }
      model.element = $("#"+id);

      var yesClicked = false, active = true;
      model.element.html(msgFn(settings.text));
      model.element.dialog({
        zIndex: 70,
        buttons: [
          { text: msgFn("button-yes"), click: function() {
            yesClicked = true;
            $(this).dialog("close");
          }},
          { text: msgFn("button-no"), click: function() {
            $(this).dialog("close");
          }}],
        title: msgFn(settings.title),
        close: function() {
          if (active) {
            active = false;
            yesClicked ? settings.onYesFn(model) : settings.onNoFn(model);
          }
        }
      });

      this.model = model;
    },

    close: function() {
      this.model.element.dialog("close");
    }

  };

})(jQuery);

/* Integration */

(function($) {

  Haala.Messages.Integ = {
    messageMappingFn: function(/*string*/ msg, /*json*/ tokens) {
      var x = msg;
      x = Haala.Misc.replaceAll(x, "button-", "btn.");
      x = Haala.Misc.replaceAll(x, "title-", "ttl.");
      return Haala.Misc.messageMapping(null, x, tokens);
    }
  };

})(jQuery);

