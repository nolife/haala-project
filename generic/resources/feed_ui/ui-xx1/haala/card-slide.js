/**
 * jQuery
 * Depends on: nothing
 */

var Haala = Haala || {};

(function($) {

  /* plugin */

  $.fn.cardSlide = function(/*json*/ options) {
    options.element = this;
    return new Haala.CardSlide(options);
  };

  /* logic */

  Haala.CardSlide = function(/*json*/ options) {
    this.init(options);
  };

  Haala.CardSlide.Settings = {
    css: { highlight: "highlight" },
    delay: 5000, hardDelay: 5000,
    prepareCardFn: function(/*json*/ card, /*json*/ model, /*fn*/ callback) { callback(); },
    slideCardsFn: function(/*json*/ card1, /*json*/ card2, /*json*/ model, /*fn*/ callback) {
      var ms = 800;
      card1.element.fadeOut(ms);
      card2.element.fadeIn(ms);
      setTimeout(callback, ms);
    },
    switchAnchorFn: function(/*json*/ card1, /*json*/ card2, /*json*/ model) {
      var cls = model.css.highlight;
      card1.anchor.removeClass(cls);
      if (!card2.anchor.hasClass(cls)) card2.anchor.addClass(cls);
    }
  };

  Haala.CardSlide.prototype = {

    init: function(/*json*/ options) {
      this.configure(options);
    },

    configure: function(/*json*/ options) {

      var configureCard = function(/*json*/ card) {
        var e = $(card.element);
        card.element = e;
        if (!e.length) return; //no such element

        var me = model.element;
        e.css({ position: "absolute", height: me.height()+"px" , width: me.width()+"px" });

        e.hover(
          function() { if (e.is(":visible")) model.runner.paused = true },
          function() { if (e.is(":visible")) model.runner.paused = false }
        );

        var ae = $(card.anchor);
        card.anchor = ae;

        ae.click(function(event) {
          event.preventDefault();
          if (model.runner.currentCard != card) gotoCard(card);
        });

        ae.hover(
          function() { model.runner.paused = true },
          function() { model.runner.paused = false }
        );
      };

      var visibleCard = function() {
        var visible = [];
        $.each(model.cards, function(n, card) { if (card.element.is(":visible")) visible.push(card); });
        $.each(visible, function(n, card) { if (n) card.element.hide(); });
        if (!visible.length) {
          model.cards[0].element.show();
          visible.push(model.cards[0]);
        }
        return visible[0];
      };

      var prepareNextCard = function(/*json*/ forceCard) {
        var index = model.cards.indexOf(model.runner.currentCard);
        var card = forceCard ? forceCard : index+1 < model.cards.length ? model.cards[index+1] : model.cards[0];
        model.runner.next = { card: card, ready: false, pause: forceCard != null };
        settings.prepareCardFn(card, model, function() { model.runner.next.ready = true; });
      };

      var gotoCard = function(/*json*/ card) {
        var runner = model.runner;
        if (runner.slidingState != "idle") return;

        prepareNextCard(card);
        runner.slidingState = "idle";
        runner.countdown = 0;
        runner.hardCountdown = 0;
        runner.paused = false;
      };

      var slide = function() {
        var runner = model.runner;
        if (runner.next.ready) {
          switch (runner.slidingState) {
            case "idle":
              runner.slidingState = "start";
              return false;
            case "start":
              runner.slidingState = "sliding";
              settings.slideCardsFn(runner.currentCard, runner.next.card, model, function() {
                runner.slidingState = "done";
              });
              settings.switchAnchorFn(runner.currentCard, runner.next.card, model);
              return false;
            case "done":
              runner.slidingState = "idle";
              runner.currentCard = runner.next.card;
              if (runner.next.pause) runner.paused = true;
              prepareNextCard();
              return true;
            default:
              return false;
          }
        }
        return false;
      };

      var tick = function() {
        var runner = model.runner;
        if (runner.paused) runner.countdown = settings.delay/10;
        if ((--runner.countdown <= 0 || --runner.hardCountdown <= 0) && slide()) {
          runner.countdown = settings.delay/10;
          runner.hardCountdown = settings.hardDelay/10;
        }
        if (runner.countdown < 0) runner.countdown = 0;
        if (runner.hardCountdown < 0) runner.hardCountdown = 0;
        setTimeout(tick, 10);
      };

      var settings = $.extend(true, {}, Haala.CardSlide.Settings, options || {});
      var model = {};
      model.css = $.extend(true, {}, settings.css, model.css);
      model.element = $(settings.element);

      if (model.element.length) {
        model.cards = settings.cards;
        $.each(model.cards, function(n, card) {
          configureCard(card);
        });
      }

      model.runner = {
        paused: false,
        countdown: settings.delay/10,
        hardCountdown: settings.hardDelay/10,
        currentCard: visibleCard(),
        next: { card: null, ready: false, pause: false },
        slidingState: "idle"  // idle -> start -> sliding -> done -> idle
      };

      prepareNextCard();

      tick();

      this.model = model;
    }

  };

})(jQuery);
