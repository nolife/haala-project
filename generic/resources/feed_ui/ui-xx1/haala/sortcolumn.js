/**
 * jQuery plugin for sortable columns
 */

;(function ( $, window, document, undefined ) {

    var pluginName = 'sortcolumn'

    // Create the plugin constructor
    function Plugin ( element, options ) {
        this.element = element
        this._name = pluginName
        this._defaults = $.fn.sortcolumn.defaults
        this.options = $.extend( {}, this._defaults, options )
        this.init()
    }

    // Avoid Plugin.prototype conflicts
    $.extend(Plugin.prototype, {

        // Initialization logic
        init: function () {
            this.buildCache()
            this.bindEvents()

            var options = this.options
            this.model = {
              rows: options.rows,
              currentPage: options.currentPage,
              totalPages: options.totalPages,
              withHash: options.withHash
            }

            this.$element.addClass(options.css)
            this.render()
        },

        // Remove plugin instance completely
        destroy: function() {
            this.unbindEvents()
            this.$element.removeData()
        },

        // Cache DOM nodes for performance
        buildCache: function () {
            this.$element = $(this.element)
            this.columns = []

            var plugin = this

            $.each(this.options.columns, function(n, vo) {
              var e = (typeof vo.selector === 'string') ? plugin.$element.find(vo.selector) : $(vo.selector)
              var arrowE = e.find(".arrow")
              if (!arrowE.length) {
                arrowE = $("<span class='arrow'></span>")
                arrowE.appendTo(e)
              }
              plugin.columns.push({ name: vo.name, order: vo.order, $element: e, $arrow: arrowE })
            })
        },

        // Bind events that trigger methods
        bindEvents: function() {
            var plugin = this

            $.each(plugin.columns, function(n, vo) {
              vo.$element.on('click'+'.'+plugin._name, function(event) {
                event.preventDefault()
                plugin.notifySort.call(plugin, vo);
              })
            })
        },

        // Unbind events that trigger methods
        unbindEvents: function() {
            this.$element.off('.'+this._name)

            var plugin = this

            $.each(plugin.columns, function(n, vo) {
              vo.$element.off('.'+plugin._name)
            })
        },

        render: function() {
          var plugin = this,
              model  = this.model

          $.each(this.columns,
            function(n, vo) {
              vo.$arrow.removeClass("asc desc")
              if (vo.order) vo.$arrow.addClass(vo.order)
            })

          plugin.notifyRender()
        },

        notifyRender: function() {
            var fn = this.options.onRender

            if ( typeof fn === 'function' ) {
                fn.call(this.element, this.model)
            }
        },

        notifySort: function(/*{}*/ column, /*asc|desc|optional*/ order) {
            var fn     = this.options.sort,
                plugin = this,
                model  = this.model

            var c = $.extend( {}, column )
            c.order = order || (c.order == "asc" ? "desc" : "asc")

            if ( typeof fn === 'function' ) {
              fn.call(this.element, this.model, c,
                function(/*bool\optional*/ success) {
                  if (success || success == undefined) {
                    $.each(plugin.columns, function(n, vo) {
                      delete vo.order
                    })
                    column.order = c.order
                    plugin.render()
                  }
                })
            }
        }

    })

    var methods = {
        sort: function (/*{}*/ column) {
          var plugin = this.data("plugin_" + pluginName)
          var c = plugin.columns.filter(function(vo) {
            return vo.name == column.name
          })
          if (c) plugin.notifySort(c[0], column.order)
        },

        update: function (/*{}|optional*/ column) {
          var plugin = this.data("plugin_" + pluginName)
          $.each(plugin.columns, function(n, vo) {
            delete vo.order
          })
          var c = plugin.columns.filter(function(vo) {
            return vo.name == column.name
          })
          if (c.length) {
            c[0].order = column.order
            plugin.render()
          }
        }
    }

    // Plugin wrapper around constructor
    $.fn.sortcolumn = function ( options ) {
      if ( methods[options] ) {
        return methods[options].apply( this, Array.prototype.slice.call( arguments, 1 ))
      } else if ( typeof options === 'object' || !options ) {
        this.each(function() {
          if ( !$.data( this, "plugin_" + pluginName ) ) {
            $.data( this, "plugin_" + pluginName, new Plugin( this, options ) )
          }
        })
        return this
      } else {
        $.error( 'Method '+ options+' does not exist on jQuery.'+pluginName )
      }
    }

    $.fn.sortcolumn.defaults = {
        onRender: null, // fn
        sort: null, // fn
        css: "sortcolumn", // CSS class applied to element
        columns: [] // columns as { name, order, selector }
                    // name - column name, order - asc | desc | undefined, relative column selector or element
    }

})( jQuery, window, document );
