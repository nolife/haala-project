/**
 * jQuery
 * Wire together plugins to make a table with sorting, rows and pagination
 */

;var Haala = Haala || {}

;(function ( $, window, document, undefined ) {

  Haala.TableBuilder = function(/*{}*/ options) {
    this.init(options || {})
  }

  Haala.TableBuilder.prototype = {

    supportedPlugins: ["listing", "sortcolumn", "pagination"],
    plugins: {},
    listingState: {},
    facadeSearch: false, // do facade call to load items

    init: function(/*{}*/ options) {
      this.facadeSearch = options.facadeSearch
    },

    pluginData: function(pluginName) {
      return this.plugins[pluginName] ? this.plugins[pluginName].data("plugin_" + pluginName) : null
    },

    plugin: function(/*plugin*/ plugin) {
      var THIS = this,
          set = false
      this.supportedPlugins.forEach(function(pluginName) {
        var data = plugin.data("plugin_" + pluginName)
        if (data != undefined) {
          THIS.plugins[pluginName] = plugin
          set = true
        }
      })
      if (!set) $.error( 'Unsupported plugin '+plugin )
      return this
    },

    /** parse state [max rows],[rows],[current page],[sort field],[sort order],[refresh] */
    state: function(/*str*/ s, /*{}*/ defaultSort) {
      var THIS = this,
          x = s.split(",")

      this.listingState = {
        rows: parseInt(x[1] || x[0]),
        currentPage: parseInt(x[2] || 1),
        sortField: x[3] ? { name: x[3], order: x[4] == "1" ? "asc" : "desc" } : defaultSort,
        refreshData: parseInt(x[5])
      }
      return this
    },

    build: function() {
      var pluginSC = this.plugins["sortcolumn"],
          pdataSC  = this.pluginData("sortcolumn"),
          pluginLS = this.plugins["listing"],
          pdataLS  = this.pluginData("listing"),
          pluginPG = this.plugins["pagination"],
          pdataPG  = this.pluginData("pagination")

      // wire sorting
      if (pluginSC && pluginLS) {
        pdataSC.options.sort = function(model, /*{}*/ column, callback) {
          pluginLS.listing("sort", { name: column.name, order: column.order })
        }
      }

      // wire pagination
      if (pluginPG && pluginLS) {
        pdataPG.options.changePage = function(model, /*num*/ page, callback) {
          pluginLS.listing("page", page)
        }
      }

      // wire listing
      if (pluginLS) {
        pdataLS.options.sort = function(model, /*{}*/ sortField, callback) {
          model.sortField = sortField
          callback()
        }

        var fn_onRender = pdataLS.options.onRender
        pdataLS.options.onRender = function(model) {
          if (model.sortField && pluginSC) pluginSC.sortcolumn("update", model.sortField)
          if (pluginPG) pluginPG.pagination("update", model.currentPage, model.totalPages)

          if (fn_onRender) fn_onRender() // call original fn
        }

        pluginLS.listing("model", this.listingState)

        // wrap load items fn to call facade
        if (this.facadeSearch) {
          var fn_loadItems = pdataLS.options.loadItems

          pdataLS.options.loadItems = function(model, callback) {
            var sort = model.sortField || {},
                command = {
                  from: model.itemsIndex, max: model.rows, refresh: model.refreshData,
                  sortBy: sort.name, sortType: sort.order == "asc" ? 1 : 0
                }
            model.userData = model.userData || {}

            /*
               1) set command consumable by facade
               2) set function to call facade and do callback

               to load items from facade (listing plugin option loadItems):

               function loadItems(model) {
                 model.userData.facadeSearch.command.target = 1 // add other properties if needed
                 model.userData.facadeSearch.call(SettingF.search, "#setting-list") // server call
               }
            */
            model.userData.facadeSearch = {}
            model.userData.facadeSearch.command = command
            model.userData.facadeSearch.call = function(facade, lockSelector) {
              Haala.AJAX.call(lockSelector, facade, model.userData.facadeSearch.command, function(data) {
                callback(parseInt(data.result.total), data.result.objects)
              })
            }

            if (fn_loadItems) fn_loadItems(model, callback) // call original fn
          }

        }
      }

      // render
      if (pluginLS) pluginLS.listing("page", pdataLS.model.currentPage)
    }

  }

})( jQuery, window, document );
