
var Haala = Haala || {};

(function($) {

  Haala.Date = {

    /** 20161230 -> 30/12/2016 */
    formatDate: function(/*num|str*/ value) {
      var str = ''+value
      var year = str.substr(0, 4), month = str.substr(4, 2), day = str.substr(6, 2)
      return day+'/'+month+'/'+year
    },

    /** 20161230 -> 30 December 2016 */
    formatDateFull: function(/*num|str*/ value) {
      var str = ''+value
      return str.substr(6, 2)+' '+Haala.Date.monthAsWord(str)+' '+str.substr(0, 4)
    },

    /** 1259 -> 12:59 */
    formatTime: function(/*num|str*/ value) {
      var str = ('0000'+value).slice(-4)
      var hours = str.substr(0, 2), minutes = str.substr(2, 2)
      return hours+':'+minutes
    },

    /** 30/12/2016 -> 20161230   3/12/2016 -> 20161203 */
    parseDate: function(/*str*/ value) {
      if (value.length == 9) value = '0'+value
      var year = value.substr(6, 4), month = value.substr(3, 2), day = value.substr(0, 2)
      var num = parseInt(year+month+day)
      return value.length == 10 && num >= 10000000 && num <= 99990000 ? ''+num : null
    },

    /** 12:59 -> 1259   2:30 -> 230 */
    parseTime: function(/*str*/ value) {
      if (value.length == 4) value = '0'+value
      var hours = '0'+value.substr(0, 2), minutes = value.substr(3, 2)
      var num = parseInt(hours+minutes)
      return value.length == 5 && num >= 0 && num <= 2400 ? ''+num : null
    },

    /** args -> UTC Date (no daylight saving) */
    dateUTC: function(/*num*/ year, /*num*/ month, /*num*/ day) {
      return new Date(Date.UTC(year, month, day))
    },

    /** 20170123 -> Date */
    strToDate: function(/*str|num*/ value) {
      var str = ''+value
      var year = str.substr(0, 4), month = str.substr(4, 2), day = str.substr(6, 2)
      return Haala.Date.dateUTC(parseInt(year), parseInt(month-1), parseInt(day))
    },

    /** Date -> 20170123 */
    dateToStr: function(/*Date*/ date) {
      return date.toJSON().slice(0,10).replace(/-/g,'')
    },

    /** 20170123 */
    todayDate: function() {
      return Haala.Date.dateToStr(new Date())
    },

    /** 20170317,845 -> 201703170845 */
    datetime: function(/*num|str*/ date, /*num|str*/ time) {
      var sdate = ''+date,
          stime = ('0000'+time).slice(-4)
      return sdate+stime
    },

    /** 20170123,1 -> 20170124 */
    addDaysDate: function(/*str|num*/ value, /*num*/ add) {
      var date = Haala.Date.strToDate(value)
      var result = Haala.Date.dateUTC(date.getFullYear(), date.getMonth(), date.getDate()+add)
      return Haala.Date.dateToStr(result)
    },

    /** 20170123 -> 20170101 */
    monthFirstDate: function(/*str*/ value) {
      var date = Haala.Date.strToDate(value)
      var result = Haala.Date.dateUTC(date.getFullYear(), date.getMonth(), 1)
      return Haala.Date.dateToStr(result)
    },

    /** 20170123 -> 20170131 */
    monthLastDate: function(/*str*/ value) {
      var date = Haala.Date.strToDate(value)
      var nextMonth = Haala.Date.dateUTC(date.getFullYear(), date.getMonth()+1, 1)
      var result = Haala.Date.dateUTC(nextMonth.getFullYear(), nextMonth.getMonth(), nextMonth.getDate()-1)
      return Haala.Date.dateToStr(result)
    },

    /** 20170123 -> 20170115 */
    monthMidDate: function(/*str*/ value) {
      var date = Haala.Date.strToDate(value)
      var result = Haala.Date.dateUTC(date.getFullYear(), date.getMonth(), 15)
      return Haala.Date.dateToStr(result)
    },

    /** 20170105 -> 20170102 (monday) */
    weekFirstDate: function(/*str*/ value) {
      var date = Haala.Date.strToDate(value)
          weekday = date.getDay(),
          add = weekday == 0 ? -6 : -weekday+1
      return Haala.Date.addDaysDate(value, add)
    },

    /** 20170105 -> 20170108 (sunday) */
    weekLastDate: function(/*str*/ value) {
      var date = Haala.Date.strToDate(value)
          weekday = date.getDay(),
          add = weekday == 0 ? 0 : -weekday+7
      return Haala.Date.addDaysDate(value, add)
    },

    /** 20170123 -> Wednesday */
    weekdayAsWord: function(/*str*/ value) {
      var date = Haala.Date.strToDate(value),
          weekday = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday']
      return weekday[date.getDay()]
    },

    /** 20170123 -> January */
    monthAsWord: function(/*str*/ value) {
      var date = Haala.Date.strToDate(value),
          month = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December']
      return month[date.getMonth()]
    },

    /** Compare dates with time (201701231220), value1 is compared with value2, return -1 or 0 or 1 */
    datetimeCompare: function(/*str*/ value1, /*str*/ value2) {
      var date1 = parseInt(value1.substr(0, 8)),
          time1 = parseInt(value1.substr(8, 4)),
          date2 = parseInt(value2.substr(0, 8)),
          time2 = parseInt(value2.substr(8, 4))
      return date1 < date2 ? -1 : date1 > date2 ? 1 : time1 < time2 ? -1 : time1 > time2 ? 1 : 0
    },

    /** 20170123,20170125 -> 2 */
    diffDaysDate: function(/*str*/ value1, /*str*/ value2) {
      var curDate = value1
          diff = 0
      while (parseInt(curDate) < parseInt(value2)) {
        diff++
        curDate = Haala.Date.addDaysDate(curDate, 1)
      }
      return diff
    },

    /** 1230,45 -> 1315, 22:00,125 -> 0005 */
    addMinutesTime: function(/*str|num*/ value, /*num*/ add) {
      var str = ('0000'+value).slice(-4),
          minutes = parseInt(str.substr(0,2))*60+parseInt(str.substr(2,2))+add,
          result = Math.floor(minutes/60)*100+Math.floor(minutes%60)
      while (result >= 2400) result -= 2400
      return ('0000'+result).slice(-4)
    }

  };

})(jQuery);
