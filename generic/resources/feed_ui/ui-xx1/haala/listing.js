/**
 * jQuery listing plugin
 */

;(function ( $, window, document, undefined ) {

    var pluginName = 'listing'

    // Create the plugin constructor
    function Plugin ( element, options ) {
        this.element = element
        this._name = pluginName
        this._defaults = $.fn.listing.defaults
        this.options = $.extend( {}, this._defaults, options )
        this.init()
    }

    // Avoid Plugin.prototype conflicts
    $.extend(Plugin.prototype, {

        // Initialization logic
        init: function () {
            this.buildCache()
            this.bindEvents()

            var options = this.options
            this.model = {
              rows: options.rows,
              currentPage: options.currentPage,
              totalPages: options.totalPages,
              sortField: options.sortField
            }

            this.$element.addClass(options.css)
            this.emptyFill()
            if (!options.delayLoad) this.notifyLoad(this.model.currentPage)
        },

        // Remove plugin instance completely
        destroy: function() {
            this.unbindEvents()
            this.$element.removeData()
        },

        // Cache DOM nodes for performance
        buildCache: function () {
            this.$element = $(this.element)
        },

        // Bind events that trigger methods
        bindEvents: function() {
            var plugin = this
        },

        // Unbind events that trigger methods
        unbindEvents: function() {
            this.$element.off('.'+this._name)
        },

        render: function(/*int*/ page, /*[{}]*/ vos) {
          var plugin = this,
              model  = this.model

          model.currentPage = page < 1 ? 1 : page > model.totalPages ? model.totalPages : page ? page : model.currentPage || 1
          model.itemsIndex = (model.currentPage-1)*model.rows

          plugin.$element.empty()

          model.items = []

          for (var z = vos.length; z < model.rows; z++)
            vos.push(null)

          var maker = plugin.options.makeRow
          if ( typeof maker === 'function' )
            $.each(vos, function(n, vo) {
              var e = maker.call(plugin.element, plugin.model, vo, n+1)
              if (e) {
                vo ? e.addClass("data") : e.addClass("empty")
                e.addClass((n+1)%2 == 0 ? "even" : "odd")
                e.appendTo(plugin.$element)
                model.items.push({ element: e })
              }
            })

          plugin.notifyRender()
        },

        emptyFill: function() {
          var plugin = this,
              model  = this.model

          plugin.$element.empty()

          model.items = []

          var maker = plugin.options.makeRow
          if ( typeof maker === 'function' )
            for (var n = 0; n < model.rows; n++) {
              var e = maker.call(plugin.element, plugin.model, null, n+1)
              if (e) {
                e.addClass("empty")
                e.addClass((n+1)%2 == 0 ? "even" : "odd")
                e.appendTo(plugin.$element)
                model.items.push({ element: e })
              }
            }
        },

        notifyRender: function() {
            var fn = this.options.onRender

            if ( typeof fn === 'function' ) {
                fn.call(this.element, this.model)
            }
        },

        notifySort: function(/*{}*/ sortField) {
            var fn     = this.options.sort
                plugin = this,
                model  = this.model

            if ( typeof fn === 'function' ) {
              fn.call(this.element, this.model, sortField,
                function(/*bool\optional*/ success) {
                  if (success || success == undefined) {
                    plugin.notifyLoad(1)
                  }
                })
            }
        },

        notifyLoad: function(/*num*/ page) {
            var fn     = this.options.loadItems,
                plugin = this,
                model  = this.model

             model.currentPage = page
             model.itemsIndex = (model.currentPage-1)*model.rows

            if ( typeof fn === 'function' ) {
              fn.call(this.element, this.model, function(/*num*/ total, /*[{}]*/ vos) {
                model.totalItems = total
                model.totalPages = Math.floor(total/model.rows+(total%model.rows == 0 ? 0 : 1)) || 1
                plugin.render(page, vos)
              })
            }
        }

    })

    var methods = {
        /** set page and load items */
        page: function (/*num|optional*/ n) {
          var plugin = this.data("plugin_" + pluginName)
          plugin.notifyLoad(n || plugin.model.currentPage)
        },

        /** set sort then re-load items */
        sort: function (/*{}*/ sortField) {
          var plugin = this.data("plugin_" + pluginName)
          plugin.notifySort(sortField)
        },

        /** extend model */
        model: function (/*{}*/ m) {
          var plugin = this.data("plugin_" + pluginName)
          $.extend(true, plugin.model, m)
        }
    }

    // Plugin wrapper around constructor
    $.fn.listing = function ( options ) {
      if ( methods[options] ) {
        return methods[options].apply( this, Array.prototype.slice.call( arguments, 1 ))
      } else if ( typeof options === 'object' || !options ) {
        this.each(function() {
          if ( !$.data( this, "plugin_" + pluginName ) ) {
            $.data( this, "plugin_" + pluginName, new Plugin( this, options ) )
          }
        })
        return this
      } else {
        $.error( 'Method '+ options+' does not exist on jQuery.'+pluginName )
      }
    }

    $.fn.listing.defaults = {
        onRender: null, // fn
        loadItems: null, // fn
        makeRow: null, // fn
        sort: null, // fn
        css: "listing", // CSS class applied to element
        rows: 10, // # of rows
        currentPage: 1, // current page
        totalPages: 1, // total pages
        delayLoad: false, // delay load items on init
        sortField: null // { name: field_name, order: asc | desc }
    }

})( jQuery, window, document );
