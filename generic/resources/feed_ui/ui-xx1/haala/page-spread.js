/**
 * jQuery
 * Depends on: misc
 *
 * Spread items to the left and to the right from the current position
 * 1 ... 23 24 [25] 26 27 ... 85
 */

var Haala = Haala || {};

(function($) {

  /* plugin */

  $.fn.pageSpread = function(/*json*/ options) {
    if (!options) options = {};
    options.element = this;
    return $(this).data("page-spread") || new Haala.PageSpread(options);
  };

  /* logic */

  Haala.PageSpread = function(/*json*/ options) {
    this.init(options);
  };

  Haala.PageSpread.Settings = {
    css: { current: "current" },
    spread: 3, total: 0, current: 1, showNextPrev: "default", // always | never
    gap: "<span>...</span>", next: "<span>&gt;</span>", prev: "<span>&lt;</span>",
    clickFn: function(/*int*/ num, /*json*/ model) {}, //deprecated in favour of listeners
    renderedFn: function(/*json*/ model) { }, //deprecated in favour of listeners
    initFn: function(/*json*/ model) { }
  };

  Haala.PageSpread.prototype = {

    init: function(/*json*/ options) {
      this.configure(options);
      var model = this.model;
      model.initFn(model);
    },

    refresh: function(/*num*/ totalPages, /*num*/ currentPage) {
      var model = this.model;
      if (totalPages != undefined) model.total = totalPages;
      if (currentPage != undefined) model.current = currentPage;

      model.element.empty();
      model.items = [];
      var total = model.total, current = model.current, spread = model.spread;
      current = current < 1 ? 1 : current > total ? total : current;

      var addItem = function(/*int*/ n, /*string*/ html, /*string*/ type) {
        var e = n  ? $("<a href='#"+n+"'>"+(html || n)+"</a>") : $(html);
        if (n == current) e.addClass(model.css.current);
        e.appendTo(model.element);
        model.items.push({ element: e, num: n, type: type || "page" });

        if (n)
          e.click(function(event) {
            event.preventDefault();
            model.clickFn(n, model);
            model.listener.fire("clicked", { page: n });
          });
      };

      /* [prev] [first] [gap] [spread] [page] [spread] [gap] [last] [next] */
      if (total > 1) {
        var b;
        switch (model.showNextPrev) {
          case "never": b = false; break;
          case "always": b = true; break;
          default: b = current > 1;
        }
        if (b) addItem(Math.max(current-1, 1), model.prev, "prev");

        var n;
        addItem(1);
        if (total <= spread*2+5) {
          /* no gaps: 1 2 3 4 5 [6] 7 8 9 10 11 */
          for (n = 2; n <= total-1; n++) addItem(n);
        } else if (current < spread*2) {
          /* bigger right spread: 1 [2] 3 4 5 6 7 8 9 ... 15 */
          for (n = 2; n <= spread*2+3; n++) addItem(n);
          addItem(0, model.gap, "right gap"); //right gap
        } else if (current > total-spread*2) {
          /*bigger left spread: 1 ... 7 8 9 10 11 12 13 [14] 15 */
          addItem(0, model.gap, "left gap"); //left gap
          for (n = total-spread*2-2; n <= total-1; n++) addItem(n);
        } else {
          /* moving spread: 1 ... 5 6 7 [8] 9 10 11 ... 15   1 2 3 4 5 [6] 7 8 9 ... 15   1 [2] 3 4 5 ... 15 */
          var start = current-spread > 2 ? current-spread : 3;
          var end = current+spread < total-2 ? current+spread : total-2;
          if (start == 3) addItem(2); else addItem(0, model.gap, "left gap"); //left gap (or unfold second page)
          for (n = start; n <= end; n++) addItem(n);
          if (end >= total-2) addItem(total-1); else addItem(0, model.gap, "right gap"); //right gap (or unfold pre last page)
        }
        addItem(total);

        switch (model.showNextPrev) {
          case "never": b = false; break;
          case "always": b = true; break;
          default: b = current < total;
        }
        if (b) addItem(Math.min(current+1, total), model.next, "next");
      }

      model.renderedFn(model);
      model.listener.fire("rendered");
    },

    configure: function(/*json*/ options) {
      var settings = $.extend(true, {}, Haala.PageSpread.Settings, options || {});
      var model = {
        spread: settings.spread, total: settings.total, current: settings.current, showNextPrev: settings.showNextPrev,
        gap: settings.gap, next: settings.next, prev: settings.prev, items: [],
        clickFn: settings.clickFn, renderedFn: settings.renderedFn, initFn: settings.initFn
      };
      model.css = $.extend(true, {}, settings.css, model.css);
      model.element = $(settings.element);

      if (model.element.length) {
        var THIS = this;
        //bypass initial rendering if no total set
        if (model.total) setTimeout(function() { THIS.refresh(); }, 10);
      }

      Haala.Misc.eventListener(model, this);

      model.element.data("page-spread", this);
      this.model = model;
    }

  };

})(jQuery);





/* Integration */

(function($) {

  Haala.PageSpread.Integ = {};

  Haala.PageSpread.Integ.DefaultOptions = {
    showNextPrev: "always",
    next: "<span class='next'></span>", prev: "<span class='prev'></span>", gap: "<span class='gap'>...</span>",
    initFn: function(/*json*/ model) {
      model.listener.add("rendered", function() {
        if (model.total <= 1) model.element.hide();
        else model.element.show();
      });
    }
  };

})(jQuery);
