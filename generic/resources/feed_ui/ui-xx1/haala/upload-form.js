/**
 * jQuery
 * Depends on: misc
 */

var Haala = Haala || {};

(function($) {

  /* plugin */

  $.fn.uploadForm = function(/*json*/ options) {
    var recreate = options != undefined;
    if (!options) options = {};
    options.element = this;
    /* recreate form so new options take effect */
    if ($(this).data("upload-form") && recreate) $(this).data("upload-form").destroy();
    return $(this).data("upload-form") || new Haala.UploadForm(options);
  };

  /* logic */

  Haala.UploadForm = function(/*json*/ options) {
    this.init(options);
  };

  Haala.UploadForm.Settings = {
    refreshDelay: 600, ifarmeSuffix: "-utrg", actionURL: "/upload.htm", iframeSrc: "/blank.htm",
    uploadSuccessFn: function(/*json*/ model) {}, //deprecated in favour of listeners
    uploadCancelFn: function(/*json*/ model) {}, //deprecated in favour of listeners
    inputClearFn: function(/*json*/ input, /*json*/ model) {},
    initFn: function(/*json*/ model) {},
    progressListener: {
      createFn: function(/*json*/ model) {},
      destroyFn: function(/*json*/ model) {},
      updateFn: function(/*json*/ model, /*fn: int*/ callback) { callback(-1); }
    }
  };

  Haala.UploadForm.prototype = {

    init: function(/*json*/ options) {
      this.configure(options);
      var model = this.model;
      model.initFn(model);
    },

    destroy: function() {
      var model = this.model;
      model.element.data("upload-form", null);
      model.iframe.remove();
    },

    clearInput: function(/*json*/ input) {
      var model = this.model;
      var element = input.element;
      var html = $("<div/>").append(element.clone()).remove().html();
      var cloned = $(html);
      element.replaceWith(cloned);
      input.element = cloned;
      model.inputClearFn(input, model);
    },

    hasFiles: function() {
      var b = false;
      $.each(this.model.input, function(n, input) {
        b |= input.element.val() != "";
      });
      return b;
    },

    cancel: function() {
      var model = this.model;
      model.iframe.attr("src", null).attr("src", model.iframeSrc); //changing SRC should cancel current upload
      model.uploadCancelFn(model);
      model.listener.fire("cancel");
    },

    success: function() {
      var model = this.model;
      model.iframe.attr("src", null).attr("src", model.iframeSrc); //changing SRC should cancel current upload
      model.uploadSuccessFn(model);
      model.listener.fire("success");
      var THIS = this;
      $.each(model.input, function(n, inp) { THIS.clearInput(inp); });
    },

    upload: function() {
      var model = this.model;
      if (!this.hasFiles()) {
        model.uploadCancelFn(model);
        model.listener.fire("cancel");
      } else {
        /* enable submit then disable, everything goes through "upload" function preventing direct
           form submissions by external code */
        var form = model.element;
        form.attr("onsubmit", "return true").submit().attr("onsubmit", "return false");
        this.progress();
      }
    },

    progress: function(/*int*/ perc) {
      var model = this.model;
      var pl = model.progressListener;
      var THIS = this;
      if (perc == undefined) {
        pl.createFn(model);
        setTimeout(function() { THIS.progress(0); }, model.refreshDelay);
      } else if (perc == 100) {
        pl.destroyFn(model);
        setTimeout(function() { THIS.success(); }, model.refreshDelay);
      } else {
        pl.updateFn(model, function(/*int*/ perc) {
          if (perc >= 0) setTimeout(function() { THIS.progress(perc); }, model.refreshDelay);
          else {
            pl.destroyFn(model);
            THIS.cancel();
          }
        });
      }
    },

    configure: function(/*json*/ options) {
      var settings = $.extend(true, {}, Haala.UploadForm.Settings, options || {});
      var model = {
        refreshDelay: settings.refreshDelay, iframeSrc: settings.iframeSrc,
        actionURL: settings.actionURL, ifarmeSuffix: settings.ifarmeSuffix,
        uploadCancelFn: settings.uploadCancelFn,
        uploadSuccessFn: settings.uploadSuccessFn,
        initFn: settings.initFn,
        inputClearFn: settings.inputClearFn,
        progressListener: settings.progressListener
      };
      model.element = $(settings.element);
      var THIS = this;

      var scanInput = function() {
        var input = [];
        model.element.find("input[type=file]").each(function(n) {
          var element = $(this);
          var name = element.attr("name");
          var clearElement = model.element.find("*[data-clear="+name+"]");
          var inp = { element: element, name: name, clearElement: clearElement };
          input.push(inp);

          clearElement.unbind("click").click(function(event) {
            event.preventDefault();
            THIS.clearInput(inp);
          });

        });
        model.input = input;
      };

      var prepareForm = function() {
        var form = model.element;
        var name = form.attr("name") || form.attr("id");
        form.attr({ target: name+model.ifarmeSuffix, method: "post", action: model.actionURL, onsubmit: "return false" });
        var iframe = $("<iframe/>").attr({ name: name+model.ifarmeSuffix, src: model.iframeSrc }).
            css({ display: "none", width: 0, height: 0 });
        form.append(iframe);
        model.iframe = iframe;
      };

      if (model.element.length) {
        scanInput();
        prepareForm();
      }

      Haala.Misc.eventListener(model, this);

      model.element.data("upload-form", this);
      this.model = model;
    }

  };

})(jQuery);

/* Integration */

(function($) {

  Haala.UploadForm.Integ = {
    onFileSelect: function(/*element*/ inputElement, /*json*/ model) {
      inputElement.unbind("change").change(function() {
        var n = inputElement.attr("name").substring(3); // dat2 -> 2
        model.element.find("*[data-row="+(++n)+"]").show();
      });
    },

    resetExpandableForm: function(/*json*/ model) {
      var integ = Haala.UploadForm.Integ;
      model.element.find("*[data-row]").each(function(n) {
        var e = $(this), ie = e.find("input");
        if (n && !ie.val()) e.hide();
        else e.show();
        integ.onFileSelect(ie, model);
      });
      model.element.find(".expand").show().unbind("click").aclick(function(event) {
        var e = $(event.target);
        e.hide();
        model.element.find(".slide").slideDown(200);
      });
    },

    collapseForm: function(/*json*/ model) {
      model.element.find(".slide").slideUp(200, function() {
        Haala.UploadForm.Integ.resetExpandableForm(model);
      });
    },

    bindSubmitTo: function(/*selector*/ sel, /*json*/ model) {
      $(sel).unbind("click").click(function(event) {
        event.preventDefault();
        model.element.uploadForm().upload();
      });
    }
  };

  Haala.UploadForm.Integ.DefaultOptions = {
    progressListener: {
      createFn: function(/*json*/ model) {
        var ajaxFrame = Haala.Messages.ajaxFrame();
        var ajaxElement = ajaxFrame.model.element;
        ajaxElement.removeClass("upload").addClass("upload");
        ajaxFrame.model.userData = { originalHTML: ajaxElement.find("p").html() };
        ajaxElement.find("p").html("0%");
        Haala.Messages.showAJAX();
      },
      destroyFn: function(/*json*/ model) {
        Haala.Messages.closeAJAX();
        var ajaxFrame = Haala.Messages.ajaxFrame();
        var ajaxElement = ajaxFrame.model.element;
        ajaxElement.removeClass("upload");
        ajaxElement.find("p").html(ajaxFrame.model.userData.originalHTML);
      },
      updateFn: function(/*json*/ model, /*fn: int,bool*/ callback) {
        Haala.AJAX.call(null, MiscF.uploadStatus, "",
          function(data) {
            var ajaxFrame = Haala.Messages.ajaxFrame();
            var ajaxElement = ajaxFrame.model.element;
            ajaxElement.find("p").html(data.result.perc+"%");
            callback(data.result.perc);
          },
          function(data) {
            Haala.AJAX.handleError(data);
            callback(-1);
          },
          Haala.AJAX.DO_NOTHING
        );
      }
    },

    inputClearFn: function(/*json*/ input, /*json*/ model) {
      Haala.UploadForm.Integ.onFileSelect(input.element, model);
    }
  };

})(jQuery);
