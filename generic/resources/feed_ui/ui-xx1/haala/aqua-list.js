/**
 * jQuery
 * Depends on:
 *   misc
 *   Haala.PageSpread (optional)
 */

var Haala = Haala || {};

(function($) {

  /* plugin */

  $.fn.aquaList = function(/*json*/ options) {
    if (!options) options = {};
    options.element = this;
    return $(this).data("aqua-list") || new Haala.AquaList(options);
  };

  /* logic */

  Haala.AquaList = function(/*json*/ options) {
    this.init(options);
  };

  Haala.AquaList.Settings = {
    rows: 10, currentPage: 1, useHash: null, userData: {},
    sort: { field: null, order: null, items: [] },
    css: { odd: "odd", even: "even", asc: "asc", desc: "desc" },
    loadItemsFn: function(/*json*/ model, /*fn*/ callback) { },
    rowElementFn: function(/*json*/ vo, /*int*/ position, /*json*/ model) { return null; },
    renderedFn: function(/*json*/ model) { }, //deprecated in favour of listeners
    initFn: function(/*json*/ model) { }
  };

  Haala.AquaList.prototype = {

    init: function(/*json*/ options) {
      this.configure(options);
      var model = this.model;
      model.initFn(model);
    },

    //deprecated in favour of listeners
    setPageSpread: function(/*object|[object]*/ pageSpread) {
      var THIS = this;
      var arr = pageSpread.length ? pageSpread : [pageSpread];
      $.each(arr, function(n, spread) {
        spread.model.clickFn = function(/*int*/ num, /*json*/ model) { THIS.page(num); };
      });
      THIS.addRenderedFn(
        function(/*json*/ model) {
          $.each(arr, function(n, spread) {
            spread.model.total = Math.max(model.totalPages, model.currentPage);
            spread.model.current = model.currentPage;
            spread.refresh();
          });
        });
    },

    //deprecated in favour of listeners
    addRenderedFn: function(/*fn*/ f) {
      var model = this.model;
      if (!model.runner.multiRendered) {
        model.runner.multiRendered = [];
        this.model.renderedFn = function(/*json*/ model) {
          $.each(model.runner.multiRendered, function(n, f) { f(model); });
        };
      }
      model.runner.multiRendered.push(f);
    },

    sort: function(/*string*/ field, /*string*/ order) {
      var model = this.model;
      var oldSortField = model.sort.field;
      if (field) model.sort.field = field;

      var sortField = model.sort.field, sortOrder = model.sort.order;
      if (sortField) {
        var sortItem = null;
        $.each(model.sort.items, function(n, item) {
          if (item.field == sortField) sortItem = item;
        });

        if (order) sortOrder = order;
        else
          if (oldSortField == sortField) sortOrder = sortOrder == "asc" ? "desc" : "asc";
          else sortOrder = sortItem ? sortItem.order || "asc" : "asc";
        model.sort.order = sortOrder;

        $.each(model.sort.items, function(n, item) {
          item.element.removeClass(model.css.asc+" "+model.css.desc);
          if (item.field == sortField) item.element.addClass(sortOrder == "asc" ? model.css.asc : model.css.desc);
        });
      }
    },

    page: function(/*int*/ num) {
      var model = this.model;
      if (model.runner.busy) return;
      model.runner.busy = true;

      model.currentPage = num < 1 ? 1 : num > model.totalPages ? model.totalPages : num ? num : model.currentPage || 1;
      model.itemsIndex = (model.currentPage-1)*model.rows;

      var itemsLoaded = function(/*int*/ total, /*[json]*/ vos) {
        model.element.empty();
        model.items = [];
        model.totalItems = total;
        model.totalPages = Math.floor(total/model.rows+(total%model.rows == 0 ? 0 : 1)) || 1;

        for (var z = vos.length; z < model.rows; z++)
          vos.push(null);

        $.each(vos, function(n, vo) {
          var e = model.rowElementFn(vo, n+1, model);
          if (e) {
            e.addClass((n+1)%2 == 0 ? model.css.even : model.css.odd);
            e.appendTo(model.element);
            model.items.push({ element: e });
          }
        });

        model.runner.busy = false;
        model.renderedFn(model);
        model.listener.fire("rendered");

        /* [page],[sortField],[sortOrder] to restore later from URL hash on copy/paste */
        if (model.useHash)
          location.hash = model.useHash+"="+model.currentPage+","+(model.sort.field || "?")+","+
                (!model.sort.order ? "?" : model.sort.order == "asc" ? "1" : "0");
      };

      model.loadItemsFn(model, itemsLoaded);
    },

    emptyFill: function() {
      var model = this.model;
      model.element.empty();
      model.items = [];

      for (var n = 0; n < model.rows; n++) {
        var e = model.rowElementFn(null, n+1, model);
        if (e) {
          e.addClass((n+1)%2 == 0 ? model.css.even : model.css.odd);
          e.appendTo(model.element);
          model.items.push({ element: e });
        }
      }
    },

    configure: function(/*json*/ options) {
      var settings = $.extend(true, {}, Haala.AquaList.Settings, options || {});
      var model = {
        currentPage: settings.currentPage, totalPages: 1, totalItems: 0, itemsIndex: 0, rows: settings.rows,
        items: [], useHash: settings.useHash,
        loadItemsFn: settings.loadItemsFn, rowElementFn: settings.rowElementFn,
        renderedFn: settings.renderedFn, initFn: settings.initFn,
        userData: settings.userData
      };
      model.sort = $.extend(true, {}, settings.sort, model.sort);
      model.css = $.extend(true, {}, settings.css, model.css);
      model.element = $(settings.element);

      var THIS = this;

      var configureSortItem = function(/*json*/ item) {
        item.element = $(item.element);
        var e = item.element;

        e.click(function(event) {
          event.preventDefault();
          THIS.sort(item.field);
          THIS.page();
        });
      };

      var restoreFromHash = function() {
        /* [page],[sortField],[sortOrder] */
        var x = (Haala.Misc.getParameter(location.hash, model.useHash) || "").split(",");
        if (x.length == 3) {
          model.currentPage = x[0];
          model.sort.field = !x[1] || x[1] == "?" ? model.sort.field : x[1];
          model.sort.order = !x[2] || x[2] == "?" ? model.sort.order : x[2] == "1" ? "asc" : "desc";
        }
      };

      if (model.element.length) {
        $.each(model.sort.items, function(n, item) {
          configureSortItem(item)
        });
        if (model.useHash) restoreFromHash();

        setTimeout(function() {
          THIS.emptyFill();
          if (model.sort.field) THIS.sort(model.sort.field, model.sort.order);
          THIS.page();
        }, 10);
      }

      model.runner = {
        busy: false
      };

      Haala.Misc.eventListener(model, this);

      model.element.data("aqua-list", this);
      this.model = model;
    }

  };

})(jQuery);



/* Integration */

(function($) {

  Haala.AquaList.Integ = {
    /** Parse state [max rows],[rows],[current page],[sort field],[sort order],[refresh] */
    parseState: function(/*string*/ s, /*json*/ defsort) {
      var x = s.split(",");
      return {
        rows: parseInt(x[1] || x[0]), currentPage: parseInt(x[2] || "1"),
        sort: x[3] ? { field: x[3], order: x[4] == "1" ? "asc" : "desc" } : defsort,
        userData: { refresh: parseInt(x[5]) }
      }
    },

    loadItemsFn: function(/*fn*/ target, /*json*/ command, /*json*/ model, /*fn*/ callback) {
      $.extend(true, command, {
        from: model.itemsIndex, max: model.rows, refresh: model.userData.refresh,
        sortBy: model.sort.field, sortType: model.sort.order == "asc" ? 1 : 0
      });
      model.userData.refresh = 0;

      Haala.AJAX.call(model.element, target, command,
        function(data) { callback(parseInt(data.result.total), data.result.objects); });
    },

    addPageSpread: function(/*object*/ list, /*object*/ spread) {
      list.addListener("rendered", function(e) {
        var model = e.target;
        spread.refresh(Math.max(model.totalPages, model.currentPage), model.currentPage);
      });
      spread.addListener("clicked", function(e) {
        list.page(e.page);
      });
    }
  };

  Haala.AquaList.Integ.DefaultOptions = {
  };

})(jQuery);
