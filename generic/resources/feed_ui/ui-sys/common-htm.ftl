<#-- Common HTM library -->

<#import "/web/common.ftl" as c >


<#-- Empty 1x1 image -->
<#macro noimg class="">
  <img src="/web/z/dot.gif" class="${class}"/>
</#macro>


<#-- <form> -->
<#macro form id="pageForm" mode="" style="" class="">
  <form id="${id}" style="${style}" class="${class}" action="#" onsubmit="return false;" <#if mode == "upload">enctype="multipart/form-data"</#if> >
  <#nested>
  </form>
</#macro>


<#-- hidden div tag -->
<#macro nodiv id="page-ivars">
  <div id="${id}" style="display:none;">
  <#nested>
  </div>
</#macro>


<#-- Print URL to a file with optional no cache and salt
  <@url path="files/beans.xml"/> -> /files/beans.xml?site=mc1&rv=1.2
-->
<#macro url path nc="" salt="">${src(path, nc, salt)}</#macro>

<#-- function alias -->
<#function src path nc="" salt="">
  <#local value >/${path}?site=${mySite}&rv=${resourceVersion}<#if nc != "">&nc=1</#if><#if salt != "">&salt=<@h.salt/></#if></#local>
  <#return value >
</#function>


<#-- Print copyright and vendor tags (deprecated, use "copyleft" and "vendor")
   <p ...>&copy; 2009-2014 My-Estate.eu</p>  <a ...>Powered by My-Estate</a>
-->
<#macro copyright copyright="y" vendor="n">
  <#local tokens>
    BrandWeb:<@h.settingValue key="Brand" mapKey="web" />,
    BrandName:<@h.settingValue key="Brand" mapKey="name" />
  </#local>

  <#if copyright == "y">
    <p class="copyright" data-cms="2:site.copy">
      <@c.label "site.copy" tokens />
    </p>
  </#if>

  <#if vendor == "y">
    <a class="vendor" target="_blank" href="http://coldcore.com"
       title="<@c.label "sys.vendor-title" />" >
      <img src="/web/z/dot.gif" alt="<@c.label "sys.vendor-alt" />" />
    </a>
  </#if>
</#macro>


<#-- Print copyright tag
   <p ...>&copy; 2009-2014 My-Estate.eu</p>
-->
<#macro copyleft >
  <#local tokens>
    BrandWeb:<@h.settingValue key="Brand" mapKey="web" />,
    BrandName:<@h.settingValue key="Brand" mapKey="name" />
  </#local>

  <p class="copyright" data-cms="2:site.copy">
    <@c.label "site.copy" tokens />
  </p>
</#macro>


<#-- Print vendor tag
   <a ...>Powered by My-Estate</a>
-->
<#macro vendor >
  <a class="vendor" target="_blank" href="http://coldcore.com" title="<@c.label "sys.vendor-title" />" >
    <img src="/web/z/dot.gif" alt="<@c.label "sys.vendor-alt" />" />
  </a>
</#macro>
