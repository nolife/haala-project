<#-- Common library -->

<#-- Print a label -->
<#macro label key tokens="" >${text(key, tokens)}</#macro>

<#-- function alias -->
<#function text key tokens="" >
  <#local label ><@h.label key=key tokens=tokens /></#local>
  <#return label >
</#function>


<#function arrayContains value array >
  <#list array as x >
    <#if x==value ><#return true ></#if>
  </#list>
  <#return false >
</#function>


<#function csvContains value csv >
  <@h.parseCsv var="array" value=csv output="array" scope="local" />
  <#return arrayContains(value, array) >
</#function>


<#function sessionAttr key default="" >
  <#local session=system.requestX.request().getSession() />
  <#return session.getAttribute(key)!default >
</#function>


<#function arrayAsJson array >
  <#local json >[<#list array as x >"${x}"<#sep>,</#list>]</#local>
  <#return json >
</#function>


<#function csvAsJson csv >
  <@h.parseCsv var="array" value=csv output="array" scope="local" />
  <#return arrayAsJson(array) >
</#function>


<#-- Include page content (provided by Dynamic controller) -->
<#macro pageContent >
  <#include "/web/pages/"+(dynamicPageName!pageName)+".ftl:"+currentSite >
</#macro>
