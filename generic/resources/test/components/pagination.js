
var currentPage = 1
var totalPages = 0

var THIS = this

$('p').pagination({
  totalPages: 20,
  currentPage: 10,
  nextprev: "always",
  changePage: function(model, callback) {
    callback()
  },
  onRender: function(model) {
    THIS.currentPage = model.currentPage
    THIS.totalPages = model.totalPages
  }
})

$("button.prev").click(function() {
  $('p').pagination("page", Math.max(1, THIS.currentPage-1))
})

$("button.next").click(function() {
  $('p').pagination("page", Math.min(THIS.currentPage+1, THIS.totalPages))
})

$("button.first").click(function() {
  $('p').pagination("page", 1)
})

$("button.last").click(function() {
  $('p').pagination("page", THIS.totalPages)
})
