
var items = []
for (var z = 1; z <= 486; z++)
  items.push(z)

var currentPage = 1
var totalPages = 1

var THIS = this

$('p').listing({
  makeRow: function(model, /*any*/ vo) {
    return vo ?
      $("<div class='data'><span>" + vo + "</span></div>") :
      $("<div class='empty'></div>")
  },
  loadItems: function(model, callback) {
    var from  = model.itemsIndex,
        till  = Math.min(from+model.rows, items.length)
    callback(items.length, items.slice(from, till))
  },
  onRender: function(model) {
    THIS.currentPage = model.currentPage
    THIS.totalPages = model.totalPages
  }
})

$("button.prev").click(function() {
  $('p').listing("page", Math.max(1, THIS.currentPage-1))
})

$("button.next").click(function() {
  $('p').listing("page", Math.min(THIS.currentPage+1, THIS.totalPages))
})

$("button.first").click(function() {
  $('p').listing("page", 1)
})

$("button.last").click(function() {
  $('p').listing("page", THIS.totalPages)
})
