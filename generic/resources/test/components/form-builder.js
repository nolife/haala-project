
var state = {
  fields: [{ name: "firstName", selector: ".first-name input", validate: { require: true } },
           { name: "lastName", selector: $(".last-name input"), validate: { require: true} },
           { name: "comment", selector: ".comment input" }],

  onValidateAfter: function(model, /*[{}]*/ fields, /*{}*/ values) {
    fields.forEach(function(field) {
      if (field.validate.fail) console.log(field.validate.fail+" "+field.name)
    })
  },

  onSubmit: function(model, /*{}*/ values) {
    console.log("Submitted")
    console.log(values)
  }
}

// use either of those for builder validate
var validator = new Haala.InputValidator()
var uiValidator = new Haala.UIValidator()

new Haala.FormBuilder()
  .plugin( $('form').formpanel(state) )
  .validate(uiValidator)
  .build()

$("button.submit").click(function() {
  $('form').formpanel("submit")
})

