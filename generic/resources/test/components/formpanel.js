
var THIS = this

$('form').formpanel({
  fields: [{ name: "firstName", selector: ".first-name input", validate: {} },
           { name: "lastName", selector: $(".last-name input"), validate: {} },
           { name: "comment", selector: ".comment input" }],

  validateFields: function(model, /*[{}]*/ fields, /*{}*/ values, callback) {
    var valid = true
    fields.forEach(function(vo) {
      var name = vo.name,
          value = values[name]
      console.log("Validating "+name+" = "+value)
      valid &= value.length ? true : false
    })
    console.log(valid ? "Valid" : "Not valid")
    callback(valid)
  },

  onSubmit: function(model, /*{}*/ values) {
    console.log("Submitted")
    console.log(values)
  }
})

$("button.collect").click(function() {
  var container = {}
  $('form').formpanel("collect", container)
  console.log(container)
})

$("button.validate").click(function() {
  $('form').formpanel("validate")
})

$("button.submit").click(function() {
  $('form').formpanel("submit")
})
