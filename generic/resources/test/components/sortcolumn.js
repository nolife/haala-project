
var THIS = this

$('p').sortcolumn({
  columns: [
    { selector: ".key", name: "key" },
    { selector: $(".value"), name: "value" }
  ],
  sort: function(model, /*{}*/ column, callback) {
    callback()
  }
})

$("button.asc1").click(function() {
  $('p').sortcolumn("sort", { name: "key", order: "asc" })
})

$("button.desc1").click(function() {
  $('p').sortcolumn("sort", { name: "key", order: "desc" })
})

$("button.asc2").click(function() {
  $('p').sortcolumn("sort", { name: "value", order: "asc" })
})

$("button.desc2").click(function() {
  $('p').sortcolumn("sort", { name: "value", order: "desc" })
})
