
function randomNumber(min, max) {
    return Math.floor(Math.random() * (max-min) + min)
}

function randomWord(length) {
    var consonants = 'bcdfghjklmnpqrstvwxyz',
        vowels = 'aeiou',
        rand = function(limit) {
            return Math.floor(Math.random()*limit);
        },
        i, word='', length = parseInt(length,10),
        consonants = consonants.split(''),
        vowels = vowels.split('');
    for (i=0;i<length/2;i++) {
        var randConsonant = consonants[rand(consonants.length)],
            randVowel = vowels[rand(vowels.length)];
        word += (i===0) ? randConsonant.toUpperCase() : randConsonant;
        word += i*2<length-1 ? randVowel : '';
    }
    return word;
}

function sortItems(prop, i, a, b) {
  if (a[prop] < b[prop]) return -1*i;
  if (a[prop] > b[prop]) return 1*i;
  return 0;
}

var sortKeysAsc = sortItems.bind(null, "key", 1)
var sortKeysDesc = sortItems.bind(null, "key", -1)
var sortValuesAsc = sortItems.bind(null, "value", 1)
var sortValuesDesc = sortItems.bind(null, "value", -1)

var items = []
for (var z = 1; z <= 33; z++)
  items.push({ key: randomNumber(100000, 999999), value: randomWord(10) })

var THIS = this

$('.head').sortcolumn({
  columns: [
    { selector: ".key", name: "key" },
    { selector: ".value", name: "value" },
  ],
  sort: function(model, /*{}*/ column, callback) {
    $('.rows').listing("sort", { name: column.name, order: column.order })
  }
})

$('.page').pagination({
  nextprev: "always",
  changePage: function(model, /*num*/ page, callback) {
    $('.rows').listing("page", page)
  }
})

$('.rows').listing({
  makeRow: function(model, /*any*/ vo) {
    return vo ?
      $("<div><span class='col-1'>" + vo.key + "</span><span class='col-2'>" + vo.value + "</span></div>") :
      $("<div></div>")
  },
  sort: function(model, /*{}*/ sortField, callback) {
    model.sortField = sortField
    callback()
  },
  loadItems: function(model, callback) { // the data is sorted and supplied by the server
    if (model.sortField) {
      if (model.sortField.name == "key") {
        if (model.sortField.order == "asc") items.sort(sortKeysAsc)
        if (model.sortField.order == "desc") items.sort(sortKeysDesc)
      }
      if (model.sortField.name == "value") {
        if (model.sortField.order == "asc") items.sort(sortValuesAsc)
        if (model.sortField.order == "desc") items.sort(sortValuesDesc)
      }
    }

    var from  = model.itemsIndex,
        till  = Math.min(from+model.rows, items.length)
    callback(items.length, items.slice(from, till))
  },
  onRender: function(model) {
    if (model.sortField) $('.head').sortcolumn("update", model.sortField)
    $('.page').pagination("update", model.currentPage, model.totalPages)
  }
})
