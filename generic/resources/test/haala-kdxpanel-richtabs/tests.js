
QUnit.module( "kdx panel (rich tabs)" );

QUnit.asyncTest( "kdx panel tabs switch and collect values", function( assert ) {

  /* Build UI */

  $("#page-form .site-tabs").tabs();
  $("#page-form .submit button").button();

  tinymce.init({
    selector: "#page-form textarea",
    theme: "modern",
    plugins: "autoresize",
    toolbar1: "undo redo | bold italic underline strikethrough",
    menubar: false,
    toolbar_items_size: 'small',
    language: "en",
    width: 300
  });

//  example of full featured version
//  tinymce.init({
//    selector: "#page-form textarea",
//    theme: "modern",
//    plugins: "autolink lists table image hr link advlist contextmenu media fullscreen paste directionality visualchars nonbreaking anchor charmap code textcolor",
//    toolbar1: "code | undo redo | bold italic underline strikethrough | alignleft aligncenter alignright alignjustify | formatselect fontselect fontsizeselect",
//    toolbar2: "table | bullist numlist | outdent indent | forecolor backcolor | link unlink anchor image media | charmap hr nonbreaking | ltr rtl | fullscreen",
//    menubar: true,
//    toolbar_items_size: 'small',
//    language: "en"
//  });

  /* Populate values */

  $("#site-tab-xx1 textarea").html("<p>some english text</p>");
  $("#site-tab-xx2 textarea").html("<p>nekotoryj russkij tekst</p>");

  /* Start testing */

  expect(7);

  /* Tabs must be switchable */

  setTimeout(function() {
    var tab = $("#page-form a[href='#site-tab-xx2']").trigger("click");
    assert.ok(tab.parent("li").hasClass("ui-state-active"), "Russian tab is active");
    assert.ok($("#site-tab-xx2").is(":visible"), "Russian text is visible");
    assert.ok(!$("#site-tab-xx1").is(":visible"), "English text is not visible");
  }, 500);

  setTimeout(function() {
    var tab = $("#page-form a[href='#site-tab-xx1']").trigger("click");
    assert.ok(tab.parent("li").hasClass("ui-state-active"), "English tab is active");
    assert.ok($("#site-tab-xx1").is(":visible"), "English text is visible");
    assert.ok(!$("#site-tab-xx2").is(":visible"), "Russian text is not visible");
  }, 1000);

  /* Panel must collect proper values from tabs */

  var panel = $("#page-form").kdxPanel({
    input: [
      { element: "#site-tab-xx1"+" textarea", key: "note_xx1" },
      { element: "#site-tab-xx2"+" textarea", key: "note_xx2" }
    ]
  });

  setTimeout(function() {
    var container = panel.collect();
    var command = {};
    command.notes = Haala.Misc.collectSiteableInput(container, ["note"]);

    assert.deepEqual(
        command,
        { notes: [
          { site: "xx1", note: "<p>some english text</p>" },
          { site: "xx2", note: "<p>nekotoryj russkij tekst</p>" }
        ] },
        "Values collected");
  }, 1100);

  setTimeout(QUnit.start, 1200);

});
