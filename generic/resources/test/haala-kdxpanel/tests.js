
QUnit.module( "kdx panel" );

QUnit.asyncTest( "kdx panel tabs switch and collect values", function( assert ) {

  /* Build UI */

  $("#page-form .site-tabs").tabs();
  $("#page-form .submit button").button();

  /* Populate values */

  $("#site-tab-xx1 textarea").html("some english text");
  $("#site-tab-xx2 textarea").html("nekotoryj russkij tekst");

  /* Start testing */

  expect(7);

  /* Tabs must be switchable */

  setTimeout(function() {
    var tab = $("#page-form a[href='#site-tab-xx2']").trigger("click");
    assert.ok(tab.parent("li").hasClass("ui-state-active"), "Russian tab is active");
    assert.ok($("#site-tab-xx2").is(":visible"), "Russian text is visible");
    assert.ok(!$("#site-tab-xx1").is(":visible"), "English text is not visible");
  }, 500);

  setTimeout(function() {
    var tab = $("#page-form a[href='#site-tab-xx1']").trigger("click");
    assert.ok(tab.parent("li").hasClass("ui-state-active"), "English tab is active");
    assert.ok($("#site-tab-xx1").is(":visible"), "English text is visible");
    assert.ok(!$("#site-tab-xx2").is(":visible"), "Russian text is not visible");
  }, 1000);

  /* Panel must collect proper values from tabs */

  var panel = $("#page-form").kdxPanel({
    input: [
      { element: "#site-tab-xx1"+" textarea", key: "note_xx1" },
      { element: "#site-tab-xx2"+" textarea", key: "note_xx2" }
    ]
  });

  setTimeout(function() {
    var container = panel.collect();
    var command = {};
    command.notes = Haala.Misc.collectSiteableInput(container, ["note"]);

    assert.deepEqual(
        command,
        { notes: [
          { site: "xx1", note: "some english text" },
          { site: "xx2", note: "nekotoryj russkij tekst" }
        ] },
        "Values collected");
  }, 1100);

  setTimeout(QUnit.start, 1200);

});
