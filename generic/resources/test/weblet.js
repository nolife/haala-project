var express = require('express');
var request = require('request');

var app = express();

var config = require('./config.json');

app.use(express.static('.'));
app.use(express.static('../feed_ui/ui-xx1'));
app.use(express.static('../feed_ui/ui-xx1/jquery'));
app.use(express.static('../feed_themes'));
app.use(express.static('../feed_endorsed'));

app.listen(config.port, function() {
  console.log('Bound weblet to port '+config.port);
});
