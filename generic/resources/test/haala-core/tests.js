
QUnit.module( "store" );

QUnit.test( "store get/set/remove", function( assert ) {
  var hs = Haala.getStore();

  hs.set("my key", "my val");
  assert.equal( hs.get("my key"), "my val", "Set/Get passed" );
  assert.equal( hs.get("my key 2"), null, "No value passed" );
  assert.equal( hs.get("my key 2", "my val 2"), "my val 2", "Default value passed" );

  hs.remove("my key");
  assert.ok( hs.get("my key") == null, "Remove passed" );
});

QUnit.test( "store push", function( assert ) {
  var hs = Haala.getStore();
  hs.push("my key", "my val 1");
  hs.push("my key", "my val 2");
  hs.push("my key", "my val 3");

  assert.equal( hs.get("my key").length, 3, "Push passed" );
  assert.equal( hs.get("my key")[0], "my val 1", "First value OK" );
  assert.equal( hs.get("my key")[2], "my val 3", "Last value OK" );
});


QUnit.module( "label" );

QUnit.test( "label parametrize", function( assert ) {
  var hl = Haala.getLabel();
  var x = "This {ref} appeared {count} times";

  assert.equal( hl.parametrize(x), x, "No tokens OK" );
  assert.equal( hl.parametrize(x, {ref: "my val", count: 23}), "This my val appeared 23 times", "With tokens OK" );
});

QUnit.test( "label getOrElse", function( assert ) {
  var hl = Haala.getLabel();
  var x = "This {ref} appeared {count} times";
  var a = "{num} retries left";
  hl.model.box = { "my key": x };

  assert.equal( hl.getOrElse("my key", a), x, "GetOrElse passed" );
  assert.equal( hl.getOrElse("my key", a, {ref: "my val", count: 23}), "This my val appeared 23 times", "Value parametrized" );

  assert.equal( hl.getOrElse("my key 2", a), a, "Default value OK" );
  assert.equal( hl.getOrElse("my key 2", a, {num: 3}), a, "Default value not parametrized" );
});


QUnit.module( "misc" );

QUnit.test( "misc parametrize", function( assert ) {
  var hm = Haala.Misc;
  var x = "This {ref} appeared {count} times";

  assert.equal( hm.parametrize(x), x, "No tokens OK" );
  assert.equal( hm.parametrize(x, {ref: "my val", count: 23}), "This my val appeared 23 times", "With tokens OK" );
});

QUnit.test( "misc loadScript", function( assert ) {
  assert.expect(3);
  var script = "haala-core/misc-loadScript.js";
  TestResource = {};

  var done = assert.async();
  assert.ok(TestResource.miscScriptloaded == undefined, "Script not yet loaded");
  Haala.Misc.reloadScript(script, function() {
    assert.ok(TestResource.miscScriptloaded != undefined, "Script loaded");
    assert.equal(TestResource.miscScriptloaded(), "yes", "Function value verified");
    done();
  });
});

QUnit.test( "misc camelCase should transform a string", function( assert ) {
  var hm = Haala.Misc;
  assert.equal( hm.camelCase("first-name"), "firstName", "Passed" );
  assert.equal( hm.camelCase("last name"), "lastName", "Passed" );
  assert.equal( hm.camelCase("Middle_Name"), "MiddleName", "Passed" );
});

QUnit.test( "misc aclick should click", function( assert ) {
  assert.expect(3);
  var fixture = $( "#qunit-fixture" );
  fixture.append("<a id='click-me' href='#' onclick='return false;'>Click me</a>");

  var done = assert.async();
  $("#click-me").aclick(function(/*event*/ event) {
    assert.ok(true, "Callback executed");
    assert.equal($(event.target).attr("id"), "click-me", "Event target OK");
    assert.ok(event.isDefaultPrevented(), "Default event prevented");
    done();
  });

  $("#click-me").trigger("click");
});


QUnit.module( "ajax" );

QUnit.test( "ajax onSuccess call", function( assert ) {
  assert.expect(3);
  var ajax = Haala.AJAX;
  var dwr = "mock/LabelF-mock.js";
  LabelF = null;

  var done = assert.async();
  assert.ok(LabelF == null, "DWR not yet loaded");
  var dwrLoaded = function() {
    assert.ok(LabelF != null, "DWR loaded");
    ajax.call(null, LabelF.edit, { labelId: 123 },
      function() { assert.ok(true, "onSuccess executed"); done(); },
      function() { assert.ok(false, "onFail executed"); done(); },
      ajax.DO_NOTHING //disable all options
    );
  };

  Haala.Misc.reloadScript(dwr, dwrLoaded);
});

QUnit.test( "ajax onFail call", function( assert ) {
  assert.expect(3);
  var ajax = Haala.AJAX;
  var dwr = "mock/LabelF-mock.js";
  LabelF = null;

  var done = assert.async();
  assert.ok(LabelF == null, "DWR not yet loaded");
  var dwrLoaded = function() {
    assert.ok(LabelF != null, "DWR loaded");
    LabelF_result = { failed: true };
    ajax.call(null, LabelF.edit, { labelId: 123 },
      function() { assert.ok(false, "onSuccess executed"); done(); },
      function() { assert.ok(true, "onFail executed"); done(); },
      ajax.DO_NOTHING //disable all options
    );
  };

  Haala.Misc.reloadScript(dwr, dwrLoaded);
});


QUnit.module( "input-validator" );

QUnit.test( "input-validator validateInput", function( assert ) {
  var hv = new Haala.InputValidator();
  var fixture = $( "#qunit-fixture" );
  fixture.append(
    "<div id='page-form'>" +
      "<div class='username'>" +
        "<input />" +
        "<p class='error'></p>" +
      "</div>" +
      "<div class='password'>" +
        "<input />" +
        "<p class='error'></p>" +
      "</div>" +
    "</div>"
  );
  var arr = [
    { element: "#page-form .username input", key: "username",
      validate: { element: "#page-form .username .error", require: true, length: [5,10] }},
    { element: "#page-form .password input", key: "password",
      validate: { element: "#page-form .password .error", require: true, length: [5] }}
  ];

  $("#page-form .username input").val("user");
  assert.ok( hv.validateInput(arr) == false, "Not valid" );
  assert.equal( arr[0].validate.fail, "min-length", "Min length constraint" );
  assert.equal( arr[1].validate.fail, "require", "Required value constraint" );

  $("#page-form .username input").val("myuser");
  $("#page-form .password input").val("mypass");
  assert.ok( hv.validateInput(arr) == true, "Validation passed" );
});


QUnit.module( "date" );

QUnit.test( "date operations", function( assert ) {
  var hd = Haala.Date;

  assert.equal( hd.formatDate( 20161230 ), "30/12/2016", "formatDate num passed" );
  assert.equal( hd.formatDate("20161230"), "30/12/2016", "formatDate str passed" );
  
  assert.equal( hd.formatTime( 1259 ), "12:59", "formatTime num passed" );
  assert.equal( hd.formatTime("1259"), "12:59", "formatTime str passed" );

  assert.equal( hd.parseDate("30/12/2016"),  "20161230", "parseDate passed" );

  assert.equal( hd.parseTime("12:59"),  "1259", "parseTime passed" );

  assert.equal( hd.datetime( 20170317,   845 ),  "201703170845", "datetime num passed" );
  assert.equal( hd.datetime("20170317", "845"),  "201703170845", "datetime str passed" );

  assert.equal( hd.addDaysDate( 20170123,  1),  "20170124", "addDaysDate num passed" );
  assert.equal( hd.addDaysDate("20170123", 1),  "20170124", "addDaysDate str passed" );

  assert.equal( hd.addMinutesTime( 1230,  45),  "1315", "addMinutesTime num passed" );
  assert.equal( hd.addMinutesTime("1230", 45),  "1315", "addMinutesTime str passed" );
});
