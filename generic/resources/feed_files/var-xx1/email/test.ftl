<#import "/var/common.ftl" as c >

<b>Node time</b> <br>
<@h.nodeProperty property="timestamp" template="{date}" /> <br><br>

<b>Node details</b> <br>
Hostname <@h.nodeProperty property="hostname" /> <br>
IP       <@h.nodeProperty property="ip" />       <br>
MAC      <@h.nodeProperty property="mac" />      <br>  