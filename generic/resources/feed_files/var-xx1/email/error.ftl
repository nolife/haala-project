<#import "/var/common.ftl" as c >

<b>Node details</b> <br>
Hostname <@h.nodeProperty property="hostname" /> <br>
IP       <@h.nodeProperty property="ip" />       <br>
MAC      <@h.nodeProperty property="mac" />      <br><br>

<b>Total errors</b> <br>
${errors?size}      <br><br>

<#list errors as x >
  <b>Error ${x?index+1}</b> <br>
  ${x}                      <br/><br/>
</#list>
