<#import "/var/common.ftl" as c >

<b>Subject</b> <br>
<@h.messageEntryProperty entry=messageEntry property="subject" /> <br><br>

<b>Date</b> <br>
<@h.messageEntryProperty entry=messageEntry property="created" /> <br><br>

<b>Sender</b> <br>
<@h.messageEntryProperty entry=messageEntry property="senderName" />  <br>
<@h.messageEntryProperty entry=messageEntry property="senderEmail" /> <br><br>

<b>Text</b> <br>
<@h.messageEntryProperty entry=messageEntry property="text" /> <br><br>

<br><br> <hr>
<a href="${ControlPanelURL}/my/message/view.htm?ref=${messageEntry.abyss().ref()}">View this message in the Control Panel</a>
