<#import "/var/common.ftl" as c >

<b>Subject</b> <br>
<@h.messageEntryProperty entry=messageEntry property="subject" /> <br><br>

<b>Date</b> <br>
<@h.messageEntryProperty entry=messageEntry property="created" /> <br><br>

<b>Payment REF</b> <br>
<@h.messageEntryProperty entry=messageEntry property="meta.paymentRef" /> <br><br>

<b>User</b> <br>
<@h.messageEntryProperty var="userId" entry=messageEntry property="meta.userId" />
<@h.user var="tmpusr" userId=userId />
${tmpusr.username()}            <br>
<@h.userFullName user=tmpusr /> <br>
${tmpusr.email()}               <br><br>

<b>Text</b> <br>
<@h.messageEntryProperty entry=messageEntry property="text" /> <br><br>

<br><br> <hr>
<@h.messageEntryProperty var="abyssId" entry=messageEntry property="meta.abyssId" />
<a href="${ControlPanelURL}/su/payment/entry.htm?id=${abyssId}">View this payment in the Control Panel</a>

<br><br> <hr>
<a href="${ControlPanelURL}/my/message/view.htm?ref=${messageEntry.abyss().ref()}">View this message in the Control Panel</a>
