<#-- Common library -->

<#-- Print a label -->
<#macro label key tokens="" >${text(key, tokens)}</#macro>

<#-- function alias -->
<#function text key tokens="" >
  <#local label ><@h.label key=key tokens=tokens /></#local>
  <#return label >
</#function>
