package com.coldcore.haala
package service

import org.scalatest.{BeforeAndAfter, FlatSpec, Matchers}
import java.util.Locale

import org.scalatest.mockito.MockitoSugar
import org.mockito.Mockito._
import org.mockito.ArgumentMatchers._
import org.mockito.ArgumentCaptor
import domain.Domain
import org.junit.runner.RunWith
import org.scalatest.junit.JUnitRunner

@RunWith(classOf[JUnitRunner])
class CurrencyServiceSpec extends FlatSpec with MockitoSugar with BeforeAndAfter with Matchers {
  var m: MyMock = _

  before {
    m = new MyMock
  }

  class MyMock extends com.coldcore.haala.service.test.MyMock {
    val service = new CurrencyServiceImpl |< { x =>
      x.serviceContainer = serviceContainer
    }
  }

  it should "get sign" in {
    when(m.settingService.querySystem("CurrencySigns")).thenReturn("GBP=GB$, EUR=EU$, USD=$, AUD=AU$, ZAR=R, CAD=CA$")
    assertResult("$") { m.service.getSign("USD") }
    assertResult("RUB") { m.service.getSign("RUB") }
  }

  it should "resolve currency" in {
    val (gbp, rub, eur) = (new Locale("en", "gb"), new Locale("ru", "ru"), new Locale("fr", "fr"))
    when(m.settingService.querySystem("Currencies")).thenReturn("GBP, EUR, USD")
    when(m.settingService.getDefaultCurrency).thenReturn("GBP")
    when(m.domainService.resolveDomain("unknown.com")).thenReturn(
      new Domain { id = 1; domain = "*"; meta = "resolve=currency"; `type` = Domain.TYPE_MAIN })
    when(m.domainService.resolveDomain("myagency.com")).thenReturn(
      new Domain { id = 1001; domain = "myagency.com"; meta = "resolve=currency"; `type` = "agency" })
    when(m.domainService.resolveDomain("rudomain.com")).thenReturn(
      new Domain { id = 1002; domain = "rudomain.com"; meta = "currency=RUB"; `type` = "agency" })
    assertResult("GBP") { m.service.resolveCurrency("unknown.com") }
    assertResult("GBP") { m.service.resolveCurrency("unknown.com", gbp) }
    assertResult("GBP") { m.service.resolveCurrency("unknown.com", gbp, eur) }
    assertResult("EUR") { m.service.resolveCurrency("unknown.com", eur) }
    assertResult("EUR") { m.service.resolveCurrency("unknown.com", eur, gbp) }
    assertResult("GBP") { m.service.resolveCurrency("unknown.com", rub) }
    assertResult("EUR") { m.service.resolveCurrency("unknown.com", rub, eur) }
    assertResult("EUR") { m.service.resolveCurrency("unknown.com", rub, eur, gbp) }
    assertResult("EUR") { m.service.resolveCurrency("myagency.com", rub, eur, gbp) }
    assertResult("RUB") { m.service.resolveCurrency("rudomain.com", rub, eur, gbp) }
  }

  it should "convert" in {
    when(m.settingService.querySystem("CurrencyRates")).thenReturn("GBP=1.000000, EUR=1.150000, USD=1.800000")
    when(m.settingService.getDefaultCurrency).thenReturn("GBP")
    assertResult(100) { m.service.convert(100, "GBP", "GBP") }
    assertResult(100) { m.service.convert(100, "EUR", "EUR") }
    assertResult(115) { m.service.convert(100, "GBP", "EUR") }
    assertResult(100) { m.service.convert(115, "EUR", "GBP") }
    assertResult(115) { m.service.convert(180, "USD", "EUR") }
    assertResult(180) { m.service.convert(115, "EUR", "USD") }
    assertResult(0) { m.service.convert(0, "EUR", "USD") }
    assertResult(0) { m.service.convert(100, "EUR", "RUB") }
  }

}
