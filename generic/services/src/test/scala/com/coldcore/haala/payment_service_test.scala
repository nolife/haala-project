package com.coldcore.haala
package service

import core.{Meta, Timestamp}
import PaymentService.{CreatePaymentIN, Mod, PaymentIN}
import exceptions.InvalidStatusException
import org.scalatest.mockito.MockitoSugar
import org.mockito.Mockito._
import org.mockito.ArgumentMatchers._
import org.scalatest._
import org.junit.runner.RunWith
import org.scalatest.junit.JUnitRunner
import domain.{AbyssEntry, User}
import scala.collection.JavaConverters._

@RunWith(classOf[JUnitRunner])
class PaymentServiceSpec extends FlatSpec with MockitoSugar with BeforeAndAfter with Matchers {
  var m: MyMock = _

  before {
    m = new MyMock
  }

  class MyMock extends com.coldcore.haala.service.test.MyMock {
    val service = new PaymentServiceImpl { serviceContainer = m.serviceContainer }

    when(settingService.querySystem("PaymentConf")).thenReturn(setting_paymentConf)

    val parentAbyss = new AbyssEntry { id = 3L; ref = "R123"; pack = "mypack" }
    when(abyssService.getById(3)).thenReturn(Some(parentAbyss))
  }

  val setting_paymentConf = """
    |{
    |   "keep": "30d",
    |   "expire": "7d",
    |   "checkoutURL": "http://example.org/pay"
    |}
    """.stripMargin.trim

  val paymentValue = "parent=3;ref=R123_3_FOO;amount=2500;currency=GBP;expire=20180216235948778;user=2;username=tester;closed=n;"
  val paymentMeta = "s_typ=pay;s_ref=R123_3_FOO;s_usr=tester;"

  "create" should "create new payment entry and new abyss" in {
    val parentAbyss = new AbyssEntry
    when(m.abyssService.getById(3)).thenReturn(None)
    when(m.userService.getById(2)).thenReturn(Some(new User { id = 2L; ref = "FOO"; username = "tester" }))
    doAnswer(invocation => { // create parent abyss
      val x = invocation.getArguments.head.asInstanceOf[AbyssService.CreateIN]
      parentAbyss.id = 3L; parentAbyss.ref = x.ref; parentAbyss.pack = x.pack
      parentAbyss.value = x.vm.value; parentAbyss.meta = x.vm.meta
      when(m.abyssService.getById(3)).thenReturn(Some(parentAbyss))
      parentAbyss
    }).when(m.abyssService).create(any[AbyssService.CreateIN])
    doAnswer(invocation => { // create payment abyss
      val x = invocation.getArguments.last.asInstanceOf[AbyssService.ValueMetaIN]
      new AbyssEntry { id = 4L; value = x.value; meta = x.meta; ref = parentAbyss.ref; pack = parentAbyss.pack }
    }).when(m.abyssService).create(anyLong, any[AbyssService.ValueMetaIN])

    val r = m.service.create(2,
      CreatePaymentIN("mypack", "R123", "my-val", "prop=a", PaymentIN(2500, "GBP", expire = Timestamp("20180216235948778"))))
    (r.abyss.id, r.amount, r.username, r.ref) shouldBe (4, 2500, "tester", "R123_3_FOO")
    (r.abyss.value, r.abyss.meta, r.abyss.ref, r.abyss.pack) shouldBe (paymentValue, paymentMeta, "R123", "mypack")
    (parentAbyss.value, parentAbyss.meta, parentAbyss.ref, parentAbyss.pack) shouldBe ("my-val", "prop=a", "R123", "mypack")
  }

  "create" should "create new payment entry for existing abyss" in {
    when(m.userService.getById(2)).thenReturn(Some(new User { id = 2L; ref = "FOO"; username = "tester" }))
    doAnswer(invocation => { // create payment abyss
      val x = invocation.getArguments.last.asInstanceOf[AbyssService.ValueMetaIN]
      new AbyssEntry { id = 4L; value = x.value; meta = x.meta; ref = m.parentAbyss.ref; pack = m.parentAbyss.pack }
    }).when(m.abyssService).create(anyLong, any[AbyssService.ValueMetaIN])

    val r = m.service.create(2, 3, PaymentIN(2500, "GBP", expire = Timestamp("20180216235948778")))
    (r.abyss.id, r.amount, r.username, r.ref) shouldBe (4, 2500, "tester", "R123_3_FOO")
    (r.abyss.value, r.abyss.meta, r.abyss.ref, r.abyss.pack) shouldBe (paymentValue, paymentMeta, "R123", "mypack")
  }

  "close" should "mark payment as closed" in {
    val abyss = new AbyssEntry { id = 4L; value = paymentValue; meta = paymentMeta }
    when(m.abyssService.getById(4)).thenReturn(Some(abyss))
    doAnswer(invocation => {
      val x = invocation.getArguments.last.asInstanceOf[AbyssService.ValueMetaIN]
      if (x.value != "") abyss.value = x.value
      if (x.meta != "") abyss.meta = x.meta
    }).when(m.abyssService).update(anyLong, any[AbyssService.ValueMetaIN])

    m.service.close(4)
    Meta(abyss.value)("closed") shouldBe "y"
    abyss.meta shouldBe paymentMeta
  }

  "push" should "push payment for further processing and close it" in {
    var processed = false
    class ModA extends Mod { override def process(abyss: AbyssEntry): Boolean = { processed = true; true }}
    class ModB extends Mod { override def process(abyss: AbyssEntry): Boolean = throw new Exception("evaluated") }
    m.service.setMods(List(new ModA, new ModB).asJava)

    val abyss = new AbyssEntry { id = 4L; value = paymentValue; meta = paymentMeta }
    when(m.abyssService.getById(4)).thenReturn(Some(abyss))
    doAnswer(invocation => {
      val a = invocation.getArguments.head.asInstanceOf[Long]
      val b = invocation.getArguments.last.asInstanceOf[AbyssService.ValueMetaIN]
      if (a == 4) {
        if (b.value != "") abyss.value = b.value
        if (b.meta != "") abyss.meta = b.meta
      }
    }).when(m.abyssService).update(anyLong, any[AbyssService.ValueMetaIN])

    m.service.push(4)
    processed shouldBe true
    Meta(abyss.value)("closed") shouldBe "y"
  }

  "push" should "reject or accept test payments" in {
    var processed = false
    class ModA extends Mod { override def process(abyss: AbyssEntry): Boolean = { processed = true; true }}
    class ModB extends Mod { override def process(abyss: AbyssEntry): Boolean = throw new Exception("evaluated") }
    m.service.setMods(List(new ModA, new ModB).asJava)

    val abyssA = new AbyssEntry { id = 4L; value = paymentValue; meta = paymentMeta }
    val abyssB = new AbyssEntry { id = 5L; value = paymentValue+";test=y"; meta = paymentMeta }
    when(m.abyssService.getById(4)).thenReturn(Some(abyssA))
    when(m.abyssService.getById(5)).thenReturn(Some(abyssB))

    an [InvalidStatusException] should be thrownBy m.service.push(4, test = true)

    m.service.push(5, test = true)
    processed shouldBe true
  }

  "push" should "reject closed payments" in {
    val abyss = new AbyssEntry { id = 4L; value = (Meta(paymentValue) + ("closed" -> "y")).serialize; meta = paymentMeta }
    when(m.abyssService.getById(4)).thenReturn(Some(abyss))

    an [InvalidStatusException] should be thrownBy m.service.push(4)
  }

}
