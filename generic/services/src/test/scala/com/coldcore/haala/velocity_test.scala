package com.coldcore.haala
package service

import org.scalatest.mockito.MockitoSugar
import org.mockito.Mockito._
import org.scalatest._
import org.junit.runner.RunWith
import org.scalatest.junit.JUnitRunner
import org.apache.velocity.exception.ResourceNotFoundException
import core.SiteChain.Implicits._
import core.VirtualPath.Implicits._

@RunWith(classOf[JUnitRunner])
class VelocityEngineSpec extends WordSpec with MockitoSugar with BeforeAndAfter {
  var m: MyMock = _

  before {
    m = new MyMock
  }

  class MyMock extends com.coldcore.haala.service.test.MyMock {
    when(fileService.getByPath("bar/unknown.vm", "xx1.un1")).thenReturn(None)
    registerFile(11, "bar/template.vm", "xx1.mc1", "Hello ${username}")
    registerFile(16, "bar/template.vm", ".sys", "This is ${location}!")

    registerFile(12, "bar/template.vm", "xx1.mh1", template_mh)
    registerFile(13, "bar/depend.vm", "xx1.mh1", depend_mh)
    registerFile(14, "bar/other.vm", "xx1.mh1", other_mh)
  }

  private val template_mh = """
      |Hello ${username}. #include("/bar/depend.vm:xx1.mh1") and #include("/bar/other.vm:xx1.mh1")
    """.stripMargin.trim // relative path #include("other.vm:xx1.mh1") does not work in Velocity

  private val depend_mh = """
      |<font size=2>Hello Fred!</font>
    """.stripMargin.trim

  private val other_mh = """
      |<font color=red>Hello Batman!</font>
    """.stripMargin.trim

  "engine" should {
    "load correct template" in {
      val r = m.processVm("bar/template.vm:xx1.mc1", Map("username" -> "Big Joe"))
      assertResult("Hello Big Joe") { r }
    }

    "error if template not found" in {
      intercept[ResourceNotFoundException] { m.vmEngine.template("bar/unknown.vm:xx1.un1") }
    }

    "resolve template dependencies" in {
      val r = m.processVm("bar/template.vm:xx1.mh1", Map("username" -> "Big Joe"))
      assertResult("Hello Big Joe. <font size=2>Hello Fred!</font> and <font color=red>Hello Batman!</font>") {
        r.replace("\n", "").replaceAll("\\s+", " ")
      }
    }

    "load sys template when no site given" in {
      val r = m.processVm("bar/template.vm", Map("location" -> "Sparta"))
      assertResult("This is Sparta!") { r }
    }
  }

}
