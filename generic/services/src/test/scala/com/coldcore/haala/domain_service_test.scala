package com.coldcore.haala
package service

import org.scalatest.{BeforeAndAfter, FlatSpec, Matchers}
import java.util.Locale

import domain.Domain
import org.scalatest.mockito.MockitoSugar
import org.mockito.Mockito._
import org.mockito.ArgumentMatchers.{eq => meq, _}
import org.mockito.ArgumentCaptor
import dao.GenericDAO
import core.SiteUtil._
import core.SiteChain.Implicits._
import core.{ParsedSiteSetting, SiteChain}
import DomainService.IN
import exceptions.{ObjectExistsException, ObjectNotFoundException}
import org.junit.runner.RunWith
import org.scalatest.junit.JUnitRunner

@RunWith(classOf[JUnitRunner])
class DomainServiceSpec extends FlatSpec with MockitoSugar with BeforeAndAfter with Matchers {
  var m: MyMock = _

  before {
    m = new MyMock
  }

  class MyMock extends com.coldcore.haala.service.test.MyMock {
    val (domainDao, domainsCache) = (mock[GenericDAO[Domain]], mock[DomainCache[Domain]])
    val service = new DomainServiceImpl |< { x =>
      x.serviceContainer = serviceContainer
      x.domainDao = domainDao; x.domainsCache = domainsCache
    }
  }

  def XIN(domain: String, domainSite: SiteChain) = IN(domain, domainSite, ""+Domain.TYPE_OTHER, "")

  "getByDomain" should "find by domain" in {
    m.service.domainsCache = new GuavaDomainCache[Domain] |< { x => x.serviceContainer = m.serviceContainer; x.afterPropertiesSet }
    when(m.domainDao.findByProperty(meq("domain"), anyString)).thenReturn(Nil)
    when(m.domainDao.findByProperty("domain", "myagency.com")).thenReturn(new Domain { id = 1L; domain = "myagency.com" } :: Nil)

    m.service.getByDomain("unknown.com") shouldBe None
    m.service.getByDomain("myagency.com").get.domain shouldBe "myagency.com"
  }

  "getByAlias" should "find by domain or alias" in {
    m.service.domainsCache = new GuavaDomainCache[Domain] |< { x => x.serviceContainer = m.serviceContainer; x.afterPropertiesSet }
    when(m.domainDao.findByProperty(meq("domain"), anyString)).thenReturn(Nil)
    when(m.domainDao.findByProperty("domain", "example.org")).thenReturn(new Domain { id = 1L; domain = "example.org"; meta = "cname=www,foo" } :: Nil)

    m.service.getByAlias("example.org"    ).get.domain shouldBe "example.org"
    m.service.getByAlias("www.example.org").get.domain shouldBe "example.org"
    m.service.getByAlias("foo.example.org").get.domain shouldBe "example.org"
    m.service.getByAlias("bar.example.org") shouldBe None
    m.service.getByAlias("www"            ) shouldBe None
  }

  "getByType" should "find by type" in {
    m.service.domainsCache = new GuavaDomainCache[Domain] |< { x => x.serviceContainer = m.serviceContainer; x.afterPropertiesSet }
    when(m.domainDao.findUniqueByProperty("type", "example")).thenReturn(None)
    when(m.domainDao.findUniqueByProperty("type", "main")).thenReturn(Some(new Domain { id = 1L; domain = "*" }))

    m.service.getByType("example") shouldBe None
    m.service.getByType("main").get.domain shouldBe "*"
  }

  it should "delete" in {
    val x = new Domain { id = 7L; domain = "myagency.com" }
    when(m.domainDao.findById(7)).thenReturn(Some(x))
    m.service.delete(7)
    verify(m.domainDao).delete(x)
    verify(m.domainsCache).remove("myagency.com")
  }

  it should "create" in {
    when(m.domainDao.findByProperty("domain", "create.me")).thenReturn(Nil)
    when(m.domainsCache.get(anyString)).thenReturn(None)
    m.service.create(XIN("create.me", "rr.foo"))
    val arg = ArgumentCaptor.forClass(classOf[Domain])
    verify(m.domainDao).saveOrUpdate(arg.capture)
    arg.getValue.domain shouldBe "create.me"
    verify(m.domainsCache).remove("create.me")

    when(m.domainDao.findByProperty("domain", "existing.me")).thenReturn(new Domain { id = 1L; domain = "existing.me" } :: Nil)
    when(m.domainsCache.get(anyString)).thenReturn(None)
    intercept[ObjectExistsException] { m.service.create(XIN("existing.me", "rr.foo")) }
  }

  it should "update" in {
    when(m.domainDao.findById(1)).thenReturn(None)
    intercept[ObjectNotFoundException] { m.service.update(1, XIN("create.me", "rr.bar")) }

    when(m.domainDao.findById(1)).thenReturn(Some(new Domain { id = 1L }))
    when(m.domainDao.findByProperty("domain", "existing.me")).thenReturn(new Domain { id = 2L; domain = "existing.me" } :: Nil)
    when(m.domainsCache.get(anyString)).thenReturn(None)
    intercept[ObjectExistsException] { m.service.update(1, XIN("existing.me", "rr.bar")) }

    when(m.domainDao.findById(1)).thenReturn(Some(new Domain { id = 1L }))
    when(m.domainDao.findByProperty("domain", "updated.me")).thenReturn(new Domain { id = 1L; domain = "update.me" } :: Nil)
    when(m.domainsCache.get(anyString)).thenReturn(None)
    m.service.update(1, XIN("updated.me", "rr.bar"))
    val arg = ArgumentCaptor.forClass(classOf[Domain])
    verify(m.domainDao).saveOrUpdate(arg.capture)
    arg.getValue.domainSite shouldBe "rr.bar"
    verify(m.domainsCache).remove("updated.me")
  }

  it should "resolve domain" in {
    m.service.domainsCache = new GuavaDomainCache[Domain] |< { x => x.serviceContainer = m.serviceContainer; x.afterPropertiesSet }
    when(m.domainDao.findByProperty(meq("domain"), anyString)).thenReturn(Nil)
    when(m.domainDao.findByProperty("domain", "*")).thenReturn(new Domain { id = 1L; domain = "*" } :: Nil)
    when(m.domainDao.findByProperty("domain", "unknown.com")).thenReturn(new Domain { id = 1L; domain = "*" } :: Nil)
    when(m.domainDao.findByProperty("domain", "myagency.com")).thenReturn(new Domain { id = 2L; domain = "myagency.com" } :: Nil)
    when(m.domainDao.findByProperty("domain", "example.org")).thenReturn(new Domain { id = 3L; domain = "example.org"; meta = "cname=www" } :: Nil)

    m.service.resolveDomain("*"              ).domain shouldBe "*"
    m.service.resolveDomain("unknown.com"    ).domain shouldBe "*"
    m.service.resolveDomain("myagency.com"   ).domain shouldBe "myagency.com"
    m.service.resolveDomain("www.example.org").domain shouldBe "example.org"
  }

  "resolveRootSite" should "construct domain root site based on locale" in {
    m.service.domainsCache = new GuavaDomainCache[Domain] |< { x => x.serviceContainer = m.serviceContainer; x.afterPropertiesSet }
    val (en, ru, fr, rs) = (new Locale("en"), new Locale("ru"), new Locale("fr"), new Locale("rs"))
    val myagency = new Domain{ id = 1L; domain = "myagency.com"; domainSite = "pp.agy"; meta = "resolve=site" }
    val rudomain = new Domain{ id = 2L; domain = "rudomain.com"; domainSite = "pp.rug"; meta = "site=xx2" }
    val rswebnet = new Domain{ id = 3L; domain = "rswebnet.com"; domainSite = "pp.rsg"; meta = "resolve=site" }
    when(m.domainDao.findByProperty("domain", "myagency.com")).thenReturn(myagency :: Nil)
    when(m.domainDao.findByProperty("domain", "rudomain.com")).thenReturn(rudomain :: Nil)
    when(m.domainDao.findByProperty("domain", "rswebnet.com")).thenReturn(rswebnet :: Nil)
    when(m.settingService.querySystem("Sites")).thenReturn("xx1=xx1:en, xx2=xx1.xx2:ru|rs")
    when(m.settingService.getParsedSites).thenReturn(ParsedSiteSetting("xx1=xx1:en") :: ParsedSiteSetting("xx2=xx1.xx2:ru|rs") :: Nil)
    when(m.settingService.getDefaultSite).thenReturn("xx1")

    m.service.resolveRootSite("myagency.com")             shouldBe "xx1"
    m.service.resolveRootSite("myagency.com", fr)         shouldBe "xx1"
    m.service.resolveRootSite("myagency.com", en)         shouldBe "xx1"
    m.service.resolveRootSite("myagency.com", fr, en)     shouldBe "xx1"
    m.service.resolveRootSite("myagency.com", ru)         shouldBe "xx1.xx2"
    m.service.resolveRootSite("myagency.com", fr, ru)     shouldBe "xx1.xx2"
    m.service.resolveRootSite("myagency.com", ru, en)     shouldBe "xx1.xx2"
    m.service.resolveRootSite("myagency.com", en, ru)     shouldBe "xx1"
    m.service.resolveRootSite("myagency.com", fr, en, ru) shouldBe "xx1"
    m.service.resolveRootSite("rudomain.com", fr, en, ru) shouldBe "xx1.xx2"
    m.service.resolveRootSite("rudomain.com")             shouldBe "xx1.xx2"
    m.service.resolveRootSite("rswebnet.com", fr, rs, en) shouldBe "xx1.xx2"
  }

  "defaultRootSite" should "construct domain default root site chain" in {
    m.service.domainsCache = new GuavaDomainCache[Domain] |< { x => x.serviceContainer = m.serviceContainer; x.afterPropertiesSet }
    val myagency = new Domain{ id = 1L; domain = "myagency.com"; domainSite = "pp.agy"; meta="resolve=site" }
    val rudomain = new Domain{ id = 2L; domain = "rudomain.com"; domainSite = "pp.rug"; meta="site=xx2" }
    when(m.domainDao.findByProperty("domain", "myagency.com")).thenReturn(myagency :: Nil)
    when(m.domainDao.findByProperty("domain", "rudomain.com")).thenReturn(rudomain :: Nil)
    when(m.settingService.querySystem("Sites")).thenReturn("xx1=xx1:en, xx2=xx1.xx2:ru|rs")
    when(m.settingService.getDefaultSite).thenReturn("xx1")
    m.service.defaultRootSite("myagency.com") shouldBe "xx1"
    m.service.defaultRootSite("rudomain.com") shouldBe "xx1.xx2"
  }

  "getDefaultSite" should "construct domain default site chain" in {
    m.service.domainsCache = new GuavaDomainCache[Domain] |< { x => x.serviceContainer = m.serviceContainer; x.afterPropertiesSet }
    val myagency = new Domain{ id = 1L; domain = "myagency.com"; domainSite = "pp.agy"; meta="resolve=site" }
    val rudomain = new Domain{ id = 2L; domain = "rudomain.com"; domainSite = "pp.rug"; meta="site=xx2" }
    when(m.domainDao.findByProperty("domain", "myagency.com")).thenReturn(myagency :: Nil)
    when(m.domainDao.findByProperty("domain", "rudomain.com")).thenReturn(rudomain :: Nil)
    when(m.settingService.querySystem("Sites")).thenReturn("xx1=xx1:en, xx2=xx1.xx2:ru|rs")
    when(m.settingService.getDefaultSite).thenReturn("xx1")

    m.service.defaultSite("myagency.com") shouldBe "xx1.pp1.agy1"
    m.service.defaultSite("rudomain.com") shouldBe "xx1.pp1.rug1.xx2.pp2.rug2"
  }

  "listSites" should "list domain site keys" in {
    val o = new Domain { id = 1L; domainSite = "wis" }
    when(m.settingService.getParsedSites).thenReturn(parseSiteSetting("xx1=xx1:en, xx2=xx1.xx2:ru|rs"))
    m.service.listSites(o) shouldBe List("wis1", "wis2")
  }

}
