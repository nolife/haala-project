package com.coldcore.haala
package service

import org.scalatest.mockito.MockitoSugar
import org.mockito.Mockito._
import org.mockito.ArgumentMatchers._
import org.mockito.ArgumentCaptor
import org.scalatest._
import org.junit.runner.RunWith
import org.scalatest.junit.JUnitRunner
import domain.{Domain, User}
import org.mockito.stubbing.Answer
import org.mockito.invocation.InvocationOnMock
import core.HttpUtil.HttpCallSuccessful
import service.exceptions.{EmailExistsException, GeneratorExhaustedException, ObjectNotFoundException, UsernameExistsException}
import com.google.gson.Gson
import service.FacebookService.{FbUser, RegisterIN}

import com.coldcore.haala.core.Timestamp

@RunWith(classOf[JUnitRunner])
class FacebookServiceSpec extends WordSpec with MockitoSugar with BeforeAndAfter {
  var m: MyMock = _

  before {
    m = new MyMock
  }

  class MyMock extends com.coldcore.haala.service.test.MyMock {
    val service = new FacebookServiceImpl { override val staticBlock = m.staticBlock; serviceContainer = m.serviceContainer }
    when(settingService.querySystem("RegEx/username")).thenReturn("[a-zA-Z][0-9a-zA-Z_]{4,99}")
  }

  // https://graph.facebook.com/me?access_token=AAAIEsU3tbjEBADkDqk82uDiUEcYCykRdBy5ZAd
  private val response_fbUser = """
    |{
    |   "id": "100003107018157",
    |   "name": "Sergei Abramov",
    |   "first_name": "Sergei",
    |   "last_name": "Abramov",
    |   "link": "http://www.facebook.com/abramov.sergei",
    |   "username": "abramov.sergei",
    |   "hometown": {
    |      "id": "106039436102339",
    |      "name": "Tallinn, Estonia"
    |   },
    |   "location": {
    |      "id": "106078429431815",
    |      "name": "London, United Kingdom"
    |   },
    |   "bio": "Java / Scala / Web developer",
    |   "work": [
    |      {
    |         "employer": {
    |            "id": "109439529075074",
    |            "name": "Lloyds Bank"
    |         },
    |         "location": {
    |            "id": "106078429431815",
    |            "name": "London, United Kingdom"
    |         },
    |         "position": {
    |            "id": "106134736085305",
    |            "name": "Software Developer"
    |         },
    |         "start_date": "0000-00"
    |      },
    |      {
    |         "employer": {
    |            "id": "107819492601508",
    |            "name": "Cognitive Match"
    |         },
    |         "position": {
    |            "id": "106134736085305",
    |            "name": "Software Developer"
    |         },
    |         "start_date": "0000-00",
    |         "end_date": "0000-00"
    |      },
    |      {
    |         "employer": {
    |            "id": "112307422119147",
    |            "name": "News International"
    |         },
    |         "position": {
    |            "id": "106134736085305",
    |            "name": "Software Developer"
    |         },
    |         "start_date": "0000-00",
    |         "end_date": "0000-00"
    |      },
    |      {
    |         "employer": {
    |            "id": "109684849050229",
    |            "name": "Associated Newspapers"
    |         },
    |         "position": {
    |            "id": "106134736085305",
    |            "name": "Software Developer"
    |         },
    |         "start_date": "0000-00",
    |         "end_date": "0000-00"
    |      },
    |      {
    |         "employer": {
    |            "id": "111931675491694",
    |            "name": "BSKYB"
    |         },
    |         "location": {
    |            "id": "106078429431815",
    |            "name": "London, United Kingdom"
    |         },
    |         "position": {
    |            "id": "106134736085305",
    |            "name": "Software Developer"
    |         },
    |         "start_date": "0000-00",
    |         "end_date": "0000-00"
    |      },
    |      {
    |         "employer": {
    |            "id": "108000409228845",
    |            "name": "BBC"
    |         },
    |         "location": {
    |            "id": "106078429431815",
    |            "name": "London, United Kingdom"
    |         },
    |         "position": {
    |            "id": "106134736085305",
    |            "name": "Software Developer"
    |         },
    |         "start_date": "0000-00",
    |         "end_date": "0000-00"
    |      }
    |   ],
    |   "education": [
    |      {
    |         "school": {
    |            "id": "111969238838316",
    |            "name": "Tallinna Kesklinna Vene G\u00fcmnaasium"
    |         },
    |         "type": "High School"
    |      },
    |      {
    |         "school": {
    |            "id": "264877868947",
    |            "name": "Tallinna Tehnika\u00fclikool/Tallinn University of Technology"
    |         },
    |         "type": "College"
    |      }
    |   ],
    |   "gender": "male",
    |   "email": "sector\u0040coldcore.com",
    |   "timezone": 0,
    |   "locale": "en_GB",
    |   "languages": [
    |      {
    |         "id": "112624162082677",
    |         "name": "Russian"
    |      },
    |      {
    |         "id": "106059522759137",
    |         "name": "English"
    |      },
    |      {
    |         "id": "112114182148309",
    |         "name": "Estonian"
    |      }
    |   ],
    |   "verified": true,
    |   "updated_time": "2013-01-21T22:44:46+0000"
    |}
    """.stripMargin.trim

  private val response_fbUser_error = """
    |{
    |   "error": {
    |      "message": "Error validating access token: The session was invalidated explicitly using an API call.",
    |      "type": "OAuthException",
    |      "code": 190,
    |      "error_subcode": 466
    |   }
    |}
    """.stripMargin.trim

  private val fbUser = new Gson().fromJson(response_fbUser, classOf[FbUser])

  "getUser" should {
    "return nothing if user cannot be retrieved" in {
      when(m.staticBlock.httpGet("https://graph.facebook.com/me?access_token=AABBFF")).thenReturn(
        HttpCallSuccessful(null, 200, response_fbUser_error, List.empty))
      assertResult(None) { m.service.getUser("AABBFF") }
    }

    "return a user by valid token" in {
      when(m.staticBlock.httpGet("https://graph.facebook.com/me?access_token=AABBCC")).thenReturn(
        HttpCallSuccessful(null, 200, response_fbUser, List.empty))
      val r = m.service.getUser("AABBCC").get
      assertResult("100003107018157") { r.id }
      assertResult("sector@coldcore.com") { r.email }
    }
  }

  "generateUsername" should {
    "fail if exhausted" in {
      when(m.userService.getByUsername(anyString)).thenReturn(Some(new User))
      intercept[GeneratorExhaustedException] { m.service.generateUsername(fbUser) }
    }

    "generate valid usernave from facebook data" in {
      when(m.userService.getByUsername(anyString)).thenReturn(None)
      var u = new Gson().fromJson(response_fbUser, classOf[FbUser])
      val r = (len: Int) => m.service.generateUsername(u).substring(0, len)

      val taken = collection.mutable.ListBuffer[String]()
      doAnswer(new Answer[Option[User]] { //return User if supplied username is in "taken" list
        val found = (s: String) => taken.exists(x => s.matches(x+"\\d+"))
        override def answer(invocation: InvocationOnMock): Option[User] = {
          found(invocation.getArguments.head.toString) ? Some(new User) | None
        }
      }).when(m.userService).getByUsername(anyString)

      //original username with replaced "."
      assertResult("abramov_sergei") { r(14) }
      u.username = ""
      //no username, use first name
      assertResult("sergei") { r(6) }
      u.username = "sector"
      //test entire sequence of suggested usernames
      assertResult("sector") { r(6) }; taken += "sector"
      assertResult("sergei") { r(6) }; taken += "sergei"
      assertResult("abramov") { r(7) }; taken += "abramov"
      assertResult("sergei_abramov") { r(14) }; taken += "sergei_abramov"
      assertResult("abramov_sergei") { r(14) }; taken += "abramov_sergei"
      assertResult("sergeiabramov") { r(13) }; taken += "sergeiabramov"
      assertResult("abramovsergei") { r(13) }; taken += "abramovsergei"
      assertResult("user") { r(4) }
    }
  }

  "register" should {
    "fail if email already exists" in {
      when(m.userService.getByUsername(anyString)).thenReturn(None)
      when(m.userService.getByEmail("sector@coldcore.com")).thenReturn(Some(new User))
      intercept[EmailExistsException] { m.service.register(RegisterIN(fbUser, "", "")) }
      when(m.userService.getByEmail("sector@coldcore.com")).thenReturn(None)
      when(m.userService.getByEmail("sergei@coldcore.com")).thenReturn(Some(new User))
      intercept[EmailExistsException] { m.service.register(RegisterIN(fbUser, "", "sergei@coldcore.com")) }
    }

    "fail if username already exists" in {
      when(m.userService.getByEmail(anyString)).thenReturn(None)
      when(m.userService.getByUsername("abramov.sergei")).thenReturn(Some(new User))
      intercept[UsernameExistsException] { m.service.register(RegisterIN(fbUser, "", "")) }
      when(m.userService.getByUsername("abramov.sergei")).thenReturn(None)
      when(m.userService.getByUsername("sector")).thenReturn(Some(new User))
      intercept[UsernameExistsException] { m.service.register(RegisterIN(fbUser, "sector", "")) }
    }

    "register new user and activate it" in {
      val user = new User { id = 22 }
      when(m.userService.getByEmail(anyString)).thenReturn(None)
      when(m.userService.getByUsername(anyString)).thenReturn(None)
      doAnswer(new Answer[User] {
        override def answer(invocation: InvocationOnMock): User = user
      }).when(m.userService).register(any.asInstanceOf[UserService.IN], any.asInstanceOf[Domain])
      doAnswer(new Answer[Unit] {
        override def answer(invocation: InvocationOnMock): Unit = user.activated = Timestamp()
      }).when(m.userService).activate(22)

      val r = m.service.register(RegisterIN(fbUser, "", ""))
      assertResult(22) { r.id }
      assertResult(true) { Option(r.activated).isDefined }
      val arg = ArgumentCaptor.forClass(classOf[UserService.IN])
      verify(m.userService).register(arg.capture, isNull.asInstanceOf[Domain])
      assertResult("abramov.sergei") { arg.getValue.username }
      assertResult("sector@coldcore.com") { arg.getValue.email }
    }
  }

  "login" should {
    "fail if no user found" in {
      when(m.userService.getByEmail(anyString)).thenReturn(None)
      intercept[ObjectNotFoundException] { m.service.login(fbUser, null) }
    }

    "login existing user" in {
      val user = new User {
        id = 22; username = "sector"; email = "sector@coldcore.com"; password = "mypass"; activated = Timestamp() }
      when(m.userService.getByEmail("sector@coldcore.com")).thenReturn(Some(user))
      doAnswer(new Answer[Option[User]] {
        override def answer(invocation: InvocationOnMock): Option[User] = Some(user)
      }).when(m.userService).login("sector", "mypass", null)

      val r = m.service.login(fbUser, null)
      assertResult(22) { r.get.id }
    }
  }

}
