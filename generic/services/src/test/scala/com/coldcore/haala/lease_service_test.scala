package com.coldcore.haala
package service

import domain.AbyssEntry
import core.{Meta, Timestamp}
import LeaseService._
import SolrService.{RangeTerm, ValueTerm}
import org.scalatest.{BeforeAndAfter, FlatSpec, Matchers}
import org.scalatest.mockito.MockitoSugar
import org.scalatest.junit.JUnitRunner
import org.mockito.Mockito._
import org.mockito.ArgumentMatchers.{eq => meq, _}
import org.mockito.ArgumentMatchers._
import org.mockito.ArgumentCaptor
import org.junit.runner.RunWith

@RunWith(classOf[JUnitRunner])
class LeaseServiceSpec extends FlatSpec with MockitoSugar with BeforeAndAfter with Matchers {
  var m: MyMock = _

  before {
    m = new MyMock
  }

  class MyMock extends com.coldcore.haala.service.test.MyMock {
    val service = new LeaseServiceImpl { serviceContainer = m.serviceContainer }
  }

  "create" should "create new lease entry" in {
    doAnswer { invocation => // create lease abyss
      val x = invocation.getArguments.head.asInstanceOf[AbyssService.CreateIN]
      new AbyssEntry { id = 4L; value = x.vm.value; meta = x.vm.meta }
    }.when(m.abyssService).create(any[AbyssService.CreateIN])

    val r = m.service.create(CreateIN("sample text", "prop=a", expires = EXPIRE_24h))

    val meta = Meta(r.abyss.meta)
    (r.abyss.id, r.abyss.value) shouldBe (4, "sample text")
    (meta("prop"), meta("s_typ"), meta("l_ttl")) shouldBe ("a", "les", "86400000")
  }

  "extend" should "extend existing lease" in {
    val abyss = new AbyssEntry { id = 4L; meta = "s_typ=les;r_exp=20160318225645;l_ttl=86400000;" }
    when(m.abyssService.getById(4)).thenReturn(Some(abyss))

    m.service.extend(4)

    // abyss keep update
    val ts = Timestamp().addSeconds(60*60*24).toLong +- 1000
    val keepArg = ArgumentCaptor.forClass(classOf[Long])
    verify(m.abyssService).extend(meq(4L), keepArg.capture)
    keepArg.getValue shouldBe ts

    // expire field update
    val metaArg = ArgumentCaptor.forClass(classOf[String])
    verify(m.abyssService).update(meq(4L), metaArg.capture)
    Meta(metaArg.getValue)("r_exp").toLong shouldBe ts
  }

  "search" should "find non expired lease by pack and extra field" in {
    doAnswer { invocation =>
      val arg = invocation.getArguments.head.asInstanceOf[AbyssService.SearchInputX]
      val now = Timestamp().toLong +- 1000

      arg.pack shouldBe Some("mypack")
      arg.dynamicTerms.size shouldBe 3
      arg.dynamicTerms.head shouldBe ValueTerm("s_typ", "les")
      arg.dynamicTerms(1).asInstanceOf[RangeTerm].from.toString.safeLong shouldBe now
      arg.dynamicTerms.last shouldBe ValueTerm("s_extra", "foo")

      val abyss = new AbyssEntry { id = 4L; value = "sample text"; meta = "s_typ=les;r_exp=20160318225645;l_ttl=86400000;s_extra=foo;" }
      SearchResult(1, abyss :: Nil)
    }.when(m.abyssService).search(any[AbyssService.SearchInputX])

    val r = m.service.search(new SearchInputX(0, 1) {
      override val pack = Some("mypack")
      override val dynamicTerms = ValueTerm("s_extra", "foo") :: Nil
    })

    (r.total, r.objects.head.abyss.id) shouldBe (1, 4)
  }

}
