package com.coldcore.haala
package service

import core.SiteChainMap
import core.SiteChain.Implicits._
import core.VirtualPath.Implicits._
import org.scalatest.{BeforeAndAfter, FlatSpec, Matchers}
import service.exceptions.ObjectNotFoundException
import service.test._
import org.scalatest.mockito.MockitoSugar
import org.mockito.Mockito._
import org.mockito.ArgumentMatchers._
import org.mockito.ArgumentCaptor
import org.mockito.stubbing.Answer
import org.mockito.invocation.InvocationOnMock
import domain.{Domain, File, Label}
import org.junit.runner.RunWith
import org.scalatest.junit.JUnitRunner

@RunWith(classOf[JUnitRunner])
class CmsServiceSpec extends FlatSpec with MockitoSugar with BeforeAndAfter with Matchers {
  var m: MyMock = _

  before {
    m = new MyMock
  }

  class MyMock extends com.coldcore.haala.service.test.MyMock {
    val service = new CmsServiceImpl |< { x =>
      x.serviceContainer = serviceContainer
    }
  }

  private def setupMocksForLabelTest(ls: LabelService) {
    when(ls.query("myKey")).thenReturn(
      new Label { id = 1L; key = "myKey"; value = "val-mh1"; site = "mh1" } ::
        new Label { id = 2L; key = "myKey"; value = "val-mh2"; site = "mh2" } ::
        new Label { id = 3L; key = "myKey"; value = "val-xx1"; site = "xx1" } ::
        new Label { id = 4L; key = "myKey"; value = "val-xx2"; site = "xx2" } ::
        new Label { id = 5L; key = "myKey"; value = "val-mh1j"; site = "mh1j" } ::
        new Label { id = 6L; key = "myKey"; value = "val-mh2j"; site = "mh2j" } :: Nil)
    when(ls.query("myKey", "mh1")).thenReturn("val-mh1")
    when(ls.query("myKey", "mh2")).thenReturn("val-mh2")
    when(ls.query("myKey", "mh1j")).thenReturn("val-mh1j")
    when(ls.query("myKey", "mh2j")).thenReturn("val-mh2j")
    when(ls.query("myKey", "xx1")).thenReturn("val-xx1") //parent of mh1
    when(ls.query("myKey", "xx1.mh1.xx2")).thenReturn("val-xx2") //parent of mh2
    when(ls.query("myKey", "xx1.mh1")).thenReturn("val-mh1") //full site of mh1
    when(ls.query("myKey", "xx1.mh1.xx2.mh2")).thenReturn("val-mh2") //full site of mh2
    when(ls.query("myKey", "xx1j.mh1j")).thenReturn("val-mh1j") //full site of mh1j
    when(ls.query("myKey", "xx1j.mh1j.xx2j.mh2j")).thenReturn("val-mh2j") //full site of mh2j
  }

  "updateLabel" should "not update non-existent label" in {
    setupMocksForLabelTest(m.labelService)
    when(m.labelService.query("unknown")).thenReturn(Nil)
    intercept[ObjectNotFoundException] { m.service.updateLabel(SiteableText("mh2", Map('unknown -> "val")) :: Nil) }
  }

  "updateLabel" should "update child values" in {
    setupMocksForLabelTest(m.labelService)
    val siteChainMap =
      SiteChainMap(Map.empty, Map("mh1" -> "xx1.mh1", "mh2" -> "xx1.mh1.xx2.mh2"), Map.empty, Map.empty)
    //update values
    m.service.updateLabel(
      SiteableText("mh2", Map('myKey -> "updated-mh2")) ::
      SiteableText("mh1", Map('myKey -> "updated-mh1")) :: Nil,
      siteChainMap)
    verify(m.labelService).write("myKey", "updated-mh1", "mh1")
    verify(m.labelService).write("myKey", "updated-mh2", "mh2")
    verify(m.labelService, times(0)).delete(anyString, anyString)
  }

  "updateLabel" should "update and save empty child value" in {
    setupMocksForLabelTest(m.labelService)
    val siteChainMap =
      SiteChainMap(Map.empty, Map("mh1" -> "xx1.mh1", "mh2" -> "xx1.mh1.xx2.mh2"), Map.empty, Map.empty)
    //one value erased, another value updated
    m.service.updateLabel(
      SiteableText("mh2", Map('myKey -> "")) ::
      SiteableText("mh1", Map('myKey -> "updated-mh1")) :: Nil,
      siteChainMap)
    verify(m.labelService).write("myKey", "updated-mh1", "mh1")
    verify(m.labelService).write("myKey", "", "mh2") //delete label
    verify(m.labelService, times(0)).delete(anyString, anyString)
  }

  "updateLabel" should "erase child labels if they match parent" in {
    setupMocksForLabelTest(m.labelService)
    val siteChainMap =
      SiteChainMap(Map.empty, Map("mh1" -> "xx1.mh1", "mh2" -> "xx1.mh1.xx2.mh2"), Map.empty, Map.empty)
    //both values = parent values
    m.service.updateLabel(
      SiteableText("mh2", Map('myKey -> "val-xx2")) ::
      SiteableText("mh1", Map('myKey -> "val-xx1")) :: Nil,
      siteChainMap)
    verify(m.labelService).delete("myKey", "mh1")
    verify(m.labelService).delete("myKey", "mh2")
  }

  "updateLabel" should "erase child label if it matches direct parent (fake chain)" in {
    setupMocksForLabelTest(m.labelService)
    val siteChainMap =
      SiteChainMap(Map.empty, Map("mh1" -> "xx1.mh1", "mh2" -> "xx1.mh1.mh2"), Map.empty, Map.empty)
    when(m.labelService.query("myKey", "xx1")).thenReturn("val-xx1") //parent of mh1
    when(m.labelService.query("myKey", "xx1.mh1")).thenReturn("same-val") //parent of mh2
    //same value (mh1 is direct parent of mh2, fake chain to test direct child erase)
    m.service.updateLabel(
      SiteableText("mh2", Map('myKey -> "same-val")) ::
      SiteableText("mh1", Map('myKey -> "same-val")) :: Nil,
      siteChainMap)
    verify(m.labelService, times(0)).delete("myKey", "mh1")
    verify(m.labelService).delete("myKey", "mh2")
  }

  "updateLabel" should "erase child labels if they match direct parents (fake chain)" in {
    setupMocksForLabelTest(m.labelService)
    val siteChainMap =
      SiteChainMap(Map.empty, Map("mh1" -> "xx1.mh1", "mh2" -> "xx1.mh1.mh2"), Map.empty, Map.empty)
    when(m.labelService.query("myKey", "xx1")).thenReturn("same-val") //parent of mh1
    when(m.labelService.query("myKey", "xx1.mh1")).thenReturn("same-val") //parent of mh2
    //same value equal parent (mh1 is direct parent of mh2, fake chain to test direct child erase)
    m.service.updateLabel(
      SiteableText("mh2", Map('myKey -> "same-val")) ::
      SiteableText("mh1", Map('myKey -> "same-val")) :: Nil,
      siteChainMap)
    verify(m.labelService).delete("myKey", "mh1")
    verify(m.labelService).delete("myKey", "mh2")
  }

  "updateLabel" should "update values if they do not match parents (incorrect)" in {
    setupMocksForLabelTest(m.labelService)
    val siteChainMap =
      SiteChainMap(Map.empty, Map("mh1" -> "xx1.mh1", "mh2" -> "xx1.mh1.xx2.mh2"), Map.empty, Map.empty)
    //same values (this in fact is incorrect behaviour, mh2 is not erased because its direct parent is xx2 rather than mh1)
    m.service.updateLabel(
      SiteableText("mh2", Map('myKey -> "same-val")) ::
      SiteableText("mh1", Map('myKey -> "same-val")) :: Nil,
      siteChainMap)
    verify(m.labelService).write("myKey", "same-val", "mh1")
    verify(m.labelService).write("myKey", "same-val", "mh2")
    verify(m.labelService, times(0)).delete(anyString, anyString)
  }

  "loadLabel" should "load values of existing label" in {
    val siteChainMap =
      SiteChainMap(Map.empty, Map("mh1" -> "xx1.mh1", "mh2" -> "xx1.mh1.xx2.mh2"),
                              Map("mh1j" -> "xx1j.mh1j", "mh2j" -> "xx1j.mh1j.xx2j.mh2j",
                                  "xx1j" -> "xx1j.mh1j", "xx2j" -> "xx1j.mh1j.xx2j.mh2j"), Map.empty)
    setupMocksForLabelTest(m.labelService)
    intercept[ObjectNotFoundException] { m.service.loadLabel("unknown", "", siteChainMap) }
    m.service.loadLabel("myKey", "", siteChainMap) shouldBe Map("mh1" -> "val-mh1", "mh2" -> "val-mh2")
    m.service.loadLabel("myKey", "j", siteChainMap) shouldBe Map("mh1j" -> "val-mh1j", "mh2j" -> "val-mh2j")
  }

  "incVerion" should "increase resource version" in {
    when(m.settingService.query("ResourceVersion", ".mh1")).thenReturn("24")
    m.service.incVerion("mh1")
    verify(m.settingService).write("ResourceVersion", "25", "mh1")
  }

  "incVerion" should "start from one if over the limit" in {
    when(m.settingService.query("ResourceVersion", ".mh1")).thenReturn("10000")
    m.service.incVerion("mh1")
    verify(m.settingService).write("ResourceVersion", "1", "mh1")
  }

  "updateFile" should "not update non-existent file" in {
    val temp = new ByteBody("qwertyuiopa".getBytes) { xSaved = true; xPath = "/tmp/bar.tmp" }
    when(m.fileService.getByPath("bar/unknown")).thenReturn(Nil)
    intercept[ObjectNotFoundException] { m.service.updateFile("bar/unknown", "mh1", temp, null, false) }
  }

  "updateFile" should "update file body" in {
    val siteChainMap = SiteChainMap(Map.empty, Map("mh1" -> "xx1.mh1"), Map.empty, Map.empty)
    val nbody = new ByteBody("qwertyuiopa".getBytes) { xSaved = true; xPath = "/tmp/bar.tmp" } //new file and body
    val nfile = new File { id = 11L; folder = "bar"; name = "foo.png"; site = "mh1" }
    val pbody = new ByteBody("12345678901".getBytes) { xSaved = true; xPath = "/fs/a.d" } //parent file and body
    val pfile = new File { id = 10L; folder = "bar"; name = "foo.png"; site = "xx1" }

    when(m.fileService.getByPath("bar/foo.png")).thenReturn(pfile :: Nil)
    when(m.fileService.getByPath("bar/foo.png", ".mh1")).thenReturn(None) //no new file created yet
    doAnswer(new Answer[File] { //new file was created
      override def answer(invocation: InvocationOnMock): File = {
        when(m.fileService.getByPath("bar/foo.png", ".mh1")).thenReturn(Some(nfile))
        when(m.fileService.getBody(nfile)).thenReturn(nbody)
        nfile
      }
    }).when(m.fileService).create("bar/foo.png", "mh1", nbody)
    when(m.fileService.getByPath("bar/foo.png", "xx1")).thenReturn(Some(pfile))
    when(m.fileService.getBody(pfile)).thenReturn(pbody)

    //update body
    m.service.updateFile("bar/foo.png", "mh1", nbody, siteChainMap, false)
    verify(m.fileService).create("bar/foo.png", "mh1", nbody)
    verify(m.fileService, times(0)).delete(anyLong)
  }

  "updateFile" should "erase file if it matches parent" in {
    val siteChainMap = SiteChainMap(Map.empty, Map("mh1" -> "xx1.mh1"), Map.empty, Map.empty)
    val nbody = new ByteBody("12345678901".getBytes) { xSaved = true; xPath = "/tmp/bar.tmp" } //new file and body
    val nfile = new File { id = 11L; folder = "bar"; name = "foo.png"; site = "mh1" }
    val pbody = new ByteBody("12345678901".getBytes) { xSaved = true; xPath = "/fs/a.d" } //parent file and body
    val pfile = new File { id = 10L; folder = "bar"; name = "foo.png"; site = "xx1" }

    when(m.fileService.getByPath("bar/foo.png")).thenReturn(pfile :: Nil)
    when(m.fileService.getByPath("bar/foo.png", ".mh1")).thenReturn(Some(nfile)) //new file already exists
    when(m.fileService.getBody(nfile)).thenReturn(nbody)
    when(m.fileService.getByPath("bar/foo.png", "xx1")).thenReturn(Some(pfile))
    when(m.fileService.getBody(pfile)).thenReturn(pbody)

    //equal parent body
    m.service.updateFile("bar/foo.png", "mh1", nbody, siteChainMap, false)
    verify(m.fileService).delete(11)
  }

  "loadFile" should "load file head of existing file" in {
    val siteChainMap = SiteChainMap(Map.empty, Map("mh1" -> "xx1.mh1"), Map.empty, Map.empty)
    val file = new File { id = 11L; folder = "bar"; name = "foo.png"; site = "mh1" }

    when(m.fileService.getByPath("bar/foo.png", "xx1.mh1")).thenReturn(Some(file))
    m.service.loadFile("bar/foo.png", siteChainMap) shouldBe Map("mh1" -> file)

    when(m.fileService.getByPath("bar/unknown", "xx1.mh1")).thenReturn(None)
    intercept[ObjectNotFoundException] { m.service.loadFile("bar/unknown", siteChainMap) }
  }

  "updateDomain" should "update existing domain" in {
    val dinfo = SiteableText("",
      Map('keywords -> "keywords val", 'insideHead -> "insideHead val", 'beforeBody -> "beforeBody val"))
    val info = SiteableText("agy1", Map('title -> "title agy1")) :: SiteableText("agy2", Map('title -> "title agy2")) :: Nil
    when(m.domainService.defaultSite("myagency.com")).thenReturn("agy1")
    when(m.labelService.query("meta.key", "agy1")).thenReturn("")
    when(m.labelService.query("site.ihead", "agy1")).thenReturn("")
    when(m.labelService.query("site.bbody", "agy1")).thenReturn("")
    when(m.labelService.query("site.ttl", "agy1")).thenReturn("")
    when(m.labelService.query("site.ttl", "agy2")).thenReturn("")

    when(m.domainService.getById(1001)).thenReturn(Some(new Domain {
      id = 1001L; domain = "myagency.com"; domainSite = "pp.agy"; meta = "resolve=site,currency"; `type` = "agency" }))
    m.service.updateDomain(1001, dinfo, info)
    verify(m.labelService).write("meta.key", "keywords val", "agy1")
    verify(m.labelService).write("site.ihead", "insideHead val", "agy1")
    verify(m.labelService).write("site.bbody", "beforeBody val", "agy1")
    verify(m.labelService).write("site.ttl", "title agy1", "agy1")
    verify(m.labelService).write("site.ttl", "title agy2", "agy2")

    when(m.domainService.getById(0)).thenReturn(None)
    intercept[ObjectNotFoundException] { m.service.updateDomain(0, dinfo, info) }
  }

  "updatePageLabels" should "update labels of existing page" in {
    var pageIndex = "00company"
    val dinfo = SiteableText("",
      Map('keywords -> "keywords val", 'description -> "description val"))
    val title = SiteableText("agy1", Map('title -> "title agy1")) :: SiteableText("agy2", Map('title -> "title agy2")) :: Nil
    when(m.domainService.defaultSite("myagency.com")).thenReturn("agy1")
    when(m.labelService.query("met.k00company", "agy1")).thenReturn("")
    when(m.labelService.query("met.d00company", "agy1")).thenReturn("")
    when(m.labelService.query("ttl.p00company", "agy1")).thenReturn("")
    when(m.labelService.query("ttl.p00company", "agy2")).thenReturn("")

    when(m.domainService.getById(1001)).thenReturn(Some(new Domain {
      id = 1001L; domain = "myagency.com"; domainSite = "pp.agy"; meta = "resolve=site,currency"; `type` = "agency" }))
    m.service.updatePageLabels(1001, pageIndex, dinfo, title)
    verify(m.labelService).write("met.k00company", "keywords val", "agy1")
    verify(m.labelService).write("met.d00company", "description val", "agy1")
    verify(m.labelService).write("ttl.p00company", "title agy1", "agy1")
    verify(m.labelService).write("ttl.p00company", "title agy2", "agy2")

    when(m.domainService.getById(0)).thenReturn(None)
    intercept[ObjectNotFoundException] { m.service.updateDomain(0, dinfo, title) }
  }

}