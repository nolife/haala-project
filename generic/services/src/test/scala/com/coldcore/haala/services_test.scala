package com.coldcore.haala
package service

import domain.{Label, Setting}
import scala.collection.JavaConverters._
import service.test.MyMock
import org.mockito.Mockito._
import org.mockito.ArgumentMatchers._
import dao.GenericDAO
import org.junit.runner.RunWith
import org.scalatest.junit.JUnitRunner
import org.scalatest._
import org.scalatest.mockito.MockitoSugar
import core.SiteChain.Implicits._
import core.VirtualPath.Implicits._

@RunWith(classOf[JUnitRunner])
class BaseServiceSpec extends FlatSpec with MockitoSugar with BeforeAndAfter with Matchers {
  var m: MyMock = _

  before {
    m = new MyMock
  }

  class MyMock extends com.coldcore.haala.service.test.MyMock {
    val settingDao = mock[GenericDAO[Setting]]
    val service = new BaseService |< { x =>
      x.serviceContainer = serviceContainer
    }
  }

  "constructWhereSite" should "construct where clause" in {
    m.service.constructWhereSite(".mw1j", "o.") shouldBe "(o.site = 'mw1j')"
    m.service.constructWhereSite(".mw1j", "x.") shouldBe "(x.site = 'mw1j')"
    m.service.constructWhereSite("xx1.mw1", "o.") shouldBe "(o.site = 'pub' or o.site = 'xx1' or o.site = 'mw1')"
  }

  "filterObjectsForSite" should "filter objects based on site" in {
    val (xx1_label, xx2_label, mh1_label, mh2_label, mh1_label_2, mh2_label_2,
         pub_label, sys_label, ms1_label, ms2_label) =
      (new Label{site = "xx1"; key = "my-key"; value = "value xx1"},
       new Label{site = "xx2"; key = "my-key"; value = "value xx2"},
       new Label{site = "mh1"; key = "my-key"; value = "value mh1"},
       new Label{site = "mh2"; key = "my-key"; value = "value mh2"},
       new Label{site = "mh1"; key = "my-key-2"; value = "value 2 mh1"},
       new Label{site = "mh2"; key = "my-key-2"; value = "value 2 mh2"},
       new Label{site = "pub"; key = "my-key"; value = "value pub"},
       new Label{site = "sys"; key = "my-key"; value = "value sys"},
       new Label{site = "ms1"; key = "my-key"; value = "value ms1"},
       new Label{site = "ms2"; key = "my-key"; value = "value ms2"})
    val fullList = xx1_label :: xx2_label :: mh1_label :: mh2_label ::
            pub_label :: sys_label :: ms1_label :: ms2_label :: Nil
    val engList = pub_label :: xx1_label :: mh1_label :: ms1_label :: Nil
    val engList2x = pub_label :: xx1_label :: mh1_label :: ms1_label :: mh1_label_2 :: Nil
    val engListNoPub = xx1_label :: mh1_label :: ms1_label :: Nil
    val fullList2x = xx1_label :: xx2_label :: mh1_label :: mh2_label :: mh1_label_2 :: mh2_label_2 ::
            pub_label :: sys_label :: ms1_label :: ms2_label :: Nil
    val srv = m.service

    //empty list test - single object
    assertResult(None) { srv.filterObjectForSite(Nil, "pub") }
    assertResult(None) { srv.filterObjectForSite(Nil, ".xx2") }
    assertResult(None) { srv.filterObjectForSite(Nil, "mh1.mr1.mh2.mr2") }
    //full list test - single object
    assertResult(Some(pub_label)) { srv.filterObjectForSite(fullList, "pub") }
    assertResult(Some(sys_label)) { srv.filterObjectForSite(fullList, "sys") }
    assertResult(Some(xx1_label)) { srv.filterObjectForSite(fullList, "xx1") }
    assertResult(Some(mh2_label)) { srv.filterObjectForSite(fullList, "mh2") }
    assertResult(Some(pub_label)) { srv.filterObjectForSite(fullList, "mr1") }
    assertResult(None) { srv.filterObjectForSite(fullList, ".mr1") }
    assertResult(Some(mh2_label)) { srv.filterObjectForSite(fullList, "mh1.mr1.mh2.mr2") }
    assertResult(Some(mh2_label)) { srv.filterObjectForSite(fullList, ".mh1.mh2") }
    assertResult(None) { srv.filterObjectForSite(fullList, ".mr1.mr2") }
    assertResult(Some(pub_label)) { srv.filterObjectForSite(fullList, "pub.mr1.mr2") }
    assertResult(Some(pub_label)) { srv.filterObjectForSite(fullList, "mr1.mr2") }
    //english list test - single object
    assertResult(Some(mh1_label)) { srv.filterObjectForSite(engList, "xx1.mh1.xx2.mh2") }
    assertResult(Some(xx1_label)) { srv.filterObjectForSite(engList, "xx1.mr1.xx2.mr2") }
    assertResult(Some(xx1_label)) { srv.filterObjectForSite(engList, "xx1.xx2") }
    assertResult(Some(pub_label)) { srv.filterObjectForSite(engList, "xx2") }
    assertResult(Some(xx1_label)) { srv.filterObjectForSite(engListNoPub, "xx1.xx2") }
    assertResult(None) { srv.filterObjectForSite(engListNoPub, "xx2") }
    //full list test - multiple objects
    assertResult(pub_label :: Nil) { srv.filterObjectsForSite(fullList2x, "pub") }
    assertResult(sys_label :: Nil) { srv.filterObjectsForSite(fullList2x, "sys") }
    assertResult(xx1_label :: Nil) { srv.filterObjectsForSite(fullList2x, "xx1") }
    assertResult(Set(mh2_label, mh2_label_2)) { srv.filterObjectsForSite(fullList2x, "mh2").toSet }
    assertResult(pub_label :: Nil) { srv.filterObjectsForSite(fullList2x, "mr1") }
    assertResult(Nil) { srv.filterObjectsForSite(fullList2x, ".mr1") }
    assertResult(Set(mh2_label, mh2_label_2)) { srv.filterObjectsForSite(fullList2x, "mh1.mr1.mh2.mr2").toSet }
    //english list test - multiple objects
    assertResult(Set(mh1_label, mh1_label_2)) { srv.filterObjectsForSite(engList2x, "xx1.mh1.xx2.mh2").toSet }
    assertResult(xx1_label :: Nil) { srv.filterObjectsForSite(engList2x, "xx1.mr1.xx2.mr2") }
    assertResult(pub_label :: Nil) { srv.filterObjectsForSite(engList2x, "xx2") }
    assertResult(Nil) { srv.filterObjectsForSite(engList2x, ".xx2") }
  }

  "constructTotalQuery" should "construct query with total count" in {
    val nospace = (x: String) => x.split(" ").filter("" !=).mkString(" ")
    assertResult("select count(*) from User") { m.service.constructTotalQuery("from User") }
    assertResult("select count(*) from User o left join o.teams as t where t.weekend.id = ?") {
      nospace(m.service.constructTotalQuery("from User o left join o.teams as t where t.weekend.id = ? order by o.id desc, t.weekend.id"))
    }
    assertResult("select count(*) from User o left join o.teams as t where t.weekend.id = ?") {
      nospace(m.service.constructTotalQuery("select o.id from User o left join o.teams as t where t.weekend.id = ?"))
    }
  }

  "searchObjects" should "fild all objects" in {
    when(m.settingDao.findCount(anyString, any)).thenReturn(107)
    var nlist = (1 to 9).map(x => new Setting { id = x })
    var dlist = (1 to 9*2).map(x => new Setting { id = x/2+1 })
    when(m.settingDao.iterate("from s1.Setting where key = ?", "s1-key")).thenReturn(nlist.iterator)
    when(m.settingDao.iterate("from s2.Setting where key = ?", "s2-key")).thenReturn(dlist.iterator)

    val result = m.service.searchObjects(2, 5, m.settingDao, "from s1.Setting where key = ?", "s1-key")
    val duplicate = m.service.searchObjects(4, 3, m.settingDao, "from s2.Setting where key = ?", "s2-key")
    assertResult(107) { result.total }
    assertResult(5) { result.objects.size }
    assertResult(3) { result.objects.head.id }
    assertResult(7) { result.objects.last.id }
    assertResult(107) { duplicate.total }
    assertResult(3) { duplicate.objects.size }
    assertResult(5) { duplicate.objects.head.id }
    assertResult(7) { duplicate.objects.last.id }
  }

  "searchObjectsWithFilter" should "find objects based on filter criteria" in {
    val f = (o: Setting) => o.id % 2 != 0; val fdup = (o: Setting) => true
    var nlist = (1 to 16).map(x => new Setting { id = x })
    var dlist = (1 to 16*2).map(x => new Setting { id = x/2+1 }) // 1,2,2,3,3...16,16,17
    when(m.settingDao.iterate("from s1.Setting where key = ?", "s1-key")).thenReturn(nlist.iterator)
    when(m.settingDao.iterate("from s2.Setting where key = ?", "s2-key")).thenReturn(dlist.iterator)

    val result = m.service.searchObjectsWithFilter(2, 5, m.settingDao, "from s1.Setting where key = ?", f, "s1-key")
    val duplicate = m.service.searchObjectsWithFilter(4, 3, m.settingDao, "from s2.Setting where key = ?", fdup, "s2-key")
    assertResult(8) { result.total }
    assertResult(5) { result.objects.size }
    assertResult(5) { result.objects.head.id }
    assertResult(13) { result.objects.last.id }
    assertResult(17) { duplicate.total }
    assertResult(3) { duplicate.objects.size }
    assertResult(5) { duplicate.objects.head.id }
    assertResult(7) { duplicate.objects.last.id }
  }

  "expandOrShrink" should "expand or shrink array with entity objects" in {
    val x = new JArrayList[Label]
    var counter = 0L
    val doF = () => { counter += 1; new Label { id = counter }}

    m.service.expandOrShrink(x, 3, doF) //expand
    assertResult(List(1,2,3)) { x.asScala.map(_.id).toList }
    m.service.expandOrShrink(x, 3, doF) //same size
    assertResult(List(1,2,3)) { x.asScala.map(_.id).toList }
    m.service.expandOrShrink(x, 1, doF) //shrink
    assertResult(List(1)) { x.asScala.map(_.id).toList }
    m.service.expandOrShrink(x, 4, doF) //expand
    assertResult(List(1,4,5,6)) { x.asScala.map(_.id).toList }
  }

  "writeLabels" should "write or remove labels" in {
    m.service.writeLabels((k: String) => "e.MC001."+k,
      SiteableText("xx1", Map('Update -> "updated", 'Delete -> "")),
      SiteableText("xx2", Map('Create -> "created")))
    verify(m.labelService).delete("e.MC001.Delete", "xx1")
    verify(m.labelService).write("e.MC001.Update", "updated", "xx1")
    verify(m.labelService).write("e.MC001.Create", "created", "xx2")
  }

  "cacheAwareResults" should "lookup results in cache or load those into cache" in {
    val cache = new GuavaDomainCache[Label] |< { x => x.serviceContainer = m.serviceContainer; x.afterPropertiesSet }
    val (o1, o2, o3, o4) = (new Label { id = 1L }, new Label { id = 2L }, new Label { id = 3L }, new Label { id = 4L })
    assertResult(List(1, 2)) { m.service.cacheAwareResults[Label](cache, "myKey1", { List(o1, o2) }).map(_.id).toList }
    assertResult(List(1, 2)) { m.service.cacheAwareResults[Label](cache, "myKey1", { List(o3, o4) }).map(_.id).toList }
    assertResult(List(3, 4)) { m.service.cacheAwareResults[Label](cache, "myKey2", { List(o3, o4) }).map(_.id).toList }
    assertResult(List(3, 4)) { m.service.cacheAwareResults[Label](cache, "myKey2", { throw new Exception }).map(_.id).toList }
  }
}

@RunWith(classOf[JUnitRunner])
class GuavaCacheTest extends WordSpec with MockitoSugar with BeforeAndAfter {
  var m: MyMock = _

  before {
    m = new MyMock
  }

  "cache" should {
    "do basic operations" in {
      val cache = new GuavaCache[Any] |< { x => x.serviceContainer = m.serviceContainer; x.afterPropertiesSet }
      assertResult(0) ( cache.count )
      assertResult(None) { cache.get("key-1") }
      assertResult(None) { cache.get("key-2") }
      cache.set("key-1", "val-1")
      assertResult(1) ( cache.count )
      assertResult("val-1") { cache.get("key-1").get }
      assertResult(None) { cache.get("key-2") }
      assertResult(1) ( cache.clear )
      assertResult(0) ( cache.count )
      assertResult(None) { cache.get("key-1") }
      assertResult(None) { cache.get("key-2") }
      cache.set("key-1", "val-1")
      assertResult("val-1") { cache.get("key-1").get }
      cache.remove("key-1")
      assertResult(0) ( cache.count )
      assertResult(None) { cache.get("key-1") }
    }

    "not exceed items limit" in {
      val cache = new GuavaCache[Any] |< { x => x.serviceContainer = m.serviceContainer; x.afterPropertiesSet }
      assertResult(0) ( cache.count )
      assertResult(None) { cache.get("key-1") }
      assertResult(None) { cache.get("key-2") }
      cache.set("key-1", "val-1")
      assertResult(1) ( cache.count )
      assertResult("val-1") { cache.get("key-1").get }
      assertResult(None) { cache.get("key-2") }
      assertResult(1) ( cache.clear )
      assertResult(0) ( cache.count )
      assertResult(None) { cache.get("key-1") }
      assertResult(None) { cache.get("key-2") }
      cache.set("key-1", "val-1")
      assertResult("val-1") { cache.get("key-1").get }
      cache.remove("key-1")
      assertResult(0) ( cache.count )
      assertResult(None) { cache.get("key-1") }
    }
  }
}

@RunWith(classOf[JUnitRunner])
class GuavaDomainCacheSpec extends WordSpec with MockitoSugar with BeforeAndAfter {
  var m: MyMock = _

  before {
    m = new MyMock
  }

  "cache" should {
    "store objects and remove by ID" in {
      val (setting1, setting2, setting3, setting4) = (new Setting { id = 1L }, new Setting { id = 2L },
                                                      new Setting { id = 3L }, new Setting { id = 4L })
      val cache = new GuavaDomainCache[Setting] |< { x => x.serviceContainer = m.serviceContainer; x.afterPropertiesSet }
      cache.entityClass = classOf[Setting]
      assertResult(0) ( cache.count )
      cache.set("key-1", setting1 :: setting2 :: Nil)
      cache.set("key-3", setting3 :: Nil)
      cache.set("key-4", setting4 :: Nil)
      assertResult(3) ( cache.count )
      assertResult(setting1 :: setting2 :: Nil) { cache.get("key-1").get }
      assertResult(setting3 :: Nil) { cache.get("key-3").get }

      cache.remove(2L, classOf[Setting].getName)
      assertResult(None) { cache.get("key-1") }
      assertResult(2) ( cache.count )

      cache.remove(3L)
      assertResult(None) { cache.get("key-3") }
      assertResult(1) ( cache.count )

      cache.remove(new Setting |< { _.id = 4L })
      assertResult(None) { cache.get("key-4") }
      assertResult(0) ( cache.count )
    }

    "remove only supported objects by ID" in {
      val (setting1, setting2) = (new Setting { id = 1L }, new Setting { id = 2L })
      val cache = new GuavaDomainCache[Setting] |< { x => x.serviceContainer = m.serviceContainer; x.afterPropertiesSet }
      cache.entityClass = classOf[Setting]
      assertResult(0) ( cache.count )
      cache.set("key-1", setting1 :: setting2 :: Nil)
      assertResult(1) ( cache.count )

      cache.remove(2L, classOf[Label].getName)
      assertResult(1) ( cache.count )
      cache.remove(new Label |< { _.id = 2L })
      assertResult(1) ( cache.count )
      cache.remove(new Setting |< { _.id = 2L })
      assertResult(0) ( cache.count )
    }
  }
}

@RunWith(classOf[JUnitRunner])
class ServiceContainerSpec extends FlatSpec with MockitoSugar with BeforeAndAfter with Matchers {
  var m: MyMock = _

  before {
    m = new MyMock
  }

  it should "get requited service" in {
    val sc = new ServiceContainer
    val (ss, ls) = (new SettingServiceImpl, new LabelServiceImpl)
    sc.services = ss :: ls :: Nil
    assertResult(ls) { sc.get[LabelService] }
    assertResult(ss) { sc.get[SettingService] }
  }

  it should "get service in correct order" in {
    val sc = new ServiceContainer
    class DummyService { var dummy: Int = _ }
    val (d1, d2) = (new DummyService { dummy = 1 }, new DummyService { dummy = 2 })
    sc.services = d1 :: d2 :: Nil
    assertResult(1) { sc.get[DummyService].dummy }
    sc.services = d2 :: d1 :: Nil
    assertResult(2) { sc.get[DummyService].dummy }
  }

  it should "get extended service in correct order" in {
    val sc = new ServiceContainer
    class DummyService { var dummy: Int = _ }
    class ExtDummyService extends DummyService
    val (d1, d2, e3) = (new DummyService { dummy = 1 }, new DummyService { dummy = 2 },new ExtDummyService { dummy = 3 })
    sc.services = d1 :: d2 :: e3 :: Nil
    assertResult(1) { sc.get[DummyService].dummy }
    sc.services = d1 :: d2 :: e3 :: Nil
    assertResult(3) { sc.get[ExtDummyService].dummy }
    sc.services = e3 :: d1 :: d2 :: Nil
    assertResult(3) { sc.get[DummyService].dummy }
    sc.services = e3 :: d1 :: d2 :: Nil
    assertResult(3) { sc.get[ExtDummyService].dummy }
  }

}
