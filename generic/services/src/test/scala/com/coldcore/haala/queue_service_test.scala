package com.coldcore.haala
package service

import org.junit.runner.RunWith
import org.scalatest.{BeforeAndAfter, FlatSpec, Matchers}
import org.scalatest.junit.JUnitRunner
import org.scalatest.mockito.MockitoSugar
import service.test.MyMock

@RunWith(classOf[JUnitRunner])
class QueueServiceSpec extends FlatSpec with MockitoSugar with BeforeAndAfter with Matchers {
  var m: MyMock = _

  before {
    m = new MyMock
  }

  it should "push" in {
    val srv = new QueueServiceImpl { queueCache = new GuavaCache[Any] |< { x => x.serviceContainer = m.serviceContainer; x.afterPropertiesSet }}
    srv.push("my-group", "my-value-1")
    assertResult("my-value-1") { srv.pop("my-group").get }
    assertResult(None) { srv.pop("my-group") }
    srv.push("my-group", "my-value-1")
    srv.push("my-group", "my-value-2")
    srv.push("my-group", "my-value-3")
    assertResult("my-value-1") { srv.pop("my-group").get }
    assertResult("my-value-2") { srv.pop("my-group").get }
    assertResult("my-value-3") { srv.pop("my-group").get }
    assertResult(None) { srv.pop("my-group") }
    srv.push("my-group-1", "my-value-1")
    srv.push("my-group-1", "my-value-2")
    srv.push("my-group-2", "my-value-3")
    srv.push("my-group-2", "my-value-4")
    assertResult("my-value-1") { srv.pop("my-group-1").get }
    assertResult("my-value-2") { srv.pop("my-group-1").get }
    assertResult(None) { srv.pop("my-group-1") }
    assertResult("my-value-3") { srv.pop("my-group-2").get }
    assertResult("my-value-4") { srv.pop("my-group-2").get }
    assertResult(None) { srv.pop("my-group-2") }
    assertResult(None) { srv.pop("my-group-3") }
  }

  it should "push non existent" in {
    val srv = new QueueServiceImpl { queueCache = new GuavaCache[Any] |< { x => x.serviceContainer = m.serviceContainer; x.afterPropertiesSet }}
    srv.pushNonExistent("my-group", "my-value-1", "uniq-1")
    assertResult("my-value-1") { srv.pop("my-group").get }
    assertResult(None) { srv.pop("my-group") }
    srv.pushNonExistent("my-group", "my-value-1", "uniq-1")
    srv.pushNonExistent("my-group", "my-value-2", "uniq-1")
    srv.pushNonExistent("my-group", "my-value-3", "uniq-1")
    assertResult("my-value-1") { srv.pop("my-group").get }
    assertResult(None) { srv.pop("my-group") }
    srv.pushNonExistent("my-group", "my-value-1", "uniq-1")
    srv.pushNonExistent("my-group", "my-value-2", "uniq-1")
    srv.pushNonExistent("my-group", "my-value-3", "uniq-2")
    srv.pushNonExistent("my-group", "my-value-4", "uniq-2")
    assertResult("my-value-1") { srv.pop("my-group").get }
    assertResult("my-value-3") { srv.pop("my-group").get }
    assertResult(None) { srv.pop("my-group") }
  }

  it should "push unique" in {
    val srv = new QueueServiceImpl { queueCache = new GuavaCache[Any] |< { x => x.serviceContainer = m.serviceContainer; x.afterPropertiesSet }}
    srv.pushUnique("my-group", "my-value-1", "uniq-1")
    assertResult("my-value-1") { srv.pop("my-group").get }
    assertResult(None) { srv.pop("my-group") }
    srv.pushUnique("my-group", "my-value-1", "uniq-1")
    srv.pushUnique("my-group", "my-value-2", "uniq-1")
    srv.pushUnique("my-group", "my-value-3", "uniq-1")
    assertResult("my-value-3") { srv.pop("my-group").get }
    assertResult(None) { srv.pop("my-group") }
    srv.pushUnique("my-group", "my-value-1", "uniq-1")
    srv.pushUnique("my-group", "my-value-2", "uniq-1")
    srv.pushUnique("my-group", "my-value-3", "uniq-2")
    srv.pushUnique("my-group", "my-value-4", "uniq-2")
    assertResult("my-value-2") { srv.pop("my-group").get }
    assertResult("my-value-4") { srv.pop("my-group").get }
    assertResult(None) { srv.pop("my-group") }
  }

/*
  it should "push unique delayed" in { //commented out for performance
    val srv = new QueueServiceImpl { queueCache = new GuavaCache[Any] |< { x => x.serviceContainer = m.serviceContainer; x.afterPropertiesSet }}
    srv.pushUniqueDelayed("my-group", "my-value-1", "uniq-1", 1000)
    assertResult(None) { srv.pop("my-group") }
    Thread.sleep(1000)
    assertResult("my-value-1") { srv.pop("my-group").get }
  }
*/

}
