package com.coldcore.haala
package service

import org.scalatest.{BeforeAndAfter, FlatSpec, Matchers}
import org.scalatest.mockito.MockitoSugar
import org.mockito.Mockito._
import org.mockito.ArgumentMatchers._
import org.mockito.ArgumentCaptor
import org.springframework.security.core.context.SecurityContext
import org.springframework.security.core.userdetails.UserDetails
import org.springframework.security.core.Authentication
import com.coldcore.haala.domain.{Role, User}
import org.junit.runner.RunWith
import org.scalatest.junit.JUnitRunner

@RunWith(classOf[JUnitRunner])
class SecurityServiceSpec extends FlatSpec with MockitoSugar with BeforeAndAfter with Matchers {
  var m: MyMock = _

  before {
    m = new MyMock
  }

  class MyMock extends com.coldcore.haala.service.test.MyMock {
    val service = new SecurityServiceImpl { override val staticBlock = m.staticBlock; serviceContainer = m.serviceContainer }
    val context = mock[SecurityContext]
  }

  val user = new User { id = 2; username = "name"; password = "pass" }

  /** Push user into mock context and return principal created by service. */
  private def setupPrincipal(roles: String*): UserDetails = {
    when(m.staticBlock.securityContext).thenReturn(m.context)
    m.service.pushPrincipal(user, roles: _*)
    val arg = ArgumentCaptor.forClass(classOf[Authentication])
    verify(m.context).setAuthentication(arg.capture)
    when(m.staticBlock.securityContext.getAuthentication).thenReturn(arg.getValue)
    arg.getValue.getPrincipal.asInstanceOf[UserDetails]
  }

  it should "push principal" in {
    val principal = setupPrincipal(Role.USER, Role.ADMIN)
    assertResult("name") { principal.getUsername }
    assertResult(Set("ROLE_USER", "ROLE_ADMIN")) { principal.getAuthorities.map(_.getAuthority).toSet }
  }

  it should "check if principal missing" in {
    when(m.staticBlock.securityContext).thenReturn(m.context)
    assertResult(true) { m.service.isPrincipalMissing }
    setupPrincipal()
    assertResult(false) { m.service.isPrincipalMissing }
  }

  it should "check if user in role" in {
    val user = new User { id = 2; username = "name"; password = "pass" }
    when(m.staticBlock.securityContext).thenReturn(m.context)
    assertResult(false) { m.service.isUserInRole(Role.ADMIN) }
    setupPrincipal()
    assertResult(false) { m.service.isUserInRole(Role.ADMIN) }
  }

  it should "check if user in admin role" in {
    val user = new User { id = 2; username = "name"; password = "pass" }
    setupPrincipal(Role.ADMIN)
    assertResult(true) { m.service.isUserInRole(Role.ADMIN) }
  }

}
