package com.coldcore.haala
package service

import org.scalatest.{BeforeAndAfter, FlatSpec, Matchers}
import org.scalatest.mockito.MockitoSugar
import org.mockito.Mockito._
import org.mockito.ArgumentMatchers._
import service.SpamService.UpdateStorageIN
import dao.GenericDAO
import domain.EmailStorage
import org.junit.runner.RunWith
import org.scalatest.junit.JUnitRunner

@RunWith(classOf[JUnitRunner])
class SpamServiceSpec extends FlatSpec with MockitoSugar with BeforeAndAfter with Matchers {
  var m: MyMock = _

  before {
    m = new MyMock
  }

  class MyMock extends com.coldcore.haala.service.test.MyMock {
    val emailStorageDao = mock[GenericDAO[EmailStorage]]
    val service = new SpamServiceImpl |< { x =>
      x.serviceContainer = serviceContainer
      x.emailStorageDao = emailStorageDao
    }
  }

  it should "update storage" in {
    val raw1 = "some xyz <foo@bar.cc>\n\"abc\" foo2@bar.cc,FOO3@email.co.uk 'my.name@me.com'; some@me. com ; @me.com"
    val raw2 = "foo@bar.cc FOO2@BAR.CC foo3 @ bar.cc my@ email.com my@bar@bar.cc \"MY@BAR.CC\""
    val storage = new EmailStorage { id = 12L; description = "my storage"; total = 0 }

    when(m.emailStorageDao.findById(12)).thenReturn(Some(storage))
    m.service.updateStorage(12, UpdateStorageIN("foo", raw1))
    assertResult(4) { storage.items.size }
    m.service.updateStorage(12, UpdateStorageIN("foo", raw2))
    assertResult(5) { storage.items.size }
    assertResult(5) { storage.total }
    val assertComtains = (x: String) => assert(storage.items.exists(_.value == x))
    assertComtains("foo@bar.cc")
    assertComtains("foo2@bar.cc")
    assertComtains("foo3@email.co.uk")
    assertComtains("my.name@me.com")
    assertComtains("my@bar.cc")
  }

}
