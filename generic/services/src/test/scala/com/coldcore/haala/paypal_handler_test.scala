package com.coldcore.haala
package service

import org.mockito.Mockito._
import org.mockito.ArgumentMatchers
import org.mockito.ArgumentMatchers._
import org.scalatest.mockito.MockitoSugar
import org.scalatest._
import org.scalatest.junit.JUnitRunner
import org.junit.runner.RunWith
import domain.AbyssEntry
import PaymentService.PaymentEntry
import PaypalHandler._
import com.google.gson.Gson
import core.HttpUtil.{HttpCallFailed, HttpCallSuccessful}
import core.{CoreException, Meta, Timestamp}
import exceptions.{AuthException, ExternalFailException, ValidateException}

@RunWith(classOf[JUnitRunner])
class PaypalHandlerSpec extends FlatSpec with MockitoSugar with BeforeAndAfter with Matchers {
  var m: MyMock = _

  before {
    m = new MyMock
  }

  class MyMock extends com.coldcore.haala.service.test.MyMock {
    val handler = new PaypalHandler with AuthStub with VerifyStub { serviceContainer = m.serviceContainer }
    val parentAbyss = new AbyssEntry { id = 3L; ref = "R123"; pack = "mypack" }
    val paymentAbyss = new AbyssEntry {
      id = 4L; ref = "R123"; pack = "mypack"
      value = "parent=3;ref=A087DECDA06F;amount=1750;currency=GBP;expire=20180216235948778;user=2;username=tester;closed=n"
      meta = "s_typ=pay;s_ref=R123_3_FOO;s_usr=tester;"
    }
    val paymentEntry = PaymentEntry(paymentAbyss, serviceContainer)
    val txAbyss = new AbyssEntry { id = 10L; ref = "R123"; pack = "mypack" }

    when(settingService.querySystem("PaymentConf")).thenReturn(setting_paymentConf())

    when(abyssService.getById(2)).thenAnswer(_ => Some(abyss_paymentSaleCompleted))
    when(abyssService.getById(3)).thenReturn(Some(parentAbyss))
    when(abyssService.getById(4)).thenReturn(Some(paymentAbyss))
    when(paymentService.searchByRef("A087DECDA06F")).thenReturn(Some(paymentEntry))
  }

  def setupTxAbyss(sandbox: Boolean = true) {
    val abyss = m.txAbyss
    val value = if (sandbox) abyss_paymentSaleCompleted.value else abyss_paymentSaleCompleted_prodction.value

    when { // create txAbyss
      m.abyssService.createNow(m.paymentAbyss.id, AbyssService.ValueMetaIN(
        value = value, meta = "vendor=paypal;verify=pending;type=sale"))
    }.thenAnswer { inv =>
      val x = inv.getArguments.last.asInstanceOf[AbyssService.ValueMetaIN]
      if (x.value != "") abyss.value = x.value
      if (x.meta != "") abyss.meta = x.meta
      when(m.abyssService.getById(10)).thenReturn(Some(abyss))
      abyss
    }
    when { // update txAbyss meta
      m.abyssService.updateNow(ArgumentMatchers.eq(10L), anyString)
    }.thenAnswer { inv =>
      val x = inv.getArguments.last.asInstanceOf[String]
      if (x != "") abyss.meta = x
    }
  }

  def setupProduction {
    setupTxAbyss(sandbox = false)
    when(m.settingService.querySystem("PaymentConf")).thenReturn(setting_paymentConf(sandbox = false))
    when(m.abyssService.getById(2)).thenAnswer(_ => Some(abyss_paymentSaleCompleted_prodction))
  }

  trait VerifyStub extends PaypalVerify {
    self: PaypalHandler =>
    val resultVerifyTx = true
    override def verifyTx(event: EventHead, key: ClientKey): Boolean = resultVerifyTx
  }

  trait AuthStub extends PaypalAuth {
    self: PaypalHandler =>
    override def authorization(key: ClientKey, passive: Boolean): (String,String) =
      ("Authorization", "Bearer my-access-token")
  }

  def setting_paymentConf(sandbox: Boolean = true) = """
      |{
      |    "paypal": {
      |        "enabled": "y",
      |        "name": "default",
      |        "env": "sandbox",
      |        "production_url": "https://api.paypal.com",
      |        "production_client": "AQbnjIKPeIOMUYiAx4cPzzvq1LmQ2dAoiXvDaP",
      |        "production_secret": "EDvFjEHcPBwpZr0jkWoT9f_zp2vj-n8DuB2PsX",
      |        "sandbox_url": "https://api.sandbox.paypal.com",
      |        "sandbox_client": "WqZyQ6Hc9kK_pgDIF0GkP1bre94m_Wkqm5U3EC9",
      |        "sandbox_secret": "XPrsDRN38DhekJ05H5ECAmtCwFC2NQN9zvjOPWQ"
      |    }
      |}
    """.stripMargin.trim.replace("\"sandbox\"", if (sandbox) "sandbox" else "production")

  lazy val abyss_paymentSaleCompleted = new AbyssEntry {
    id = 2L
    value = m.getResourceAsString("/paypal/event-payment.sale.completed.json") // lazy loaded
    meta = "vendor=paypal;type=event;tmp=y"
  }

  lazy val abyss_paymentSaleCompleted_prodction = new AbyssEntry {
    id = 2L
    value = m.getResourceAsString("/paypal/event-payment.sale.completed.json") // lazy loaded
        .replace("api.sandbox.paypal.com", "api.paypal.com")
    meta = "vendor=paypal;type=event;tmp=y"
  }

  it should "ignore all other events" in {
    val content = """{ "id": "WH-066332897V146121C-7FT48942H5416970K", "event_type": "PAYMENT.OTHER" }"""
    val abyss = new AbyssEntry { id = 2L; value = content; meta = "vendor=paypal;type=event;tmp=y" }
    when(m.abyssService.getById(2)).thenReturn(Some(abyss))

    m.handler.onEvent(2)
    verify(m.paymentService, times(0)).searchByRef(anyString)
  }

  it should "ignore closed or missing payment entries" in {
    val handler = new PaypalHandler {
      serviceContainer = m.serviceContainer
      override protected def paymentCompleted(event: PaymentCompletedEvent, content: String, paymentEntry: PaymentEntry, eventAbyssId: Long) =
        throw new Exception("evaluated")
    }
    m.paymentAbyss.value = (Meta(m.paymentAbyss.value) + ("closed" -> "y")).serialize
    when(m.paymentService.searchByRef("A087DECDA06F")).thenReturn(Some(PaymentEntry(m.paymentAbyss, m.serviceContainer)))
    handler.onEvent(2)

    when(m.paymentService.searchByRef("A087DECDA06F")).thenReturn(None)
    handler.onEvent(2)
  }

  it should "process payment completed event (sandbox)" in {
    setupTxAbyss()

    m.handler.onEvent(2)

    m.txAbyss.value shouldBe abyss_paymentSaleCompleted.value
    val txMeta = Meta(m.txAbyss.meta)
    (txMeta("vendor"), txMeta("type"), txMeta("verify"), txMeta("env")) shouldBe ("paypal", "sale", "valid", "sandbox")
    verify(m.paymentService).push(m.paymentAbyss.id, test = true)
    verify(m.paymentService).message(m.paymentAbyss.id)
  }

  it should "process payment completed event (production)" in {
    setupProduction

    m.handler.onEvent(2)

    m.txAbyss.value shouldBe abyss_paymentSaleCompleted_prodction.value
    val txMeta = Meta(m.txAbyss.meta)
    (txMeta("vendor"), txMeta("type"), txMeta("verify"), txMeta("env")) shouldBe ("paypal", "sale", "valid", "production")
    verify(m.paymentService).push(m.paymentAbyss.id, test = false)
    verify(m.paymentService).message(m.paymentAbyss.id)
  }

  it should "save entry when verify fails" in { 
    trait VerifyFail extends PaypalVerify {
      self: PaypalHandler =>
      override def verifyTx(event: EventHead, key: ClientKey): Boolean = throw new ExternalFailException
    }
    val handler = new PaypalHandler with AuthStub with VerifyFail { serviceContainer = m.serviceContainer }
    setupTxAbyss()

    an [ExternalFailException] should be thrownBy handler.onEvent(2)

    m.txAbyss.value shouldBe abyss_paymentSaleCompleted.value
    val txMeta = Meta(m.txAbyss.meta)
    (txMeta("vendor"), txMeta("type"), txMeta("verify")) shouldBe ("paypal", "sale", "pending")
  }

  it should "process payment completed event and not rethrow push errors" in {
    setupTxAbyss()
    when(m.paymentService.push(m.paymentAbyss.id, test = true)).thenThrow(new CoreException)

    m.handler.onEvent(2)

    m.txAbyss.value shouldBe abyss_paymentSaleCompleted.value
    verify(m.paymentService).push(m.paymentAbyss.id, test = true)
    verify(m.paymentService).message(m.paymentAbyss.id)
  }

  it should "not push payment completed event (not verified)" in {
    val handler = new PaypalHandler with AuthStub with VerifyStub {
      serviceContainer = m.serviceContainer
      override val resultVerifyTx: Boolean = false
    }
    setupTxAbyss()

    handler.onEvent(2)

    m.txAbyss.value shouldBe abyss_paymentSaleCompleted.value
    Meta(m.txAbyss.meta)("verify") shouldBe "invalid"
    verify(m.paymentService, times(0)).push(m.paymentAbyss.id, test = true)
    verify(m.paymentService, times(0)).push(m.paymentAbyss.id, test = false)
    verify(m.paymentService, times(0)).message(m.paymentAbyss.id)
  }

  it should "not push payment completed event (unable to verify due to invalid env)" in {
    when(m.settingService.querySystem("PaymentConf")).thenReturn(
      setting_paymentConf().replace("https://api.sandbox.paypal.com", "https://api.other.paypal.com"))
    setupTxAbyss()

    m.handler.onEvent(2)

    m.txAbyss.value shouldBe abyss_paymentSaleCompleted.value
    Meta(m.txAbyss.meta)("verify") shouldBe "unable"
    verify(m.paymentService, times(0)).push(m.paymentAbyss.id, test = true)
    verify(m.paymentService, times(0)).push(m.paymentAbyss.id, test = false)
    verify(m.paymentService, times(0)).message(m.paymentAbyss.id)
  }

  it should "not push payment completed event (mismatch amount)" in {
    m.paymentAbyss.value = m.paymentAbyss.value.replace("amount=1750;", "amount=2500;")
    val paymentEntry = PaymentEntry(m.paymentAbyss, m.serviceContainer)
    when(m.paymentService.searchByRef("A087DECDA06F")).thenReturn(Some(paymentEntry))
    setupTxAbyss()

    m.handler.onEvent(2)

    m.txAbyss.value shouldBe abyss_paymentSaleCompleted.value
    Meta(m.txAbyss.meta)("verify") shouldBe "mismatch"
    verify(m.paymentService, times(0)).push(m.paymentAbyss.id, test = true)
    verify(m.paymentService, times(0)).push(m.paymentAbyss.id, test = false)
    verify(m.paymentService, times(0)).message(m.paymentAbyss.id)
  }

  it should "not push payment completed event (mismatch currency)" in {
    m.paymentAbyss.value = m.paymentAbyss.value.replace("currency=GBP;", "currency=USD;")
    val paymentEntry = PaymentEntry(m.paymentAbyss, m.serviceContainer)
    when(m.paymentService.searchByRef("A087DECDA06F")).thenReturn(Some(paymentEntry))
    setupTxAbyss()

    m.handler.onEvent(2)

    m.txAbyss.value shouldBe abyss_paymentSaleCompleted.value
    Meta(m.txAbyss.meta)("verify") shouldBe "mismatch"
    verify(m.paymentService, times(0)).push(m.paymentAbyss.id, test = true)
    verify(m.paymentService, times(0)).push(m.paymentAbyss.id, test = false)
    verify(m.paymentService, times(0)).message(m.paymentAbyss.id)
  }

}

@RunWith(classOf[JUnitRunner])
class PaypalAuthSpec extends FlatSpec with MockitoSugar with BeforeAndAfter with Matchers {
  var m: MyMock = _

  before {
    m = new MyMock
  }

  class MyMock extends com.coldcore.haala.service.test.MyMock {
    val handler = new PaypalHandler with VerifyStub {
      serviceContainer = m.serviceContainer
      override val staticBlock = m.staticBlock
    }
    val clientKey = ClientKey("my-clientId", "my-secret", sandbox = true)

    when(settingService.querySystem("PaymentConf")).thenReturn(setting_paymentConf())
  }

  def setupValidToken {
    when(m.settingService.querySystem("Payment/paypal_token_default")).thenReturn( // non expired token
      "token=my-access-token;created={created};expires={expires}"
        .replace("{created}", Timestamp().addMinutes(-5).toString)
        .replace("{expires}", Timestamp().addMinutes(55).toString))
  }

  def setupFetchToken(sandbox: Boolean = true, code: Int = 200,
                      body: String = """{ "access_token": "access-token-new", "expires_in": 3600 }""") {
    val urlPrefix = if (sandbox) "https://api.sandbox.paypal.com" else "https://api.paypal.com"
    when(m.staticBlock.httpPost(s"$urlPrefix/v1/oauth2/token",
      Map("grant_type" -> "client_credentials"),
      ("Authorization", "Basic "+"my-clientId:my-secret".encodeBase64) :: Nil)).thenReturn(
      HttpCallSuccessful(null, code, body, Nil))
  }

  trait VerifyStub extends PaypalVerify {
    self: PaypalHandler =>
    override def verifyTx(event: EventHead, key: ClientKey): Boolean = true
  }

  def setting_paymentConf(sandbox: Boolean = true) = """
      |{
      |    "paypal": {
      |        "enabled": "y",
      |        "name": "default",
      |        "env": "sandbox",
      |        "production_url": "https://api.paypal.com",
      |        "production_client": "AQbnjIKPeIOMUYiAx4cPzzvq1LmQ2dAoiXvDaP",
      |        "production_secret": "EDvFjEHcPBwpZr0jkWoT9f_zp2vj-n8DuB2PsX",
      |        "sandbox_url": "https://api.sandbox.paypal.com",
      |        "sandbox_client": "WqZyQ6Hc9kK_pgDIF0GkP1bre94m_Wkqm5U3EC9",
      |        "sandbox_secret": "XPrsDRN38DhekJ05H5ECAmtCwFC2NQN9zvjOPWQ"
      |    }
      |}
    """.stripMargin.trim.replace("\"sandbox\"", if (sandbox) "sandbox" else "production")

  it should "fetch new access token (sandbox)" in {
    setupFetchToken()

    m.handler.authorization(m.clientKey, passive = false) shouldBe ("Authorization", "Bearer access-token-new")
  }

  it should "fetch new access token (production)" in {
    when(m.settingService.querySystem("PaymentConf")).thenReturn(setting_paymentConf(sandbox = false))
    setupFetchToken(sandbox = false)

    m.handler.authorization(m.clientKey.copy(sandbox = false), passive = false) shouldBe ("Authorization", "Bearer access-token-new")
  }

  it should "reuse valid access token from settings" in {
    setupValidToken

    val (_, token) = m.handler.authorization(m.clientKey)
    token shouldBe "Bearer my-access-token"
  }

  it should "fetch new access token even if settings token exists" in {
    setupValidToken
    setupFetchToken()

    val (_, token) = m.handler.authorization(m.clientKey, passive = false)
    token shouldBe "Bearer access-token-new"
  }

  it should "fetch new access token and save it" in {
    setupFetchToken()

    val (_, token) = m.handler.authorization(m.clientKey)
    token shouldBe "Bearer access-token-new"
    verify(m.settingService).write( // save token
      ArgumentMatchers.eq("Payment/paypal_token_default"), anyString, ArgumentMatchers.eq("sys"))
  }

  it should "fetch new access token if settings token expired" in {
    when(m.settingService.querySystem("Payment/paypal_token_default")).thenReturn( // just now expired token
      "token=my-access-token;created={created};expires={expires}"
        .replace("{created}", Timestamp().addMinutes(-60).toString)
        .replace("{expires}", Timestamp().toString))
    setupFetchToken()

    val (_, token) = m.handler.authorization(m.clientKey)
    token shouldBe "Bearer access-token-new"
  }

  it should "throw error if response has no token" in {
    setupFetchToken(code = 401, body = """{"error":"invalid_token","error_description":"Token signature verification failed"}""")
    an [AuthException] should be thrownBy m.handler.authorization(m.clientKey)

    setupFetchToken(code = 404, body = "Page not found")
    an [AuthException] should be thrownBy m.handler.authorization(m.clientKey)

    setupFetchToken(code = 503, body = null)
    an [AuthException] should be thrownBy m.handler.authorization(m.clientKey)
  }

  it should "throw error if response fails" in {
    val urlPrefix = "https://api.sandbox.paypal.com"
    when(m.staticBlock.httpPost(s"$urlPrefix/v1/oauth2/token",
      Map("grant_type" -> "client_credentials"),
      ("Authorization", "Basic "+"my-clientId:my-secret".encodeBase64) :: Nil)).thenReturn(
      HttpCallFailed(null, null))

    an [ExternalFailException] should be thrownBy m.handler.authorization(m.clientKey)
  }

}

@RunWith(classOf[JUnitRunner])
class PaypalVerifySpec extends FlatSpec with MockitoSugar with BeforeAndAfter with Matchers {
  var m: MyMock = _

  before {
    m = new MyMock
  }

  class MyMock extends com.coldcore.haala.service.test.MyMock {
    val handler = new PaypalHandler with AuthStub {
      serviceContainer = m.serviceContainer
      override val staticBlock = m.staticBlock
    }
    val clientKey = ClientKey("my-clientId", "my-secret", sandbox = true)
    val txId = "WH-066332897V146121C-7FT48942H5416970K"

    when(settingService.querySystem("PaymentConf")).thenReturn(setting_paymentConf())
  }

  def setupFetchContent(content: String, sandbox: Boolean = true, code: Int = 200, token: String = "my-access-token") {
    val urlPrefix = if (sandbox) "https://api.sandbox.paypal.com" else "https://api.paypal.com"
    when(m.staticBlock.httpGet(s"$urlPrefix/v1/notifications/webhooks-events/${m.txId}",
      ("Authorization", "Bearer "+token) :: Nil)).thenReturn(HttpCallSuccessful(null, code, content, Nil))
  }

  trait AuthStub extends PaypalAuth {
    self: PaypalHandler =>
    override def authorization(key: ClientKey, passive: Boolean): (String,String) =
      ("Authorization", "Bearer my-access-token")
  }

  /** Fetch token only once then error */
  trait AuthOnce extends PaypalAuth {
    self: PaypalHandler =>
    var authorized = false
    override def authorization(key: ClientKey, passive: Boolean): (String,String) = {
      if (passive && !authorized) ("Authorization", "Bearer my-access-token")
      else if (authorized) throw new CoreException // already authorized once
      else {
        authorized = true
        ("Authorization", "Bearer access-token-new")
      }
    }
  }

  def setting_paymentConf(sandbox: Boolean = true) = """
      |{
      |    "paypal": {
      |        "enabled": "y",
      |        "name": "default",
      |        "env": "sandbox",
      |        "production_url": "https://api.paypal.com",
      |        "production_client": "AQbnjIKPeIOMUYiAx4cPzzvq1LmQ2dAoiXvDaP",
      |        "production_secret": "EDvFjEHcPBwpZr0jkWoT9f_zp2vj-n8DuB2PsX",
      |        "sandbox_url": "https://api.sandbox.paypal.com",
      |        "sandbox_client": "WqZyQ6Hc9kK_pgDIF0GkP1bre94m_Wkqm5U3EC9",
      |        "sandbox_secret": "XPrsDRN38DhekJ05H5ECAmtCwFC2NQN9zvjOPWQ"
      |    }
      |}
    """.stripMargin.trim.replace("\"sandbox\"", if (sandbox) "sandbox" else "production")

  lazy val event_paymentSaleCompleted = m.getResourceAsString("/paypal/event-payment.sale.completed.json") // lazy loaded
  lazy val event = new Gson().fromJson(event_paymentSaleCompleted, classOf[EventHead])

  it should "verify transaction (sandbox)" in {
    setupFetchContent(event_paymentSaleCompleted)
    m.handler.verifyTx(event, m.clientKey) shouldBe true
  }

  it should "verify transaction (production)" in {
    setupFetchContent(event_paymentSaleCompleted, sandbox = false)
    m.handler.verifyTx(event, m.clientKey.copy(sandbox = false)) shouldBe true
  }

  it should "reject on transaction content mismatch" in {
    setupFetchContent(event_paymentSaleCompleted.replace("2017-05-08T15:21:37.846Z", "2017-05-08T15:21:37.847Z"))
    m.handler.verifyTx(event, m.clientKey) shouldBe false

    setupFetchContent("Page not found", code = 404)
    m.handler.verifyTx(event, m.clientKey) shouldBe false

    setupFetchContent(null, code = 503)
    m.handler.verifyTx(event, m.clientKey) shouldBe false
  }

  it should "throw error if response fails" in {
    val urlPrefix = "https://api.sandbox.paypal.com"
    when(m.staticBlock.httpGet(s"$urlPrefix/v1/notifications/webhooks-events/${m.txId}",
      ("Authorization", "Bearer my-access-token") :: Nil)).thenReturn(HttpCallFailed(null, null))
    an [ExternalFailException] should be thrownBy m.handler.verifyTx(event, m.clientKey)
  }

  it should "authorize if needed" in {
    val handler = new PaypalHandler with AuthOnce {
      serviceContainer = m.serviceContainer
      override val staticBlock = m.staticBlock
    }
    val msg401 = """{"error":"invalid_token","error_description":"Token signature verification failed"}"""
    setupFetchContent(msg401, code = 401)
    setupFetchContent(event_paymentSaleCompleted, token = "access-token-new")

    handler.verifyTx(event, m.clientKey) shouldBe true
  }

  it should "throw error on invalid authorization" in {
    val handler = new PaypalHandler with AuthOnce {
      serviceContainer = m.serviceContainer
      override val staticBlock = m.staticBlock
    }
    val msg401 = """{"error":"invalid_token","error_description":"Token signature verification failed"}"""
    setupFetchContent(msg401, code = 401)
    setupFetchContent(msg401, code = 401, token = "access-token-new")

    an [AuthException] should be thrownBy handler.verifyTx(event, m.clientKey)
  }

}
