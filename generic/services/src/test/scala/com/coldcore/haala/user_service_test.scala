package com.coldcore.haala
package service

import service.exceptions._
import service.UserService._
import org.scalatest._
import org.scalatest.mockito.MockitoSugar
import org.scalatest.junit.JUnitRunner
import org.mockito.Mockito._
import org.mockito.ArgumentMatchers._
import org.mockito.ArgumentCaptor
import org.mockito.invocation.InvocationOnMock
import org.mockito.stubbing.Answer
import dao.GenericDAO
import core.{Timestamp, Meta}
import core.VirtualPath.Implicits._
import scala.collection.JavaConverters._
import domain._
import org.junit.runner.RunWith

@RunWith(classOf[JUnitRunner])
class UserServiceSpec extends FlatSpec with MockitoSugar with BeforeAndAfter with Matchers {
  var m: MyMock = _

  before {
    m = new MyMock
  }

  class MyMock extends com.coldcore.haala.service.test.MyMock {
    val (userDao, roleDao) = (mock[GenericDAO[User]], mock[GenericDAO[Role]])
    val service = new UserServiceImpl |< { x =>
      x.serviceContainer = serviceContainer
      x.userDao = userDao; x.roleDao = roleDao
    }

    // add CP user mod
    val cpDomain = new Domain { id = 2L }
    val cpmod = new ControlPanelUserMod |< { x => x.serviceContainer = serviceContainer }
    service.setMods(List(cpmod).asJava.asInstanceOf[JList[UserService.Mod]])
    when (domainService.getAllByType(Domain.TYPE_CONTROL_PANEL)).thenReturn(cpDomain :: Nil)
  }

  private def setupUsers = {
    val systemUser = new User { id = 1L; ref = "*"; username = "*"; email = "system@local.cc" }
    val myUser = new User { id = 2L; ref = "MC002"; username = "myuser"; password = "myPass"; email = "my.user@local.cc";
      primaryNumber = "09876123456"; activated = Timestamp.fromMills(System.currentTimeMillis-24L*60L*60L*1000L) }
    val noactUser = new User { id = 3L; ref = "MC003"; username = "noactuser"; password = "myPass"; email = "noact.user@local.cc" }
    (systemUser, myUser, noactUser)
  }

  private def XIN(username: String, email: String) = IN(
    username = username, password = "passwd", email = email, firstName = "name", lastName = "surname",
    primaryNumber = "020345678", salutation = ""+User.SALUTATION_MS,
    address = AddressIN(line1 = "12 Hyde park corner", line2 = null, town = "London",
      postcode = "WC1 EC2", country = "UK"))

  "activate" should "acivate a user" in {
    val (systemUser, myUser, noactUser) = setupUsers
    val cur = Timestamp()
    when (m.userDao.findById(0)).thenReturn(None)
    when (m.userDao.findById(2)).thenReturn(Some(myUser))
    when (m.userDao.findById(3)).thenReturn(Some(noactUser))

    intercept[ObjectNotFoundException] { m.service.activate(0) }
    m.service.activate(2)
    assert(myUser.activated < cur)
    m.service.activate(3)
    assert(noactUser.activated >= cur)
  }

  "login" should "login with username and password or with email" in {
    val (systemUser, myUser, noactUser) = setupUsers
    val cur = Timestamp()
    when(m.userDao.findUniqueByProperty("username", "unknown")).thenReturn(None)
    when(m.userDao.findUniqueByProperty("username", "*")).thenReturn(Some(systemUser))
    when(m.userDao.findUniqueByProperty("username", "noactuser")).thenReturn(Some(noactUser))
    when(m.userDao.findUniqueByProperty("username", "myuser")).thenReturn(Some(myUser))

    // username / password
    intercept[ObjectNotFoundException] { m.service.login("unknown", "nopass", null) }
    intercept[ObjectNotFoundException] { m.service.login("*", "nopass", null) }
    intercept[UserNotActivatedException] { m.service.login("noactuser", "myPass", null) }
    assertResult(None) { m.service.login("myuser", "nopass", null) }
    assertResult("MC002") { m.service.login("myuser", "myPass", null).get.ref }
    assert(myUser.lastLogin >= cur)

    // email / password
    when(m.userDao.findUniqueByProperty("username", "my.user@local.cc")).thenReturn(None)
    when(m.userDao.findUniqueByProperty("email", "my.user@local.cc")).thenReturn(Some(myUser))
    assertResult("MC002") { m.service.login("my.user@local.cc", "myPass", null).get.ref }
  }

  "loginSso" should "login with ticket" in {
    val (systemUser, myUser, noactUser) = setupUsers
    val fiveMinAgo = (System.currentTimeMillis-1000L*60L*5L).hex
    val twoMinAgo = (System.currentTimeMillis-1000L*60L*2L).hex
    val oneMinAgo = (System.currentTimeMillis-1000L*60L).hex
    myUser.ticket = "MC002_"+twoMinAgo+"_12345"
    when(m.settingService.querySystem("SystemParams")).thenReturn("ssoTimeout=3m")
    when(m.userDao.findUniqueByProperty("ref", "MC002")).thenReturn(Some(myUser))
    when(m.userDao.findUniqueByProperty("ref", "MC000")).thenReturn(None)

    intercept[ObjectExpiredException] { m.service.loginSso("MC002_BAR_12345", 1) }
    intercept[ObjectExpiredException] { m.service.loginSso("MC002_"+fiveMinAgo+"_12345", 1) }
    assertResult(None) { m.service.loginSso("MC002_"+oneMinAgo+"_12345", 1) }
    assertResult("MC002") { m.service.loginSso("MC002_"+twoMinAgo+"_12345", 1).get.ref }
    assertResult(None) { m.service.loginSso("MC002_"+twoMinAgo+"_12345", 1) }
    intercept[ObjectNotFoundException] { m.service.loginSso("MC000_"+twoMinAgo+"_12345", 1) }
  }

  "register" should "register a new user" in {
    val (systemUser, myUser, noactUser) = setupUsers
    when(m.userDao.findUniqueByProperty(anyString, anyString)).thenReturn(None) //generateUniqueRef
    when(m.userDao.findUniqueByProperty("email", "my.user@local.cc")).thenReturn(Some(myUser))
    when(m.userDao.findUniqueByProperty("username", "myuser")).thenReturn(Some(myUser))
    when(m.userDao.findUniqueByProperty("email", "new.email@local.cc")).thenReturn(None)
    when(m.userDao.findUniqueByProperty("username", "newuser")).thenReturn(None)
    doAnswer(new Answer[Unit] {
      override def answer(invocation: InvocationOnMock) {
        val x = invocation.getArguments.head.asInstanceOf[User]; x.id = 7 //assign ID to prepare Solr doc
      }
    }).when(m.userDao).saveOrUpdate(any.asInstanceOf[User])

    intercept[EmailExistsException] { m.service.register(XIN("newuser", "my.user@local.cc"), null) }
    intercept[UsernameExistsException] { m.service.register(XIN("myuser", "new.email@local.cc"), null) }
    val user = m.service.register(XIN("newuser", "new.email@local.cc"), null)
    assertResult(8) { user.ref.length }
    assertResult("surname") { user.lastName }
    assertResult("WC1 EC2") { user.address.postcode }
    assertResult(0) { user.roles.size }
    verify(m.solrService).submitWT(any.asInstanceOf[SolrService.SolrDoc])
  }

  "delete" should "delete an existing user" in {
    val (systemUser, myUser, noactUser) = setupUsers
    when (m.userDao.findById(2)).thenReturn(Some(myUser))
    when (m.userDao.findById(1)).thenReturn(Some(systemUser))
    intercept[ObjectNotFoundException] { m.service.delete(1) } //system user
    m.service.delete(2)
    verify(m.userDao).delete(myUser)
    verify(m.labelService).deleteByKeyLike("u.MC002.%")
    verify(m.fileService).deleteByFolder("u/MC002/*")
    verify(m.solrService).deleteByIdsWT(any.asInstanceOf[SolrService.IdTerm])
  }

  "update" should "update an existing user" in {
    val (systemUser, myUser, noactUser) = setupUsers
    when (m.userDao.findById(2)).thenReturn(Some(myUser))
    when (m.userDao.findById(0)).thenReturn(None)
    when(m.userDao.findUniqueByProperty("email", "noact.user@local.cc")).thenReturn(Some(noactUser))
    when(m.userDao.findUniqueByProperty("username", "noactuser")).thenReturn(Some(noactUser))
    when(m.userDao.findUniqueByProperty("email", "email@local.cc")).thenReturn(None)
    when(m.userDao.findUniqueByProperty("username", "username")).thenReturn(None)
    when(m.userDao.findUniqueByProperty("username", "myuser")).thenReturn(Some(myUser))

    intercept[ObjectNotFoundException] { m.service.update(0, XIN("username", "email@local.cc")) }
    intercept[UsernameExistsException] { m.service.update(2, XIN("noactuser", "email@local.cc")) }
    intercept[EmailExistsException] { m.service.update(2, XIN("username", "noact.user@local.cc")) }
    m.service.update(2, XIN("myuser", "email@local.cc"))
    assertResult("email@local.cc") ( myUser.email )
  }

  "changePassword" should "change user password" in {
    val (systemUser, myUser, noactUser) = setupUsers
    when (m.userDao.findById(2)).thenReturn(Some(myUser))
    when (m.userDao.findById(0)).thenReturn(None)
    intercept[ObjectNotFoundException] { m.service.changePassword(0, "changed") }
    m.service.changePassword(2, "changed")
    assertResult("changed") ( myUser.password )
  }

  "credit" should "credit or debit user balance" in {
    val (systemUser, myUser, noactUser) = setupUsers
    when (m.userDao.findById(2)).thenReturn(Some(myUser))
    when (m.userDao.findById(0)).thenReturn(None)

    intercept[ObjectNotFoundException] { m.service.credit(0, 100) }
    m.service.credit(2, 100)
    assertResult(100) ( myUser.credit )
    m.service.credit(2, -70)
    assertResult(30) ( myUser.credit )
    m.service.credit(2, -500)
    assertResult(0) ( myUser.credit )
  }

  "isSiteOwner" should "check if a user owns a site" in {
    val (systemUser, myUser, noactUser) = setupUsers
    val domain = new Domain { id = 1001L; domain = "myagency.com"; domainSite = "pp.agy"; `type` = "agency" }
    myUser.domains.add(domain)
    when(m.domainService.listSites(domain)).thenReturn("agy1" :: "agy2" :: Nil)
    when (m.userDao.findById(2)).thenReturn(Some(myUser))
    when (m.userDao.findById(0)).thenReturn(None)

    assert(!m.service.isSiteOwner(0, "unk1"))
    assert(!m.service.isSiteOwner(2, "unk1"))
    assert(!m.service.isSiteOwner(2, "agy5"))
    assert(m.service.isSiteOwner(2, "agy2"))
    assert(m.service.isSiteOwner(2, "agy1j"))
  }

  "isDomainOwner" should "check if a user owns a domain" in {
    val (systemUser, myUser, noactUser) = setupUsers
    val domain = new Domain { id = 1001L; domain = "myagency.com"; domainSite = "pp.agy"; `type` = "agency" }
    myUser.domains.add(domain)
    when(m.domainService.getByDomain("myagency.com")).thenReturn(Some(domain))
    when(m.domainService.getByDomain("unknown.com")).thenReturn(None)
    when(m.domainService.getById(1001)).thenReturn(Some(domain))
    when(m.domainService.getById(0)).thenReturn(None)
    when (m.userDao.findById(2)).thenReturn(Some(myUser))
    when (m.userDao.findById(0)).thenReturn(None)

    assert(!m.service.isDomainOwner(0, "unknown.com"))
    assert(!m.service.isDomainOwner(0, 0))
    assert(!m.service.isDomainOwner(2, "unknown.com"))
    assert(!m.service.isDomainOwner(2, 0))
    assert(m.service.isDomainOwner(2, "myagency.com"))
    assert(m.service.isDomainOwner(2, 1001))
  }

  "genTicket" should "generate a new ticket for SSO" in {
    val (systemUser, myUser, noactUser) = setupUsers
    when (m.userDao.findById(2)).thenReturn(Some(myUser))
    when (m.userDao.findById(0)).thenReturn(None)
    intercept[ObjectNotFoundException] { m.service.genTicket(0) }
    m.service.genTicket(2)
    assert(myUser.ticket.startsWith("MC002_"))
  }

  it should "register a user, accept terms and login to Control Panel" in {
    when(m.userDao.findUniqueByProperty(anyString, anyString)).thenReturn(None) //generateUniqueRef
    when(m.userService.getRoles(Nil)).thenReturn(Nil)
    doAnswer(new Answer[Unit] {
      override def answer(invocation: InvocationOnMock) {
        val x = invocation.getArguments.head.asInstanceOf[User]; x.id = 7 //assign ID to prepare Solr doc
      }
    }).when(m.userDao).saveOrUpdate(any.asInstanceOf[User])

    // myuser needs to accept T&C when login to CP (registered through other domain)
    val userMyDomain = m.service.register(XIN("myuser", "my.email@local.cc"), new Domain)
    Meta(userMyDomain.meta).csv("terms").serialize shouldBe "CP;"
    userMyDomain.activated = Timestamp()
    when (m.userDao.findUniqueByProperty("username", userMyDomain.username)).thenReturn(Some(userMyDomain))
    when (m.userDao.findUniqueByProperty("ref", userMyDomain.ref)).thenReturn(Some(userMyDomain))

    // cpuser does not need to accept T&C when login to CP (registered through CP domain)
    val userCpDomain = m.service.register(XIN("cpuser", "cp.email@local.cc"), m.cpDomain)
    Meta(userCpDomain.meta).csv("terms").serialize shouldBe ""
    userCpDomain.activated = Timestamp()
    when (m.userDao.findUniqueByProperty("username", userCpDomain.username)).thenReturn(Some(userCpDomain))
    when (m.userDao.findUniqueByProperty("ref", userCpDomain.ref)).thenReturn(Some(userCpDomain))

    intercept[UserNoAgreementException] { m.service.login(userMyDomain.username, userMyDomain.password, m.cpDomain) }
    m.service.acceptAgreement(userMyDomain.ref, m.cpDomain)
    Meta(userMyDomain.meta).csv("terms").serialize shouldBe ""

    m.service.login(userMyDomain.username, userMyDomain.password, m.cpDomain) shouldBe Some(userMyDomain)
    m.service.login(userCpDomain.username, userCpDomain.password, m.cpDomain) shouldBe Some(userCpDomain)
  }

}
