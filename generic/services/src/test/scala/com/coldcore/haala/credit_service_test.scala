package com.coldcore.haala
package service

import com.coldcore.haala.core.Meta
import com.coldcore.haala.service.exceptions.ObjectProcessedException
import org.scalatest.{BeforeAndAfter, FlatSpec, Matchers}
import org.scalatest.mockito.MockitoSugar
import org.mockito.Mockito._
import org.mockito.ArgumentMatchers._
import org.mockito.ArgumentCaptor
import domain.{AbyssEntry, User}
import org.junit.runner.RunWith
import org.scalatest.junit.JUnitRunner
import service.CreditService.CreditUserIN

@RunWith(classOf[JUnitRunner])
class CreditServiceSpec extends FlatSpec with MockitoSugar with BeforeAndAfter with Matchers {
  var m: MyMock = _

  before {
    m = new MyMock
  }

  class MyMock extends com.coldcore.haala.service.test.MyMock {
    val service = new CreditServiceImpl { serviceContainer = m.serviceContainer }

    when(settingService.querySystem("CreditConf")).thenReturn(setting_creditConf)
  }

  private val setting_creditConf = """
    |{
    |   "keep": "30d"
    |}
  """.stripMargin.trim

  "creditUser" should "credit user and create new credit abyss entry" in {
    when(m.userService.getById(2)).thenReturn(Some(new User { id = 2L; ref = "FOO"; username = "tester" }))
    doAnswer(invocation => {
      val x = invocation.getArguments.head.asInstanceOf[AbyssService.CreateIN]
      new AbyssEntry { id = 4L; value = x.vm.value; meta = x.vm.meta }
    }).when(m.abyssService).create(any[AbyssService.CreateIN])

    val r = m.service.creditUser(2, CreditUserIN("mypack", "message=my-text;foo=bar", "prop=a", -25))
    (r.abyss.id, r.amount, r.username, r.message) shouldBe (4, -25, "tester", "my-text")
    verify(m.userService).credit(2, -25)
  }

  "onPayment" should "process previously created entry (paid)" in {
    val abyss = new AbyssEntry { id = 3L; value = "user=2;amount=25;complete=n" }
    when(m.userService.getById(2)).thenReturn(Some(new User { id = 2L; ref = "FOO"; username = "tester" }))
    doAnswer(invocation => {
      val x = invocation.getArguments.last.asInstanceOf[AbyssService.ValueMetaIN]
      if (x.value != "") abyss.value = x.value
      if (x.meta != "") abyss.meta = x.meta
    }).when(m.abyssService).update(anyLong, any[AbyssService.ValueMetaIN])

    m.service.onPayment(abyss)
    Meta(abyss.value)("complete") shouldBe "y"
    verify(m.userService).credit(2, 25)
    verify(m.abyssService).create(3, AbyssService.ValueMetaIN(value = "complete=auto"))
  }

  "onPayment" should "not process the same entry again" in {
    val abyss = new AbyssEntry { id = 3L; value = "user=2;amount=25;complete=y" }
    when(m.userService.getById(2)).thenReturn(Some(new User { id = 2L; ref = "FOO"; username = "tester" }))

    an [ObjectProcessedException] should be thrownBy m.service.onPayment(abyss)
  }

}

@RunWith(classOf[JUnitRunner])
class CreditPaymentModSpec extends FlatSpec with MockitoSugar with BeforeAndAfter with Matchers {
  var m: MyMock = _

  before {
    m = new MyMock
  }

  class MyMock extends com.coldcore.haala.service.test.MyMock {
    val mod = new CreditPaymentMod { serviceContainer = m.serviceContainer }
  }

  it should "only process credit payment entries" in {
    val abyssA = new AbyssEntry { value = "user=2;amount=25;complete=y;process=crd1" }
    val abyssB = new AbyssEntry { value = "user=2;amount=25;complete=y;process=crd1"; pack="mypack" }
    val abyssC = new AbyssEntry 
    m.mod.process(abyssA) shouldBe true
    m.mod.process(abyssB) shouldBe false
    m.mod.process(abyssC) shouldBe false
  }

}
