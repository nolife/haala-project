package com.coldcore.haala
package service

import org.scalatest.mockito.MockitoSugar
import org.mockito.Mockito._
import org.scalatest._
import org.junit.runner.RunWith
import org.scalatest.junit.JUnitRunner
import java.io.FileNotFoundException
import core.SiteChain.Implicits._
import core.VirtualPath.Implicits._

@RunWith(classOf[JUnitRunner])
class FreemarkerEngineSpec extends WordSpec with MockitoSugar with BeforeAndAfter {
  var m: MyMock = _

  before {
    m = new MyMock
  }

  class MyMock extends com.coldcore.haala.service.test.MyMock {
    when(fileService.getByPath("bar/unknown.ftl", "xx1.un1")).thenReturn(None)
    registerFile(11, "bar/template.ftl", "xx1.mc1", "Hello ${username}")
    registerFile(16, "bar/template.ftl", ".sys", "This is ${location}!")

    registerFile(12, "bar/template.ftl", "xx1.mh1", template_mh)
    registerFile(13, "bar/depend.ftl", "xx1.mh1", depend_mh)
    registerFile(14, "bar/other.ftl", "xx1.mh1", other_mh)
  }

  private val template_mh = """
      |<#include "/bar/depend.ftl:xx1.mh1">
      |<#include "other.ftl:xx1.mh1">
      |
      |Hello ${username}.
      |<@greet person="Fred"/> and <@hello person="Batman"/>
    """.stripMargin.trim

  private val depend_mh = """
      |<#macro greet person>
      |  <font size=2>Hello ${person}!</font>
      |</#macro>
    """.stripMargin.trim

  private val other_mh = """
      |<#macro hello person>
      |  <font color=red>Hello ${person}!</font>
      |</#macro>
    """.stripMargin.trim

  "engine" should {
    "load correct template" in {
      val r = m.processFtl("bar/template.ftl:xx1.mc1", Map("username" -> "Big Joe"))
      assertResult("Hello Big Joe") { r }
    }

    "error if template not found" in {
      intercept[FileNotFoundException] { m.fmEngine.template("bar/unknown.ftl:xx1.un1") }
    }

    "resolve template dependencies" in {
      val r = m.processFtl("bar/template.ftl:xx1.mh1", Map("username" -> "Big Joe"))
      assertResult("Hello Big Joe. <font size=2>Hello Fred!</font> and <font color=red>Hello Batman!</font>") {
        r.replace("\n", "").replaceAll("\\s+", " ")
      }
    }

    "load sys template when no site given" in {
      val r = m.processFtl("bar/template.ftl", Map("location" -> "Sparta"))
      assertResult("This is Sparta!") { r }
    }
  }

}
