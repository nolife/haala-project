package com.coldcore.haala
package service

import org.scalatest.{BeforeAndAfter, FlatSpec, Matchers}
import com.coldcore.gfx.Gfx
import com.coldcore.haala.core.VirtualPath
import org.scalatest.mockito.MockitoSugar
import org.mockito.Mockito._
import org.mockito.ArgumentMatchers._
import org.junit.runner.RunWith
import org.mockito.ArgumentCaptor
import org.scalatest.junit.JUnitRunner
import core.SiteChain.Implicits._
import core.VirtualPath.Implicits._

@RunWith(classOf[JUnitRunner])
class TuringServiceSpec extends FlatSpec with MockitoSugar with BeforeAndAfter with Matchers {
  var m: MyMock = _

  before {
    m = new MyMock
  }

  class MyMock extends com.coldcore.haala.service.test.MyMock {
    val service = new TuringServiceImpl |< { x =>
      x.serviceContainer = serviceContainer
    }
  }

  "generate" should "generate image" in {
    when(m.settingService.query("TuringParams", "xx1")).thenReturn("")
    val r = m.service.generate("xx1")
    val (argPath, argSite, argData) = (
      ArgumentCaptor.forClass(classOf[VirtualPath]), ArgumentCaptor.forClass(classOf[String]), ArgumentCaptor.forClass(classOf[Array[Byte]]))
    verify(m.fileService).create(argPath.capture, argSite.capture, argData.capture)
    r.value should have length 5
    argSite.getValue shouldBe "pub"
    argPath.getValue.folder shouldBe "turing"
    argPath.getValue.filename should endWith (".png")
    val img = Gfx.createImage(argData.getValue)
    img should not be null
    img.getWidth(null)+"x"+img.getHeight(null) shouldBe "100x20"
  }

  "generate" should "generate image based on parameters" in {
    when(m.settingService.query("TuringParams", "xx1")).thenReturn("size=120x30, n=4")
    val r = m.service.generate("xx1")
    val argData = ArgumentCaptor.forClass(classOf[Array[Byte]])
    verify(m.fileService).create(any[VirtualPath], anyString, argData.capture)
    r.value should have length 4
    val img = Gfx.createImage(argData.getValue)
    img should not be null
    img.getWidth(null)+"x"+img.getHeight(null) shouldBe "120x30"
  }

}