package com.coldcore.haala
package service

import org.scalatest.mockito.MockitoSugar
import org.mockito.Mockito._
import org.scalatest._
import org.junit.runner.RunWith
import org.scalatest.junit.JUnitRunner
import MiscService.{FreemarkerTemplateIN, VelocityTemplateIN}
import core.SiteChain.Implicits._
import core.VirtualPath.Implicits._

@RunWith(classOf[JUnitRunner])
class MiscServiceSpec extends WordSpec with MockitoSugar with BeforeAndAfter {
  var m: MyMock = _

  before {
    m = new MyMock
  }

  class MyMock extends com.coldcore.haala.service.test.MyMock {
    val service = new MiscServiceImpl |< { x =>
      x.serviceContainer = serviceContainer
      x.freemarkerEngine = fmEngine
      x.velocityEngine = vmEngine
    }
  }

  "freemarkerTemplate" should {
    "process template" in {
      m.registerFile(11, "bar/template.ftl", "xx1.mc1", "Hello ${username}")
      val model = Map("username" -> "Big Joe")
      val r = m.service.freemarkerTemplate(FreemarkerTemplateIN("bar/template.ftl:xx1.mc1", model))
      assertResult("Hello Big Joe") { r.get }
    }

    "not fail if template not found" in {
      when(m.fileService.getByPath("bar/template.ftl", "xx1.mc1")).thenReturn(None)
      val r = m.service.freemarkerTemplate(FreemarkerTemplateIN("bar/template.ftl:xx1.mc1", Map.empty))
      assertResult(None) { r }
    }
  }

  "velocityTemplate" should {
    "process template" in {
      m.registerFile(11, "bar/template.vm", "xx1.mc1", "Hello ${username}")
      val model = Map("username" -> "Big Joe")
      val r = m.service.velocityTemplate(VelocityTemplateIN("bar/template.vm:xx1.mc1", model))
      assertResult("Hello Big Joe") { r.get }
    }

    "not fail if template not found" in {
      when(m.fileService.getByPath("bar/template.vm", "xx1.mc1")).thenReturn(None)
      val r = m.service.velocityTemplate(VelocityTemplateIN("bar/template.vm:xx1.mc1", Map.empty))
      assertResult(None) { r }
    }
  }

}
