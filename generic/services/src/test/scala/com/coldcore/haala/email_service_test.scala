package com.coldcore.haala
package service

import javax.mail.internet.MimeMessage
import javax.mail.Message
import org.scalatest.{BeforeAndAfter, FlatSpec, Matchers}
import org.scalatest.mockito.MockitoSugar
import org.scalatest.junit.JUnitRunner
import org.mockito.Mockito._
import org.mockito.ArgumentCaptor
import com.coldcore.email.{OutMessage, Sender}
import com.coldcore.haala.core.Timestamp
import domain.User
import core.SiteChain.Implicits._
import org.junit.runner.RunWith

@RunWith(classOf[JUnitRunner])
class EmailServiceSpec extends FlatSpec with MockitoSugar with BeforeAndAfter with Matchers {
  var m: MyMock = _

  before {
    m = new MyMock
  }

  class MyMock extends com.coldcore.haala.service.test.MyMock {
    val sender = mock[Sender]
    val service = new EmailServiceImpl |< { x =>
      x.serviceContainer = serviceContainer
      x.mailSender = sender
    }

    when(m.settingService.getDefaultSite).thenReturn("xx1")
    when(m.settingService.query("Brand", "mh1")).thenReturn("web=Holiday-Estates.eu")
    when(m.settingService.query("SupportEmail", "mh1")).thenReturn("support@my-estate.eu")
  }

  "toSupport" should "email support with user as sender" in {
    when(m.labelService.query("email.supsu", "xx1")).thenReturn("Message from {BrandWeb}")

    m.service.toSupport("you@domain.com", "Hello there", "mh1", Nil)
    
    val arg = ArgumentCaptor.forClass(classOf[OutMessage])
    verify(m.sender).sendMessage(arg.capture)
    val msg = arg.getValue.createMessage(null).asInstanceOf[MimeMessage]
    msg.getFrom.head.toString shouldBe "you@domain.com"
    msg.getRecipients(Message.RecipientType.TO).head.toString shouldBe "support@my-estate.eu"
    msg.getContent.toString shouldBe "Hello there"
    msg.getSubject shouldBe "Message from Holiday-Estates.eu"
  }

  "fromSupport" should "email recipient with support as sender" in {
    when(m.labelService.query("email.supsu", "mh1")).thenReturn("Message from {BrandWeb}")

    m.service.fromSupport("you@domain.com", "Hello there", "mh1", Nil)

    val arg = ArgumentCaptor.forClass(classOf[OutMessage])
    verify(m.sender).sendMessage(arg.capture)
    val msg = arg.getValue.createMessage(null).asInstanceOf[MimeMessage]
    msg.getFrom.head.toString shouldBe "support@my-estate.eu"
    msg.getRecipients(Message.RecipientType.TO).head.toString shouldBe "you@domain.com"
    msg.getContent.toString shouldBe "Hello there"
    msg.getSubject shouldBe "Message from Holiday-Estates.eu"
  }

  "remindPassword" should "email password or activation link to a user" in {
    val user = new User { ref = "MC003"; email = "my.user@local.cc"; username = "myuser"; password = "myPass"; activated = Timestamp() }
    when(m.userService.getByUsernameOrEmail("myuser")).thenReturn(Some(user))
    when(m.userService.getByUsernameOrEmail("my.user@local.cc")).thenReturn(Some(user))
    when(m.settingService.query("MainURL", "mh1")).thenReturn("http://my-estate.eu")
    when(m.labelService.query("email.acttx", "mh1")).thenReturn("Confirm by clicking {link}, then sign in with your username {username} and password {password}")
    when(m.labelService.query("email.remtx", "mh1")).thenReturn("Your username is {username} and password is {password}")
    when(m.labelService.query("email.supsu", "mh1")).thenReturn("Message from {BrandWeb}")


    //activated user, by username
    m.service.remindPassword("myuser", "mh1")
    var arg = ArgumentCaptor.forClass(classOf[OutMessage])
    verify(m.sender).sendMessage(arg.capture)
    var msg = arg.getValue.createMessage(null).asInstanceOf[MimeMessage]
    msg.getFrom.head.toString shouldBe "support@my-estate.eu"
    msg.getRecipients(Message.RecipientType.TO).head.toString shouldBe "my.user@local.cc"
    msg.getContent.toString shouldBe "Your username is myuser and password is myPass"
    msg.getSubject shouldBe "Message from Holiday-Estates.eu"

    //activated user, by email
    reset(m.sender)
    m.service.remindPassword("my.user@local.cc", "mh1")
    arg = ArgumentCaptor.forClass(classOf[OutMessage])
    verify(m.sender).sendMessage(arg.capture)
    msg = arg.getValue.createMessage(null).asInstanceOf[MimeMessage]
    msg.getRecipients(Message.RecipientType.TO).head.toString shouldBe "my.user@local.cc"
    msg.getContent.toString shouldBe "Your username is myuser and password is myPass"

    //not activated user, by username
    reset(m.sender)
    user.activated = null
    m.service.remindPassword("myuser", "mh1")
    arg = ArgumentCaptor.forClass(classOf[OutMessage])
    verify(m.sender).sendMessage(arg.capture)
    msg = arg.getValue.createMessage(null).asInstanceOf[MimeMessage]
    msg.getContent.toString should startWith ("Confirm by clicking http://my-estate.eu/act.htm?ref=MC003&")
    msg.getContent.toString should endWith (", then sign in with your username myuser and password myPass")
  }

  "reportError" should "email errors to support with system user as sender" in {
    when(m.userService.getSystemUser).thenReturn(new User { email = "system@local.cc" })
    when(m.settingService.getDefaultSite).thenReturn("xx1")
    when(m.settingService.query("SupportEmail", "xx1")).thenReturn("support@my-estate.eu")
    when(m.labelService.query("email.errsu", "xx1")).thenReturn("Error on {BrandWeb}")
    when(m.settingService.query("Brand", "xx1")).thenReturn("web=example.org")

    m.service.reportError("caused by: void")

    val arg = ArgumentCaptor.forClass(classOf[OutMessage])
    verify(m.sender).sendMessage(arg.capture)
    val msg = arg.getValue.createMessage(null).asInstanceOf[MimeMessage]
    msg.getFrom.head.toString shouldBe "system@local.cc"
    msg.getRecipients(Message.RecipientType.TO).head.toString shouldBe "support@my-estate.eu"
    msg.getContent.toString shouldBe "caused by: void"
    msg.getSubject shouldBe "Error on example.org"
  }

}
