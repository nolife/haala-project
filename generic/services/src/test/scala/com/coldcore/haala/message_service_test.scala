package com.coldcore.haala
package service

import core.Meta
import core.Constants._
import MessageService.CreateIN
import com.coldcore.haala.service.SolrService.ValueTerm
import org.scalatest.{BeforeAndAfter, FlatSpec, Matchers}
import org.scalatest.mockito.MockitoSugar
import org.mockito.Mockito._
import org.mockito.ArgumentMatchers._
import domain.{AbyssEntry, User}
import org.junit.runner.RunWith
import org.scalatest.junit.JUnitRunner

@RunWith(classOf[JUnitRunner])
class MessageServiceSpec extends FlatSpec with MockitoSugar with BeforeAndAfter with Matchers {
  var m: MyMock = _

  before {
    m = new MyMock
  }

  class MyMock extends com.coldcore.haala.service.test.MyMock {
    val service = new MessageServiceImpl { serviceContainer = m.serviceContainer }

    when(settingService.querySystem("MessageConf")).thenReturn(setting_messageConf)
    when(settingService.querySystem("SystemParams")).thenReturn("batch=2")
  }

  private val setting_messageConf = """
    |{
    |   "keep": "30d"
    |}
  """.stripMargin.trim

  "create" should "create new message entry" in {
    when(m.userService.getById(2)).thenReturn(Some(new User { id = 2L; ref = "FOO"; username = "tester" }))
    doAnswer { invocation => // create message abyss
      val x = invocation.getArguments.head.asInstanceOf[AbyssService.CreateIN]
      new AbyssEntry { id = 4L; value = x.vm.value; meta = x.vm.meta }
    }.when(m.abyssService).create(any[AbyssService.CreateIN])

    val r = m.service.create(2, CreateIN(MESSAGE_TYPE_FEEDBACK, "sample text", "prop=a"))

    (r.abyss.id, r.user.username, r.abyss.value, r.abyss.meta) shouldBe (
      4, "tester", "sample text", "prop=a;s_fwd=y;s_typ=msg;l_usr=2;s_red=n;i_mtp=1;s_sub=1;")
  }

  "clean" should "remove user read messages" in {
    val (msg2A, msg2B, msg2C, msg2D, msg2E, msg2F) = (
      new AbyssEntry { id = 21L; meta = "s_typ=msg;l_usr=2;s_red=n;i_mtp=1;s_sub=1;" },
      new AbyssEntry { id = 22L; meta = "s_typ=msg;l_usr=2;s_red=y;i_mtp=1;s_sub=1;" },
      new AbyssEntry { id = 23L; meta = "s_typ=msg;l_usr=2;s_red=y;i_mtp=1;s_sub=1;" },
      new AbyssEntry { id = 24L; meta = "s_typ=msg;l_usr=2;s_red=y;i_mtp=1;s_sub=1;" },
      new AbyssEntry { id = 25L; meta = "s_typ=msg;l_usr=2;s_red=y;i_mtp=1;s_sub=1;" },
      new AbyssEntry { id = 26L; meta = "s_typ=msg;l_usr=2;s_red=y;i_mtp=1;s_sub=1;" })
    val msgs = List(msg2A, msg2B, msg2C, msg2D, msg2E, msg2F)

    when(m.userService.getById(2)).thenReturn(Some(new User { id = 2L; ref = "FOO"; username = "tester" }))

    when(m.abyssService.getById(21)).thenReturn(Some(msg2A))
    when(m.abyssService.getById(22)).thenReturn(Some(msg2B))
    when(m.abyssService.getById(23)).thenReturn(Some(msg2C))
    when(m.abyssService.getById(24)).thenReturn(Some(msg2D))
    when(m.abyssService.getById(25)).thenReturn(Some(msg2E))
    when(m.abyssService.getById(26)).thenReturn(Some(msg2F))

    when(m.abyssService.search(new AbyssService.SearchInputX(0, 2, "") { // search
      override val dynamicTerms = ValueTerm("s_typ", "msg") :: ValueTerm("l_usr", 2) :: ValueTerm("s_red", "y") :: Nil
    })).thenAnswer { _ =>
      val xs = msgs.filter(x => Meta(x.meta)("s_red", "") == "y").filterNot(_.value == "deleted").take(2)
      SearchResult(xs.size, xs)
    }
    when(m.abyssService.deleteNow(any[List[Long]])).thenAnswer { invocation => // delete
      val ids = invocation.getArguments.head.asInstanceOf[List[Long]]
      ids.flatMap(id => msgs.find(_.id == id)).filterNot(_.value == "deleted").foreach(_.value = "deleted")
    }

    m.service.clean(2)

    verify(m.abyssService).deleteNow(22L :: 23L :: Nil)
    verify(m.abyssService).deleteNow(24L :: 25L :: Nil)
    verify(m.abyssService).deleteNow(26L :: Nil)
    verify(m.abyssService, times(0)).deleteNow(21L :: Nil)
  }

}
