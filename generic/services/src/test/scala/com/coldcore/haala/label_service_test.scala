package com.coldcore.haala
package service

import org.scalatest.{BeforeAndAfter, FlatSpec, Matchers}
import exceptions.{ObjectExistsException, ObjectNotFoundException}
import LabelService._
import domain.Label
import core.SiteChain.Implicits._
import core.VirtualPath.Implicits._
import org.scalatest.mockito.MockitoSugar
import org.mockito.Mockito._
import org.mockito.ArgumentMatchers._
import dao.GenericDAO
import org.junit.runner.RunWith
import org.mockito.ArgumentCaptor
import org.mockito.stubbing.Answer
import org.mockito.invocation.InvocationOnMock
import org.scalatest.junit.JUnitRunner

@RunWith(classOf[JUnitRunner])
class LabelServiceSpec extends FlatSpec with MockitoSugar with BeforeAndAfter with Matchers {
  var m: MyMock = _

  before {
    m = new MyMock
  }

  class MyMock extends com.coldcore.haala.service.test.MyMock {
    val (labelDao, labelsCache) = (mock[GenericDAO[Label]], mock[DomainCache[Label]])
    val service = new LabelServiceImpl |< { x =>
      x.serviceContainer = serviceContainer
      x.labelDao = labelDao; x.labelsCache = labelsCache
    }
  }

  it should "query by key and site" in {
    m.service.labelsCache = new GuavaDomainCache[Label] |< { x => x.serviceContainer = m.serviceContainer; x.afterPropertiesSet }
    when(m.labelDao.findByProperty("key", "key")).thenReturn(
      new Label { id = 1L; key = "key"; value = "foo"; site = "xx2" } ::
        new Label { id = 2L; key = "key"; value = "val"; site = "xx1" } ::
        new Label { id = 3L; key = "key"; value = "bar"; site = "zz1" } :: Nil)
    assertResult("val") { m.service.query("key", "xx1") }
    assertResult("foo") { m.service.query("key", "xx1.xx2") }
    assertResult(null) { m.service.query("key", "unk1") }

    when(m.labelDao.findByProperty("key", "unknown")).thenReturn(Nil)
    assertResult(null) { m.service.query("unknown", "xx1") }
  }

  it should "query by key" in {
    m.service.labelsCache = new GuavaDomainCache[Label] |< { x => x.serviceContainer = m.serviceContainer; x.afterPropertiesSet }
    when(m.labelDao.findByProperty("key", "key")).thenReturn(
      new Label { id = 1L; key = "key"; value = "val"; site = "xx1" } :: Nil)
    assertResult("val") { m.service.query("key").head.value }

    when(m.labelDao.findByProperty("key", "unknown")).thenReturn(Nil)
    assertResult(0) { m.service.query("unknown").size }
  }

  it should "get by partial key and site" in {
    m.service.labelsCache = new GuavaDomainCache[Label] |< { x => x.serviceContainer = m.serviceContainer; x.afterPropertiesSet }
    when(m.labelDao.find(anyString, any)).thenReturn(
      new Label { id = 1L; key = "KeyLike_2"; value = "val_2"; site = "xx1" } ::
        new Label { id = 2L; key = "KeyLike_3"; value = "val_3"; site = "xx1" } :: Nil)
    assertResult(Map("KeyLike_2" -> "val_2", "KeyLike_3" -> "val_3")) { m.service.getForSiteLike("KeyLike%", "xx1") }
    assertResult(Map.empty) { m.service.getForSiteLike("KeyLike%", "sys") }
  }

  it should "get by partial key" in {
    m.service.labelsCache = new GuavaDomainCache[Label] |< { x => x.serviceContainer = m.serviceContainer; x.afterPropertiesSet }
    when(m.labelDao.find(anyString, any)).thenReturn(
      new Label { id = 1L; key = "KeyLike_2"; value = "val_2"; site = "xx1" } ::
        new Label { id = 2L; key = "KeyLike_3"; value = "val_3"; site = "xx1" } :: Nil)
    assertResult("KeyLike_2") { m.service.getByKeyLike("KeyLike%").head.key }
    assertResult("KeyLike_3") { m.service.getByKeyLike("KeyLike%").last.key }

    when(m.labelDao.find(anyString, any)).thenReturn(Nil)
    assertResult(Nil) { m.service.getByKeyLike("Unknown%") }
  }

  it should "get by site" in {
    m.service.labelsCache = new GuavaDomainCache[Label] |< { x => x.serviceContainer = m.serviceContainer; x.afterPropertiesSet }
    when(m.labelDao.find(anyString, any)).thenReturn(
      new Label { id = 1L; key = "ForSite_4"; value = "val_4"; site = "gfs1" } ::
        new Label { id = 2L; key = "ForSite_5"; value = "val_5"; site = "gfs1" } :: Nil)
    assertResult(Map("ForSite_4" -> "val_4", "ForSite_5" -> "val_5")) { m.service.getForSite("gfs1") }
    assertResult(Map.empty) { m.service.getForSite("unk1") }
  }

  it should "delete" in {
    val x = new Label { id = 7L; key = "key" }
    when(m.labelDao.findById(7)).thenReturn(Some(x))
    m.service.delete(7)
    verify(m.labelDao).delete(x)
    verify(m.solrService).deleteByIds(any)
    verify(m.labelsCache).remove("key")
  }

  it should "delete by key and site" in {
    val x = new Label { id = 7L; key = "key" }
    when(m.labelDao.findUniqueByProperties(Map("key" -> "key", "site" -> "xx1"))).thenReturn(Some(new Label { id = 7L; key = "key" }))
    when(m.labelDao.findById(7)).thenReturn(Some(x))
    m.service.delete("key", "xx1")
    verify(m.labelDao).delete(x)
    verify(m.solrService).deleteByIds(any)
    verify(m.labelsCache).remove("key")
  }

  it should "delete by partial key" in {
    when(m.labelDao.find(anyString, any)).thenReturn(
      new Label { id = 7L; key = "Delete_7"; value = "val_7"; site = "xx1" } ::
        new Label { id = 8L; key = "Delete_8"; value = "val_8"; site = "xx1" } :: Nil)
    when(m.labelDao.findById(7)).thenReturn(Some(new Label { id = 7L; key = "Delete_7" }))
    when(m.labelDao.findById(8)).thenReturn(Some(new Label { id = 8L; key = "Delete_8" }))
    m.service.deleteByKeyLike("Delete%")
    verify(m.labelDao, times(2)).delete(any.asInstanceOf[Label])
    verify(m.solrService, times(2)).deleteByIds(any)
    verify(m.labelsCache).remove("Delete_7")
    verify(m.labelsCache).remove("Delete_8")
  }

  it should "create" in {
    when(m.labelDao.findUniqueByProperties(Map("key" -> "key", "site" -> "xx1"))).thenReturn(None)
    doAnswer(new Answer[Unit] {
      override def answer(invocation: InvocationOnMock) {
        val x = invocation.getArguments.head.asInstanceOf[Label]; x.id = 7L //assign ID to prepare Solr doc
      }
    }).when(m.labelDao).saveOrUpdate(any.asInstanceOf[Label])
    m.service.create(IN("key", "val", "xx1"))
    val arg = ArgumentCaptor.forClass(classOf[Label])
    verify(m.labelDao).saveOrUpdate(arg.capture)
    assertResult("val") { arg.getValue.value }
    verify(m.solrService).submit(any.asInstanceOf[SolrService.SubmitDoc])
    verify(m.labelsCache).remove("key")

    when(m.labelDao.findUniqueByProperties(Map("key" -> "key", "site" -> "xx1"))).thenReturn(Some(new Label))
    intercept[ObjectExistsException] { m.service.create(IN("key", "val", "xx1")) }
  }

  it should "update" in {
    when(m.labelDao.findById(1)).thenReturn(None)
    intercept[ObjectNotFoundException] { m.service.update(1, IN("key", "val", "xx1")) }

    when(m.labelDao.findById(1)).thenReturn(Some(new Label { id = 1L }))
    when(m.labelDao.findUniqueByProperties(Map("key" -> "key", "site" -> "xx1"))).thenReturn(Some(new Label { id = 2L }))
    intercept[ObjectExistsException] { m.service.update(1, IN("key", "val", "xx1")) }

    when(m.labelDao.findById(1)).thenReturn(Some(new Label { id = 1L; key = "key"; value = "foo"; site = "xx1" }))
    when(m.labelDao.findUniqueByProperties(Map("key" -> "key", "site" -> "xx1"))).thenReturn(Some(new Label { id = 1L }))
    m.service.update(1, IN("key", "val", "xx1"))
    val arg = ArgumentCaptor.forClass(classOf[Label])
    verify(m.labelDao).saveOrUpdate(arg.capture)
    assertResult("val") { arg.getValue.value }
    verify(m.solrService).submit(any.asInstanceOf[SolrService.SubmitDoc])
    verify(m.labelsCache).remove("key")
  }

  it should "write" in {
    m.service.labelsCache = new GuavaDomainCache[Label] |< { x => x.serviceContainer = m.serviceContainer; x.afterPropertiesSet }
    //update existing
    when(m.labelDao.findUniqueByProperties(Map("key" -> "key", "site" -> "xx1"))).thenReturn(Some(new Label { id = 2L }))
    when(m.labelDao.findById(2)).thenReturn(Some(new Label { id = 2L }))
    m.service.write("key", "foo", "xx1")
    val arg = ArgumentCaptor.forClass(classOf[Label])
    verify(m.labelDao).saveOrUpdate(arg.capture)
    assertResult("foo") { arg.getValue.value }
  }

  it should "prepare solr doc" in {
    assertResult(List('id, "1_0000000000000000002", 'o_typ, 1, 'ol_key, "UpdateMe", 'ol_sit, "xx1", 'text, "val")) {
      m.service.prepareSolrDoc(new Label{id = 2L; key = "UpdateMe"; value = "val"; site = "xx1"})
    }
  }

  it should "update solr" in {
    m.service.labelsCache = new GuavaDomainCache[Label] |< { x => x.serviceContainer = m.serviceContainer; x.afterPropertiesSet }
    when(m.labelDao.findUniqueByProperties(Map("key" -> "UpdateMe", "site" -> "xx1"))).thenReturn(Some(new Label { id = 2L }))
    when(m.labelDao.findById(2)).thenReturn(Some(new Label { id = 2L }))
    m.service.write("UpdateMe", "updated", "xx1")
    verify(m.solrService).submit(List('id, "1_0000000000000000002", 'o_typ, 1, 'ol_key, "UpdateMe", 'ol_sit, "xx1", 'text, "updated"))
  }

  it should "search" in {
    m.service.labelsCache = new GuavaDomainCache[Label] |< { x => x.serviceContainer = m.serviceContainer; x.afterPropertiesSet }
    val solrResult = SearchResult[SolrService.ResultDoc](2, List(Map('id -> 11), Map('id -> 12)))
    when(m.solrService.search(anyString, any)).thenReturn(solrResult)
    when(m.solrService.fetchObjects(solrResult, m.labelDao, classOf[Label])).thenReturn(
      SearchResult(2,
        new Label { id = 11L; key = "SearchMe_1"; value = "val_1"; site = "xx1" } ::
          new Label { id = 12L; key = "SearchMe_2"; value = "val_2"; site = "xx1" } :: Nil))
    val result = m.service.search(new SearchInputX(0,10,"id") {
      override val filterValue: String = "SearchMe"
    })
    assertResult(2) { result.total }
    assertResult("SearchMe_1") { result.objects.head.key }
    assertResult("SearchMe_2") { result.objects.last.key }
  }

}
