package com.coldcore.haala
package service

import org.scalatest.{BeforeAndAfter, FlatSpec, Matchers}
import org.scalatest.mockito.MockitoSugar
import org.mockito.Mockito._
import org.mockito.ArgumentMatchers._
import org.mockito.ArgumentCaptor
import dao.GenericDAO
import com.coldcore.haala.domain.{Country, Location, Postcode}
import core.HttpUtil.HttpCallSuccessful
import com.coldcore.misc.scala.StringOp._
import org.junit.runner.RunWith
import org.scalatest.junit.JUnitRunner
import service.exceptions.ExternalFailException

@RunWith(classOf[JUnitRunner])
class LocationServiceSpec extends FlatSpec with MockitoSugar with BeforeAndAfter with Matchers {
  var m: MyMock = _

  before {
    m = new MyMock
  }

  class MyMock extends com.coldcore.haala.service.test.MyMock {
    val (locationDao, countryDao, countryCache, postcodeCache) =
      (mock[GenericDAO[Location]], mock[GenericDAO[Country]], mock[DomainCache[Country]], mock[DomainCache[Postcode]])
    val service = new LocationServiceImpl {
      override val staticBlock = m.staticBlock
      serviceContainer = m.serviceContainer
    } |< { x =>
      x.locationDao = locationDao; x.countryDao = countryDao
      x.countryCache = countryCache; x.postcodeCache = postcodeCache
    }
    when(locationDao.findById(818)).thenReturn(Some(england))
    when(locationDao.findById(1265)).thenReturn(Some(westCountry))
    when(locationDao.findById(1402)).thenReturn(Some(islesOfScilly))
    when(locationDao.findById(1403)).thenReturn(Some(hugnTown))
  }

  private val england = new Location{ id = 818; `type` = Location.TYPE_COUNTRY }
  private val westCountry = new Location{ id = 1265; `type` = Location.TYPE_REGION; parent = england }
  private val islesOfScilly = new Location{ id = 1402; `type` = Location.TYPE_SUB_REGION; parent = westCountry }
  private val hugnTown = new Location{ id = 1403; `type` = Location.TYPE_TOWN; parent = islesOfScilly }

  england.items.add(westCountry)
  westCountry.items.add(islesOfScilly)
  islesOfScilly.items.add(hugnTown)

  /* http://developers.google.com/maps/documentation/geocoding */
  val geocodeRequest1Address = "1600 Amphitheatre Parkway, Mountain View, CA"
  val geocodeRequest1 = "http://maps.googleapis.com/maps/api/geocode/json?address=1600+Amphitheatre+Parkway%2C+Mountain+View%2C+CA&sensor=false"
  val geocodeResponse1 = """
    {
       "results" : [
          {
             "address_components" : [
                {
                   "long_name" : "1600",
                   "short_name" : "1600",
                   "types" : [ "street_number" ]
                },
                {
                   "long_name" : "Amphitheatre Pkwy",
                   "short_name" : "Amphitheatre Pkwy",
                   "types" : [ "route" ]
                },
                {
                   "long_name" : "Mountain View",
                   "short_name" : "Mountain View",
                   "types" : [ "locality", "political" ]
                },
                {
                   "long_name" : "Santa Clara",
                   "short_name" : "Santa Clara",
                   "types" : [ "administrative_area_level_2", "political" ]
                },
                {
                   "long_name" : "California",
                   "short_name" : "CA",
                   "types" : [ "administrative_area_level_1", "political" ]
                },
                {
                   "long_name" : "United States",
                   "short_name" : "US",
                   "types" : [ "country", "political" ]
                },
                {
                   "long_name" : "94043",
                   "short_name" : "94043",
                   "types" : [ "postal_code" ]
                }
             ],
             "formatted_address" : "1600 Amphitheatre Pkwy, Mountain View, CA 94043, USA",
             "geometry" : {
                "location" : {
                   "lat" : 37.42291810,
                   "lng" : -122.08542120
                },
                "location_type" : "ROOFTOP",
                "viewport" : {
                   "northeast" : {
                      "lat" : 37.42426708029149,
                      "lng" : -122.0840722197085
                   },
                   "southwest" : {
                      "lat" : 37.42156911970850,
                      "lng" : -122.0867701802915
                   }
                }
             },
             "types" : [ "street_address" ]
          }
       ],
       "status" : "OK"
    }
    """

  val geocodeResponse1NoResults = """
    {
       "results" : [ ],
       "status" : "ZERO_RESULTS"
    }
    """

  it should "parent chain" in {
    assertResult(List(818)) { m.service.parentChain(818).map(_.id).toList } //country
    assertResult(List(818, 1265)) { m.service.parentChain(1265).map(_.id).toList } //region
    assertResult(List(818, 1265, 1402)) { m.service.parentChain(1402).map(_.id).toList } //sub-region
    assertResult(List(818, 1265, 1402, 1403)) { m.service.parentChain(1403).map(_.id).toList } //town
  }

  it should "get real countries" in {
    when(m.countryCache.get(anyString)).thenReturn(None)
    when(m.countryDao.findAll).thenReturn(new Country { id = 1; code = "UK" } :: Nil)
    assertResult("UK") { m.service.getRealCountries.head.code }
  }

  it should "get real country by code" in {
    when(m.countryCache.get(anyString)).thenReturn(None)
    when(m.countryDao.findAll).thenReturn(new Country { id = 1; code = "UK" } :: Nil)
    assertResult(None) { m.service.getRealCountryByCode("FOO") }
    assertResult("UK") { m.service.getRealCountryByCode("UK").get.code }
  }

  it should "geocode" in {
    when(m.staticBlock.httpGet(geocodeRequest1)).thenReturn(HttpCallSuccessful(geocodeRequest1, 200, geocodeResponse1, List.empty))
    val result = m.service.geocode(geocodeRequest1Address)
    assertResult("37.4229181000000000 -122.0854212000000000") { toXLatLng(result.lat)+" "+toXLatLng(result.lng) }
    assertResult(1) { result.`type` }
    assertResult(Map('lat -> "37.4229181000000000", 'lng -> "-122.0854212000000000", 'type -> 1)) { result.serializeAsMap }
  }

  it should "geocode with no results" in {
    when(m.staticBlock.httpGet(geocodeRequest1)).thenReturn(HttpCallSuccessful(geocodeRequest1, 200, geocodeResponse1NoResults, List.empty))
    intercept[ExternalFailException] { m.service.geocode(geocodeRequest1Address) }
  }

}
