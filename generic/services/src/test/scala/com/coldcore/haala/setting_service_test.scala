package com.coldcore.haala
package service

import org.scalatest.{BeforeAndAfter, FlatSpec, Matchers}
import exceptions.{ObjectExistsException, ObjectNotFoundException}
import SettingService._
import core.SiteChain.Implicits._
import domain.Setting
import org.scalatest.mockito.MockitoSugar
import org.mockito.Mockito._
import org.mockito.ArgumentMatchers._
import dao.GenericDAO
import org.junit.runner.RunWith
import org.mockito.ArgumentCaptor
import org.scalatest.junit.JUnitRunner

@RunWith(classOf[JUnitRunner])
class SettingServiceSpec extends FlatSpec with MockitoSugar with BeforeAndAfter with Matchers {
  var m: MyMock = _

  before {
    m = new MyMock
  }

  class MyMock extends com.coldcore.haala.service.test.MyMock {
    val (settingDao, settingsCache) = (mock[GenericDAO[Setting]], mock[DomainCache[Setting]])
    val service = new SettingServiceImpl |< { x =>
      x.serviceContainer = serviceContainer
      x.settingDao = settingDao; x.settingsCache = settingsCache
    }
  }

  it should "query" in {
    m.service.settingsCache = new GuavaDomainCache[Setting] |< { x => x.serviceContainer = m.serviceContainer; x.afterPropertiesSet }
    when(m.settingDao.findByProperty("key", "key")).thenReturn(
      new Setting { id = 1L; key = "key"; value = "foo"; site = "xx2" } ::
        new Setting { id = 2L; key = "key"; value = "val"; site = "xx1" } ::
        new Setting { id = 3L; key = "key"; value = "bar"; site = "zz1" } :: Nil)
    assertResult("val") { m.service.query("key", "xx1") }

    //cached
    assertResult("foo") { m.service.query("key", "xx2") }
    assertResult("foo") { m.service.query("key", "xx1.xx2") }
    assertResult(null) { m.service.query("key", "unk1") }

    when(m.settingDao.findByProperty("key", "unknown")).thenReturn(Nil)
    assertResult(null) { m.service.query("unknown", "xx1") }
  }

  it should "query (pub site)" in {
    m.service.settingsCache = new GuavaDomainCache[Setting] |< { x => x.serviceContainer = m.serviceContainer; x.afterPropertiesSet }
    when(m.settingDao.findByProperty("key", "key")).thenReturn(
      new Setting { id = 1L; key = "key"; value = "foo"; site = "pub" } ::
        new Setting { id = 2L; key = "key"; value = "bar"; site = "xx1" } ::
        new Setting { id = 3L; key = "key"; value = "val"; site = "xx2" } :: Nil)
    assertResult("foo") { m.service.query("key", "zz1") }

    //cached
    assertResult("foo") { m.service.query("key", "zz1.zz2") }
    assertResult("bar") { m.service.query("key", "zz1.xx1") }
    assertResult("val") { m.service.query("key", "zz1.xx1.xx2") }
    assertResult("bar") { m.service.query("key", "xx2.xx1") }
  }

  it should "query system" in {
    m.service.settingsCache = new GuavaDomainCache[Setting] |< { x => x.serviceContainer = m.serviceContainer; x.afterPropertiesSet }
    when(m.settingDao.findByProperty("key", "key")).thenReturn(
      new Setting { id = 1L; key = "key"; value = "val"; site = "xx1" } ::
        new Setting { id = 2L; key = "key"; value = "bar"; site = "sys" } :: Nil)
    assertResult("bar") { m.service.querySystem("key") }

    when(m.settingDao.findByProperty("key", "unknown")).thenReturn(Nil)
    assertResult(null) { m.service.querySystem("unknown") }
  }

  it should "get default site and currency" in {
    m.service.settingsCache = new GuavaDomainCache[Setting] |< { x => x.serviceContainer = m.serviceContainer; x.afterPropertiesSet }
    when(m.settingDao.findByProperty("key", "Sites")).thenReturn(
      new Setting { id = 1L; key = "Sites"; value = "xx1=xx1:en, xx2=xx1.xx2:ru|rs"; site = "sys" } :: Nil)
    when(m.settingDao.findByProperty("key", "Currencies")).thenReturn(
      new Setting { id = 2L; key = "Currencies"; value = "GBP, EUR, USD"; site = "sys" } :: Nil)
    assertResult("xx1") { m.service.getDefaultSite }
    assertResult("GBP") { m.service.getDefaultCurrency }
  }

  it should "get by partial key and site" in {
    m.service.settingsCache = new GuavaDomainCache[Setting] |< { x => x.serviceContainer = m.serviceContainer; x.afterPropertiesSet }
    when(m.settingDao.find(anyString, any)).thenReturn(
      new Setting { id = 1L; key = "KeyP"; value = "car"; site = "pub" } ::
        new Setting { id = 2L; key = "KeyA"; value = "val"; site = "xx1" } ::
        new Setting { id = 3L; key = "KeyA"; value = "dar"; site = "xx2" } ::
        new Setting { id = 4L; key = "KeyB"; value = "foo"; site = "yy1" } ::
        new Setting { id = 5L; key = "KeyC"; value = "bar"; site = "xx1" } :: Nil)
    assertResult(Map("KeyP" -> "car", "KeyA" -> "dar", "KeyC" -> "bar")) { m.service.getForSiteLike("Key%", "xx1.xx2") }

    when(m.settingDao.find(anyString, any)).thenReturn(Nil)
    assertResult(Map.empty) { m.service.getForSiteLike("Key%", "xx1") }
  }

  it should "get items per page" in {
    m.service.settingsCache = new GuavaDomainCache[Setting] |< { x => x.serviceContainer = m.serviceContainer; x.afterPropertiesSet }
    when(m.settingDao.findByProperty("key", "ItemsPerPage")).thenReturn(
      new Setting { id = 1L; key = "ItemsPerPage"; value = "def=10, my=15"; site = "sys" } ::
        new Setting { id = 2L; key = "ItemsPerPage"; value = "def=20, fav=5"; site = "mh1" } ::
        new Setting { id = 3L; key = "ItemsPerPage"; value = "fav=5"; site = "mc1" } :: Nil)
    assertResult(10) { m.service.getItemsPerPage("unknown") }
    assertResult(10) { m.service.getItemsPerPage("fav") }
    assertResult(15) { m.service.getItemsPerPage("my") }
    assertResult(20) { m.service.getItemsPerPage("unknown", "mh1") }
    assertResult(5) { m.service.getItemsPerPage("fav", "mh1") }
    assertResult(20) { m.service.getItemsPerPage("my", "mh1") }
    assertResult(10) { m.service.getItemsPerPage("unknown", "mc1") }
    assertResult(5) { m.service.getItemsPerPage("fav", "mc1") }
    assertResult(15) { m.service.getItemsPerPage("my", "mc1") }
  }

  it should "delete" in {
    val x = new Setting { id = 7L; key = "key" }
    when(m.settingDao.findById(7)).thenReturn(Some(x))
    m.service.delete(7)
    verify(m.settingDao).delete(x)
    verify(m.settingsCache).remove("key")
  }

  it should "create" in {
    when(m.settingDao.findUniqueByProperties(Map("key" -> "key", "site" -> "xx1"))).thenReturn(None)
    m.service.create(IN("key", "val", "xx1"))
    val arg = ArgumentCaptor.forClass(classOf[Setting])
    verify(m.settingDao).saveOrUpdate(arg.capture)
    assertResult("val") { arg.getValue.value }
    verify(m.settingsCache).remove("key")

    when(m.settingDao.findUniqueByProperties(Map("key" -> "key", "site" -> "xx1"))).thenReturn(Some(new Setting))
    intercept[ObjectExistsException] { m.service.create(IN("key", "val", "xx1")) }
  }

  it should "update" in {
    when(m.settingDao.findById(1)).thenReturn(None)
    intercept[ObjectNotFoundException] { m.service.update(1, IN("key", "val", "xx1")) }

    when(m.settingDao.findById(1)).thenReturn(Some(new Setting { id = 1L }))
    when(m.settingDao.findUniqueByProperties(Map("key" -> "key", "site" -> "xx1"))).thenReturn(Some(new Setting { id = 2L }))
    intercept[ObjectExistsException] { m.service.update(1, IN("key", "val", "xx1")) }

    when(m.settingDao.findById(1)).thenReturn(Some(new Setting { id = 1L; key = "key"; value = "foo"; site = "xx1" }))
    when(m.settingDao.findUniqueByProperties(Map("key" -> "key", "site" -> "xx1"))).thenReturn(Some(new Setting { id = 1L }))
    m.service.update(1, IN("key", "val", "xx1"))
    val arg = ArgumentCaptor.forClass(classOf[Setting])
    verify(m.settingDao).saveOrUpdate(arg.capture)
    assertResult("val") { arg.getValue.value }
    verify(m.settingsCache).remove("key")
  }

  it should "write new setting" in {
    m.service.settingsCache = new GuavaDomainCache[Setting] |< { x => x.serviceContainer = m.serviceContainer; x.afterPropertiesSet }
    //insert new
    when(m.settingDao.findUniqueByProperties(Map("key" -> "key", "site" -> "xx1"))).thenReturn(None)
    m.service.write("key", "foo", "xx1")
    val arg = ArgumentCaptor.forClass(classOf[Setting])
    verify(m.settingDao).saveOrUpdate(arg.capture)
    assertResult("foo") { arg.getValue.value }
  }

  it should "overwrite existing setting" in {
    m.service.settingsCache = new GuavaDomainCache[Setting] |< { x => x.serviceContainer = m.serviceContainer; x.afterPropertiesSet }
    //updated existing
    when(m.settingDao.findUniqueByProperties(Map("key" -> "key", "site" -> "xx1"))).thenReturn(
      Some(new Setting { id = 1L; key = "key"; value = "foo"; site = "xx1" }))
    when(m.settingDao.findById(1)).thenReturn(Some(new Setting { id = 1L }))
    m.service.write("key", "bar", "xx1")
    val arg = ArgumentCaptor.forClass(classOf[Setting])
    verify(m.settingDao).saveOrUpdate(arg.capture)
    assertResult("bar") { arg.getValue.value }
  }

  it should "search" in {
    m.service.settingsCache = new GuavaDomainCache[Setting] |< { x => x.serviceContainer = m.serviceContainer; x.afterPropertiesSet }
    when(m.settingDao.iterate(anyString, any)).thenReturn(
      (new Setting { id = 1L; key = "SomeKeyA"; value = "foo"; site = "xx2" } ::
        new Setting { id = 2L; key = "SomeKeyA"; value = "val"; site = "xx1" } ::
        new Setting { id = 2L; key = "Home"; value = "val"; site = "xx1" } ::
        new Setting { id = 2L; key = "Dome"; value = "val"; site = "xx1" } ::
        new Setting { id = 3L; key = "KeyB"; value = "bar"; site = "zz1" } :: Nil).iterator)
    val result = m.service.search(new SearchInputX(0,10,"id") {
      override val filterKey: String = "key"
    })
    assertResult(3) { result.total }
    assertResult("foo" :: "val" :: "bar" :: Nil) { result.objects.map(_.value) }
  }

  it should "search and remove duplicates" in {
    m.service.settingsCache = new GuavaDomainCache[Setting] |< { x => x.serviceContainer = m.serviceContainer; x.afterPropertiesSet }
    when(m.settingDao.iterate(anyString, any)).thenReturn(
      (new Setting { id = 1L; key = "KeyA"; value = "foo"; site = "xx2" } ::
        new Setting { id = 2L; key = "KeyA"; value = "val"; site = "xx1" } ::
        new Setting { id = 2L; key = "KeyA"; value = "val"; site = "xx1" } ::
        new Setting { id = 3L; key = "KeyB"; value = "bar"; site = "zz1" } ::
        new Setting { id = 2L; key = "KeyA"; value = "val"; site = "xx1" } :: Nil).iterator)
    val result = m.service.search(new SearchInputX(0,10,"id") {
      override val filterKey: String = "key"
    })
    assertResult(3) { result.total }
    assertResult("foo" :: "val" :: "bar" :: Nil) { result.objects.map(_.value) }
  }

  it should "query constant" in {
    m.service.settingsCache = new GuavaDomainCache[Setting] |< { x => x.serviceContainer = m.serviceContainer; x.afterPropertiesSet }
    when(m.settingDao.findByProperty("key", "Constants")).thenReturn(
      new Setting { id = 1L; key = "Constants"; value = "estate.SOLR_TYPE_ESTATE=2, estate.SOLR_PREFIX_ESTATE=oe"; site = "sys" } :: Nil)
    assertResult(2) { m.service.queryConstant[Int]("Constants", "estate", "SOLR_TYPE_ESTATE") }
    assertResult("oe") { m.service.queryConstant[String]("Constants", "estate", "SOLR_PREFIX_ESTATE") }
  }

}
