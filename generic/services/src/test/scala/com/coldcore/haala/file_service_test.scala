package com.coldcore.haala
package service

import core.CoreException
import core.SiteChain.Implicits._
import core.VirtualPath.Implicits._
import org.scalatest.{BeforeAndAfter, FlatSpec, Matchers}
import service.exceptions.{ObjectExistsException, ObjectNotFoundException}
import service.FileService._
import domain.File
import org.scalatest.mockito.MockitoSugar
import org.mockito.Mockito._
import org.mockito.ArgumentMatchers._
import org.mockito.ArgumentCaptor
import dao.GenericDAO
import org.junit.runner.RunWith
import service.test.ByteBody
import org.mockito.stubbing.Answer
import org.mockito.invocation.InvocationOnMock
import org.scalatest.junit.JUnitRunner

@RunWith(classOf[JUnitRunner])
class FileServiceSpec extends FlatSpec with MockitoSugar with BeforeAndAfter with Matchers {
  var m: MyMock = _

  before {
    m = new MyMock
  }

  class MyMock extends com.coldcore.haala.service.test.MyMock {
    val (fileDao, bodyFact, filesCache) = (mock[GenericDAO[File]], mock[BodyFactory], mock[DomainCache[File]])
    val service = new FileServiceImpl { override lazy val bodyFactory = bodyFact } |< { x =>
      x.serviceContainer = serviceContainer; x.fileDao = fileDao; x.filesCache = filesCache
    }
    when(settingService.querySystem("FsPath")).thenReturn("/usr/haala_fs")
  }

  "body" should "return a body for a path" in {
    when(m.bodyFact.make(anyString)).thenReturn(new ByteBody(null))
    when(m.bodyFact.make("/tmp/bar.png")).thenReturn(
      new ByteBody("12345678900".getBytes) { xSaved = true; xPath = "/tmp/bar.png" })
    m.service.body.exists shouldBe false
    m.service.body("/tmp/foo.png").exists shouldBe false
    m.service.body("/tmp/bar.png").exists shouldBe true
  }

  "getBody" should "return a body for a file and suffix" in {
    val (file1, file2) = (new File { id = 1L }, new File { id = 2L })
    when(m.bodyFact.make(file1, "d")).thenReturn(new ByteBody(null))
    when(m.bodyFact.make(file2, "d")).thenReturn(
      new ByteBody("12345678900".getBytes) { xSaved = true; xPath = "/tmp/bar.png" })
    m.service.getBody(file1, "d").exists shouldBe false
    m.service.getBody(file2, "d").exists shouldBe true
  }

  "getByBody" should "return a file by its body" in {
    val (body1, body2) = (new ByteBody("12345678900".getBytes) { xSaved = true; xPath = "/fs/0.png" },
      new ByteBody("12345678900".getBytes) { xSaved = true; xPath = "/fs/a.png" })
    when(m.fileDao.findById(0)).thenReturn(None)
    when(m.fileDao.findById(10)).thenReturn(Some(new File { id = 10L; folder = "foo"; name = "bar.png"}))
    m.service.getByBody(body1) shouldBe empty
    m.service.getByBody(body2).get.getPath shouldBe "foo/bar.png"
  }

  "copy" should "copy a body into a new temp body" in {
    val body = new ByteBody("12345678900".getBytes) { xSaved = true; xPath = "/fs/a.png" }
    doAnswer(new Answer[Body] {
      override def answer(invocation: InvocationOnMock): Body = { //assign generated path
        new ByteBody(null) { xPath = invocation.getArguments.head.asInstanceOf[String] }
      }
    }).when(m.bodyFact).make(anyString)
    val temp = m.service.copy(body)
    temp.path should startWith ("/usr/haala_fs/tmp/")
    temp.path should endWith (".tmp")
  }

  "copyBody" should "copy a body for a file and suffic into a new temp body" in {
    val file = new File { id = 10L; folder = "foo"; name = "bar.png"}
    val body = new ByteBody("12345678900".getBytes) { xSaved = true; xPath = "/fs/a.png" }
    when(m.bodyFact.make(file, "d")).thenReturn(body)
    doAnswer(new Answer[Body] {
      override def answer(invocation: InvocationOnMock): Body = { //assign generated path
        new ByteBody(null) { xPath = invocation.getArguments.head.asInstanceOf[String] }
      }
    }).when(m.bodyFact).make(anyString)
    val temp = m.service.copyBody(file, "d")
    temp.path should startWith ("/usr/haala_fs/tmp/")
    temp.path should endWith (".tmp")
  }

  "attachBody" should "attach a body to a file for suffix" in {
    val file = new File { id = 10L; folder = "foo"; name = "bar.png"}
    val tempO = new ByteBody("qwertyuiopa".getBytes) { xSaved = true; xPath = "/tmp/barO.tmp" }
    val tempS = new ByteBody("qwertyuiopa345".getBytes) { xSaved = true; xPath = "/tmp/barS.tmp" }
    when(m.fileDao.findById(10)).thenReturn(Some(file))
    when(m.bodyFact.make(file, "o")).thenReturn(new ByteBody(null) { xPath = "/fs/a.o" })
    when(m.bodyFact.make(file, "2")).thenReturn(new ByteBody(null) { xPath = "/fs/a.2" })

    m.service.attachBody(10, "o", tempO)
    tempO.path shouldBe "/fs/a.o"
    file.meta shouldBe "sfx=o;"

    m.service.attachBody(10, "2", tempS)
    tempS.path shouldBe "/fs/a.2"
    file.meta shouldBe "sfx=o,2;"
  }

  "detachBody" should "detach a body from a file for suffix" in {
    val file = new File { id = 10L; folder = "foo"; name = "bar.png"; meta="sfx=o,2;"}
    val bodyO = new ByteBody("12345678900".getBytes) { xSaved = true; xPath = "/fs/a.o" }
    val bodyS = new ByteBody("12345678900345".getBytes) { xSaved = true; xPath = "/fs/a.2" }
    when(m.fileDao.findById(10)).thenReturn(Some(file))
    when(m.bodyFact.make(file, "o")).thenReturn(bodyO)
    when(m.bodyFact.make(file, "2")).thenReturn(bodyS)

    m.service.detachBody(10, "o")
    bodyO.xSaved shouldBe false
    file.meta shouldBe "sfx=2;"

    m.service.detachBody(10, "2")
    bodyS.xSaved shouldBe false
    file.meta shouldBe "" 
  }

  "detachBody" should "not detach a default body" in {
    val file = new File { id = 10L; folder = "foo"; name = "bar.png"}
    val body = new ByteBody("12345678900".getBytes) { xSaved = true; xPath = "/fs/a.d" }
    when(m.fileDao.findById(10)).thenReturn(Some(file))
    when(m.bodyFact.make(file, "d")).thenReturn(body)
    a [CoreException] should be thrownBy m.service.detachBody(10, "d")
    body.xSaved shouldBe true
  }

  "getByPath" should "return a file by its path" in {
    when(m.filesCache.get(anyString)).thenReturn(None)
    when(m.fileDao.findByProperties(Map("folder" -> "etc/foo", "name" -> "bar.png"))).thenReturn(
      new File { id = 3L; folder = "etc/foo"; name = "bar.png"; site = "xx1" } ::
        new File { id = 4L; folder = "etc/foo"; name = "bar.png"; site = "xx2" } :: Nil)
    m.service.getByPath("etc/foo/bar.png").map(_.id) should contain only (3, 4)
    m.service.getByPath("etc/foo/bar.png", "xx1").get.id shouldBe 3
    m.service.getByPath("etc/foo/bar.png", "unk") shouldBe empty
  }

  "getByFolder" should "return files by a folder and site chain" in {
    when(m.filesCache.get(anyString)).thenReturn(None)
    when(m.fileDao.find(anyString, any)).thenReturn(
      new File { id = 5L; folder = "etc/foo"; name = "car.png"; site = "xx1" } ::
        new File { id = 3L; folder = "etc/foo"; name = "bar.png"; site = "xx1" } ::
        new File { id = 4L; folder = "etc/foo"; name = "bar.png"; site = "xx2" } :: Nil)
    m.service.getByFolder("etc/foo", "xx1").map(_.id) should contain only (5, 3)
    m.service.getByFolder("etc/foo", "unk") shouldBe empty
  }

  "getByFolder" should "return files by a folder and sub folders" in {
    val fileA = new File { id = 7L; folder = "foo"; name = "bar.png"}
    val fileB = new File { id = 8L; folder = "foo/abc"; name = "car.png"}
    when(m.filesCache.get(anyString)).thenReturn(None)
    when(m.fileDao.find(
      "from " + classOf[File].getName + " o where o.folder = ? or o.folder like ?",
      "foo", "foo/%")).thenReturn(fileA :: fileB :: Nil)
    m.service.getByFolder("foo/*").map(_.id) should contain only (7, 8) // check if correct query used
  }

  "delete" should "delete a file" in {
    val file = new File { id = 7L; folder = "foo"; name = "bar.png"}
    when(m.fileDao.findById(7)).thenReturn(Some(file))
    m.service.delete(7)
    verify(m.fileDao).delete(file)
    verify(m.filesCache).remove("foo/bar.png")
  }

  "deleteByFolder" should "delete files by a folder and sub folders" in {
    val fileA = new File { id = 7L; folder = "foo"; name = "bar.png"}
    val fileB = new File { id = 8L; folder = "foo/abc"; name = "car.png"}
    when(m.filesCache.get(anyString)).thenReturn(None)
    when(m.fileDao.find(
      "from " + classOf[File].getName + " o where o.folder = ? or o.folder like ?",
      "foo", "foo/%")).thenReturn(fileA :: fileB :: Nil)
    m.service.deleteByFolder("foo/*")
    verify(m.fileDao).findById(7L)
    verify(m.fileDao).findById(8L)
  }

  "create" should "not create a new file if another file exists with the same path" in {
    val temp = new ByteBody("qwertyuiopa".getBytes) { xSaved = true; xPath = "/tmp/bar.tmp" }
    when(m.filesCache.get(anyString)).thenReturn(None)
    when(m.fileDao.findByProperties(Map("folder" -> "etc/foo", "name" -> "bar.png"))).thenReturn(
      new File { id = 3L; folder = "etc/foo"; name = "bar.png"; site = "xx1" } :: Nil)
    an [ObjectExistsException] should be thrownBy m.service.create(IN("etc/foo/bar.png", "xx1"), temp)
  }

  "create" should "not create a new file with default body" in {
    val temp = new ByteBody("qwertyuiopa".getBytes) { xSaved = true; xPath = "/tmp/bar.tmp" }
    when(m.filesCache.get(anyString)).thenReturn(None)
    when(m.fileDao.findByProperties(Map("folder" -> "etc/foo", "name" -> "bar.png"))).thenReturn(List.empty)
    when(m.bodyFact.make(any.asInstanceOf[File], anyString)).thenReturn(new ByteBody(null) { xPath = "/fs/a.d" })
    doAnswer(new Answer[Unit] {
      override def answer(invocation: InvocationOnMock) {
        val x = invocation.getArguments.head.asInstanceOf[File]; x.id = 7L //assign ID to prepare Solr doc
      }
    }).when(m.fileDao).saveOrUpdate(any.asInstanceOf[File])
    m.service.create(IN("etc/foo/bar.png", "xx1"), temp)
    val arg = ArgumentCaptor.forClass(classOf[File])
    verify(m.fileDao).saveOrUpdate(arg.capture)
    verify(m.filesCache).remove("etc/foo/bar.png")
    arg.getValue.getPath shouldBe "etc/foo/bar.png"
    temp.path shouldBe "/fs/a.d"
  }

  "update" should "not update a file if another file exists with the new path" in {
    when(m.filesCache.get(anyString)).thenReturn(None)
    when(m.fileDao.findByProperties(Map("folder" -> "etc/foo", "name" -> "bar.png"))).thenReturn(
      new File { id = 7L; folder = "etc/foo"; name = "bar.png"; site = "xx1" } :: Nil)
    when(m.fileDao.findById(0)).thenReturn(None)
    when(m.fileDao.findById(10)).thenReturn(Some(new File { id = 10L; folder = "foo"; name = "bar.png"}))
    an [ObjectExistsException] should be thrownBy m.service.update(10, IN("etc/foo/bar.png", "xx1"))
    an [ObjectNotFoundException] should be thrownBy m.service.update(0, IN("etc/foo/bar.png", "xx1"))
  }

  "update" should "update a file" in {
    when(m.filesCache.get(anyString)).thenReturn(None)
    when(m.fileDao.findByProperties(Map("folder" -> "etc/foo", "name" -> "bar.png"))).thenReturn(
      new File { id = 10L; folder = "foo"; name = "bar.png"; site = "xx1" } :: Nil)
    when(m.fileDao.findById(10)).thenReturn(Some(new File { id = 10L; folder = "foo"; name = "bar.png"}))
    m.service.update(10, IN("etc/foo/bar.png", "xx1")) // foo/bar.png -> etc/foo/bar.png
    val arg = ArgumentCaptor.forClass(classOf[File])
    verify(m.fileDao).saveOrUpdate(arg.capture)
    verify(m.filesCache).remove("etc/foo/bar.png")
    arg.getValue.getPath shouldBe "etc/foo/bar.png"
  }

  "update" should "update a file and default body" in {
    val temp = new ByteBody("qwertyuiopa".getBytes) { xSaved = true; xPath = "/tmp/bar.tmp" }
    when(m.filesCache.get(anyString)).thenReturn(None)
    when(m.fileDao.findByProperties(Map("folder" -> "etc/foo", "name" -> "bar.png"))).thenReturn(
      new File { id = 10L; folder = "foo"; name = "bar.png"; site = "xx1" } :: Nil)
    when(m.fileDao.findById(10)).thenReturn(Some(new File { id = 10L; folder = "foo"; name = "bar.png"}))
    when(m.bodyFact.make(any.asInstanceOf[File], anyString)).thenReturn(new ByteBody(null) { xPath = "/fs/a.d" })
    m.service.update(10, IN("etc/foo/bar.png", "xx1"), temp) // foo/bar.png -> etc/foo/bar.png (with a new body)
    val arg = ArgumentCaptor.forClass(classOf[File])
    verify(m.fileDao).saveOrUpdate(arg.capture)
    verify(m.filesCache).remove("etc/foo/bar.png")
    arg.getValue.getPath shouldBe "etc/foo/bar.png"
    temp.path shouldBe "/fs/a.d"
  }

}
