package com.coldcore.haala
package service

import domain.{File, Picture}
import org.scalatest.{BeforeAndAfter, FlatSpec}
import exceptions.InvalidFormatException
import PictureService.Fit
import com.coldcore.gfx.{Gfx, ImageType}
import java.io.ByteArrayOutputStream
import javax.imageio.ImageIO
import org.scalatest.mockito.MockitoSugar
import org.mockito.Mockito._
import org.mockito.ArgumentMatchers._
import org.mockito.ArgumentCaptor
import dao.GenericDAO
import org.junit.runner.RunWith
import service.test.ByteBody
import org.scalatest.junit.JUnitRunner
import core.VirtualPath.Implicits._

@RunWith(classOf[JUnitRunner])
class PictureServiceSpec extends FlatSpec with MockitoSugar with BeforeAndAfter {
  var m: MyMock = _

  before {
    m = new MyMock
  }

  class MyMock extends com.coldcore.haala.service.test.MyMock {
    val pictureDao = mock[GenericDAO[Picture]]
    val service = new PictureServiceImpl |< { x =>
      x.serviceContainer = serviceContainer
      x.pictureDao = pictureDao
    }
  }

  private def pngBytes(w: Int, h: Int): Array[Byte] = {
    val img = Gfx.createImage(w, h, ImageType.TYPE_INT_RGB)
    val out = new ByteArrayOutputStream
    ImageIO.write(img, "png", out)
    out.toByteArray
  }

  it should "delete a picture" in {
    val x = new Picture { id = 7L; file = new File { id = 10L }}
    when(m.pictureDao.findById(7)).thenReturn(Some(x))
    m.service.delete(7)
    verify(m.pictureDao).delete(x)
    verify(m.fileService).delete(10)
  }
  
  it should "create a picture" in {
    val o = m.service.create(3, 4, 200, 300)
    verify(m.pictureDao).saveOrUpdate(any.asInstanceOf[Picture])
    assertResult("200x300") { o.width+"x"+o.height }
  }

  it should "update a picture" in {
    val o = new Picture { id = 1L; width = 320; height = 200 }
    m.service.update(o, 10, 20)
    verify(m.pictureDao).saveOrUpdate(any.asInstanceOf[Picture])
    assertResult("10x20") { o.width+"x"+o.height }
  }

  it should "attach default and original bodies to a new file" in {
    val o = new Picture { id = 1L; width = 320; height = 200 }
    val bodyD = new ByteBody("qwertyuiopa".getBytes) { xSaved = true; xPath = "/tmp/bar.tmp" }
    val bodyO = new ByteBody("12345678901".getBytes) { xSaved = true; xPath = "/tmp/foo.tmp" }
    val file = new File { id = 10L; folder = "foo"; name = "bar.png"}
    when(m.fileService.create("attach/bar/foo.png", "xx1", bodyD)).thenReturn(file)
    m.service.attachBody(o, "attach/bar/foo.png", "xx1", bodyD, bodyO)
    verify(m.fileService).create("attach/bar/foo.png", "xx1", bodyD)
    verify(m.fileService).attachBody(10, "o", bodyO)
    verify(m.pictureDao).saveOrUpdate(any.asInstanceOf[Picture])
  }

  it should "reattach bodies with default and second suffixes" in {
    val o = new Picture { id = 1L; width = 320; height = 200
      file = new File { id = 10L; folder = "foo"; name = "bar.png" }}
    val bodyD = new ByteBody("qwertyuiopa".getBytes) { xSaved = true; xPath = "/tmp/bar.tmp" }
    val bodyS = new ByteBody("ABCDEFGHQWE".getBytes) { xSaved = true; xPath = "/tmp/mac.tmp" }
    m.service.reattachBody(o, bodyD, File.SUFFIX_DEFAULT)
    m.service.reattachBody(o, bodyS, "2")
    verify(m.fileService).attachBody(10, "d", bodyD)
    verify(m.fileService).attachBody(10, "2", bodyS)
  }

  "imageSize" should "convert to an image and measure it" in {
    val data = pngBytes(10, 20)
    val body = new ByteBody(data)
    intercept[InvalidFormatException] { m.service.imageSize("01234567890".getBytes) }
    assertResult((10, 20)) { m.service.imageSize(data) }
    assertResult((10, 20)) { m.service.imageSize(body) }
  }

  "cropImg" should "crop an image and select the best fit" in {
    val fixedFitH = Fit(320, 240, 320, 240)
    val fixedFitW = Fit(240, 320, 240, 320)
    val dynamicFitH = Fit(320/2, 240/2, 320, 240)
    val dynamicFitW = Fit(240/2, 320/2, 240, 320)
    val fixedFitW2 = Fit(320*2, 240, 320*2, 240)

    //fixed landscape
    var img = m.service.cropImg(Gfx.createImage(320, 240, ImageType.TYPE_INT_RGB), Seq(fixedFitH))
    assertResult("320x240") { img.getWidth+"x"+img.getHeight }
    img = m.service.cropImg(Gfx.createImage(320+32, 240+24, ImageType.TYPE_INT_RGB), Seq(fixedFitH))
    assertResult("320x240") { img.getWidth+"x"+img.getHeight }

    //fixed landscape or portrait
    img = m.service.cropImg(Gfx.createImage(320+32, 240+24, ImageType.TYPE_INT_RGB), Seq(fixedFitW, fixedFitH))
    assertResult("320x240") { img.getWidth+"x"+img.getHeight }
    img = m.service.cropImg(Gfx.createImage(240+24, 320+32, ImageType.TYPE_INT_RGB), Seq(fixedFitW, fixedFitH))
    assertResult("240x320") { img.getWidth+"x"+img.getHeight }

    //dynamic landscape or portrait (correct ratio, outside of boundaries)
    img = m.service.cropImg(Gfx.createImage(320+32, 240+24, ImageType.TYPE_INT_RGB), Seq(dynamicFitW, dynamicFitH))
    assertResult("320x240") { img.getWidth+"x"+img.getHeight }
    img = m.service.cropImg(Gfx.createImage(240+24, 320+32, ImageType.TYPE_INT_RGB), Seq(dynamicFitW, dynamicFitH))
    assertResult("240x320") { img.getWidth+"x"+img.getHeight }
    img = m.service.cropImg(Gfx.createImage(320/2-32, 240/2-24, ImageType.TYPE_INT_RGB), Seq(dynamicFitW, dynamicFitH))
    assertResult("160x120") { img.getWidth+"x"+img.getHeight }
    img = m.service.cropImg(Gfx.createImage(240/2-24, 320/2-32, ImageType.TYPE_INT_RGB), Seq(dynamicFitW, dynamicFitH))
    assertResult("120x160") { img.getWidth+"x"+img.getHeight }

    //dynamic landscape (inside of boundaries)
    img = m.service.cropImg(Gfx.createImage(300, 200, ImageType.TYPE_INT_RGB), Seq(dynamicFitW, dynamicFitH))
    assertResult("300x200") { img.getWidth+"x"+img.getHeight }

    //dynamic landscape (incorrect ratio, outside of boundaries)
    img = m.service.cropImg(Gfx.createImage(400, 220, ImageType.TYPE_INT_RGB), Seq(dynamicFitW, dynamicFitH))
    assertResult("320x176") { img.getWidth+"x"+img.getHeight }
    img = m.service.cropImg(Gfx.createImage(200, 100, ImageType.TYPE_INT_RGB), Seq(dynamicFitW, dynamicFitH))
    assertResult("240x120") { img.getWidth+"x"+img.getHeight }

    //fixed panorama
    img = m.service.cropImg(Gfx.createImage(320*2-32*2, 240-24, ImageType.TYPE_INT_RGB), Seq(fixedFitW, fixedFitH, fixedFitW2))
    assertResult("640x240") { img.getWidth+"x"+img.getHeight }
  }

  "cop" should "crop an image file and select the best fit" in {
    val face = List("320x240", "240x320", "640x240")
    val dyna = List("160x120-320x240")
    intercept[InvalidFormatException] { m.service.crop(new ByteBody("01234567890".getBytes), 0.8F, face) }
    var result = m.service.crop(new ByteBody(pngBytes(320+32, 240+24)), 0.8F, face)
    assertResult("320x240") { result.width+"x"+result.height }
    result = m.service.crop(new ByteBody(pngBytes(240+24, 320+32)), 0.8F, face)
    assertResult("240x320") { result.width+"x"+result.height }
    result = m.service.crop(new ByteBody(pngBytes(320+32, 240+24)), 0.8F, dyna)
    assertResult("320x240") { result.width+"x"+result.height }
    result = m.service.crop(new ByteBody(pngBytes(300, 200)), 0.8F, dyna)
    assertResult("300x200") { result.width+"x"+result.height }
  }

}
