package com.coldcore.haala
package service

import core.Timestamp
import service.AbyssService.{CreateIN, ValueMetaIN}
import service.exceptions.ObjectNotFoundException
import org.scalatest.{BeforeAndAfter, FlatSpec, Matchers}
import org.scalatest.mockito.MockitoSugar
import org.mockito.Mockito._
import org.mockito.ArgumentMatchers._
import org.mockito.ArgumentCaptor
import org.mockito.stubbing.Answer
import org.mockito.invocation.InvocationOnMock
import dao.GenericDAO
import domain.AbyssEntry
import org.junit.runner.RunWith
import org.scalatest.junit.JUnitRunner

@RunWith(classOf[JUnitRunner])
class AbyssServiceSpec extends FlatSpec with MockitoSugar with BeforeAndAfter with Matchers {
  var m: MyMock = _

  before {
    m = new MyMock
  }

  class MyMock extends com.coldcore.haala.service.test.MyMock {
    val abyssEntryDao = mock[GenericDAO[AbyssEntry]]
    val service = new AbyssServiceImpl |< { x =>
      x.serviceContainer = serviceContainer
      x.abyssEntryDao = abyssEntryDao
    }
  }

  it should "create" in {
    doAnswer(new Answer[Unit] {
      override def answer(invocation: InvocationOnMock) {
        val x = invocation.getArguments.head.asInstanceOf[AbyssEntry]; x.id = 7L //assign ID to prepare Solr doc
      }
    }).when(m.abyssEntryDao).saveOrUpdate(any[AbyssEntry])
    m.service.create(CreateIN("R123", "mypack", ValueMetaIN(value = "CreateMe"), Timestamp("20180216235948778")))
    val arg = ArgumentCaptor.forClass(classOf[AbyssEntry])
    verify(m.abyssEntryDao).saveOrUpdate(arg.capture)
    arg.getValue.value shouldBe "CreateMe"
    verify(m.solrService).submit(any[SolrService.SubmitDoc])
  }

  it should "create from parent" in {
    when(m.abyssEntryDao.findById(5)).thenReturn(Some(new AbyssEntry {
      id = 5L; ref="R123"; pack="mypack"; value = "val"; keep = Timestamp("20180216235948778") }))
    doAnswer(new Answer[Unit] {
      override def answer(invocation: InvocationOnMock) {
        val x = invocation.getArguments.head.asInstanceOf[AbyssEntry]; x.id = 7L //assign ID to prepare Solr doc
      }
    }).when(m.abyssEntryDao).saveOrUpdate(any[AbyssEntry])

    m.service.create(5, ValueMetaIN(value = "CreateMe"))
    val arg = ArgumentCaptor.forClass(classOf[AbyssEntry])
    verify(m.abyssEntryDao).saveOrUpdate(arg.capture)
    arg.getValue.value shouldBe "CreateMe"
    arg.getValue.ref shouldBe "R123"
    arg.getValue.pack shouldBe "mypack"
    arg.getValue.keep shouldBe Timestamp("20180216235948778").toLong
    verify(m.solrService).submit(any[SolrService.SubmitDoc])
  }

  it should "update value and meta" in {
    when(m.abyssEntryDao.findById(0)).thenReturn(None)
    intercept[ObjectNotFoundException] { m.service.update(0, ValueMetaIN(value = "val", meta = "prop=a")) }

    when(m.abyssEntryDao.findById(7)).thenReturn(Some(new AbyssEntry { id = 7L; value = "val"; meta="prop=a" }))
    m.service.update(7, ValueMetaIN(value = "val-a", meta = "updated=1"))
    val arg = ArgumentCaptor.forClass(classOf[AbyssEntry])
    verify(m.abyssEntryDao).saveOrUpdate(arg.capture)
    arg.getValue.value shouldBe "val-a"
    arg.getValue.meta shouldBe "updated=1"
    verify(m.solrService).submit(any[SolrService.SubmitDoc])
  }

  it should "update value" in {
    when(m.abyssEntryDao.findById(7)).thenReturn(Some(new AbyssEntry { id = 7L; value = "val"; meta="prop=a" }))
    m.service.update(7, ValueMetaIN(value = "updated"))
    val arg = ArgumentCaptor.forClass(classOf[AbyssEntry])
    verify(m.abyssEntryDao).saveOrUpdate(arg.capture)
    arg.getValue.value shouldBe "updated"
    arg.getValue.meta shouldBe "prop=a"
    verify(m.solrService).submit(any[SolrService.SubmitDoc])
  }

  it should "update meta" in {
    when(m.abyssEntryDao.findById(7)).thenReturn(Some(new AbyssEntry { id = 7L; value = "val"; meta="prop=a" }))
    m.service.update(7, "updated=1")
    val arg = ArgumentCaptor.forClass(classOf[AbyssEntry])
    verify(m.abyssEntryDao).saveOrUpdate(arg.capture)
    arg.getValue.value shouldBe "val"
    arg.getValue.meta shouldBe "updated=1"
    verify(m.solrService).submit(any[SolrService.SubmitDoc])
  }

  it should "delete" in {
    val x = new AbyssEntry { id = 7L; value = "val" }
    when(m.abyssEntryDao.findById(7)).thenReturn(Some(x))
    m.service.delete(7)
    verify(m.abyssEntryDao).delete(x)
    verify(m.solrService).deleteByIds(any)
  }

  it should "prepare solr doc" in {
    val x = m.service.prepareSolrDoc(new AbyssEntry {
      id = 1L; ref="R123"; pack="mypack"; value="type=2;status=created;"; meta="key=K4352FF;obj=1;paid=0;i_pkg=2;s_grp=bill;" })
    x.toSet shouldBe Set('id, "4_0000000000000000001", 'o_typ, 4, 'oa_ref, "R123", 'oa_pac, "mypack",
                         'text, "type=2;status=created;", 'oa_s_grp, "bill", 'oa_i_pkg, "2")
  }

}
