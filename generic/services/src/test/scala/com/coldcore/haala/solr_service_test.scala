package com.coldcore.haala

import dao.GenericDAO
import domain.{BaseOption, Domain, Label}
import org.scalatest._
import org.scalatest.mockito.MockitoSugar
import org.scalatest.junit.JUnitRunner
import org.mockito.Mockito._
import org.junit.runner.RunWith
import service.SolrService._
import service.{FacetResult, SolrServiceImpl}
import service.SolrService.{SolrDoc, ObjectTypeTerm, SolrQuery, ValueTerm, CombinedIdTerm, ObjectIdTerm}
import service.builder.{EntitySolrDocBuilder, SolrDocBuilder}
import scala.collection.JavaConverters._

@RunWith(classOf[JUnitRunner])
class SolrServiceSpec extends WordSpec with MockitoSugar with BeforeAndAfter with Matchers {
  var m: MyMock = _

  before {
    m = new MyMock
  }

  "solrId" should {
    "return correct ID" in {
      assertResult("1_0000000000000001234") { solrId(1, 1234) }
      assertResult(1234) { solrId(1, "1_1234") }
    }
  }

  "solrDoc" should {
    "return inital Doc" in {
      assertResult(List('id, "1_0000000000000001234", 'o_typ, 1)) { solrDoc(1, 1234) }
    }
  }

  "solrObjectIds" should {
    "return list of database IDs" in {
      val docs = List(Map('id -> "1_1234"), Map('id -> "1_1236"), Map('id -> "1_0000000000000001238"))
      assertResult(List(1234, 1236, 1238)) { solrObjectIds(docs: _*).toList }
    }

    "return list of database IDs (With Terms)" in {
      val docs = List(SolrDoc(CombinedIdTerm("1_1234")), SolrDoc(CombinedIdTerm("1_1236")), SolrDoc(CombinedIdTerm("1_0000000000000001238")))
      assertResult(List(1234, 1236, 1238)) { solrObjectIdsWT(docs: _*).toList }
    }
  }

  "solrEscapeChars" should {
    "escape special Solr chars" in {
      assertResult("Some\\: thing \\\\  \\*\\*  is \\&& here\\!") { solrEscapeChars("Some: thing \\  **  is && here!") }
    }
  }

  "solrSolrSort" should {
    "map database sort order to Solr sort order" in {
      assertResult("id asc") { solrSort("ol", "id") }
      assertResult("ol_cat desc, id asc") { solrSort("ol", "category desc, id", Map("category" -> "cat")) }
      assertResult("ol_key asc, ol_rte asc, id desc") { solrSort("ol", "key asc, rate, id desc", Map("rate" -> "rte")) }
      assertResult("random_abc asc") { solrSort("ol", "random_abc") }
    }
  }

  "solrDynamicFields" should {
    "convert fields into Solr fields" in {
      val r = solrDynamicFields("oa", Map("i_cat" -> 2, "i_gen" -> "4", "s_txt" -> "foo", "l_num" -> 6, "unk" -> "bar"))
      val grouped = r grouped 2
      assertResult(Set(List('oa_i_cat, 2), List('oa_i_gen, "4"), List('oa_s_txt, "foo"), List('oa_l_num, 6))) { r.grouped(2).toSet }
    }
  }

  "solrDynamicFieldsWT" should {
    "convert fields into Solr fields (With Rerms)" in {
      val r = solrDynamicFieldsWT("oa", Map("i_cat" -> 2, "i_gen" -> "4", "s_txt" -> "foo", "l_num" -> 6, "unk" -> "bar"))
      assertResult(Set(ValueTerm("oa_i_cat", 2), ValueTerm("oa_i_gen", "4"), ValueTerm("oa_s_txt", "foo"), ValueTerm("oa_l_num", 6))) { r.toSet }
    }
  }

  private val dummy_response = """
    |<?xml version="1.0" encoding="UTF-8"?>
    |<response>
      |<lst name="responseHeader">
        |<int name="status">0</int>
        |<int name="QTime">4</int>
        |<lst name="params">
          |<str name="indent">on</str>
          |<str name="start">0</str>
          |<str name="q">*:*</str>
          |<str name="rows">2</str>
          |<str name="version">2.2</str>
        |</lst>
      |</lst>
      |<result name="response" numFound="17" start="0">
        |<doc>
          |<str name="id">1_5</str>
        |</doc>
        |<doc>
          |<str name="id">1_0000000000000000007</str>
        |</doc>
      |</result>
    |</response>
    """.stripMargin.trim

  private val dummy_facetQueries = """
    |<?xml version="1.0" encoding="UTF-8"?>
    |<response>
      |<lst name="responseHeader">
        |<int name="status">0</int>
        |<int name="QTime">1</int>
        |<lst name="params">
          |<str name="facet">true</str>
          |<arr name="facet.query">
            |<str>foo</str>
            |<str>bar</str>
            |<str>som</str>
          |</arr>
          |<str name="q">*</str>
          |<str name="rows">0</str>
        |</lst>
      |</lst>
      |<result name="response" numFound="13" start="0"/>
      |<lst name="facet_counts">
        |<lst name="facet_queries">
          |<int name="foo">11</int>
          |<int name="bar">7</int>
          |<int name="som">2</int>
        |</lst>
        |<lst name="facet_fields"/>
        |<lst name="facet_dates"/>
        |<lst name="facet_ranges"/>
      |</lst>
    |</response>
    """.stripMargin.trim

  private val dummy_facetFields = """
    |<?xml version="1.0" encoding="UTF-8"?>
    |<response>
      |<lst name="responseHeader">
        |<int name="status">0</int>
        |<int name="QTime">1</int>
        |<lst name="params">
          |<str name="facet">true</str>
          |<arr name="facet.field">
            |<str>foo</str>
            |<str>bar</str>
            |<str>som</str>
          |</arr>
          |<str name="q">*</str>
          |<str name="rows">0</str>
        |</lst>
      |</lst>
      |<result name="response" numFound="34" start="0"/>
      |<lst name="facet_counts">
        |<lst name="facet_queries"/>
        |<lst name="facet_fields">
          |<lst name="category">
            |<int name="foo">11</int>
            |<int name="bar">7</int>
            |<int name="som">2</int>
          |</lst>
          |<lst name="camera">
            |<int name="nikon">3</int>
            |<int name="canon">5</int>
          |</lst>
        |</lst>
        |<lst name="facet_dates"/>
        |<lst name="facet_ranges"/>
      |</lst>
    |</response>
    """.stripMargin.trim

  /** Extend the service to expose protected methods */
  class OpenSolrServiceImpl extends SolrServiceImpl {
    override def constructAddXml(docs: SolrDoc*) = super.constructAddXml(docs: _*)
    override def prepareAddXml(docs: SubmitDoc*) = super.prepareAddXml(docs: _*)
    override def constructDeleteByIdsXml(ids: IdTerm*) = super.constructDeleteByIdsXml(ids: _*)
    override def prepareDeleteByIdsXml(ids: Any*) = super.prepareDeleteByIdsXml(ids: _*)
    override def prepareDeleteXml(q: String) = super.prepareDeleteXml(q)
    override def parse_response(s: String) = super.parse_response(s)
    override def parse_responseWT(s: String) = super.parse_responseWT(s)
    override def parse_facetQueries(s: String) = super.parse_facetQueries(s)
    override def parse_facetFields(s: String) = super.parse_facetFields(s)
    override def constructSelectQuery(q: String, prms: Term*) = super.constructSelectQuery(q, prms: _*)
    override def constructSelectQuery(q: SolrQuery, prms: Term*) = super.constructSelectQuery(q, prms: _*)
    override def prepareSelectQuery(q: String, prms: Any*) = super.prepareSelectQuery(q, prms: _*)
    override def expandAsQuery(q: SolrQuery): String = super.expandAsQuery(q)
    override def expandAsQuery(prms: Term*): String = super.expandAsQuery(prms: _*)
  }

  class MyMock extends com.coldcore.haala.service.test.MyMock {
    val service = new OpenSolrServiceImpl |< { x => x.serviceContainer = serviceContainer }
    when(settingService.querySystem("SolrURL")).thenReturn("http://localhost/solr")
  }

  "prepareAddXml" should {
    "construct correct XML for Solr to add" in {
      val (docA, docB) = (List('foo, "bar", 'key, "val"), List('som, "mar"))
      val x = m.service.prepareAddXml(docA, docB)
      assertResult("""<add>
                 |<doc><field name="foo">bar</field><field name="key">val</field></doc>
                 |<doc><field name="som">mar</field></doc>
               |</add>""".stripMargin.trim.filter('\n'!=).mkString) { x }
    }
  }

  "constructAddXml" should {
    "construct correct XML for Solr to add" in {
      val docA = SolrDoc(ObjectIdTerm(4343, ObjectTypeTerm(2)),
        ValueTerm("ol_key", "venice"), ValueTerm("text", "Something about Venice"))
      val docB = SolrDoc(ObjectIdTerm(7788, ObjectTypeTerm(2)),
        ValueTerm("ol_key", "paris"), ValueTerm("text", "Paris is in France"))

      val x = m.service.constructAddXml(docA, docB)
      assertResult("""<add>
                  <doc>
                    <field name="id">2_0000000000000004343</field>
                    <field name="o_typ">2</field>
                    <field name="ol_key">venice</field>
                    <field name="text">Something about Venice</field>
                  </doc>
                  <doc>
                    <field name="id">2_0000000000000007788</field>
                    <field name="o_typ">2</field>
                    <field name="ol_key">paris</field>
                    <field name="text">Paris is in France</field>
                  </doc>
                </add>""".replace("\n", "").replaceAll("\\>\\s*\\<", "><")) { x }
    }
  }

  "prepareDeleteByIdsXml" should {
    "construct correct XML for Solr to delete" in {
      val x = m.service.prepareDeleteByIdsXml(3, 7)
      assertResult("<delete><id>3</id><id>7</id></delete>") { x }
    }
  }

  "constructDeleteByIdsXml" should {
    "construct correct XML for Solr to delete" in {
      val x = m.service.constructDeleteByIdsXml(ObjectIdTerm(4343, ObjectTypeTerm(2)), CombinedIdTerm("2_7788"))
      assertResult("<delete><id>2_0000000000000004343</id><id>2_7788</id></delete>") { x }
    }
  }

  "prepareDeleteXml" should {
    "construct correct XML for Solr to delete" in {
      val x = m.service.prepareDeleteXml("foo")
      assertResult("<delete><query>foo</query></delete>") { x }
    }
  }

  "parse_response" should {
    "parse response XML section" in {
      val r = m.service.parse_response(dummy_response)
      assertResult(17) { r.total }
      assertResult(2) { r.objects.size }
      assertResult("1_5" :: "1_0000000000000000007" :: Nil) { r.objects.map(_.get('id).get) }
    }

    "parse response XML section (With Terms)" in {
      val r = m.service.parse_responseWT(dummy_response)
      assertResult(17) { r.total }
      assertResult(2) { r.objects.size }
      assertResult("1_0000000000000000005" :: "1_0000000000000000007" :: Nil) { r.objects.map(_.objectIdTerm.get.toCombinedIdTerm.combinedId) }
    }
  }

  "parse_facetQueries" should {
    "parse facet queries response XML section" in {
      val r = m.service.parse_facetQueries(dummy_facetQueries)
      assertResult(13) { r.total }
      assertResult(3) { r.objects.size }
      assertResult("foo 11" :: "bar 7" :: "som 2" :: Nil) { r.objects.map(x => x._1+" "+x._2) }
    }
  }

  "parse_facetFields" should {
    "parse facet fields response XML section" in {
      val r = m.service.parse_facetFields(dummy_facetFields)
      assertResult(34) { r.total }
      assertResult(5) { r.objects.size }
      assertResult("category:foo 11" :: "category:bar 7" :: "category:som 2" :: "camera:nikon 3" :: "camera:canon 5" :: Nil) {
        r.objects.map(x => x._1+" "+x._2) }
     }
  }

  "trimFieldPrefix" should {
    "strip prefix from a field" in {
      val r = trimFieldPrefix(FacetResult(0, ("cn:UK", 12L) :: ("cn:EU", 5L) :: Nil))
      assertResult(FacetResult(0, ("UK", 12L) :: ("EU", 5L) :: Nil)) { r }
    }
  }

  "merge" should {
    "sum values of facet fields with the same name" in {
      val r = merge(FacetResult(0, ("cn:UK", 12L) :: ("cn:EU", 5L) :: ("cn:UK", 3L) :: Nil))
      assertResult(FacetResult(0, ("cn:UK", 15L) :: ("cn:EU", 5L) :: Nil)) { r }
    }
  }

  "removeZeroFields" should {
    "filter out empty values" in {
      val r = removeZeroFields(FacetResult(0, ("cn:UK", 12L) :: ("cn:EU", 5L) :: ("cn:CZ", 0L) :: Nil))
      assertResult(FacetResult(0, ("cn:UK", 12L) :: ("cn:EU", 5L) :: Nil)) { r }
    }
  }

  "removeNamedFields" should {
    "filter out fields by name" in {
      val r = removeNamedFields(FacetResult(0, ("cn:UK", 12L) :: ("cn:EU", 5L) :: ("cn:CZ", 3L) :: Nil), "cn:EU", "cn:CZ")
      assertResult(FacetResult(0, ("cn:UK", 12L) :: Nil)) { r }
    }
  }

  "split" should {
    "split a result into facets one per each field" in {
      val r = split(FacetResult(0, ("cn:UK", 12L) :: ("cn:EU", 5L) :: ("to:London", 3L) :: Nil))
      assertResult(
        Map("cn" -> FacetResult(0, ("cn:UK", 12L) :: ("cn:EU", 5L):: Nil),
            "to" -> FacetResult(0, ("to:London", 3L) :: Nil))) {
        r
      }
    }
  }

  "prepareSelectQuery" should {
    "construct proper select URL query" in {
      assertResult("http://localhost/solr/select/?q=o_typ%3A1&fl=id&start=0&rows=2") {
        m.service.prepareSelectQuery("o_typ:1", 'fl, "id", 'start, 0, 'rows, 2) }
     }
  }

  "constructSelectQuery" should {
    "construct proper select URL query" in {
      assertResult("http://localhost/solr/select/?q=o_typ%3A1&fl=id&start=0&rows=2") {
        m.service.constructSelectQuery("o_typ:1", ValueTerm("fl", "id"), ValueTerm("start", 0), ValueTerm("rows", 2)) }

      assertResult("http://localhost/solr/select/?q=o_typ%3A1&fl=id&start=0&rows=2") {
        m.service.constructSelectQuery("o_typ:1", FlTerm("id"), StartTerm(0), RowsTerm(2)) }
    }

    "construct proper select URL query (SolrQuery)" in {
      val q = SolrQuery(ObjectTypeTerm(1))
      assertResult("http://localhost/solr/select/?q=o_typ%3A1&fl=id&start=0&rows=2") {
        m.service.constructSelectQuery(q, ValueTerm("fl", "id"), ValueTerm("start", 0), ValueTerm("rows", 2)) }

      assertResult("http://localhost/solr/select/?q=o_typ%3A1&fl=id&start=0&rows=2") {
        m.service.constructSelectQuery(q, FlTerm("id"), StartTerm(0), RowsTerm(2)) }
    }
  }

  "expandAsQuery (SolrQuery)" should {
    "construct proper Solr query" in {
      val q = SolrQuery(ObjectTypeTerm(1))
      assertResult("o_typ:1") { m.service.expandAsQuery(q) }
    }

    "construct proper Solr query (samples)" in {
      assertResult("o_typ:1 AND ol_key:venice") {
        m.service.expandAsQuery(SolrQuery(
          AndTerm(
            ObjectTypeTerm(1),
            ValueTerm("ol_key", "venice")))) }

      assertResult("o_typ:1 AND ol_key:venice AND ol_sit:cp1") {
        m.service.expandAsQuery(SolrQuery(
          AndTerm(
            ObjectTypeTerm(1),
            ValueTerm("ol_key", "venice"),
            ValueTerm("ol_sit", "cp1")))) }

      assertResult("o_typ:1 AND ol_key:venice AND (ol_sit:cp1 OR ol_sit:cp2 OR ol_sit:xx1)") {
        m.service.expandAsQuery(SolrQuery(
          AndTerm(
            ObjectTypeTerm(1),
            ValueTerm("ol_key", "venice"),
            OrTerm(
              ValueTerm("ol_sit", "cp1"),
              ValueTerm("ol_sit", "cp2"),
              ValueTerm("ol_sit", "xx1")) ))) }

      assertResult("(o_typ:1 OR o_typ:2) AND ol_key:venice AND (ol_sit:cp1 OR ol_sit:cp2) AND -ol_sta:3 AND -ol_sta:4") {
        m.service.expandAsQuery(SolrQuery(
          AndTerm(
            OrTerm(
              ObjectTypeTerm(1),
              ObjectTypeTerm(2)),
            ValueTerm("ol_key", "venice"),
            OrTerm(
              ValueTerm("ol_sit", "cp1"),
              ValueTerm("ol_sit", "cp2")),
            NotTerm(
              ValueTerm("ol_sta", "3")),
            NotTerm(
              ValueTerm("ol_sta", "4")) ))) }

      assertResult("((o_typ:1 AND ol_sta:3 OR o_typ:2 AND ol_sta:4) OR o_typ:8)") {
        m.service.expandAsQuery(SolrQuery(
          OrTerm(
            OrTerm(
              AndTerm(
                ObjectTypeTerm(1),
                ValueTerm("ol_sta", "3")),
              AndTerm(
                ObjectTypeTerm(2),
                ValueTerm("ol_sta", "4"))),
              ObjectTypeTerm(8) ))) }

      assertResult("o_typ:1 AND ol_sta:3 AND o_typ:2 AND ol_sta:4") {
        m.service.expandAsQuery(SolrQuery(
          AndTerm(
            AndTerm(
              List(ObjectTypeTerm(1),
                ValueTerm("ol_sta", "3")): _*),
            AndTerm(
              List(ObjectTypeTerm(2),
                ValueTerm("ol_sta", "4")): _*) ))) }

      assertResult("o_typ:1 AND ol_sta:3") {
        m.service.expandAsQuery(SolrQuery(
          AndTerm(
            AndTerm(
              List(ObjectTypeTerm(1),
                ValueTerm("ol_sta", "3")): _*),
            NoopTerm ))) }
    }

    "construct proper Solr query (NoopTerm)" in {
      assertResult("o_typ:1 AND ol_key:venice") {
        m.service.expandAsQuery(SolrQuery(
          AndTerm(
            ObjectTypeTerm(1),
            NoopTerm,
            ValueTerm("ol_key", "venice")))) }
    }

    "construct proper Solr query (TextTerm)" in {
      assertResult("o_typ:1 AND venice italy") {
        m.service.expandAsQuery(SolrQuery(
          AndTerm(
            ObjectTypeTerm(1),
            TextTerm("venice italy")))) }
    }

  }

  "fetchObjects" should {
    "fetch objects from database based on Solr IDs" in {
      val (solrResult, dao) = (m.service.parse_response(dummy_response), mock[GenericDAO[Label]])
      when(dao.find("from " + classOf[Label].getName + " where id in (5,7)")).thenReturn(
        new Label { id = 5L } :: new Label { id = 7L } :: Nil) //add in reverse to test sorting - 5,7
      val r = m.service.fetchObjects(solrResult, dao, classOf[Label])
      assertResult(17) { r.total }
      assertResult(2) { r.objects.size }
      assertResult(5 :: 7 :: Nil) { r.objects.map(_.id).toList }
    }

    "fetch objects from database based on Solr IDs (With Terms)" in {
      val (solrResult, dao) = (m.service.parse_responseWT(dummy_response), mock[GenericDAO[Label]])
      when(dao.find("from " + classOf[Label].getName + " where id in (5,7)")).thenReturn(
        new Label { id = 5L } :: new Label { id = 7L } :: Nil) //add in reverse to test sorting - 5,7
      val r = m.service.fetchObjectsWT(solrResult, dao, classOf[Label])
      assertResult(17) { r.total }
      assertResult(2) { r.objects.size }
      assertResult(5 :: 7 :: Nil) { r.objects.map(_.id).toList }
    }
  }

  "arbitrary prepareSolrDoc" should {
    "create Solr terms and convert into SolrDoc" in {
      def packPrefix: String = "arbit_pr"
      def packTerm(key: String, value: Any): Term = ValueTerm(packPrefix+"_"+key, value)
      def packDynIntTerm(key: String, value: Any): Term = packTerm("i_"+key, value)
      case class Domain(id: Long)
      case class Opt(typ: Long)
      val terms =
        List(
          ObjectIdTerm(12, ObjectTypeTerm(2)),
          packTerm("ref", "MY_REF"),
          packTerm("sta", 3),
          packTerm("usr", 5),
          packDynIntTerm("lck", 0),
          ValueTerm("text", "Foo Bar")) :::
          //domains
          (List.empty[Term] /: List(Domain(34), Domain(35)))((a,b) => packTerm("dom", b.id) :: a) :::
          //options
          (List.empty[Term] /: List(Opt(44), Opt(45)))((a,b) => packTerm("opt", b.typ) :: a)
      val r = SolrDoc(terms: _*)
      assertResult(r.terms) {
        ObjectIdTerm(12,ObjectTypeTerm(2)) :: ValueTerm("arbit_pr_ref", "MY_REF") :: ValueTerm("arbit_pr_sta", 3) ::
          ValueTerm("arbit_pr_usr", 5) :: ValueTerm("arbit_pr_i_lck", 0) :: ValueTerm("text", "Foo Bar") ::
          ValueTerm("arbit_pr_dom", 35) :: ValueTerm("arbit_pr_dom", 34) ::
          ValueTerm("arbit_pr_opt", 45) :: ValueTerm("arbit_pr_opt", 44) :: Nil
      }
      assertResult(ObjectIdTerm(12,ObjectTypeTerm(2))) { r.objectIdTerm.get }
    }
  }

}

@RunWith(classOf[JUnitRunner])
class SolrBuilderSpec extends FlatSpec with Matchers {

  it should "create a new SolrDoc from terms" in {
    val doc =
      SolrDocBuilder(3, 1)
        .value("key D", "val D")
        .value("key F" -> "val F")
        .term(ValueTerm("key A", "val A"))
        .term(ValueTerm("key B", "val B"), ValueTerm("key C", "val C"))
        .some(Some(ValueTerm("key E", "val E")), None)
        .build

    doc shouldBe SolrDoc(
      ObjectIdTerm(3, ObjectTypeTerm(1)),
      ValueTerm("key D", "val D"),
      ValueTerm("key F", "val F"),
      ValueTerm("key A", "val A"),
      ValueTerm("key B", "val B"),
      ValueTerm("key C", "val C"),
      ValueTerm("key E", "val E"))
  }

  it should "create a new SolrDoc for an entity object" in {
    class LabelOption extends BaseOption("pkey")

    val label = new Label {
      id = 4L; key = "foo"; value = "bar"
      var options = new JArrayList[LabelOption]((
        new LabelOption { `type` = 1; value = "opval 1a" } ::
        new LabelOption { `type` = 1; value = "opval 1b" } ::
        new LabelOption { `type` = 2; value = "opval 2a" } ::
        new LabelOption { `type` = 2; value = "opval 2b" } :: Nil).asJava)
      var domains = new JArrayList[Domain]((
        new Domain { id = 5L } :: new Domain { id = 6L } :: Nil).asJava)
    }

    val doc =
      EntitySolrDocBuilder(label, 2)
        .value("key" -> label.key,
               "val" -> label.value)
        .option("op 1", 1, take = 1)
        .option("op 2", 2)
        .domains()
        .build

    doc shouldBe SolrDoc(
      ObjectIdTerm(4, ObjectTypeTerm(2)),
      ValueTerm("key", "foo"),
      ValueTerm("val", "bar"),
      ValueTerm("op 1", "opval 1a"),
      ValueTerm("op 2", "opval 2a"),
      ValueTerm("op 2", "opval 2b"),
      ValueTerm("dom", 5L),
      ValueTerm("dom", 6L))
  }

}
