PayPal sandbox instructions
===========================
  1) Login to "https://developer.paypal.com/developer/accounts/"
  2) Create a sandbox Business account
  3) Create REST API application "https://developer.paypal.com/developer/applications/" and link to the sandbox account
  4) Create a web hook for "Payment sale completed" event pointing to CP, eg. "https://cpanel.example.org/paypal_events"
  5) Use REST app Client ID and Secret in "PaymentConf.json"
  6) Create payments in CP and use test account to pay

Auto apply credit to user
=========================
  1) Create a payment in "user" view and use "complete=n;process=crd1;user=3;amount=200;" as "value"
     "amount" is credit and "user" is ID of a user to apply credit to
  2) Find this payment in admin "payments" and add "test=y" to the payment entry to process sandbox accounts
  3) Use checkout link and pay with sandbox account
  4) PayPal
    *) can reach the server - do nothing
    *) cannot reach the server - get TX content from PayPal and POST it to "http://cpanel.example.org/paypal_events"
  5) Credit applied and payment updated

  To POST event use either REST plugin or CURL
  curl -v -X POST http://cpanel.example.org/paypal_events -H "Content-Type:application/json" -d @event-sale-completed.json

Sample files
------------
  *) event-payment.sale.completed.json
     generic PayPal TX to use in testing

  *) sample1-event-payment.sale.completed.json
     real example of TX from PayPal sandbox

  *) sample1-event-payment.sale.completed-selfref.json
     the above TX retrieved by "self" link

  *) sample1-event-payment.sale.completed-tx_id.json
     the above TX retrieved with "transactionId=xxx" request parameter
