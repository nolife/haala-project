package com.coldcore.haala.service.annotations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

//todo Not used
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
public @interface Exposed {

}