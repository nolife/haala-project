package com.coldcore.haala
package service

import core.{ParsedSiteSetting, SiteChain}
import core.SiteUtil._
import com.coldcore.misc.scala.StringOp._
import domain.Setting
import exceptions.{ObjectExistsException, ObjectNotFoundException}
import beans.BeanProperty
import dao.GenericDAO
import org.springframework.transaction.annotation.{Propagation, Transactional}

import scala.reflect.ClassTag

object SettingService {
  case class IN(key: String, value: String, site: String)

  class SearchInputX(from: Long, max: Int, orderBy: String = "") extends SearchInput(from, max, orderBy) {
    def this(in: SearchInput) = this(in.from, in.max, in.orderBy)

    val filterKey = ""
    val filterValue = ""
    val siteKeys = List.empty[String]
  }
}

trait SettingService {
  import SettingService._

  def getById(id: Long): Option[Setting]
  def getByKeyAndSite(key: String, site: String): Option[Setting]
  def query(key: String, site: SiteChain): String
  def querySystem(key: String): String
  def queryNoCache(key: String): String
  def getDefaultSite: SiteChain
  def getParsedSites: List[ParsedSiteSetting]
  def getDefaultCurrency: String
  def getItemsPerPage(key: String, site: SiteChain = SiteChain.sys): Int
  def getForSiteLike(key: String, site: SiteChain): Map[String,String]
  def update(id: Long, in: IN)
  def create(in: IN): Setting
  def delete(id: Long)
  def write(key: String, value: String, site: String)
  def search(in: SearchInputX): SearchResult[Setting]
  def getResourceVersion(site: String): String
  def getEnv: String
  def scaled: Boolean
  def queryConstant[T](key: String, pfx: String, const: String)(implicit m: ClassTag[T]): T
}

@Transactional
class SettingServiceImpl extends BaseService with SettingService {
  import SettingService._

  @BeanProperty var settingDao: GenericDAO[Setting] = _
  @BeanProperty var settingsCache: DomainCache[Setting] = _

  private def merge(o: Setting, in: IN) {
    o.key = in.key
    o.value = in.value.safe //nullable field
    o.site = in.site
  }

  /** Get an object by its ID. */
  @Transactional (readOnly = true)
  override def getById(id: Long): Option[Setting] = settingDao.findById(id)

  /** Find an object by a key and site 'chain' and return its value. Cache-aware. */
  @Transactional (readOnly = true)
  override def query(key: String, site: SiteChain): String = {
    val list = cacheAwareResults[Setting](settingsCache, key, { settingDao.findByProperty("key", key) })
    filterObjectForSite(list, site).map(_.getValue).orNull
  }

  /** Query a system setting. Cache-aware. */
  @Transactional (readOnly = true)
  override def querySystem(key: String): String = query(key, SiteChain.sys)

  /** Query a system setting bypassing local cache */
  @Transactional (readOnly = true)
  override def queryNoCache(key: String): String = {
    val list = settingDao.findByProperty("key", key)
    filterObjectForSite(list, SiteChain.sys).map(_.getValue).orNull
  }

  /** Return a default site chain. Cache-aware. */
  @Transactional (readOnly = true)
  override def getDefaultSite: SiteChain = getParsedSites.head.site

  /** Return a default site chain. Cache-aware. */
  @Transactional (readOnly = true)
  override def getParsedSites: List[ParsedSiteSetting] = parseSiteSetting(querySystem("Sites"))

  /** Return a default currency. Cache-aware. */
  @Transactional (readOnly = true)
  override def getDefaultCurrency: String = querySystem("Currencies").parseCSV.head

  /** Return # of items per page registered for a key. Cache-aware. */
  @Transactional (readOnly = true)
  override def getItemsPerPage(key: String, site: SiteChain = SiteChain.sys): Int = {
    val ipp = parseCSVMap(query("ItemsPerPage", site).safe)
    val ips = parseCSVMap(querySystem("ItemsPerPage"))
    ipp.getOrElse(key, ipp.getOrElse("def", ips.getOrElse(key, ips("def")))).toInt
  }

  /** Find objects by a key and site 'chain'. Return a map with key-value pairs. */
  @Transactional (readOnly = true)
  override def getForSiteLike(key: String, site: SiteChain): Map[String,String] = {
    val query = "from "+classOf[Setting].getName+" o where o.key like ? and "+constructWhereSite(site)
    val objs = filterObjectsForSite(settingDao.find(query, key), site)
    (for (x <- objs) yield x.getKey -> x.getValue).toMap
  }

  /** Get an object by properties. */
  override def getByKeyAndSite(key: String, site: String): Option[Setting] =
    settingDao.findUniqueByProperties(Map("key" -> key, "site" -> site))

  /** Update a setting. Cache-aware. */
  @Transactional (readOnly = false)
  override def update(id: Long, in: IN) {
    val o = getById(id).getOrElse(throw new ObjectNotFoundException)
    if (getByKeyAndSite(in.key, in.site).exists(_.id != id)) throw new ObjectExistsException
    merge(o, in)
    settingDao.saveOrUpdate(o)
    settingsCache.remove(o.key) //remove from cache
  }

  /** Create a setting. Cache-aware. */
  @Transactional (readOnly = false)
  override def create(in: IN): Setting = {
    if (getByKeyAndSite(in.key, in.site).isDefined) throw new ObjectExistsException
    val o = new Setting
    merge(o, in)
    settingDao.saveOrUpdate(o)
    settingsCache.remove(o.key) //remove from cache
    o
  }

  /** Delete a setting. Cache-aware. */
  @Transactional (readOnly = false)
  override def delete(id: Long) = getById(id).foreach { x =>
    settingDao.delete(x)
    settingsCache.remove(x.key) //remove from cache
  }

  /** Update a setting if it exists or create a new one. Cache-aware. */
  @Transactional (readOnly = false)
  override def write(key: String, value: String, site: String) {
    val in = IN(key, value, site)
    getByKeyAndSite(key, site) match {
      case Some(x) => update(x.id, in)
      case None => create(in)
    }
  }

  @Transactional (readOnly = true)
  override def search(in: SearchInputX): SearchResult[Setting] = {
    val f = (o: Setting) =>
      (in.siteKeys.isEmpty || in.siteKeys.contains(o.site)) &&
      (in.filterKey == "" || o.key.toLowerCase.contains(in.filterKey)) &&
      (in.filterValue == "" || o.value.toLowerCase.contains(in.filterValue))
    val query = "from "+classOf[Setting].getName+" "+in.orderByClause
    searchObjectsWithFilter(in.from, in.max, settingDao, query, f)
  }

  @Transactional (readOnly = true)
  override def getResourceVersion(site: String): String =
    if (querySystem("Dev/NoCache").isTrue) System.currentTimeMillis.hex //cache disabled (dev mode flag)
    else (querySystem("ResourceVersion") or "0")+"."+(query("ResourceVersion", SiteChain.key(site)) or "0")

  @Transactional (readOnly = true)
  override def getEnv: String = querySystem("Environment").parseCSVMap()("env")

  /** Check if system is scaled across several nodes (bypassing local cache). */
  @Transactional (readOnly = true)
  override def scaled: Boolean = queryNoCache("SystemParams").parseCSVMap().getOrElse("scaled", "").isTrue

  /** Query and parse constant value. Cache-aware.
    * (Constants, estate, SOLR_TYPE_ESTATE) -> Constants / estate.SOLR_TYPE_ESTATE=2 -> 2
    */
  @Transactional (readOnly = true)
  override def queryConstant[T](key: String, pfx: String, const: String)(implicit m: ClassTag[T]): T = {
    val x = query(key, SiteChain.sys).parseCSVMap()(s"$pfx.$const")
    ((m.runtimeClass == classOf[Int]) ? x.toInt | x.toString).asInstanceOf[T]
  }

}
