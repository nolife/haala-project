package com.coldcore.haala
package service

import SolrService.{RangeTerm, Term, ValueTerm}
import org.springframework.transaction.annotation.Transactional
import domain.AbyssEntry
import core.{Meta, Timestamp}

object LeaseService {
  val EXPIRE_24h = 1000L*60L*60L*24L

  case class CreateIN(value: String, meta: String = "", pack: String = "", expires: Long = EXPIRE_24h)

  class SearchInputX(from: Long, max: Int, orderBy: String = "") extends SearchInput(from, max, orderBy) {
    def this(in: SearchInput) = this(in.from, in.max, in.orderBy)

    val pack: Option[String] = None
    val dynamicTerms = List.empty[Term] // ValueTerm(s_usr, foo)
  }

  /** Lease entry Abyss wrapper */
  class LeaseEntry(val abyss: AbyssEntry) {
    def meta(key: String): String = Meta(abyss.meta)(key)
    def meta(key: String, default: => String): String = Meta(abyss.meta)(key, default)
    val value = abyss.value
  }
  object LeaseEntry {
    def apply(abyss: AbyssEntry) = new LeaseEntry(abyss)
  }
}

trait LeaseService {
  import LeaseService._

  def getById(abyssId: Long): Option[LeaseEntry]
  def create(in: CreateIN): LeaseEntry
  def extend(abyssId: Long)
  def search(in: SearchInputX): SearchResult[LeaseEntry]
}

@Transactional
class LeaseServiceImpl extends BaseService with LeaseService {
  import LeaseService._

  private val sorlDynamicType = "les"

  /** Get an object by its ID. */
  @Transactional (readOnly = true)
  override def getById(abyssId: Long): Option[LeaseEntry] =
    sc.get[AbyssService].getById(abyssId).map(LeaseEntry(_))

  /** Create lease entry. */
  @Transactional (readOnly = false)
  override def create(in: CreateIN): LeaseEntry = {
    val ts = Timestamp.fromMills(System.currentTimeMillis+in.expires)
    val meta = Meta(in.meta) +
      ("s_typ", sorlDynamicType) +
      ("r_exp", ts) +
      ("l_ttl", in.expires)

    val abyss = sc.get[AbyssService].create(AbyssService.CreateIN("", in.pack,
      AbyssService.ValueMetaIN(value = in.value, meta = meta.serialize), ts))

    LeaseEntry(abyss)
  }

  /** Extend lease entry. */
  @Transactional (readOnly = false)
  override def extend(abyssId: Long) =  getById(abyssId).foreach { entry =>
    val ts = Timestamp.fromMills(System.currentTimeMillis+entry.meta("l_ttl").toLong)
    val meta = Meta(entry.abyss.meta) + ("r_exp", ts)
    sc.get[AbyssService].update(abyssId, meta.serialize)
    sc.get[AbyssService].extend(abyssId, ts)
  }

  /** Solr search for non expired entries */
  @Transactional (readOnly = true)
  override def search(in: SearchInputX): SearchResult[LeaseEntry] = {
    val result = sc.get[AbyssService].search(new AbyssService.SearchInputX(in.from, in.max, in.orderBy) {
      override val pack = in.pack
      override val dynamicTerms =
        ValueTerm("s_typ", sorlDynamicType) ::
        RangeTerm("r_exp", from = Timestamp()) ::
        in.dynamicTerms
    })
    SearchResult(result.total, result.objects.map(LeaseEntry(_)))
  }

}
