package com.coldcore.haala
package service

import core.GeneralUtil._
import core.Timestamp
import core.Constants.UTF8
import org.springframework.transaction.annotation.Transactional
import java.io.{FileNotFoundException, StringWriter}
import scala.collection.JavaConverters._
import scala.beans.BeanProperty
import org.apache.velocity.VelocityContext
import org.apache.velocity.exception.ResourceNotFoundException
import org.hibernate.SessionFactory

object MiscService {
  case class FreemarkerTemplateIN(template: String, model: Map[String,AnyRef])
  case class VelocityTemplateIN(template: String, model: Map[String,AnyRef])
}

trait MiscService {
  import MiscService._

  def reportSystemError(msg: String, ex: Throwable)
  def freemarkerTemplate(in: FreemarkerTemplateIN): Option[String]
  def velocityTemplate(in: VelocityTemplateIN): Option[String]
  def databaseTime: Timestamp
}

@Transactional
class MiscServiceImpl extends BaseService with MiscService {
  import MiscService._

  @BeanProperty var sessionFactory: SessionFactory = _

  /** Push an error message into a queue. */
  @Transactional (readOnly = true)
  override def reportSystemError(msg: String, e: Throwable) {
    val trace = printStackTrace(e)
    val otime = "Occurred on "+Timestamp().toString("E dd/MM/yyyy HH:mm:ss")
    sc.get[QueueService].pushNonExistent("SystemErrors", otime+"\n"+msg+"\n"+trace, digest(trace, "MD5"))
  }

  /** Load Freemarker template and process it. */
  @Transactional (readOnly = true)
  override def freemarkerTemplate(in: FreemarkerTemplateIN): Option[String] =
    try {
      val template = freemarkerEngine.template(in.template)
      val model = new JHashMap[String,AnyRef]
      model.putAll(in.model.asJava)

      val writer = new StringWriter
      val env = template.createProcessingEnvironment(model, writer)
      env.setOutputEncoding(UTF8)
      env.process

      Some(writer.toString)
    } catch { case e: FileNotFoundException => None }

  /** Load Velocity template and process it. */
  @Transactional (readOnly = true)
  override def velocityTemplate(in: VelocityTemplateIN): Option[String] =
    try {
      val template = velocityEngine.template(in.template)
      val model = new JHashMap[String,AnyRef]
      model.putAll(in.model.asJava)

      val writer = new StringWriter
      val context = new VelocityContext(model)
      template.merge(context, writer)

      Some(writer.toString)
    } catch { case e: ResourceNotFoundException => None }

  /** Database current time. */
  @Transactional (readOnly = true)
  override def databaseTime: Timestamp =
    sessionFactory.getCurrentSession.doReturningWork[Timestamp](connection => {
      val call = connection.prepareCall("{ ? = call timestamp_fn() }")
      call.registerOutParameter(1, java.sql.Types.TIMESTAMP)
      call.execute
      val x = call.getTimestamp(1)
      call.close()
      Timestamp.fromMills(x.getTime)
    })

}
