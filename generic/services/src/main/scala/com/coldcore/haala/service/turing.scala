package com.coldcore.haala
package service

import core.{Constants, SiteChain, Turing, VirtualPath}
import core.GeneralUtil.generateRef
import com.coldcore.misc5.IdGenerator.generateNumbers
import com.coldcore.misc.scala.StringOp._
import java.awt.{Color, Font}
import java.awt.image.BufferedImage
import java.io.ByteArrayOutputStream
import javax.imageio.ImageIO
import org.springframework.transaction.annotation.Transactional

trait TuringService {
  def generate(site: SiteChain): Turing
}

@Transactional
class TuringServiceImpl extends BaseService with TuringService {
  case class Config(width: Int, height: Int, fgColor: Color, bgColor: Color, rgColor: Color,
                    font: String, fontSize: Int, count: Int)

  /** Convert HEX string into Color. */
  private def hexColor(hex: String): Color =
    new Color(Integer.parseInt(hex.substring(0, 2), 16),
              Integer.parseInt(hex.substring(2, 4), 16),
              Integer.parseInt(hex.substring(4, 6), 16))

  /** Create an image containing a turing. */
  private def renderSimple(value: String, c: Config): BufferedImage = {
    val v = value.mkString(" ")
    val img = new BufferedImage(c.width, c.height, BufferedImage.TYPE_INT_RGB)
    val g = img.createGraphics
    g.setFont(new Font(c.font, Font.PLAIN, c.fontSize))
    val (sh, sw, w, h) = (g.getFont.getSize, g.getFontMetrics.stringWidth(v), c.width, c.height)
    g.setColor(c.bgColor)
    g.fillRect(0, 0, w, h)
    g.setColor(c.fgColor)
    g.drawString(v, (w-sw)/2, (h-sh)/2+sh-2)
    g.setColor(c.rgColor)
    g.drawRect(0, 0, w-1, h-1)
    g.dispose()
    img
  }

  /** Return an image (as PNG byte array) containing a turing. */
  private def renderSimplePng(value: String, c: Config): Array[Byte] = {
    val out = new ByteArrayOutputStream
    ImageIO.write(renderSimple(value, c), "png", out)
    out.toByteArray
  }

  /** Create configuration. */
  private def config(site: SiteChain): Config = {
    val params = parseCSVMap(setting("TuringParams", site))
    val size = params.getOrElse("size", "100x20").split("x")
    val (w, h) = (Integer.parseInt(size.head), Integer.parseInt(size.last))
    Config(width = w, height = h,
      fgColor = hexColor(params.getOrElse("fcol", "000000")), //foreground color
      bgColor = hexColor(params.getOrElse("bcol", "FFFFFF")), //background color
      rgColor = hexColor(params.getOrElse("rcol", params.getOrElse("fcol", "000000"))), //rectangle color (default = foreground)
      font = params.getOrElse("font", "Verdana"),
      fontSize = Integer.parseInt(params.getOrElse("fosz", "16")),
      count = Integer.parseInt(params.getOrElse("n", "5")))
  }

  /** Generate a turing. */
  @Transactional (readOnly = false)
  override def generate(site: SiteChain): Turing = {
    val c = config(site)
    val value = generateNumbers(c.count, c.count)
    val data = renderSimplePng(value, c) //render an image
    val path = VirtualPath("turing", generateRef("|hms|.|sym10|").toLowerCase++".png")
    sc.get[FileService].create(path, Constants.SITE_PUBLIC, data) //save an image
    Turing(value, path)
  }
}
