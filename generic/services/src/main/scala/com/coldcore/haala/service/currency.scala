package com.coldcore.haala
package service

import com.coldcore.misc.scala.StringOp._
import java.util.{Currency, Locale}

import com.coldcore.haala.core.Meta
import org.springframework.transaction.annotation.Transactional

trait CurrencyService {
  def convert(amount: Long, from: String, to: String): Long
  def resolveCurrency(domain: String, locales: Locale*): String
  def getSign(currency: String): String
}

@Transactional
class CurrencyServiceImpl extends BaseService with CurrencyService {
  /** Recalculate amount from one currency into another. Inputs: amount to convert and two currencies. */
  @Transactional (readOnly = true)
  override def convert(amount: Long, from: String, to: String): Long = {
    val cRates = parseCSVMap(setting("CurrencyRates"))
    if (from == to || amount == 0) amount
    else if (cRates.contains(from) && cRates.contains(to)) {
      val x = if (defaultCurrency == from) amount else math.round(amount.toDouble/cRates(from).toDouble)
      if (defaultCurrency == to) x else math.round(x.toDouble*cRates(to).toDouble)
    } else 0
  }

  /** Resolve a currency based on user's locales. Earliest locale in a list takes priority. */
  @Transactional (readOnly = true)
  override def resolveCurrency(domain: String, locales: Locale*): String = {
    val o = sc.get[DomainService].resolveDomain(domain) //select a domain or get a default one
    val meta = Meta(o.meta)
    val currencies = setting("Currencies").parseCSV
    val currency = if (meta.csv("resolve").get("currency").isEmpty) None else //allowed to resolve
      locales.map { x =>
        try {
          val code = Currency.getInstance(x).getCurrencyCode
          Some(code).filter(currencies.contains)
        } catch { case e: IllegalArgumentException => None }
      }.find(None!=).flatten

    //return resolved currency or a default for a domain if resolving failed
    currency getOrElse meta("currency", defaultCurrency)
  }

  /** Return a currency sign. */
  @Transactional (readOnly = true)
  override def getSign(currency: String): String =
    parseCSVMap(setting("CurrencySigns")).getOrElse(currency, currency)
}
