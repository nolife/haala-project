package com.coldcore.haala
package service

import dao.GenericDAO
import beans.BeanProperty
import exceptions.{ObjectExistsException, ObjectNotFoundException}
import SolrService.solrEscapeChars
import core.{Constants, SiteChain}
import org.springframework.transaction.annotation.{Propagation, Transactional}
import domain.Label

object LabelService {
  case class IN(key: String, value: String, site: String)

  class SearchInputX(from: Long, max: Int, orderBy: String = "") extends SearchInput(from, max, orderBy) {
    def this(in: SearchInput) = this(in.from, in.max, in.orderBy)

    val filterValue = ""
    val siteKeys = List.empty[String]
  }
}

trait LabelService {
  import LabelService._

  def prepareSolrDoc(o: Label): List[Any]
  def getById(id: Long): Option[Label]
  def query(key: String, site: SiteChain): String
  def query(key: String): List[Label]
  def getForSiteLike(key: String, site: SiteChain): Map[String,String]
  def getForSite(site: SiteChain): Map[String,String]
  def update(id: Long, in: IN)
  def create(in: IN): Label
  def delete(id: Long)
  def delete(key: String, site: String)
  def deleteByKeyLike(key: String)
  def getByKeyLike(key: String): List[Label]
  def write(key: String, value: String, site: String)
  def searchFast(in: SearchInput): SearchResult[Label]
  def search(in: SearchInputX): SearchResult[Label]
  def rename(id: Long, key: String)
}

@Transactional
class LabelServiceImpl extends BaseService with LabelService {
  import LabelService._

  @BeanProperty var labelDao: GenericDAO[Label] = _
  @BeanProperty var labelsCache: DomainCache[Label] = _

  private def merge(o: Label, in: IN) {
    o.key = in.key
    o.value = in.value.safe //nullable field
    o.site = in.site
  }

  private val solrTyp = Constants.SOLR_TYPE_LABEL
  private val solrId = SolrService.solrId(solrTyp, _: Long)
  private val solrDoc = SolrService.solrDoc(solrTyp, _: Long)
  private val solrSort = SolrService.solrSort(Constants.SOLR_PREFIX_LABEL, _: String)

  /** Prepare Solr doc. */
  @Transactional (readOnly = true)
  override def prepareSolrDoc(o: Label): List[Any] =
    solrDoc(o.id) ::: List('ol_key, o.key, 'ol_sit, o.site, 'text, o.value)

  /** Submit an object to Solr. */
  private def doSolrSubmit(o: Label) = sc.get[SolrService].submit(prepareSolrDoc(o))

  /** Delete an object from Solr. */
  private def doSolrDelete(o: Label) = sc.get[SolrService].deleteByIds(solrId(o.id))

  /** Get an object by its ID. */
  @Transactional (readOnly = true)
  override def getById(id: Long): Option[Label] = labelDao.findById(id)

  /** Get an object by properties. */
  private def getByKeyAndSite(key: String, site: String): Option[Label] =
    labelDao.findUniqueByProperties(Map("key" -> key, "site" -> site))

  /** Find an object by a key and site 'chain' and return its value. Cache-aware. */
  @Transactional (readOnly = true)
  override def query(key: String, site: SiteChain): String =
    filterObjectForSite(query(key), site).map(_.getValue).orNull

  /** Find objects by a key and site 'chain'. Cache-aware. */
  @Transactional (readOnly = true)
  override def query(key: String): List[Label] =
    cacheAwareResults[Label](labelsCache, key, { labelDao.findByProperty("key", key) })

  /** Find objects by a key and site 'chain'. Return a map with key-value pairs. */
  @Transactional (readOnly = true)
  override def getForSiteLike(key: String, site: SiteChain): Map[String,String] = {
    val query = "from "+classOf[Label].getName+" o where o.key like ? and "+constructWhereSite(site)
    val objs = filterObjectsForSite(labelDao.find(query, key), site)
    (for (x <- objs) yield x.getKey -> x.getValue).toMap
  }

  /** Find objects by a site 'chain'. Return a map with key-value pairs. */
  @Transactional (readOnly = true)
  override def getForSite(site: SiteChain): Map[String,String] = {
    val query = "from "+classOf[Label].getName+" o where "+constructWhereSite(site)
    val objs = filterObjectsForSite(labelDao.find(query), site)
    (for (x <- objs) yield x.getKey -> x.getValue).toMap
  }

  /** Update a label. Cache-aware. */
  @Transactional (readOnly = false)
  override def update(id: Long, in: IN) {
    val o = getById(id).getOrElse(throw new ObjectNotFoundException)
    if (getByKeyAndSite(in.key, in.site).exists(_.id != id)) throw new ObjectExistsException
    merge(o, in)
    labelDao.saveOrUpdate(o)
    labelsCache.remove(o.key) //remove from cache
    doSolrSubmit(o) //update Solr
  }

  /** Rename a label. Cache-aware. */
  @Transactional (readOnly = true)
  def rename(id: Long, key: String) {
    val o = getById(id).getOrElse(throw new ObjectNotFoundException)
    update(id, IN(key, o.value, o.site))
  }

  /** Create a label. Cache-aware. */
  @Transactional (readOnly = false)
  override def create(in: IN): Label = {
    if (getByKeyAndSite(in.key, in.site).isDefined) throw new ObjectExistsException
    val o = new Label
    merge(o, in)
    labelDao.saveOrUpdate(o)
    labelsCache.remove(o.key) //remove from cache
    doSolrSubmit(o) //update Solr
    o
  }

  /** Delete a label. Cache-aware. */
  @Transactional (readOnly = false)
  override def delete(id: Long) = getById(id).foreach { x =>
    labelDao.delete(x)
    labelsCache.remove(x.key) //remove from cache
    doSolrDelete(x) //update Solr
  }

  /** Delete a label. Cache-aware. */
  @Transactional (readOnly = false)
  override def delete(key: String, site: String) = getByKeyAndSite(key, site).foreach(x => delete(x.id))

  /** Delete labels. Cache-aware. */
  @Transactional (readOnly = false)
  override def deleteByKeyLike(key: String) = for (x <- getByKeyLike(key)) delete(x.id)

  /** Find objects by a key. */
  @Transactional (readOnly = true)
  override def getByKeyLike(key: String): List[Label] = {
    val query = "from "+classOf[Label].getName+" o where o.key like ?"
    labelDao.find(query, key)
  }

  /** Update a label if it exists or create a new one. Cache-aware. */
  @Transactional (readOnly = false)
  override def write(key: String, value: String, site: String) {
    val in = IN(key, value, site)
    getByKeyAndSite(key, site) match {
      case Some(x) => update(x.id, in)
      case None => create(in)
    }
  }

  /** Fast search without filtering. */
  @Transactional (readOnly = true)
  override def searchFast(in: SearchInput): SearchResult[Label] = {
    val query = "from "+classOf[Label].getName+" "+in.orderByClause
    searchObjects(in.from, in.max, labelDao, query)
  }

  /** Solr search */
  @Transactional (readOnly = true)
  override def search(in: SearchInputX): SearchResult[Label] = {
    val result = sc.get[SolrService].search(
      "o_typ:"+solrTyp+
      (if (in.siteKeys.nonEmpty) " AND ("+in.siteKeys.map("ol_sit:"+_).mkString(" OR ")+")" else "")+
      (if (in.filterValue != "") " AND "+solrEscapeChars(in.filterValue) else ""),
      'fl, "id", 'start, in.from, 'rows, in.max, 'sort, solrSort(in.orderBy))
    sc.get[SolrService].fetchObjects(result, labelDao, classOf[Label])
  }
}
