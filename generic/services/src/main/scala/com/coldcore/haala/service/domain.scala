package com.coldcore.haala
package service

import dao.GenericDAO

import beans.BeanProperty
import org.springframework.transaction.annotation.Transactional
import service.exceptions.{ObjectExistsException, ObjectNotFoundException}
import core.SiteUtil._
import com.coldcore.misc.scala.StringOp._
import domain.Domain
import java.util.Locale

import com.coldcore.haala.core.{Meta, SiteChain}

object DomainService {
  case class IN(domain: String, domainSite: SiteChain, `type`: String, meta: String)
}

trait DomainService {
  import DomainService._

  def getById(id: Long): Option[Domain]
  def getByDomain(domain: String): Option[Domain]
  def getByAlias(alias: String): Option[Domain]
  def getByType(typ: String): Option[Domain]
  def getAllByType(typ: String): List[Domain]
  def update(id: Long, in: IN)
  def create(in: IN): Domain
  def delete(id: Long)
  def resolveDomain(domain: String): Domain
  def resolveRootSite(domain: String, locales: Locale*): SiteChain
  def defaultRootSite(domain: String): SiteChain
  def defaultSite(domain: String): SiteChain
  def getAll: List[Domain]
  def listSites(o: Domain): List[String]
  def search(in: SearchInput): SearchResult[Domain]
}

@Transactional
class DomainServiceImpl extends BaseService with DomainService {
  import DomainService._

  @BeanProperty var domainDao: GenericDAO[Domain] = _
  @BeanProperty var domainsCache: DomainCache[Domain] = _

  private def merge(o: Domain, in: IN) {
    o.domain = in.domain
    o.domainSite = in.domainSite.value
    o.`type` = in.`type` or Domain.TYPE_OTHER
    o.meta = in.meta
  }

  /** Get an object by its ID. */
  @Transactional (readOnly = true)
  override def getById(id: Long): Option[Domain] = domainDao.findById(id)

  /** Get an object by a domain's host (eg. 'holiday-estates.eu'). Cache-aware. */
  @Transactional (readOnly = true)
  override def getByDomain(domain: String): Option[Domain] =
    cacheAwareResults[Domain](domainsCache, domain, { domainDao.findByProperty("domain", domain) }).headOption

  /** Get an object by a domain's name or alias (eg. 'example.org' or 'www.example.org'). Cache-aware. */
  @Transactional (readOnly = true)
  override def getByAlias(alias: String): Option[Domain] =
    getByDomain(alias) orElse getByDomain(alias.dropWhile('.' !=).drop(1)).filter(_.aliases.contains(alias)) // www.example.org -> example.org

  /** Get an object by a domain's type. */
  @Transactional (readOnly = true)
  override def getByType(typ: String): Option[Domain] = domainDao.findUniqueByProperty("type", typ)

  /** Get objects by a domain's type. */
  @Transactional (readOnly = true)
  override def getAllByType(typ: String): List[Domain] = domainDao.findByProperty("type", typ)

  /** Update a domain. Cache-aware. */
  @Transactional (readOnly = false)
  override def update(id: Long, in: IN) {
    val o = getById(id).getOrElse(throw new ObjectNotFoundException)
    if (getByDomain(in.domain).exists(_.id != id)) throw new ObjectExistsException
    merge(o, in)
    domainDao.saveOrUpdate(o)
    domainsCache.remove(o.domain) //remove from cache
  }

  /** Create a domain. Cache-aware. */
  @Transactional (readOnly = false)
  override def create(in: IN): Domain = {
    if (getByDomain(in.domain).isDefined) throw new ObjectExistsException
    val o = new Domain
    merge(o, in)
    domainDao.saveOrUpdate(o)
    domainsCache.remove(o.domain) //remove from cache
    o
  }

  /** Delete a domain. Cache-aware. */
  @Transactional (readOnly = false)
  override def delete(id: Long) = getById(id).foreach { x =>
    domainDao.delete(x)
    domainsCache.remove(x.domain) //remove from cache
  }

  /** Select a domain or get a default one. */
  @Transactional (readOnly = true)
  override def resolveDomain(alias: String): Domain = getByAlias(alias).getOrElse(getByDomain("*").get)

  /** Resolve root site chain based on a domain and list of user's locales (xx1.xx2).
   *  Earliest locale in a list takes priority.
   */
  @Transactional (readOnly = true)
  override def resolveRootSite(domain: String, locales: Locale*): SiteChain = {
    val o = resolveDomain(domain) //select a domain or get a default one
    val meta = Meta(o.meta)
    val site = if (meta.csv("resolve").get("site").isEmpty) None else //allowed to resolve
      (for { locale <- locales; v <- parsedSites
             if locale != null && v.locales.contains(locale.getLanguage) } yield v.site).headOption // xx2=xx1.xx2:ru|rs -> xx1.xx2

    //return resolved site or a default if resolving failed
    site.getOrElse {
      val sites = parseCSVMap(setting("Sites")) // xx1=xx1:en, xx2=xx1.xx2:ru
      meta.get("site").map(site => SiteChain(sites(site).split(":")(0))).getOrElse(defaultSite) // xx2 -> xx2=xx1.xx2:ru|rs -> xx1.xx2
    }
  }

  /** Return default root site chain based on a domain (xx1.xx2). */
  @Transactional (readOnly = true)
  override def defaultRootSite(domain: String): SiteChain = {
    val o = resolveDomain(domain) //select a domain or get a default one
    val meta = Meta(o.meta)
    val sites = parseCSVMap(setting("Sites"))
    meta.get("site").map(site => SiteChain(sites(site).split(":")(0))).getOrElse(defaultSite) // xx2=xx1.xx2:ru|rs -> xx1.xx2
  }

  /** Return default site chain based on a domain (xx1.pp1.wis1). */
  @Transactional (readOnly = true)
  override def defaultSite(domain: String): SiteChain = {
    val o = resolveDomain(domain) //select a domain or get a default one
    mixRootPureSites(defaultRootSite(o.domain), SiteChain(o.domainSite)) // xx1.xx2+ph.zx.mw -> xx1.ph1.zx1.mw1.xx2.ph2.zx2.mw2
  }

  /** Return all domains. */
  @Transactional (readOnly = true)
  override def getAll: List[Domain] = domainDao.findAll

  /** Return list of domain sites (wis1, wis2). */
  @Transactional (readOnly = true)
  override def listSites(o: Domain): List[String] = {
    val rootSites = parsedSites.map(_.key)
    rootSites.map(x => mixRootPureSites(SiteChain(x), SiteChain(o.domainSite)).key) // xx2+ph.zx.mw -> xx2.ph2.zx2.mw2 -> mw2
  }

  @Transactional (readOnly = true)
  override def search(in: SearchInput): SearchResult[Domain] = {
    val query = "from "+classOf[Domain].getName+" "+in.orderByClause
    searchObjects(in.from, in.max, domainDao, query)
  }
}
