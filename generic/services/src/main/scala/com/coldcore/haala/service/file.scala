package com.coldcore.haala
package service

import beans.BeanProperty
import dao.GenericDAO
import org.springframework.transaction.annotation.Transactional
import com.coldcore.misc5.CFile
import domain.File
import java.io.{BufferedInputStream, FileInputStream, FilenameFilter, InputStream, File => JFile}
import core._
import core.GeneralUtil.generateRef
import exceptions.{ObjectExistsException, ObjectNotFoundException}
import SolrService._

object FileService {
  case class IN(path: VirtualPath, site: String)

  class SearchInputX(from: Long, max: Int, orderBy: String = "") extends SearchInput(from, max, orderBy) {
    def this(in: SearchInput) = this(in.from, in.max, in.orderBy)

    val queryAnd = ""
    val filterValue = ""
    val siteKeys = List.empty[String]
  }

  /** File body abstraction from HDD */
  trait Body {
    def exists: Boolean
    def size: Long
    def name: String
    def path: String
    def copy(dest: Body)
    def delete: Boolean
    def rename(dest: Body): Boolean
    def write(x: Array[Byte])
    def write(x: InputStream)
    def sourceStream: InputStream
  }

  class JFileBody(private val source: JFile) extends Body {
    def exists: Boolean = source.exists
    def size: Long = if (exists) source.length else 0
    def name: String = source.getName
    def path: String = source.getAbsolutePath
    def copy(dest: Body) = new CFile(source).copy(dest.asInstanceOf[JFileBody].source)
    def delete: Boolean = !exists || source.delete
    def rename(dest: Body): Boolean = source.renameTo(dest.asInstanceOf[JFileBody].source)
    def write(x: Array[Byte]) = new CFile(source).rewriteFile(x)
    def write(x: InputStream) = new CFile(source).rewriteFile(x)
    def sourceStream: InputStream = new BufferedInputStream(new FileInputStream(source))
  }

  class BodyFactory(val sc: ServiceContainer) {
    /** Parent directory of a file on a disk.
     *  Can store up to 1.000.000.000.000(x10) files (10.000 folders per dir with 10.000(x10) files to avoid FS limits).
     */
    private def disk(id: Long): JFile = {
      val (dirA, dirB) = ((id/10000L/10000L).hex, (id/10000L).hex)
      val jd = new JFile(sc.get[SettingService].querySystem("FsPath"), "data/"+dirA+"/"+dirB)
      if (!jd.exists) jd.mkdirs //ensure a directory exists
      jd
    }

    /** Return a body created from disk file path. */
    def make(path: String): Body = new JFileBody(new JFile(path))

    /** Return a body (with a Java File object which may not exist). Inputs: a file and optional body suffix. */
    def make(o: File, sfx: String = File.SUFFIX_DEFAULT): Body =
      new JFileBody(new JFile(disk(o.id), o.id.hex+"."+sfx))

    /** Return all bodies for a file found on disk. */
    def bodies(o: File): List[Body] =
      disk(o.id).listFiles(new FilenameFilter {
        override def accept(d: JFile, name: String): Boolean = name.startsWith(o.id.hex+".")
      }).map(new JFileBody(_)).toList
  }
}

trait FileService {
  import FileService._

  def prepareSolrDoc(o: File): List[Any]
  def body(path: String): Body
  def body: Body
  def getBody(o: File, sfx: String = File.SUFFIX_DEFAULT): Body
  def getById(id: Long): Option[File]
  def getByBody(b: Body): Option[File]
  def copy(x: Body): Body
  def copyBody(o: File, sfx: String = File.SUFFIX_DEFAULT): Body
  def attachBody(id: Long, sfx: String, b: Body)
  def detachBody(id: Long, sfx: String)
  def getByPath(path: VirtualPath): List[File]
  def getByPath(path: VirtualPath, site: SiteChain): Option[File]
  def getByFolder(path: VirtualPath, site: SiteChain): List[File]
  def deleteByFolder(path: VirtualPath, deep: Boolean = true)
  def getByFolder(path: VirtualPath, deep: Boolean = true): List[File]
  def delete(id: Long)
  def update(id: Long, in: IN, b: Body = null)
  def update(id: Long, in: IN, data: Array[Byte])
  def create(in: IN, b: Body): File
  def create(in: IN, data: Array[Byte]): File
  def create(path: VirtualPath, site: String, b: Body): File
  def create(path: VirtualPath, site: String, data: Array[Byte]): File
  def searchFast(in: SearchInput): SearchResult[File]
  def search(in: SearchInputX): SearchResult[File]
  def refresh(id: Long)
  def rename(id: Long, path: VirtualPath)
}

@Transactional
class FileServiceImpl extends BaseService with FileService {

  import FileService._

  @BeanProperty var fileDao: GenericDAO[File] = _
  @BeanProperty var filesCache: DomainCache[File] = _

  lazy val bodyFactory: BodyFactory = new BodyFactory(sc)

  private val solrTyp = Constants.SOLR_TYPE_FILE
  private val solrId = SolrService.solrId(solrTyp, _: Long)
  private val solrDoc = SolrService.solrDoc(solrTyp, _: Long)
  private val solrSort = SolrService.solrSort(Constants.SOLR_PREFIX_FILE, _: String)

  /** Prepare Solr doc. */
  @Transactional(readOnly = true)
  override def prepareSolrDoc(o: File): List[Any] =
    solrDoc(o.id) ::: List('of_fld, o.folder, 'of_nam, o.name, 'of_sit, o.site)

  /** Submit an object to Solr. */
  private def doSolrSubmit(o: File) = sc.get[SolrService].submit(prepareSolrDoc(o))

  /** Delete an object from Solr. */
  private def doSolrDelete(o: File) = sc.get[SolrService].deleteByIds(solrId(o.id))

  private def merge(o: File, in: IN) {
    o.folder = in.path.folder
    o.name = in.path.filename
    o.site = in.site
  }

  /** Return a body created from file path on disk. */
  @Transactional(readOnly = true)
  override def body(path: String): Body = bodyFactory.make(path)

  @Transactional(readOnly = true)
  override def body: Body = body(setting("FsPath")+"/tmp/"+generateRef("|hms|.|sym10|").toLowerCase+".tmp")

  @Transactional(readOnly = true)
  override def getBody(o: File, sfx: String = File.SUFFIX_DEFAULT): Body = bodyFactory.make(o, sfx)

  /** Get an object by its ID. */
  @Transactional(readOnly = true)
  override def getById(id: Long): Option[File] = fileDao.findById(id)

  /** Get an object by its body. */
  @Transactional(readOnly = true)
  override def getByBody(b: Body): Option[File] =
    getById(java.lang.Long.parseLong(b.name.takeWhile('.' !=), 16))

  /** Copy file's body into a temporary body. */
  @Transactional(readOnly = true)
  override def copyBody(o: File, sfx: String = File.SUFFIX_DEFAULT): Body = copy(getBody(o, sfx))

  /** Copy a body into a temporary body. */
  @Transactional(readOnly = true)
  override def copy(b: Body): Body = {
    val t = body; b.copy(t); t
  }

  /** Try several times to delete a body as a drive may refuse to do so, log a message if failed. */
  private def delete(b: Body) = {
    val f = () => {
      val x = b.delete; if (!x) Thread.sleep(100); x
    }
    if (!(1 to 100).exists(_ => f())) log.warn("Cannot remove " + b.path)
  }

  /** Try several times to rename a body as a drive may refuse to do so, log a message if failed. */
  private def rename(b: Body, dest: Body) {
    delete(dest)
    val f = () => {
      val x = b.rename(dest); if (!x) Thread.sleep(100); x
    }
    if (!(1 to 100).exists(_ => f())) log.warn("Cannot rename " + b.path + " to " + dest.path)
  }

  /** Attach a body to a file. Will overwrite an existing body with the same suffix if exist.
    * "b = null" will remove the existing body without attaching a new one.
    */
  @Transactional(readOnly = false)
  override def attachBody(id: Long, sfx: String, b: Body) {
    val o = getById(id).getOrElse(throw new ObjectNotFoundException)
    if (sfx == File.SUFFIX_DEFAULT && b == null) throw new CoreException("Cannot delete default body")
    delete(getBody(o, sfx))
    Option(b).foreach(_ => rename(b, getBody(o, sfx)))

    if (sfx == File.SUFFIX_DEFAULT) {
      o.updated = Timestamp()
      o.size = getBody(o, sfx).size
    } else { // keep file body suffixes in "meta"
      val msfx = Meta(o.meta).csv("sfx")
      Option(b) match { // add or remove suffix
        case Some(_) => msfx + sfx
        case None => msfx - sfx
      }
      o.meta = (if (msfx.imap.nonEmpty) Meta(o.meta) + ("sfx", msfx) else Meta(o.meta) - "sfx").serialize
    }

    fileDao.saveOrUpdate(o)
    filesCache.remove(o.getPath) //remove from cache
    doSolrSubmit(o) //update Solr
  }

  /** Detach a body from a file. */
  @Transactional(readOnly = true)
  override def detachBody(id: Long, sfx: String) = attachBody(id, sfx, null)

  /** Return objects by its virtual folder and name. Cache-aware. */
  @Transactional(readOnly = true)
  override def getByPath(path: VirtualPath): List[File] =
    cacheAwareResults[File](filesCache, path.path, {
      fileDao.findByProperties(Map("folder" -> path.folder, "name" -> path.filename))
    })

  /** Return an object by its virtual folder and name. Cache-aware. */
  @Transactional(readOnly = true)
  override def getByPath(path: VirtualPath, site: SiteChain): Option[File] = filterObjectForSite(getByPath(path), site)

  /** Find objects by a folder and site. */
  @Transactional(readOnly = true)
  override def getByFolder(path: VirtualPath, site: SiteChain): List[File] = {
    val query = "from " + classOf[File].getName + " o where o.folder = ? and " + constructWhereSite(site)
    filterObjectsForSite(fileDao.find(query, path.folder), site)
  }

  /** Delete objects from folder and sub-folders. Cache-aware. */
  @Transactional(readOnly = false)
  override def deleteByFolder(path: VirtualPath, deep: Boolean = true) {
    val query = "from " + classOf[File].getName + " o where o.folder = ?"+(if (deep) " or o.folder like ?" else "")
    for (o <- fileDao.find(query, path.folder, path.folder+"/%")) delete(o.id)
  }

  /** Find objects by folder and sub-folders. */
  @Transactional (readOnly = true)
  override def getByFolder(path: VirtualPath, deep: Boolean = true): List[File] = {
    val query = "from " + classOf[File].getName + " o where o.folder = ?"+(if (deep) " or o.folder like ?" else "")
    fileDao.find(query, path.folder, path.folder+"/%")
  }

  /** Delete a file. Cache-aware.
    * Bodies (Java File) deleted by a task as files may have multiple bodies or none.
    */
  @Transactional(readOnly = false)
  override def delete(id: Long) = getById(id) match {
    case Some(x) =>
      fileDao.delete(x)
      filesCache.remove(x.getPath) //remove from cache
      doSolrDelete(x) //update Solr
    case _ =>
  }

  /** Update an object and an optional body. Cache-aware.
    * If a new body is supplied, the existing body will be overwritten.
    */
  @Transactional(readOnly = false)
  override def update(id: Long, in: IN, b: Body = null) {
    val o = getById(id).getOrElse(throw new ObjectNotFoundException)
    if (getByPath(in.path, SiteChain.key(in.site)).exists(_.id != id)) throw new ObjectExistsException
    merge(o, in)
    o.updated = Timestamp()

    //re-attach a body
    if (b != null) {
      o.size = b.size
      rename(b, getBody(o))
    }
    fileDao.saveOrUpdate(o)
    filesCache.remove(o.getPath) //remove from cache
    doSolrSubmit(o) //update Solr
  }

  @Transactional(readOnly = false)
  override def update(id: Long, in: IN, data: Array[Byte]) {
    val b = body
    b.write(data)
    update(id, in, b)
  }

  /** Rename a file. Cache-aware. */
  @Transactional(readOnly = false)
  override def rename(id: Long, path: VirtualPath) {
    val o = getById(id).getOrElse(throw new ObjectNotFoundException)
    update(id, IN(path, o.site))
  }

  /** Create an object and a body (a Java File or a byte array). Cache-aware.
    * There is also a helper method which accepts a path and site instead of IN.
    */
  @Transactional(readOnly = false)
  override def create(in: IN, b: Body): File = {
    if (getByPath(in.path, SiteChain.key(in.site)).isDefined) throw new ObjectExistsException
    val o = new File
    merge(o, in)
    o.size = b.size
    fileDao.saveOrUpdate(o) //acquire ID for body
    filesCache.remove(o.getPath) //remove from cache
    rename(b, getBody(o))
    doSolrSubmit(o) //update Solr
    o
  }

  @Transactional(readOnly = false)
  override def create(path: VirtualPath, site: String, b: Body): File =
    create(IN(path, site), b)

  @Transactional(readOnly = false)
  override def create(in: IN, data: Array[Byte]): File = {
    val b = body
    b.write(data)
    create(in, b)
  }

  @Transactional(readOnly = false)
  override def create(path: VirtualPath, site: String, data: Array[Byte]): File = {
    val b = body
    b.write(data)
    create(path, site, b)
  }

  /** Fast search without filtering. */
  @Transactional(readOnly = true)
  override def searchFast(in: SearchInput): SearchResult[File] = {
    val query = "from " + classOf[File].getName + " " + in.orderByClause
    searchObjects(in.from, in.max, fileDao, query)
  }

  /** Solr search */
  @Transactional(readOnly = true)
  override def search(in: SearchInputX): SearchResult[File] = {
    val result = sc.get[SolrService].search(
      "o_typ:" + solrTyp +
        (if (in.queryAnd != "") " AND " + in.queryAnd else "") +
        (if (in.siteKeys.nonEmpty) " AND (" + in.siteKeys.map("of_sit:" + _).mkString(" OR ") + ")" else "") +
        (if (in.filterValue != "") " AND " + solrEscapeChars(in.filterValue) else ""),
      'fl, "id", 'start, in.from, 'rows, in.max, 'sort, solrSort(in.orderBy))
    sc.get[SolrService].fetchObjects(result, fileDao, classOf[File])
  }

  /** Refresh file size and meta to match its bodies values */
  @Transactional(readOnly = false)
  override def refresh(id: Long) {
    val o = getById(id).getOrElse(throw new ObjectNotFoundException)
    val (dbody, bodies) = bodyFactory.bodies(o).partition(_.name.endsWith("."+File.SUFFIX_DEFAULT))

    o.size = dbody.head.size // default body size

    val msfx = MetaCSV("")
    bodies.foreach(b => msfx + b.name.dropWhile('.' !=).drop(1)) // add suffix to meta
    o.meta = (if (msfx.imap.nonEmpty) Meta(o.meta) + ("sfx", msfx) else Meta(o.meta) - "sfx").serialize

    fileDao.saveOrUpdate(o)
    filesCache.remove(o.getPath) //remove from cache
  }
}