package com.coldcore.haala
package service

import dao.GenericDAO
import beans.BeanProperty
import org.springframework.transaction.annotation.{Propagation, Transactional}
import core.{Constants, Meta, Timestamp}
import core.GeneralUtil.generateRef
import domain.AbyssEntry
import service.SolrService._
import exceptions.ObjectNotFoundException

object AbyssService {
  case class ValueMetaIN(value: String = "", meta: String = "")
  case class CreateIN(ref: String, pack: String, vm: ValueMetaIN, keep: Long)

  class SearchInputX(from: Long, max: Int, orderBy: String = "") extends SearchInput(from, max, orderBy) {
    def this(in: SearchInput) = this(in.from, in.max, in.orderBy)

    val ref: Option[String] = None
    val pack: Option[String] = None
    val dynamicTerms = List.empty[Term] // ValueTerm(s_usr, foo)
    val sortSub = Map.empty[String,String] // username -> s_usr
  }
}

trait AbyssService {
  import AbyssService._

  def getById(id: Long): Option[AbyssEntry]
  def update(id: Long, meta: String)
  def updateNow(id: Long, meta: String)
  def update(id: Long, in: ValueMetaIN)
  def updateNow(id: Long, in: ValueMetaIN)
  def create(in: CreateIN): AbyssEntry
  def createNow(in: CreateIN): AbyssEntry
  def create(parentId: Long, in: ValueMetaIN): AbyssEntry
  def createNow(parentId: Long, in: ValueMetaIN): AbyssEntry
  def extend(id: Long, keep: Long)
  def delete(id: Long)
  def deleteNow(ids: List[Long])
  def searchFast(in: SearchInput): SearchResult[AbyssEntry]
  def searchExpired(in: SearchInput): SearchResult[AbyssEntry]
  def search(in: SearchInputX): SearchResult[AbyssEntry]
  def prepareSolrDoc(o: AbyssEntry): List[Any]
}

@Transactional
class AbyssServiceImpl extends BaseService with AbyssService {
  import AbyssService._

  @BeanProperty var abyssEntryDao: GenericDAO[AbyssEntry] = _

  private val solrType = Constants.SOLR_TYPE_ABYSS
  private val solrId = SolrService.solrId(solrType, _: Long)
  private val solrDoc = SolrService.solrDoc(solrType, _: Long)
  private def solrSort = SolrService.solrSort(Constants.SOLR_PREFIX_ABYSS, _: String, _: Map[String,String])

  /** Prepare Solr doc. */
  @Transactional (readOnly = true)
  override def prepareSolrDoc(o: AbyssEntry): List[Any] = {
    val meta = Meta(o.meta)
    solrDoc(o.id) :::
    List('oa_ref, o.ref, 'oa_pac, o.pack, 'text, o.value) :::
    solrDynamicFields(Constants.SOLR_PREFIX_ABYSS, meta.imap.toMap)
  }

  /** Submit an object to Solr. */
  private def doSolrSubmit(o: AbyssEntry) = sc.get[SolrService].submit(prepareSolrDoc(o))

  /** Delete an object from Solr. */
  private def doSolrDelete(o: AbyssEntry) = sc.get[SolrService].deleteByIds(solrId(o.id))

  /** Get an object by its ID. */
  @Transactional (readOnly = true)
  override def getById(id: Long): Option[AbyssEntry] = abyssEntryDao.findById(id)

  /** Create an object. */
  @Transactional (readOnly = false)
  override def create(in: CreateIN): AbyssEntry = {
    val o = new AbyssEntry
    o.ref = in.ref or generateRef("|hms|_|sym10|")
    o.pack = in.pack
    o.value = in.vm.value
    o.meta = in.vm.meta
    o.keep = in.keep
    abyssEntryDao.saveOrUpdate(o)
    doSolrSubmit(o) //update Solr
    o
  }
  @Transactional (readOnly = false, propagation = Propagation.REQUIRES_NEW)
  override def createNow(in: CreateIN): AbyssEntry = create(in)

  /** Create an object. */
  @Transactional (readOnly = false)
  override def create(parentId: Long, in: ValueMetaIN): AbyssEntry = {
    val parent = getById(parentId).getOrElse(throw new ObjectNotFoundException)
    val o = new AbyssEntry
    o.ref = parent.ref
    o.pack = parent.pack
    o.value = in.value
    o.meta = in.meta
    o.keep = parent.keep
    abyssEntryDao.saveOrUpdate(o)
    doSolrSubmit(o) //update Solr
    o
  }
  @Transactional (readOnly = false, propagation = Propagation.REQUIRES_NEW)
  override def createNow(parentId: Long, in: ValueMetaIN): AbyssEntry = create(parentId, in)

  /** Update an object. */
  @Transactional (readOnly = false)
  override def update(id: Long, meta: String) {
    update(id, ValueMetaIN("", meta))
  }
  @Transactional (readOnly = false, propagation = Propagation.REQUIRES_NEW)
  override def updateNow(id: Long, meta: String) = update(id, meta)

  /** Update an object. */
  @Transactional (readOnly = false)
  override def update(id: Long, in: ValueMetaIN) {
    val o = getById(id).getOrElse(throw new ObjectNotFoundException)
    in.value.on { o.value = in.value; "" }
    in.meta.on { o.meta = in.meta; "" }
    o.updated = Timestamp()
    abyssEntryDao.saveOrUpdate(o)
    doSolrSubmit(o) //update Solr
  }
  @Transactional (readOnly = false, propagation = Propagation.REQUIRES_NEW)
  override def updateNow(id: Long, in: ValueMetaIN) = update(id, in)

  /** Extend keep of a single entry. */
  @Transactional (readOnly = false)
  override def extend(id: Long, keep: Long) = getById(id).foreach { o =>
    o.keep = keep
    o.updated = Timestamp()
    abyssEntryDao.saveOrUpdate(o)
    doSolrSubmit(o) //update Solr
  }

  /** Delete an object. */
  @Transactional (readOnly = false)
  override def delete(id: Long) = getById(id).foreach { x =>
    //delete abyss's labels, files and an abyss itself (commented out as no code stores labels nor files at the moment)
    //labelService.deleteByKeyLike("a."+id.hex+ ".%")
    //sc.get[FileService].deleteByFolder(VirtualPath("a/"+id.hex, "*"))
    abyssEntryDao.delete(x)
    doSolrDelete(x) //update Solr
  }

  /** Delete objects. */
  @Transactional (readOnly = false, propagation = Propagation.REQUIRES_NEW)
  override def deleteNow(ids: List[Long]) = ids.foreach(delete)

  /** Fast search without filtering. */
  @Transactional (readOnly = true)
  override def searchFast(in: SearchInput): SearchResult[AbyssEntry] = {
    val query = "from "+classOf[AbyssEntry].getName+" "+in.orderByClause
    searchObjects(in.from, in.max, abyssEntryDao, query)
  }

  /** Find entries created before date. */
  @Transactional (readOnly = true)
  override def searchExpired(in: SearchInput): SearchResult[AbyssEntry] = {
    val query = "from "+classOf[AbyssEntry].getName+" where keep < ?"
    searchObjects(in.from, in.max, abyssEntryDao, query, Timestamp().toLong)
  }

  /** Solr search */
  @Transactional (readOnly = true)
  override def search(in: SearchInputX): SearchResult[AbyssEntry] = {
    val result = sc.get[SolrService].searchWT(
      SolrQuery(
        AndTerm(
          ObjectTypeTerm(solrType),

          in.ref.map(ValueTerm("oa_ref", _)).getOrElse(NoopTerm),
          in.pack.map(ValueTerm("oa_pac", _)).getOrElse(NoopTerm),

          // add dynamic terms with "oa_" prefix
          if (in.dynamicTerms.isEmpty) NoopTerm
          else AndTerm(in.dynamicTerms.map {
            case x: ValueTerm => ValueTerm("oa_"+x.key, x.value)
            case x: RangeTerm => RangeTerm("oa_"+x.key, x.from, x.to)
            case x => x // unknown term - leave as is
          }: _*)
        
        )),
        FlTerm("id"), StartTerm(in.from), RowsTerm(in.max),
        SortTerm(solrSort(in.orderBy, Map("pack" -> "pac") ++ in.sortSub)))
    sc.get[SolrService].fetchObjectsWT(result, abyssEntryDao, classOf[AbyssEntry])
  }

}
