package com.coldcore.haala
package service

import core.HttpUtil.{HttpCallFailed, HttpCallSuccessful, httpGet}
import core._
import PaymentService.{PaymentEntry, Handler}
import exceptions._
import com.coldcore.misc.scala.StringOp.toX100
import domain.AbyssEntry

import beans.BeanProperty
import com.google.gson.{Gson, JsonSyntaxException}
import org.apache.http.HttpStatus

/** PayPal auth
  * https://developer.paypal.com/docs/api/overview
  */
trait PaypalAuth {
  self: PaypalHandler =>
  import PaypalHandler._

  /** PayPal auth JSON response */
  private class AuthResponse {
    @BeanProperty var access_token: String = _ // A21AAGyRfrXhIXe83k7nE3R...
    @BeanProperty var expires_in: Int = _ // 32400
  }

  /** Read access token from settings and validate if expired */
  private def accessToken: Option[AccessToken] = {
    val meta = Meta(sc.get[SettingService].querySystem("Payment/paypal_token_"+paypalConfig.name).safe)
    val at = AccessToken(meta("token", ""), meta("created", "0").toLong, meta("expires", "0").toLong)
    if (at.token != "" && Timestamp().compareTo(Timestamp(at.expires)) < 0) Some(at) else None
  }

  /** Fetch new token from vendor and store it in system setting */
  private def fetchToken(urlPrefix: String, key: ClientKey): AccessToken =
    sb.httpPost(s"$urlPrefix/v1/oauth2/token",
      Map("grant_type" -> "client_credentials"),
      ("Authorization", "Basic "+(key.clientId+":"+key.secret).encodeBase64) :: Nil) match {
      case x: HttpCallSuccessful if x.code == HttpStatus.SC_OK =>
        val json = new Gson().fromJson(x.body, classOf[AuthResponse])
        val (token, created, expires) = (json.access_token, Timestamp(), Timestamp().addSeconds(json.expires_in))
        val at = AccessToken(token, created, expires)
        val meta = Meta("") + (at.serializeAsMap.map { case (k,v) => k.name -> v.toString }.toList: _*)
        sc.get[SettingService].write("Payment/paypal_token_"+paypalConfig.name, meta.serialize, Constants.SITE_SYSTEM)
        at
      case x: HttpCallSuccessful =>
        LOG.warn(s"${x.code} ${x.url}\n"+x.body)
        throw new AuthException
      case x: HttpCallFailed =>
        LOG.error("Paypal API failed", x.error)
        throw new ExternalFailException
    }

  /** Authorization header for HTTP call
    * passive - if "true" and non-expired token exists then it will be used
    *           otherwise fetch a new access token from vendor
    */
  def authorization(key: ClientKey, passive: Boolean = true): (String,String) = {
    val at = accessToken
    val urlPrefix = if (key.sandbox) paypalConfig.sandbox_url else paypalConfig.production_url
    val x = if (passive && at.isDefined) at.get.token else fetchToken(urlPrefix, key).token
    ("Authorization", "Bearer "+x)
  }
}

/** PayPal verify payment
  * https://developer.paypal.com/docs/api/webhooks/v1/#event_list
  */
trait PaypalVerify {
  self: PaypalHandler =>
  import PaypalHandler._

  /** Retrieve transaction content from vendor (authorize if needed) */
  private def txContent(urlPrefix: String, txId: String, key: ClientKey, auth: Boolean = false): String =
    sb.httpGet(s"$urlPrefix/v1/notifications/webhooks-events/$txId",
      authorization(key, passive = !auth) :: Nil) match {
      case x: HttpCallSuccessful if x.code == HttpStatus.SC_OK => x.body
      case x: HttpCallSuccessful if x.code == HttpStatus.SC_UNAUTHORIZED =>
        if (!auth) txContent(urlPrefix, txId, key, auth = true) // same call with authorization
        else throw new AuthException
      case x: HttpCallSuccessful =>
        LOG.warn(s"${x.code} ${x.url}\n"+x.body)
        x.body
      case x: HttpCallFailed =>
        LOG.error("Paypal API failed", x.error)
        throw new ExternalFailException
    }

  /** Verify transaction */
  def verifyTx(event: EventHead, key: ClientKey): Boolean = {
    val urlPrefix = if (key.sandbox) paypalConfig.sandbox_url else paypalConfig.production_url
    val tx = txContent(urlPrefix, event.id, key).safe
    try {
      val txEvent = new Gson().fromJson(tx, classOf[EventHead]) 
      Option(txEvent).exists(_.sameAs(event)) // compare with content retrieved from vendor
    } catch {
      case e: JsonSyntaxException => false
    }
  }
}

object PaypalHandler {

  /** Client ID and secret key */
  case class ClientKey(clientId: String, secret: String, sandbox: Boolean = false)

  /** Auth access token */
  case class AccessToken(token: String, created: Long, expires: Long) {
    def serializeAsMap: Map[Symbol,Any] = Map('token-> token, 'created -> created, 'expires -> expires)
  }

  /** Payment config JSON */
  class PaymentConfig {
    @BeanProperty var paypal: Paypal = _
    class Paypal {
      @BeanProperty var enabled: String = _
      @BeanProperty var name: String = _
      @BeanProperty var env: String = _
      @BeanProperty var production_url: String = _
      @BeanProperty var production_client: String = _
      @BeanProperty var production_secret: String = _
      @BeanProperty var sandbox_url: String = _
      @BeanProperty var sandbox_client: String = _
      @BeanProperty var sandbox_secret: String = _
    }
  }

  /** refer to "event-payment.sale.completed.json" */
  class EventHead {
    @BeanProperty var id: String = _ // WH-066332897V146121C-7FT48942H5416970K
    @BeanProperty var create_time: String = _ // 2017-05-08T15:21:37.846Z
    @BeanProperty var resource_type: String = _ // sale
    @BeanProperty var event_type: String = _ // PAYMENT.SALE.COMPLETED
    @BeanProperty var summary: String = _ // Payment completed for GBP 17.5 GBP

    // extra methods
    def paymentCompleted: Boolean = event_type == "PAYMENT.SALE.COMPLETED"
    def sameAs(e: EventHead): Boolean =
      id == e.id && create_time == e.create_time &&
        resource_type == e.resource_type && event_type == e.event_type && summary == e.summary
  }

  /** refer to "event-payment.sale.completed.json" */
  class PaymentCompletedEvent extends EventHead {
    @BeanProperty var resource: Resource = _
    class Resource {
      @BeanProperty var invoice_number: String = _ // ref=A087DECDA06F

      @BeanProperty var amount: Amount = _
      class Amount {
        @BeanProperty var total: String = _ // 17.50
        @BeanProperty var currency: String = _ // GBP

        @BeanProperty var details: Details = _
        class Details {
          @BeanProperty var subtotal: String = _ // 17.50
        }
      }
    }

    @BeanProperty var links: Array[Links] = _
    class Links {
      @BeanProperty var href: String = _ // https://api.sandbox.paypal.com/v1/notifications/webhooks-events/WH-066332897V146121C-7FT48942H5416970K
      @BeanProperty var rel: String = _ // self | resend
    }

    // extra methods
    var config: PaymentConfig#Paypal = _

    def ref: String = Meta(resource.invoice_number)("ref")
    def selfHref: String = links.find(_.rel == "self").map(_.href).get

    def productionEnv: Boolean = selfHref.startsWith(config.production_url)
    def sandboxEnv: Boolean = selfHref.contains(config.sandbox_url)

    def matchAmount(amount: Long, currency: String): Boolean = // amount x100
      resource.amount.total == toX100(amount) && resource.amount.currency == currency &&
        resource.amount.total == resource.amount.details.subtotal
  }

}

/** PayPal vendor handler
  * https://developer.paypal.com/docs/integration/direct/webhooks/rest-webhooks
  */
class PaypalHandler extends Handler with Dependencies with PaypalAuth with PaypalVerify {
  import PaypalHandler._

  lazy val LOG = new NamedLog("paypal-handler", log)

  /** Payment configuration */
  def paypalConfig: PaymentConfig#Paypal =
    new Gson().fromJson(sc.get[SettingService].querySystem("PaymentConf"), classOf[PaymentConfig]).paypal

  /** Read client data from configuration */
  private def clientKey(sandbox: Boolean = false): ClientKey =
    if (sandbox) ClientKey(paypalConfig.sandbox_client, paypalConfig.sandbox_secret, sandbox = true)
    else ClientKey(paypalConfig.production_client, paypalConfig.production_secret)

  private def addTxEntry(paymentEntry: PaymentEntry, content: String, typ: String): AbyssEntry =
    sc.get[AbyssService].createNow(paymentEntry.abyss.id,
      AbyssService.ValueMetaIN(value = content, meta = "vendor=paypal;verify=pending;type="+typ))

  /** Process event: payment sale completed  */
  protected def paymentCompleted(event: PaymentCompletedEvent, content: String, paymentEntry: PaymentEntry, eventAbyssId: Long) {
    val tx = addTxEntry(paymentEntry, content, "sale")
    val env = if (event.productionEnv) "production" else if (event.sandboxEnv) "sandbox" else "?"
    val sandbox = env == "sandbox"
    val verify =
      if (env == "?") "unable" // cannot verify without env
      else if (verifyTx(event, clientKey(sandbox))) { // verify transaction then do amount check
        if (event.matchAmount(paymentEntry.amount, paymentEntry.currency)) "valid" else "mismatch"
      } else "invalid"
    sc.get[AbyssService].updateNow(tx.id, (Meta(tx.meta) + ("verify" -> verify) + ("env" -> env)).serialize)

    LOG.info(s"Handled payment completed, event #$eventAbyssId, ref ${paymentEntry.ref}, verify $verify, env $env")

    if (verify == "valid") // notify and push valid payments
      try {
        sc.get[PaymentService].message(paymentEntry.abyss.id)
        sc.get[PaymentService].push(paymentEntry.abyss.id, test = sandbox)
      } catch {
        case _: InvalidStatusException => LOG.warn("Push failed (invalid status), event #" + eventAbyssId)
        case _: NotSupportedException => LOG.warn("Push failed (not supported), event #" + eventAbyssId)
        case e: Throwable => LOG.error("Push failed (error), event #" + eventAbyssId, e)
      }
  }

  /** Inbound event from PayPal */
  override def onEvent(eventAbyssId: Long) {
    val content = sc.get[AbyssService].getById(eventAbyssId).get.value
    val eventHead = new Gson().fromJson(content, classOf[EventHead])

    if (eventHead.paymentCompleted) { // payment completed event
      val event = new Gson().fromJson(content, classOf[PaymentCompletedEvent]) |< { e => e.config = paypalConfig }
      sc.get[PaymentService].searchByRef(event.ref) match {
        case Some(paymentEntry) if paymentEntry.closed => LOG.warn("Closed payment entry, event #" + eventAbyssId)
        case Some(paymentEntry) => paymentCompleted(event, content, paymentEntry, eventAbyssId)
        case None => LOG.warn("Payment entry not found, event #" + eventAbyssId)
      }
    } else {
      LOG.warn("Unsupported event #" + eventAbyssId)
    }
  }

}
