package com.coldcore.haala
package service

import com.coldcore.haala.service.SolrService.ValueTerm
import core.{Meta, SiteChain, Timestamp}
import core.Constants._
import core.GeneralUtil.configToMills
import com.coldcore.misc.scala.StringOp._

import beans.BeanProperty
import exceptions.ObjectNotFoundException
import com.google.gson.Gson
import org.springframework.transaction.annotation.Transactional
import domain.{AbyssEntry, User}

object MessageService {
  case class PaymentIN(ref: String, abyssId: Long, userId: Long)
  case class FeedbackIN(text: String, userId: Option[Long], name: String, email: String, domain: String, site: SiteChain)
  case class ErrorIN(text: String)
  case class CreateIN(typ: Int, text: String, meta: String = "", pack: String = "", site: SiteChain = SiteChain.empty)

  class SearchInputX(from: Long, max: Int, orderBy: String = "") extends SearchInput(from, max, orderBy) {
    def this(in: SearchInput) = this(in.from, in.max, in.orderBy)

    val ref: Option[String] = None
    val userId: Option[Long] = None
    val read: Option[Boolean] = None
    val forward: Option[Boolean] = None
  }

  /** Message config JSON */
  class MessageConfig {
    @BeanProperty var keep: String = _ // time to keep abyss record: 30d, 90d
  }

  /** Message entry Abyss wrapper */
  class MessageEntry(val abyss: AbyssEntry, sc: ServiceContainer) {
    def meta(key: String): String = Meta(abyss.meta)(key)
    def meta(key: String, default: => String): String = Meta(abyss.meta)(key, default)

    val typ = meta("i_mtp").toInt // type: 1, 2, 3
    val subject = meta("s_sub") // pack+type for sorting and label keys: 1, 2, 3 or mypack1, mypack2, mypack3
    val text = abyss.value
    val read = meta("s_red").isTrue

    lazy val site = SiteChain(meta("site", sc.get[SettingService].getDefaultSite.value))
    lazy val user: User = sc.get[UserService].getById(meta("l_usr").toLong).getOrElse(throw new ObjectNotFoundException)
  }
  object MessageEntry {
    def apply(abyss: AbyssEntry, sc: ServiceContainer) = new MessageEntry(abyss, sc)
  }
}

trait MessageService {
  import MessageService._

  def getById(abyssId: Long): Option[MessageEntry]
  def create(userId: Long, in: CreateIN): MessageEntry
  def read(abyssId: Long)
  def forwarded(abyssId: Long)
  def clean(userId: Long)
  def search(in: SearchInputX): SearchResult[MessageEntry]
  def searchByRef(x: String): Option[MessageEntry]
  def payment(in: PaymentIN): MessageEntry
  def feedback(userId: Long, in: FeedbackIN): MessageEntry
  def feedback(in: FeedbackIN): MessageEntry
  def error(in: ErrorIN): MessageEntry
}

@Transactional
class MessageServiceImpl extends BaseService with MessageService {
  import MessageService._

  private val sorlDynamicType = "msg"

  /** Message configuration */
  private def messageConfig: MessageConfig =
    new Gson().fromJson(setting("MessageConf"), classOf[MessageConfig])

  /** Get an object by its ID. */
  @Transactional (readOnly = true)
  override def getById(abyssId: Long): Option[MessageEntry] =
    sc.get[AbyssService].getById(abyssId).map(MessageEntry(_, sc))

  /** Create a message. */
  @Transactional (readOnly = false)
  override def create(userId: Long, in: CreateIN): MessageEntry = {
    val user = sc.get[UserService].getById(userId).getOrElse(throw new ObjectNotFoundException)
    val keep = Timestamp.fromMills(System.currentTimeMillis+configToMills(messageConfig.keep))

    val forward = Meta(user.meta)("msgfwd", "y").isTrue
    val meta = (Meta(in.meta) ? (in.site.nonEmpty, "site", in.site.value) ? (forward, "s_fwd", "y")) +
      ("s_typ", sorlDynamicType) +
      ("l_usr", user.id) +
      ("s_red", "n") +
      ("i_mtp", in.typ) +
      ("s_sub", in.pack+in.typ)

    val abyss = sc.get[AbyssService].create(AbyssService.CreateIN("", in.pack,
      AbyssService.ValueMetaIN(value = in.text, meta = meta.serialize), keep))

    MessageEntry(abyss, sc)
  }

  /** Mark as read. */
  @Transactional (readOnly = false)
  override def read(abyssId: Long) {
    val abyss = sc.get[AbyssService].getById(abyssId).getOrElse(throw new ObjectNotFoundException)
    val meta = Meta(abyss.meta) + ("s_red" -> "y")
    sc.get[AbyssService].update(abyssId, meta.serialize)
  }

  /** Mark as forwarded. */
  @Transactional (readOnly = false)
  override def forwarded(abyssId: Long) {
    val abyss = sc.get[AbyssService].getById(abyssId).getOrElse(throw new ObjectNotFoundException)
    val meta = Meta(abyss.meta) - "s_fwd"
    sc.get[AbyssService].update(abyssId, meta.serialize)
  }

  /** Remove read messages. */
  @Transactional (readOnly = true)
  override def clean(userId: Long) {
    val user = sc.get[UserService].getById(userId).getOrElse(throw new ObjectNotFoundException)
    val batch = parseCSVMap(setting("SystemParams"))("batch").toInt

    Iterator.fill(100)(search(new SearchInputX(0, batch) { override val userId = Some(user.id) }))
      .find { result =>
        sc.get[AbyssService].deleteNow(result.objects.map(_.abyss.id.toLong))
        result.objects.isEmpty
      }
  }

  /** Solr search */
  @Transactional (readOnly = true)
  override def search(in: SearchInputX): SearchResult[MessageEntry] = {
    val result = sc.get[AbyssService].search(new AbyssService.SearchInputX(in.from, in.max, in.orderBy) {
      override val ref = in.ref
      override val dynamicTerms =
        ValueTerm("s_typ", sorlDynamicType) :: (
          in.userId.map(ValueTerm("l_usr", _)) ::
          in.forward.map(x => ValueTerm("s_fwd", x ? "y" | "n")) ::
          in.read.map(x => ValueTerm("s_red", x ? "y" | "n")) ::
          Nil).flatten
      override val sortSub = Map("read" -> "s_red", "subject" -> "s_sub", "created" -> "id")
    })
    SearchResult(result.total, result.objects.map(MessageEntry(_, sc)))
  }

  /** Solr search by REF */
  @Transactional (readOnly = true)
  override def searchByRef(x: String): Option[MessageEntry] =
    search(new SearchInputX(0,1) { override val ref = Some(x) }).objects.headOption

  /** System sends payment notification to system user. */
  @Transactional (readOnly = false)
  override def payment(in: PaymentIN): MessageEntry = {
    val meta = s"paymentRef=${in.ref};abyssId=${in.abyssId};userId=${in.userId};"
    create(sc.get[UserService].getSystemUser.id, CreateIN(MESSAGE_TYPE_PAYMENT, text = "", meta))
  }

  /** Feedback from a user to another user. */
  @Transactional (readOnly = false)
  override def feedback(userId: Long, in: FeedbackIN): MessageEntry = {
    val user = sc.get[UserService].getById(userId).getOrElse(throw new ObjectNotFoundException)
    val meta = (Meta("") ? (in.userId.isDefined, "senderId", in.userId.get.toString)) +
      ("replyName" -> in.name) +
      ("replyEmail" -> in.email) +
      ("domain" -> in.domain)
    create(userId, CreateIN(MESSAGE_TYPE_FEEDBACK, in.text, meta.serialize, site = in.site))
  }

  /** User sends feedback to system user. */
  @Transactional (readOnly = false)
  override def feedback(in: FeedbackIN): MessageEntry =
    feedback(sc.get[UserService].getSystemUser.id, in)

  /** Error report to system user. */
  @Transactional (readOnly = false)
  override def error(in: ErrorIN): MessageEntry =
    create(sc.get[UserService].getSystemUser.id, CreateIN(MESSAGE_TYPE_ERROR, in.text))

}
