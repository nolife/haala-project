package com.coldcore.haala
package service

import core._
import core.GeneralUtil._
import com.coldcore.misc.scala.StringOp._
import com.coldcore.misc5.IdGenerator.generate
import beans.BeanProperty
import dao.GenericDAO
import org.springframework.transaction.annotation.Transactional
import exceptions._
import scala.collection.JavaConverters._
import domain._
import SolrService._

object UserService {
  case class IN(username: String, password: String, email: String, firstName: String, lastName: String,
                primaryNumber: String, salutation: String, address: AddressIN)
  case class AddressIN(line1: String, line2: String, town: String, postcode: String, country: String)
  case class UpdatePropsIN(username: String, disabled: String, activated: String,
                           domains: List[String], roles: List[String])

  /** Handle actions on a user */
  trait Mod {
    /** Add pack to a user (called from a task to add missing packs to all users) */
    def addPack(o: User) {}

    /** Delete a user */
    def delete(o: User) {}

    /** User submitted to Solr (not called from a task submitting all users) */
    def submit(o: User) {}

    /** User registration (domain specific) */
    def register(o: User, domain: Domain) {}

    /** User login (domain specific) */
    def login(o: User, domain: Domain) {}

    /** User accepted T&C (domain specific) */
    def accept(o: User, domain: Domain) {}
  }

  /** Pack data such as roles and domains */
  trait ModPack {
    self: Mod { val sc: ServiceContainer } =>

    /** Pack name */
    protected val pack: String = ""

    /** Roles to assign */
    protected val assignRoles: List[String] = Nil

    /** All domains related to a pack */
    protected def packDomains: List[Domain] = Nil

    /** Check if a domain matches pack domains */
    protected def packDomainMatch(domain: Domain): Boolean = packDomains.exists(domain ==)

    /** Assign roles to a user if missing */
    protected def assignMissingRoles(o: User) {
      val roles = sc.get[UserService].getRoles(assignRoles)
      roles.foreach(role => if (!o.roles.contains(role)) o.roles.add(role))
    }

  }

  /** T&C (terms) addition to methods (none of them called by the user service directly).
    * 1) termsOnRegister      user accepts T&C on pack-specific registration page, other packs set their T&C required to be accepted
    * 2) termsOnLogin         user logs in through pack-specific login page and accepts pack-specific T&C if required (termsOnAccept)
    * 3) termsOnAccept        user accepted T&C on pack-specific T&C page
    * 4) termsBeforeAddPack   task adds missing packs to all users and set their T&C required to be accepted (missing packs only)
    */
  trait ModTerms extends ModPack {
    self: Mod { val sc: ServiceContainer } =>

    /** Check if terms for a pack require to be accepted */
    protected def termsCheckRequired(o: User): Boolean = {
      val meta = Meta(o.meta)
      meta("terms", "").parseCSV.exists(pack ==)
    }

    /** Check if terms for a pack require to be accepted (on domain match) */
    protected def termsCheckRequired(o: User, domain: Domain): Boolean =
      packDomainMatch(domain) && termsCheckRequired(o)

    /** User login (throw exception if terms need to be accepted) */
    protected def termsOnLogin(o: User, domain: Domain) =
      if (termsCheckRequired(o, domain)) throw new UserNoAgreementException(o.ref)

    /** Accept terms and assign roles for a pack (roles assigned on terms accept only and can be removed after) */
    protected def termsAcceptAndSetRoles(o: User): Unit = {
      Some(Meta(o.meta).csv("terms"))
        .collect { case terms if terms.get(pack).isDefined => terms }
        .foreach { terms =>
          // assign roles if missing
          assignMissingRoles(o)

          // remove pack from terms and save
          terms - pack
          o.meta = (Meta(o.meta) + ("terms", terms)).serialize
        }
    }

    /** Accept terms and set roles for a pack (on domain match) */
    protected def termsAcceptAndSetRoles(o: User, domain: Domain): Unit =
      if (packDomainMatch(domain)) termsAcceptAndSetRoles(o)

    /** User terms (accept terms and assign pack roles) */
    protected def termsOnAccept(o: User, domain: Domain): Unit =
      termsAcceptAndSetRoles(o, domain)

    /** Set terms as required to be accepted for a pack */
    protected def termsSetRequired(o: User) = {
      Some(Meta(o.meta).csv("terms"))
        .collect { case terms if terms.get(pack).isEmpty => terms }
        .foreach { terms =>
          // add pack to terms and save
          terms + pack
          o.meta = (Meta(o.meta) + ("terms", terms)).serialize
        }
    }

    /** Set terms as required to be accepted for a pack (on domain not match).
      * When a domain matches it is assumed that a user had already accepted T&C as part of registration
      * and the routine assigns roles to a user (roles assigned on registration only and can be removed after)
      */
    protected def termsSetRequired(o: User, domain: Domain): Unit =
      if (!packDomainMatch(domain)) termsSetRequired(o)
      else assignMissingRoles(o)

    /** User registration (set terms as required on no domain match).
      * When a domain matches it is assumed that a user had already accepted T&C as part of registration
      * and the routine assigns roles to a user (roles assigned on registration only and can be removed after)
      */
    protected def termsOnRegister(o: User, domain: Domain) =
      termsSetRequired(o, domain)

    /** Add a pack (if a user has no pack assigned yet then set terms as required).
      * Needs to be called before adding a pack to ensure a user has no pack already assigned.
      */
    protected def termsBeforeAddPack(o: User) =
      if (!o.hasPack(pack)) termsSetRequired(o)

  }

}

/** Addition to user service for Control Panel. */
class ControlPanelUserMod extends Dependencies with UserService.Mod with UserService.ModTerms {
  override protected val pack: String = "CP"

  override protected def packDomains: List[Domain] =
    sc.get[DomainService].getAllByType(Domain.TYPE_CONTROL_PANEL)

  override def register(o: User, domain: Domain) =
    termsOnRegister(o, domain)

  override def login(o: User, domain: Domain) =
    termsOnLogin(o, domain)

  override def accept(o: User, domain: Domain) =
    termsOnAccept(o, domain)
}

trait UserService {
  import UserService._

  def allIds: List[Long]
  def getById(id: Long): Option[User]
  def getByUsername(username: String): Option[User]
  def getByEmail(email: String): Option[User]
  def getByRef(ref: String): Option[User]
  def getByUsernameOrEmail(value: String): Option[User]
  def getSystemUser: User
  def activate(id: Long)
  def login(username: String, password: String, domain: Domain): Option[User]
  def loginSso(ticket: String, mode: Int = 1): Option[User]
  def register(in: IN, domain: Domain): User
  def delete(id: Long)
  def update(id: Long, in: IN)
  def changePassword(id: Long, password: String)
  def credit(id: Long, amount: Long)
  def searchFast(in: SearchInput): SearchResult[User]
  def searchNotActivated(before: Timestamp, in: SearchInput): SearchResult[User]
  def search(in: SearchInput): SearchResult[User]
  def getAgentByDomain(domainId: Long): Option[User]
  def getByDomain(domainId: Long): Option[User]
  def isSiteOwner(id: Long, siteK: String): Boolean
  def isDomainOwner(id: Long, domain: String): Boolean
  def isDomainOwner(id: Long, domainId: Long): Boolean
  def genTicket(id: Long): String
  def genToken(id: Long): String
  def fullName(user: User, site: SiteChain): String
  def countAll(): Long
  def countActivated(): Long
  def countLoggedIn(since: Timestamp): Long
  def addMissingPacks(id: Long)
  def acceptAgreement(ref: String, domain: Domain): User
  def getAllRoles: List[Role]
  def getRoles(roles: List[String]): List[Role]
  def updateProps(id: Long, in: UpdatePropsIN): User
  def registerMigrated(id: Long, created: Long, activated: Long, lastLogin: Long, assignRoles: List[String], meta: String): User
  def prepareSolrDoc(o: User): SolrDoc
  def updateMeta(id: Long, meta: String)
}

@Transactional
class UserServiceImpl extends BaseService with UserService {
  import UserService._

  @BeanProperty var userDao: GenericDAO[User] = _
  @BeanProperty var roleDao: GenericDAO[Role] = _

  private var mods = List.empty[Mod]
  def setMods(x: JList[Mod]) = mods = x.toList

  private val solrType = Constants.SOLR_TYPE_USER
  private def solrSort = SolrService.solrSort(Constants.SOLR_PREFIX_USER, _: String, _: Map[String,String])

  /** Prepare Solr doc. */
  override def prepareSolrDoc(o: User): SolrDoc = {
    val terms =
      List(
        ObjectIdTerm(o.id, ObjectTypeTerm(solrType)),
        ValueTerm("ou_ref", o.ref),
        ValueTerm("ou_act", Option(o.activated).map(_ => 1).getOrElse(0)),
        ValueTerm("ou_reg", o.created),
        ValueTerm("ou_eml", o.email),
        ValueTerm("ou_usr", o.username),
        Option(o.lastLogin).map(x => ValueTerm("ou_llo", x)).getOrElse(NoopTerm)) 
    SolrDoc(terms: _*)
  }

  /** Submit an object to Solr. */
  @Transactional (readOnly = true)
  private def doSolrSubmit(o: User) = {
    sc.get[SolrService].submitWT(prepareSolrDoc(o))
    mods.foreach(_.submit(o)) //process mods
  }

  /** Delete an object from Solr. */
  @Transactional (readOnly = true)
  private def doSolrDelete(o: User) = sc.get[SolrService].deleteByIdsWT(ObjectIdTerm(o.id, ObjectTypeTerm(solrType)))

  private def merge(o: User, in: IN) {
    in.getClass.getDeclaredFields

    o.username = in.username
    o.password = in.password
    o.email = in.email
    o.firstName = in.firstName
    o.lastName = in.lastName
    o.primaryNumber = in.primaryNumber
    o.salutation = in.salutation.or(""+User.SALUTATION_OTHER).toInt

    if (in.address != null) {
      if (o.address == null) o.address = new Address
      val (oa, ina) = (o.address, in.address)
      oa.line1 = ina.line1
      oa.line2 = ina.line2.safe //nullable field
      oa.town = ina.town
      oa.postcode = ina.postcode
      oa.country = ina.country
    }
  }

  /** Find all IDs. */
  @Transactional (readOnly = true)
  override def allIds: List[Long] = userDao.findX[Long]("select id from "+classOf[User].getName)

  /** Get an object by its ID. */
  @Transactional (readOnly = true)
  override def getById(id: Long): Option[User] = userDao.findById(id)

  /** Get an object by a username. */
  @Transactional (readOnly = true)
  override def getByUsername(username: String): Option[User] = userDao.findUniqueByProperty("username", username)

  /** Get an object by a email. */
  @Transactional (readOnly = true)
  override def getByEmail(email: String): Option[User] = userDao.findUniqueByProperty("email", email)

  /** Get an object by a ref. */
  @Transactional (readOnly = true)
  override def getByRef(ref: String): Option[User] =
    if (ref == "*") None else userDao.findUniqueByProperty("ref", ref)

  /** Get a system user. */
  @Transactional (readOnly = true)
  override def getSystemUser: User = getByUsername("*").get

  /** Activate a user. */
  @Transactional (readOnly = false)
  override def activate(id: Long) {
    val o = getById(id).getOrElse(throw new ObjectNotFoundException)
    if (o.activated == null) {
      o.activated = Timestamp()
      userDao.saveOrUpdate(o)
      doSolrSubmit(o) //update Solr
    }
  }

  /** Get a user by her username or email. */
  @Transactional (readOnly = true)
  override def getByUsernameOrEmail(value: String): Option[User] =
    getByUsername(value) orElse (if (value.contains("@")) getByEmail(value) else None)

  /** Sign in a user by her username (or email) and password. */
  @Transactional (readOnly = false)
  override def login(username: String, password: String, domain: Domain): Option[User] = {
    val o = getByUsernameOrEmail(username) match {
      case Some(x) if x.ref == "*" => throw new ObjectNotFoundException
      case Some(x) if x.activated == null => throw new UserNotActivatedException
      case Some(x) => x
      case None => throw new ObjectNotFoundException
    }

    val meta = Meta(o.meta)
    val disabled = meta("disabled", "").isTrue

    if (password != o.password) None
    else {
      if (disabled) throw new UserDisabledException(o.ref) //disabled user
      mods.foreach(_.login(o, domain)) //process mods (may throw exception to forbid login)
      o.lastLogin = Timestamp()
      userDao.saveOrUpdate(o)
      doSolrSubmit(o) //update Solr
      Some(o)
    }
  }

  /** Sign in a user using a ticket.
    * mode 1 - use ticket which expires after login and valid for very short time
    * mode 2 - use token which does not expire after login and has no timeout
    */
  @Transactional (readOnly = false)
  override def loginSso(ticket: String, mode: Int = 1): Option[User] = {
    val arr = ticket.split("_")
    val (ref, time) = (arr.head, try { java.lang.Long.parseLong(arr.tail.head, 16) } catch { case _: Throwable => 0 })
    val timeout = configToMills(parseCSVMap(setting("SystemParams"))("ssoTimeout"))
    if (mode == 1 && System.currentTimeMillis > time+timeout) throw new ObjectExpiredException //ticket has expired

    val o = getByRef(ref).getOrElse(throw new ObjectNotFoundException)
    ticket match {
      case x if mode == 1 && x == o.ticket =>
        o.ticket = null //remove a ticket
        userDao.saveOrUpdate(o)
        Some(o)
      case x if mode == 2 && x == o.token => Some(o)
      case _ => None
    }
  }

  /** Generate a unique ref. */
  private def generateUniqueRef: String =
    Iterator.fill(10000)(generate(8,8).toUpperCase).find(x => getByRef(x).isEmpty).
      getOrElse(throw new CoreException("Ref generator exhausted"))

  /** Create a new user. */
  @Transactional (readOnly = false)
  override def register(in: IN, domain: Domain): User = {
    if (getByEmail(in.email).isDefined) throw new EmailExistsException
    if (getByUsername(in.username).isDefined) throw new UsernameExistsException
    val o = new User
    merge(o, in)
    o.ref = generateUniqueRef
    mods.foreach(_.register(o, domain)) //process mods
    userDao.saveOrUpdate(o)
    doSolrSubmit(o) //update Solr
    o
  }

  /** Additional fields to the "register" method for migrated users. */
  @Transactional (readOnly = false)
  override def registerMigrated(id: Long, created: Long, activated: Long, lastLogin: Long, assignRoles: List[String], meta: String): User = {
    val o = assertUser(id)
    o.created = created
    o.activated = activated
    o.lastLogin = lastLogin
    o.meta = (Meta(o.meta) + Meta(meta)).serialize

    getRoles(assignRoles).foreach(role => if (!o.roles.contains(role)) o.roles.add(role))

    userDao.saveOrUpdate(o)
    doSolrSubmit(o) //update Solr
    o
  }

  /** User accepted agreement. */
  @Transactional (readOnly = false)
  override def acceptAgreement(ref: String, domain: Domain): User = {
    val o = getByRef(ref).getOrElse(throw new ObjectNotFoundException)
    mods.foreach(_.accept(o, domain)) //process mods
    userDao.saveOrUpdate(o)
    o
  }

  /** Delete a user. */
  @Transactional (readOnly = false)
  override def delete(id: Long) =
    getById(id) match {
      case Some(o) if o.ref == "*" => throw new ObjectNotFoundException
      case Some(o) =>
        //delete user's labels, files, estates etc. and a user itself
        labelService.deleteByKeyLike("u."+o.ref+".%")
        sc.get[FileService].deleteByFolder(VirtualPath("u/"+o.ref, "*"))
        mods.foreach(_.delete(o)) //process mods
        userDao.delete(o)
        doSolrDelete(o) //update Solr
      case _ =>
    }

  private def assertUser(id: Long): User = getById(id).getOrElse(throw new ObjectNotFoundException)

  /** Update a user. */
  @Transactional (readOnly = false)
  override def update(id: Long, in: IN) {
    val o = assertUser(id)
    if (getByUsername(in.username).exists(_.id != id)) throw new UsernameExistsException
    if (getByEmail(in.email).exists(_.id != id)) throw new EmailExistsException
    merge(o, in)
    userDao.saveOrUpdate(o)
    doSolrSubmit(o) //update Solr
  }

  /** Update a user. */
  @Transactional (readOnly = false)
  override def changePassword(id: Long, password: String) {
    val o = assertUser(id)
    o.password = password
    userDao.saveOrUpdate(o)
  }

  /** Credit or debit a user. */
  @Transactional (readOnly = false)
  override def credit(id: Long, amount: Long) {
    val o = assertUser(id)
    o.credit = math.max(0L, o.credit+amount)
    userDao.saveOrUpdate(o)
    doSolrSubmit(o) //update Solr
  }

  /** Fast search without filtering. */
  @Transactional (readOnly = true)
  override def searchFast(in: SearchInput): SearchResult[User] = {
    val query = "from "+classOf[User].getName+" "+in.orderByClause
    searchObjects(in.from, in.max, userDao, query)
  }

  /** Find not activated users before a supplied date. */
  @Transactional (readOnly = true)
  override def searchNotActivated(before: Timestamp, in: SearchInput): SearchResult[User] = {
    val query = "from "+classOf[User].getName+" where activated is NULL and created < ? and username <> ?"
    searchObjects(in.from, in.max, userDao, query, before.toLong, "*")
  }

  /** Return a user (first one) who is an agent belonging to a domain (agency owner of a domain). */
  @Transactional (readOnly = true)
  override def getAgentByDomain(domainId: Long): Option[User] = {
    val query = "select o from "+classOf[User].getName+" o join o.domains as d join o.roles as r where d.id = ? and r.role = ?"
    userDao.find(query, domainId, "ROLE_AGENT").headOption
  }

  /** Return a user (first one) who is owner of a domain. */
  @Transactional (readOnly = true)
  override def getByDomain(domainId: Long): Option[User] = {
    val query = "select o from "+classOf[User].getName+" o join o.domains as d where d.id = ?"
    userDao.find(query, domainId).headOption
  }

  /** Check if a user owns domain's site (wis1). */
  @Transactional (readOnly = true)
  override def isSiteOwner(id: Long, siteK: String): Boolean = getById(id) match {
    case Some(o) =>
      val domainSites = o.domains.flatMap(d => sc.get[DomainService].listSites(d)).toList // wis1, wis2
      val sites = domainSites ::: (for { d <- domainSites; x <- Constants.SITE_EXTRAS } yield d+x) // wis1, wis2, wis1j, wis2b ...
      sites.contains(siteK)
    case None => false
  }

  /** Check if a user owns a domain. */
  @Transactional (readOnly = true)
  override def isDomainOwner(id: Long, domain: String): Boolean = getById(id) match {
    case Some(o) => o.domains.contains(sc.get[DomainService].getByDomain(domain).orNull)
    case None => false
  }

  @Transactional (readOnly = true)
  override def isDomainOwner(id: Long, domainId: Long): Boolean = getById(id) match {
    case Some(o) => o.domains.contains(sc.get[DomainService].getById(domainId).orNull)
    case None => false
  }

  /** Generate a ticket and return it. */
  @Transactional (readOnly = false)
  override def genTicket(id: Long): String = {
    val o = assertUser(id)
    o.ticket = o.ref+"_"+generateRef("|hms|_|sym10|")
    userDao.saveOrUpdate(o)
    o.ticket
  }

  /** Generate a token and return it. */
  @Transactional (readOnly = false)
  override def genToken(id: Long): String = {
    val o = assertUser(id)
    o.token = o.ref+"_"+generateRef("|hms|_|sym10|")
    userDao.saveOrUpdate(o)
    o.token
  }

  /** Construct a user's full name. */
  @Transactional (readOnly = true)
  override def fullName(user: User, site: SiteChain): String = {
    val s = label("salut"+user.salutation, site)
    (if (user.salutation == User.SALUTATION_OTHER) "" else s+" ")+user.firstName+" "+user.lastName
  }

  /** Statistics. */
  @Transactional (readOnly = true)
  override def countAll(): Long = {
    val query = "select count(*) from "+classOf[User].getName
    userDao.findCount(query)
  }

  /** Statistics. */
  @Transactional (readOnly = true)
  override def countActivated(): Long = {
    val query = "select count(*) from "+classOf[User].getName+" where activated is not null"
    userDao.findCount(query)
  }

  /** Statistics. */
  @Transactional (readOnly = true)
  override def countLoggedIn(since: Timestamp): Long = {
    val query = "select count(*) from "+classOf[User].getName+" where lastLogin >= ?"
    userDao.findCount(query, since.toLong)
  }

  @Transactional (readOnly = false)
  override def addMissingPacks(id: Long) {
    val o = assertUser(id)
    mods.foreach(_.addPack(o))
    userDao.saveOrUpdate(o)
  }

  @Transactional (readOnly = false)
  override def updateMeta(id: Long, meta: String) {
    val o = assertUser(id)
    o.meta = (Meta(o.meta) + Meta(meta)).serialize
    userDao.saveOrUpdate(o)
  }

  /** Return all roles. */
  @Transactional (readOnly = true)
  override def getAllRoles: List[Role] = roleDao.findAll

  /** Return all roles. */
  @Transactional (readOnly = true)
  override def getRoles(roles: List[String]): List[Role] = roles.flatMap(role => roleDao.findByProperty("role", role))

  /** Update users's properties. */
  @Transactional (readOnly = false)
  override def updateProps(id: Long, in: UpdatePropsIN): User = {
    val o = assertUser(id)
    if (getByUsername(in.username).exists(_.id != id)) throw new UsernameExistsException

    o.username = in.username
    o.activated = if (in.activated.safe != "") Timestamp(in.activated, "dd/MM/yyyy").midnight else null

    val domains = in.domains.map(x => sc.get[DomainService].getById(x.toLong).get)
    o.domains.clear()
    o.domains.addAll(domains.asJava)

    val roles = in.roles.map(x => roleDao.findById(x.toLong).get)
    o.roles.clear()
    o.roles.addAll(roles.asJava)

    o.meta = Meta(o.meta).?(in.disabled.isTrue, "disabled", "1").serialize
    userDao.saveOrUpdate(o)
    doSolrSubmit(o) //update Solr
    o
  }

  @Transactional (readOnly = true)
  override def search(in: SearchInput): SearchResult[User] = {
    val result = sc.get[SolrService].searchWT(
      SolrQuery(
        ObjectTypeTerm(solrType)),
      FlTerm("id"), StartTerm(in.from), RowsTerm(in.max),
      SortTerm(solrSort(in.orderBy, Map("email" -> "eml", "username" -> "usr"))))
    sc.get[SolrService].fetchObjectsWT(result, userDao, classOf[User])
  }

}
