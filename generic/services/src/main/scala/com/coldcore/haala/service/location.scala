package com.coldcore.haala
package service

import com.coldcore.haala.dao.{Asc, GenericDAO}
import com.coldcore.misc.scala.StringOp._

import beans.BeanProperty
import domain.{Country, Location, Postcode}
import org.springframework.transaction.annotation.Transactional
import java.net.URLEncoder

import core.HttpUtil.{HttpCallFailed, HttpCallSuccessful}
import com.google.gson.Gson
import service.exceptions.ExternalFailException
import core.{CoreException, NamedLog}
import core.Constants.UTF8

object LocationService {
  case object GeocodeResult {
    val TYPE_UNKNOWN = 0
    val TYPE_PRECISE = 1
    val TYPE_APPROXIMATE = 2
  }

  case class GeocodeResult(lat: Long, lng: Long) { // x10,000,000,000,000,000
    val `type`: Int = 0
    def serializeAsMap: Map[Symbol,Any] = Map('lat -> toXLatLng(lat), 'lng -> toXLatLng(lng), 'type -> `type`)
  }
}

trait LocationService {
  import LocationService._

  def getById(id: Long): Option[Location]
  def parentChain(id: Long): List[Location]
  def getRealCountries: List[Country]
  def getRealCountryByCode(code: String): Option[Country]
  def getCountries: List[Location]
  def geocode(address: String): GeocodeResult
  def getPostcodeById(id: Long): Option[Postcode]
  def getPostcodesByLocationId(id: Long): List[Postcode]
}

@Transactional
class LocationServiceImpl extends BaseService with LocationService {
  import LocationService._

  private lazy val LOG = new NamedLog("location-service", log)

  @BeanProperty var locationDao: GenericDAO[Location] = _
  @BeanProperty var countryDao: GenericDAO[Country] = _
  @BeanProperty var postcodeDao: GenericDAO[Postcode] = _
  @BeanProperty var countryCache: DomainCache[Country] = _
  @BeanProperty var postcodeCache: DomainCache[Postcode] = _

  val googleGeocodeUrl = "http://maps.googleapis.com/maps/api/geocode"

  /** JSON response for geocode by address */
  private class GoogleGeocodeByAddress {
    @BeanProperty var results: JList[Result] = _
    @BeanProperty var status: String = _

    class Result {
      @BeanProperty var address_components: JList[Component] = _
      @BeanProperty var formatted_address: String = _
      @BeanProperty var geometry: Geometry = _
    }
    class Component {
      @BeanProperty var long_name: String = _
      @BeanProperty var short_name: String = _
      @BeanProperty var types: JList[String] = _
    }
    class Location {
      @BeanProperty var lat: String = _
      @BeanProperty var lng: String = _
    }
    class Viewport {
      @BeanProperty var northeast: Location = _
      @BeanProperty var southwest: Location = _
    }
    class Geometry {
      @BeanProperty var location: Location = _
      @BeanProperty var location_type: String = _
      @BeanProperty var viewport: Viewport = _
    }
  }

  /** Get an object by its ID. Cannot be cached with lazy-loading. */
  @Transactional (readOnly = true)
  override def getById(id: Long): Option[Location] = locationDao.findById(id)

  /** Used in the 'parent-chain' mathod to create a list of locations. */
  private def parentsOf(o: Location): List[Location] =
    o :: (if (o.parent == null) Nil else parentsOf(o.parent)) //max 4 cycles, no need for @tailrec

  /** Construct a list of parents locations including a supplied location in the end of the list. */
  @Transactional (readOnly = true)
  override def parentChain(id: Long): List[Location] = getById(id) match {
    case Some(o) => parentsOf(o).reverse
    case None => Nil
  }

  /** Return list of countries. Cache-aware. */
  @Transactional (readOnly = true)
  override def getRealCountries: List[Country] =
    cacheAwareResults[Country](countryCache, "rc", { countryDao.findAll })

  /** Get an object by its code. Cache-aware. */
  @Transactional (readOnly = true)
  override def getRealCountryByCode(code: String): Option[Country] =
    getRealCountries.find(_.code == code)

  /** Return list of locations with a coutry type. Cannot be cached with lazy-loading. */
  @Transactional (readOnly = true)
  override def getCountries: List[Location] =
    locationDao.findByProperties(Map("type" -> Location.TYPE_COUNTRY), Asc("index"), Asc("id"))

  /** Return geocode by address. */
  @Transactional (readOnly = true)
  override def geocode(address: String): GeocodeResult = {
    if (address.trim == "") throw new CoreException("No address for geocoding") //not wasting geocode requests

    val enc = (x: Any) => URLEncoder.encode(x.toString, UTF8)
    val json = sb.httpGet(googleGeocodeUrl+"/json?address="+enc(address)+"&sensor=false") match {
      case x: HttpCallSuccessful => x.body
      case x: HttpCallFailed =>
        LOG.error("Failed geocode call", x.error)
        throw new ExternalFailException
    }
    val r = new Gson().fromJson(json, classOf[GoogleGeocodeByAddress])
    if (r.status != "OK") {
      LOG.warn("Bad geocode status: "+r.status)
      throw new ExternalFailException
    }

    import GeocodeResult._
    val latlng = r.results.head.geometry.location
    new GeocodeResult(toXLatLng(latlng.lat), toXLatLng(latlng.lng)) {
      override val `type` = r.results.head.geometry.location_type match {
        case "ROOFTOP" => TYPE_PRECISE
        case "APPROXIMATE" => TYPE_APPROXIMATE
        case _ => TYPE_UNKNOWN
      }
    }
  }

  /** Get postcode by its ID. */
  @Transactional (readOnly = true)
  override def getPostcodeById(id: Long): Option[Postcode] = postcodeDao.findById(id)

  /** Get postcodes by its location ID. Cache-aware. */
  @Transactional (readOnly = true)
  override def getPostcodesByLocationId(id: Long): List[Postcode] =
    cacheAwareResults[Postcode](postcodeCache, s"lp$id", { postcodeDao.findByProperty("location.id", id) })

}
