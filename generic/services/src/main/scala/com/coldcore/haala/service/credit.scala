package com.coldcore.haala
package service

import core.{Meta, NamedLog, Timestamp}
import core.GeneralUtil.configToMills
import domain.{AbyssEntry, User}
import beans.BeanProperty
import org.springframework.transaction.annotation.Transactional
import service.exceptions.{ObjectNotFoundException, ObjectProcessedException}
import com.google.gson.Gson

object CreditService {
  case class CreditUserIN(pack: String, value: String, meta: String, amount: Long, ref: String = "")

  case class CreditUserOUT(username: String, amount: Long) {
    def serializeAsMap: Map[Symbol,Any] = Map('username -> username, 'amount -> amount)
  }

  /** Credit config JSON */
  class CreditConfig {
    @BeanProperty var keep: String = _ // time to keep abyss record: 180d, 1y
  }

  /** Credit entry Abyss wrapper */
  class CreditEntry(val abyss: AbyssEntry, sc: ServiceContainer) {
    def value(key: String): String = Meta(abyss.value)(key)
    def meta(key: String): String = Meta(abyss.meta)(key)

    val amount = value("amount").toLong
    val username = value("username")
    val message = value("message").safe

    lazy val user: User = sc.get[UserService].getById(value("user").toLong).getOrElse(throw new ObjectNotFoundException)
  }
  object CreditEntry {
    def apply(abyss: AbyssEntry, sc: ServiceContainer) = new CreditEntry(abyss, sc)
  }
}

class CreditPaymentMod extends PaymentService.Mod with Dependencies {
  override def process(abyss: AbyssEntry): Boolean = {
    if (abyss.pack.safe == "" && Meta(abyss.value)("process", "") == "crd1") { // no pack and process=crd1
      sc.get[CreditService].onPayment(abyss)
      true
    } else false
  }
}

trait CreditService {
  import CreditService._

  def creditUser(userId: Long, in: CreditUserIN): CreditEntry
  def onPayment(abyss: AbyssEntry)
}

@Transactional
class CreditServiceImpl extends BaseService with CreditService {
  import CreditService._

  private val sorlDynamicType = "crd"

  /** Credit configuration */
  private def creditConfig: CreditConfig =
    new Gson().fromJson(setting("CreditConf"), classOf[CreditConfig])

  /** Create new credit entry and credit user. */
  @Transactional (readOnly = false)
  override def creditUser(userId: Long, in: CreditUserIN): CreditEntry = {
    val keep = Timestamp.fromMills(System.currentTimeMillis+configToMills(creditConfig.keep))
    val user = sc.get[UserService].getById(userId).getOrElse(throw new ObjectNotFoundException)
    val ref = in.ref

    val crdValue = Meta(in.value) +
      ("amount", in.amount) +
      ("user", user.id) +
      ("username", user.username)

    val crdMeta = Meta(in.meta) +
      ("s_typ", sorlDynamicType) +
      ("s_usr", user.username)

    val abyss = sc.get[AbyssService].create(AbyssService.CreateIN(ref, in.pack,
      AbyssService.ValueMetaIN(value = crdValue.serialize, meta = crdMeta.serialize), keep))

    sc.get[UserService].credit(userId, in.amount)

    CreditEntry(abyss, sc)
  }

  /** Process credit payment entry. */
  @Transactional (readOnly = false)
  override def onPayment(abyss: AbyssEntry) {
    val value = Meta(abyss.value) // user=2;amount=25;complete=n
    if (value("complete").isTrue) throw new ObjectProcessedException

    val user = sc.get[UserService].getById(value("user").toLong).getOrElse(throw new ObjectNotFoundException)
    sc.get[UserService].credit(user.id, value("amount").toLong)
    sc.get[AbyssService].update(abyss.id, AbyssService.ValueMetaIN(value = (value + ("complete" -> "y")).serialize))
    sc.get[AbyssService].create(abyss.id, AbyssService.ValueMetaIN(value = "complete=auto"))
  }

}
