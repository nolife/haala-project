package com.coldcore.haala
package service

import com.coldcore.misc.scala.StringOp._
import com.coldcore.email.{Attachment, OutMessage, Sender}
import domain.User
import beans.BeanProperty
import org.springframework.transaction.annotation.Transactional
import javax.mail.internet.AddressException
import core.{NamedLog, SiteChain}
import exceptions.{EmailServerDownException, InvalidEmailException, ObjectNotFoundException}

trait EmailService {
  def email(from: String, to: String, subject: String, text: String, attachments: List[Attachment] = Nil)
  def remindPassword(value: String, site: SiteChain): User
  def toSupport(from: String, text: String, site: SiteChain, attachments: List[Attachment] = Nil)
  def fromSupport(to: String, text: String, site: SiteChain, attachments: List[Attachment] = Nil)
  def reportError(text: String)
}

@Transactional
class EmailServiceImpl extends BaseService with EmailService {
  private lazy val LOG = new NamedLog("email-service", log)

  @BeanProperty var mailSender: Sender = _

  /** Send an email in HTML format. */
  @Transactional (readOnly = true)
  override def email(from: String, to: String, subject: String, text: String, attachments: List[Attachment] = Nil) =
    try {
      val msg = new OutMessage
      val recipient = if (setting("Dev/EmailSupport").isTrue) supportEmail(defaultSite) else to //any email goes to support (dev mode flag)
      msg.addRecipient(recipient, OutMessage.RecipientType.TO)
      msg.setHtmlText(text)
      msg.setSubject(subject)
      msg.setFrom(from)
      attachments.foreach(msg.addAttachment)

      if (setting("Dev/NoEmail").isTrue) LOG.info(s"Email disabled: from $from to $to") //no email (dev mode flag)
      else { 
        mailSender.sendMessage(msg)
        LOG.info(s"Email sent: from $from to $to")
      }
    } catch {
      case e: AddressException =>
        LOG.error(s"Invalid address: from $from to $to", e)
        throw new InvalidEmailException
      case e: Throwable =>
        LOG.error(s"Cannot send email: from $from to $to", e)
        throw new EmailServerDownException
    } finally {
      if (setting("Dev/LogEmail").isTrue) //log emails (dev mode flag)
        LOG.info(s"Email content: from $from to $to \nSubject: $subject \n$text")
    }

  private def brandWeb(site: SiteChain): String = parseCSVMap(setting("Brand", site)).getOrElse("web", "?")

  private def supportEmail(site: SiteChain): String = setting("SupportEmail", site)

  /** Send a password reminder email to a user. Inputs: user's email or username and her current site. */
  @Transactional (readOnly = true)
  override def remindPassword(value: String, site: SiteChain): User = {
    val user = sc.get[UserService].getByUsernameOrEmail(value).getOrElse(throw new ObjectNotFoundException)
    val mainUrl = setting("MainURL", site)
    val actLink = mainUrl+"/act.htm?ref="+user.ref+"&"+System.currentTimeMillis.hex
    val textKey = Option(user.activated).map(_ => "email.remtx").getOrElse("email.acttx") //normal reminder or activate account reminder
    val text = parametrize(label(textKey, site), "username:"+user.username, "password:"+user.password, "link:"+actLink)
    val subject = parametrize(label("email.supsu", site), "BrandWeb:"+brandWeb(site))
    email(supportEmail(site), user.email, subject, text)
    user
  }

  /** Email from sender to support. */
  @Transactional (readOnly = true)
  override def toSupport(from: String, text: String, site: SiteChain, attachments: List[Attachment] = Nil) {
    val subject = parametrize(label("email.supsu", defaultSite), "BrandWeb:"+brandWeb(site)) // default site subject
    email(from, supportEmail(site), subject, text)
  }

  /** Email from support to recipient. */
  @Transactional (readOnly = true)
  override def fromSupport(to: String, text: String, site: SiteChain, attachments: List[Attachment] = Nil) {
    val subject = parametrize(label("email.supsu", site), "BrandWeb:"+brandWeb(site)) 
    email(supportEmail(site), to, subject, text)
  }

  /** Report a system error by email. */
  @Transactional (readOnly = true)
  override def reportError(text: String) {
    val subject = parametrize(label("email.errsu", defaultSite), "BrandWeb:"+brandWeb(defaultSite))
    email(sc.get[UserService].getSystemUser.email, supportEmail(defaultSite), subject, text)
  }
}
