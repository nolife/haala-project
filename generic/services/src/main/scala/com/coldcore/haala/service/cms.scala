package com.coldcore.haala
package service

import core.GeneralUtil._
import core.{SiteChain, SiteChainMap, UrlMapping, VirtualPath}
import core.Constants.UTF8
import exceptions.{InvalidFormatException, ObjectExistsException, ObjectNotFoundException}
import FileService.Body
import domain.File
import org.springframework.transaction.annotation.Transactional
import com.google.gson.Gson
import java.io.ByteArrayOutputStream
import com.coldcore.misc5.CByte._

object CmsService {
  case class PageMappingIN(index: String, pattern: String, pageName: String, params: String, options: String,
                           mod: String, facade: String, downtime: String, handler: String)
}

trait CmsService {
  import CmsService._

  def updateLabel(sat: List[SiteableText], siteChainMap: SiteChainMap = null)
  def loadLabel(key: String, sfx: String, siteChainMap: SiteChainMap): Map[String,String]
  def incVerion(site: String)
  def updateFile(path: VirtualPath, site: String, body: Body, siteChainMap: SiteChainMap = null, cd: Boolean = false)
  def loadFile(path: VirtualPath, siteChainMap: SiteChainMap): Map[String,File]
  def updateDomain(id: Long, dinfo: SiteableText, info: List[SiteableText])
  def updatePageMapping(domainId: Long, pageIndex: String, in: PageMappingIN)
  def addPageMapping(domainId: Long, in: PageMappingIN)
  def deletePageMapping(domainId: Long, pageIndex: String)
  def loadTextFile(id: Long): (File,String)
  def updatePageLabels(domainId: Long, pageIndex: String, dinfo: SiteableText, title: List[SiteableText])
}

@Transactional
class CmsServiceImpl extends BaseService with CmsService {
  import CmsService._

  private def merge(o: UrlMapping#Mapping, in: PageMappingIN) {
    o.pattern = in.pattern.safe
    o.params = in.params.safe
    o.handler = in.handler.safe
    val hc = Option(o.handlerConf).getOrElse(o.newHandlerConf)
    hc.index = in.index.safe
    hc.mod = in.mod.safe
    hc.facade = in.facade.safe
    hc.pageName = in.pageName.safe
    hc.options = in.options.safe
    hc.downtime = in.downtime.safe.isTrue ? "1" | "0"
  }

  /** Update label values. Cache-aware.
   *  If 'site-chain-map' is omitted then no parent chains will be checked.
   */
  @Transactional (readOnly = false)
  override def updateLabel(sat: List[SiteableText], siteChainMap: SiteChainMap = null) {
    val key = sat.head.text.keys.head.name
    if (labels(key).isEmpty) throw new ObjectNotFoundException

    //regular update (without deleting empty labels)
    rewriteLabels((k: String) => k, sat: _*)

    //delete labels matching parent values
    if (siteChainMap != null)
      sat.foreach { x =>
        val (msite, xsite) = (siteChainMap.mineSiteChain.getOrElse(x.site, SiteChain.empty),
                              siteChainMap.xtraSiteChain.getOrElse(x.site, SiteChain.empty))
        val mxsite = if (msite.nonEmpty) msite else xsite
        if (mxsite.nonEmpty) {
          val psite = SiteChain(mxsite.keys.dropRight(1))
          val pvalue = label(key, psite)
          if (pvalue == x.text(Symbol(key))) labelService.delete(key, x.site)
        }
      }
  }

  /** Fill a map for every site using 'chain' values. Cache-aware. */
  @Transactional (readOnly = true)
  override def loadLabel(key: String, sfx: String, siteChainMap: SiteChainMap): Map[String,String] = {
    val mine = siteChainMap.mineSiteChain
    val mxKeys = mine.keys.map(_+sfx.safe).toList //mh2 -> mh2   j,mh2 -> mh2j   (sfx: b / j)
    val mx = if (sfx.safe != "") siteChainMap.xtraSiteChain else mine
    val m =
      (for ((k,v) <- mx) yield //remove root 'xtra' keys, leaving only proper keys
        k -> label(key, v)).filter { case (k,v) => mxKeys.contains(k) }
    if (m.isEmpty || m.values.exists(null==)) throw new ObjectNotFoundException
    m
  }

  /** Inc a version. Cache-aware. */
  @Transactional (readOnly = false)
  override def incVerion(site: String) {
    val rv = setting("ResourceVersion", SiteChain.key(site)).toInt+1
    settingService.write("ResourceVersion", ""+(if (rv < 10000) rv else 1), site)
  }

  /** Update a file body. Cache-aware.
   * If 'site-chain-map' is omitted then no parent chains will be checked.
   * If CD is TRUE (create / delete) the file will be created if does not exist or deleted if no body.
   */
  @Transactional (readOnly = false)
  override def updateFile(path: VirtualPath, site: String, body: Body, siteChainMap: SiteChainMap = null, cd: Boolean = false) {
    if (sc.get[FileService].getByPath(path).isEmpty && !cd) throw new ObjectNotFoundException
    if (body != null) {
      //regular update / create
      sc.get[FileService].getByPath(path, SiteChain.key(site)) match {
        case Some(o) => sc.get[FileService].update(o.id, FileService.IN(VirtualPath(o.folder, o.name), site), body)
        case None => sc.get[FileService].create(path, site, body)
      }

      //delete created or updated file if it matched its parent
      if (Option(siteChainMap).exists(_.mineSiteChain.contains(site))) {
        val msite = siteChainMap.mineSiteChain(site)
        val psite = SiteChain(msite.keys.dropRight(1))
        sc.get[FileService].getByPath(path, psite).foreach { p =>
          val o = sc.get[FileService].getByPath(path, SiteChain.key(site)).get
          val od = withOpen(sc.get[FileService].getBody(o).sourceStream) { digest(_, "MD5") }
          val pd = withOpen(sc.get[FileService].getBody(p).sourceStream) { digest(_, "MD5") }
          if (od == pd) sc.get[FileService].delete(o.id)
        }
      }
    } else if (cd) {
      //delete
      sc.get[FileService].getByPath(path, SiteChain.key(site)).foreach(o => sc.get[FileService].delete(o.id))
    }
  }

  /** Fill a map for every site using 'chain' files. Cache-aware. */
  @Transactional (readOnly = true)
  override def loadFile(path: VirtualPath, siteChainMap: SiteChainMap): Map[String,File] =
    for ((k,v) <- siteChainMap.mineSiteChain) yield
      k -> sc.get[FileService].getByPath(path, v).getOrElse(throw new ObjectNotFoundException)

  /** Update domain's data. Cache-aware. */
  @Transactional (readOnly = false)
  override def updateDomain(id: Long, dinfo: SiteableText, info: List[SiteableText]) {
    val o = sc.get[DomainService].getById(id).getOrElse(throw new ObjectNotFoundException)

    //default site labels
    val site = sc.get[DomainService].defaultSite(o.domain).key // wis1 (default site key)
    writeLabels((k: String) => k,
      SiteableText(site, Map(Symbol("meta.key") -> dinfo.text.getOrElse('keywords, ""),
                             Symbol("site.ihead") -> dinfo.text.getOrElse('insideHead, ""),
                             Symbol("site.bbody") -> dinfo.text.getOrElse('beforeBody, ""))))

    //multilingual
    val keyF = (k: String) => k match { case "title" => "site.ttl" }
    writeLabels(keyF, info: _*)
  }

  /** Update page labels. Cache-aware. */
  @Transactional (readOnly = false)
  override def updatePageLabels(domainId: Long, pageIndex: String, dinfo: SiteableText, title: List[SiteableText]) {
    val o = sc.get[DomainService].getById(domainId).getOrElse(throw new ObjectNotFoundException)

    //default site labels
    val site = sc.get[DomainService].defaultSite(o.domain).key // wis1 (default site key)
    writeLabels((k: String) => k,
      SiteableText(site, Map(Symbol("met.k"+pageIndex) -> dinfo.text.getOrElse('keywords, ""),
                             Symbol("met.d"+pageIndex) -> dinfo.text.getOrElse('description, ""))))

    //multilingual
    val keyF = (k: String) => k match { case "title" => "ttl.p"+pageIndex }
    writeLabels(keyF, title: _*)
  }

  /** Update page mapping data. Cache-aware. */
  @Transactional (readOnly = false)
  override def updatePageMapping(domainId: Long, pageIndex: String, in: PageMappingIN) {
    val domain = sc.get[DomainService].getById(domainId).getOrElse(throw new ObjectNotFoundException)
    val site = sc.get[DomainService].defaultSite(domain.domain).key // wis1 (default site key)

    val urlMapping = Option(new Gson().fromJson(setting("UrlMapping", SiteChain.key(site)).safe,
      classOf[UrlMapping])).getOrElse(throw new ObjectNotFoundException)
    val mapping = urlMapping.mapping.find(_.handlerConf.index == pageIndex).getOrElse(throw new ObjectNotFoundException)
    if (pageIndex != in.index && urlMapping.mapping.exists(_.handlerConf.index == in.index)) throw new ObjectExistsException

    merge(mapping, in)
    val json = new Gson().toJson(urlMapping, classOf[UrlMapping])
    settingService.write("UrlMapping", json, site)
  }

  /** Add page mapping data. Cache-aware. */
  @Transactional (readOnly = false)
  override def addPageMapping(domainId: Long, in: PageMappingIN) {
    val domain = sc.get[DomainService].getById(domainId).getOrElse(throw new ObjectNotFoundException)
    val site = sc.get[DomainService].defaultSite(domain.domain).key // wis1 (default site key)

    val urlMapping = Option(new Gson().fromJson(setting("UrlMapping", SiteChain.key(site)).safe,
      classOf[UrlMapping])).getOrElse(throw new ObjectNotFoundException)
    if (urlMapping.mapping.exists(_.handlerConf.index == in.index)) throw new ObjectExistsException

    val mapping = new urlMapping.Mapping
    urlMapping.mapping = (mapping :: urlMapping.mapping.toList).toArray

    merge(mapping, in)
    val json = new Gson().toJson(urlMapping, classOf[UrlMapping])
    settingService.write("UrlMapping", json, site)
  }

  /** Delete page mapping data. Cache-aware. */
  @Transactional (readOnly = false)
  override def deletePageMapping(domainId: Long, pageIndex: String) {
    val domain = sc.get[DomainService].getById(domainId).getOrElse(throw new ObjectNotFoundException)
    val site = sc.get[DomainService].defaultSite(domain.domain).key // wis1 (default site key)

    val urlMapping = Option(new Gson().fromJson(setting("UrlMapping", SiteChain.key(site)).safe,
      classOf[UrlMapping])).getOrElse(throw new ObjectNotFoundException)

    urlMapping.mapping = urlMapping.mapping.filter(_.handlerConf.index != pageIndex)

    val json = new Gson().toJson(urlMapping, classOf[UrlMapping])
    settingService.write("UrlMapping", json, site)
  }

  /** Load a text file */
  @Transactional (readOnly = true)
  override def loadTextFile(id: Long): (File,String) = {
    val fs = sc.get[FileService]
    val file = fs.getById(id).getOrElse(throw new ObjectNotFoundException)

    val body = fs.getBody(file)
    val bodyKB = math.round(body.size.toDouble/1024d)
    if (bodyKB > 50) throw new InvalidFormatException // 50 KB is the limit (can be parametrized)

    val out = new ByteArrayOutputStream
    withOpen(body.sourceStream) { r => inputToOutput(r, out) }
    (file, out.toString(UTF8))
  }

}
