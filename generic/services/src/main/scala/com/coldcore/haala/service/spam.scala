package com.coldcore.haala
package service

import beans.BeanProperty
import dao.GenericDAO
import org.springframework.transaction.annotation.Transactional
import core.{Constants, NamedLog}
import domain.{EmailStorage, EmailStorageItem}

import com.google.gson.Gson
import service.exceptions.{ObjectExistsException, ObjectNotFoundException}

object SpamService {
  case class CreateStorageIN(description: String)
  case class UpdateStorageIN(description: String, append: String)
  case class StartTaskIN(storageId: Long, from: String, subject: String, text: String)

  class TaskConfig {
    @BeanProperty var storage: JLong = _
    @BeanProperty var from: String = _
    @BeanProperty var good: JLong = 0L
    @BeanProperty var bad: JLong = 0L
    @BeanProperty var error: String = ""
  }

  class ByDescriptionSearchInput(from: Long, max: Int, orderBy: String = "") extends SearchInput(from, max, orderBy) {
    def this(in: SearchInput) = this(in.from, in.max, in.orderBy)
    val description = ""
  }
}

trait SpamService {
  import SpamService._

  def getStorageById(id: Long): Option[EmailStorage]
  def createStorage(in: CreateStorageIN): EmailStorage
  def deleteStorage(id: Long)
  def updateStorage(id: Long, in: UpdateStorageIN): EmailStorage
  def searchStorages(in: SearchInput): SearchResult[EmailStorage]
  def searchStoragesByDescription(in: ByDescriptionSearchInput): SearchResult[EmailStorage]
  def findIdleStorageItems(storageId: Long, max: Int): List[EmailStorageItem]
  def updateStorageItemStatus(itemId: Long, status: Int)
  def getTaskConfig: Option[TaskConfig]
  def writeTaskConfig(x: TaskConfig)
  def startTask(in: StartTaskIN)
  def stopTask
}

@Transactional
class SpamServiceImpl extends BaseService with SpamService {
  import SpamService._

  private lazy val LOG = new NamedLog("spam-service", log)

  @BeanProperty var emailStorageDao: GenericDAO[EmailStorage] = _
  @BeanProperty var emailStorageItemDao: GenericDAO[EmailStorageItem] = _

  /** Get an object by its ID. */
  @Transactional (readOnly = true)
  override def getStorageById(id: Long): Option[EmailStorage] = emailStorageDao.findById(id)

  /** Create a storage. */
  @Transactional (readOnly = false)
  override def createStorage(in: CreateStorageIN): EmailStorage = {
    val o = new EmailStorage
    o.description = in.description
    emailStorageDao.saveOrUpdate(o)
    o
  }

  /** Delete a storage. */
  @Transactional (readOnly = false)
  override def deleteStorage(id: Long) = getStorageById(id).foreach(emailStorageDao.delete)

  @Transactional (readOnly = false)
  override def updateStorage(id: Long, in: UpdateStorageIN): EmailStorage = {
    val o = getStorageById(id).getOrElse(throw new ObjectNotFoundException)
    o.description = in.description

    val extractEmail = (raw: String) =>
      if (raw.length > 100 || !raw.contains("@") || raw.split("@").length != 2 ||
          !raw.split("@").last.matches(".+\\.\\D+")) None
      else { // some xyz <name@email.cc> -> some,xyz,,name ... -> name@email.cc
        val char = (c: Char) => if ((""+c).matches("[A-Za-z0-9\\-\\.]")) c else ','
        val name = raw.split("@").head.reverse.map(char).reverse.mkString.split(",").last.toLowerCase
        val domain = raw.split("@").last.map(char).mkString.split(",").head.toLowerCase
        if (name != "" && domain != "") Some(name+"@"+domain) else None
      }

    //extract emails and append to storage items
    val emails = in.append.parseCSV(" ;,\n").flatMap(extractEmail(_))
    if (emails.nonEmpty) {
      for (x <- emails; if !o.items.exists(_.value == x)) {
        val item = new EmailStorageItem
        item.value = x
        o.items.add(item)
      }
      o.total = o.items.size
      LOG.info("Appended emails: "+emails.mkString(", "))
    }

    emailStorageDao.saveOrUpdate(o)
    o
  }

  @Transactional (readOnly = true)
  override def searchStorages(in: SearchInput): SearchResult[EmailStorage] = {
    val query = "from "+classOf[EmailStorage].getName+" "+in.orderByClause
    searchObjects(in.from, in.max, emailStorageDao, query)
  }

  @Transactional (readOnly = true)
  override def searchStoragesByDescription(in: ByDescriptionSearchInput): SearchResult[EmailStorage] = {
    val query = "from "+classOf[EmailStorage].getName+" where description = ? "+in.orderByClause
    searchObjects(in.from, in.max, emailStorageDao, query, in.description)
  }

  @Transactional (readOnly = true)
  override def findIdleStorageItems(storageId: Long, max: Int): List[EmailStorageItem] = {
    val query = "from "+classOf[EmailStorageItem].getName+" where storage.id = ? and status = ?"
    emailStorageItemDao.findRange(0, max, query, storageId, EmailStorageItem.STATUS_IDLE)
  }

  @Transactional (readOnly = false)
  override def updateStorageItemStatus(itemId: Long, status: Int) =
    emailStorageItemDao.findById(itemId).foreach { x =>
      x.status = status
      emailStorageItemDao.saveOrUpdate(x)
    }

  @Transactional (readOnly = true)
  override def getTaskConfig: Option[TaskConfig] =
    Option(new Gson().fromJson(setting("SpamTaskConf").safe, classOf[TaskConfig]))

  @Transactional (readOnly = false)
  override def writeTaskConfig(x: TaskConfig) =
    settingService.write("SpamTaskConf", new Gson().toJson(x, classOf[TaskConfig]), Constants.SITE_SYSTEM)

  @Transactional (readOnly = false)
  override def startTask(in: StartTaskIN) = {
    val o = getStorageById(in.storageId).getOrElse(throw new ObjectNotFoundException)
    if (setting("SpamEmailsFlag").isTrue) throw new ObjectExistsException //already running

    //set items to idle
    val query = "update "+classOf[EmailStorageItem].getName+" set status = ? where storage.id = ?"
    emailStorageItemDao.executeUpdate(query, EmailStorageItem.STATUS_IDLE, o.id)

    //save parameters and kick off task
    labelService.write("spam.emailSubject", in.subject, Constants.SITE_SYSTEM)
    labelService.write("spam.emailText", in.text, Constants.SITE_SYSTEM)
    writeTaskConfig(new TaskConfig { storage = o.id; from = in.from })
    settingService.write("SpamEmailsFlag", "1", Constants.SITE_SYSTEM)
  }

  @Transactional (readOnly = false)
  override def stopTask =
    settingService.write("SpamEmailsFlag", "0", Constants.SITE_SYSTEM)

}
