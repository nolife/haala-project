package com.coldcore.haala
package service

import org.springframework.transaction.annotation.Transactional
import service.exceptions._
import core.HttpUtil.{HttpCallFailed, HttpCallSuccessful}

import beans.BeanProperty
import com.google.gson.Gson
import domain.{Domain, User}
import com.coldcore.misc5.IdGenerator._
import com.coldcore.haala.core.{Meta, NamedLog}

object FacebookService {
  class FbUser {
    @BeanProperty var id: String = _
    @BeanProperty var first_name: String = _
    @BeanProperty var last_name: String = _
    @BeanProperty var username: String = _
    @BeanProperty var email: String = _
  }

  case class RegisterIN(fbUser: FbUser, username: String, email: String)
}

trait FacebookService {
  import FacebookService._

  def getUser(token: String): Option[FbUser]
  def register(in: RegisterIN): User
  def generateUsername(fbUser: FbUser): String
  def login(fbUser: FbUser, domain: Domain): Option[User]
}

@Transactional
class FacebookServiceImpl extends BaseService with FacebookService {
  import FacebookService._

  private lazy val LOG = new NamedLog("facebook-service", log)

  class GraphError {
    @BeanProperty var error: Error = _

    class Error {
      @BeanProperty var message: String = _
      @BeanProperty var `type`: String = _
      @BeanProperty var code: String = _
      @BeanProperty var error_subcode: String = _
    }
  }

  /** Get user from FB by auth token. */
  @Transactional (readOnly = true)
  override def getUser(token: String): Option[FbUser] = {
    sb.httpGet("https://graph.facebook.com/me?access_token="+token) match {
      case call: HttpCallSuccessful =>
        val e = new Gson().fromJson(call.body, classOf[GraphError])
        if (e.error == null) Some(new Gson().fromJson(call.body, classOf[FbUser]))
        else {
          LOG.warn("Error retrieving user: "+e.error.message.safe)
          None
        }
      case call: HttpCallFailed =>
        LOG.error("Facebook API failed", call.error)
        throw new ExternalFailException
    }
  }

  /** Generate username. */
  @Transactional (readOnly = true)
  override def generateUsername(fbUser: FbUser): String = {
    val regex = setting("RegEx/username")
    val safe = (x: String) => x.safe.replace(".", "_").replace(" ", "")
    val rname = (name: String) =>
      Iterator.fill(100)(name+generateNumbers(1,4)).find {
        case x => x.matches(regex) && sc.get[UserService].getByUsername(x).isEmpty
      }
    // abramov_sergei5   sergei34   abramov12   sergei_abramov23   abramov_sergei44   sergeiabramov18   abramovsergei88   user234
    val names = safe(fbUser.username) :: safe(fbUser.first_name) :: safe(fbUser.last_name) ::
                safe(fbUser.first_name)+"_"+safe(fbUser.last_name) :: safe(fbUser.last_name)+"_"+safe(fbUser.first_name) ::
                safe(fbUser.first_name)+safe(fbUser.last_name) :: safe(fbUser.last_name)+safe(fbUser.first_name) ::
                "user" :: Nil
    (Option.empty[String] /: names)((a,b) => a.isDefined ? a | rname(b.toLowerCase)).getOrElse(throw new GeneratorExhaustedException)
  }

  /** Create a new internal user from facebook user. */
  @Transactional (readOnly = false)
  override def register(in: RegisterIN): User = {
    val u = in.fbUser
    val (email, username) = (in.email or u.email.safe, in.username or u.username.safe)
    if (sc.get[UserService].getByEmail(email).isDefined) throw new EmailExistsException
    if (sc.get[UserService].getByUsername(username).isDefined) throw new UsernameExistsException
    val user = sc.get[UserService].register(UserService.IN(
      username, generate(8,8), email, u.first_name, u.last_name, "?", ""+User.SALUTATION_OTHER, null), domain = null)
    sc.get[UserService].activate(user.id)
    user
  }

  /** Sign in a user. */
  @Transactional (readOnly = false)
  override def login(fbUser: FbUser, domain: Domain): Option[User] =
    sc.get[UserService].getByEmail(fbUser.email) match {
      case Some(x) => sc.get[UserService].login(x.username, x.password, domain)
      case None => throw new ObjectNotFoundException
    }

}
