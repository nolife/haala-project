package com.coldcore.haala
package service
package test

import java.io.{ByteArrayInputStream, InputStream, StringWriter}
import com.coldcore.misc5.CByte._
import FileService.Body
import org.scalatest.mockito.MockitoSugar
import org.mockito.Mockito._
import com.coldcore.haala.domain.File
import scala.collection.JavaConverters._
import freemarker.core.Environment
import core.Constants._
import core.{SiteChain, VirtualPath}
import org.apache.velocity.VelocityContext

class MyMock extends com.coldcore.haala.test.MyMock with MockitoSugar {
  val m = this
  
  val settingService = mock[SettingService]
  val solrService = mock[SolrService]
  val labelService = mock[LabelService]
  val queueService = mock[QueueService]
  val turingService = mock[TuringService]
  val domainService = mock[DomainService]
  val currencyService = mock[CurrencyService]
  val fileService = mock[FileService]
  val emailService = mock[EmailService]
  val userService = mock[UserService]
  val locationService = mock[LocationService]
  val abyssService = mock[AbyssService]
  val messageService = mock[MessageService]
  val pictureService = mock[PictureService]
  val cmsService = mock[CmsService]
  val spamService = mock[SpamService]
  val creditService = mock[CreditService]
  val paymentService = mock[PaymentService]
  val miscService = mock[MiscService]
  val attachmentService = mock[AttachmentService]
  val leaseService = mock[LeaseService]
  val securityService = mock[SecurityService]
  val facebookService = mock[FacebookService]

  val serviceContainer = new ServiceContainer {
    services =
      settingService :: solrService :: labelService :: queueService :: turingService :: domainService ::
      currencyService :: fileService :: emailService :: userService :: locationService :: abyssService ::
      messageService :: pictureService :: cmsService :: spamService :: creditService :: paymentService ::
      miscService :: attachmentService :: leaseService :: securityService :: facebookService :: Nil
  }

  val staticBlock = mock[StaticBlock]

  val fmEngine = new FreemarkerEngine |< { x => x.serviceContainer = serviceContainer }
  val vmEngine = new VelocityEngine |< { x => x.serviceContainer = serviceContainer }

  /*** helper methods ***/

  /** Register a file to be picked up by the file service */
  def registerFile(fileId: Long, path: VirtualPath, site: SiteChain, content: String) {
    val nfile = new File { id = fileId }
    val nbody = new ByteBody(content.getBytes)
    when(fileService.getByPath(path, site)).thenReturn(Some(nfile))
    when(fileService.getBody(nfile)).thenReturn(nbody)
  }

  /** Process FTL template */
  def processFtlEnv(source: String, data: Map[String,AnyRef]): (String,Environment) = {
    val template = fmEngine.template(source)
    val model = new JHashMap[String,AnyRef]
    model.putAll(data.asJava)

    val writer = new StringWriter
    val env = template.createProcessingEnvironment(model, writer)
    env.setOutputEncoding(UTF8)
    env.process

    (writer.toString, env)
  }

  /** Process FTL template */
  def processFtl(source: String, data: Map[String,AnyRef]): String = processFtlEnv(source, data)._1

  /** Process VM template */
  def processVm(source: String, data: Map[String,AnyRef]): String = {
    val template = vmEngine.template(source)
    val model = new JHashMap[String,AnyRef]
    model.putAll(data.asJava)

    val writer = new StringWriter
    val context = new VelocityContext(model)
    template.merge(context, writer)

    writer.toString
  }

}

/** File body which contains byte array as content instead of Java file object */
class ByteBody(var source: Array[Byte] = null) extends Body {
  var xSaved = false
  var xPath: String = _

  override def size = if (exists) source.length else 0
  override def exists = xSaved && source != null && xPath != null
  override def name = if (xPath != null) xPath.substring(xPath.lastIndexOf("/")+1) else null
  override def path = xPath
  override def delete = { xSaved = false; true }
  override def sourceStream: InputStream = new ByteArrayInputStream(source)
  override def write(x: InputStream) = write(toByteArray(x))

  override def write(x: Array[Byte]) {
    source = x
    xSaved = true
  }

  override def copy(dest: Body) {
    assert(exists)
    val xdest = dest.asInstanceOf[ByteBody]
    xdest.source = source
    xdest.xSaved = true
  }

  override def rename(dest: Body) = {
    assert(exists)
    val xdest = dest.asInstanceOf[ByteBody]
    xPath = xdest.xPath
    true
  }
}
