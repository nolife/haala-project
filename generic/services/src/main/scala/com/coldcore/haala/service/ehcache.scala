package com.coldcore.haala
package service

import java.net.URISyntaxException
import java.util
import java.util.Properties
import java.util.concurrent.ConcurrentHashMap
import java.util.logging.Logger
import javax.naming.{Context, NamingException}

import com.coldcore.haala.core.ApplicationContextHolder
import com.coldcore.haala.domain.BaseObject
import net.sf.ehcache.distribution.jms.JMSUtil
import net.sf.ehcache.event.{CacheEventListener, CacheEventListenerFactory}
import net.sf.ehcache.{Ehcache, Element}
import org.apache.activemq.jndi.ActiveMQInitialContextFactory

/** Replicate EhCache via ActiveMQ JMS
 *  http://ehcache.org/documentation/replication/jms-replicated-caching
 *  http://stackoverflow.com/questions/666630/adding-jms-info-breaks-ehcache
 */
class EhCacheActiveMQInitialContextFactory extends ActiveMQInitialContextFactory {

  override def getInitialContext(environment: util.Hashtable[_,_]): Context = {
    val data = new ConcurrentHashMap[String,AnyRef]
    val topicFactoryBindingName = ""+environment.get(JMSUtil.TOPIC_CONNECTION_FACTORY_BINDING_NAME)
    val queueFactoryBindingName = ""+environment.get(JMSUtil.GET_QUEUE_CONNECTION_FACTORY_BINDING_NAME)

    try {
      data.put(topicFactoryBindingName, createConnectionFactory(environment))
      data.put(queueFactoryBindingName, createConnectionFactory(environment))
    } catch {
      case e: URISyntaxException =>
        throw new NamingException("Error initialisating ConnectionFactory with message "+e.getMessage)
    }

    val topicBindingName = ""+environment.get(JMSUtil.REPLICATION_TOPIC_BINDING_NAME)
    val queueBindingName = ""+environment.get(JMSUtil.GET_QUEUE_BINDING_NAME)
    data.put(topicBindingName, createTopic(topicBindingName))
    data.put(queueBindingName, createQueue(queueBindingName))

    createContext(environment, data)
  }
}

/** Create listeners to operate on an internal domain cache used by services. */
class ResetDomainCacheEventListenerFactory extends CacheEventListenerFactory {
  private val log: Logger = Logger.getLogger(classOf[ResetDomainCacheEventListenerFactory].getName)

  override def createCacheEventListener(properties: Properties): CacheEventListener = {
    val cache = properties.getProperty("domainCacheRef")
    log.info(s"Domain cache refs: $cache")
    val beans =
      (List.empty[DomainCache[BaseObject]] /: cache.parseCSV)((a,b) =>
        ApplicationContextHolder.applicationContext.getBean(b, classOf[DomainCache[BaseObject]]) :: a)
    new ResetDomainCacheEventListener(beans)
  }
}

/** Listen to events from other distributed caches and remove elements from an internal domain cache
  * used by services so those could be re-fetched from a 2nd level cache.
  */
class ResetDomainCacheEventListener(beans: List[DomainCache[BaseObject]]) extends CacheEventListener {
  // [lower case package] [camel case class] [ID]   com.coldcore.haala.domain.MyEntity#1613
  val ENTITY_CLASS_REGEX = "[a-z\\.]*[A-Z]+[a-zA-Z]*#[0-9]+"

  private def removeDomainObject(e: Element) =
    if (e.getObjectKey.toString.matches(ENTITY_CLASS_REGEX)) {
      val pair = e.getObjectKey.toString.split("#")
      beans.map(_.remove(pair.last.toLong, pair.head))
    }

  override def notifyElementRemoved(c: Ehcache, e: Element) = removeDomainObject(e)
  override def notifyElementExpired(c: Ehcache, e: Element) = removeDomainObject(e)
  override def notifyElementEvicted(c: Ehcache, e: Element) = removeDomainObject(e)
  override def notifyElementPut(c: Ehcache, e: Element) = removeDomainObject(e)
  override def notifyElementUpdated(c: Ehcache, e: Element) = removeDomainObject(e)

  override def notifyRemoveAll(c: Ehcache) {}

  override def dispose {}

  override def clone: AnyRef = {
    super.clone
    new ResetDomainCacheEventListener(beans)
  }
}