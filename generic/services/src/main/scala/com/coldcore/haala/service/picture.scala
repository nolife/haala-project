package com.coldcore.haala
package service

import dao.GenericDAO
import com.coldcore.misc5.CByte._
import com.coldcore.gfx._
import beans.BeanProperty
import service.FileService.Body
import service.exceptions.InvalidFormatException
import java.awt.{Color, Graphics2D, Image}
import java.awt.image.BufferedImage
import core.{Constants, VirtualPath}
import org.springframework.transaction.annotation.Transactional
import domain.{File, Picture}

object PictureService {
  case class Fit(width: Int, height: Int, maxWidth: Int, maxHeight: Int)
  case class CropResult(data: Array[Byte], width: Int, height: Int)
}

trait PictureService {
  import PictureService._

  def getById(id: Long): Option[Picture]
  def delete(id: Long)
  def create(pos: Int, typ: Int, w: Int, h: Int): Picture
  def update(o: Picture, w: Int, h: Int)
  def attachBody(o: Picture, path: VirtualPath, site: String, body: Body, original: Body)
  def reattachBody(o: Picture, body: Body, sfx: String)
  def imageSize(data: Array[Byte]): (Int, Int)
  def imageSize(x: Body): (Int, Int)
  def imageSize(x: File): (Int, Int)
  def crop(body: Body, quality: Float, size: List[String]): CropResult
  def cropImg(img: Image, fits: Seq[Fit]): BufferedImage
  def blankJpeg(w: Int, h: Int): Array[Byte]
}

@Transactional
class PictureServiceImpl extends BaseService with PictureService {
  import PictureService._

  @BeanProperty var pictureDao: GenericDAO[Picture] = _

  /** Get an object by its ID. */
  @Transactional (readOnly = true)
  override def getById(id: Long): Option[Picture] = pictureDao.findById(id)

  /** Delete a picture. */
  @Transactional (readOnly = false)
  override def delete(id: Long) = getById(id).foreach { x =>
    pictureDao.delete(x)
    sc.get[FileService].delete(x.file.id) //delete a file
  }

  /** Create a picture. */
  @Transactional (readOnly = false)
  override def create(pos: Int, typ: Int, w: Int, h: Int): Picture = {
    val o = new Picture
    o.position = pos; o.`type` = typ; o.width = w; o.height = h
    pictureDao.saveOrUpdate(o)
    pictureDao.flush // acquire picture ID
    o
  }

  /** Update a picture. */
  @Transactional (readOnly = false)
  override def update(o: Picture, w: Int, h: Int) {
    o.width = w
    o.height = h
    pictureDao.saveOrUpdate(o)
  }

  /** Attach a default and original bodies to a picture (create a new picture file). */
  @Transactional (readOnly = false)
  override def attachBody(o: Picture, path: VirtualPath, site: String, body: Body, original: Body) {
    o.file = sc.get[FileService].create(path, site, body)
    pictureDao.saveOrUpdate(o)
    pictureDao.flush // acquire file ID
    sc.get[FileService].attachBody(o.file.id, File.SUFFIX_ORIGINAL, original)
  }

  /** Re-attach a body to a picture with specific suffix (re-use existing picture file). */
  @Transactional (readOnly = true)
  override def reattachBody(o: Picture, body: Body, sfx: String) =
    sc.get[FileService].attachBody(o.file.id, sfx, body)

  /** Convert into an image and return its size. */
  @Transactional (readOnly = true)
  override def imageSize(data: Array[Byte]): (Int, Int) = {
    val img = Gfx.createImage(data)
    if (img == null) throw new InvalidFormatException
    (img.getWidth(null), img.getHeight(null))
  }

  @Transactional (readOnly = true)
  override def imageSize(x: Body): (Int, Int) =
    withOpen(x.sourceStream) { r => imageSize(toByteArray(r)) }

  @Transactional (readOnly = true)
  override def imageSize(x: File): (Int, Int) = imageSize(sc.get[FileService].getBody(x))

  /** Crop/scale an image to fit rectangles provided. */
  @Transactional (readOnly = true)
  override def crop(body: Body, quality: Float, size: List[String]): CropResult = {
    val img = withOpen(body.sourceStream) { r => Gfx.createImage(toByteArray(r)) }
    if (img == null) throw new InvalidFormatException

    //all rectangles to select the best fit: WxH or WxH-WxH
    val fits =
      for (s <- size) yield {
        val arr = (if (s.indexOf("-") == -1) s+"-"+s else s).split("-")
        val (a, b) = (arr.head.split("x"), arr.last.split("x"))
        Fit(a.head.toInt, a.last.toInt, b.head.toInt, b.last.toInt)
      }

    val croppedImg = cropImg(img, fits)
    val croppedData = Jpeg.write(croppedImg, quality)
    CropResult(croppedData, croppedImg.getWidth, croppedImg.getHeight)
  }

  /** Crop/scale an image to fit a rectangle 'fits' selected by the best fit. */
  @Transactional (readOnly = true)
  override def cropImg(img: Image, fits: Seq[Fit]): BufferedImage = {
    val (iw, ih) = (img.getWidth(null), img.getHeight(null))
    val il = if (iw > ih) if (iw/ih >= 2)
      Picture.LAYOUT_PANORAMA else Picture.LAYOUT_HORIZONTAL else Picture.LAYOUT_VERTICAL

    //select the best fit by matching with image's layout
    var (fw, fh, fwMax, fhMax) = (0, 0, 0, 0)
    for (f <- fits) {
      val fl = if (f.width > f.height) if (f.width/f.height >= 2)
        Picture.LAYOUT_PANORAMA else Picture.LAYOUT_HORIZONTAL else Picture.LAYOUT_VERTICAL
      if (fl == il || fw == 0) { fw = f.width; fh = f.height; fwMax = f.maxWidth; fhMax = f.maxHeight }
    }

    val sp = new ScaleParameters
    sp.setImageType(ImageType.TYPE_INT_RGB)
    val lp = new LayerParameters
    lp.setAlign(Align.CENTER, Align.CENTER)

    if (fw == fwMax && fh == fhMax) {
      //fixed size - scale and crop
      sp.setMinSize(0, fh); sp.setMaxSize(0, fh)
      val scaledH = Scale.scaleImage(img, sp)
      sp.setMinSize(fw, 0); sp.setMaxSize(fw, 0)
      val scaledW = Scale.scaleImage(img, sp)
      val scaled  = if (scaledH.getWidth >= fw) scaledH else scaledW
      val croppedImg = Gfx.createImage(fw, fh, ImageType.TYPE_INT_RGB)
      Layer.addLayer(croppedImg, scaled, lp)
      croppedImg
    } else {
      //scale to fit min/max size
      sp.setMinSize(fw, fh); sp.setMaxSize(fwMax, fhMax)
      Scale.scaleImage(img, sp)
    }
  }

  @Transactional (readOnly = true)
  override def blankJpeg(w: Int, h: Int): Array[Byte] = {
    val img = Gfx.createImage(w, h, ImageType.TYPE_INT_RGB)
    val g2 = img.getGraphics.asInstanceOf[Graphics2D]
    g2.setColor(new Color((math.random*255).toInt, (math.random*255).toInt, (math.random*255).toInt))
    g2.fillRect(0,0,w,h)
    g2.dispose()
    Jpeg.write(img, 0.1F)
  }
}

/** Scale and crop operations used in packs */
trait ScaleAndCrop {
  self: BaseService =>

  import PictureService._

  case class ScaleResult(crop: CropResult, body: Body, sfx: String)

  type QualityAndSize = (String,List[String],String) //eg. quality: 0.8, size: [800x600, 600x800], default suffix

  /** Scale a picture based on QaS parameter */
  def scale(body: Body, qas: QualityAndSize): ScaleResult = {
    val (quality, size, sfx) = (qas._1.toFloat, qas._2, qas._3)
    val result = sc.get[service.PictureService].crop(body, quality, size)
    val cropped = sc.get[FileService].body
    cropped.write(result.data)
    ScaleResult(result, cropped, sfx)
  }

  /** Scale a picture based on QaS parameters */
  def scale(body: Body, qas: List[QualityAndSize]): List[ScaleResult] =
    qas.map(scale(body, _))

  /** Re-scale a picture and re-attach default and additional bodies */
  def rescaleAndUpdate(o: Picture, scalePicture: (Body, Int) => List[ScaleResult]) {
    val (obody, dbody) = (sc.get[FileService].getBody(o.file, File.SUFFIX_ORIGINAL), sc.get[FileService].getBody(o.file))
    val scaled = scalePicture(if (obody.exists) obody else dbody, o.`type`) // use original or default body as source
    sc.get[service.PictureService].update(o, scaled.head.crop.width, scaled.head.crop.height)
    reattachScaledBodies(o, scaled)
  }

  /** Re-attach already scaled default and additional bodies */
  def reattachScaledBodies(o: Picture, scaled: List[ScaleResult]) =
    scaled.foreach(x => sc.get[service.PictureService].reattachBody(o, x.body, x.sfx))

  /** Create a scaled picture with a new file attached. */
  def createScaledPicture(body: Body, position: Int, typ: Int,
                          scalePicture: (Body, Int) => List[ScaleResult], path: Picture => VirtualPath,
                          site: String = Constants.SITE_PUBLIC): Picture = {
    val scaled = scalePicture(body, typ)
    val o = sc.get[service.PictureService].create(position, typ, scaled.head.crop.width, scaled.head.crop.height)
    sc.get[service.PictureService].attachBody(o, path(o), site, scaled.head.body, original = body)
    reattachScaledBodies(o, scaled.tail)  
    o
  }

}
