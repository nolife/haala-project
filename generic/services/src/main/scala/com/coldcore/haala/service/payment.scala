package com.coldcore.haala
package service

import com.coldcore.haala.service.SolrService.ValueTerm
import core._
import core.GeneralUtil.configToMills
import exceptions._
import domain.{AbyssEntry, User}
import org.springframework.transaction.annotation.Transactional

import beans.BeanProperty
import com.google.gson.Gson

object PaymentService {
  case class CreatePaymentIN(pack: String, ref: String, value: String, meta: String, pin: PaymentIN)
  case class PaymentIN(amount: Long, currency: String, expire: Long = 0) // amount x100

  class SearchInputX(from: Long, max: Int, orderBy: String = "") extends SearchInput(from, max, orderBy) {
    def this(in: SearchInput) = this(in.from, in.max, in.orderBy)

    val ref: Option[String] = None // non-unique abyss REF
    val pack: Option[String] = None
    val fullRef: Option[String] = None // unique payment REF stored in meta
    val username: Option[String] = None
  }

  /** Payment config JSON */
  class PaymentConfig {
    @BeanProperty var keep: String = _ // time to keep abyss record: 180d, 1y
    @BeanProperty var expire: String = _ // time for payment to expire: 15m, 7d
    @BeanProperty var checkoutURL: String = _
  }

  /** Convert unique payment REF into non-unique abyss REF */
  //def shortRef(ref: String): String = ref.takeWhile('_'!=)

  /** Payment entry Abyss wrapper */
  class PaymentEntry(val abyss: AbyssEntry, sc: ServiceContainer) {
    def value(key: String): String = Meta(abyss.value)(key)
    def meta(key: String): String = Meta(abyss.meta)(key)

    val ref = value("ref")
    val amount = value("amount").toLong // x100
    val currency = value("currency")
    val username = value("username")
    val closed = value("closed").isTrue
    val expire = Timestamp(value("expire"))
    val active = !closed && Timestamp().compareTo(expire) < 0
    val test = Meta(abyss.value)("test", "").isTrue

    lazy val user: User = sc.get[UserService].getById(value("user").toLong).getOrElse(throw new ObjectNotFoundException)
    lazy val parent: AbyssEntry = sc.get[AbyssService].getById(value("parent").toLong).getOrElse(throw new ObjectNotFoundException)
    lazy val url: String = Option(sc).map(_ => sc.get[PaymentService].checkoutUrl(abyss.id)).orNull
  }
  object PaymentEntry {
    def apply(abyss: AbyssEntry, sc: ServiceContainer) = new PaymentEntry(abyss, sc)
  }

  trait Mod {
    /** Process paid abyss entry */
    def process(abyss: AbyssEntry): Boolean = false
  }

  /** Handler to process external events from 3rd party payment sytem */
  trait Handler {
    def onEvent(eventAbyssId: Long)
  }
}

trait PaymentService {
  import PaymentService._

  def getById(abyssId: Long): Option[PaymentEntry]
  def create(userId: Long, in: CreatePaymentIN): PaymentEntry
  def create(userId: Long, abyssId: Long, in: PaymentIN): PaymentEntry
  def checkoutUrl(abyssId: Long): String
  def search(in: SearchInputX): SearchResult[PaymentEntry]
  def searchByRef(ref: String): Option[PaymentEntry]
  def push(abyssId: Long, test: Boolean = false)
  def close(abyssId: Long)
  def handler(name: String): Handler
  def message(abyssId: Long)
}

@Transactional
class PaymentServiceImpl extends BaseService with PaymentService {
  import PaymentService._

  private val sorlDynamicType = "pay"

  private var mods = List.empty[Mod]
  def setMods(x: JList[Mod]) = mods = x.toList

  private var handlers = Map.empty[String,Handler]
  def setHandlers(x: JMap[String,Handler]) = handlers = x.toMap

  override def handler(name: String): Handler =
    handlers.getOrElse(name, throw new ObjectNotFoundException)

  /** Payment configuration */
  private def paymentConfig: PaymentConfig =
    new Gson().fromJson(setting("PaymentConf"), classOf[PaymentConfig])

  private def fullRef(ref: String, userRef: String, abyssId: Long): String =
    (ref+"_"+abyssId.hex+"_"+userRef).toUpperCase

  /** Get an object by its ID. */
  @Transactional (readOnly = true)
  override def getById(abyssId: Long): Option[PaymentEntry] =
    sc.get[AbyssService].getById(abyssId).map(PaymentEntry(_, sc))

  /** Create new payment entry and new parent abyss entry. */
  @Transactional (readOnly = false)
  override def create(userId: Long, in: CreatePaymentIN): PaymentEntry = {
    val keep = Timestamp.fromMills(System.currentTimeMillis+configToMills(paymentConfig.keep))
    val abyssA = sc.get[AbyssService].create(AbyssService.CreateIN(in.ref, in.pack,
      AbyssService.ValueMetaIN(value = in.value, meta = in.meta), keep))
    create(userId, abyssA.id, in.pin)
  }

  /** Create new payment entry for existing parent abyss entry. */
  @Transactional (readOnly = false)
  override def create(userId: Long, abyssId: Long, in: PaymentIN): PaymentEntry = {
    val abyss = sc.get[AbyssService].getById(abyssId).getOrElse(throw new ObjectNotFoundException)
    val keep = Timestamp.fromMills(System.currentTimeMillis+configToMills(paymentConfig.keep))
    val user = sc.get[UserService].getById(userId).getOrElse(throw new ObjectNotFoundException)

    val expire = Timestamp.fromMills(System.currentTimeMillis+configToMills(paymentConfig.expire))
    val ref = fullRef(abyss.ref, user.ref, abyss.id)

    val value = Meta("") +
      ("parent", abyss.id) +
      ("ref", ref) +
      ("amount", in.amount) +
      ("currency", in.currency) +
      ("expire", if (in.expire > 0) in.expire else expire) +
      ("user", user.id) +
      ("username", user.username) +
      ("closed", "n")

    val meta = Meta("") +
      ("s_typ", sorlDynamicType) +
      ("s_ref", ref) +
      ("s_usr", user.username)

    val abyssB = sc.get[AbyssService].create(abyss.id,
      AbyssService.ValueMetaIN(value = value.serialize, meta = meta.serialize))

    PaymentEntry(abyssB, sc)
  }

  /** Checkout URL by full payment ref. */
  private def checkoutUrl(ref: String): String = {
    val url = paymentConfig.checkoutURL // http://example.org/pay.htm?ref=foo   http://example.org/pay/foo
    url+(if (url.split("/").last.contains(".")) "?ref=" else "/")+ref
  }

  /** Checkout URL for existing payment entry. */
  @Transactional (readOnly = true)
  override def checkoutUrl(abyssId: Long): String = {
    val abyss = sc.get[AbyssService].getById(abyssId).getOrElse(throw new ObjectNotFoundException)
    checkoutUrl(Meta(abyss.value)("ref"))
  }

  /** Solr search */
  @Transactional (readOnly = true)
  override def search(in: SearchInputX): SearchResult[PaymentEntry] = {
    val result = sc.get[AbyssService].search(new AbyssService.SearchInputX(in.from, in.max, in.orderBy) {
      override val ref = in.ref
      override val pack = in.pack
      override val dynamicTerms =
        ValueTerm("s_typ", sorlDynamicType) :: (
          in.fullRef.map(ValueTerm("s_ref", _)) ::
          in.username.map(ValueTerm("s_usr", _)) ::
          Nil).flatten
      override val sortSub = Map("username" -> "s_usr")
    })
    SearchResult(result.total, result.objects.map(PaymentEntry(_, sc)))
  }

  /** Solr search by unique payment REF */
  @Transactional (readOnly = true)
  override def searchByRef(x: String): Option[PaymentEntry] =
    search(new SearchInputX(0,1) { override val fullRef = Some(x) }).objects.headOption

  /** Push payment entry for further processing by mods */
  @Transactional (readOnly = false)
  override def push(abyssId: Long, test: Boolean = false) {
    val entry = sc.get[AbyssService].getById(abyssId).map(PaymentEntry(_, sc)).getOrElse(throw new ObjectNotFoundException)
    if (entry.closed) throw new InvalidStatusException // closed payment
    if (test && !entry.test) throw new InvalidStatusException // process test payment
    if (!mods.exists(_.process(entry.parent))) throw new NotSupportedException
    close(abyssId)
  }

  /** Mark payment as closed */
  @Transactional (readOnly = false)
  override def close(abyssId: Long) {
    val entry = sc.get[AbyssService].getById(abyssId).map(PaymentEntry(_, sc)).getOrElse(throw new ObjectNotFoundException)
    if (!entry.closed) sc.get[AbyssService].update(abyssId,
      AbyssService.ValueMetaIN(value = (Meta(entry.abyss.value) + ("closed" -> "y")).serialize))
  }

  /** Sent payment message to admin */
  @Transactional (readOnly = false)
  override def message(abyssId: Long) = {
    val entry = sc.get[AbyssService].getById(abyssId).map(PaymentEntry(_, sc)).get
    sc.get[MessageService].payment(MessageService.PaymentIN(entry.ref, entry.abyss.id, entry.user.id))
  }

}
