package com.coldcore.haala
package service

import org.springframework.transaction.annotation.Transactional
import com.coldcore.haala.domain.User
import scala.collection.JavaConverters._
import beans.BeanProperty
import org.springframework.security.core.{Authentication, GrantedAuthority}
import org.springframework.security.core.authority.SimpleGrantedAuthority
import org.springframework.security.core.userdetails.{UsernameNotFoundException, UserDetailsService, UserDetails}

trait SecurityService {
  def isUserInRole(role: String): Boolean
  def isPrincipalMissing: Boolean
  def pushPrincipal(user: User, roles: String*)
  def removePrincipal
  def getPrincipal: Option[UserDetails]
}

@Transactional
class SecurityServiceImpl extends BaseService with SecurityService {

  class CustomAuthentication(principal: CustomUserDetails) extends Authentication {
    override def getAuthorities: JCollection[GrantedAuthority] = principal.getAuthorities
    override def getCredentials = principal
    override def getDetails = principal
    override def getPrincipal = principal
    override def isAuthenticated = true
    override def setAuthenticated(b: Boolean) {}
    override def getName = principal.getUsername
  }

  class CustomUserDetails extends UserDetails {
    @BeanProperty var authorities: JCollection[GrantedAuthority] = _
    @BeanProperty var password: String = _
    @BeanProperty var username: String = _
    override def isAccountNonExpired = true
    override def isAccountNonLocked = true
    override def isCredentialsNonExpired = true
    override def isEnabled = true
  }

  /** Check if a user has a specified role assigned. */
  @Transactional (readOnly = true)
  override def isUserInRole(role: String): Boolean =
    if (isPrincipalMissing) false
    else {
      val principal = sb.securityContext.getAuthentication.getPrincipal.asInstanceOf[UserDetails]
      principal.getAuthorities.exists(_.getAuthority == role)
    }

  /** Check if a user does not have a principal stored. */
  @Transactional (readOnly = true)
  override def isPrincipalMissing: Boolean = {
    val authentication = sb.securityContext.getAuthentication
    authentication == null || authentication.getPrincipal== null ||
      !authentication.getPrincipal.isInstanceOf[UserDetails]
  }

  /** Get principal. */
  @Transactional (readOnly = true)
  override def getPrincipal: Option[UserDetails] =
    Option(sb.securityContext.getAuthentication.getPrincipal.asInstanceOf[UserDetails])

  /** Stores a principal. Inputs: a user and a list of roles assigned to her. */
  @Transactional (readOnly = true)
  override def pushPrincipal(user: User, roles: String*) {
    val auth = user.roles.map(r => new SimpleGrantedAuthority(r.role)) ++
      roles.map(r => new SimpleGrantedAuthority(r))
    val principal = new CustomUserDetails {
      username = user.username
      authorities = auth.asJava.asInstanceOf[JCollection[GrantedAuthority]]
    }
    sb.securityContext.setAuthentication(new CustomAuthentication(principal))
  }

  /** Removes a principal. */
  @Transactional (readOnly = true)
  override def removePrincipal = sb.securityContext.setAuthentication(null)
}

class DummyUserDetailsService extends UserDetailsService {
  override def loadUserByUsername(username: String): UserDetails = throw new UsernameNotFoundException("Not implemented")
}
