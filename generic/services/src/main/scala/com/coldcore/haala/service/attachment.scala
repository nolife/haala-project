package com.coldcore.haala
package service

import dao.GenericDAO

import beans.BeanProperty
import FileService.Body
import com.coldcore.haala.core.VirtualPath
import org.springframework.transaction.annotation.Transactional
import domain.{Attachment, File}

trait AttachmentService {
  def getById(id: Long): Option[Attachment]
  def delete(id: Long)
  def create(pos: Int, typ: Int): Attachment
  def attachBody(o: Attachment, path: VirtualPath, site: String, body: Body)
  def reattachBody(o: Attachment, body: Body)
}

@Transactional
class AttachmentServiceImpl extends BaseService with AttachmentService {
  @BeanProperty var attachmentDao: GenericDAO[Attachment] = _

  /** Get an object by its ID. */
  @Transactional (readOnly = true)
  override def getById(id: Long): Option[Attachment] = attachmentDao.findById(id)

  /** Delete aa attachment. */
  @Transactional (readOnly = false)
  override def delete(id: Long) = getById(id).foreach { x =>
    attachmentDao.delete(x)
    sc.get[FileService].delete(x.file.id) //delete a file
  }

  /** Create an Attachment. */
  @Transactional (readOnly = false)
  override def create(pos: Int, typ: Int): Attachment = {
    val o = new Attachment
    o.position = pos; o.`type` = typ
    attachmentDao.saveOrUpdate(o)
    o
  }

  /** Attach a body to an attachment. */
  @Transactional (readOnly = false)
  override def attachBody(o: Attachment, path: VirtualPath, site: String, body: Body) {
    o.file = sc.get[FileService].create(path, site, body)
  }

  /** Re-attach a body to an attachment. */
  @Transactional (readOnly = true)
  override def reattachBody(o: Attachment, body: Body) {
    sc.get[FileService].attachBody(o.file.id, File.SUFFIX_DEFAULT, body)
  }
}
