package com.coldcore.haala
package service

import java.net.URLEncoder
import core.CoreException
import core.HttpUtil._
import core.Constants.UTF8
import domain.BaseObject
import dao.GenericDAO
import org.springframework.transaction.annotation.Transactional
import xml.{Node, XML}
import org.apache.http.HttpStatus
import scala.collection.mutable.ListBuffer

object SolrService {
  type SubmitDoc = Seq[Any]
  type ResultDoc = Map[Symbol,Any]

  /** "Term", "SolrDoc" and "SolrQuery" are replacements for SubmitDoc (which is sequence of Symbol/String pairs)
    *  and ResultDoc. Methods operating on terms have WT suffix for "With Term". With the replacement the code
    *  should be more readable, strongly typed, pattern matched and easier to understand and debug.
    */

  sealed abstract class Term
  sealed abstract class IdTerm extends Term { val solrField = "id" }

  /** Key and value (multi purpose class: used in SolrDoc, SolrQuery and as query parameter) */
  case class ValueTerm(key: String, value: Any) extends Term

  /** Common Solr query parameters */
  def FlTerm(value: String) = ValueTerm("fl", value)
  def StartTerm(value: Long) = ValueTerm("start", value)
  def RowsTerm(value: Int) = ValueTerm("rows", value)
  def SortTerm(value: String) = ValueTerm("sort", value)

  /** Solr facet parameters */
  def FacetTerm = ValueTerm("facet", true)
  def FacetQueryTerm(value: SolrQuery) = ValueTerm("facet.query", value)
  def FacetFieldTerm(value: String) = ValueTerm("facet.field", value)
  def FacetLimitTerm(value: Int) = ValueTerm("facet.limit", value)
  def FacetOffsetTerm(value: Long) = ValueTerm("facet.offset", value)
  def FacetSortTerm(value: String) = ValueTerm("facet.sort", value)

  /** No operation
    * eg. if (filter) TextTerm(filter) else NoopTerm
    */
  case object NoopTerm extends Term

  /** Free form text */
  case class TextTerm(text: String) extends Term

  /** Range */
  case class RangeTerm(key: String, from: Any = "*", to: Any = "*") extends Term

  /** Unique object type to differentiate between objects stored in Solr */
  case class ObjectTypeTerm(typeOfObject: Int) extends Term {
    val solrField = "o_typ"
  }

  /** Database ID and type of an object
    * 2 4343 -> 1_0000000000000004343 (sort by Solr ID works correctly)
    */
  case class ObjectIdTerm(objectId: Long, typeTerm: ObjectTypeTerm) extends IdTerm {
    def toCombinedIdTerm: CombinedIdTerm =  CombinedIdTerm(solrId(typeTerm.typeOfObject, objectId))
  }

  /** Solr ID of an object (combination of object database ID and the type of object)
    * 2_0000000000000004343 -> 4343 2   2_4343 -> 4343 2
    */
  case class CombinedIdTerm(combinedId: String) extends IdTerm {
    def toObjectIdTerm: ObjectIdTerm = {
      val typeAndId = combinedId.split("_")
      ObjectIdTerm(typeAndId.last.toLong, ObjectTypeTerm(typeAndId.head.toInt))
    }
  }

  /** Solr query.
    * Note: It can take only one term and the rest should be embedded into that term or "expandAsQuery" will
    * provide a query with missing links (may be refactored later):
    *   AndTerm(a,b) AndTerm(c,d) --> "a AND b [missing] c AND d"
    *   AndTerm(AndTerm(a,b) AndTerm(c,d)) --> "a AND b [AND] c AND d"
    */
  case class SolrQuery(terms: Term*) //Note: can take only one term and the rest should be embedded into that term (make default AND|OR and take multiple terms then embed)

  /** Solr operations ADN, OR, NOT */
  case class NotTerm(term: Term) extends Term
  case class AndTerm(terms: Term*) extends Term
  case class OrTerm(terms: Term*) extends Term

  /** Solr document */
  case class SolrDoc(terms: Term*) {
    def objectIdTerm: Option[ObjectIdTerm] =
      terms.find(_.isInstanceOf[IdTerm]) match {
        case Some(t @ ObjectIdTerm(_,_)) => Some(t)
        case Some(t @ CombinedIdTerm(_)) => Some(t.toObjectIdTerm)
        case _ => None
      }
  }

  /** Convert object ID into Solr ID and vice versa (sort by Solr ID works correctly).
   *  1234 -> 1_0000000000000001234   1_0000000000000001234 -> 1234
   */
  def solrId(typ: Int, id: String): Long = id.substring(id.indexOf('_')+1).toLong
  def solrId(typ: Int, id: Long): String = {
    val x = "".padTo(19-id.toString.length, "0").mkString+id
    typ+"_"+x
  }

  /** Make initial doc. Inputs: object ID and object type. */
  def solrDoc(typ: Int, id: Long): List[Any] = List('id, solrId(typ, id), 'o_typ, typ)

  /** Return a list of object IDs extracted from Solr docs. */
  def solrObjectIds(docs: ResultDoc*): Seq[Long] = for (doc <- docs) yield solrId(0, doc('id).toString)

  /** Return a list of object IDs extracted from Solr docs. */
  def solrObjectIdsWT(docs: SolrDoc*): Seq[Long] = for (doc <- docs) yield doc.objectIdTerm.get.objectId

  /** Escape special characters.
   *  + - && || ! ( ) { } [ ] ^ \" ~ * ? : \\
   */
  def solrEscapeChars(x: String): String = {
    val r = List("\\", "+", "-", "&&", "||", "!", "(", ")", "{", "}", "[", "]", "^", "\"", "~", "*", "?", ":")
    (x /: r)((a, b) => a.replace(b, "\\"+b))
  }

  /** Convert simple DB order by parameters into Solr sort (parameter names should be lowercased).
   *  Inputs: object prefix, order-by parameters and their substitute map.
   *  category, id desc, random -> ol_cat asc, id desc, random_# asc
   */
  def solrSort(pfx: String, orderBy: String, sub: Map[String,String] = Map.empty): String =
    (for (prm <- orderBy.parseCSV) yield {
      val pair = prm.trim.split(" ")
      val (a, b) = (pair.head.trim.toLowerCase, if (pair.size > 1) pair.last.trim else "asc")
      a match {
        case "id" => a+" "+b
        case "random" => "random_"+System.currentTimeMillis+" "+b
        case x if x.startsWith("random_") => x+" "+b
        case _ => pfx+"_"+sub.getOrElse(a, a)+" "+b
      }
    }).mkString(", ")

  // dynamic fields prefix "[pfx]_cat"
  val DYNAMIC_PFX_STRING     = "s" // string
  val DYNAMIC_PFX_INT        = "i" // int
  val DYNAMIC_PFX_LONG       = "l" // long
  val DYNAMIC_PFX_INT_RANGE  = "n" // int range
  val DYNAMIC_PFX_LONG_RANGE = "r" // long range

  /** Dynamic fields "[type]_cat" */
  private val dynamicKeys = (xs: Iterable[String]) => {
    val pfx =
      DYNAMIC_PFX_STRING ::
      DYNAMIC_PFX_INT ::
      DYNAMIC_PFX_LONG ::
      DYNAMIC_PFX_INT_RANGE ::
      DYNAMIC_PFX_LONG_RANGE ::
      Nil
    xs.filter(x => pfx.map(_+"_").exists(x.startsWith))
  }

  /** Filter fields matching dynamic syntax from parameter/value map and convert to Solr parameter list.
   *  i_cat -> oa_i_cat
   */
  def solrDynamicFields(pfx: String, xs: Map[String,Any]): List[Any] =
    dynamicKeys(xs.keys).map(key => Symbol(pfx+"_"+key) :: xs(key) :: Nil).toList.flatten

  def solrDynamicFieldsWT(pfx: String, xs: Map[String,Any]): List[Term] =
    dynamicKeys(xs.keys).map(key => ValueTerm(pfx+"_"+key, xs(key))).toList

  /** Cut off field prefix (facetQueries/Fields output).
   *  oe_ttl:headline -> headline
   */
  def trimFieldPrefix(in: FacetResult): FacetResult =
    FacetResult(in.total,
      in.objects.map { case (k,v) => val x = k.contains(":") ? k.substring(k.indexOf(":")+1) | k; (x, v) })

  /** Remove fields with zero count (facetQueries/Fields output). */
  def removeZeroFields(in: FacetResult): FacetResult =
    FacetResult(in.total, in.objects.filter(_._2 != 0))

  /** Remove fields with specific value (facetQueries/Fields output). */
  def removeNamedFields(in: FacetResult, values: String*): FacetResult =
    FacetResult(in.total, in.objects.filterNot(x => values.contains(x._1)))

  /** Sum values of facet fields with the same name preserving original order
    * oe_cco:UK,100 oe_cco:EU,23 oe_cco:UK,5 -> oe_cco:UK,105 oe_cco:EU,23
    */
  def merge(in: FacetResult): FacetResult = {
    val groups = in.objects.groupBy(_._1).map { case (k,x) => k -> x.foldLeft(0L)(_+_._2) }
    val xs = in.objects.map { case (k,v) => k -> groups(k) }
    var keys = Set.empty[String]
    val objects = (List.empty[(String,Long)] /: xs) { (r,kv) => if (keys(kv._1)) r else { keys += kv._1; r :+ kv }}
    FacetResult(in.total, objects)
  }

  /** Split a result into facets one per each field preserving original order in each result
    * oe_cco:UK,23 oe_cco:EU,12 oe_cit:London,21 -> FacetResult(oe_cco:UK,23 oe_cco:EU,12) FacetResult(oe_cit:London,21)
    */
  def split(in: FacetResult): Map[String,FacetResult] = {
    val prefix = (k: String) => k.takeWhile(':' !=).mkString
    val map = collection.mutable.Map[String, FacetResult]()
    in.objects.map { case (k,_) => map += prefix(k) -> FacetResult(0, List.empty) }
    in.objects.foreach { case (k,v) => map += prefix(k) -> FacetResult(0, map(prefix(k)).objects :+ (k,v)) }
    map.toMap
  }
}

trait SolrService {
  import SolrService._

  def search(q: String, prms: Any*): SearchResult[ResultDoc]
  def searchWT(q: String, prms: Term*): SearchResult[SolrDoc]
  def searchWT(q: SolrQuery, prms: Term*): SearchResult[SolrDoc]
  def facetQueriesWT(q: SolrQuery, prms: Term*): FacetResult
  def facetFieldsWT(q: SolrQuery, prms: Term*): FacetResult
  def submit(docs: SubmitDoc*)
  def submitWT(docs: SolrDoc*)
  def submitNoCommit(docs: SubmitDoc*)
  def submitNoCommitWT(docs: SolrDoc*)
  def delete(q: String)
  def deleteWT(q: SolrQuery)
  def deleteByIds(ids: Any*)
  def deleteByIdsWT(ids: IdTerm*)
  def commit
  def rollback
  def optimize
  def fetchObjects[T <: BaseObject](result: SearchResult[ResultDoc], dao: GenericDAO[T], c: Class[T]): SearchResult[T]
  def fetchObjectsWT[T <: BaseObject](result: SearchResult[SolrDoc], dao: GenericDAO[T], c: Class[T]): SearchResult[T]
}

/** Service for external Solr search engine. */
@Transactional
class SolrServiceImpl extends BaseService with SolrService {
  import SolrService._

  implicit class AnyX(a: Any) {
    def encode = URLEncoder.encode(a.toString, UTF8)
  }

  /** Return <add> XML string ready to be submitted to Solr. */
  protected def constructAddXml(docs: SolrDoc*): String =
    <add>{
      for (doc <- docs) yield
        <doc>{ for (term <- doc.terms) yield term match {
          case x @ ObjectIdTerm(_, typeTerm) =>
            <field name={x.solrField}>{x.toCombinedIdTerm.combinedId}</field>
            <field name={typeTerm.solrField}>{typeTerm.typeOfObject}</field>
          case x @ CombinedIdTerm(_) =>
            <field name={x.solrField}>{x.combinedId}</field>
            <field name={x.toObjectIdTerm.typeTerm.solrField}>{x.toObjectIdTerm.typeTerm.typeOfObject}</field>
          case ValueTerm(name, value) =>
            <field name={name}>{value}</field>
          case NoopTerm => // ignore
          case x => throw new CoreException("Unsupported term: "+x)
        }}</doc>
    }</add> toString

  /** Return <delete> XML string ready to be submitted to Solr. */
  protected def constructDeleteByIdsXml(ids: IdTerm*): String =
    <delete>{
      for (term <- ids) yield {
        <id>{
          term match {
            case x @ ObjectIdTerm(_,_) => x.toCombinedIdTerm.combinedId
            case CombinedIdTerm(combinedId) => combinedId
            //sealed class, no catch all required
        }}</id>
      }
    }</delete> toString

  /** Return <delete> XML string ready to be submitted to Solr. */
  protected def constructDeleteXml(q: SolrQuery): String =
    <delete><query>{expandAsQuery(q)}</query></delete> toString

  /** Return <add> XML string ready to be submitted to Solr.
   *  Input: array of docs where each doc is an array (name text ... name text).
   */
  protected def prepareAddXml(docs: SubmitDoc*): String =
    <add>{
      for (x <- docs) yield
        <doc>{ x.grouped(2).map { case Seq(n: Symbol, t) => <field name={n.name}>{t}</field> } }</doc>
    }</add> toString

  /** Return <delete> XML string ready to be submitted to Solr. Input: array of IDs. */
  protected def prepareDeleteByIdsXml(ids: Any*): String =
    <delete>{ ids.map(x => <id>{x}</id>) }</delete> toString

  /** Return <delete> XML string ready to be submitted to Solr. Input: query. */
  protected def prepareDeleteXml(q: String): String =
    <delete><query>{q}</query></delete> toString

  /** Check if response is OK. */
  private def assertResponseOk(hc: HttpCall): HttpCallSuccessful = hc match {
    case x: HttpCallSuccessful if HttpStatus.SC_OK == x.code => x
    case x: HttpCallSuccessful => throw new CoreException("URL error (code "+x.code+") "+x.url+"\n"+x.body)
    case x: HttpCallFailed => throw x.error
  }

  private def updateUrl =
    setting("SolrURL")+"/update"

  /** Parse Solr XML: response section. */
  protected def parse_response(s: String): SearchResult[ResultDoc] = {
    val xml = XML.loadString(s)
    val nameResponse = (node: Node) => node.attribute("name").exists(_.text == "response")
    val total = ((xml \\ "result" filter nameResponse) \ "@numFound").text.toLong
    val objects =
      (for (doc <- (xml \\ "result" filter nameResponse) \\ "doc") yield
        (doc \ "_").map { node =>
          val name = (node \ "@name").text
          val value = node match {
            case <int>{v}</int> => v.text.toInt
            case <long>{v}</long> => v.text.toLong
            case v => v.text
          }
          Symbol(name) -> value
        }.toMap).toList
    SearchResult(total, objects)
  }

  private def resultDocToSolrDoc(doc: ResultDoc): SolrDoc = {
    val terms =
      doc.map { case (n,v) =>
        n.name match {
          case "id" => CombinedIdTerm(v.toString) //Solr returns combined ID (eg. 2_4343)
          case x => ValueTerm(x, v)
        }
      }.toList
    SolrDoc(terms: _*)
  }

  /** Parse Solr XML: response section. */
  protected def parse_responseWT(s: String): SearchResult[SolrDoc] = {
    val x = parse_response(s)
    SearchResult(x.total, x.objects.map(resultDocToSolrDoc))
  }

  /** Parse Solr XML: facet_queries section. */
  protected def parse_facetQueries(s: String): FacetResult = {
    val xml = XML.loadString(s)
    val nameResponse = (node: Node) => node.attribute("name").exists(_.text == "response")
    val total = ((xml \\ "result" filter nameResponse) \ "@numFound").text.toLong
    val nameFacet = (node: Node) => node.attribute("name").exists(_.text == "facet_queries")
    val objects =
      (for (lst <- xml \\ "lst" filter nameFacet) yield
        (lst \ "_").map { node =>
          val name = (node \ "@name").text
          val value = node.text.toLong
          (name, value)
        }).flatten.toList
    FacetResult(total, objects)
  }

  /** Parse Solr XML: facet_fields section. */
  protected def parse_facetFields(s: String): FacetResult = {
    val xml = XML.loadString(s)
    val nameResponse = (node: Node) => node.attribute("name").exists(_.text == "response")
    val total = ((xml \\ "result" filter nameResponse) \ "@numFound").text.toLong
    val nameFacet = (node: Node) => node.attribute("name").exists(_.text == "facet_fields")
    val objects =
      (for (lst <- (xml \\ "lst" filter nameFacet) \ "lst") yield {
        val field = (lst \ "@name").text
        (lst \ "_").map { node =>
          val name = (node \ "@name").text
          val value = node.text.toLong
          (field+":"+name, value)
        }}).flatten.toList
    FacetResult(total, objects)
  }

  protected def prepareSelectQuery(q: String, prms: Any*): String =
    setting("SolrURL")+"/select/?q="+q.encode+
      (if (prms.nonEmpty) prms.grouped(2).map { case Seq(n: Symbol, x) => "&"+n.name+"="+x.encode }.mkString else "")

  protected def constructSelectQuery(q: String, prms: Term*): String =
    setting("SolrURL")+"/select/?q="+q.encode+expandAsQuery(prms: _*)

  protected def constructSelectQuery(q: SolrQuery, prms: Term*): String =
    setting("SolrURL")+"/select/?q="+expandAsQuery(q).encode+expandAsQuery(prms: _*)

  /** Expand a query into format which Solr query understands (eg. o_typ:1 AND ol_key:venice) */
  protected def expandAsQuery(q: SolrQuery): String = {
    def exp(term: Term): String = term match {
      case x @ ObjectTypeTerm(typeOfObject) => x.solrField+":"+typeOfObject // o_typ:1
      case ValueTerm(name, value) => name+":"+((value.toString.contains(" ") && value.toString.take(1) != "\"") ? ("\""+value+"\"") | value) // ol_key:venice   ol_key:"venice italy"
      case RangeTerm(name, from, to) => s"$name:[$from TO $to]" // ol_prc:[200 TO 300]   ol_prc:[* TO 300]
      case TextTerm(text) => text // "venice italy"
      case AndTerm(xs @ _*) => xs.map(exp).filter(""!=).mkString(" AND ") // term AND term AND term
      case OrTerm(xs @ _*) => "("+xs.map(exp).filter(""!=).mkString(" OR ")+")" // (term OR term OR term)
      case NotTerm(x) => "-"+exp(x) // -ol_key:venice
      case NoopTerm => "" //eg. if (filter) TextTerm(filter) else NoopTerm
      case x => throw new CoreException("Unsupported term: "+x)
    }
    q.terms.map(exp).mkString
  }

  /** Expand parameters into format which Solr query understands (eg. start=1 & fl=id & rows=10) */
  protected def expandAsQuery(prms: Term*): String = prms.map({
    case ValueTerm(name, value: SolrQuery) => "&"+name+"="+expandAsQuery(value).encode
    case ValueTerm(name, value) => "&"+name+"="+value.encode
    case NoopTerm => // ignore
    case x => throw new CoreException("Unsupported term: "+x)
  }).mkString

  /** Execute a query and parse "response" result.
   *  Input: query and a map of optional parameters (key/value).
   */
  @Transactional (readOnly = true)
  override def search(q: String, prms: Any*): SearchResult[ResultDoc] =
    parse_response(assertResponseOk(sb.httpGet(prepareSelectQuery(q, prms: _*))).body)

  /** Execute a query and parse "response" result. */
  @Transactional (readOnly = true)
  override def searchWT(q: String, prms: Term*): SearchResult[SolrDoc] =
    parse_responseWT(assertResponseOk(sb.httpGet(constructSelectQuery(q, prms: _*))).body)

  /** Execute a query and parse "response" result. */
  @Transactional (readOnly = true)
  override def searchWT(q: SolrQuery, prms: Term*): SearchResult[SolrDoc] =
    parse_responseWT(assertResponseOk(sb.httpGet(constructSelectQuery(q, prms: _*))).body)

  /** Execute a query and parse "facet_counts/facet_queries" result.
   *  Input: query and a map of optional parameters (key/value).
   */
  @Transactional (readOnly = true)    
  override def facetQueriesWT(q: SolrQuery, prms: Term*): FacetResult =
    parse_facetQueries(assertResponseOk(sb.httpGet(constructSelectQuery(q, FacetTerm +: RowsTerm(0) +: prms: _*))).body)

  /** Execute a query and parse "facet_counts/facet_queries" result.
   *  Input: query and a map of optional parameters (key/value).
   */
  @Transactional (readOnly = true)
  override def facetFieldsWT(q: SolrQuery, prms: Term*): FacetResult =
    parse_facetFields(assertResponseOk(sb.httpGet(constructSelectQuery(q, FacetTerm +: RowsTerm(0) +: prms: _*))).body)

  /** Submit to Solr.
   *  Input: array of docs where each doc is an array (name text ... name text).
   */
  @Transactional (readOnly = true)
  override def submit(docs: SubmitDoc*): Unit = {
    submitNoCommit(docs: _*)
    commit
  }

  /** Submit to Solr. */
  @Transactional (readOnly = true)
  override def submitWT(docs: SolrDoc*): Unit = {
    submitNoCommitWT(docs: _*)
    commit
  }

  /** Post XML to Solr update URL. */
  private val httpPostXml = (content: String) =>
    sb.httpPost(updateUrl, content, ("Content-type", "text/xml; charset=UTF-8") :: Nil)

  /** Submit to Solr without committing.
   *  Input: array of docs where each doc is an array (name text ... name text).
   */
  @Transactional (readOnly = true)
  override def submitNoCommit(docs: SubmitDoc*): Unit =
    assertResponseOk(httpPostXml(prepareAddXml(docs: _*)))

  /** Submit to Solr without committing. */
  @Transactional (readOnly = true)
  override def submitNoCommitWT(docs: SolrDoc*): Unit =
    assertResponseOk(httpPostXml(constructAddXml(docs: _*)))

  /** Delete records. Input: query. */
  @Transactional (readOnly = true)
  override def delete(q: String): Unit = {
    assertResponseOk(httpPostXml(prepareDeleteXml(q)))
    commit
  }

  /** Delete records. Input: query. */
  @Transactional (readOnly = true)
  override def deleteWT(q: SolrQuery): Unit = {
    assertResponseOk(httpPostXml(constructDeleteXml(q)))
    commit
  }

  /** Delete record. Input: array of IDs. */
  @Transactional (readOnly = true)
  override def deleteByIds(ids: Any*): Unit = {
    assertResponseOk(httpPostXml(prepareDeleteByIdsXml(ids: _*)))
    commit
  }

  /** Delete record. Input: array of Terms (IdTerm). */
  @Transactional (readOnly = true)
  override def deleteByIdsWT(ids: IdTerm*): Unit = {
    assertResponseOk(httpPostXml(constructDeleteByIdsXml(ids: _*)))
    commit
  }

  @Transactional (readOnly = true)
  override def commit =
    assertResponseOk(httpPostXml(<commit/> toString))

  @Transactional (readOnly = true)
  override def rollback =
    assertResponseOk(httpPostXml(<rollback/> toString))

  @Transactional (readOnly = true)
  override def optimize = 
    assertResponseOk(httpPostXml(<optimize/> toString))


  /** Fetch objects found by Solr from DB and order those. */
  @Transactional (readOnly = true)
  override def fetchObjects[T <: BaseObject](result: SearchResult[ResultDoc], dao: GenericDAO[T], c: Class[T]): SearchResult[T] =
    if (result.objects.isEmpty) SearchResult(result.total, List.empty[T])
    else {
      val query = "from "+c.getName+" where id in ("+solrObjectIds(result.objects: _*).mkString(",")+")"
      val objs = dao.find(query)
      val byId = (id: Long) => objs.find(_.id == id)
      SearchResult(result.total, solrObjectIds(result.objects: _*).flatMap(byId(_)).toList)
    }

  /** Fetch objects found by Solr from DB and order those. */
  @Transactional (readOnly = true)
  override def fetchObjectsWT[T <: BaseObject](result: SearchResult[SolrDoc], dao: GenericDAO[T], c: Class[T]): SearchResult[T] =
    if (result.objects.isEmpty) SearchResult(result.total, List.empty[T])
    else {
      val query = "from "+c.getName+" where id in ("+solrObjectIdsWT(result.objects: _*).mkString(",")+")"
      val objs = dao.find(query)
      val byId = (id: Long) => objs.find(_.id == id)
      SearchResult(result.total, solrObjectIdsWT(result.objects: _*).flatMap(byId(_)).toList)
    }
}

package builder {

  import SolrService._
  import SolrDocBuilder._
  import domain._

  object SolrDocBuilder {
    private type T = SolrDocBuilder
    type TermMaker = (String, Any) => Term

    val defaultTermMaker: TermMaker = (key, value) => ValueTerm(key, value)

    def apply(id: Long, typ: Int) = new SolrDocBuilder(id, typ, defaultTermMaker)
    def apply(id: Long, typ: Int, termMaker: TermMaker) = new SolrDocBuilder(id, typ, termMaker)
  }

  class SolrDocBuilder(id: Long, typ: Int, termMaker: TermMaker) {
    val terms = ListBuffer[Term](ObjectIdTerm(id, ObjectTypeTerm(typ)))
    def build: SolrDoc = SolrDoc(terms: _*)

    private def add(t: Term*): T = {
      terms ++= t
      this
    }

    def term(t: Term*): T = add(t: _*)
    def some(t: Option[Term]*): T = add(t.flatten: _*)
    def value(key: String, value: Any): T = add(termMaker(key, value))
    def value(keyval: (String,Any)*): T = add(keyval.map { case (k,v) => termMaker(k, v) }: _*)
  }

  object EntitySolrDocBuilder {
    def apply(o: BaseObject, typ: Int) = new EntitySolrDocBuilder(o, typ, defaultTermMaker)
    def apply(o: BaseObject, typ: Int, termMaker: (String, Any) => Term) = new EntitySolrDocBuilder(o, typ, termMaker)
  }

  class EntitySolrDocBuilder(o: BaseObject, typ: Int, termMaker: TermMaker) extends SolrDocBuilder(o.id, typ, termMaker) {
    private type T = EntitySolrDocBuilder

    override def value(key: String, value: Any): T = {
      super.value(key, value)
      this
    }

    override def value(keyval: (String,Any)*): T = {
      super.value(keyval: _*)
      this
    }

    def option(key: String, typ: Int, take: Int = 0): T = {
      val options = o.asInstanceOf[{ def options: JList[BaseOption] }].options
      val xs = options
          .filter(_.`type` == typ)
          .map(op => termMaker(key, op.value))
      terms ++= (if (take > 0) xs.take(take) else xs)
      this
    }

    def option(keytyp: (String,Int)*): T = {
      keytyp.map { case (k,t) => option(k, t) }
      this
    }

    def domains(key: String = "dom"): T = {
      val domains = o.asInstanceOf[{ def domains: JList[Domain] }].domains
      terms ++= domains.map(x => termMaker(key, x.id))
      this
    }
  }

}