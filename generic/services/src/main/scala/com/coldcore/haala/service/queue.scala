package com.coldcore.haala
package service

import core.CoreException
import beans.BeanProperty
import com.coldcore.misc5.IdGenerator._
import collection.mutable.ListBuffer
import org.springframework.transaction.annotation.Transactional

trait QueueService {
  def pop(group: String): Option[Any]
  def push(group: String, value: Any)
  def pushDelayed(group: String, value: Any, delay: Long)
  def pushUniqueDelayed(group: String, value: Any, uniq: Any, delay: Long)
  def pushNonExistentDelayed(group: String, value: Any, uniq: Any, delay: Long)
  def pushUnique(group: String, value: Any, uniq: Any)
  def pushNonExistent(group: String, value: Any, uniq: Any)
}

@Transactional
class QueueServiceImpl extends BaseService with QueueService {
  @BeanProperty var queueCache: Cache[Any] = _

  case class Holder(value: Any, uniq: Any, delay: Long)

  /** Generate a unique key. */
  private def generateUniqueKey(qseq: Seq[String]): String =
    Iterator.fill(10000)(generate(100,100).toUpperCase).find(x => !qseq.contains(x)).
      getOrElse(throw new CoreException("Ref generator exhausted"))

  /** Return matched entry's value in a group.
   *  Inputs: group to query, remove entries matched by a filter, stop after the first match or not, a match function.
   */
  private def matchEntry(group: String, remove: Boolean, stop: Boolean, matchF: Holder => Boolean): Option[Any] = {
    val qskey = "keys.seq."+group
    queueCache.get(qskey).map { x =>
      val qseq = x.asInstanceOf[ListBuffer[String]]
      qseq.filter(queueCache.get(_).isEmpty).map(qseq -= _) //remove unused keys (as part of maintenance)
      val matchedKeys = qseq.filter(x => matchF(queueCache.get(x).get.asInstanceOf[Holder]))

      if (matchedKeys.nonEmpty) {
        if (remove && !stop) matchedKeys.map(qseq -= _) //remove all matched keys if requested
        if (remove && stop) qseq -= matchedKeys.head //remove the first matched key if requested
        //return the first or the last matched object from cache as requested
        Some(queueCache.get(if (stop) matchedKeys.head else matchedKeys.last).get.asInstanceOf[Holder].value)
      } else None
    }.getOrElse(None)
  }

  /** Retrieve the next value from a group. */
  @Transactional (readOnly = true)
  override def pop(group: String): Option[Any] = queueCache.synchronized {
    val mills = System.currentTimeMillis
    matchEntry(group, remove = true, stop = true, (x: Holder) => { mills >= x.delay })
  }

  /** Push a value into a group. */
  private def doPush(group: String, value: Any, uniq: Any, delay: Long) = queueCache.synchronized {
    val qskey = "keys.seq."+group
    if (queueCache.get(qskey).isEmpty) queueCache.set(qskey, new ListBuffer[String])
    val qseq = queueCache.get(qskey).get.asInstanceOf[ListBuffer[String]]
    val key = generateUniqueKey(qseq)
    qseq += key
    queueCache.set(key, Holder(value, uniq, delay))
  }

  /** Push a value into a group. */
  @Transactional (readOnly = true)
  override def push(group: String, value: Any) = doPush(group, value, "", 0)

  /** Push a value into a group making it available to retrieve after delay. */
  @Transactional (readOnly = true)
  override def pushDelayed(group: String, value: Any, delay: Long) = doPush(group, value, "", System.currentTimeMillis+delay)

  /** Push a value into a group and remove others with the same 'uniq'.
   *  Value can only be retrieved after the delay.
   */
  @Transactional (readOnly = true)
  override def pushUniqueDelayed(group: String, value: Any, uniq: Any, delay: Long) = queueCache.synchronized {
    matchEntry(group, remove = true, stop = false, (x: Holder) => { uniq == x.uniq })
    doPush(group, value, uniq, System.currentTimeMillis+delay)
  }

  /** Push a value into a group if it is not already there (match by 'uniq').
   *  Value can only be retrieved after the delay.
   */
  @Transactional (readOnly = true)
  override def pushNonExistentDelayed(group: String, value: Any, uniq: Any, delay: Long) = queueCache.synchronized {
    if (matchEntry(group, remove = false, stop = true, (x: Holder) => { uniq == x.uniq }).isEmpty)
      doPush(group, value, uniq, System.currentTimeMillis+delay)
  }

  /** Push a value into a group and remove others with the same 'uniq'. */
  @Transactional (readOnly = true)
  override def pushUnique(group: String, value: Any, uniq: Any) = pushUniqueDelayed(group, value, uniq, 0)

  /** Push a value into a group if it is not already there (match by 'uniq'). */
  @Transactional (readOnly = true)
  override def pushNonExistent(group: String, value: Any, uniq: Any) = pushNonExistentDelayed(group, value, uniq, 0)
}
