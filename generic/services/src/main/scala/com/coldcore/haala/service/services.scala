package com.coldcore.haala
package service

import java.util.concurrent.locks.ReentrantReadWriteLock
import java.util.concurrent.{Callable, TimeUnit}
import java.util.Properties

import exceptions.ObjectNotFoundException
import domain.{BaseObject, Role}
import org.springframework.beans.factory.InitializingBean

import reflect.ClassTag
import beans.BeanProperty
import domain.annotations.{Site, SiteGroup}
import com.coldcore.misc.scala.ReflectUtil._
import com.coldcore.misc5.IdGenerator.generate
import com.coldcore.misc5.CByte._
import dao.GenericDAO
import java.io.{InputStream, Reader, StringReader}
import scala.collection.mutable

import core._
import core.NetworkUtil.hostAddresses
import core.Constants.UTF8

trait Dependencies {
  @BeanProperty var serviceContainer: ServiceContainer = _
  lazy val sc = serviceContainer

  val staticBlock = new StaticBlock
  lazy val sb = staticBlock

  @BeanProperty var velocityEngine: VelocityEngine = _
  @BeanProperty var freemarkerEngine: FreemarkerEngine = _
  @BeanProperty var log: Log = new SLF4J
}

class BaseService extends Dependencies with SettingsReader with LabelsReader {
  /** Construct a 'where' clause for a query by splitting a site chain.
   *  Append a public site to a chain if does not start with a dot.
   * .mw1j -> (site = 'mw1j')   xx1.mw1 -> (site = 'pub' or site = 'xx1' or site = 'mw1')
   */
  def constructWhereSite(site: SiteChain, pfx: String = "o."): String =
    "("+site.bits.map(pfx+"site = '"+_+"'").mkString(" or ")+")"

  /** Select objects from a collection matching a site chain. The most-right site in a chain has priority over
   *  sites standing on the left ('pub.xx1.xx2' - will try to select 'xx2' then 'xx1' then 'pub').
   */
  def filterObjectsForSite[T <: BaseObject](list: Seq[T], site: SiteChain): List[T] =
    if (list.isEmpty) Nil
    else {
      val clazz = list.head.getClass
      val siteField = fieldByAnnotation[Site](clazz).getOrElse(
        throw new CoreException("No Site annotated field found in "+clazz))
      val groupField = fieldByAnnotation[SiteGroup](clazz).getOrElse(
        throw new CoreException("No SiteGroup annotated field found in "+clazz))

      case class X(site: String, group: String, value: T)
      val xx = for (x <- list) yield
        X(fieldValue(x, siteField).toString, fieldValue(x, groupField).toString, x)

      val result = new scala.collection.mutable.HashMap[String,X]
      val sites = site.bits
      val insert = (x: X) =>
        result.get(x.group) match {
          case Some(e) =>
            val (ePos, xPos) = (sites.indexOf(e.site), sites.indexOf(x.site))
            if (xPos > ePos) result.put(x.group, x)
          case None if sites.contains(x.site) => result.put(x.group, x)
          case _ =>
        }

      xx.map(insert)
      result.values.map(_.value).toList
    }

  def filterObjectForSite[T <: BaseObject](list: List[T], site: SiteChain): Option[T] =
    filterObjectsForSite(list, site).headOption

  /** Return results from cache or query and cache results. Inputs: cache and a key to look up cached results,
   *  a function to query results if not yet cached.
   */
  def cacheAwareResults[T <: BaseObject](cache: DomainCache[T], key: String, queryF: => List[T]): List[T] =
    cache.get(key).getOrElse { val x = queryF; cache.set(key, x); x }

  /** Construct a total query from a resulting query:
   *  select count(*) from com.coldcore.haala.domain.User o left join o.teams as t where t.weekend.id = ?
   */
  def constructTotalQuery(query: String): String = {
    val x = if (query.startsWith("from")) query else query.substring(query.indexOf(" ", 7)+1)
    "select count(*) "+(if (x.contains("order by")) x.substring(0, x.indexOf("order by")) else x)
  }

  /** Return total # of objects found and an objects' list containing requested sub set.
   *  Inputs:
   *   'query' is a query to iterate for results: 'select o from com.coldcore.haala.domain.User o left join o.teams as t where t.weekend.id = ? order by o.email asc, o.id'
   *   'total-query' is a query to get total # of objects: 'select count(*) from com.coldcore.haala.domain.User o left join o.teams as t where t.weekend.id = ?'
   *   'params' parameters to apply to both of the queries.
   */
  private def searchObjectsLogic[T <: BaseObject](from: Long, max: Int, dao: GenericDAO[T], query: String,  tquery: String, params: Any*): SearchResult[T] = {
    val p = params.asInstanceOf[Seq[AnyRef]]
    val total = dao.findCount(tquery, p: _*)
    val iter = dao.iterate(query, p: _*)
    var (addedCounter, fromCounter) = (0, from)
    val addedIds = new scala.collection.mutable.HashSet[Long]
    val objects =
      Iterator.continually(0).takeWhile(_ => addedCounter < max && iter.hasNext).flatMap { _ =>
        val o = iter.next
        val id = o.id.asInstanceOf[Long]
        if (addedIds.contains(id)) None
        else {
          addedIds.add(id)
          fromCounter -= 1
          if (fromCounter >= 0) None
          else {
            addedCounter += 1
            Some(o)
          }
        }
      }.toList
    SearchResult(total, objects)
  }

  /** Return a map with total # of objects found and an objects' list containing requested sub set.
   *  Slow performance, as it iterates through every object in a query to apply a supplied matching filter.
   *  Inputs:
   *    'query' is a query to iterate for results: 'select o from com.coldcore.haala.domain.User o left join o.teams as t where t.weekend.id = ? order by o.email asc, o.id'
   *    'filterF' is a matching filter executing complex checks which are hard to implement as a SQL query.
   *    'params' parameters to apply to a query.
   */
  def searchObjectsWithFilter[T <: BaseObject](from: Long, max: Int, dao: GenericDAO[T], query: String,  filterF: T => Boolean, params: Any*): SearchResult[T] = {
    val p = params.asInstanceOf[Seq[AnyRef]]
    val iter = dao.iterate(query, p: _*)
    var (totalCounter, addedCounter, fromCounter) = (0L, 0, from)
    val addedIds = new scala.collection.mutable.HashSet[Long]
    //iterate through results applying a filter and adding requested sub set into an objects' list
    val objects =
      Iterator.continually(0).takeWhile(_ => iter.hasNext).flatMap { _ =>
        val o = iter.next
        val id = o.id.asInstanceOf[Long]
        if (addedIds.contains(id) || !filterF(o)) None
        else {
          addedIds.add(id)
          totalCounter += 1
          fromCounter -= 1
          if (addedCounter >= max || fromCounter >= 0) None
          else {
            addedCounter += 1
            Some(o)
          }
        }
      }.toList
    SearchResult(totalCounter, objects)
  }

  /** Return total # of objects found and an objects' list containing requested sub set.
   *  Inputs: index to start from, max results to return, DAO, query, optional query to calculate total results, parameters.
   */
  def searchObjectsWithTotal[T <: BaseObject](from: Long, max: Int, dao: GenericDAO[T], query: String, tquery: String, params: Any*): SearchResult[T] =
    searchObjectsLogic(from, max, dao, query, tquery, params: _*)

  def searchObjects[T <: BaseObject](from: Long, max: Int, dao: GenericDAO[T], query: String, params: Any*): SearchResult[T] =
    searchObjectsLogic(from, max, dao, query, constructTotalQuery(query), params: _*)

  /** Expand or shrink hibernate list matching a supplied size.
   *  Hibernate BUG: Always remove last and set parent on new or a list may lose elements
   */
  def expandOrShrink[T <: BaseObject](x: JList[T], nsize: Int, doF: () => T) {
    val xsize = x.size
    (nsize until xsize).map{ _ => x.remove(x.size-1) } //shrink
    (xsize until nsize).map{ _ => x.add(doF()) } //expand and fill with initial DO
  }

  /** Write (multilingual) labels removing with empty values and rewriting the rest. */
  def writeLabels(keyF: (String) => String, sat: SiteableText*) =
    for (p <- sat) p.text.foreach { case (k, v) =>
      val key = keyF(k.name)
      if (v.safe == "") labelService.delete(key, p.site) else labelService.write(key, v, p.site)
    }

  /** Rewrite (multilingual) labels (do not remove labels with empty values). */
  def rewriteLabels(keyF: (String) => String, sat: SiteableText*) =
    for (p <- sat) p.text.foreach { case (k, v) => labelService.write(keyF(k.name), v, p.site) }
}

/** Search methods input, could be extended with more parameters. */
case class SearchInput(from: Long, max: Int, orderBy: String = "") {
  val orderByClause = if (orderBy.nonEmpty) "order by "+orderBy else ""
}

/** Search methods output. */
case class SearchResult[T](total: Long, objects: List[T]) {
  def serializeAsMap[U](onObject: T => U): Map[Symbol,Any] =
    Map('total -> total, 'objects -> objects.map(onObject))
}

/** Facet methods output. */
case class FacetResult(total: Long, objects: List[(String, Long)]) {
  def serializeAsMap[U](onObject: (String, Long) => U): Map[Symbol,Any] =
    Map('total -> total, 'objects -> objects.map(x => onObject(x._1, x._2)))
}

/** Container which holds text mappings specific to site key for multilingual operations.
  * eg. xx1: (name -> Paris, place-> Eiffel Tower)
  */
case class SiteableText(site: String, text: Map[Symbol,String])

trait Cache[T] {
  def get(key: String): Option[T]
  def get(key: String, load: => T): Option[T]
  def set(key: String, o: T)
  def remove(key: String)
  def count: Long
  def clear: Long
  def scheduledClear: Long
}

trait DomainCache[T <: BaseObject] extends Cache[List[T]] {
  def remove(o: BaseObject)
  def remove(id: Long)
  def remove(id: Long, className: String)
}

/** Cache implementation based on Guava cache */
class GuavaCache[T] extends Cache[T] with InitializingBean with Dependencies {
  import com.google.common.cache.{RemovalNotification, RemovalListener, CacheBuilder, Cache => GCache}
  import com.google.common.util.concurrent.UncheckedExecutionException

  @BeanProperty var limit: Int = _
  @BeanProperty var timeout: Int = _ //seconds
  @BeanProperty var name: String = "?"
  var cache: GCache[String,T] = _
  private val nullValue = new ObjectNotFoundCallable
  private var removed = 0

  /** requires restart to enable/disable */
  protected lazy val nocache = sc.get[SettingService].queryNoCache("Dev/NoCache").isTrue // cache disabled (dev mode flag)
  protected lazy val disabled = nocache && name == "?" // disable (except named caches: ACTIVE / QUEUE)

  override def afterPropertiesSet {
    val x = CacheBuilder.newBuilder
    if (limit > 0) x.maximumSize(limit)
    if (timeout > 0) x.expireAfterAccess(timeout, TimeUnit.SECONDS)
    x.removalListener(new RemovalListener[String,T]() {
      override def onRemoval(removal: RemovalNotification[String,T]) = onRemove(removal.getKey, removal.getValue)
    })
    cache = x.asInstanceOf[CacheBuilder[String,T]].build[String,T]()
  }

  class ObjectNotFoundCallable extends Callable[T] {
    override def call = throw new ObjectNotFoundException
  }

  /** Remove listener. */
  protected def onRemove(key: String, o: T) = removed += 1

  /** Get an object by its key. */
  override def get(key: String): Option[T] =
    try {
      Option(cache.get(key, nullValue))
    } catch { case e: UncheckedExecutionException if e.getCause.isInstanceOf[ObjectNotFoundException] => None }

  /** Get an object by its key. */
  override def get(key: String, load: => T): Option[T] =
    Option(cache.get(key, new Callable[T] { override def call = load }))

  /** Cache an object. */
  override def set(key: String, o: T) = if (!disabled) cache.put(key, o)

  /** Remove an entry by its key. */
  override def remove(key: String) = cache.invalidate(key)

  /** Return # of items in cache. */
  override def count: Long = cache.size

  /** Cache wipe. Return # of items cleared. */
  override def clear: Long = {
    val x = cache.size
    cache.invalidateAll
    x
  }

  /** Scheduled clear routine. Return # of items cleared. */
  override def scheduledClear: Long = {
    val x = removed
    removed = 0
    x
  }
}

/** Domain cache implementation based on Guava cache */
class GuavaDomainCache[T <: BaseObject] extends GuavaCache[List[T]] with DomainCache[T] {
  /* GuavaDomainCache class is created by Spring thus T.runtimeClass.getName is always BaseObject,
     this will inject proper class (e.g. Setting) to compare with in the "supports" method. */
  @BeanProperty var entityClass: JClass[T] = _ //todo default classOf[T] of fix supports to also test for T runtime class

  /** requires restart to enable/disable */
  override protected lazy val disabled = nocache || sc.get[SettingService].scaled // disable local cache if scaled

  /** Test is object is supported by this cache. */
  private def supports(o: BaseObject): Boolean = o.getClass == entityClass
  private def supports(className: String): Boolean = className == entityClass.getName

  /** Remove object by object's ID */
  override def remove(o: BaseObject) = if (supports(o)) remove(o.id)
  override def remove(id: Long) = keyById(id).foreach(remove)
  override def remove(id: Long, className: String) = if (supports(className)) remove(id)

  private val idKeyMap = mutable.Map[Long,String]()
  private val lock = new ReentrantReadWriteLock
  private val (readLock, writeLock) = (lock.readLock, lock.writeLock)

  private def keyById(id: Long): Option[String] = {
    readLock.lock
    try {
      idKeyMap.get(id)
    } finally {
      readLock.unlock
    }
  }

  /** Cache an object. */
  override def set(key: String, o: List[T]) = if (!disabled) {
    writeLock.lock
    try {
      super.set(key, o)
      o.foreach(x => idKeyMap.put(x.id, key))
    } finally {
      writeLock.unlock
    }
  }

  /** Remove listener. */
  override protected def onRemove(key: String, o: List[T]) {
    writeLock.lock
    try {
      super.onRemove(key, o)
      o.foreach(x => idKeyMap.remove(x.id))
    } finally {
      writeLock.unlock
    }
  }

  /** Cache wipe. */
  override def clear: Long = {
    writeLock.lock
    try {
      idKeyMap.clear
      super.clear
    } finally {
      writeLock.unlock
    }
  }

}

package exceptions {
  import core.CoreException

  class ServiceException(m: String, t: Throwable) extends CoreException(m, t) {
    def this() = this(null, null)
    def this(m: String) = this(m, null)
    def this(t: Throwable) = this(null, t)
  }
  class EmailExistsException extends ServiceException
  class EmailServerDownException extends ServiceException
  class InvalidFormatException extends ServiceException
  class InvalidStatusException extends ServiceException
  class LimitExceededException extends ServiceException
  class MismatchException(m: String = null) extends ServiceException(m)
  class ObjectExistsException extends ServiceException
  class ObjectExpiredException extends ServiceException
  class ObjectNotFoundException extends ServiceException
  class NotSupportedException extends ServiceException
  class ObjectProcessedException extends ServiceException
  class UsernameExistsException extends ServiceException
  class UserNotActivatedException extends ServiceException
  class UserDisabledException(@BeanProperty val ref: String) extends ServiceException
  class UserNoAgreementException(@BeanProperty val ref: String) extends ServiceException
  class ValidateException extends ServiceException
  class InvalidEmailException extends ServiceException
  class ExternalFailException extends ServiceException
  class GeneratorExhaustedException extends ServiceException
  class AuthException extends ServiceException
}

class ServiceContainer {
  //services (first match takes precedence): sc.get[SettingManager].querySystem("key")
  private var _services = List.empty[AnyRef]
  private val cache = mutable.HashMap.empty[ClassTag[_],Any]

  def services = _services

  def services_= (x: List[AnyRef]): Unit = {
    _services = x
    cache.clear()
  }

  def setServices(x: JList[AnyRef]) =
    services = x.toList

  def
  get[T](implicit m: ClassTag[T]): T =
    cache.getOrElse(m, {
      val x = _services.filter(m.runtimeClass.isInstance(_)).head.asInstanceOf[T]
      cache.put(m, x)
      x
    }).asInstanceOf[T]
}

class StaticBlock extends core.StaticBlock

class VelocityEngine {
  import org.apache.velocity.runtime.resource.loader.ClasspathResourceLoader
  import org.apache.velocity.app.{VelocityEngine => VEngine}
  import org.apache.velocity.Template
  import org.apache.velocity.exception.ResourceNotFoundException


  @BeanProperty var serviceContainer: ServiceContainer = _
  lazy val sc = serviceContainer

  private val configuration = Map[String,AnyRef](
    "resource.loader" -> "haala",
    "haala.resource.loader.cache" -> "false",
    "haala.resource.loader.modificationCheckInterval" -> "0",
    "haala.resource.loader.instance" -> new Loader
  )

  private implicit def map2props(m: Map[String,AnyRef]): Properties =
    (new Properties /: m) { case (props, (k,v)) => props.put(k, v); props }

  private val engine: VEngine = new VEngine(configuration)

  def template(name: String): Template = engine.getTemplate(name, UTF8)

  class Loader extends ClasspathResourceLoader {
    /** Load Velocity resource by source path:site.
     *  eg. foo/bar/temp.vm:xx1.xx2
     */
    override def getResourceStream(source: String): InputStream = 
      if (source == "VM_global_library.vm") super.getResourceStream(source)
      else {
        val source0 = source.contains(":") ? source | (source+":"+SiteChain.sys) // bar/my.vm:.sys
        val arr = (source0.startsWith("/") ? source0.substring(1) | source0).split(":")
        val (path, site) = (VirtualPath(arr.head), SiteChain(arr.last))
        sc.get[FileService].getByPath(path, site) match {
          case Some(x) => sc.get[FileService].getBody(x).sourceStream
          case None => throw new ResourceNotFoundException(source)
        }
      }
  }
}

class FreemarkerEngine {
  import freemarker.template._
  import freemarker.cache._
  import freemarker.ext.beans.BeansWrapper

  @BeanProperty var serviceContainer: ServiceContainer = _
  lazy val sc = serviceContainer

  private val configuration = new Configuration(Configuration.DEFAULT_INCOMPATIBLE_IMPROVEMENTS)
  private val wrapper = new DefaultObjectWrapper(Configuration.DEFAULT_INCOMPATIBLE_IMPROVEMENTS)
  wrapper.setExposureLevel(BeansWrapper.EXPOSE_ALL)
  configuration.setObjectWrapper(wrapper)
  configuration.setDefaultEncoding(UTF8)
  configuration.setLocalizedLookup(false)
  configuration.setNumberFormat("computer") // http://freemarker.org/docs/ref_directive_setting.html
  configuration.setTemplateExceptionHandler(TemplateExceptionHandler.RETHROW_HANDLER)
  configuration.setTemplateLoader(new Loader)

  def template(name: String): Template = configuration.getTemplate(name)
  def objectWrapper(): ObjectWrapper = configuration.getObjectWrapper

  private class Loader extends TemplateLoader {
    /** Load Freemarker resource by source path:site.
      *  eg. foo/bar/temp.ftl:xx1.xx2
      */
    override def findTemplateSource(source: String): AnyRef = {
      val source0 = source.contains(":") ? source | (source+":"+SiteChain.sys) // bar/my.ftl:.sys
      val arr = (source0.startsWith("/") ? source0.substring(1) | source0).split(":")
      val (path, site) = (VirtualPath(arr.head), SiteChain(arr.last))
      sc.get[FileService].getByPath(path, site) match {
        case Some(x) => withOpen(sc.get[FileService].getBody(x).sourceStream) { r => new String(toByteArray(r), UTF8) }
        case None => null
      }
    }

    override def getLastModified(templateSource: Any): Long = -1

    override def getReader(templateSource: Any, encoding: String): Reader =
      new StringReader(templateSource.asInstanceOf[String])

    override def closeTemplateSource(templateSource: scala.Any) {}
  }
}

/** Frequently used "read" methods of the SettingService. */
trait SettingsReader {
  self: { val sc: ServiceContainer } =>
  lazy val settingService = sc.get[SettingService]

  def setting(id: Long) = settingService.getById(id)
  def setting(key: String, site: SiteChain) = settingService.query(key, site)
  def setting(key: String) = settingService.querySystem(key)
  def constant[T](key: String, pfx: String, const: String)(implicit m: ClassTag[T]) = settingService.queryConstant[T](key, pfx, const)
  def parsedSites = settingService.getParsedSites
  def defaultSite = settingService.getDefaultSite
  def defaultCurrency = settingService.getDefaultCurrency
}

/** Frequently used "read" methods of the LabelService. */
trait LabelsReader {
  self: { val sc: ServiceContainer } =>
  lazy val labelService = sc.get[LabelService]

  def label(id: Long) = labelService.getById(id)
  def label(key: String, site: SiteChain) = labelService.query(key, site)
  def labels(key: String) = labelService.query(key)
}

/** Frequently used role-related methods of the SecurityService. */
trait UserRoleCheck {
  self: { val sc: ServiceContainer } =>
  lazy val securityService = sc.get[SecurityService]

  def isUserInRole(role: String) = securityService.isUserInRole(Role.role(role))
  def isUserAdmin = securityService.isUserInRole(Role.ADMIN)
}

/** Node heartbeat info */
class HeartbeatInfo extends Dependencies {
  @BeanProperty var hostname = ""
  @BeanProperty var ipPrefix = ""

  /** node unique host (change in properties) */
  def nodeHost: String =
    hostname or // properties: hostname
    (ipPrefix on { hostAddresses.find(_.startsWith(ipPrefix)).getOrElse("") }) or // properties: IP address - find matching address
    sb.localhostName or // use box hostname
    generate(8,8).toLowerCase // generate random (should not happen)
}
