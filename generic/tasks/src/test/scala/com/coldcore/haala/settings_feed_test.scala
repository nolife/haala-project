package com.coldcore.haala
package task

import org.scalatest.mockito.MockitoSugar
import org.mockito.Mockito._
import org.mockito.ArgumentMatchers.{eq => meq, _}
import java.io.{File => JFile}
import org.scalatest.{BeforeAndAfter, WordSpec}
import org.junit.runner.RunWith
import org.scalatest.junit.JUnitRunner
import org.mockito.ArgumentCaptor
import com.google.gson.Gson
import core.{MetaCSV, Meta, UrlMapping}
import core.SiteChain.Implicits._

@RunWith(classOf[JUnitRunner])
class SettingsFeedSpec extends WordSpec with MockitoSugar with BeforeAndAfter {
  var m: MyMock = _

  before {
    m = new MyMock
  }

  class MyMock extends com.coldcore.haala.service.test.MyMock {
    val task = new SettingsFeedMod { override val staticBlock = m.staticBlock; serviceContainer = m.serviceContainer }
    when(settingService.querySystem("FsPath")).thenReturn(((JFile.separator == "/") ? "" | "c:")+"/usr/haala_fs")
  }

  val json_v1_merge_urlmapping = """
    {
      "version": "1",
      "content": [
        { "baseDir": "settings-cp1", "site": "cp1", "action": "merge-urlmapping",
          "items": [
            { "key": "UrlMapping", "file": "UrlMapping.json" }
          ]}
      ]
    }
    """

  //merge target (existing UrlMapping setting)
  val urlMapping_cp1 = """
    {
      "mapping": [
        { "pattern": "/", "handler": "dynamicCtrl",
          "handlerConf": {
            "index": "00018",
            "pageName": "home/home",
            "options": "ftl-page",
            "downtime": "0"
          }},
        { "pattern": "login.htm", "handler": "dynamicCtrl",
          "handlerConf": {
            "index": "00login",
            "pageName": "login/login",
            "options": "ftl-page",
            "mod": "turingCtrlMod",
            "facade": "UserF",
            "downtime": "1"
          }}
      ]
    }
    """

  //this will be merged into UrlMapping setting
  val urlMapping_json = """
    {
      "mapping": [
        { "pattern": "ad/manager/toyshop/main.htm", "handler": "dynamicCtrl",
          "handlerConf": {
            "index": "02toyshopMain",
            "pageName": "manager/toyshop/main",
            "options": "ftl-page",
            "downtime": "1"
          }},
        { "pattern": "ad/manager/toyshop/edit.htm", "handler": "dynamicCtrl",
          "handlerConf": {
            "index": "02toyshopEdit",
            "pageName": "manager/toyshop/edit",
            "options": "ftl-page",
            "downtime": "1"
          }}
      ]
    }
    """

  //this will be merged into UrlMapping setting
  val urlMapping_dup_json = """
    {
      "mapping": [
        { "pattern": "login.htm", "handler": "loginCtrl" }
      ]
    }
    """

  "merge-urlmapping" should {
    "merge 2 UrlMapping settings" in {
      when(m.staticBlock.fileExists(any.asInstanceOf[JFile])).thenReturn(true)
      when(m.staticBlock.readFile(any.asInstanceOf[JFile])).thenReturn(urlMapping_json.getBytes)
      when(m.settingService.query("UrlMapping", ".cp1")).thenReturn(urlMapping_cp1)
      val file = new JFile("x-settings.json")
      when(m.staticBlock.readFile(file)).thenReturn(json_v1_merge_urlmapping.getBytes)

      m.task.process(file)
      val arg = ArgumentCaptor.forClass(classOf[String])
      verify(m.settingService).write(meq("UrlMapping"), arg.capture, meq("cp1"))
      val urlMapping = Option(new Gson().fromJson(arg.getValue, classOf[UrlMapping])).get
      assertResult(4) { urlMapping.mapping.size }
      assertResult("/") { urlMapping.mapping(0).pattern }
      assertResult("login.htm") { urlMapping.mapping(1).pattern }
      assertResult("ad/manager/toyshop/main.htm") { urlMapping.mapping(2).pattern }
      assertResult("ad/manager/toyshop/edit.htm") { urlMapping.mapping(3).pattern }
    }

    "merge 2 UrlMapping settings (duplicate check)" in {
      when(m.staticBlock.fileExists(any.asInstanceOf[JFile])).thenReturn(true)
      when(m.staticBlock.readFile(any.asInstanceOf[JFile])).thenReturn(urlMapping_dup_json.getBytes)
      when(m.settingService.query("UrlMapping", ".cp1")).thenReturn(urlMapping_cp1)
      val file = new JFile("x-settings.json")
      when(m.staticBlock.readFile(file)).thenReturn(json_v1_merge_urlmapping.getBytes)

      m.task.process(file)
      val arg = ArgumentCaptor.forClass(classOf[String])
      verify(m.settingService).write(meq("UrlMapping"), arg.capture, meq("cp1"))
      val urlMapping = Option(new Gson().fromJson(arg.getValue, classOf[UrlMapping])).get
      assertResult(2) { urlMapping.mapping.size }
      assertResult("/") { urlMapping.mapping(0).pattern }
      assertResult("login.htm") { urlMapping.mapping(1).pattern }
      assertResult("loginCtrl") { urlMapping.mapping(1).handler }
    }
  }

  val json_v1_merge_csvmap = """
    {
      "version": "1",
      "content": [
        { "baseDir": "settings-cp1", "site": "cp1", "action": "merge-csvmap",
          "items": [
            { "key": "Constants", "value": "toyshop.SOLR_TYPE_TOYSHOP=101" }
          ]}
      ]
    }
    """

  "merge-csvmap" should {
    "merge 2 CSV map settings" in {
      when(m.settingService.query("Constants", ".cp1")).thenReturn("estate.SOLR_TYPE_HOUSE=2, estate.SOLR_TYPE_VILLA=3")
      val file = new JFile("x-settings.json")
      when(m.staticBlock.readFile(file)).thenReturn(json_v1_merge_csvmap.getBytes)

      m.task.process(file)
      val arg = ArgumentCaptor.forClass(classOf[String])
      verify(m.settingService).write(meq("Constants"), arg.capture, meq("cp1"))
      val meta = Meta(arg.getValue)
      assertResult(Some("2")) { meta.get("estate.SOLR_TYPE_HOUSE") }
      assertResult(Some("3")) { meta.get("estate.SOLR_TYPE_VILLA") }
      assertResult(Some("101")) { meta.get("toyshop.SOLR_TYPE_TOYSHOP") }
    }
  }

  val json_v1_merge_csv = """
    {
      "version": "1",
      "content": [
        { "baseDir": "settings-cp1", "site": "cp1", "action": "merge-csv",
          "items": [
            { "key": "InstalledPacks", "value": "estate" }
          ]}
      ]
    }
    """

  "merge-csv" should {
    "merge 2 CSV settings" in {
      when(m.settingService.query("InstalledPacks", ".cp1")).thenReturn("toyshop, minishop")
      val file = new JFile("x-settings.json")
      when(m.staticBlock.readFile(file)).thenReturn(json_v1_merge_csv.getBytes)

      m.task.process(file)
      val arg = ArgumentCaptor.forClass(classOf[String])
      verify(m.settingService).write(meq("InstalledPacks"), arg.capture, meq("cp1"))
      val meta = MetaCSV(arg.getValue)
      assertResult(Some("estate")) { meta.get("estate") }
      assertResult(Some("toyshop")) { meta.get("toyshop") }
      assertResult(Some("minishop")) { meta.get("minishop") }
    }
  }

}