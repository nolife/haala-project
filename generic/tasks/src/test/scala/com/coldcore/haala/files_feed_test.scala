package com.coldcore.haala
package task

import org.scalatest.mockito.MockitoSugar
import org.mockito.Mockito._
import org.mockito.ArgumentMatchers._
import org.mockito.ArgumentCaptor
import java.io.{File => JFile}

import com.coldcore.haala.core.{SiteChain, VirtualPath}
import org.scalatest.{BeforeAndAfter, WordSpec}
import service.FileService.IN
import service.test.ByteBody
import org.junit.runner.RunWith
import org.scalatest.junit.JUnitRunner
import domain.File
import core.SiteChain.Implicits._
import core.VirtualPath.Implicits._

@RunWith(classOf[JUnitRunner])
class FilesFeedSpec extends WordSpec with MockitoSugar with BeforeAndAfter {
  var m: MyMock = _

  before {
    m = new MyMock
  }

  class MyMock extends com.coldcore.haala.service.test.MyMock {
    val task = new FilesFeedMod { override val staticBlock = m.staticBlock; serviceContainer = m.serviceContainer }
    when(settingService.querySystem("FsPath")).thenReturn(((JFile.separator == "/") ? "" | "c:")+"/usr/haala_fs")
  }

  val json_v1 = """
    {
      "version": "1",
      "content": [
        { "baseDir": "x-mc1", "baseFolder": "web/gfx/x", "site": "mc1", "action": "upload",
          "items": [
            { "files": [
                "header-logo.gif"
            ]},

            { "subDir": "user-manager", "subFolder": "user-manager", "files": [
                "attachment-pdf.png",
                "attachment-txt.png"
            ]}
          ]},

        { "baseDir": "x-mc2", "baseFolder": "web/gfx/x", "site": "mc2", "action": "upload",
          "items": [
            { "files": [
                "help-close.gif"
            ]}
          ]}
      ]
    }
    """

  "process" should {
    "parse JSON v1 and create new files or update originals" in {
      val file = new JFile("foo-files.json")
      val body = new ByteBody("1234567890".getBytes)
      when(m.staticBlock.readFile(file)).thenReturn(json_v1.getBytes)
      when(m.staticBlock.fileExists(any.asInstanceOf[JFile])).thenReturn(true)
      when(m.fileService.getByPath(any[VirtualPath], any[SiteChain])).thenReturn(None)
      when(m.fileService.getByPath("web/gfx/x/header-logo.gif", ".mc1")).thenReturn(Some(new File { id = 7L }))
      when(m.fileService.body(anyString)).thenReturn(body)
      m.task.process(file)
      verify(m.fileService).update(7, IN("web/gfx/x/header-logo.gif", "mc1"), body)
      verify(m.fileService).create(IN("web/gfx/x/user-manager/attachment-pdf.png", "mc1"), body)
      verify(m.fileService).create(IN("web/gfx/x/user-manager/attachment-txt.png", "mc1"), body)
      verify(m.fileService).create(IN("web/gfx/x/help-close.gif", "mc2"), body)
    }
  }

}