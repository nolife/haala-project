package com.coldcore.haala
package task

import org.scalatest.{BeforeAndAfter, FlatSpec, Matchers}
import org.scalatest.mockito.MockitoSugar
import org.mockito.Mockito._
import org.mockito.ArgumentMatchers._
import org.mockito.ArgumentCaptor
import xml.{Node, XML}
import java.io.{File => JFile}
import org.junit.runner.RunWith
import org.scalatest.junit.JUnitRunner

@RunWith(classOf[JUnitRunner])
class LabelsFeedSpec extends FlatSpec with MockitoSugar with BeforeAndAfter with Matchers {
  var m: MyMock = _

  before {
    m = new MyMock
  }

  class MyMock extends com.coldcore.haala.service.test.MyMock {
    val task = new LabelsFeedMod { override val staticBlock = m.staticBlock; serviceContainer = m.serviceContainer }
  }

  val xml_simple = """
    <config>
      <version>1.00</version>
      <insert site="mc1">
        <item key="noobj">Ooooops</item>
      </insert>
      <insert site="mc2">
        <item key="noobj">Uuuuups</item>
      </insert>
    </config>
    """

  val xml_html = """
    <config>
      <version>1.00</version>
      <insert site="mc1">
        <item key="noobj">
          <![CDATA[
            <div id="no-object">
              <h3>Ooooops</h3>
            </div>
          ]]>
        </item>
      </insert>
      <insert site="mc2">
        <item key="noobj">
          <![CDATA[
            <div id="no-object">
              <h3>Uuuuups</h3>
            </div>
          ]]>
        </item>
      </insert>
    </config>
    """

  it should "process xml" in {
    val file = new JFile("foo.xml")
    when(m.staticBlock.loadXmlFile(file)).thenReturn(XML.loadString(xml_simple))
    m.task.process(file)
    verify(m.labelService).write("noobj", "Ooooops", "mc1")
    verify(m.labelService).write("noobj", "Uuuuups", "mc2")
  }

  it should "process xml with html" in {
    val file = new JFile("foo.xml")
    when(m.staticBlock.loadXmlFile(file)).thenReturn(XML.loadString(xml_html))
    m.task.process(file)
    val x1 = """<div id="no-object">
               |<h3>Ooooops</h3>
               |</div>""".stripMargin
    val x2 = """<div id="no-object">
               |<h3>Uuuuups</h3>
               |</div>""".stripMargin
    verify(m.labelService).write("noobj", x1, "mc1")
    verify(m.labelService).write("noobj", x2, "mc2")
  }

}
