package com.coldcore.haala
package task

import directive.freemarker.FreemarkerDirectives
import core.CoreException
import core.SiteChain.Implicits._
import domain.{AbyssEntry, Domain, User}
import service.MessageService.{ErrorIN, MessageEntry}
import service.MiscService.FreemarkerTemplateIN
import org.junit.runner.RunWith
import org.scalatest.{BeforeAndAfter, FlatSpec, Matchers}
import org.scalatest.mockito.MockitoSugar
import org.mockito.Mockito._
import org.mockito.ArgumentMatchers._
import org.scalatest.junit.JUnitRunner

@RunWith(classOf[JUnitRunner])
class ForwardMessagesTaskSpec extends FlatSpec with MockitoSugar with BeforeAndAfter with Matchers {
  var m: MyMock = _

  before {
    m = new MyMock
  }

  class MyMock extends com.coldcore.haala.service.test.MyMock {
    val task = new ForwardMessagesTask {
      serviceContainer = m.serviceContainer
      freemarkerDirectives = mock[FreemarkerDirectives]
    }

    when(userService.getById(1)).thenReturn(Some(new User { id = 1L; ref = "*"; email = "system@example.org" }))
    when(userService.getById(2)).thenReturn(Some(new User { id = 2L; ref = "FOO"; email = "foo@example.org" }))

    // freemarker setup
    when(settingService.getDefaultSite).thenReturn("xx1")
    when(task.freemarkerDirectives.getAsMap).thenReturn(Map.empty[String,Map[String,AnyRef]])
    when(domainService.getByType(Domain.TYPE_CONTROL_PANEL)).thenReturn(None)
  }

  it should "fail if template does not exist" in {
    val abyss = new AbyssEntry { id = 4L; pack = ""; meta = "i_mtp=1;l_usr=2;s_red=n;s_sub=1;" }
    val msg = new MessageEntry(abyss, m.serviceContainer)

    when(m.miscService.freemarkerTemplate(any[FreemarkerTemplateIN])).thenReturn(None)

    a [CoreException] should be thrownBy m.task.process(msg)
  }

  it should "forward message to support email if for system user" in {
    val abyss = new AbyssEntry { id = 4L; pack = ""; meta = "i_mtp=1;l_usr=1;s_red=n;s_sub=1;" }
    val msg = new MessageEntry(abyss, m.serviceContainer)

    when(m.miscService.freemarkerTemplate(any[FreemarkerTemplateIN])).thenAnswer {
      _.getArgument[FreemarkerTemplateIN](0).template match {
        case "var/email/msg1.ftl:xx1" => Some("templateA")
        case x => throw new Exception("Invalid template: "+x)
      }
    }

    m.task.process(msg)

    verify(m.emailService).toSupport("system@example.org", "templateA", "xx1")
  }

  it should "forward support message to user email" in {
    val abyss = new AbyssEntry { id = 4L; pack = ""; meta = "i_mtp=1;l_usr=2;s_red=n;s_sub=1;" }
    val msg = new MessageEntry(abyss, m.serviceContainer)

    when(m.miscService.freemarkerTemplate(any[FreemarkerTemplateIN])).thenAnswer {
      _.getArgument[FreemarkerTemplateIN](0).template match {
        case "var/email/msg1.ftl:xx1" => Some("templateA")
        case x => throw new Exception("Invalid template: "+x)
      }
    }

    m.task.process(msg)

    verify(m.emailService).fromSupport("foo@example.org", "templateA", "xx1")
  }

  it should "forward support message to user email (pack template)" in {
    val abyss = new AbyssEntry { id = 4L; pack = "mypack"; meta = "i_mtp=1;l_usr=2;s_red=n;s_sub=1;" }
    val msg = new MessageEntry(abyss, m.serviceContainer)

    when(m.miscService.freemarkerTemplate(any[FreemarkerTemplateIN])).thenAnswer {
      _.getArgument[FreemarkerTemplateIN](0).template match {
        case "var/mypack/email/msg1.ftl:xx1" => Some("templateA")
        case x => throw new Exception("Invalid template: "+x)
      }
    }

    m.task.process(msg)

    verify(m.emailService).fromSupport("foo@example.org", "templateA", "xx1")
  }

  it should "handle error reports" in {
    val abyss = new AbyssEntry { id = 4L; pack = ""; meta = "i_mtp=2;l_usr=1;s_red=n;s_sub=2;" }
    val msg = new MessageEntry(abyss, m.serviceContainer)

    when(m.miscService.freemarkerTemplate(any[FreemarkerTemplateIN])).thenAnswer {
      _.getArgument[FreemarkerTemplateIN](0).template match {
        case "var/email/msg2.ftl:xx1" => Some("templateA")
        case x => throw new Exception("Invalid template: "+x)
      }
    }

    m.task.process(msg)

    verify(m.emailService).reportError("templateA")
  }

}

@RunWith(classOf[JUnitRunner])
class ForwardErrorsTaskSpec extends FlatSpec with MockitoSugar with BeforeAndAfter with Matchers {
  var m: MyMock = _

  before {
    m = new MyMock
  }

  class MyMock extends com.coldcore.haala.service.test.MyMock {
    val task = new ForwardErrorsTask {
      serviceContainer = m.serviceContainer
      freemarkerDirectives = mock[FreemarkerDirectives]
    }

    // freemarker setup
    when(settingService.getDefaultSite).thenReturn("xx1")
    when(task.freemarkerDirectives.getAsMap).thenReturn(Map.empty[String,Map[String,AnyRef]])
    when(domainService.getByType(Domain.TYPE_CONTROL_PANEL)).thenReturn(None)
  }

  it should "fail if template does not exist" in {
    when(m.miscService.freemarkerTemplate(any[FreemarkerTemplateIN])).thenReturn(None)

    a [CoreException] should be thrownBy m.task.process(Nil)
  }

  it should "convert errors to message" in {
    when(m.miscService.freemarkerTemplate(any[FreemarkerTemplateIN])).thenAnswer {
      _.getArgument[FreemarkerTemplateIN](0).template match {
        case "var/email/error.ftl:xx1" => Some("templateA")
        case x => throw new Exception("Invalid template: "+x)
      }
    }

    m.task.process("FOO" :: "BAR" :: Nil)

    verify(m.messageService).error(ErrorIN("templateA"))
  }
}
