package com.coldcore.haala
package task

import core.NamedLog
import org.junit.runner.RunWith
import org.scalatest.{BeforeAndAfter, FlatSpec, Matchers}
import org.scalatest.mockito.MockitoSugar
import org.mockito.Mockito._
import org.scalatest.junit.JUnitRunner

@RunWith(classOf[JUnitRunner])
class BaseTaskSpec extends FlatSpec with MockitoSugar with BeforeAndAfter with Matchers {
  var m: MyMock = _

  before {
    m = new MyMock
  }

  class MyMock extends com.coldcore.haala.service.test.MyMock {
    var internalRunCount = 0
    val task = new BaseTask {
      val name = "BummyTask"
      finished = System.currentTimeMillis-1000L*60L*60L //finished 1h ago
      def internalRun: Result =  { internalRunCount += 1; Result() }
      def setBusySince(x: Long) = busySince = x
      def setFinished(x: Long) = finished = x

      override val staticBlock = m.staticBlock
      serviceContainer = m.serviceContainer

      //override lazy val LOG = new NamedLog(name, m.consoleLog) // enable to see output
    }
  }

  it should "run default" in {
    when(m.settingService.querySystem("BummyTask")).thenReturn("sleep=1")
    when(m.settingService.querySystem("SuspendFlag")).thenReturn("0")
    m.task.run
    assertResult(1) { m.internalRunCount }
  }

  it should "run inactive" in {
    when(m.settingService.querySystem("BummyTask")).thenReturn("sleep=0")
    when(m.settingService.querySystem("SuspendFlag")).thenReturn("0")
    m.task.run
    assertResult(0) { m.internalRunCount }
  }

  it should "run suspended" in {
    when(m.settingService.querySystem("BummyTask")).thenReturn("sleep=1")
    when(m.settingService.querySystem("SuspendFlag")).thenReturn("1")
    m.task.run
    assertResult(0) { m.internalRunCount }
  }

  it should "run busy" in {
    when(m.settingService.querySystem("BummyTask")).thenReturn("sleep=1")
    when(m.settingService.querySystem("SuspendFlag")).thenReturn("0")
    m.task.setBusySince(System.currentTimeMillis-1000L)
    m.task.run
    assertResult(0) { m.internalRunCount }
    m.task.setBusySince(0)
    m.task.run
    assertResult(1) { m.internalRunCount }
  }

  it should "run sleep" in {
    when(m.settingService.querySystem("BummyTask")).thenReturn("sleep=1")
    when(m.settingService.querySystem("SuspendFlag")).thenReturn("0")
    m.task.run
    assertResult(1) { m.internalRunCount }
    m.task.run
    assertResult(1) { m.internalRunCount }
    m.task.setFinished(System.currentTimeMillis-1000L*60L-1000L)
    m.task.run
    assertResult(2) { m.internalRunCount }
    m.task.setFinished(System.currentTimeMillis-1000L*60L-1000L)
    m.task.run
    assertResult(3) { m.internalRunCount }
    m.task.run
    assertResult(3) { m.internalRunCount }
  }

}
