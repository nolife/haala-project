package com.coldcore.haala
package task

import service.HeartbeatInfo
import core.{Meta, Timestamp}
import core.SiteChain.Implicits._
import org.scalatest.mockito.MockitoSugar
import org.mockito.Mockito._
import org.mockito.ArgumentMatchers.{eq => meq, _}
import org.mockito.ArgumentCaptor
import org.scalatest.{BeforeAndAfter, WordSpec}
import org.junit.runner.RunWith
import org.scalatest.junit.JUnitRunner

@RunWith(classOf[JUnitRunner])
class NodeHeartbeatSpec extends WordSpec with MockitoSugar with BeforeAndAfter {
  var m: MyMock = _

  before {
    m = new MyMock
  }

  class MyMock extends com.coldcore.haala.service.test.MyMock {
    val task = new NodeHeartbeatTask {
      serviceContainer = m.serviceContainer
      heartbeatInfo = new HeartbeatInfo { override val staticBlock = m.staticBlock }
    }
    when(staticBlock.localhostName).thenReturn(host)
    when(miscService.databaseTime).thenReturn(Timestamp())
  }

  val host = "hydra.example.org"
  val time = Timestamp()

  "task" should {
    "increase heartbeat and set status" in {
      m.task.internalRun
      val valArg = ArgumentCaptor.forClass(classOf[String])
      verify(m.settingService).write(meq(s"Heartbeat/$host"), valArg.capture, meq("sys"))
      assertResult("slave") { Meta(valArg.getValue)("status") }
      assertResult(true) { Meta(valArg.getValue)("beat").toLong > Timestamp().addSeconds(-1)}
    }

    "increase heartbeat and resurrect from dead" in {
      when(m.settingService.querySystem(s"Heartbeat/$host")).thenReturn(s"status=dead; beat=$time")
      m.task.internalRun
      val valArg = ArgumentCaptor.forClass(classOf[String])
      verify(m.settingService).write(meq(s"Heartbeat/$host"), valArg.capture, meq("sys"))
      assertResult("slave") { Meta(valArg.getValue)("status") }
      assertResult(true) { Meta(valArg.getValue)("beat").toLong > Timestamp().addSeconds(-1)}
    }
  }

}

@RunWith(classOf[JUnitRunner])
class NodeMaintenanceSpec extends WordSpec with MockitoSugar with BeforeAndAfter {
  var m: MyMock = _

  before {
    m = new MyMock
  }

  class MyMock extends com.coldcore.haala.service.test.MyMock {
    val task = new NodeMaintenanceTask {
      serviceContainer = m.serviceContainer
      heartbeatInfo = new HeartbeatInfo { override val staticBlock = m.staticBlock }
      heartbeatData = new HeartbeatData
    }
    when(settingService.querySystem("NodeMaintenanceTask")).thenReturn("sleep=1, dead=5m")
    when(staticBlock.localhostName).thenReturn(host)
  }

  val host = "hydra.example.org"
  val time = Timestamp()

  "task" should {
    "mark masters as slaves if there more than one master" in {
      when(m.settingService.querySystem(s"Heartbeat/$host")).thenReturn(s"status=slave; beat=$time")
      when(m.settingService.getForSiteLike("Heartbeat/%", ".sys")).thenReturn(
        Map(
          s"Heartbeat/$host" -> s"status=master; beat=$time; voted=y", //master (voted flag to eliminate voting phase)
          "Heartbeat/192.168.xxx.2" -> s"status=master; beat=$time", //master
          "Heartbeat/192.168.xxx.3" -> s"status=slave; beat=$time",
          "Heartbeat/192.168.xxx.4" -> s"status=idle; beat=$time"))
      m.task.internalRun
      val keyArg = ArgumentCaptor.forClass(classOf[String])
      val valArg = ArgumentCaptor.forClass(classOf[String])
      verify(m.settingService, times(2)).write(keyArg.capture, valArg.capture, meq("sys"))
      assertResult(Set(s"Heartbeat/$host", "Heartbeat/192.168.xxx.2")) {
        Set(keyArg.getAllValues.get(0), keyArg.getAllValues.get(1)) }
      assertResult("slave") { Meta(valArg.getAllValues.get(0))("status") }
      assertResult("slave") { Meta(valArg.getAllValues.get(1))("status") }
    }

    "mark dead nodes" in {
      when(m.settingService.querySystem(s"Heartbeat/$host")).thenReturn(s"status=slave; beat=$time")
      when(m.settingService.getForSiteLike("Heartbeat/%", ".sys")).thenReturn(
        Map(
          s"Heartbeat/$host" -> s"status=master; beat=${time-1000L}", //dead
          "Heartbeat/192.168.xxx.2" -> s"status=slave; beat=${time-1000L}", //dead
          "Heartbeat/192.168.xxx.3" -> s"status=slave; beat=${time-1000L}", //dead
          "Heartbeat/192.168.xxx.4" -> s"status=idle; beat=$time"))
      when(m.settingService.querySystem("NodeMaintenanceTask")).thenReturn("sleep=1, dead=0") //instant dead
      m.task.internalRun
      val keyArg = ArgumentCaptor.forClass(classOf[String])
      val valArg = ArgumentCaptor.forClass(classOf[String])
      verify(m.settingService, times(3)).write(keyArg.capture, valArg.capture, meq("sys"))
      assertResult(Set(s"Heartbeat/$host", "Heartbeat/192.168.xxx.2", "Heartbeat/192.168.xxx.3")) {
        Set(keyArg.getAllValues.get(0), keyArg.getAllValues.get(1), keyArg.getAllValues.get(2)) }
      assertResult("dead") { Meta(valArg.getAllValues.get(0))("status") }
      assertResult("dead") { Meta(valArg.getAllValues.get(1))("status") }
      assertResult("dead") { Meta(valArg.getAllValues.get(2))("status") }
    }

    "vote for new master" in {
      when(m.settingService.querySystem(s"Heartbeat/$host")).thenReturn(s"status=slave; beat=$time; votes=5")
      when(m.settingService.getForSiteLike("Heartbeat/%", ".sys")).thenReturn(
        Map(
          s"Heartbeat/$host" -> s"status=slave; beat=$time; votes=5",
          "Heartbeat/192.168.xxx.2" -> s"status=slave; beat=$time; votes=5",
          "Heartbeat/192.168.xxx.3" -> s"status=slave; beat=$time; votes=5",
          "Heartbeat/192.168.xxx.4" -> s"status=idle; beat=$time"))
      m.task.internalRun
      val keyArg = ArgumentCaptor.forClass(classOf[String]) // (0) vote for master, (1) set flag on itself
      val valArg = ArgumentCaptor.forClass(classOf[String]) // ...
      verify(m.settingService, times(2)).write(keyArg.capture, valArg.capture, meq("sys"))
      assertResult(true) {
        val xs = List(s"Heartbeat/$host", "Heartbeat/192.168.xxx.2", "Heartbeat/192.168.xxx.3")
        xs.contains(keyArg.getAllValues.get(0))
      }
      assertResult(s"Heartbeat/$host") { keyArg.getAllValues.get(1) }
      assertResult(6) { Meta(valArg.getAllValues.get(0))("votes").toInt }
      assertResult("y") { Meta(valArg.getAllValues.get(1))("voted") }
    }

    "set itself as master if winner" in {
      when(m.settingService.querySystem(s"Heartbeat/$host")).thenReturn(s"status=slave; beat=$time; votes=10; voted=y")
      when(m.settingService.getForSiteLike("Heartbeat/%", ".sys")).thenReturn(
        Map(
          s"Heartbeat/$host" -> s"status=slave; beat=$time; votes=10; voted=y", //winner
          "Heartbeat/192.168.xxx.2" -> s"status=slave; beat=$time; votes=5; voted=y",
          "Heartbeat/192.168.xxx.3" -> s"status=slave; beat=$time; votes=5; voted=y",
          "Heartbeat/192.168.xxx.4" -> s"status=idle; beat=$time; voted=y"))
      m.task.internalRun
      val keyArg = ArgumentCaptor.forClass(classOf[String]) // (0..3) clear votes, (4) set itself as master
      val valArg = ArgumentCaptor.forClass(classOf[String]) // ...
      verify(m.settingService, times(5)).write(keyArg.capture, valArg.capture, meq("sys"))

      List(s"Heartbeat/$host", "Heartbeat/192.168.xxx.2", "Heartbeat/192.168.xxx.3", "Heartbeat/192.168.xxx.4").foreach { host =>
        assertResult(true) { keyArg.getAllValues.exists(host==) }
        val meta = Meta(valArg.getAllValues.get(keyArg.getAllValues.indexOf(host)))
        assertResult(None) { meta.get("votes") }
        assertResult(None) { meta.get("voted") }
      }
      assertResult(s"Heartbeat/$host") { keyArg.getAllValues.get(4) }
      assertResult("master") { Meta(valArg.getAllValues.get(4))("status") }
    }

    "clear votes if more than one winner" in {
      when(m.settingService.querySystem(s"Heartbeat/$host")).thenReturn(s"status=slave; beat=$time; votes=10; voted=y")
      when(m.settingService.getForSiteLike("Heartbeat/%", ".sys")).thenReturn(
        Map(
          s"Heartbeat/$host" -> s"status=slave; beat=$time; votes=10; voted=y", //winner #1
          "Heartbeat/192.168.xxx.2" -> s"status=slave; beat=$time; votes=10; voted=y", //winner #2
          "Heartbeat/192.168.xxx.3" -> s"status=slave; beat=$time; votes=5; voted=y",
          "Heartbeat/192.168.xxx.4" -> s"status=idle; beat=$time; voted=y"))
      m.task.internalRun
      val keyArg = ArgumentCaptor.forClass(classOf[String]) // (0..3) clear votes
      val valArg = ArgumentCaptor.forClass(classOf[String]) // ...
      verify(m.settingService, times(4)).write(keyArg.capture, valArg.capture, meq("sys"))

      List(s"Heartbeat/$host", "Heartbeat/192.168.xxx.2", "Heartbeat/192.168.xxx.3", "Heartbeat/192.168.xxx.4").foreach { host =>
        assertResult(true) { keyArg.getAllValues.exists(host==) }
        val meta = Meta(valArg.getAllValues.get(keyArg.getAllValues.indexOf(host)))
        assertResult(None) { meta.get("votes") }
        assertResult(None) { meta.get("voted") }
      }
    }

    "wait for all nodes to vote" in {
      when(m.settingService.querySystem(s"Heartbeat/$host")).thenReturn(s"status=slave; beat=$time; votes=10; voted=y")
      when(m.settingService.getForSiteLike("Heartbeat/%",".sys")).thenReturn(
        Map(
          s"Heartbeat/$host" -> s"status=slave; beat=$time; votes=10; voted=y",
          "Heartbeat/192.168.xxx.2" -> s"status=slave; beat=$time; votes=5; voted=y",
          "Heartbeat/192.168.xxx.3" -> s"status=slave; beat=$time; votes=5; voted=y",
          "Heartbeat/192.168.xxx.4" -> s"status=idle; beat=$time")) //needs to vote
      m.task.internalRun
      val keyArg = ArgumentCaptor.forClass(classOf[String])
      val valArg = ArgumentCaptor.forClass(classOf[String])
      verify(m.settingService, times(0)).write(keyArg.capture, valArg.capture, meq("sys"))
    }

    "not set itself as master if not winner" in {
      when(m.settingService.querySystem(s"Heartbeat/$host")).thenReturn(s"status=slave; beat=$time; votes=10; voted=y")
      when(m.settingService.getForSiteLike("Heartbeat/%", ".sys")).thenReturn(
        Map(
          s"Heartbeat/$host" -> s"status=slave; beat=$time; votes=10; voted=y",
          "Heartbeat/192.168.xxx.2" -> s"status=slave; beat=$time; votes=11; voted=y", //winner
          "Heartbeat/192.168.xxx.3" -> s"status=slave; beat=$time; votes=5; voted=y",
          "Heartbeat/192.168.xxx.4" -> s"status=idle; beat=$time")) //need to vote
      m.task.internalRun
      val keyArg = ArgumentCaptor.forClass(classOf[String])
      val valArg = ArgumentCaptor.forClass(classOf[String])
      verify(m.settingService, times(0)).write(keyArg.capture, valArg.capture, meq("sys"))
    }
  }

}
