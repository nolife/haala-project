package com.coldcore.haala
package task

import com.coldcore.haala.service.PaypalHandler
import org.junit.runner.RunWith
import org.scalatest.{BeforeAndAfter, FlatSpec, Matchers}
import org.scalatest.mockito.MockitoSugar
import org.scalatest.junit.JUnitRunner
import org.mockito.Mockito._
import org.mockito.ArgumentMatchers._

@RunWith(classOf[JUnitRunner])
class PaymentEventTaskSpec extends FlatSpec with MockitoSugar with BeforeAndAfter with Matchers {
  var m: MyMock = _

  before {
    m = new MyMock
  }

  class MyMock extends com.coldcore.haala.service.test.MyMock {
    val task = new PaymentEventTask { serviceContainer = m.serviceContainer }
    val handler = mock[PaypalHandler]

    when(paymentService.handler("paypal")).thenReturn(handler)
  }

  it should "ignore if queue is empty" in {
    when(m.queueService.pop("PaymentEvent")).thenReturn(None)

    m.task.internalRun

    verify(m.paymentService, times(0)).handler(anyString)
  }

  it should "read event from queue and forward to handler" in {
    when(m.queueService.pop("PaymentEvent")).thenReturn(Some("handler=paypal;abyss=3"))

    m.task.internalRun

    verify(m.paymentService.handler("paypal")).onEvent(3)
  }

  it should "process one event per run" in {
    when(m.queueService.pop("PaymentEvent")).thenReturn(
      Some("handler=paypal;abyss=3"), Some("handler=paypal;abyss=4"), Some("handler=paypal;abyss=5"))

    m.task.internalRun

    verify(m.paymentService.handler("paypal")).onEvent(3)
    verify(m.paymentService.handler("paypal"), times(0)).onEvent(4)
    verify(m.paymentService.handler("paypal"), times(0)).onEvent(5)

    m.task.internalRun

    verify(m.paymentService.handler("paypal")).onEvent(3)
    verify(m.paymentService.handler("paypal")).onEvent(4)
    verify(m.paymentService.handler("paypal"), times(0)).onEvent(5)
  }

}
