package com.coldcore.haala
package task

import core.WeakCoreException
import core.Constants.MESSAGE_TYPE_ERROR
import service._
import service.exceptions.{EmailServerDownException, ServiceException}
import service.MessageService.{ErrorIN, MessageEntry}

import scala.collection.JavaConverters._

/** Email messages to recipients */
class ForwardMessagesTask extends BaseTask with FreemarkerSupport {
  override val name = "ForwardMessagesTask"

  def process(o: MessageEntry) {
    val text = freemarkerTemplate(
      "var/"+o.abyss.pack.on(o.abyss.pack+"/")+"email/msg"+o.typ+".ftl:"+defaultSite,
      Map("messageEntry" -> o))
    if (o.typ == MESSAGE_TYPE_ERROR) sc.get[EmailService].reportError(text) // error
    else if (o.user.ref == "*") sc.get[EmailService].toSupport(o.user.email, text, o.site) // to support
    else sc.get[EmailService].fromSupport(o.user.email, text, o.site) // support to user
  }

  override def internalRun: Result = {
    var counter = 0
    var warning = false
    val batch = loadConfig("items").toInt
    val ms = sc.get[MessageService]

    val exclude = (o: MessageEntry) => ms.forwarded(o.abyss.id) // exclude from future forwards
    val search = (max: Int) => ms.search(new MessageService.SearchInputX(0, max) { override val forward = Some(true) })

    //process # within a new session
    val total = search(0).total.toInt
    for (_ <- 0 to total by batch) sessionHandler.withSession {
      for (o <- search(batch).objects)
        try {
          process(o)
          exclude(o)
          ms.read(o.abyss.id)
          counter += 1
        } catch {
          case e: EmailServerDownException => //email server down, exit with warning
            LOG.error(s"Error forwarding (abyss #${o.abyss.id}): email server down")
            throw new WeakCoreException
          case e: ServiceException => //invalid recipient, exclude and ignore with warning
            warning = true
            exclude(o)
            LOG.error(s"Error forwarding (abyss #${o.abyss.id}): "+e.getClass.toString.split("\\.").last)
          case e: Throwable => //failed for other reason, exit with error
            LOG.error(s"Error forwarding (abyss #${o.abyss.id}): "+e.getClass.toString.split("\\.").last)
            throw e
        }
    }

    if (counter > 0) LOG.info("Forwarded "+counter+" messages in "+executingTime)
    Result(items = counter, warning = warning)
  }
}

/** Convert error reports to messages */
class ForwardErrorsTask extends BaseTask with FreemarkerSupport {
  override val name = "ForwardErrorsTask"

  override def goAhead: Boolean = super.goAheadLocal

  /** Create a report from provided list of errors. */
  def process(es: List[String]) {
    val text = freemarkerTemplate(
      "var/email/error.ftl:"+defaultSite,
      Map("errors" -> es.map(_.convert("html")).asJava))
    sc.get[MessageService].error(ErrorIN(text))
  }

  /** Collect errors from queue. */
  private def collectErrors: List[String] =
    Iterator.fill(50)(sc.get[QueueService].pop("SystemErrors")).flatten.map(_.asInstanceOf[String]).toList

  override def internalRun: Result = {
    val es = collectErrors
    if (es.nonEmpty) {
      process(es)
      LOG.info("Forwarded "+es.size+" errors in "+executingTime)
    }
    Result(items = es.size)
  }
}
