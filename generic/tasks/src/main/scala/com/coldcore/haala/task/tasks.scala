package com.coldcore.haala
package task

import beans.BeanProperty
import com.coldcore.misc.scala.StringOp._
import xml.{Node, XML}
import java.util.TimerTask
import directive.freemarker.FreemarkerDirectives
import domain._
import core._
import core.HttpUtil.{HttpCallFailed, HttpCallSuccessful}
import core.Timestamp
import core.GeneralUtil._
import session.SessionHandler
import service._
import service.exceptions.{EmailServerDownException, InvalidEmailException}
import service.MiscService.FreemarkerTemplateIN
import scala.collection.JavaConverters._

trait Dependencies extends service.Dependencies {
  @BeanProperty var sessionHandler: SessionHandler = _
  @BeanProperty var resourceLoaderHolder: ResourceLoaderHolder = _
}

/** Print task status into settings */
trait Status {
  self: BaseTask =>

  private val time = () => "time" -> Timestamp().toShortString
  private val busyMinutes = () => (System.currentTimeMillis-busySince)/60000L
  private val save = (meta: Meta) => // save only master node data to avoid overlaps
    if (masterNode) sc.get[SettingService].write(statusKey, meta.serialize, Constants.SITE_SYSTEM)

  /** Task stared */
  protected def statusStart = save(Meta("") + time() +
    ("runs" -> execCounter) +
    ("started" -> Timestamp.fromMills(busySince).toShortString))

  /** Task ended */
  protected def statusEnd(result: Result, error: Boolean) {
    val meta = Meta(setting(statusKey)) + time() +
      ("runs" -> execCounter) +
      ("finished" -> Timestamp.fromMills(finished).toShortString)
    meta + Meta(result.meta)
    meta ? (error, "error", "y")
    meta ? (result.warning, "warning", "y")
    meta ? (result.items > 0, "items", result.items.toString)
    save(meta)
  }

  /** Update task busy time (taking too long to complete) */
  protected def statusBusy = save((Meta(setting(statusKey)) + time()) ?
    (busyMinutes() > 0, "busy", busyMinutes()+"m" ))
}

/** Task run result:
  *   items   - total processed items
  *   warning - true if error or warning was logged
  *   meta    - extra information to store in task status
  */
case class Result(items: Int = 0, warning: Boolean = false, meta: String = "") {
  def +(r: Result): Result = Result(items+r.items, warning && r.warning, (Meta(meta)+Meta(r.meta)).serialize)
}

abstract class BaseTask extends TimerTask with Dependencies with MasterNode with SettingsReader with LabelsReader with Status {
  protected var warned: Long = _
  protected var busySince: Long = _
  protected var finished: Long = _
  protected var execCounter: Long = _

  val name: String // overwrite task name

  lazy protected val statusKey: String = name+"Status"
  lazy val LOG = new NamedLog(name, log)

  /** Check if a task can proceed with execution (configuration based). */
  def goAhead: Boolean = notSuspended && masterNode
  def goAheadLocal: Boolean = notSuspended //do not check for master node (task operates on local resources)
  def notSuspended: Boolean = setting("SuspendFlag").isFalse

  /** Load configuration for a task. */
  def loadConfig: Map[String,String] = parseCSVMap(setting(name).safe)

  /** Short form to get task timeout (mills to construct date). */
  def configuredTimeout(key: String = "wait"): Long = System.currentTimeMillis-configToMills(loadConfig(key))

  /** Try to acquire a lock to ensure that no other tasks interfere while the task is running.
   *  Inputs: 'flag' is a lock of the task, 'fail-on' is a list of other locks to test before aquiring the lock.
   */
  def acquireLock(flag: String, failOn: String*): Boolean = {
    settingService.write(flag, "1", Constants.SITE_SYSTEM) //prevent other locks from being set
    if (!failOn.exists(setting(_) == "1")) true
    else { releaseLock(flag); false } //failed to acquire a lock
  }

  /** Release a lock acquired by the 'acquire-lock' method. */
  def releaseLock(flag: String) = settingService.write(flag, "0", Constants.SITE_SYSTEM)

  /** The time it took to execute a task so far (seconds). */
  def executingTime: String = millsToSec(System.currentTimeMillis-busySince)+"s"

  def internalRun: Result

  /** Run a task. Call an internal run method if all conditions are met. */
  override def run =
    if (loadConfig.isEmpty) {
      // timer tick every 10 seconds, print warning every 5 minutes
      if (execCounter%30 == 0) LOG.warn("Not configured task")
      execCounter += 1
    } else if (busySince > 0) {
      //task is still busy, print warning every 5 minutes
      statusBusy
      val minutes = (System.currentTimeMillis-busySince)/60000L
      if (minutes > warned && minutes%5 == 0) LOG.warn("Task is busy for "+minutes+"m")
      warned = minutes
    } else {
      //task is idle - proceed
      warned = 0L
      busySince = System.currentTimeMillis //set busy flag to prevent concurrent execution
      if (finished == 0) execCounter = 0 // reset counter (if ticked when not configured)
      val finish = (result: Result, error: Boolean) => {
        finished = System.currentTimeMillis; execCounter += 1; statusEnd(result, error) }

      try {
        val sleep = configToMills(loadConfig("sleep")) //sleep time between executions, 0 - suspended
        if (sleep > 0 && System.currentTimeMillis > finished+sleep && goAhead) {
          statusStart
          val result = internalRun
          finish(result, false)
        }
      } catch {
        case e: WeakCoreException => //warning and ignore
          finish(Result(warning = true), false)
        case e: Throwable => //error and report
          finish(Result(), true)
          LOG.error("Error running task", e)
          sc.get[MiscService].reportSystemError("["+name+"] Error running task", e)
      } finally { busySince = 0L } //reset busy flag 
    }
}

trait FreemarkerSupport {
  self: { val sc: ServiceContainer } =>

  @BeanProperty var freemarkerDirectives: FreemarkerDirectives = _

  // similar to model of BaseFreemarkerLoadTag
  private def appendModel(model: Map[String, AnyRef]): Map[String, AnyRef] = {
    val system = Map( //system objects
      "serviceContainer" -> sc
    )

    val directives = //wired directives
      (Map.empty[String, AnyRef] /: freemarkerDirectives.getAsMap)((r,x) => r + (x._1 -> x._2.asJava))

    val attributes = Map( //other attributes
      "currentSite" -> sc.get[SettingService].getDefaultSite,
      "ControlPanelURL" -> sc.get[DomainService].getByType(Domain.TYPE_CONTROL_PANEL)
        .map(x => sc.get[SettingService].query("MainURL", sc.get[DomainService].defaultSite(x.domain))).getOrElse("?")
    )

    directives ++ Map("system" -> system.asJava) ++ attributes ++ model
  }

  def freemarkerTemplate(template: String, model: Map[String, AnyRef]): String =
    sc.get[MiscService].freemarkerTemplate(FreemarkerTemplateIN(template, appendModel(model)))
      .getOrElse(throw new CoreException("Template not found: " + template))
}

class CurrencyRatesTask extends BaseTask {
  override val name = "CurrencyRatesTask"

  /** Parse a RSS feed. Works with 'http://feeds.currencysource.com/xxx.xml' feeds formatted as
   *  1 GBP = USD (1.609393)   1 GBP = COP (3,380.365914)   1 GBP = GBP (1.000000)
   *  Return a string to save as a setting (eg 'USD=1.609393, COP=3380.365914').
   */
  private def parseRss_v1(xml: Node): String =
    (for (elem <- xml \ "channel" \ "item" \ "title") yield {
      //proceed on correct element only (1 GBP)
      val s =
        if (elem.text.split("=")(0).trim.startsWith("1 "+defaultCurrency)) {
          val pair = elem.text.split("=")(1).trim.split(" ") // COP (3,380.365914)
          val currency = pair(0).trim // COP
          val rate = pair(1).filterNot("(), ".contains(_)).mkString // (3,380.365914) -> 3380.365914
          if (rate != "") currency+"="+rate else "" // ... USD=1.609393, COP=3380.365914
        } else ""

      if (s != "") s else { LOG.warn("Skipping invalid element: "+elem.text); "" }
    }).filter(""!=).mkString(", ")

  override def internalRun: Result = {
    val allCur = setting("Currencies").parseCSV
    val xml = sb.httpGet(setting("CurrencyRatesRSS")) match {
      case call: HttpCallSuccessful => XML.loadString(call.body)
      case call: HttpCallFailed => throw call.error
    }
    val result = parseRss_v1(xml)
    val cRates = parseCSVMap(result)
    val counter = cRates.size

    //validate a result before saving (uppercased keys and default currency rate should always be 1)
    for ((k,v) <- cRates) if (k != k.toUpperCase || k == defaultCurrency && v.toDouble != 1d)
      throw new CoreException(s"Invalid currency: $k=$v")
    for (s <- allCur) cRates.getOrElse(s, throw new CoreException(s"No currency: $s"))

    if (counter > 0) {
      settingService.write("CurrencyRates", result, Constants.SITE_SYSTEM)
      LOG.info("Saved "+counter+" rates in "+executingTime)
    }
    Result(items = counter)
  }
}

/** On demand (by system) task to spam emails. */
class SpamEmailsTask extends BaseTask {
  override val name = "SpamEmailsTask"
  private val flagName = "SpamEmailsFlag"

  override def goAhead: Boolean = super.goAhead && setting(flagName).isTrue

  override def internalRun: Result = {
    val batch = loadConfig("items").toInt
    val config = sc.get[SpamService].getTaskConfig.orNull

    if (config != null) sessionHandler.withSession {
      val items = sc.get[SpamService].findIdleStorageItems(config.storage, batch)
      val (subject, text) = (label("spam.emailSubject", SiteChain.sys), label("spam.emailText", SiteChain.sys))

      if (items.nonEmpty && subject.safe != "" && text.safe != "") {
        //process batch of idle items
        for ((item, n) <- items.zipWithIndex)
          if (n == 0 || config.error == "") try { //ignore saved error in the beginning, it may be cleared
            sc.get[EmailService].email(config.from, item.value, subject, text)
            config.good = config.good+1
            sc.get[SpamService].updateStorageItemStatus(item.id, EmailStorageItem.STATUS_PROCESSED)
            config.error = "" //clear error
          } catch {
            case e: InvalidEmailException => //invalied email, mark and ignore
              config.bad = config.bad+1
              sc.get[SpamService].updateStorageItemStatus(item.id, EmailStorageItem.STATUS_ERROR)
            case e: EmailServerDownException => config.error = "Email server down" //pause spamming
          }
        //save updated statistics
        sc.get[SpamService].writeTaskConfig(config)
      } else {
        //no idle items left or not enough data
        releaseLock(flagName)
        LOG.info("Done spamming (task is now disabled)")
      }
    }

    Result()
  }

}

/** On demand task to add missing user packs. */
class AddMissingPacksTask extends BaseTask {
  override val name = "AddMissingPacksTask"
  private val flagName = "AddMissingPacksFlag"

  override def goAhead: Boolean =
    super.goAhead && setting(flagName).isTrue

  override def internalRun: Result = {
    var counter = 0

    val ids = sc.get[UserService].allIds
    LOG.info("Processing users (found "+ids.size+")")

    for (id <- ids) {
      sc.get[UserService].addMissingPacks(id)
      counter += 1
      if (counter%10 == 0) LOG.info("Processed so far "+counter+" users")
    }
    releaseLock(flagName)

    LOG.info("Processed "+counter+" users in "+executingTime+" (task is now disabled)")
    Result(items = counter)
  }
}

/** On demand task to update objects in batch. */
abstract class BaseUpdateTask[T <: BaseObject] extends BaseTask {
  val flagName: String
  val logCounterAt: Int = 100
  def fastSearch(in: SearchInput): SearchResult[T]
  def update(o: T): Unit

  override def goAhead: Boolean =
    super.goAhead && setting(flagName).isTrue

  override def internalRun: Result = {
    var (counter, logCounter) = (0, 0)
    val batch = parseCSVMap(setting("SystemParams"))("batch").toInt

    val total = fastSearch(SearchInput(0, 0)).total.toInt
    LOG.info("Updating objects (found "+total+")")

    //process # within a new session
    for (pg <- 0 to total by batch) sessionHandler.withSession {
      val objs = fastSearch(SearchInput(pg, batch, orderBy = "id")).objects
      for (x <- objs) update(x)
      counter += objs.size
      logCounter += objs.size
      if (counter < total && logCounter >= logCounterAt) {
        logCounter = 0
        LOG.info("Updated so far "+counter+" objects")
      }
    }
    releaseLock(flagName)

    LOG.info("Updated "+counter+" objects in "+executingTime+" (task is now disabled)")
    Result(items = counter)
  }
}

/** On demand task to refresh file size and meta. */
class RefreshFilesTask extends BaseUpdateTask[File] {
  override val name = "RefreshFilesTask"
  override val flagName = "RefreshFilesFlag"
  override val logCounterAt: Int = 1000
  override def fastSearch(in: SearchInput) = sc.get[FileService].searchFast(in)
  override def update(o: File) = sc.get[FileService].refresh(o.id)
}

/** Execute payment service for inbound event */
class PaymentEventTask extends BaseTask {
  override val name = "PaymentEventTask"

  override def internalRun: Result = {
    var counter = 0

    sc.get[QueueService].pop("PaymentEvent") match {
      case Some(meta: String) =>
        val (handler, abyssId) = (Meta(meta)("handler"), Meta(meta)("abyss"))
        try { sc.get[PaymentService].handler(handler).onEvent(abyssId.toLong) }
        catch { case e: Throwable => LOG.error("Error processing event #"+abyssId, e) }
      case _ =>
    }

    if (counter > 0) LOG.info("Processed event in "+executingTime)
    Result(items = counter)
  }
}
