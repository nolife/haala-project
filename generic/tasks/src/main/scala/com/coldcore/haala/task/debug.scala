package com.coldcore.haala
package task

import core.Constants
import scala.util.Random

/** DEBUG task to test EhCache replication via JMS across all nodes (horizontal scaling).
  * The task constantly reads setting value and prints it out, all nodes print the same value.
  * Eventually the setting value is changed by the Master Node task, the change is replicated to the other nodes,
  * and all nodes print the new value.
  * 1) EhCache Entity cache is enabled (updated on replication via JMS) (get entity by ID)
  * 2) EhCache QueryCache is enabled (updated on replication via JMS) and queries set as cache-able (get entity by property)
  * 3) internal DomainCache is enabled (updated in EhCache listener on remote events) (stores entities in memory)
  */
class DebugCacheReplicationTask extends BaseTask {
  override val name = "DebugCacheReplicationTask"  //sys setting value: sleep=1s, wait=30s

  private var lastWrote = System.currentTimeMillis
  private val settingKey = "Debug-001"

  override def goAhead: Boolean = super.goAheadLocal

  override def internalRun: Result = {
    if (masterNode && configuredTimeout() > lastWrote) {
      lastWrote = System.currentTimeMillis
      val x = Random.nextInt(100)
      settingService.write(settingKey, ""+x, Constants.SITE_SYSTEM)
      LOG.info(s"$settingKey wrote value: $x")
    }
    val value = setting(settingKey).safe
    //    val value = setting(100).get.value.safe
    LOG.info(s"$settingKey read value: $value")

    Result()
  }
}
