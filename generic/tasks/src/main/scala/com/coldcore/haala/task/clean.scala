package com.coldcore.haala
package task

import beans.BeanProperty
import com.coldcore.misc.scala.StringOp._
import com.coldcore.misc5.CFile
import service._
import core._
import core.Constants._
import java.io.{File => JFile}

class CleanTuringTask extends BaseTask {
  override val name = "CleanTuringTask"
  private var deleted: Int = _

  override def internalRun: Result = {
    var counter = 0
    val timeout = Timestamp.fromMills(configuredTimeout()) //timeout before deleting files
    for (o <- sc.get[FileService].getByFolder(VirtualPath("turing"), SiteChain.pub) if o.updated < timeout) {
      sc.get[FileService].delete(o.id)
      counter += 1
      deleted += 1
    }

    if (deleted >= 100) {
      LOG.info("Deleted "+deleted+" turing files")
      deleted = 0
    }

    Result(items = counter)
  }
}

class CleanTmpTask extends BaseTask {
  override val name = "CleanTmpTask"

  override def internalRun: Result = {
    var counter = 0
    val timeout = configuredTimeout() //timeout before deleting files
    val tmpDir = new JFile(setting("FsPath"), "tmp")
    for (f <- tmpDir.listFiles if f.lastModified < timeout) {
      new CFile(f).delete //delete a file or directory
      counter += 1
    }

    if (counter > 0) LOG.info("Deleted "+counter+" temporary files in "+executingTime)
    Result(items = counter)
  }
}

class CleanCacheTask extends BaseTask {
  override val name = "CleanCacheTask"
  private var removed: Long = _
  private var lastWiped: Long = _

  @BeanProperty var cache: JList[Cache[Any]] = _

  override def goAhead: Boolean = super.goAheadLocal


  override def internalRun: Result = {
    var counter = 0
    val timeout = configuredTimeout("wipe") //timeout before wiping cache
    for (c <- cache) {
      val n = if (timeout > lastWiped) c.clear else c.scheduledClear
      counter += n.toInt
      removed += n
    }
    if (timeout > lastWiped) lastWiped = System.currentTimeMillis

    if (removed >= 1000) {
      LOG.info("Removed "+removed+" cached keys")
      removed = 0
    }

    val meta = Meta("") ? (lastWiped > 0, "lastWiped", Timestamp.fromMills(lastWiped).toShortString)
    Result(items = counter, meta = meta.serialize)
  }
}

class CleanActiveCacheTask extends CleanCacheTask {
  override val name = "CleanActiveCacheTask"
}

/** Delete files from disk which do not have database counterpart.
  * Collected statistics: total amount of files and size of "data" directory.
  */
class CleanFilesTask extends BaseTask {
  override val name = "CleanFilesTask"

  /** Collect files statistics */
  class Collected(var count: Int = 0, var size: Long = 0, var maxId: Long = 0) {
    def add(f: JFile) {
      count += 1
      size += f.length
      val id = java.lang.Long.parseLong(f.getName.takeWhile('.' !=), 16)
      maxId = math.max(maxId, id)
    }

    def save {
      val dir = new JFile(setting("FsPath"), "data")
      val meta = Meta("") +
        ("timestamp", Timestamp()) +
        ("space", dir.getUsableSpace) +
        ("files", count) +
        ("size", size) +
        ("maxId", maxId)
      sc.get[SettingService].write("FsSummary", meta.serialize, SITE_SYSTEM)
    }
  }

  override def internalRun: Result = {
    var counter = 0
    val timeout = configuredTimeout() // timeout before deleting files
    val fs = sc.get[FileService]
    val collected = new Collected

    def detached(dir: JFile): Unit = {
      val files = new CFile(dir)
      for (f <- files.list(true)) {
        collected.add(f) // collect statistics
        if (f.lastModified < timeout && fs.getByBody(fs.body(f.getAbsolutePath)).isEmpty) { // detached file
          f.delete
          counter += 1
        }
      }
      files.list(false).foreach(detached)
    }

    detached(new JFile(setting("FsPath"), "data"))
    collected.save

    if (counter > 0) LOG.info("Deleted "+counter+" detached files in "+executingTime)
    Result(items = counter)
  }
}

class CleanFeedTask extends BaseTask {
  override val name = "CleanFeedTask"

  override def internalRun: Result = {
    var counter: Int = 0
    val timeout = configuredTimeout() //timeout before deleting files
    val feedDir = new JFile(setting("FsPath"), "feed")
    for (f <- feedDir.listFiles if f.lastModified < timeout) {
      new CFile(f).delete //delete a file or directory
      counter += 1
    }

    if (counter > 0) LOG.info("Deleted "+counter+" feed files in "+executingTime)
    Result(items = counter)
  }
}

class CleanUsersTask extends BaseTask {
  override val name = "CleanUsersTask"

  override def internalRun: Result = {
    var counter = 0
    val us = sc.get[UserService]
    val timeout = Timestamp.fromMills(configuredTimeout()) //timeout before deleting users

    //delete one by one within a new session
    val total = us.searchNotActivated(timeout, SearchInput(0, 0)).total.toInt
    for (_ <- 1 to total) sessionHandler.withSession {
      us.searchNotActivated(timeout, SearchInput(0, 1)).objects.foreach { o => us.delete(o.id); counter += 1 }
    }

    if (counter > 0) LOG.info("Deleted "+counter+" not activated users in "+executingTime)
    Result(items = counter)
  }
}

class CleanAbyssTask extends BaseTask {
  override val name = "CleanAbyssTask"

  override def internalRun: Result = {
    var counter = 0
    val as = sc.get[AbyssService]
    val batch = parseCSVMap(setting("SystemParams"))("batch").toInt

    //delete # within a new session
    val total = as.searchExpired(SearchInput(0, 0)).total.toInt
    for (_ <- 0 to total by batch) sessionHandler.withSession {
      as.searchExpired(SearchInput(0, batch)).objects.foreach { o => as.delete(o.id); counter += 1 }
    }

    if (counter > 0) LOG.info("Deleted "+counter+" abyss entries in "+executingTime)
    Result(items = counter)
  }
}
