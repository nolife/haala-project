package com.coldcore.haala
package task

import beans.BeanProperty
import xml.Node
import com.coldcore.misc.scala.StringOp._
import com.coldcore.misc5.CFile
import service._
import core._
import core.GeneralUtil._
import core.Constants.UTF8
import com.google.gson.Gson
import java.io.{File => JFile}

import net.lingala.zip4j.core.ZipFile
import org.hibernate.SessionFactory

object FeedTask {
  class Out {
    var counter = 0
    var broken: Boolean = _
    var msg: String = ""
    val delete = new collection.mutable.HashSet[JFile]
    def break(m: String) = if (!broken) { msg = m; broken = true }
  }

  trait Mod {
    val name: String
    def handles(file: JFile): Boolean
    def process(file: JFile): Out
  }
}

/** Process files with feed mods. */
class FeedTask extends BaseTask {
  import FeedTask._

  override val name = "FeedTask"
  protected var feedBusySince: Long = _

  var mods = List.empty[Mod]
  def setMods(x: JList[Mod]) = mods = x.toList

  /** Rename a file and write an error message. */
  private def errorFile(file: JFile, suffix: String, msg: String) {
    val apath = file.getAbsolutePath
    new JFile(apath+"."+suffix).delete //delete a renamed copy if it already exists
    file.renameTo(new JFile(apath+"."+suffix))
    if (msg != "") new CFile(new JFile(apath+"."+suffix+"-msg")).rewriteFile(msg.bytesUTF8)
  }

  /** The time it took to execute a file so far (seconds). */
  private def feedExecutingTime: String = millsToSec(System.currentTimeMillis-feedBusySince)+"s"

  private val notSpecialFile = (x: JFile) => !x.getName.startsWith(".") && !x.getName.startsWith("_") // _warning.txt .DS_Store
  private val notBrokenFile = (x: JFile) => !x.getName.endsWith(".broken") && !x.getName.endsWith(".broken-msg") // x-files.json.broken

  /** Recursively goes into non empty directories and executes "f" */
  private def inNonEmptyDirs(dir: JFile)(f: JFile => Unit): Unit =
    new CFile(dir).list(true).toList match { //list files
      case xs if xs.exists(notSpecialFile) => f(dir) //file found
      case _ => new CFile(dir).list(false).foreach(inNonEmptyDirs(_)(f))
    }

  /** Recursively goes into directories removing empty ones (including the root directory) */
  private def rmEmptyDirs(dir: JFile): Unit = {
    new CFile(dir).list(false).foreach(rmEmptyDirs)
    val hasFiles = dir.listFiles.exists(x => x.isFile && notSpecialFile(x))
    if (!hasFiles && !dir.listFiles.exists(x => x.isDirectory)) new CFile(dir).delete
  }

  /** Check is file has write permission by renaming it. */
  private def writePermission(file: JFile): Boolean = {
    val check = new JFile(file.getAbsolutePath+".check")
    if (file.renameTo(check)) check.renameTo(file) else false
  }

  override def internalRun: Result = {
    var counter = 0
    var warning = false
    val fsPath = setting("FsPath")

    inNonEmptyDirs(new JFile(fsPath, "feed")) { dir =>
      // LOG.info("Directory "+dir.getAbsolutePath) // display directory processing order
      for (xfile <- new CFile(dir).list(true))
        try {
          //execute feeding logic in a fresh session
          feedBusySince = System.currentTimeMillis //flag for statistics
          var out: Out = null
          sessionHandler.withSession {
            mods.find(_.handles(xfile)) match {
              case Some(mod) if !writePermission(xfile) =>
                warning = true
                LOG.warn("No write permission: "+xfile.getName)
              case Some(mod) =>
                LOG.info(s"${mod.name} mod handles file: "+xfile.getName)
                out = mod.process(xfile)
              case None =>
                if (notSpecialFile(xfile) && notBrokenFile(xfile)) {
                  warning = true
                  LOG.warn("No mod configured for file: "+xfile.getName)
                }
            }
          }

          if (out != null)
            if (out.broken) {
              //fail
              warning = true
              val msg = (out.msg != "") ? s" (${out.msg})" | ""
              LOG.error(s"Skipped broken file$msg: "+xfile.getName)
              errorFile(xfile, "broken", out.msg)
            } else {
              //success - clean up 'delete' files (ensure those reside in 'feed' path)
              out.delete += xfile
              for (f <- out.delete) {
                val fpath = f.getAbsolutePath.replace("\\", "/")
                if (f.exists && fpath.startsWith(fsPath+"/feed/")) new CFile(f).delete //delete a file or directory
              }
              counter += 1
              LOG.info(s"${xfile.getName} ${out.counter} items in $feedExecutingTime")
            }
        } catch {
          case e: Throwable =>
            warning = true
            LOG.error("Error processing file: "+xfile.getName, e)
            errorFile(xfile, "broken", printStackTrace(e))
        } finally { feedBusySince = 0L } //reset a busy flag
    }
    for (dir <- new CFile(new JFile(fsPath, "feed")).list(false)) rmEmptyDirs(dir)

    if (counter > 0) LOG.info("Processed "+counter+" files in "+executingTime)
    Result(items = counter, warning = warning)
  }

}

/** Process "[any]-labels.xml" to write labels. */
class LabelsFeedMod extends FeedTask.Mod with Dependencies {
  import FeedTask._

  override val name = "LabelsFeed"
  override def handles(file: JFile): Boolean = file.getName.endsWith("-labels.xml")
  private case class Data(key: String, text: String, site: String)

  private def parse_v1_00(xml: Node, r: Out)(f: Data => Any) =
    for (insertE <- xml \\ "insert"; itemE <- insertE \ "item") {
      val (site, key, text) = ((insertE \ "@site").text.trim, (itemE \ "@key").text.trim, trimm(itemE.text))
      if (site == "") r.break("Insert tag is missing required attributes")
      if (key == "") r.break("Item tag is missing required attributes")
      if (!r.broken) f(Data(key, text, site))
    }

  override def process(file: JFile): Out = {
    val xml = sb.loadXmlFile(file)
    val (r, ver) = (new Out, (xml \ "version").text)

    if (ver == "1.00") {
      parse_v1_00(xml, r) { _ => } //validate
      if (!r.broken) parse_v1_00(xml, r) { x => sc.get[LabelService].write(x.key, x.text, x.site); r.counter += 1 }
    } else r.break("Unknown file version")
    r
  }
}

/** Process "[any]-files.xml" or "[any]-files.json" to upload files. */
class FilesFeedMod extends FeedTask.Mod with Dependencies {
  import FeedTask._
  import FileService._

  override val name = "FilesFeed"

  override def handles(file: JFile): Boolean =
    file.getName.endsWith("-files.xml") || file.getName.endsWith("-files.json")

  class JsonVersion {
    @BeanProperty var version: String = _
  }

  class JsonV1 {
    @BeanProperty var version: String = _
    @BeanProperty var content: Array[Content] = _

    class Content {
      @BeanProperty var action: String = _
      @BeanProperty var baseDir: String = _
      @BeanProperty var baseFolder: String = _
      @BeanProperty var site: String = _
      @BeanProperty var items: Array[Item] = _
    }

    class Item {
      @BeanProperty var subDir: String = _
      @BeanProperty var subFolder: String = _
      @BeanProperty var files: Array[String] = _
    }
  }

  private def parseJson(parentDir: JFile, json: JsonV1, r: Out)(f: (String, String, String, JFile) => Any) =
    for (c <- json.content.safe) {
      val (baseDir, baseFolder, site, action) = (c.baseDir.safe, c.baseFolder.safe, c.site.safe, c.action.safe)
      if (baseDir == "" || baseFolder == "" || site == "" || action == "")
        r.break("Content is missing required attributes")

      action match {
        case "upload" =>
          for { item <- c.items.safe; filename <- item.files.safe } {
            val dir = (baseDir :: item.subDir.safe :: Nil).filter(""!=).mkString("/")
            val folder = (baseFolder :: item.subFolder.safe :: Nil).filter(""!=).mkString("/")
            val file = new JFile(parentDir, dir+"/"+filename)

            if (!sb.fileExists(file)) r.break(s"File not found: $dir/$filename")
            if (!r.broken) f(folder, filename, site, file)
          }
        case x => r.break("Unknown action: "+x)
      }
    }

  private def processJson(file: JFile): Out = {
    val (r, s) = (new Out, new String(sb.readFile(file), UTF8))

    new Gson().fromJson(s, classOf[JsonVersion]).version match {
      case "1" =>
        val json = new Gson().fromJson(s, classOf[JsonV1])
        parseJson(file.getParentFile, json, r) { (_, _, _, _) => } //validate

        if (!r.broken) parseJson(file.getParentFile, json, r) { (folder, filename, site, f) =>
          val b = sc.get[FileService].body(f.getAbsolutePath)
          sc.get[FileService].getByPath(VirtualPath(folder, filename), SiteChain.key(site)) match {
            case Some(ef) => sc.get[FileService].update(ef.id, IN(VirtualPath(folder, filename), site), b)
            case None => sc.get[FileService].create(IN(VirtualPath(folder, filename), site), b)
          }

          r.delete += f.getParentFile
          r.counter += 1
        }
      case x => r.break("Unsupported version: "+x)
    }
    r
  }

  private def parseXml_v1_00(xml: Node, r: Out)(f: (String,String,String,String) => Any) =
    for (uploadE <- xml \\ "upload"; fileE <- uploadE \ "file") {
      val (site, folder, dir, filename) =
        ((uploadE \ "@site").text.trim, (uploadE \ "@folder").text.trim, (uploadE \ "@dir").text.trim, fileE.text.trim)
      if (site == "" || folder == "" || dir == "") r.break("Upload tag is missing required attributes")
      if (filename == "") r.break("File tag is missing required attributes")
      if (!r.broken) f(folder, filename, site, dir)
    }

  //@deprecated //use JSON instead
  private def processXml(file: JFile): Out = {
    val xml = sb.loadXmlFile(file)
    val (r, ver) = (new Out, (xml \ "version").text)

    if (ver == "1.00") {
      parseXml_v1_00(xml, r) { (folder, filename, site, dir) => //validate
        val f = new JFile(file.getParentFile, dir+"/"+filename)
        if (!f.exists) r.break(s"File not found: $dir/$filename")
      }

      if (!r.broken) parseXml_v1_00(xml, r) { (folder, filename, site, dir) =>
        val f = new JFile(file.getParentFile, dir+"/"+filename)
        val b = sc.get[FileService].body(f.getAbsolutePath)
        sc.get[FileService].getByPath(VirtualPath(folder, filename), SiteChain.key(site)) match {
          case Some(ef) => sc.get[FileService].update(ef.id, IN(VirtualPath(folder, filename), site), b)
          case None => sc.get[FileService].create(IN(VirtualPath(folder, filename), site), b)
        }

        r.delete += f.getParentFile
        r.counter += 1
      }
    } else r.break("Unknown file version")
    r
  }

  override def process(file: JFile): Out =
    file.getName.endsWith("-files.json") ? processJson(file) | processXml(file)
}

/** Process "[any]-settings.json" to write settings. */
class SettingsFeedMod extends FeedTask.Mod with Dependencies with SettingsReader {
  import FeedTask._

  override val name = "SettingsFeed"

  override def handles(file: JFile): Boolean = file.getName.endsWith("-settings.json")

  class JsonVersion {
    @BeanProperty var version: String = _
  }

  class JsonV1 {
    @BeanProperty var version: String = _
    @BeanProperty var content: Array[Content] = _

    class Content {
      @BeanProperty var action: String = _
      @BeanProperty var baseDir: String = _
      @BeanProperty var site: String = _
      @BeanProperty var items: Array[Item] = _
    }

    class Item {
      @BeanProperty var key: String = _
      @BeanProperty var value: String = _
      @BeanProperty var file: String = _
    }
  }

  private def parseJson(parentDir: JFile, json: JsonV1, r: Out, noFake: Boolean)(f: Option[JFile] => Any) =
    for (c <- json.content.safe) {
      val (baseDir, site, action) = (c.baseDir.safe, c.site.safe, c.action.safe)
      if (baseDir == "" || site == "" || action == "")
        r.break("Content is missing required attributes")

      action match {
        case "write" => //write a new setting from "value" or "file"
          for { item <- c.items.safe } {
            val (key, value, filename) = (item.key.safe, item.value.safe, item.file.safe)
            val file = new JFile(parentDir, baseDir+"/"+filename)

            if (!filename.equals("") && !sb.fileExists(file))
              r.break(s"File not found: $baseDir/$filename")

            if (!r.broken && noFake)
              if (filename.isEmpty) {
                settingService.write(key, value, site)
                f(None)
              } else {
                settingService.write(key, new String(sb.readFile(file), UTF8), site)
                f(Some(file))
              }
          }
        case "merge-csv" => //merge a new setting with an existing one as CSV
          for { item <- c.items.safe } {
            val (key, value, filename) = (item.key.safe, item.value.safe, item.file.safe)

            if (!r.broken && noFake) {
              val meta = MetaCSV(setting(key, SiteChain.key(site)).safe, ",;")
              value.parseCSV.foreach { meta + }
              settingService.write(key, meta.serialize, site)
              f(None)
            }
          }
        case "merge-csvmap" => //merge a new setting with an existing one as CSV map
          for { item <- c.items.safe } {
            val (key, value, filename) = (item.key.safe, item.value.safe, item.file.safe)

            if (!r.broken && noFake) {
              val meta = Meta(setting(key, SiteChain.key(site)).safe, ",;")
              value.parseCSVMap.foreach { meta + }
              settingService.write(key, meta.serialize, site)
              f(None)
            }
          }
        case "merge-urlmapping" => //merge a new UrlMapping setting with an existing one
          for { item <- c.items.safe } {
            val (key, value, filename) = (item.key.safe, item.value.safe, item.file.safe)
            val file = new JFile(parentDir, baseDir+"/"+filename)

            if (!sb.fileExists(file))
              r.break(s"File (UrlMapping) not found: $baseDir/$filename")

            val newMapping = new Gson().fromJson(new String(sb.readFile(file), UTF8), classOf[UrlMapping])

            val urlMapping = Option(new Gson().fromJson(setting(key, SiteChain.key(site)).safe,
              classOf[UrlMapping])).orNull
            if (urlMapping == null) r.break("Setting (UrlMapping) not found: "+key)

            val clone = (x: UrlMapping#Mapping) => {
              val o = new urlMapping.Mapping
              o.pattern = x.pattern
              o.handler = x.handler
              o.params = x.params
              if (x.handlerConf != null) {
                val (hc, xhc) = (o.newHandlerConf, x.handlerConf)
                hc.index = xhc.index
                hc.mod = xhc.mod
                hc.facade = xhc.facade
                hc.pageName = xhc.pageName
                hc.downtime = xhc.downtime
                hc.options = xhc.options
              }
              o
            }

            if (!r.broken && noFake) {
              urlMapping.mapping = //get old mappings with non existent patterns and append those with new mappings
                (urlMapping.mapping.filter(o => !newMapping.mapping.exists(_.pattern == o.pattern)).toList :::
                  newMapping.mapping.map(clone).toList).toArray

              val json = new Gson().toJson(urlMapping, classOf[UrlMapping])
              settingService.write(key, json, site)
              f(Some(file))
            }
          }
        case x => r.break("Unknown action: "+x)
      }
    }

  override def process(file: JFile): Out = {
    val (r, s) = (new Out, new String(sb.readFile(file), UTF8))

    new Gson().fromJson(s, classOf[JsonVersion]).version match {
      case "1" =>
        val json = new Gson().fromJson(s, classOf[JsonV1])
        parseJson(file.getParentFile, json, r, noFake = false) { _ => } //validate

        if (!r.broken) parseJson(file.getParentFile, json, r, noFake = true) { f =>
          if (f.isDefined) r.delete += f.get.getParentFile
          r.counter += 1
        }
      case x => r.break("Unsupported version: "+x)
    }
    r
  }
}

/** Unzip for further processing and delete zip archive. */
class ZipFeedMod extends FeedTask.Mod with Dependencies {
  import FeedTask._

  override val name = "ZipFeed"
  override def handles(file: JFile): Boolean = file.getName.endsWith("-feed.zip")

  override def process(file: JFile): Out = {
    val r = new Out
    new ZipFile(file).extractAll(file.getParent)
    r.counter += 1
    r.delete += file
    r
  }
}

/** Apply simple SQL to database line by line (errors ignored). */
class SqlFeedMod extends FeedTask.Mod with Dependencies {
  import FeedTask._

  @BeanProperty var sessionFactory: SessionFactory = _

  override val name = "SqlFeed"
  override def handles(file: JFile): Boolean = file.getName.endsWith("-feed.sql")

  lazy val LOG = new NamedLog(name, log)

  private def sql(s: String) =
    sessionFactory.getCurrentSession.doWork(connection => {
      val stmt = connection.createStatement
      try {
        val i = stmt.executeUpdate(s)
        val msg = s"SQL $i rows: $s"
        if (i == 0) LOG.warn(msg) else LOG.info(msg)
      } catch { case t: Throwable => LOG.error(s"SQL error, ignoring: $s \n${t.getMessage}") }
      stmt.close
    })

  override def process(file: JFile): Out = {
    val (r, content) = (new Out, new String(sb.readFile(file), UTF8))
    content.split(";").foreach { line =>
      val s = line.split("\n").map(_.trim).filter("" !=).filterNot(_.startsWith("--")).mkString(" ") // inline
      if (s.nonEmpty) { sql(s); r.counter += 1 }
    }
    r.delete += file
    r
  }
}
