package com.coldcore.haala
package task

import com.coldcore.misc.scala.StringOp._
import core._
import domain._
import service._
import service.SolrService.{SubmitDoc, SolrDoc}

/** Optimize Solr index. */
class SolrOptimizeTask extends BaseTask {
  override val name = "SolrOptimizeTask"

  override def internalRun: Result = {
    sc.get[SolrService].optimize
    LOG.info("Solr optimized in "+executingTime)
    Result()
  }
}

/** On demand task to submit objects into Solr. */
abstract class BaseSolrSubmitTask[T <: BaseObject] extends BaseTask {
  val flagName: String
  def solrType: Int
  def fastSearch(in: SearchInput): SearchResult[T]
  def solrDoc(o: T): Any // List[Any] or SolrDoc

  override def goAhead: Boolean =
    super.goAhead && setting(flagName).isTrue 

  override def internalRun: Result = {
    var counter = 0
    val batch = parseCSVMap(setting("SystemParams"))("batch").toInt*1000 // x1000 for read only operations

    val total = fastSearch(SearchInput(0, 0)).total.toInt
    LOG.info("Submitting objects (found "+total+")")

    val ss = sc.get[SolrService]
    ss.delete("o_typ:"+solrType)

    //process # within a new session
    for (pg <- 0 to total by batch) sessionHandler.withSession {
      val objs = fastSearch(SearchInput(pg, batch, orderBy = "id")).objects
      for (x <- objs.grouped(100))
        x.map(solrDoc) match {
          case docs @ Seq(h, _*) if h.isInstanceOf[SolrDoc] => ss.submitNoCommitWT(docs.asInstanceOf[Seq[SolrDoc]]: _*)
          case docs @ Seq(h, _*) if h.isInstanceOf[List[_]] => ss.submitNoCommit(docs.asInstanceOf[Seq[SubmitDoc]]: _*)
          case _ => //ignore empty sequence or unknown class
        }
      ss.commit
      counter += objs.size
      if (counter < total) LOG.info("Submitted so far "+counter+" objects")
    }

    ss.optimize
    releaseLock(flagName)

    LOG.info("Submitted "+counter+" objects in "+executingTime+" (task is now disabled)")
    Result(items = counter)
  }
}

/** On demand task to submit all labels into Solr. */
class SolrSubmitLabelsTask extends BaseSolrSubmitTask[Label] {
  override val name = "SolrSubmitLabelsTask"
  override val flagName = "SolrSubmitLabelsFlag"
  override def solrType: Int = Constants.SOLR_TYPE_LABEL
  override def fastSearch(in: SearchInput) = labelService.searchFast(in)
  override def solrDoc(o: Label) = labelService.prepareSolrDoc(o)
}

/** On demand task to submit all files into Solr. */
class SolrSubmitFilesTask extends BaseSolrSubmitTask[File] {
  override val name = "SolrSubmitFilesTask"
  override val flagName = "SolrSubmitFilesFlag"
  override def solrType: Int = Constants.SOLR_TYPE_FILE
  override def fastSearch(in: SearchInput) = sc.get[FileService].searchFast(in)
  override def solrDoc(o: File) = sc.get[FileService].prepareSolrDoc(o)
}

/** On demand task to submit all abyss into Solr. */
class SolrSubmitAbyssTask extends BaseSolrSubmitTask[AbyssEntry] {
  override val name = "SolrSubmitAbyssTask"
  override val flagName = "SolrSubmitAbyssFlag"
  override def solrType: Int = Constants.SOLR_TYPE_ABYSS
  override def fastSearch(in: SearchInput) = sc.get[AbyssService].searchFast(in)
  override def solrDoc(o: AbyssEntry) = sc.get[AbyssService].prepareSolrDoc(o)
}

/** On demand task to submit all users into Solr. */
class SolrSubmitUsersTask extends BaseSolrSubmitTask[User] {
  override val name = "SolrSubmitUsersTask"
  override val flagName = "SolrSubmitUsersFlag"
  override def solrType: Int = Constants.SOLR_TYPE_USER
  override def fastSearch(in: SearchInput) = sc.get[UserService].searchFast(in)
  override def solrDoc(o: User) = sc.get[UserService].prepareSolrDoc(o)
}
