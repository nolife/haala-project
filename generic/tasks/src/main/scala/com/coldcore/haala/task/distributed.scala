package com.coldcore.haala
package task

import service.exceptions.EmailServerDownException
import service._
import beans.BeanProperty
import scala.util.Random
import core.GeneralUtil._
import core.NetworkUtil._
import core._

trait MasterNode {
  self: BaseTask =>

  @BeanProperty var heartbeatInfo: HeartbeatInfo = new HeartbeatInfo

  /** Test is node is master or distributed mode is off (then node is pseudo master) */
  def masterNode: Boolean = {
    val host = heartbeatInfo.nodeHost
    val master = Meta(setting(s"Heartbeat/$host").safe)("status", "") == "master"
    val distributed = sc.get[SettingService].scaled
    master || !distributed
  }
}

trait DistributedTask extends MasterNode {
  self: BaseTask =>

  def nodeHost: String = heartbeatInfo.nodeHost
  def nodeStatus: String = heartbeatMeta("status", "")

  /** Test if distributed mode enabled */
  def distributed: Boolean = sc.get[SettingService].scaled

  def heartbeatMeta: Meta = heartbeatMeta(nodeHost)
  def heartbeatMeta(host: String): Meta = Meta(setting(s"Heartbeat/$host").safe)

  def writeHeartbeatMeta(meta: Meta): Unit = writeHeartbeatMeta(nodeHost, meta)
  def writeHeartbeatMeta(host: String, meta: Meta): Unit =
    settingService.write(s"Heartbeat/$host", meta.serialize, Constants.SITE_SYSTEM)
}

/** Node heartbeat task.
  *  Increase node "beat" counter in database setting and preserve its status (master or slave).
  */
class NodeHeartbeatTask extends BaseTask with DistributedTask {
  override val name = "NodeHeartbeatTask"

  override def goAhead: Boolean =
    super.goAheadLocal && distributed

  override def internalRun: Result = {
    val meta = heartbeatMeta
    meta + ("beat", sc.get[MiscService].databaseTime) // use DB time (time across nodes may vary)
    val status = meta.get("status") match {
      case Some(x @ ("master" | "slave" | "idle")) => x //idle nodes do not do anything
      case _ => "slave" //any other status or no status
    }
    meta + ("status" -> status)
    writeHeartbeatMeta(meta)
    Result()
  }
}

/** Node maintenance task.
  *  e.g. mark non-responsive nodes as dead, elect new master etc.
  */
class NodeMaintenanceTask extends BaseTask with DistributedTask {
  override val name = "NodeMaintenanceTask"

  private var nodeTimeout: Long = _ //timeout to detect dead nodes in seconds (e.g. 5 min)
  @BeanProperty var heartbeatData: HeartbeatData = _

  type Node = HeartbeatData#Node
  private def nodes = heartbeatData.nodes

  override def goAhead: Boolean =
    super.goAheadLocal && distributed && List("master", "slave", "idle").contains(nodeStatus)

  /** filter functions **/
  private val status = (node: Node) => node.heartbeatMeta("status", "")
  private val master = (node: Node) => status(node) == "master"
  private val slave = (node: Node) => status(node) == "slave"
  private val idle = (node: Node) => status(node) == "idle"
  private val timeout = (node: Node) => node.alive > 0 && node.alive <= Timestamp.fromMills(System.currentTimeMillis-nodeTimeout)
  private val voted = (node: Node) => node.heartbeatMeta.get("voted").isDefined
  private val votes = (node: Node) => node.heartbeatMeta("votes", "0").toInt
  private val currentNode = () => nodes(nodeHost) //exists, checked in goAhead

  /** Node with any status: set all masters as slave if more than one master */
  private def dedupMasters {
    val mcount = nodes.values.count(master)
    for (node <- nodes.values if mcount > 1) { //if more than one master then set all as slave
      node.heartbeatMeta.get("status") match {
        case Some("master") => //mark only masters
          node.heartbeatMeta + ("status" -> "slave")
          writeHeartbeatMeta(node.host, node.heartbeatMeta)
        case _ =>
      }
    }
  }

  /** Node with any status: set non-responsive masters or slaves as dead */
  private def markDead =
    nodes.values.filter(timeout).foreach { node =>
      node.heartbeatMeta.get("status") match {
        case Some(x @ ("master" | "slave")) => //mark only masters or slaves
          node.heartbeatMeta + ("status" -> "dead")
          writeHeartbeatMeta(node.host, node.heartbeatMeta)
        case _ =>
      }
    }

  /** Master, slave or idle can vote for a new master (if there is no one already) */
  private def voteMaster {
    val (mcount, slaves) = (nodes.values.count(master), nodes.values.filter(slave))
    if (mcount == 0 && slaves.nonEmpty && //no master but slaves
      currentNode().heartbeatMeta.get("voted").isEmpty &&
      List("master", "slave", "idle").contains(currentNode().heartbeatMeta("status", ""))) { //this node is eligible to vote
      val slave = slaves.toList(Random.nextInt(slaves.size)) //vote for this slave
      slave.heartbeatMeta + ("votes", ""+(slave.heartbeatMeta("votes", "0").toInt+1)) //vote for
      writeHeartbeatMeta(slave.host, slave.heartbeatMeta)
      currentNode().heartbeatMeta + ("voted" -> "y") //voted flag on itself (could be the same as slave.meta if voted for itself)
      writeHeartbeatMeta(currentNode().heartbeatMeta)
    }
  }

  private def clearVotes =
    nodes.values.foreach { node =>
      node.heartbeatMeta - ("votes", "voted")
      writeHeartbeatMeta(node.host, node.heartbeatMeta)
    }

  /** If this node is a winner then set it as a master (if there is no one already),
   *  but node with any status may clear all votes if more than one winner.
   */
  private def setMaster =
    if (nodes.values.count(master) == 0 && nodes.values.filter(x => slave(x) || idle(x)).forall(voted)) { // everyone voted
      val slaves = nodes.values.filter(slave).toList
      if (slaves.isEmpty) clearVotes // // no candidates for new master - vote again
      else { // candidates for new master
        val winner = slaves.reduce((a,b) => if (votes(a) > votes(b)) a else b) // winner by the max votes
        val winners = nodes.values.count(votes(_) == votes(winner))
        if (votes(winner) == 0 || winners >= 2) clearVotes // vote again
        else if (votes(winner) > 0 && winner.host == nodeHost) { // this node is a new master by max votes
          clearVotes
          currentNode().heartbeatMeta + ("status" -> "master")
          writeHeartbeatMeta(currentNode().heartbeatMeta)
        }
      }
    }

  override def internalRun: Result = {
    nodeTimeout = configToMills(loadConfig("dead"))
    val heartbeats = settingService.getForSiteLike("Heartbeat/%", SiteChain.sys)
    heartbeats.foreach { case (k,heartbeat) => //collect and update all nodes
      val host = k.dropWhile('/' !=).drop(1) // Heartbeat/192.168.0.32/hydra --> 192.168.0.32/hydra
      val node = heartbeatData.add(host, heartbeat)
      node.update(heartbeat)
    }
    /* those methods need to operate of loaded values only and do not load fresh heartbeats */
    dedupMasters; markDead; voteMaster; setMaster
    Result()
  }
}

/** Store nodes heartbeat settings for the maintenance task */
class HeartbeatData {
  /** Class holds node info parsed from database setting */
  case class Node(host: String, var heartbeat: String) {
    var alive: Long = _ //local timestamp when node was last alive (beat changed)
    var heartbeatMeta = Meta(heartbeat) //heartbeat meta container
    var beat: Long = Meta(heartbeat)("beat", "0").toLong //beat is read-only so it extracted directly

    def update(heartbeat: String) {
      this.heartbeat = heartbeat
      heartbeatMeta = Meta(heartbeat)
      val beat = heartbeatMeta("beat", "0").toLong
      if (beat != this.beat || alive == 0) alive = Timestamp()
      this.beat = beat
    }
  }

  val nodes = new scala.collection.mutable.HashMap[String,Node]

  def add(host: String, heartbeat: String): Node =
    nodes.getOrElseUpdate(host, Node(host, heartbeat))
}

/** Save node summary (ip, hostname, mac etc) into settings. */
class NodeSummaryTask extends BaseTask with DistributedTask {
  override val name = "NodeSummaryTask"

  @BeanProperty var statistics: Statistics = _

  override def goAhead: Boolean =
    super.goAheadLocal && distributed

  private def writeSummaryMeta(meta: Meta) =
    settingService.write(s"NodeSummary/$nodeHost", meta.serialize, Constants.SITE_SYSTEM)

  override def internalRun: Result = {
    val runtime = Runtime.getRuntime
    val meta = Meta("") +
      ("hostname", localhostName) +
      ("ip", localhostAddress) +
      ("mac", localhostMac) +
      ("started", statistics.started) + // local start time
      ("timestamp", Timestamp()) + // local time
      ("dbtime", sc.get[MiscService].databaseTime) + // database time
      ("memory", runtime.freeMemory+","+runtime.totalMemory+","+runtime.maxMemory) + // free < total < max
      ("requests", statistics.requests.get)
    writeSummaryMeta(meta)
    Result()
  }
}

/** Test node functions (email sending) */
class NodeTestTask extends BaseTask with DistributedTask with FreemarkerSupport {  
  override val name = "NodeTestTask"

  override def goAhead: Boolean =
    super.goAheadLocal && testEmail

  private def key: String = s"NodeTest/$nodeHost"
  private def testEmail: Boolean = Meta(setting(key).safe)("email", "").nonEmpty

  /** Create a test message and send it by email.
    * Use to test node mail server and to troubleshoot routing issues.
    */
  def processEmail: Result = {
    try {
      val text = freemarkerTemplate("var/email/test.ftl:"+defaultSite, Map.empty)
      sc.get[EmailService].email(loadConfig("emailFrom"), loadConfig("emailTo"), "Test email "+Timestamp(), text)
    } catch {
      case e: EmailServerDownException => //email server down, exit with warning
        LOG.error("Error sending: email server down")
        throw new WeakCoreException
      case e: Throwable => //failed for other reason, exit with error
        throw e
    }
    Result(items = 1)
  }

  override def internalRun: Result =
    try {
      if (testEmail) processEmail else Result()
    } finally { // delete test setting
      val ss = sc.get[SettingService]
      ss.getByKeyAndSite(key, Constants.SITE_SYSTEM).foreach(x => ss.delete(x.id))
      LOG.info("Node test complete in "+executingTime)
    }
}
