package com.coldcore.haala
package directive.freemarker

import java.text.SimpleDateFormat
import com.coldcore.misc.scala.StringOp._
import BaseDirective._
import core.Timestamp
import service.PaymentService.PaymentEntry
import service.PaypalHandler.PaymentConfig
import service._
import com.google.gson.Gson

trait GetPaymentEntry {
  self: { val sc: ServiceContainer } =>

  def getPaymentEntry(state: State): Option[PaymentEntry] = {
    import state._
    val pval = (x: String) => paramAsString(x).getOrElse("")
    val (id, ref) = (pval("entryId"), pval("ref"))

    paramAsObject[PaymentEntry]("entry").orElse { //entry object as param
      if (id.nonEmpty) sc.get[PaymentService].getById(id.toLong) //by ID
      if (ref.nonEmpty) sc.get[PaymentService].searchByRef(ref) //by REF
      else None
    }
  }
}

class PaymentEntryDirective extends BaseDirective with GetPaymentEntry {
  override def execute(state: State) =
    getPaymentEntry(state).foreach(entry => outputTM(state, state.wrap(entry)))
}

class PaymentEntryPropertyDirective extends BaseDirective with StateWithVars with GetPaymentEntry {

  class TextProcessor(state: StateWithVars, entry: PaymentEntry) extends Templated(state) {
    import state._
    val value = label("a."+entry.abyss.id.hex+".text", site)
    templatedValue(value, output(state, _: String))
  }

  class TimestampProcessor(ts: Long, state: StateWithVars, entry: PaymentEntry) extends Templated(state) {
    val sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm")
    templatedValue(sdf.format(Timestamp(ts).toDate), output(state, _: String))
  }

  class PriceProcessor(state: StateWithVars, entry: PaymentEntry) extends Templated(state) {
    case class Tuple(currency: String, sign: String, amount: String)
    val tuple = Tuple(entry.currency, sc.get[CurrencyService].getSign(entry.currency), toX100(entry.amount))
    val convert = (t: Tuple) => Seq("currency:"+t.currency, "sign:"+t.sign, "amount:"+t.amount)
    templatedTuple[Tuple](Some(tuple), output(state, _), _.amount, convert)
  }

  override def execute(state: StateWithVars) = getPaymentEntry(state).foreach { entry =>
    import state._
    pval("property") match {
      case "price" => new PriceProcessor(state, entry)
      case "created" => new TimestampProcessor(entry.abyss.created, state, entry)
      case "updated" => new TimestampProcessor(entry.abyss.updated, state, entry)
      case "keep" => new TimestampProcessor(entry.abyss.keep, state, entry)
      case "expire" => new TimestampProcessor(entry.expire, state, entry)
      case _ => new TextProcessor(state, entry)
    }
  }
}

class PaymentPaypalConfigDirective extends BaseDirective with StateWithVars {

  private def paypalConfig: PaypalHandler.PaymentConfig#Paypal =
    new Gson().fromJson(sc.get[SettingService].querySystem("PaymentConf"), classOf[PaymentConfig]).paypal

  class ClientProcessor(state: StateWithVars) extends Templated(state) {
    val sandbox = paypalConfig.env == "sandbox"
    val v = if (sandbox) paypalConfig.sandbox_client else paypalConfig.production_client
    templatedValue(v, output(state, _: String))
  }

  class EnvProcessor(state: StateWithVars) extends Templated(state) {
    templatedValue(paypalConfig.env, output(state, _: String))
  }

  class EnabledProcessor(state: StateWithVars) extends Templated(state) {
    val v = if (paypalConfig.enabled.isTrue) "y" else "n"
    templatedValue(v, output(state, _: String))
  }

  override def execute(state: StateWithVars) {
    import state._
    pval("property") match {
      case "client" => new ClientProcessor(state)
      case "env" => new EnvProcessor(state)
      case "enabled" => new EnabledProcessor(state)
    }
  }
}
