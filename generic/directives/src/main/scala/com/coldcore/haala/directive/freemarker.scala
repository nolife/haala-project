package com.coldcore.haala
package directive.freemarker

import freemarker.template._
import freemarker.core.Environment
import scala.collection.JavaConverters._
import scala.beans.BeanProperty
import scala.reflect.ClassTag
import domain._
import service._
import com.coldcore.misc.scala.StringOp._
import com.coldcore.misc5.CByte._
import java.io.ByteArrayOutputStream
import core._
import core.Constants.UTF8
import core.GeneralUtil.generateRef
import BaseDirective._

class FreemarkerDirectives {
  private var directives = Map.empty[String,List[AnyRef]] //namespace / list of directives
  def setDirectives(x: JMap[String,JList[AnyRef]]) = directives = x.map { case (k,v) => k -> v.toList }.toMap

  /** Map (namespace / X) where X is map (directive name / directive object) */
  def getAsMap: Map[String,Map[String,AnyRef]] =
    directives.map { case (k,v) => k -> v.map(x => x.asInstanceOf[BaseDirective].name -> x).toMap }
}

object State {
  def apply(env: Environment, parameters: JMap[_,_], loopVars: Array[TemplateModel], body: TemplateDirectiveBody) =
    new State(env, parameters, loopVars, body)
}

class State(val env: Environment, val parameters: JMap[_,_], val loopVars: Array[TemplateModel], val body: TemplateDirectiveBody) {
  val params = parameters.asInstanceOf[JMap[AnyRef,AnyRef]].toMap
  val objectWrapper = env.getObjectWrapper.asInstanceOf[DefaultObjectWrapper]

  def print(s: String) = env.getOut.write(s)

  def renderBody() = body.render(env.getOut)

  def wrap(o: AnyRef): TemplateModel = objectWrapper.wrap(o)
  def unwrap[T](o: TemplateModel)(implicit m: ClassTag[T]): T = objectWrapper.unwrap(o, m.runtimeClass).asInstanceOf[T]

  def paramAsString(x: String): Option[String] =
    paramAsObject[TemplateScalarModel](x).map(_.getAsString)

  def paramAsNumber(x: String): Option[Long] =
    paramAsObject[TemplateNumberModel](x).map(_.getAsNumber.longValue)

  def paramAsObject[T](x: String)(implicit m: ClassTag[T]): Option[T] =
    params.get(x).map(a => m.runtimeClass.isInstance(a) ? a.asInstanceOf[T] | unwrap[T](a.asInstanceOf[TemplateModel]))

  def varAsString(x: String): Option[String] =
    varAsObject[TemplateScalarModel](x).map(_.getAsString)

  def varAsNumber(x: String): Option[Long] =
    varAsObject[TemplateNumberModel](x).map(_.getAsNumber.longValue)

  def varAsObject[T](x: String)(implicit m: ClassTag[T]): Option[T] =
    Option(env.getVariable(x)).map(a => m.runtimeClass.isInstance(a) ? a.asInstanceOf[T] | unwrap[T](a))

  def systemVarAsString(x: String): Option[String] =
    systemVarAsObject[TemplateScalarModel](x).map(_.getAsString)

  def systemVarAsNumber(x: String): Option[Long] =
    systemVarAsObject[TemplateNumberModel](x).map(_.getAsNumber.longValue)

  def systemVarAsObject[T](x: String)(implicit m: ClassTag[T]): Option[T] =
    varAsObject[TemplateHashModel]("system").map(hash =>
      Option(hash.get(x)).map(a => m.runtimeClass.isInstance(a) ? a.asInstanceOf[T] | unwrap[T](a))).getOrElse(None)

  /** Return service container. */
  def serviceContainer: ServiceContainer = systemVarAsObject[ServiceContainer]("serviceContainer").get

  /** Return current site. */
  def resolveSite: SiteChain = SiteChain(paramAsString("site") orElse varAsString("currentSite") getOrElse "")
}

object BaseDirective {
  trait TMMaker[-A] {
    def make(a: A): TemplateModel
  }

  implicit val StringTMMaker: TMMaker[String] = new TMMaker[String] {
    override def make(v: String) = new SimpleScalar(v)
  }

  implicit val LongTMMaker: TMMaker[Long] = new TMMaker[Long] {
    override def make(v: Long) = new SimpleNumber(v)
  }

  implicit val IntTMMaker: TMMaker[Int] = new TMMaker[Int] {
    override def make(v: Int) = new SimpleNumber(v)
  }

  implicit val BooleanTMMaker: TMMaker[Boolean] = new TMMaker[Boolean] {
    override def make(v: Boolean) = new TemplateBooleanModel {
      override def getAsBoolean: Boolean = v
      override def toString: String = v.toString
    }
  }

}

abstract class BaseDirective extends TemplateDirectiveModel with Dependencies with SettingsReader with LabelsReader with UserRoleCheck {
  @BeanProperty var name: String = _

  /** Either set as variable or print to a page */
  def outputTM(state: State, v: TemplateModel) {
    import state._
    paramAsString("var") match {
      case Some(x) =>
        paramAsString("scope").orNull match {
          case "global" => env.setGlobalVariable(x, v)
          case "local" => env.setLocalVariable(x, v)
          case _ => env.setVariable(x, v)
        }
      case None => if (v.toString != null) print(v.toString)
    }
  }

  def output[A : TMMaker](state: State, v: A) {
    val m = implicitly[TMMaker[A]]
    outputTM(state, m.make(v))
  }

  def render[A](state: State, xs: Seq[A], f: A => TemplateModel = (x: A) => new SimpleScalar(x.toString)) =
    xs.zipWithIndex.foreach { case (x,n) =>
      import state._
      loopVars.update(0, f(x))
      if (loopVars.length > 1) loopVars.update(1, new SimpleNumber(n))
      renderBody()
    }

  override def execute(env: Environment, parameters: JMap[_,_], loopVars: Array[TemplateModel], body: TemplateDirectiveBody) =
    execute(State(env, parameters, loopVars, body))

  def execute(state: State)

  implicit val SeqTMMaker: TMMaker[Seq[_]] = new TMMaker[Seq[_]] {
    override def make(v: Seq[_]) = new SimpleSequence(v.asJava, freemarkerEngine.objectWrapper)
  }

}

/** Trait which provides common variables and functions on State */
trait StateWithVars {
  self: BaseDirective =>

  object StateWithVars {
    def apply(state: State) = new StateWithVars(state)
  }

  class StateWithVars(state: State) extends State(state.env, state.parameters, state.loopVars, state.body) {
    val pval = (x: String) => paramAsString(x).getOrElse("")
    val pnum = (x: String) => paramAsNumber(x).getOrElse(0L)
    val vstr = (x: String) => varAsString(x).getOrElse("")
    val vnum = (x: String) => varAsNumber(x).getOrElse(0L)
    val site = resolveSite
  }

  override def execute(state: State): Unit =
    execute(StateWithVars(state))

  def execute(state: StateWithVars)
}

/** Find a template and output values (text, options, converted tuples) into it.
  * If no template is found then output depends on a value.
  * Template may be searched by:
  *   a) in case of an option by its name or value key: "+" --> "productPcOpt.v201"   todo ("+a"  results in "productPcOpt.v201a")
  *      or by its value key with the value appended: "++" --> "productPcOpt.v112.3"  todo ("++a" results in "productPcOpt.v112a.3")
  *   b) label key: "productPcOpt.v201"
  *   c) template: "My name is {val}"
  */
class Templated(state: State) extends LabelsReader {
  import state._
  private val pval = (x: String) => paramAsString(x).getOrElse("")
  private val site = resolveSite
  val sc = serviceContainer

  def prepareTemplate(op: Option[BaseOption] = None): String = {
    val (labelKeyArg, templateArg) = (pval("label"), pval("template"))
    if (labelKeyArg == "+") op.map(x => label(x.getValueKey, site)).orNull
    else if (labelKeyArg == "++") op.map(x => label(x.getValueKey+"."+x.value, site)).orNull
    else if (labelKeyArg.nonEmpty) label(labelKeyArg, site)
    else if (templateArg.nonEmpty) templateArg
    else ""
  }

  def templatedValue(value: String, output: String => Unit) {
    val template = prepareTemplate()
    if (template.safe.isEmpty || value.safe.isEmpty) output(value)
    else output(parametrize(template, "val:"+value))
  }

  def templatedOption(value: Option[BaseOption], output: String => Unit) {
    val template = prepareTemplate(value)
    if (template.safe.isEmpty || value.isEmpty) output(value.map(_.value).orNull)
    else output(parametrize(template, "val:"+value.get.value))
  }

  def templatedOptions(values: Seq[BaseOption], output: Seq[String] => Unit) {
    val template = prepareTemplate(values.headOption)
    if (template.safe.isEmpty) output(values.map(value => value.value))
    else output(values.map(value => parametrize(template, "val:"+value.value)))
  }

  def templatedTuple[T](value: Option[T], output: String => Unit, simple: T => String, complex: T => Seq[String]) {
    val template = prepareTemplate()
    if (template.safe.isEmpty || value.isEmpty) output(value.map(simple(_)).orNull) // name
    else output(parametrize(template, complex(value.get): _*))
  }

  def templatedTuples[T](values: Seq[T], output: Seq[String] => Unit, simple: T => String, complex: T => Seq[String]) {
    val template = prepareTemplate()
    if (template.safe.isEmpty) output(values.map(simple))
    else output(values.map(value => parametrize(template, complex(value): _*)))
  }
}

class UrlDirective extends BaseDirective with StateWithVars {
  override def execute(state: StateWithVars) {
    import state._
    val (path, nc, salt) = (pval("path"), pval("nc"), pval("salt"))
    val (ver, siteK) = (vstr("resourceVersion"), vstr("mySite"))

    output(state, path match {
      case "" => ""
      case x =>
        s"/$x?site=$siteK&rv=$ver"+
          ((nc != "") ? "&nc=1" | "")+
          ((salt != "") ? s"&salt=${System.currentTimeMillis.hex}" | "")
    })
  }
}

class SaltDirective extends BaseDirective {
  override def execute(state: State) = output(state, System.currentTimeMillis.hex)
}

class DotDirective extends BaseDirective {
  override def execute(state: State) = state.print("/web/z/dot.gif")
}

class ParseCsvDirective extends BaseDirective with StateWithVars {
  override def execute(state: StateWithVars) {
    import state._
    val (value, sp) = (pval("value"), pval("sp") or ",;")

    pval("output") match {
      case "json" => output(state, value.parseCSV(sp).map("\""+_+"\"").mkString("[", ",", "]"))
      case "array" => output(state, value.parseCSV(sp))
      case _ => output(state, MetaCSV(value, ",").serialize) //normalise CSV
    }
  }
}

class SettingValueDirective extends BaseDirective with StateWithVars {
  override def execute(state: StateWithVars) {
    import state._
    val (sp, mapSp) = (pval("sp") or ",;", pval("mapSp") or ",;")
    val x = setting(pval("key"), site)
    val value = pval("mapKey").nonEmpty ? x.parseCSVMap(mapSp)(pval("mapKey")) | x

    pval("output") match {
      case "json" => output(state, value.parseCSV(sp).map("\""+_+"\"").mkString("[", ",", "]"))
      case "array" => output(state, value.parseCSV(sp))
      case _ => output(state, value)
    }
  }
}

/** Security tag checking if current user has required role. If the user is not in the role then
 *  tag body will be skipped. The tag operates on current user.
 *  Role:
 *      current - to test if a user is logged in
 *      super   - to test if a user is a super user under someone else's shell
 *      otherwise any role to test (eg. admin) with admin user qualifying for any role.
 */
class OnRoleDirective extends BaseDirective with StateWithVars {
  override def execute(state: StateWithVars) {
    import state._
    def hasRole(x: String) = x match {
      case "current" => vnum("currentUserId") > 0
      case "super" => vnum("superUserId") > 0
      case _ => isUserAdmin || isUserInRole(x)
    }

    val b = pval("role").parseCSV match {
      case Seq("-", x @ _*) => !x.exists(hasRole) //none of the roles
      case Seq("+", x @ _*) => x.count(hasRole) == x.size //all of the roles (simple check by count)
      case x => x.exists(hasRole) //any of the roles
    }

    if (b) renderBody()
  }
}

class LabelDirective extends BaseDirective with StateWithVars {
  override def execute(state: StateWithVars) {
    import state._
    val (key0, tokens) = (pval("key"), pval("tokens"))
    val (mode, suffix, pindex) = (pval("mode"), pval("suffix"), pval("pindex"))
    val tokenSp = (tokens+" ").head match { case ';' => ";"; case _ => "," } //first char may define a separator
    val bydefault = paramAsString("bydefault")

    val pageIndex = systemVarAsString("pageIndex").getOrElse("")
    val key = if (key0.contains(".") || pageIndex == "" || pindex == "0") key0 else pageIndex+"."+key0 // 001 -> 03022.001

    val value = mode match {
      case "seq" => label(key+suffix, site) nil label(key, site)
      case _ => label(key, site) nil bydefault.map(x => label(x, site) nil x).orNull
    }

    output(state, Option(value).map(x => parametrize(x, tokens.parseCSV(tokenSp): _*)).getOrElse(key))
  }
}

trait GetUser {
  self: { val sc: ServiceContainer } =>

  def getUser(state: State): Option[User] = {
    import state._
    val pval = (x: String) => paramAsString(x).getOrElse("")
    val vnum = (x: String) => varAsNumber(x).getOrElse(0L)
    val us = sc.get[UserService]
    val (id, ref, cid) = (pval("userId"), pval("userRef"), vnum("currentUserId"))

    paramAsObject[User]("user").orElse { //user object as param
      if (id.nonEmpty) us.getById(id.toLong) //by ID
      else if (ref == "system") Some(us.getSystemUser) //system user
      else if (ref.nonEmpty) us.getByRef(ref) //by REF
      else if (cid > 0) us.getById(cid) //current user
      else None
    }.map { user => user.password = null; user } // hide password
  }
}

trait GetDomain {
  self: { val sc: ServiceContainer } =>

  def getDomain(state: State): Option[Domain] = {
    import state._
    val pval = (x: String) => paramAsString(x).getOrElse("")
    val id = pval("domainId")

    paramAsObject[Domain]("domain").orElse { //domain object as param
      if (id.nonEmpty) sc.get[DomainService].getById(id.toLong) //by ID
      else None
    }
  }
}

class OnPackDirective extends BaseDirective with StateWithVars with GetUser {
  override def execute(state: StateWithVars) {
    import state._
    getUser(state).foreach { user =>
      val r = (false /: pval("pack").parseCSV)((a,b) => a || user.packs.exists(p => p.pack == b))
      if (r) renderBody()
    }
  }
}

class ListUserPacksDirective extends BaseDirective with GetUser {
  override def execute(state: State) =
    getUser(state).foreach { user =>
      val xs = user.packs.map(p => p.pack).sorted
      render(state, xs)
    }
}

class ListInstalledPacksDirective extends BaseDirective {
  override def execute(state: State) {
    val xs = setting("InstalledPacks").safe.parseCSV.sorted
    render(state, xs)
  }
}

class OnDomainTypeDirective extends BaseDirective with StateWithVars {
  override def execute(state: StateWithVars) {
    import state._
    val siteState = systemVarAsObject[SiteState]("siteState").get
    if (pval("type").parseCSV.contains(siteState.currentDomainType)) renderBody()
  }
}

class CountUserUnreadMessagesDirective extends BaseDirective with GetUser {
  override def execute(state: State) =
    getUser(state).foreach(user => output(state,
      sc.get[MessageService].search(new MessageService.SearchInputX(0, 0) {
        override val userId = Some(user.id)
        override val read = Some(false)
      }).total))
}

class UserDirective extends BaseDirective with GetUser {
  override def execute(state: State) =
    getUser(state).foreach(user => outputTM(state, state.wrap(user)))
}

class UserFullNameDirective extends BaseDirective with GetUser {
  override def execute(state: State) =
    getUser(state).foreach(user => output(state, sc.get[UserService].fullName(user, state.resolveSite)))
}

class UserMetaValueDirective extends BaseDirective with StateWithVars with GetUser {
  override def execute(state: StateWithVars) =
    getUser(state).foreach { user =>
      import state._
      output(state, Meta(user.meta)(pval("key"), ""))
    }
}

class UserTextDirective extends BaseDirective with StateWithVars with GetUser {
  override def execute(state: StateWithVars) =
    getUser(state).foreach { user =>
      import state._
      output(state, label("u."+user.ref+"."+pval("key"), site).safe)
    }
}

class UserCreditDirective extends BaseDirective with GetUser {
  override def execute(state: State) =
    getUser(state).foreach(user => output(state, toX100(user.credit*100L)))
}

class ListUserDomainsDirective extends BaseDirective with GetUser {
  override def execute(state: State) =
    getUser(state).foreach { user =>
      val xs = (if (isUserAdmin) sc.get[DomainService].getAll else user.domains.toList).sortBy(_.domain)
      render(state, xs, (x: Domain) => state.wrap(x))
    }
}

class ListDomainsDirective extends BaseDirective {
  override def execute(state: State) {
    val xs = sc.get[DomainService].getAll.sortBy(_.domain)
    render(state, xs, (x: Domain) => state.wrap(x))
  }
}

class ListRolesDirective extends BaseDirective {
  override def execute(state: State) {
    val xs = sc.get[UserService].getAllRoles.sortBy(_.role)
    render(state, xs, (x: Role) => state.wrap(x))
  }
}

class OnPageDirective extends BaseDirective with StateWithVars {
  override def execute(state: StateWithVars) {
    import state._
    val pageIndex = systemVarAsString("pageIndex").getOrElse("")
    val b = pval("index").parseCSV match {
      case Seq("-", x @ _*) => !x.contains(pageIndex) //none of the pages
      case x => x.contains(pageIndex) //any of the pages
    }
    if (b) renderBody()
  }
}

class RealCountriesDirective extends BaseDirective {
  override def execute(state: State) =
    output(state, sc.get[LocationService].getRealCountries)
}

class CountryNameDirective extends BaseDirective {
  override def execute(state: State) {
    import state._
    val obj = paramAsObject[Country]("obj").get
    output(state, label(obj.getTextKey, resolveSite))
  }
}

class OnFileDirective extends BaseDirective with StateWithVars {
  override def execute(state: StateWithVars) {
    import state._
    sc.get[FileService].getByPath(VirtualPath(pval("path")), site).foreach(_ => renderBody())
  }
}

class HasFileDirective extends BaseDirective with StateWithVars {
  override def execute(state: StateWithVars) {
    import state._
    output(state, sc.get[FileService].getByPath(VirtualPath(pval("path")), site).isDefined)
  }
}

class PrintFileDirective extends BaseDirective with StateWithVars {
  override def execute(state: StateWithVars) {
    import state._
    sc.get[FileService].getByPath(VirtualPath(pval("path")), site).foreach { file =>
      val body = sc.get[FileService].getBody(file, paramAsString("sfx").getOrElse(File.SUFFIX_DEFAULT))
      val out = new ByteArrayOutputStream
      withOpen(body.sourceStream) { inputToOutput(_, out) }
      val x = out.toString(UTF8)
      output(state, x.convert(pval("output")))
    }
  }
}

class DomainDefaultSiteDirective extends BaseDirective with StateWithVars with GetDomain {
  override def execute(state: StateWithVars) =
    getDomain(state).foreach { domain =>
      import state._
      val site = sc.get[DomainService].defaultSite(domain.domain) //default site chain (xx1.pp1.wis1)
      output(state, if (pval("mode") == "key") site.key else site.value) //default site key (wis1)
    }
}

/** These are the sites domain owner can use to input multilingual text into
  * her domain (order matches the default input sites)
  */
class ListDomainInputSitesDirective extends BaseDirective with StateWithVars with GetDomain {
  override def execute(state: StateWithVars) =
    getDomain(state).foreach { domain =>
      import state._
      val xtra = pval("xtra")+" "
      val domainSites = sc.get[DomainService].listSites(domain) // wis1 wis2
      val xtraDomainSites = for { d <- domainSites; x <- Constants.SITE_EXTRAS.filter(xtra.head==) } yield d+x // wis1j wis2j
      val xs = pval("xtra").nonEmpty ? xtraDomainSites | domainSites
      pval("output") match {
        case "json" => output(state, xs.map("\""+_+"\"").mkString("[", ",", "]"))
        case _ =>
          if (pval("var") != "") output(state, xs)
          else render(state, xs)
      }
    }
}

/** These are the default sites (or mine sites for CMS) user can use to input multilingual text */
class ListInputSitesDirective extends BaseDirective with StateWithVars {
  override def execute(state: StateWithVars) {
    import state._
    val siteChainMap = systemVarAsObject[SiteChainMap]("siteChainMap").get

    /** Return list of root sites. */
    def rootSites: List[String] = parsedSites.map(_.key).toList

    /** Return list of sites from 'chain' map in the same order as 'root' sites. */
    def listSites(chainMap: Map[String,_]): List[String] = {
      val linkRoot = siteChainMap.rootSiteLinks
      (for (rootSite <- rootSites) yield chainMap.keys.filter(rootSite == linkRoot(_)).head).toList
    }

    val xs = pval("mode") match {
      case "mine" => listSites(siteChainMap.mineSiteChain)
      case "xtra" => listSites(siteChainMap.xtraSiteChain)
      case _ => rootSites
    }

    pval("output") match {
      case "json" => output(state, xs.map("\""+_+"\"").mkString("[", ",", "]"))
      case _ =>
        if (pval("var") != "") output(state, xs)
        else render(state, xs)
    }
  }
}

/** Return site's locale, if no site supplied then revert to a current site. */
class SiteLocaleDirective extends BaseDirective with StateWithVars {
  override def execute(state: StateWithVars) {
    import state._
    val siteChainMap = systemVarAsObject[SiteChainMap]("siteChainMap").get
    val siteState = systemVarAsObject[SiteState]("siteState").get
    val site = pval("key") or siteState.rootSite
    val rootSite = siteChainMap.rootSiteLinks(site)
    val x = parsedSites.filter(_.key == rootSite).head.locale
    output(state, x)
  }
}

class ParamLimitDirective extends BaseDirective with StateWithVars {
  override def execute(state: StateWithVars) {
    import state._
    val x =
      pval("mode") match {
        case "ipp" => settingService.getItemsPerPage(pval("key"))
        case _ => setting("ParamsLimit").parseCSVMap()(pval("key")).toInt
      }
    output(state, x)
  }
}

class ListCurrenciesDirective extends BaseDirective {
  override def execute(state: State) {
    val xs = setting("Currencies").parseCSV
    render(state, xs)
  }
}

class DefaultCurrencyDirective extends BaseDirective {
  override def execute(state: State) =
    output(state, sc.get[SettingService].getDefaultCurrency)
}

/** Wrap a seq into template model to access in FTL */
class WrapSeqDirective extends BaseDirective {
  override def execute(state: State) {
    import state._
    val seq = paramAsObject[Seq[_]]("seq").getOrElse(Seq.empty)
    output(state, seq) // applied SeqTMMaker
  }
}

/** Format timestamp as pattern */
class TimestampDirective extends BaseDirective with StateWithVars {
  override def execute(state: StateWithVars) {
    import state._
    val ts = paramAsObject[Any]("ts") match {
      case Some(x: SimpleScalar) if x.getAsString == "now" => Timestamp()
      case Some(x: SimpleNumber) => Timestamp(x.getAsNumber.longValue)
      case Some(x: SimpleScalar) => Timestamp(x.getAsString)
      case Some(x) if x != null => throw new CoreException("Unknown timestamp: "+x.getClass+" "+x)
      case _ => null
    }

    if (Option(ts).isDefined) pval("format") match {
      case x if x != "" => output(state, ts.toString(pval("format")))
      case _ => ts.toString
    }
  }
}

class GenerateRefDirective extends BaseDirective with StateWithVars {
  override def execute(state: StateWithVars) {
    import state._
    output(state, generateRef(pval("pattern")))
  }
}
