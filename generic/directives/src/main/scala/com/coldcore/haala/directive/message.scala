package com.coldcore.haala
package directive.freemarker

import java.text.SimpleDateFormat
import domain._
import service._
import service.MessageService.MessageEntry
import com.coldcore.misc.scala.StringOp._
import BaseDirective._
import core.Timestamp

trait GetMessageEntry {
  self: { val sc: ServiceContainer } =>

  def getMessageEntry(state: State): Option[MessageEntry] = {
    import state._
    val pval = (x: String) => paramAsString(x).getOrElse("")
    val (id, ref) = (pval("entryId"), pval("ref"))

    paramAsObject[MessageEntry]("entry").orElse { //entry object as param
      if (id.nonEmpty) sc.get[MessageService].getById(id.toLong) //by ID
      if (ref.nonEmpty) sc.get[MessageService].searchByRef(ref) //by REF
      else None
    }
  }
}

class MessageEntryPropertyDirective extends BaseDirective with StateWithVars with GetMessageEntry {

  private def sender(entry: MessageEntry): Option[User] =
    sc.get[UserService].getById(entry.meta("senderId", "0").safeLong)

  class TimestampProcessor(ts: Long, state: StateWithVars, entry: MessageEntry) extends Templated(state) {
    val sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm")
    templatedValue(sdf.format(Timestamp(ts).toDate), output(state, _: String))
  }

  class SubjectProcessor(state: StateWithVars, entry: MessageEntry) extends Templated(state) {
    val domain = entry.meta("domain", "")
    val value = parametrize(
      label("msg.sub"+entry.abyss.pack.on("_")+entry.subject, state.site), // msg.sub1   msg.sub_mypack1
      "domain:"+domain)
    templatedValue(value, output(state, _: String))
  }

  class SenderNameProcessor(state: StateWithVars, entry: MessageEntry) extends Templated(state) {
    val value = entry.meta("replyName", sender(entry).map(sc.get[UserService].fullName(_, state.site)).getOrElse(""))
    templatedValue(value, output(state, _: String))
  }

  class SenderEmailProcessor(state: StateWithVars, entry: MessageEntry) extends Templated(state) {
    val value = entry.meta("replyEmail", sender(entry).map(_.email).getOrElse(""))
    templatedValue(value, output(state, _: String))
  }

  class SenderUsernameProcessor(state: StateWithVars, entry: MessageEntry) extends Templated(state) {
    val value = sender(entry).map(_.username).getOrElse("")
    templatedValue(value, output(state, _: String))
  }

  class MetaProcessor(key: String, state: StateWithVars, entry: MessageEntry) extends Templated(state) {
    templatedValue(entry.meta(key, "?"), output(state, _: String))
  }

  class TextProcessor(state: StateWithVars, entry: MessageEntry, convert: Boolean = true) extends Templated(state) {
    val value = if (convert) entry.text.convert("html") else entry.text
    templatedValue(value, output(state, _: String))
  }

  class TemplateProcessor(state: StateWithVars, entry: MessageEntry) extends Templated(state) {
    val value = entry.abyss.pack.on(entry.abyss.pack+"/")+"view/msg"+entry.typ+".ftl:"+state.site
    templatedValue(value, output(state, _: String))
  }

  override def execute(state: StateWithVars) = getMessageEntry(state).foreach { entry =>
    import state._
    pval("property") match {
      case "created" => new TimestampProcessor(entry.abyss.created, state, entry)
      case "subject" => new SubjectProcessor(state, entry)
      case "senderName" => new SenderNameProcessor(state, entry)
      case "senderEmail" => new SenderEmailProcessor(state, entry)
      case "senderUsername" => new SenderUsernameProcessor(state, entry)
      case "text" => new TextProcessor(state, entry)
      case "html" => new TextProcessor(state, entry, convert = false)
      case "template" => new TemplateProcessor(state, entry)
      case x if x.startsWith("meta.") => new MetaProcessor(x.drop(5), state, entry)
      case _ =>
    }
  }
}
