package com.coldcore.haala
package directive.freemarker

import domain._
import service._
import com.coldcore.misc.scala.StringOp._
import BaseDirective._

class ListSpamStoragesDirective extends BaseDirective {
  override def execute(state: State) {
    val xs = sc.get[SpamService].searchStorages(SearchInput(0, 999, "id desc")).objects
    render(state, xs, (x: EmailStorage) => state.wrap(x))
  }
}

class SpamTaskStatusDirective extends BaseDirective {
  override def execute(state: State) {
    val x = sc.get[SpamService].getTaskConfig match {
      case Some(config) =>
        val running = setting("SpamEmailsFlag").isTrue
        if (running) 1 else if (config.error != "") 3 else 2 // 1 running, 2 stopped, 3 waiting
      case None => 2
    }
    output(state, x)
  }
}

class SpamTaskErrorDirective extends BaseDirective {
  override def execute(state: State) =
    sc.get[SpamService].getTaskConfig.foreach(config => output(state, config.error))
}

class PrintSpamTaskProgressDirective extends BaseDirective {
  override def execute(state: State) =
    sc.get[SpamService].getTaskConfig.foreach { config =>
      import state._
      val text = label("spam.taskProgress", resolveSite).safe
      val processed = config.good+config.bad
      val total = sc.get[SpamService].getStorageById(config.storage).map(_.total.toInt).getOrElse(0)
      val ratio = if (total == 0) "?" else math.round(processed.toDouble*100d/total.toDouble)+"%"
      output(state, parametrize(text, "processed:"+processed, "total:"+total, "ratio:"+ratio))
    }
}

class PrintSpamTaskRatioDirective extends BaseDirective {
  override def execute(state: State) =
    sc.get[SpamService].getTaskConfig.foreach { config =>
      import state._
      val text = label("spam.taskRatio", resolveSite).safe
      val ratio = math.round(config.good.toDouble*100d/(config.good+config.bad).toDouble)+"%"
      output(state, parametrize(text, "good:"+config.good, "bad:"+config.bad, "ratio:"+ratio))
    }
}
