package com.coldcore.haala
package directive.freemarker

import freemarker.template._
import domain._
import com.coldcore.misc.scala.StringOp._
import BaseDirective._

/** Provide option groups */
trait OptionsGroup {
  def onGroup(group: String): List[Int]

  def optionsGroup(state: State): List[Int] = {
    val pval = (x: String) => state.paramAsString(x).getOrElse("")
    onGroup(pval("group"))
  }
}

/** Retrieve option container object from state */
trait GetOptionObject[U <: BaseOption, T <: BaseOptionObject[U]] {
  def getObject(state: State): Option[T]
}

/** Construct dummy option for static-like methods */
trait NewOption[U <: BaseOption] {
  def newOption: U
}

/** List all options provided by OptionsGroup */
abstract class ListOptionTypesDirective extends BaseDirective with OptionsGroup {
  override def execute(state: State) = render(state, optionsGroup(state), (typ: Int) => new SimpleNumber(typ))
}

/** Print label for option key */
abstract class OptionKeyDirective[U <: BaseOption] extends BaseDirective with StateWithVars with NewOption[U] {
  override def execute(state: StateWithVars) {
    import state._
    val key = newOption.getTextKeyByType(pnum("type").toInt)
    output(state, label(key, site) or key)
  }
}

/** Print option value */
abstract class OptionValueDirective[U <: BaseOption, T <: BaseOptionObject[U]] extends BaseDirective with StateWithVars with GetOptionObject[U,T] {
  override def execute(state: StateWithVars) =
    getObject(state).foreach { obj =>
      import state._
      val x = obj.options.filter(_.`type` == pnum("type")).map(_.value).headOption.getOrElse("")
      output(state, x)
    }
}

/** Test if object has specified option */
abstract class HasOptionDirective[U <: BaseOption, T <: BaseOptionObject[U]] extends BaseDirective with StateWithVars with GetOptionObject[U,T] {
  override def execute(state: StateWithVars) =
    getObject(state).foreach { obj =>
      import state._
      val x = obj.options.exists(_.`type` == pnum("type"))
      output(state, x)
    }
}

/** Print label for option value */
abstract class OptionTextDirective[U <: BaseOption, T <: BaseOptionObject[U]] extends BaseDirective with StateWithVars with GetOptionObject[U,T] {
  override def execute(state: StateWithVars) =
    getObject(state).foreach { obj =>
      import state._
      val x =
        obj.options.filter(_.`type` == pnum("type")) match {
          case Seq(opt, _*) =>
            val x = label(opt.getTextKey+"b", site) or label(opt.getTextKey, site).safe
            (if (x == "" || opt.value.safe == "") x else parametrize(x, "val:"+opt.value)) or opt.getTextKey
          case _ => ""
        }
      output(state, x)
    }
}

/** Print option mode */
abstract class OptionModeDirective[U <: BaseOption] extends BaseDirective with StateWithVars with NewOption[U] {
  override def execute(state: StateWithVars) {
    import state._
    output(state, newOption.getMode(pnum("type").toInt))
  }
}
