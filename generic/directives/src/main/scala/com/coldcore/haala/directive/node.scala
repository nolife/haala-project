package com.coldcore.haala
package directive.freemarker

import BaseDirective._
import service.{HeartbeatInfo, MiscService, ServiceContainer, SettingService}
import core.{Meta, SiteChain, Statistics, Timestamp}
import core.NetworkUtil._

import scala.beans.BeanProperty

case class NodeSettingTuple(host: String, value: Meta)

trait GetNodeHeartbeat {
  self: { val sc: ServiceContainer } =>

  def getNodeHeartbeat(state: State): Option[NodeSettingTuple] = {
    import state._
    val host = paramAsString("host").getOrElse("")

    paramAsObject[NodeSettingTuple]("heartbeat").orElse { //heartbeat object as param
      if (host.nonEmpty) Option(sc.get[SettingService].querySystem(s"Heartbeat/$host")) //by host
        .map(x => NodeSettingTuple(host, Meta(x)))
      else None
    }
  }
}

trait GetNodeSummary {
  self: { val sc: ServiceContainer } =>

  def getNodeSummary(state: State): Option[NodeSettingTuple] = {
    import state._
    val host = paramAsString("host").getOrElse("")

    paramAsObject[NodeSettingTuple]("summary").orElse { //summary object as param
      if (host.nonEmpty) Option(sc.get[SettingService].querySystem(s"NodeSummary/$host")) //by host
        .map(x => NodeSettingTuple(host, Meta(x)))
      else None
    }
  }
}

/** List all node heartbeat settings */
class ListNodeHeartbeatsDirective extends BaseDirective {
  override def execute(state: State) {
    val heartbeats = sc.get[SettingService].getForSiteLike("Heartbeat/%", SiteChain.sys).toList.sortBy(_._1)
    val xs = heartbeats.map { case (k,v) => NodeSettingTuple(k.dropWhile('/' !=).drop(1), Meta(v)) }
    render(state, xs, state.wrap)
  }
}

trait NodeTimestampProcessor {
  self: BaseDirective with StateWithVars =>

  private val timespan = (ts1: Timestamp, ts2: Timestamp) => {
    val (d, h, m, s, _) = ts1.timespan(ts2)
    (d :: h :: m :: s :: Nil).zip("d" :: "h" :: "m" :: "s" :: Nil).dropWhile(_._1 == 0)
      .map(x => x._1+x._2).mkString(" ") or "0s"
  }

  class TsProcessor(ts: String, state: StateWithVars, databaseTime: Boolean = false) extends Templated(state) {
    case class Tuple(ts: String, ago: String, date: String)
    val convert = (t: Tuple) => Seq("ts:"+t.ts, "ago:"+t.ago, "date:"+t.date)
    val ago =
      if (ts.safeLong == 0) "?"
      else timespan(if (databaseTime) sc.get[MiscService].databaseTime else Timestamp(), Timestamp(ts))
    val date = if (ts.safeLong == 0) "?" else Timestamp(ts).toString("dd/MM/yyyy HH:mm:ss")
    templatedTuple[Tuple](Some(Tuple(ts, ago, date)), output(state, _), _.ts, convert)
  }

  class UptimeProcessor(ts1: String, ts2: String, state: StateWithVars) extends Templated(state) {
    val value =
      if (ts1.isEmpty || ts2.isEmpty) "?"
      else timespan(Timestamp(ts1), Timestamp(ts2))
    templatedValue(value, output(state, _: String))
  }

}

trait NodeMemoryProcessor {
  self: BaseDirective with StateWithVars =>

  class MemoryProcessor(free: Option[Long], total: Option[Long], max: Option[Long], state: StateWithVars) extends Templated(state) {
    case class Tuple(total: String, max: String, used: String)
    val convert = (t: Tuple) => Seq("total:"+t.total, "max:"+t.max, "used:"+t.used)
    val mb = (size: Option[Long]) => size.map(x => math.round(x.toDouble/1024d/1024d)+"m").getOrElse("?")
    val usedMB =  mb(for (t <- total; f <- free) yield t-f)
    templatedTuple[Tuple](Some(Tuple(mb(total), mb(max), usedMB)), output(state, _), _.used, convert)
  }

}

/** Property of node heartbeat setting */
class NodeHeartbeatPropertyDirective extends BaseDirective with StateWithVars with GetNodeHeartbeat with NodeTimestampProcessor {

  class ValueProcessor(value: Any, state: StateWithVars) extends Templated(state) {
    templatedValue(value.toString or "?", output(state, _: String))
  }

  override def execute(state: StateWithVars) = getNodeHeartbeat(state).foreach { heartbeat =>
    import state._
    val meta = (key: String) => heartbeat.value(key, "?")
    pval("property") match {
      case "host" => new ValueProcessor(heartbeat.host, state)
      case key @ "beat" => new TsProcessor(meta(key), state, databaseTime = true)
      case key @ "status" => new ValueProcessor(meta(key), state)
      case _ =>
    }
  }
}

/** Property of node summary setting */
class NodeSummaryPropertyDirective extends BaseDirective with StateWithVars with GetNodeSummary
  with NodeTimestampProcessor with NodeMemoryProcessor {

  class ValueProcessor(value: Any, state: StateWithVars) extends Templated(state) {
    templatedValue(value.toString or "?", output(state, _: String))
  }

  override def execute(state: StateWithVars) = getNodeSummary(state).foreach { summary =>
    import state._
    val memory = (n: Int) => summary.value("memory", "").parseCSV.lift(n).map(_.safeLong)
    val meta = (key: String) => summary.value(key, "?")
    pval("property") match {
      case "host" => new ValueProcessor(summary.host, state)
      case key @ "timestamp" => new TsProcessor(meta(key), state)
      case key @ "dbtime" => new TsProcessor(meta(key), state, databaseTime = true)
      case key @ "hostname" => new ValueProcessor(meta(key), state)
      case key @ "ip" => new ValueProcessor(meta(key), state)
      case key @ "mac" => new ValueProcessor(meta(key), state)
      case key @ "requests" => new ValueProcessor(meta(key), state)
      case "memory" => new MemoryProcessor(memory(0), memory(1), memory(2), state)
      case "uptime" => new UptimeProcessor(summary.value("started", ""), summary.value("timestamp", ""), state)
      case _ =>
    }
  }
}

/** Property of current node */
class NodePropertyDirective extends BaseDirective with StateWithVars with NodeTimestampProcessor with NodeMemoryProcessor {
  @BeanProperty var statistics: Statistics = _
  @BeanProperty var heartbeatInfo: HeartbeatInfo = _

  private def gb(size: Long): String =
    BigDecimal(size.toDouble/1024d/1024d/1024d).setScale(2, BigDecimal.RoundingMode.HALF_UP).toDouble+"g"

  class ValueProcessor(value: Any, state: StateWithVars) extends Templated(state) {
    templatedValue(value.toString or "?", output(state, _: String))
  }

  class FsSummaryProcessor(state: StateWithVars) extends Templated(state) {
    case class Tuple(files: String, size: String, capacity: String, free: String)
    val convert = (t: Tuple) => Seq("files:"+t.files, "size:"+t.size, "capacity:"+t.capacity, "free:"+t.free)
    val meta = Meta(setting("FsSummary").safe)
    val size = meta.get("size").map(_.toLong).map(gb).getOrElse("?")
    val free = meta.get("space").map(_.toLong).map(gb).getOrElse("?")
    val capacity = meta.get("maxId").map(_.toLong).map(x => math.round((x*100L).toDouble/10000d/10000d)+"%").getOrElse("?")
    templatedTuple[Tuple](Some(Tuple(meta("files", "?"), size, capacity, free)), output(state, _), _.size, convert)
  }

  override def execute(state: StateWithVars) {
    import state._
    val runtime = Runtime.getRuntime
    pval("property") match {
      case "host" => new ValueProcessor(heartbeatInfo.nodeHost, state)
      case "hostname" => new ValueProcessor(localhostName, state)
      case "ip" => new ValueProcessor(localhostAddress, state)
      case "mac" => new ValueProcessor(localhostMac, state)
      case "timestamp" => new TsProcessor(Timestamp().toString, state)
      case "scaled" => new ValueProcessor(sc.get[SettingService].scaled ? "y" | "n", state)
      case "dbtime" => new TsProcessor(sc.get[MiscService].databaseTime.toString, state, databaseTime = true)
      case "requests" => new ValueProcessor(statistics.requests, state)
      case "memory" => new MemoryProcessor(Some(runtime.freeMemory), Some(runtime.totalMemory), Some(runtime.maxMemory), state)
      case "uptime" => new UptimeProcessor(statistics.started.toString, Timestamp().toString, state)
      case "filesystem" => new FsSummaryProcessor(state)
      case "fstime" => new TsProcessor(Meta(setting("FsSummary").safe)("timestamp", ""), state)
      case _ =>
    }
  }
}
