package com.coldcore.haala
package directive.freemarker

import java.text.SimpleDateFormat
import java.util.Date
import com.coldcore.haala.core.{CoreException, Timestamp}
import org.scalatest.mockito.MockitoSugar
import org.mockito.Mockito._
import org.scalatest.{BeforeAndAfter, FlatSpec, Matchers, WordSpec}
import org.junit.runner.RunWith
import org.scalatest.junit.JUnitRunner
import service.test.MyMock
import scala.collection.JavaConverters._
import freemarker.template.{SimpleNumber, SimpleScalar, SimpleSequence, TemplateModel}
import domain.{Label, Pack, Role, User}
import core.SiteChain.Implicits._
import core.VirtualPath.Implicits._

@RunWith(classOf[JUnitRunner])
class UrlDirectiveSpec extends WordSpec with MockitoSugar with BeforeAndAfter {
  var m: MyMock = _

  before {
    m = new MyMock
  }

  "directive" should {
    "construct proper URL" in {
      m.registerFile(11, "bar/template.ftl", ".sys", """<@mydir path="files/beans.xml" nc="1"/>""")

      val r = m.processFtl("bar/template.ftl", Map(
        "mySite" -> "mc1",
        "resourceVersion" -> "1.2",
        "mydir" -> new UrlDirective))

      assertResult("/files/beans.xml?site=mc1&rv=1.2&nc=1") { r }
    }
  }

}

@RunWith(classOf[JUnitRunner])
class DotDirectiveSpec extends WordSpec with MockitoSugar with BeforeAndAfter {
  var m: MyMock = _

  before {
    m = new MyMock
  }

  "directive" should {
    "construct proper URL" in {
      m.registerFile(11, "bar/template.ftl", ".sys", """<@mydir />""")

      val r = m.processFtl("bar/template.ftl", Map(
        "mydir" -> new DotDirective))

      assertResult("/web/z/dot.gif") { r }
    }
  }

}

@RunWith(classOf[JUnitRunner])
class SettingValueDirectiveSpec extends WordSpec with MockitoSugar with BeforeAndAfter {
  var m: MyMock = _

  before {
    m = new MyMock
  }

  class MyMock extends com.coldcore.haala.service.test.MyMock {
    val directive = new SettingValueDirective |< { x =>
      x.serviceContainer = serviceContainer; x.freemarkerEngine = fmEngine }
  }

  private def process =
    m.processFtl("bar/template.ftl", Map(
      "currentSite" -> "xx1.mc1",
      "mydir" -> m.directive))

  "directive" should {
    "pring setting" in {
      m.registerFile(11, "bar/template.ftl", ".sys", """<@mydir key="MySetting" />""")
      when(m.settingService.query("MySetting", "xx1.mc1")).thenReturn("foo = 1,2,3")
      assertResult("foo = 1,2,3") { process }
    }

    "format and print setting" in {
      m.registerFile(11, "bar/template.ftl", ".sys", """<@mydir key="MySetting" mapKey="foo" mapSp=";" output="json" />""")
      when(m.settingService.query("MySetting", "xx1.mc1")).thenReturn("foo = 1,2,3")
      assertResult("""["1","2","3"]""") { process }
    }

    "set global var" in {
      m.registerFile(11, "bar/template.ftl", ".sys", """<@mydir var="myVar" scope="global" key="MySetting" />""")
      when(m.settingService.query("MySetting", "xx1.mc1")).thenReturn("foo = 1,2,3")
      val (r, env) =
        m.processFtlEnv("bar/template.ftl", Map(
          "currentSite" -> "xx1.mc1",
          "myVar" -> "myValue",
          "mydir" -> m.directive))
      assertResult("foo = 1,2,3") { env.getGlobalVariable("myVar").toString }
    }

    "format and set var" in {
      m.registerFile(11, "bar/template.ftl", ".sys", """<@mydir var="myVar" key="MySetting" mapKey="foo" mapSp=";" output="array" />""")
      when(m.settingService.query("MySetting", "xx1.mc1")).thenReturn("foo = 1,2,3")
      val (r, env) =
        m.processFtlEnv("bar/template.ftl", Map(
          "currentSite" -> "xx1.mc1",
          "myVar" -> "myValue",
          "mydir" -> m.directive))
      assertResult("1") { env.getVariable("myVar").asInstanceOf[SimpleSequence].get(0).toString }
    }

  }

}

@RunWith(classOf[JUnitRunner])
class OnRoleDirectiveSpec extends WordSpec with MockitoSugar with BeforeAndAfter {
  var m: MyMock = _

  before {
    m = new MyMock
  }

  class MyMock extends com.coldcore.haala.service.test.MyMock {
    val directive = new OnRoleDirective |< { x => x.serviceContainer = serviceContainer }
  }

  "directive" should {
    "skip content with no user" in {
      m.registerFile(11, "bar/template.ftl", ".sys", """<@mydir role="current">foo</@>""")

      val r = m.processFtl("bar/template.ftl", Map(
        "mydir" -> m.directive))

      assertResult("") { r }
    }

    "proceed with logged in user" in {
      m.registerFile(11, "bar/template.ftl", ".sys", """<@mydir role="current">foo</@>""")

      val r = m.processFtl("bar/template.ftl", Map(
        "currentUserId" -> 2L,
        "mydir" -> m.directive).asInstanceOf[Map[String,AnyRef]])

      assertResult("foo") { r }
    }

    "proceed with no user (negative check)" in {
      m.registerFile(11, "bar/template.ftl", ".sys", """<@mydir role="-, current">foo</@>""")

      val r = m.processFtl("bar/template.ftl", Map(
        "mydir" -> m.directive).asInstanceOf[Map[String,AnyRef]])

      assertResult("foo") { r }
    }

    "skip content with logged in user (negative check)" in {
      m.registerFile(11, "bar/template.ftl", ".sys", """<@mydir role="-,current">foo</@>""")

      val r = m.processFtl("bar/template.ftl", Map(
        "currentUserId" -> 2L,
        "mydir" -> m.directive).asInstanceOf[Map[String,AnyRef]])

      assertResult("") { r }
    }

    "skip content with no super user" in {
      m.registerFile(11, "bar/template.ftl", ".sys", """<@mydir role="super">foo</@>""")

      val r = m.processFtl("bar/template.ftl", Map(
        "currentUserId" -> 2L,
        "mydir" -> m.directive).asInstanceOf[Map[String,AnyRef]])

      assertResult("") { r }
    }

    "proceed with logged in super user" in {
      m.registerFile(11, "bar/template.ftl", ".sys", """<@mydir role="super">foo</@>""")

      val r = m.processFtl("bar/template.ftl", Map(
        "currentUserId" -> 2L,
        "superUserId" -> 3L,
        "mydir" -> m.directive).asInstanceOf[Map[String,AnyRef]])

      assertResult("foo") { r }
    }

    "skip content with no role" in {
      m.registerFile(11, "bar/template.ftl", ".sys", """<@mydir role="cms,agent">foo</@>""")
      when(m.securityService.isUserInRole(Role.ADMIN)).thenReturn(false)
      when(m.securityService.isUserInRole("ROLE_CMS")).thenReturn(false)
      when(m.securityService.isUserInRole("ROLE_AGENT")).thenReturn(false)

      val r = m.processFtl("bar/template.ftl", Map(
        "currentUserId" -> 2L,
        "mydir" -> m.directive).asInstanceOf[Map[String,AnyRef]])

      assertResult("") { r }
    }

    "proceed with any role" in {
      m.registerFile(11, "bar/template.ftl", ".sys", """<@mydir role="cms,agent">foo</@>""")
      when(m.securityService.isUserInRole(Role.ADMIN)).thenReturn(false)
      when(m.securityService.isUserInRole("ROLE_CMS")).thenReturn(true)
      when(m.securityService.isUserInRole("ROLE_AGENT")).thenReturn(false)

      val r = m.processFtl("bar/template.ftl", Map(
        "currentUserId" -> 2L,
        "mydir" -> m.directive).asInstanceOf[Map[String,AnyRef]])

      assertResult("foo") { r }
    }

    "proceed with admin role" in {
      m.registerFile(11, "bar/template.ftl", ".sys", """<@mydir role="cms,agent">foo</@>""")
      when(m.securityService.isUserInRole(Role.ADMIN)).thenReturn(true)
      when(m.securityService.isUserInRole("ROLE_CMS")).thenReturn(false)
      when(m.securityService.isUserInRole("ROLE_AGENT")).thenReturn(false)

      val r = m.processFtl("bar/template.ftl", Map(
        "currentUserId" -> 2L,
        "mydir" -> m.directive).asInstanceOf[Map[String,AnyRef]])

      assertResult("foo") { r }
    }

    "skip content with any role missing (plus check)" in {
      m.registerFile(11, "bar/template.ftl", ".sys", """<@mydir role="+,cms,agent">foo</@>""")
      when(m.securityService.isUserInRole(Role.ADMIN)).thenReturn(false)
      when(m.securityService.isUserInRole("ROLE_CMS")).thenReturn(true)
      when(m.securityService.isUserInRole("ROLE_AGENT")).thenReturn(false)

      val r = m.processFtl("bar/template.ftl", Map(
        "currentUserId" -> 2L,
        "mydir" -> m.directive).asInstanceOf[Map[String,AnyRef]])

      assertResult("") { r }
    }

    "proceed with all roles (plus check)" in {
      m.registerFile(11, "bar/template.ftl", ".sys", """<@mydir role="+,cms,agent">foo</@>""")
      when(m.securityService.isUserInRole(Role.ADMIN)).thenReturn(false)
      when(m.securityService.isUserInRole("ROLE_CMS")).thenReturn(true)
      when(m.securityService.isUserInRole("ROLE_AGENT")).thenReturn(true)

      val r = m.processFtl("bar/template.ftl", Map(
        "currentUserId" -> 2L,
        "mydir" -> m.directive).asInstanceOf[Map[String,AnyRef]])

      assertResult("foo") { r }
    }

    "proceed with any role missing (plus check, admin)" in {
      m.registerFile(11, "bar/template.ftl", ".sys", """<@mydir role="+,cms,agent">foo</@>""")
      when(m.securityService.isUserInRole(Role.ADMIN)).thenReturn(true)
      when(m.securityService.isUserInRole("ROLE_CMS")).thenReturn(true)
      when(m.securityService.isUserInRole("ROLE_AGENT")).thenReturn(false)

      val r = m.processFtl("bar/template.ftl", Map(
        "currentUserId" -> 2L,
        "mydir" -> m.directive).asInstanceOf[Map[String,AnyRef]])

      assertResult("foo") { r }
    }

    "proceed with all roles missing (negative check)" in {
      m.registerFile(11, "bar/template.ftl", ".sys", """<@mydir role="-,cms,agent">foo</@>""")
      when(m.securityService.isUserInRole(Role.ADMIN)).thenReturn(false)
      when(m.securityService.isUserInRole("ROLE_CMS")).thenReturn(false)
      when(m.securityService.isUserInRole("ROLE_AGENT")).thenReturn(false)

      val r = m.processFtl("bar/template.ftl", Map(
        "currentUserId" -> 2L,
        "mydir" -> m.directive).asInstanceOf[Map[String,AnyRef]])

      assertResult("foo") { r }
    }

    "skip content with any role (negative check)" in {
      m.registerFile(11, "bar/template.ftl", ".sys", """<@mydir role="-,cms,agent">foo</@>""")
      when(m.securityService.isUserInRole(Role.ADMIN)).thenReturn(false)
      when(m.securityService.isUserInRole("ROLE_CMS")).thenReturn(true)
      when(m.securityService.isUserInRole("ROLE_AGENT")).thenReturn(false)

      val r = m.processFtl("bar/template.ftl", Map(
        "currentUserId" -> 2L,
        "mydir" -> m.directive).asInstanceOf[Map[String,AnyRef]])

      assertResult("") { r }
    }

    "skip content with any role (negative check, admin)" in {
      m.registerFile(11, "bar/template.ftl", ".sys", """<@mydir role="-,cms,agent">foo</@>""")
      when(m.securityService.isUserInRole(Role.ADMIN)).thenReturn(true)
      when(m.securityService.isUserInRole("ROLE_CMS")).thenReturn(true)
      when(m.securityService.isUserInRole("ROLE_AGENT")).thenReturn(false)

      val r = m.processFtl("bar/template.ftl", Map(
        "currentUserId" -> 2L,
        "mydir" -> m.directive).asInstanceOf[Map[String,AnyRef]])

      assertResult("") { r }
    }

  }

}

@RunWith(classOf[JUnitRunner])
class LabelDirectiveSpec extends WordSpec with MockitoSugar with BeforeAndAfter {
  var m: MyMock = _

  before {
    m = new MyMock
  }

  class MyMock extends com.coldcore.haala.service.test.MyMock {
    val directive = new LabelDirective |< { x => x.serviceContainer = serviceContainer }
  }

  "directive" should {
    "print text key" in {
      m.registerFile(11, "bar/template.ftl", ".sys", """<@mydir key="x.1"/>""")

      val r = m.processFtl("bar/template.ftl", Map(
        "mydir" -> m.directive))

      assertResult("x.1") { r }
    }

    "print text based on page index" in {
      m.registerFile(11, "bar/template.ftl", ".sys", """<@mydir key="001"/>""")
      when(m.labelService.query("00018.001", "xx1.mc1")).thenReturn("Hello {name}!")
      when(m.labelService.query("001", "xx1.mc1")).thenReturn("Yello {name}!")

      val systemObjects = Map(
        "pageIndex" -> "00018"
      )

      val r = m.processFtl("bar/template.ftl", Map(
        "currentSite" -> "xx1.mc1",
        "system" -> systemObjects.asJava,
        "mydir" -> m.directive))

      assertResult("Hello {name}!") { r }
    }

    "print text not based on page index" in {
      m.registerFile(11, "bar/template.ftl", ".sys", """<@mydir key="001" pindex="0"/>""")
      when(m.labelService.query("00018.001", "xx1.mc1")).thenReturn("Hello {name}!")
      when(m.labelService.query("001", "xx1.mc1")).thenReturn("Yello {name}!")

      val systemObjects = Map(
        "pageIndex" -> "00018"
      )

      val r = m.processFtl("bar/template.ftl", Map(
        "currentSite" -> "xx1.mc1",
        "system" -> systemObjects.asJava,
        "mydir" -> m.directive))

      assertResult("Yello {name}!") { r }
    }

    "print parametrized text" in {
      m.registerFile(11, "bar/template.ftl", ".sys", """<@mydir key="x.1" tokens="name:Big Joe,amount:100"/>""")
      when(m.labelService.query("x.1", "xx1.mc1")).thenReturn("{name} has ${amount}")

      val r = m.processFtl("bar/template.ftl", Map(
        "currentSite" -> "xx1.mc1",
        "mydir" -> m.directive))

      assertResult("Big Joe has $100") { r }
    }

    "print parametrized text (semicolon check)" in {
      m.registerFile(11, "bar/template.ftl", ".sys", """<@mydir key="x.1" tokens=";name:Big Joe;amount:100,50"/>""")
      when(m.labelService.query("x.1", "xx1.mc1")).thenReturn("{name} has ${amount}")

      val r = m.processFtl("bar/template.ftl", Map(
        "currentSite" -> "xx1.mc1",
        "mydir" -> m.directive))

      assertResult("Big Joe has $100,50") { r }
    }

    "print default label" in {
      m.registerFile(11, "bar/template.ftl", ".sys", """<@mydir key="x.1" bydefault="x.2"/>""")
      when(m.labelService.query("x.1", "xx1.mc1")).thenReturn(null)
      when(m.labelService.query("x.2", "xx1.mc1")).thenReturn("Good bye!")

      val r = m.processFtl("bar/template.ftl", Map(
        "currentSite" -> "xx1.mc1",
        "mydir" -> m.directive))

      assertResult("Good bye!") { r }
    }

    "print default text" in {
      m.registerFile(11, "bar/template.ftl", ".sys", """<@mydir key="x.1" bydefault="Good bye!"/>""")
      when(m.labelService.query("x.1", "xx1.mc1")).thenReturn(null)

      val r = m.processFtl("bar/template.ftl", Map(
        "currentSite" -> "xx1.mc1",
        "mydir" -> m.directive))

      assertResult("Good bye!") { r }
    }

    "print default text (empty check)" in {
      m.registerFile(11, "bar/template.ftl", ".sys", """<@mydir key="x.1" bydefault=""/>""")
      when(m.labelService.query("x.1", "xx1.mc1")).thenReturn(null)

      val r = m.processFtl("bar/template.ftl", Map(
        "currentSite" -> "xx1.mc1",
        "mydir" -> m.directive))

      assertResult("") { r }
    }

  }

}

@RunWith(classOf[JUnitRunner])
class UserDirectiveSpec extends WordSpec with MockitoSugar with BeforeAndAfter {
  var m: MyMock = _

  before {
    m = new MyMock
  }

  "directive" should {
    "return wrapped user" in {
      m.registerFile(11, "bar/template.ftl", ".sys", """<@mydir var="user" userId="2"/> ${user.username()} """)
      when(m.userService.getById(2)).thenReturn(Some(new User { id = 2L; username = "joe2" }))

      val r = m.processFtl("bar/template.ftl", Map(
        "mydir" -> new UserDirective { serviceContainer = m.serviceContainer }))

      assertResult("joe2") { r.trim }
    }
  }

}

@RunWith(classOf[JUnitRunner])
class ListUserPacksDirectiveSpec extends WordSpec with MockitoSugar with BeforeAndAfter {
  var m: MyMock = _

  before {
    m = new MyMock
  }

  "directive" should {
    "return render user packs" in {
      m.registerFile(11, "bar/template.ftl", ".sys", """ <@mydir userId="2" ; x, n ><p>${n+"."+x}</p></@> """)
      when(m.userService.getById(2)).thenReturn(Some(new User {
        id = 2L; username = "joe2"
        packs = List(new Pack { pack = "PackA" }, new Pack { pack = "PackB" }).asJava
      }))

      val r = m.processFtl("bar/template.ftl", Map(
        "mydir" -> new ListUserPacksDirective { serviceContainer = m.serviceContainer }))

      assertResult("<p>0.PackA</p><p>1.PackB</p>") { r.trim }
    }
  }

}

@RunWith(classOf[JUnitRunner])
class ParseCsvDirectiveSpec extends WordSpec with MockitoSugar with BeforeAndAfter {
  var m: MyMock = _

  before {
    m = new MyMock
  }

  "directive" should {
    "return render user packs" in {
      m.registerFile(11, "bar/template.ftl", ".sys",
        """ <@mydir var="array" value="a,b,c" output="array" /> """)

      val (r, env) =
        m.processFtlEnv("bar/template.ftl", Map(
          "mydir" -> new ParseCsvDirective { serviceContainer = m.serviceContainer; freemarkerEngine = m.fmEngine }))
      assertResult(List("a", "b", "c")) { env.getVariable("array").asInstanceOf[SimpleSequence].toList.asScala }
    }
  }

}

@RunWith(classOf[JUnitRunner])
class TimestampDirectiveSpec extends FlatSpec with MockitoSugar with BeforeAndAfter with Matchers {
  var m: MyMock = _

  before {
    m = new MyMock
  }

  def process(arg: AnyRef): String = {
    m.registerFile(11, "bar/template.ftl", ".sys",
      """ <@mydir ts=arg format="dd/MM/yyyy HH:mm" /> """)
    m.processFtl("bar/template.ftl", Map(
      "arg" -> arg,
      "mydir" -> new TimestampDirective { serviceContainer = m.serviceContainer })).trim
  }

  "directive" should "fail on unknown timestamp argument" in {
    a [CoreException] should be thrownBy process(new Label)
  }

  "directive" should "print current timestamp (now as argument)" in {
    m.registerFile(11, "bar/template.ftl", ".sys",
      """ <@mydir ts="now" format="dd/MM/yyyy" /> """)
    val r = m.processFtl("bar/template.ftl", Map(
      "mydir" -> new TimestampDirective { serviceContainer = m.serviceContainer })).trim
    r shouldBe new SimpleDateFormat("dd/MM/yyyy").format(new Date)
  }

  "directive" should "print timestamp (java long as argument)" in { // domain objects TS as JLong
    process(new java.lang.Long(20180318225634L)) shouldBe "18/03/2018 22:56"
  }

  "directive" should "print timestamp (tm number as argument)" in {
    process(new SimpleNumber(20180318225634L)) shouldBe "18/03/2018 22:56"
  }

  "directive" should "print timestamp (tm scalar as argument)" in {
    process(new SimpleScalar("20180318225634")) shouldBe "18/03/2018 22:56"
  }

  "timestamp" should "format itself without directive" in {
    m.registerFile(11, "bar/template.ftl", ".sys",
      """ ${ts.toString("dd/MM/yyyy HH:mm")} """)
    m.processFtl("bar/template.ftl", Map("ts" -> Timestamp(20180318225634L))).trim shouldBe "18/03/2018 22:56"
  }

}
