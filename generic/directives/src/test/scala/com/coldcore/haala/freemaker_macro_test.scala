package com.coldcore.haala
package directive.freemarker

import org.scalatest.mockito.MockitoSugar
import org.scalatest.{BeforeAndAfter, WordSpec}
import org.junit.runner.RunWith
import org.scalatest.junit.JUnitRunner
import com.coldcore.haala.service.test.MyMock
import core.SiteChain.Implicits._
import core.VirtualPath.Implicits._

@RunWith(classOf[JUnitRunner])
class SimpleMacroSpec extends WordSpec with MockitoSugar with BeforeAndAfter {
  var m: MyMock = _

  before {
    m = new MyMock
  }

  "noimg" should {
    "print image tag" in {
      val template =
        """
          |<#macro noimg>
          |  <img src="/web/z/dot.gif"/>
          |</#macro>
          |
          |<@noimg/>
        """.stripMargin

      m.registerFile(11, "bar/template.ftl", ".sys", template)

      val r = m.processFtl("bar/template.ftl", Map.empty)
      assertResult("""<img src="/web/z/dot.gif"/>""") { r.trim }
    }
  }

  "url" should {
    var template =
      """
        |<#macro url path nc="" salt="">
        |  /${path}?site=${mySite}&rv=${resourceVersion}<#if nc != "">&nc=1</#if><#if salt != "">&salt=456</#if>
        |</#macro>
      """.stripMargin

    "print full path" in {
      m.registerFile(11, "bar/template.ftl", ".sys", template+"""<@url path="files/beans.xml"/>""")

      val r = m.processFtl("bar/template.ftl", Map(
        "resourceVersion" -> "1.2",
        "mySite" -> "mc1"
      ))
      assertResult("""/files/beans.xml?site=mc1&rv=1.2""") { r.trim }
    }

    "print full path (no cache check)" in {
      m.registerFile(11, "bar/template.ftl", ".sys", template+"""<@url path="files/beans.xml" nc="y" salt="y"/>""")

      val r = m.processFtl("bar/template.ftl", Map(
        "resourceVersion" -> "1.2",
        "mySite" -> "mc1"
      ))
      assertResult("""/files/beans.xml?site=mc1&rv=1.2&nc=1&salt=456""") { r.trim }
    }
  }

  "form" should {
    var template =
      """
        |<#macro form id="pageForm" mode="" style="">
        |<form id="${id}" style="${style}" action="#" onsubmit="return false;" <#if mode == "upload">enctype="multipart/form-data"</#if> >
        |<#nested>
        |</form>
        |</#macro>
      """.stripMargin

    "print form tag" in {
      m.registerFile(11, "bar/template.ftl", ".sys", template+"""<@form mode="upload" id="myForm" style="display:none">foo</@>""")

      val r = m.processFtl("bar/template.ftl", Map.empty)
      assertResult("""<form id="myForm" style="display:none" action="#" onsubmit="return false;" enctype="multipart/form-data" >foo</form>""") {
        r.trim.replace("\n", "") }
    }
  }

}
