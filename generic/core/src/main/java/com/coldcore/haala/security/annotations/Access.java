package com.coldcore.haala.security.annotations;

public enum Access {
    NEVER, ALWAYS;

  public String getValue() {
      switch (this) {
          case ALWAYS: return "ALWAYS";
          default: return "NEVER";
      }
  }

  public static Access byValue(String value) {
      if (value.equals(ALWAYS.getValue())) return ALWAYS;
      return NEVER;
  }
}