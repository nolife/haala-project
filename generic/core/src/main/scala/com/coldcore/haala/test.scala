package com.coldcore.haala
package test

import org.springframework.core.io.ResourceLoader
import org.scalatest.mockito.MockitoSugar
import core.{Initializer, ResourceLoaderHolder, Log}
import core.GeneralUtil.printStackTrace
import com.coldcore.misc5.CByte._
import com.coldcore.haala.core.Constants._

/** Base mock, every other mock extends this. */
class MyMock extends MockitoSugar {
  new Initializer

  val resourceLoader = mock[ResourceLoader]
  val resourceLoaderHolder = new ResourceLoaderHolder |< { x => x.resourceLoader = resourceLoader }

  def testing(method: String) = println("Testing "+method)

  def getResourceAsStream(filename: String) = getClass.getResourceAsStream(filename)
  def getResourceAsString(filename: String) = new String(toByteArray(getResourceAsStream(filename)), UTF8)

  val consoleLog = new Log {
    override def info(msg: String) = println("INFO "+msg)
    override def warn(msg: String) = println("WARN "+msg)
    override def debug(msg: String) = println("DEBUG "+msg)
    override def error(msg: String, e: Throwable = null) = println("ERROR " + msg+"\n"+printStackTrace(e))
  }
}
