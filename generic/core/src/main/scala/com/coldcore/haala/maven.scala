package com.coldcore.haala
package maven

import java.io.File
import com.coldcore.misc5.CFile
import java.util.regex.Pattern
import com.coldcore.misc.scala.StringOp._
import com.coldcore.haala.core.{XmlUtil, CoreException}
import scala.xml.{Elem, NodeSeq}
import core.Constants.UTF8

/** Read XML file content and substitute proprietary directives (such as xml::import)
  * with content read from other files. Processes Spring XML resources in WEB-INF directory
  * as there is no other way to include content (or parts of the content) into Spring XML
  * from external XML files. Should be used from Maven passing directories containing XML
  * files as arguments into the main method.
  */
object ProcessXmlResources {

  implicit def file2cFile[T](f: File) = new CFile(f)

  def main(args: Array[String]) {
    val dirs = args.map(x => new File(x)).toList //directories containing XML files
    dirs.foreach { dir =>
      println("Processing XML resources in "+dir.getAbsoluteFile)
      if (!dir.exists() || !dir.isDirectory) println("Directory does not exist")
      else {
        val files = dir.list(true).filter(_.getName.endsWith(".xml"))
        println(s"Found ${files.size} XML files to process")
        processXml(new File(dir, "pack-all.xml"), dirs) //process pack-all.xml file first
        for (file <- files) processXml(file, dirs) //process the rest of the XML files
      }
    }
  }

  private def processXml(file: File, dirs: List[File]) = if (file.exists) {
    val content = readFile(file)
    val x = xmlImport(content, file, dirs)
    file.rewriteFile(x.bytesUTF8)
  }

  private def readFile(file: File): String = {
    if (!file.exists || !file.isFile) {
      println("File does not exist: "+file.getName)
      throw new CoreException("File does not exist: "+file.getAbsoluteFile)
    }
    new String(file.readFile(), UTF8)
  }

  /** Find a file in base directories */
  private def findFile(filename: String, dirs: List[File]): File =
    dirs.find(dir => new File(dir, filename).exists) match {
      case Some(dir) => new File(dir, filename)
      case None => throw new CoreException(s"File $filename not found in any of the directories")
    }

  implicit class ChildSelectable(ns: NodeSeq) {
    def \* = ns.flatMap { _ match {
      case e:Elem => e.child
      case _ => NodeSeq.Empty
    }}
  }

  /** Process xml::import directive */
  private def xmlImport(content: String, file: File, dirs: List[File]): String = {
    val f = (v: String) => {
      println("Found xml::import declaration in "+file.getName)
      val pmap = v.parseCSVMap(";")
      val (filename, nodes) = (pmap("file"), pmap("node").parseCSV)
      nodes.map { node =>
        val xml = XmlUtil.loadFile(findFile(filename, dirs))
        (node == "root") ? (xml \*) | (xml \\ node \*)
      }.mkString("\n\n")
    }

    val (pfx, sfx) = ("<!-- xml::import", "-->")
    val (qpfx, qsfx) = (Pattern.quote(pfx), Pattern.quote(sfx))
    if (!content.contains(pfx)) content //no declaration
    else (content /: (1 to 100)) { (s, _) => //process declarations in imported files
      findTokens(qpfx, qsfx, s).headOption match {
        case None => s
        case Some(part) =>
          val x = findTokens("\\{", "\\}", part).map(f).mkString("\n\n")
          s.substring(0, s.indexOf(pfx))+x+s.substring(s.indexOf(sfx, s.indexOf(pfx))+sfx.length)
      }
    }
  }

}
