package com.coldcore.haala.session.hibernate

import javax.servlet.FilterChain
import javax.servlet.ServletRequest
import javax.servlet.ServletResponse
import com.coldcore.haala.session.SessionHandler
import org.springframework.orm.hibernate5.support.OpenSessionInViewFilter
import org.springframework.mock.web.{MockHttpServletResponse, MockHttpServletRequest}

class CustomSessionHandlerOSIV extends OpenSessionInViewFilter with SessionHandler {
  override def withSession(x: => Unit) =
    doFilterInternal(new MockHttpServletRequest, new MockHttpServletResponse, new CustomFilterChain(x))
}

class CustomFilterChain(x: => Unit) extends FilterChain {
  override def doFilter(request: ServletRequest, response: ServletResponse) = x
}
