package com.coldcore.haala
package core

import annotation.tailrec
import java.util.{Calendar, Date, TimeZone}
import java.security.MessageDigest
import java.io._
import java.io.{File => JFile}
import java.net.{InetAddress, NetworkInterface}
import java.text.SimpleDateFormat
import org.springframework.security.core.context.{SecurityContext, SecurityContextHolder}
import xml.{Elem, XML}
import com.coldcore.misc.scala.StringOp._
import com.coldcore.misc5.{CByte, CFile}
import com.coldcore.misc5.IdGenerator.{generate, generateNumbers}
import com.google.gson.Gson
import Constants.UTF8
import scala.collection.JavaConverters._

/** Utils operating on site keys and chains. */
object SiteUtil {
  /** Parse site *setting* and return a list with its ordered tuples.
   *  xx1=xx1:en, xx2=xx1.xx2:ru|rs -> [(xx1 xx1 en [en]) (xx2 xx1.xx2 ru [ru,rs])]
   */
  def parseSiteSetting(setting: String): List[ParsedSiteSetting] =
    for (x <- setting.parseCSV) yield ParsedSiteSetting(x)

  /** Parse site *setting* and return a locale specified by root site chain (or first available if no match).
   *  [xx1.xx2] xx1=xx1:en, xx2=xx1.xx2:ru|rs -> ru
   */
  def siteLocale(site: SiteChain, setting: String): String = {
    val parsed = parseSiteSetting(setting)
    parsed.filter(_.site == site).map(_.locale).headOption.getOrElse(parsed.head.locale)
  }

  /** Create an extra site chain by suffixing its original chain.
   *  uk1 -> uk1j   uk1.ru8 -> uk1j.ru8j
   */
  def suffixSite(site: SiteChain, sfx: String): SiteChain = SiteChain(site.keys.map(_+sfx).mkString("."))

  /** Strip root site values of unnecessary data leaving only site chains.
   *  Input: site_key=value map produced by parse-csv-map of root sites *setting*.
   *  {xx1 xx1:en, xx2 xx1.xx2:ru} -> {xx1 xx1, xx2 xx1.xx2}
   */
  def stripRootSites(rootSites: Map[String,String]): Map[String,SiteChain] =
    (Map.empty[String,String] /: rootSites)((a,b) => b match { case (k,v) => a + (k -> v.split(":").head) }) //xx2=xx1.xx2
        .map { case (k,v) => k -> SiteChain(v) }

  /** Return a root site key of a chain. Input: a site chain and a map (or its *setting* CSV) with root sites.
   *  xx1.mw1.xx2.mw2 -> xx2   xx1.ph1.zx1.mw1.xx2.ph2.zx2.mw2 -> xx2
   */
  def rootSiteKey(site: SiteChain, rootSites: String): String = rootSiteKey(site, parseCSVMap(rootSites))

  def rootSiteKey(site: SiteChain, rootSites: Map[String,_]): String =
    site.keys.filter(rootSites.get(_).isDefined).reverse.headOption.getOrElse("")

  /** Mix root site chain with *pure* site chain.
   *  xx1.xx2+mw -> xx1.mw1.xx2.mw2   xx1.xx2+ph.zx.mw -> xx1.ph1.zx1.mw1.xx2.ph2.zx2.mw2
   */
  def mixRootPureSites(rootSite: SiteChain, site: SiteChain): SiteChain = {
    val f = (s: String) => { // xx1 xx2
      val sx = s.substring(2) //root site format: starts with 2 symbols followed by numbers (xx1)
      "."+s+site.keys.map("."+_+sx).mkString // ph.zx.mw -> [.xx1].ph1.zx1.mw1
    }
    SiteChain(rootSite.keys.map(f).mkString.substring(1)) // .xx1.ph1.zx1.mw1 -> xx1.ph1.zx1.mw1
  }

  /** Create root, mine, extra site chains and links to root.
   *  Inputs: pure-site - *pure* site (eg. ph.zx.mw)
   *          root-sites - *stripped* map with root sites (eg. xx2=xx1.xx2)
   *          extra-sfx - extra suffixes (eg. jb)
   */
  def createChainMap(pureSite: SiteChain, rootSites: Map[String,SiteChain], extraSfx: String): SiteChainMap = {
    // xx2=xx1.xx2:ru|rs -> xx2=xx1.mw1.xx2.mw2   xx2=xx1.xx2:ru|rs -> xx2=xx1.ph1.zx1.mw1.xx2.ph2.zx2.mw2
    def mapMaker(f: (String, SiteChain, String) => Map[String,SiteChain]) =
      (for ((rootKey, rootSite) <- rootSites) yield { // [xx2] = [xx1.xx2]
        val curSite = mixRootPureSites(rootSite, pureSite) // xx1.ph1.zx1.mw1.xx2.ph2.zx2.mw2
        val myKey = curSite.key // mw2
        f(rootKey, curSite, myKey)
      }).foldLeft(Map.empty[String,SiteChain])(_++_)

    val rootMap = mapMaker((rootKey: String, curSite: SiteChain, _) => Map(rootKey -> curSite)) // xx1=xx1.mw1   xx2=xx1.mw1.xx2.mw2
    val mineMap = mapMaker((_, curSite: SiteChain, myKey: String) => Map(myKey -> curSite)) // mw1=xx1.mw1   mw2=xx1.mw1.xx2.mw2
    val xtraMap = mapMaker((rootKey: String, curSite: SiteChain, myKey: String) => // xx1j=xx1j.mw1j   mw2j=xx1j.mw1j.xx2j.mw2j
      extraSfx.map { s =>
        val extraSite = suffixSite(curSite, ""+s)
        Map(rootKey+s -> extraSite, myKey+s -> extraSite)
      }.foldLeft(Map.empty[String,SiteChain])(_++_))

    val rootRoot = mapMaker((rootKey: String, _, _) => Map(rootKey -> SiteChain(rootKey))) // xx1=xx1   xx2=xx2
    val mineRoot = mapMaker((rootKey: String, _, myKey: String) => Map(myKey -> SiteChain(rootKey))) // mw1=xx1   mw2=xx2
    val xtraRoot = mapMaker((rootKey: String, _, myKey: String) => // xx1j=xx1   mw2j=xx2
      extraSfx.map(s => Map(rootKey+s -> SiteChain(rootKey), myKey+s -> SiteChain(rootKey)))
              .foldLeft(Map.empty[String,SiteChain])(_++_))

    SiteChainMap(rootMap, mineMap, xtraMap, (rootRoot ++ mineRoot ++ xtraRoot).map { case (k,v) => k -> v.value })
  }
}

object GeneralUtil {
  /** Return formatted price.
   *  1025.00 -> 1,025.00   1025 -> 1,025   25 -> 25   1000000 -> 1,000,000
   */
  def formatPrice(price: String): String = {
    val i = price.indexOf(".")
    val (a, b) = if (i == -1) (price, "") else (price.substring(0, i), price.substring(i))
    ("".padTo(12-a.length, " ").mkString+a).grouped(3).map(_.trim).filter(""!=).mkString(",")+b
  }

  /** Convert configuration parameter into mills.
   *  Supported suffixes: s - seconds, m - minutes, h - hours, d - days, y - years ('m' is default).
   *  10s -> 10*1000   3h -> 3*60*60*1000   2 -> 2*60*1000
   */
  def configToMills(value: String): Long =
    Map('s' -> 1000L, 'm' -> 1000L*60L, 'h' -> 1000L*60L*60L, 'd' -> 1000L*60L*60L*24L, 'y' -> 1000L*60L*60L*24L*365L)
      .get(value.last).map(value.take(value.length-1).toLong*_).getOrElse(value.toLong*1000L*60L)

  /** Convert configuration parameter into bytes.
   *  Supported suffixes: k - kb, m - mb, g - gb ('m' is default).
   *  75k -> 75*1024   3g -> 3*1024*1024*1024   2 -> 2*1024*1024
   */
  def configToBytes(value: String): Long =
    Map('k' -> 1024L, 'm' -> 1024L*1024L, 'g' -> 1024L*1024L*1024L)
      .get(value.last).map(value.take(value.length-1).toLong*_).getOrElse(value.toLong*1024L*1024L)

  def smartToken(label: String, token: String, value: String): String = {
    val (anySep, goSep, haltSep) = (".,:;!?", ",", ".:!?")
    val isAnySep = (x: Char) => anySep.contains(x)
    val isHaltSep = (x: Char) => haltSep.contains(x)
    val isGoSep = (x: Char) => goSep.contains(x)
    val isSpace = (x: Char) => ' ' == x

    //replace 'src' with 'trg', repeat until 'src' not found.
    @tailrec def replaceStr(s: String, src: String, trg: String): String = s match {
      case x if !x.contains(src) => x
      case x => replaceStr(x.replace(src, trg), src, trg)
    }

    //remove a unset token '{token}' from a label preserving a correct separator
    val removeToken = (lbl: String, t: String) =>
      if (!lbl.contains(t)) lbl
      else {
        val index = lbl.indexOf(t); val end = index+t.length
        val (prevSym1, prevSym2, nextSym1) = (lbl(index-1), lbl(index-2), lbl(end))
        // located in Spain, {town}[.any] -> located in Spain.
        if (isSpace(prevSym1) && isGoSep(prevSym2) && isAnySep(nextSym1))
          lbl.substring(0, index-2)+lbl.substring(end)
        else // located where[?halt] {town}[.any] -> located where?
        if (isSpace(prevSym1) && isHaltSep(prevSym2) && isAnySep(nextSym1))
          lbl.substring(0, index-1)+lbl.substring(end+1)
        else // added {town}[:halt] -> added:
        if (isSpace(prevSym1) && !isAnySep(prevSym2) && isHaltSep(nextSym1))
          lbl.substring(0, index-1)+lbl.substring(end)
        else // located in Spain {town} -> located in Spain
          lbl.substring(0, index)+lbl.substring(end)
      }

    var s = s"   $label   " //3 spaces to avoid substring errors
    s = if (value.safe == "") removeToken(s, token) else replaceStr(s, token, value)
    s = replaceStr(s, "  ", " ") //compact spaces
    for (p <- anySep) s = replaceStr(s, " "+p, ""+p) //trim separator spaces
    for { p <- anySep; v <- anySep } s = replaceStr(s, ""+p+v, ""+p) //.! -> .  ?! -> ?  ,; -> ,
    s = s.trim
    if (isAnySep((s+" ")(0))) s.substring(1).trim else s
  }

  /** Convert mills into sec.
   *  1024 -> 1.024   0 -> 0.000   10 -> 0.010
   */
  def millsToSec(time: Long): String = {
    val s = "".padTo(4-(""+time).length, "0").mkString+time
    s.substring(0, s.length-3)+"."+s.substring(s.length-3)
  }

  def digest(in: InputStream, alg: String): String = {
    val md = MessageDigest.getInstance(alg)
    val update = () => {
      val fin = new FilterInputStream(in) {
        override def read(b: Array[Byte], off: Int, len: Int): Int = {
          val i = in.read(b, off, len); if (i > 0) md.update(b, 0, i); i
        }
      }
      md.reset
      CByte.toByteArray(fin)
    }
    update()
    md.digest.map(x => java.lang.Integer.toHexString(x & 0xFF)).mkString
  }

  def digest(s: String, alg: String): String =
    digest(new ByteArrayInputStream(s.bytesUTF8), alg)

  def printStackTrace(e: Throwable): String = {
    val writer = new StringWriter
    e match {
      case _: NoTraceException => writer.append(e.getMessage)
      case _ => e.printStackTrace(new PrintWriter(writer))
    }
    writer.toString
  }

  def methodName(depth: Int = 0): String =
    Thread.currentThread().getStackTrace.toList(2+depth).getMethodName //ignore getStackTrace and methodName

  /** Generate REF based on pattern separated by | character eg. |RT|hms|sym8|ms|num6|
    * ms  - mills (hms as hex)
    * sym - random symbols including numbers
    * num - random numbers
    * uppercase included as is
    */
  def generateRef(pattern: String, f: PartialFunction[String,String] = PartialFunction.empty): String =
    pattern.split("\\|").map(_.trim).filter("" !=).map {
      case x if f.isDefinedAt(x) => f(x)
      case x if x == x.toUpperCase => x // use uppercase as is
      case "ms" => System.currentTimeMillis
      case "hms" => System.currentTimeMillis.hex
      case x if x matches "(sym)[0-9]+" => val n = x.drop(3).toInt; generate(n,n)
      case x if x matches "(num)[0-9]+" => val n = x.drop(3).toInt; generateNumbers(n,n)
      case x => throw new CoreException("Unknown pattern: "+x)
    }.mkString.toUpperCase

}

object HttpUtil {
  import tools.nsc.io.Streamable
  import org.apache.http.client.methods.{HttpPost, HttpGet, HttpRequestBase}
  import org.apache.http.entity.{StringEntity, ContentType}
  import org.apache.http.client.entity.UrlEncodedFormEntity
  import org.apache.http.message.BasicNameValuePair
  import org.apache.http.impl.client.HttpClients
  import org.apache.http.client.config.RequestConfig
  import org.apache.http.impl.conn.PoolingHttpClientConnectionManager

  trait HttpCall
  case class HttpCallSuccessful(url: String, code: Int, body: String, rh: List[(String,String)]) extends HttpCall
  case class HttpCallFailed(url: String, error: Throwable) extends HttpCall

  private val cm = new PoolingHttpClientConnectionManager()
  cm.setMaxTotal(5)
  cm.setDefaultMaxPerRoute(20)

  private val rc = RequestConfig.custom()
    .setConnectTimeout(30000)  // 30 seconds
    .setConnectionRequestTimeout(30000) // ...
    .setSocketTimeout(30000) // ...
    .build

  private val client = HttpClients.custom
    .setConnectionManager(cm)
    .setDefaultRequestConfig(rc)
    .build

  private def doGetOrPost(url: String, method: HttpRequestBase): HttpCall =
    try {
      val response = client.execute(method)
      try {
        HttpCallSuccessful(url, response.getStatusLine.getStatusCode,
          new String(Streamable.bytes(response.getEntity.getContent), UTF8),
          response.getAllHeaders.map(x => (x.getName, x.getValue)).toList)
      } finally { response.close() }
    } catch { case e : Throwable => HttpCallFailed(url, e)
    } finally { method.releaseConnection() }

  /** HTTP GET */
  def httpGet(url: String, rh: List[(String,String)]): HttpCall = {
    val method = new HttpGet(url)
    for ((a, b) <- rh) method.setHeader(a, b)
    doGetOrPost(url, method)
  }
  // alternative for simple header name and values
  def httpGet(url: String, rh: String*): HttpCall =
    httpGet(url, (for (Seq(a, b) <- rh.grouped(2)) yield a -> b).toList)

  /** HTTP POST as "text/html", overwrite with "rh" as "Content-type", "text/xml; charset=UTF-8" */
  def httpPost(url: String, content: String, rh: List[(String,String)]): HttpCall = {
    val method = new HttpPost(url)
    method.setEntity(new StringEntity(content, ContentType.create("text/html", UTF8)))
    for ((a, b) <- rh) method.setHeader(a, b)
    doGetOrPost(url, method)
  }
  // alternative for simple header name and values
  def httpPost(url: String, content: String, rh: String*): HttpCall =
    httpPost(url, content, (for (Seq(a, b) <- rh.grouped(2)) yield a -> b).toList)

  /** HTTP POST as "application/x-www-form-urlencoded" */
  def httpPost(url: String, rp: Map[String,String], rh: List[(String,String)]): HttpCall = {
    val method = new HttpPost(url)
    val nvps = for ((a, b) <- rp) yield new BasicNameValuePair(a, b)
    method.setEntity(new UrlEncodedFormEntity(nvps.toList.asJava, UTF8))
    for ((a, b) <- rh) method.setHeader(a, b)
    doGetOrPost(url, method)
  }

}

object SecurityUtil {
  def context: SecurityContext = SecurityContextHolder.getContext
}

object XmlUtil {
  def loadFile(file: JFile): Elem =
    withOpen(new InputStreamReader(new BufferedInputStream(new FileInputStream(file)), UTF8) ) { XML.load }
}

object FileUtil {
  def read(file: JFile): Array[Byte] = new CFile(file).readFile
  def exists(file: JFile): Boolean = file.exists
}

object JsonUtil {
  def fromJson[T](content: String, c: Class[T]): T = Option(new Gson().fromJson(content.safe, c)).get
  def toJson[T](content: T, c: Class[T]): String = new Gson().toJson(content, c)
}

object Timestamp {
  implicit def timestamp2long(t: Timestamp): Long = t.toLong
  implicit def timestamp2jlong(t: Timestamp): JLong = t.toLong

  def utcCalendar: Calendar = Calendar.getInstance(TimeZone.getTimeZone("UTC"))

  def apply() = new Timestamp
  def apply(d: Date) = new Timestamp(d)
  def apply(s: String) = new Timestamp(s)
  def apply(date: Int, time: Int, ms: Int = 0) = new Timestamp(date, time, ms)
  def apply(ts: Long) = new Timestamp(ts)
  def apply(s: String, pattern: String) = new Timestamp(s, pattern)

  def fromMills(mills: Long): Timestamp = {
    val t = new Timestamp
    t.toCalendar.setTimeInMillis(mills)
    t
  }

  private def prependZero(i: Int, n: Int): String = i.toString.reverse.padTo(n, "0").reverse.mkString
}

class Timestamp(c: Calendar) {

  def this() {
    this(Timestamp.utcCalendar)
  }

  def this(d: Date) {
    this
    c.setTime(d)
  }

  /** "20180216235948778" (result of "toLong" method) */
  def this(s: String) {
    this
    assert(s.length == 14 || s.length == 17, s"String size is ${s.length}, needs to be 14 or 17")
    val xs = s.take(4).toInt :: s.grouped(2).drop(2).map(_.toInt).toList
    c.set(Calendar.YEAR, xs.head)
    c.set(Calendar.MONTH, xs(1)-1)
    c.set(Calendar.DAY_OF_MONTH, xs(2))
    c.set(Calendar.HOUR_OF_DAY, xs(3))
    c.set(Calendar.MINUTE, xs(4))
    c.set(Calendar.SECOND, xs(5))
    c.set(Calendar.MILLISECOND, if (s.length == 17) s.takeRight(3).toInt else 0)
  }

  /** 20180216 235948 778 */
  def this(date: Int, time: Int, ms: Int = 0) {
    this(Timestamp.prependZero(date, 8)+Timestamp.prependZero(time, 6)+Timestamp.prependZero(ms, 3))
  }

  /** 20180216235948778 (result of "toLong" method) */
  def this(ts: Long) {
    this(ts.toString)
  }

  /** "2018-02-16" "yyyy-MM-dd" */
  def this(s: String, pattern: String) {
    this
    c.setTime(new SimpleDateFormat(pattern).parse(s))
  }

  def copy: Calendar = {
    val cc = Timestamp.utcCalendar
    cc.setTime(c.getTime)
    cc
  }

  override def toString: String = { // format: yyyyMMddHHmmssSSS
    val prepend = (i: Int, n: Int) => i.toString.reverse.padTo(n, "0").reverse.mkString
    prepend(c.get(Calendar.YEAR), 4) +
      (c.get(Calendar.MONTH)+1 :: c.get(Calendar.DAY_OF_MONTH) ::
        c.get(Calendar.HOUR_OF_DAY) :: c.get(Calendar.MINUTE) :: c.get(Calendar.SECOND) :: Nil)
        .map(prepend(_, 2)).mkString +
      prepend(c.get(Calendar.MILLISECOND), 3)
  }

  def toShortString: String = toString.dropRight(3) // format: yyyyMMddHHmmss

  /** "yyyy-MM-dd" */
  def toString(pattern: String): String = new SimpleDateFormat(pattern).format(c.getTime)

  /** 20180216235948778 */
  def toLong: Long = toString.toLong

  def toMills: Long = c.getTimeInMillis

  def toDate: Date = c.getTime

  /** Calendar used by this Timestamp */
  def toCalendar: Calendar = c

  def add(field: Int, num: Int): Timestamp = {
    c.add(field, num)
    this
  }

  def addMills(num: Int): Timestamp = add(Calendar.MILLISECOND, num)
  def addSeconds(num: Int): Timestamp = add(Calendar.SECOND, num)
  def addMinutes(num: Int): Timestamp = add(Calendar.MINUTE, num)
  def addHours(num: Int): Timestamp = add(Calendar.HOUR_OF_DAY, num)
  def addDays(num: Int): Timestamp = add(Calendar.DAY_OF_MONTH, num)
  def addYears(num: Int): Timestamp = add(Calendar.YEAR, num)

  def midnight: Timestamp = {
    (Calendar.HOUR_OF_DAY :: Calendar.MINUTE :: Calendar.SECOND :: Calendar.MILLISECOND :: Nil).foreach(c.set(_, 0))
    this
  }

  def tomorrow: Timestamp = addDays(1)
  def yesterday: Timestamp = addDays(-1)

  def compareTo(t: Timestamp) = c.compareTo(t.toCalendar)
  def compareTo(d: Date) = toDate.compareTo(d)
  def compareTo(c2: Calendar) = c.compareTo(c2)

  /** Time span between two Timestamps (days, hours, minutes, seconds, milliseconds) */
  def timespan(t: Timestamp): (Int,Int,Int,Int,Int) = {
    val diff = math.abs(toMills-t.toMills)
    val d = diff/(24L*60L*60L*1000L)
    val h = diff/(60L*60L*1000L)%24L
    val m = diff/(60L*1000L)%60L
    val s = diff/1000L%60L
    val ms = diff%1000L
    (d.toInt, h.toInt, m.toInt, s.toInt, ms.toInt)
  }
}

object NetworkUtil {

  private val hardwareAddress2str = (b: Array[Byte]) => b.map("%02X".format(_)).mkString("-")

  /** fe80:0:0:0:149b:1a03:645f:32c2%en1
    * 192.168.0.16
    * fe80:0:0:0:0:0:0:1%lo0
    * 127.0.2.5
    * 127.0.0.1
    */
  def hostAddresses: List[String] = {
    (for {
      n <- NetworkInterface.getNetworkInterfaces
      i <- n.getInetAddresses
    } yield i.getHostAddress).toList
  }

  /** E4-CE-8F-35-91-46 */
  def macAddresses: List[String] =
    (for {
      n <- NetworkInterface.getNetworkInterfaces
      i <- Option(n.getHardwareAddress)
    } yield hardwareAddress2str(i)).toList

  /** 192.168.0.16 */
  def localhostAddress: String = InetAddress.getLocalHost.getHostAddress

  /** hydra.local */
  def localhostName: String = InetAddress.getLocalHost.getHostName

  /** E4-CE-8F-35-91-46 */
  def localhostMac: String = {
    val ip = InetAddress.getLocalHost
    val ia = NetworkInterface.getByInetAddress(ip)
    Option(ia.getHardwareAddress).map(hardwareAddress2str).getOrElse("")
  }
}