package com.coldcore.haala.session

trait SessionHandler {
  def withSession(x: => Unit)
}
