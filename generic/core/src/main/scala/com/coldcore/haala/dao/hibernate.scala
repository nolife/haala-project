package com.coldcore.haala
package dao
package hibernate

import org.hibernate.{ObjectNotFoundException, Session, SessionFactory}
import org.hibernate.query.Query
import scala.collection.JavaConverters._
import beans.BeanProperty
import org.springframework.transaction.annotation.{Propagation, Transactional}

@Transactional (propagation = Propagation.MANDATORY)
class HibernateGenericDAO[T](entityClass: JClass[T]) extends GenericDAO[T] {
  @BeanProperty var sessionFactory: SessionFactory = _
  @BeanProperty var queryCacheable: Boolean = _
  @BeanProperty var queryCacheRegion: String = _

  private def session: Session = sessionFactory.getCurrentSession

  implicit class RList(query: Query[_]) {
    def rlist: List[T] =
      (try {
        query.list
      } catch {
        case e: ObjectNotFoundException if queryCacheable =>
          clearQueryCache()
          query.list
      }).toList.asInstanceOf[List[T]]
  }

  private def clearQueryCache() =
    if (queryCacheable) {
      val cache = sessionFactory.getCache
      Option(queryCacheRegion).isDefined ? cache.evictQueryRegion(queryCacheRegion) | cache.evictDefaultQueryRegion
    }

  override def findById(id: Long): Option[T] =
    Option(session.get(entityClass, id))

  override def findAll: List[T] =
    session.createQuery("from "+entityClass.getName).rlist

  override def findByProperty(name: String, value: Any): List[T] = {
    session.createQuery(s"from ${entityClass.getName} where $name = :p1").
      setParameter("p1", value).setCacheable(queryCacheable).setCacheRegion(queryCacheRegion).rlist
  }

  /** Prepare query for find/iterate with JPA-style positional parameters */
  private def parametrizeQuery(query: String, values: Any*): Query[_] = {
    var counter = 0 // from foo where name = ? and key = ? -> from foo where name = :p1 and key = :p2
    val pq = if (query.contains(":p0") || query.contains(":p1")) query //already positioned
             else query.map(c => if (c == '?') { counter = counter+1; ":p"+counter } else c).mkString
    val cq = session.createQuery(pq).setCacheable(queryCacheable).setCacheRegion(queryCacheRegion)
    values.zipWithIndex.map { case (v,n) => cq.setParameter("p"+(n+1), v.asInstanceOf[AnyRef]) }
    cq
  }

  override def find(query: String, values: Any*): List[T] =
    parametrizeQuery(query, values: _*).rlist

  override def findRange(from: Int, max: Int, query: String, values: Any*): List[T] =
    parametrizeQuery(query, values: _*).setFirstResult(from).setMaxResults(max).rlist

  override def findCount(query: String, values: Any*): Long =
    parametrizeQuery(query, values: _*).list.head.asInstanceOf[Long]

  override def iterate(query: String, values: Any*): Iterator[T] = 
    parametrizeQuery(query, values: _*).iterate.asScala.asInstanceOf[Iterator[T]]

  override def findX[U](query: String, values: Any*): List[U] = 
    parametrizeQuery(query, values: _*).list.toList.asInstanceOf[List[U]]

  override def delete(entity: T): Unit = session.delete(entity)

  override def saveOrUpdate(entity: T): Unit = session.saveOrUpdate(entity)

  override def executeUpdate(query: String, values: Any*): Int = parametrizeQuery(query, values: _*).executeUpdate

  override def findByProperties(properties: Map[String,Any], order: dao.Order*): List[T] = {
    val cb = session.getCriteriaBuilder
    val cq = cb.createQuery(entityClass)
    val root = cq.from(entityClass)
    val restrictions = for ((key, value) <- properties) yield cb.equal(root.get(key), value)
    val orderby = order.map {
      case Asc(key) => cb.asc(root.get(key))
      case Desc(key) => cb.desc(root.get(key))
    }
    cq.where(restrictions.toList: _*)
    cq.orderBy(orderby: _*)
    session.createQuery(cq).asInstanceOf[Query[_]].rlist
  }

  override def findUniqueByProperties(properties: Map[String,Any]): Option[T] =
    findByProperties(properties).headOption

  override def findUniqueByProperty(name: String, value: Any): Option[T] =
    findByProperty(name, value).headOption

  override def flush = session.flush()

}
