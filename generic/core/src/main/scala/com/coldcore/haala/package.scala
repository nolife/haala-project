package com.coldcore

import misc.scala.HTML._
import scala.collection.JavaConverters._
import com.coldcore.misc.scala.StringOp
import org.apache.commons.net.util.Base64
import reflect.ClassTag

package object haala {
  type JClass[A] = java.lang.Class[A]
  type JBoolean = java.lang.Boolean
  type JInteger = java.lang.Integer
  type JLong = java.lang.Long
  type JFloat = java.lang.Float
  type JDouble = java.lang.Double
  type JIterator[A] = java.util.Iterator[A]
  type JCollection[A] = java.util.Collection[A]
  type JList[A] = java.util.List[A]
  type JArrayList[A] = java.util.ArrayList[A]
  type JSet[A] = java.util.Set[A]
  type JHashSet[A] = java.util.HashSet[A]
  type JMap[A,B] = java.util.Map[A,B]
  type JHashMap[A,B] = java.util.HashMap[A,B]
  type JLinkedHashMap[A,B] = java.util.LinkedHashMap[A,B]
  type JEnumeration[A] = java.util.Enumeration[A]

  class StringX(s: String) {
    val option = Option(s).filter(""!=)

    def safe = option.getOrElse("")
    def safeInt = try { s.toInt } catch { case _: Throwable => 0 }
    def safeLong = try { s.toLong } catch { case _: Throwable => 0L }
    def bytesUTF8 = safe.getBytes("UTF-8")

    def or(x: => String) =  option.getOrElse(x)
    def on(f: => String) =  option.map(_ => f).getOrElse(s)
    def nil(x: => String) = Option(s).getOrElse(x)

    def containsAny(xs: String*): Boolean = xs.exists(s.contains)
    def matchesAny(xs: String*): Boolean = xs.exists(s.matches)
    def equalsAny(xs: String*): Boolean = xs.contains(s)

    def parseCSV() = StringOp.parseCSV(s)
    def parseCSV(sp: String) = StringOp.parseCSV(s, sp)
    def parseCSVMap() = StringOp.parseCSVMap(s)
    def parseCSVMap(sp: String) = StringOp.parseCSVMap(s, sp)

    def convert(to: String) = to match {
      case "html" => plainToHTML(s) //full HTML preserving all spaces as &nbsp;
      case "html-nlb" => plainToHTML(s, Map("<br>" -> "\n")) // "no line breaks" - removes <br> in HTML
      case "html-lb" => plainToHTML_lb(s)  // "single line break" - replaces seq of <br> with just one
      case "html-dlb" => plainToHTML_dlb(s) // "double line break" - same as "lb" but allows <br><br>
      case "html-lite" => s.replaceAll("&", "&amp;").replaceAll("<", "&lt;").replaceAll(">", "&gt;") // HTML to display in "textarea"
      case _ => s
    }

    def encodeBase64 = new String(Base64.encodeBase64(bytesUTF8), "UTF-8")
    def decodeBase64 = new String(Base64.decodeBase64(bytesUTF8), "UTF-8")

    def isTrue = { val a = safe.toLowerCase.trim; a == "true" || a == "1" || a == "yes" || a == "y" || a == "on" }
    def isFalse = !isTrue
  }

  class LongX(x: Long) {
    def isTrue = x == 1
    def isFalse = !isTrue
    def hex: String = java.lang.Long.toHexString(x)
  }

  class ArrayX[T: ClassTag](a: Array[T]) {
    def safe = Option(a).getOrElse(Array.empty[T])
  }

  class ListX[T: ClassTag](l: List[T]) {
    def safe = Option(l).getOrElse(List.empty[T])
  }

  class SeqX[T: ClassTag](s: Seq[T]) {
    def safe = Option(s).getOrElse(Seq.empty[T])
    def random = s((math.random*s.size.toDouble).toInt)
  }

  implicit def toStringX(s: String) = new StringX(s)
  implicit def toIntX(x: Int) = new LongX(x)
  implicit def toLongX(x: Long) = new LongX(x)
  implicit def toJIntX(x: JInteger) = new LongX(x.toInt)
  implicit def toJLongX(x: JLong) = new LongX(x.toLong)
  implicit def toArrayX[T: ClassTag](a: Array[T]) = new ArrayX(a)
  implicit def toListX[T: ClassTag](l: List[T]) = new ListX(l)
  implicit def toSeqX[T: ClassTag](s: Seq[T]) = new SeqX(s)

  implicit def javaCollection2Scala[T](o: JCollection[T]) = o.asScala
  implicit def javaList2Scala[T](o: JList[T]) = o.asScala
  implicit def javaMap2Scala[T,U](o: JMap[T,U]) = o.asScala
  implicit def javaEnumeration2Scala[T](o: JEnumeration[T]) = o.asScala

  def withOpen[R <: { def close(): Unit }, T](r: R)(f: R => T) =
    try { f(r) } finally { r.close }

  /**
   * new File("/") |> { f => new File(f, "Users") } |> { case f if f.isDirectory => "dir"; case _ => "file" }
   * val x = new BasicDataSource |< { ds =>  ds.setUsername(getProperty("User")); ds.setPassword(getProperty("Password")) }
   */
  implicit class Piper[A](a: A) {
    def |>[B](f: A => B): B = f(a)
    def |<(f: A => Any): A = { f(a); a }
  }

  /** bool ? a | b */
  implicit class TernaryOp(cond: Boolean) {
    def ?[A](thenExp: => A) = new IfThenElse[A](thenExp)
    class IfThenElse[+A](thenExp: => A) {
      def |[U >: A](elseExp: => U): U = if (cond) thenExp else elseExp
    }
  }

}
