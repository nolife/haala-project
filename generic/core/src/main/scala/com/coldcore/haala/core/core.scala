package com.coldcore.haala
package core

import org.springframework.context.{ApplicationContext, ApplicationContextAware, ResourceLoaderAware}

import beans.BeanProperty
import java.util.{Locale, TimeZone}

import org.springframework.core.io.ResourceLoader
import org.slf4j.{Logger, LoggerFactory}
import java.io.{File => JFile}
import java.util.concurrent.atomic.AtomicLong

import com.coldcore.misc.scala.StringOp._

class ResourceLoaderHolder extends ResourceLoaderAware {
  @BeanProperty var resourceLoader: ResourceLoader = _
  def get = resourceLoader
}

object ApplicationContextHolder {
  var applicationContext: ApplicationContext = _ //static context for specially created classes (e.g. by EhCache)
}

class ApplicationContextHolder extends ApplicationContextAware {
  import ApplicationContextHolder._
  def get = applicationContext
  override def setApplicationContext(ac: ApplicationContext) = applicationContext = ac
}

trait Log {
  def info(msg: String)
  def warn(msg: String)
  def debug(msg: String)
  def error(msg: String, e: Throwable = null)
}

class SLF4J extends Log {
  private var log: Logger = _
  private var logName: String = _

  init("com.coldcore.haala.CORE")
  private def init(x: String) { logName = x; log = LoggerFactory.getLogger(x) }

  /** Change in application-context to "com.coldcore.haala.[pack-name]" */
  def setName(x: String) = init(x)

  override def info(msg: String) = log.info(msg)
  override def warn(msg: String) = log.warn(msg)
  override def debug(msg: String) = log.debug(msg)
  override def error(msg: String, e: Throwable = null) = log.error(msg, e)
}

/** Named log delegate: lazy val LOG = new NamedLog("cms-facade", log) */
class NamedLog(name: String, log: Log) extends SLF4J {
  override def info(msg: String) = log.info("["+name+"] "+msg)
  override def warn(msg: String) = log.warn("["+name+"] "+msg)
  override def debug(msg: String) = log.debug("["+name+"] "+msg)
  override def error(msg: String, e: Throwable = null) =
    log.error("["+name+"] "+msg+
      Option(e).filter(_.isInstanceOf[NoTraceException]).map("\n"+_.getMessage).getOrElse(""),
      Option(e).filterNot(_.isInstanceOf[NoTraceException]).orNull)
}

class Initializer {
  Locale.setDefault(Locale.ENGLISH)
  TimeZone.setDefault(TimeZone.getTimeZone("UTC")) //always UTC for midnight conversion (matches DB encoding for Date columns)
}

class Statistics {
  val started = Timestamp()
  val requests = new AtomicLong
}

object Constants {
  val SITE_PUBLIC = "pub"
  val SITE_SYSTEM = "sys"
  val SITE_EXTRAS = "jb"

  val SOLR_TYPE_LABEL = 1
  val SOLR_TYPE_USER = 2
  val SOLR_TYPE_ABYSS = 4
  val SOLR_TYPE_FILE = 5

  val SOLR_PREFIX_LABEL = "ol"
  val SOLR_PREFIX_USER = "ou"
  val SOLR_PREFIX_ABYSS = "oa"
  val SOLR_PREFIX_FILE = "of"

  val UTF8 = "UTF-8"

  val MESSAGE_TYPE_FEEDBACK = 1
  val MESSAGE_TYPE_ERROR = 2
  val MESSAGE_TYPE_PAYMENT = 3
}

class CoreException(m: String, t: Throwable) extends RuntimeException(m, t) {
  def this() = this(null, null)
  def this(m: String) = this(m, null)
  def this(t: Throwable) = this(null, t)
}

/** Exception that should be caught but not reported */
class WeakCoreException(t: Throwable) extends CoreException(null, t) {
  def this() = this(null)
}

/** Exception without stack trace to avoid reporting not relevant info */
class NoTraceException(m: String) extends CoreException(m, null)

case class Turing(value: String, path: VirtualPath, var attempt: Int = 0) {
  def attempted: Int = { attempt += 1; attempt }
}

case class SiteState(currentDomain: String, currentDomainId: Long, currentDomainSite: SiteChain,
                     currentDomainType: String, currentDomainStyle: String,
                     var currentSite: SiteChain = SiteChain.empty,
                     var defaultSite: SiteChain = SiteChain.empty,
                     var rootSite: String = "")

object SiteChainMap {
  def apply(rootSiteChain: Map[String,SiteChain], mineSiteChain: Map[String,SiteChain],
            xtraSiteChain: Map[String,SiteChain], rootSiteLinks: Map[String,String]) =
    new SiteChainMap(rootSiteChain, mineSiteChain, xtraSiteChain, rootSiteLinks)
}

class SiteChainMap(val rootSiteChain: Map[String,SiteChain], val mineSiteChain: Map[String,SiteChain],
                   val xtraSiteChain: Map[String,SiteChain], val rootSiteLinks: Map[String,String]) {
  /** Find a site 'key' by its 'chain' in any of the chain maps or return an empty string.
   *  xx1.mw1.xx2.mw2 -> mw2
   */
  def keyByChain(chain: SiteChain): String = {
    val f = (m: Map[String,SiteChain]) => m.find(_._2 == chain).map(_._1)
    f(xtraSiteChain) orElse f(mineSiteChain) orElse f(rootSiteChain) getOrElse ""
  }

  /** Find a site 'chain' by its 'key' in any of the chain maps or return an empty string.
   *  mw2 -> xx1.mw1.xx2.mw2
   */
  def chainByKey(key: String): SiteChain =
    xtraSiteChain.get(key) orElse mineSiteChain.get(key) orElse rootSiteChain.get(key) getOrElse SiteChain.empty

  /** Check if a site 'key' has a 'chain' associated with it. */
  private def assertChainByKey(m: Map[String,SiteChain])(key: String) =
    m.getOrElse(key, throw new CoreException("Invalid site: "+key))

  /** Check if a site 'key' exists in an extra chain map (mw1b/mw2j). */
  def assertXtraChainByKey = assertChainByKey(xtraSiteChain) _

  /** Check if a site 'key' exists in a root chain map (xx1/xx2). */
  def assertRootChainByKey = assertChainByKey(rootSiteChain) _

  /** Check if a site 'key' exists in a mane chain map (xx1/xx2). */
  def assertMineChainByKey = assertChainByKey(mineSiteChain) _
}

/** Delegate to static methods (simplifies test mocking) */
class StaticBlock {
  def httpGet(url: String, rh: List[(String,String)] = Nil) = HttpUtil.httpGet(url, rh)
  def httpPost(url: String, content: String, rh: List[(String,String)] = Nil) = HttpUtil.httpPost(url, content, rh)
  def httpPost(url: String, rp: Map[String,String], rh: List[(String,String)]) = HttpUtil.httpPost(url, rp, rh)
  def securityContext = SecurityUtil.context
  def loadXmlFile(file: JFile) = XmlUtil.loadFile(file)
  def readFile(file: JFile) = FileUtil.read(file)
  def fileExists(file: JFile) = FileUtil.exists(file)
  def localhostName = NetworkUtil.localhostName
}

/** Operates on CSVMap string (eg. "foo=1;bar=2;") */
class Meta(data: String, dataSp: String = ";") {
  private val m = new scala.collection.mutable.LinkedHashMap[String,String]
  m.clear
  m ++= parseCSVMap(data.safe, dataSp)

  def apply(key: String): String = get(key).get // meta("foo") -> "bar"
  def apply(key: String, default: => String): String = get(key).getOrElse(default) // ...

  private def replace(x: String) = x.replaceAll("=", "-").replaceAll(";", ",")
  def put(k: String, v: String) = m.put(replace(k), replace(v))
  def put(k: String, csv: MetaCSV): Unit = put(k, csv.serialize.dropRight(1))
  def get = (replace _ andThen m.get)(_)
  def csv = get(_: String).map(MetaCSV(_, ",")).getOrElse(MetaCSV(""))
  def rem = (replace _ andThen m.remove)(_)
  def merge(x: Meta) = x.imap.foreach(m +=)
  def imap = m
  def flag(b: Boolean, k: String, v: => String) = if (b) put(k, v) else rem(k)

  def serialize: String = m.map { case (k,v) => s"$k=$v;"}.mkString

  // alias
  def +(kv: (String, Any)) = { put(kv._1, kv._2.toString); this }
  def +(kv: (String, Any)*) = { kv.foreach(x => put(x._1, x._2.toString)); this }
  def +(key: String, value: Any) = { put(key, value.toString); this }
  def +(key: String, value: MetaCSV) = { put(key, value); this }
  def +(x: Meta) = { merge(x); this }
  def -(key: String) = { rem(key); this }
  def -(key: String*) = { key.foreach(rem); this }
  def ?(b: Boolean, k: String, v: => String) = { flag(b, k, v); this }
}

object Meta {
  def apply(data: String, dataSp: String = ";") = new Meta(data, dataSp)
}

/** Operates on CSV string (eg. "foo;bar;") */
class MetaCSV(data: String, dataSp: String = ";") {
  private val m = new scala.collection.mutable.LinkedHashSet[String]
  m.clear
  m ++= parseCSV(data.safe, dataSp)

  def apply(key: String): String = get(key).get // meta("foo") -> "foo"

  private def replace(x: String) = x.replaceAll(";", ",")
  def put = (replace _ andThen m.add)(_)
  def get = (replace _ andThen { k => m.contains(k) ? Some(k) | None })(_)
  def rem = (replace _ andThen m.remove)(_)
  def merge(x: MetaCSV) = x.imap.foreach(m +=)
  def imap = m
  def flag(b: Boolean, k: String) = if (b) put(k) else rem(k)

  def serialize: String = m.map(k => s"$k;").mkString

  // alias
  def +(key: String) = { put(key); this }
  def +(key: String*) = { key.foreach(x => put(x)); this }
  def +(x: MetaCSV) = { merge(x); this }
  def -(key: String) = { rem(key); this }
  def -(key: String*) = { key.foreach(rem); this }
  def ?(b: Boolean, k: String) = { flag(b, k); this }
}

object MetaCSV {
  def apply(data: String, dataSp: String = ";") = new MetaCSV(data, dataSp)
}

/** Parsed part of site *setting*
  *  xx2=xx1.xx2:ru|rs -> key: xx2, chain: xx1.xx2, locale: ru, locales: [ru,rs]
  */
class ParsedSiteSetting(x: String) {
  private val a = x.parseCSV("=") // xx2 (key) and xx1.xx2:ru|rs
  private val b = a.last.parseCSV(":") // xx1.xx2 (chain) and ru|rs
  private val c = b.last.parseCSV("|") // ru (locale) and rs   [ru,rs] (locales)
  val (key, site, locale, locales) = (a.head, SiteChain(b.head), c.head, c)

  if (key != site.key) throw new CoreException("Invalid mapping: "+x)
}

object ParsedSiteSetting {
  def apply(x: String) = new ParsedSiteSetting(x)
}

/** Site chain (or key) wrapper
  *  xx1   xx1.xx2   .xx1.xx2
  */
class SiteChain(val value: String) {
  private val prv: Boolean = value.startsWith(".")
  val keys: List[String] = value.split("\\.").filter(""!=).toList // xx1, xx2
  val bits: List[String] = if (prv) keys else Constants.SITE_PUBLIC :: keys // pub, xx1, xx2
  val key: String = if (keys.nonEmpty) keys.last else "" // xx2 - matches mapping key
  override def toString: String = value
  override def equals(obj: Any): Boolean = value == obj.toString
  override def hashCode(): Int = value.hashCode
  val isEmpty: Boolean = value == ""
  val nonEmpty: Boolean = !isEmpty
}

object SiteChain {
  def apply(value: String) = new SiteChain(value)
  def apply(keys: Seq[String]) = new SiteChain(keys.mkString("."))
  def empty = SiteChain("")
  def prv(value: String) = SiteChain((if (value.nonEmpty && !value.startsWith(".")) "." else "")+value)  // .xx1.xx2
  def pub = prv(Constants.SITE_PUBLIC) // .pub
  def sys = prv(Constants.SITE_SYSTEM) // .sys
  def key(value: String) = if (SiteChain(value).keys.size == 1) prv(value) else throw new CoreException("Invalid key: "+value) // .xx1

  object Implicits { // Implicits._ used in tests, code should use explicit SiteChain to avoid ambiguity
    implicit def toSiteChain(value: String): SiteChain = SiteChain(value)
    implicit def fromSiteChain(chain: SiteChain): String = chain.value
  }
}

class VirtualPath(val path: String) {
  if (path.startsWith("/") || path.isEmpty) throw new CoreException("Invalid path: "+path)
  val folder: String = path.split("/").dropRight(1).mkString("/") // foo/bar/abc.txt -> foo/bar
  val filename: String = path.split("/").last or path // foo/bar/abc.txt -> abc.txt
  override def toString: String = path
  override def equals(obj: Any): Boolean = path == obj.toString
  override def hashCode(): Int = path.hashCode
}

object VirtualPath {
  def apply(path: String) = new VirtualPath(path)
  def apply(folder: String, filename: String) = new VirtualPath((folder on (folder+"/")) + filename)

  object Implicits { // Implicits._ used in tests, code should use explicit VirtualPath to avoid ambiguity
    implicit def toVirtualPath(path: String): VirtualPath = VirtualPath(path)
    implicit def fromVirtualPath(path: VirtualPath): String = path.path
  }
}

/** URL mapping configuration (UrlMapping setting in JSON format) */
class UrlMapping {
  @BeanProperty var mapping: Array[Mapping] = Array.empty
  @BeanProperty var unorderPrefix: String = _

  class Mapping {
    @BeanProperty var pattern: String = _
    @BeanProperty var handler: String = _
    @BeanProperty var params: String = _
    @BeanProperty var handlerConf: HandlerConf = _

    def newHandlerConf: HandlerConf = {
      handlerConf = new HandlerConf
      handlerConf
    }
  }

  class HandlerConf {
    @BeanProperty var index: String = _
    @BeanProperty var mod: String = _
    @BeanProperty var facade: String = _
    @BeanProperty var pageName: String = _
    @BeanProperty var downtime: String = _
    @BeanProperty var options: String = _
  }
}
