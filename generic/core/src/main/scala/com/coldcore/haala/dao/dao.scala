package com.coldcore.haala
package dao

trait GenericDAO[T] {
  /** Finds the object based on an ID. */
  def findById(id: Long): Option[T]

  /** Find all objects of a type T. */
  def findAll: List[T]

  /** Find collection of T objects where the property has a specific value. There is no limit on
   *  the number of results returned.
   */
  def findByProperty(name: String, value: Any): List[T]

  /** Find a collection of T objects based on the given query. */
  def find(query: String, values: Any*): List[T]

  def findRange(from: Int, max: Int, query: String, values: Any*): List[T]

  def findCount(query: String, values: Any*): Long

  def iterate(query: String, values: Any*): Iterator[T]

  /** Find a collection of U objects based on the given query. */
  def findX[U](query: String, values: Any*): List[U]

  /** Deletes an object. */
  def delete(entity: T): Unit

  /** Saves or updates an object. */
  def saveOrUpdate(entity: T): Unit

  def executeUpdate(query: String, values: Any*): Int

  /** Find collection of T objects where the property has a specific values. There is no limit on
   *  the number of results returned.
   */
  def findByProperties(properties: Map[String,Any], orders: Order*): List[T]

  /** Find unique T object where the property has specific values. Only 1 result will be returned
   *  from this method even if more than 1 match. You should only use this method if you know that
   *  only 1 result will be returned (i.e. there are equivalent unique constraints in database).
   */
  def findUniqueByProperties(properties: Map[String, Any]): Option[T]

  /** Find unique object where the property has a specific value. */
  def findUniqueByProperty(name: String, value: Any): Option[T]

  /** Flush. */
  def flush

}

sealed trait Order
case class Asc(key: String) extends Order
case class Desc(key: String) extends Order
