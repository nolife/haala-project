package com.coldcore.haala

import org.scalatest.{FlatSpec, Matchers}
import org.junit.runner.RunWith
import org.scalatest.junit.JUnitRunner
import org.scalatest.mockito.MockitoSugar

@RunWith(classOf[JUnitRunner])
class PackageSpec extends FlatSpec with MockitoSugar with Matchers {

  "StringX" should "operate on string" in {
    val n: String  = null

    n   .safe     shouldBe ""
    "12".safe     shouldBe "12"
    "12".safeInt  shouldBe 12
    "ab".safeInt  shouldBe 0
    "12".safeLong shouldBe 12
    "ab".safeLong shouldBe 0

    n   .option   shouldBe None
    ""  .option   shouldBe None
    "12".option   shouldBe Some("12")

    "" or n or "" or "b" or "c"                        shouldBe "b"
    "" or "b" or { throw new Exception("evaluated") }  shouldBe "b"

    "" nil n nil n nil "b" nil "c"                     shouldBe ""
    n nil n nil "b" nil "c"                            shouldBe "b"
    n nil "b" nil { throw new Exception("evaluated") } shouldBe "b"

    "b" on "c"        shouldBe "c"
    "b" on "c" on "d" shouldBe "d"
    ""  on "c"        shouldBe ""
    n   on "c"        shouldBe n
    "b" on "" on { throw new Exception("evaluated") } shouldBe ""

    "aa bb cc dd ee".containsAny("aa", "ff")       shouldBe true
    "aa bb cc dd ee".containsAny("bb", "dd", "ee") shouldBe true
    "aa bb cc dd ee".containsAny("ff", "hh", "gg") shouldBe false

    "abc".matchesAny("\\D+", "efg")     shouldBe true
    "abc".matchesAny("[abc]", "[abc]+") shouldBe true
    "abc".matchesAny("[abc]", "123")    shouldBe false

    "abc".equalsAny("abc", "123") shouldBe true
    "abc".equalsAny("ab", "bc")   shouldBe false

    "a;b, c".parseCSV      shouldBe "a" :: "b" :: "c" :: Nil
    "a;b, c".parseCSV(",") shouldBe "a;b" :: "c" :: Nil

    "a=1;b =2, c = 3".parseCSVMap      shouldBe Map("a" -> "1", "b" -> "2", "c" -> "3")
    "a=1;b =2, c = 3".parseCSVMap(",") shouldBe Map("a" -> "1;b =2", "c" -> "3")

    "abc" .encodeBase64 shouldBe "YWJj"
    "YWJj".decodeBase64 shouldBe "abc"
    ""    .encodeBase64 shouldBe ""
    ""    .decodeBase64 shouldBe ""
  }

  "SeqX" should "operate on sequence" in {
    val n: Seq[Int] = null
    
    n.safe          shouldBe Seq.empty
    Seq(1,2,3).safe shouldBe Seq(1,2,3)

    Seq(1,2,3).contains(Seq(1,2,3).random) shouldBe true
  }

  "listX" should "operate on list" in {
    val n: List[Int] = null

    n.safe           shouldBe Nil
    List(1,2,3).safe shouldBe List(1,2,3)
  }

  "ArrayX" should "operate on array" in {
    val n: Array[Int] = null

    n.safe            shouldBe Array.empty
    Array(1,2,3).safe shouldBe Array(1,2,3)
  }

  "withOpen" should "close resource when finished" in {
    val resource = new {
      var open = true
      def close() { open = false }
      def dummy(s: String) { }
    }
    withOpen(resource) { r => r.dummy("123") }
    resource.open shouldBe false
  }

  "Piper" should "pipe object operations" in {
    class Dummy { var (name, email, value) = ("", "", ""); var foo = new Foo }
    class Foo { var key = "" }

    // val a = new Class |< { init } |< { init }
    val a = new Dummy |< { x => x.name = "myName"; x.email = "myEmail" } |< { x => x.value = x.name+"/"+x.email }
    a.name  shouldBe "myName"
    a.email shouldBe "myEmail"
    a.value shouldBe "myName/myEmail"

    // b |> { transform } |> { transform }
    val b = new Dummy
    b |> { x => x.name = "myName"; x.email = "myEmail"; (x.email, x.foo) } |> { case (email, foo) => foo.key = "myKey/"+email }
    b.name    shouldBe "myName"
    b.email   shouldBe "myEmail"
    b.foo.key shouldBe "myKey/myEmail"
  }

  "TernaryOp" should "provide if else construct" in {
    // bool ? a : b  ->  bool ? a | b
    true  ? "a" | "b"                                  shouldBe "a"
    true  ? "a" | { throw new Exception("evaluated") } shouldBe "a"
    false ? "a" | (true ? "b" | "c")                   shouldBe "b"

    val x = Seq(1,2,3)
    (x.size == 1) ? "1" | ((x.size == 2) ? "2" | "3")  shouldBe "3"
    (x.size >= 1) ? "1" | ((x.size >= 2) ? "2" | "3")  shouldBe "1"
  }

}
