package com.coldcore.haala
package core

import org.junit.runner.RunWith
import org.scalatest.junit.JUnitRunner
import org.scalatest.{FlatSpec, Matchers, WordSpec}
import org.scalatest.mockito.MockitoSugar

@RunWith(classOf[JUnitRunner])
class MetaSpec extends WordSpec with MockitoSugar with Matchers {

  "serialize" should {
    "produce successful result" in {
      val meta = Meta("foo=1;bar=2;")
      meta.serialize shouldBe "foo=1;bar=2;"
    }

    "produce successful result (separator check))" in {
      val meta = Meta("foo=1,bar=2", ",;")
      meta.serialize shouldBe "foo=1;bar=2;"
    }
  }

  "put" should {
    "add new entry" in {
      val meta = Meta("foo=1;bar=2;")
      meta.put("dar", "3"); meta.serialize shouldBe "foo=1;bar=2;dar=3;"
    }

    "chenge existing entry" in {
      val meta = Meta("foo=1;bar=2;")
      meta.put("bar", "3"); meta.serialize shouldBe "foo=1;bar=3;"
    }

    "add new entry (escaped)" in {
      val meta = Meta("foo=1;bar=2;")
      meta.put("d=a;r", "3=2;1"); meta.serialize shouldBe "foo=1;bar=2;d-a,r=3-2,1;"
    }

    "add new CSV entry" in {
      val csv = MetaCSV("a;b;c;")
      val meta = Meta("foo=1;bar=2;")
      meta.put("dar", csv); meta.serialize shouldBe "foo=1;bar=2;dar=a,b,c;"
    }

    "add new entry (alias)" in {
      val meta = Meta("foo=1;")
      meta + ("bar", "2"); meta.serialize shouldBe "foo=1;bar=2;"
      meta + ("abc" -> "3"); meta.serialize shouldBe "foo=1;bar=2;abc=3;"
      meta + ("xyz" -> "4", "foo" -> "5"); meta.serialize shouldBe "foo=5;bar=2;abc=3;xyz=4;"
    }

    "add new CSV entry (alias)" in {
      val csv = MetaCSV("a;b;c;")
      val meta = Meta("foo=1;bar=2;")
      meta + ("dar", csv); meta.serialize shouldBe "foo=1;bar=2;dar=a,b,c;"
    }

  }

  "rem" should {
    "remove entry" in {
      val meta = Meta("foo=1;bar=2;")
      meta.rem("bar"); meta.serialize shouldBe "foo=1;"
    }
    "remove entry (alias)" in {
      val meta = Meta("foo=1;bar=2;abc=3;xyz=4;")
      meta - "bar"; meta.serialize shouldBe "foo=1;abc=3;xyz=4;"
      meta - ("bar", "abc", "xyz"); meta.serialize shouldBe "foo=1;"
    }
  }

  "get" should {
    "return value" in {
      val meta = Meta("foo=1;bar=2;")
      meta.get("bar") shouldBe Some("2")

      meta("bar") shouldBe "2"
      meta("abc", "3") shouldBe "3"
    }
    "return CSV value" in {
      val meta = Meta("foo=1;bar=2;dar=a,b,c;")
      meta.csv("dar").serialize shouldBe "a;b;c;"
    }
    "return CSV value even if ascent" in {
      val meta = Meta("foo=1;bar=2;")
      meta.csv("dar").serialize shouldBe ""
    }
  }

  "merge" should {
    "merge with meta" in {
      val meta = Meta("foo=1;bar=2;")
      meta.merge(Meta("foo=A;car=B;"))
      meta.get("bar") shouldBe Some("2")
      meta.get("foo") shouldBe Some("A")
      meta.get("car") shouldBe Some("B")
      meta.imap.size shouldBe 3
    }

    "merge with meta (alias)" in {
      val meta = Meta("foo=1;bar=2;")
      meta + Meta("foo=A;car=B;")
      meta.get("bar") shouldBe Some("2")
      meta.get("foo") shouldBe Some("A")
      meta.get("car") shouldBe Some("B")
      meta.imap.size shouldBe 3
    }
  }

  "flag" should {
    "add entry" in {
      val meta = Meta("foo=1;bar=2;")
      meta.flag(true, "dar", "3"); meta.serialize shouldBe "foo=1;bar=2;dar=3;"
      meta.flag(true, "foo", "4"); meta.serialize shouldBe "foo=4;bar=2;dar=3;"
    }

    "add entry (alias)" in {
      val meta = Meta("foo=1;bar=2;")
      meta.?(true, "dar", "3"); meta.serialize shouldBe "foo=1;bar=2;dar=3;"
      meta.?(true, "foo", "4"); meta.serialize shouldBe "foo=4;bar=2;dar=3;"
    }

    "remove entry" in {
      val meta = Meta("foo=1;bar=2;")
      meta.flag(false, "bar", "0"); meta.serialize shouldBe "foo=1;"
      meta.flag(false, "abc", "4"); meta.serialize shouldBe "foo=1;"
      meta.flag(false, "abc", throw new Exception("evaluated")); meta.serialize shouldBe "foo=1;"
    }

    "remove entry (alias)" in {
      val meta = Meta("foo=1;bar=2;")
      meta.?(false, "bar", "0"); meta.serialize shouldBe "foo=1;"
      meta.?(false, "abc", "4"); meta.serialize shouldBe "foo=1;"
      meta.?(false, "abc", throw new Exception("evaluated")); meta.serialize shouldBe "foo=1;"
    }
  }

}

@RunWith(classOf[JUnitRunner])
class MetaCsvSpec extends WordSpec with MockitoSugar with Matchers {

  "serialize" should {
    "produce successful result" in {
      val meta = MetaCSV("foo;bar;")
      meta.serialize shouldBe "foo;bar;"
    }

    "produce successful result (separator check))" in {
      val meta = MetaCSV("foo,bar", ",;")
      meta.serialize shouldBe "foo;bar;"
    }
  }

  "put" should {
    "add new entry" in {
      val meta = MetaCSV("foo;bar;")
      meta.put("dar"); meta.serialize shouldBe "foo;bar;dar;"
    }

    "change existing entry" in {
      val meta = MetaCSV("foo;bar;")
      meta.put("bar"); meta.serialize shouldBe "foo;bar;"
    }

    "add new entry (escaped)" in {
      val meta = MetaCSV("foo;bar;")
      meta.put("d=a;r"); meta.serialize shouldBe "foo;bar;d=a,r;"
    }

    "add new entry (alias)" in {
      val meta = MetaCSV("foo;")
      meta + "bar"; meta.serialize shouldBe "foo;bar;"
      meta + ("foo", "abc", "xyz"); meta.serialize shouldBe "foo;bar;abc;xyz;"
    }
  }

  "rem" should {
    "remove entry" in {
      val meta = MetaCSV("foo;bar;")
      meta.rem("bar"); meta.serialize shouldBe "foo;"
    }

    "remove entry (alias)" in {
      val meta = MetaCSV("foo;bar;abc;xyz;")
      meta - "bar"; meta.serialize shouldBe "foo;abc;xyz;"
      meta - ("bar", "abc", "xyz"); meta.serialize shouldBe "foo;"
    }
  }

  "get" should {
    "return value" in {
      val meta = MetaCSV("foo;bar;")
      meta.get("bar") shouldBe Some("bar")

      meta("bar") shouldBe "bar"
    }
  }

  "merge" should {
    "merge with meta" in {
      val meta = MetaCSV("foo;bar;")
      meta.merge(MetaCSV("foo;car;"))
      meta.get("bar") shouldBe Some("bar")
      meta.get("foo") shouldBe Some("foo")
      meta.get("car") shouldBe Some("car")
      meta.imap.size shouldBe 3
    }

    "merge with meta (alias)" in {
      val meta = MetaCSV("foo;bar;")
      meta + MetaCSV("foo;car;")
      meta.get("bar") shouldBe Some("bar")
      meta.get("foo") shouldBe Some("foo")
      meta.get("car") shouldBe Some("car")
      meta.imap.size shouldBe 3
    }
  }

  "flag" should {
    "add entry" in {
      val meta = MetaCSV("foo;bar;")
      meta.flag(true, "dar"); meta.serialize shouldBe "foo;bar;dar;"
      meta.flag(true, "foo"); meta.serialize shouldBe "foo;bar;dar;"
    }

    "remove entry" in {
      val meta = MetaCSV("foo;bar;dar;")
      meta.flag(false, "dar"); meta.serialize shouldBe "foo;bar;"
      meta.flag(false, "foo"); meta.serialize shouldBe "bar;"
      meta.flag(false, "abc"); meta.serialize shouldBe "bar;"
    }
  }

}

@RunWith(classOf[JUnitRunner])
class SiteChainSpec extends FlatSpec with MockitoSugar with Matchers {

  it should "parse value" in {
    case class SiteChainX(key: String, keys: List[String], bits: List[String]) {
      def sameAs(x: SiteChain) { x.keys shouldBe keys; x.bits shouldBe bits }
    }
    
    SiteChainX("xx2", "xx1" :: "xx2" :: Nil, "pub" :: "xx1" :: "xx2" :: Nil) sameAs SiteChain("xx1.xx2")
    SiteChainX("xx2", "xx1" :: "xx2" :: Nil, "xx1" :: "xx2" :: Nil) sameAs SiteChain(".xx1.xx2")

    SiteChainX("xx2", "xx1" :: "xx2" :: Nil, "xx1" :: "xx2" :: Nil) sameAs SiteChain.prv("xx1.xx2")
    SiteChainX("xx2", "xx1" :: "xx2" :: Nil, "xx1" :: "xx2" :: Nil) sameAs SiteChain.prv(".xx1.xx2")

    SiteChainX("xx1", "xx1" :: Nil, "pub" :: "xx1" :: Nil) sameAs SiteChain("xx1")
    SiteChainX("xx1", "xx1" :: Nil, "xx1" :: Nil) sameAs SiteChain(".xx1")

    SiteChainX("", Nil, "pub" :: Nil) sameAs SiteChain("")
    SiteChainX("", Nil, "pub" :: Nil) sameAs SiteChain.empty
    SiteChainX("pub", "pub" :: Nil, "pub" :: Nil) sameAs SiteChain.pub
    SiteChainX("sys", "sys" :: Nil, "sys" :: Nil) sameAs SiteChain.sys

    SiteChainX("xx1", "xx1" :: Nil, "xx1" :: Nil) sameAs SiteChain.key("xx1")
    SiteChainX("xx1", "xx1" :: Nil, "xx1" :: Nil) sameAs SiteChain.key(".xx1")
    the [CoreException] thrownBy SiteChain.key("xx1.xx2") should have message "Invalid key: xx1.xx2"
  }

  it should "provide value as toString and equal" in {
    SiteChain("xx1.xx2").value shouldBe "xx1.xx2"
    SiteChain("xx1.xx2").toString shouldBe "xx1.xx2"

    SiteChain("xx1.xx2") shouldEqual "xx1.xx2"
    SiteChain("xx1.xx2") shouldBe "xx1.xx2"
    SiteChain("xx1.xx2") shouldEqual SiteChain("xx1.xx2")
    SiteChain("xx1.xx2") shouldBe SiteChain("xx1.xx2")
  }

}

@RunWith(classOf[JUnitRunner])
class VirtualPathSpec extends FlatSpec with MockitoSugar with Matchers {

  it should "split path into folder and filename" in {
    VirtualPath("foo/bar/abc.txt").folder   shouldBe "foo/bar"
    VirtualPath("foo/bar/abc.txt").filename shouldBe "abc.txt"
    VirtualPath(        "abc.txt").folder   shouldBe ""
    VirtualPath(        "abc.txt").filename shouldBe "abc.txt"

    the [CoreException] thrownBy VirtualPath("/bar/abc.txt") should have message "Invalid path: /bar/abc.txt"
    the [CoreException] thrownBy VirtualPath("") should have message "Invalid path: "
  }

  it should "combine folder and filename into path" in {
    VirtualPath("foo/bar", "abc.txt").path shouldBe "foo/bar/abc.txt"
    VirtualPath("",        "abc.txt").path shouldBe "abc.txt"

    the [CoreException] thrownBy VirtualPath("/bar", "abc.txt") should have message "Invalid path: /bar/abc.txt"
    the [CoreException] thrownBy VirtualPath("", "") should have message "Invalid path: "
  }

}

