package com.coldcore.haala
package core

import java.util.Calendar
import org.scalatest.{BeforeAndAfter, FlatSpec, Matchers}
import org.junit.runner.RunWith
import org.scalatest.junit.JUnitRunner
import core.SiteChain.Implicits._

@RunWith(classOf[JUnitRunner])
class SiteUtilSpec extends FlatSpec with Matchers {
  import SiteUtil._

  "parseSiteSetting" should "parse configuration text" in {
    case class ParsedSiteSettingX(key: String, chain: String, locale: String, locales: List[String]) {
      def sameAs(x: ParsedSiteSetting) {
        x.key shouldBe key; x.site shouldBe chain; x.locale shouldBe locale; x.locales shouldBe locales
      }
    }
    val xs = parseSiteSetting("xx1=xx1:en, xx2=xx1.xx2:ru|rs")
    ParsedSiteSettingX("xx1", "xx1", "en", "en" :: Nil)             sameAs xs.head
    ParsedSiteSettingX("xx2", "xx1.xx2", "ru", "ru" :: "rs" :: Nil) sameAs xs.last
  }

  "siteLocale" should "extract locale by chain" in {
    siteLocale("xx1.xx2", "xx1=xx1:en, xx2=xx1.xx2:ru|rs") shouldBe "ru"
    siteLocale("uk1.uk2", "xx1=xx1:en, xx2=xx1.xx2:ru|rs") shouldBe "en"
  }

  "suffixSite" should "suffix chain" in {
    suffixSite("xx1", "b")             shouldBe "xx1b"
    suffixSite("xx1.ph1.zx1.mw1", "j") shouldBe "xx1j.ph1j.zx1j.mw1j"
  }

  "stripRootSites" should "strip chain of redundant data" in {
    stripRootSites(Map("xx1" -> "xx1:foo:bar", "xx2" -> "xx1.xx2:foo:bar")) shouldBe Map("xx1" -> "xx1", "xx2" -> "xx1.xx2")
  }

  "rootSiteKey" should "return root key by chain" in {
    rootSiteKey("xx1.ph1.zx1.mw1",                 Map("xx1" -> "xx1", "xx2" -> "xx1.xx2")) shouldBe "xx1"
    rootSiteKey("xx1.ph1.zx1.mw1.xx2.ph2.zx2.mw2", Map("xx1" -> "xx1", "xx2" -> "xx1.xx2")) shouldBe "xx2"
    rootSiteKey("xx1.ph1.zx1.mw1",                 Map("xx1" -> "xx1:foo:bar", "xx2" -> "xx1.xx2:foo:bar")) shouldBe "xx1"
    rootSiteKey("xx1.ph1.zx1.mw1.xx2.ph2.zx2.mw2", Map("xx1" -> "xx1:foo:bar", "xx2" -> "xx1.xx2:foo:bar")) shouldBe "xx2"
    rootSiteKey("xx1.ph1.zx1.mw1",                 "xx1=xx1:foo:bar, xx2=xx1.xx2:foo:bar") shouldBe "xx1"
    rootSiteKey("xx1.ph1.zx1.mw1.xx2.ph2.zx2.mw2", "xx1=xx1:foo:bar, xx2=xx1.xx2:foo:bar") shouldBe "xx2"
  }

  "mixRootPureSites" should "mix root chain with pure chain" in {
    mixRootPureSites("xx1",     "ph.zx.mw") shouldBe "xx1.ph1.zx1.mw1"
    mixRootPureSites("xx1.xx2", "ph.zx.mw") shouldBe "xx1.ph1.zx1.mw1.xx2.ph2.zx2.mw2"
  }

  "createChainMap" should "create map with chains (simple pure chain)" in {
    val result = createChainMap("mw", Map("xx1" -> "xx1", "xx2" -> "xx1.xx2"), "jb")
    result.rootSiteLinks shouldBe Map("xx1"  -> "xx1", "xx2"  -> "xx2",
                                      "mw1"  -> "xx1", "mw2"  -> "xx2",
                                      "xx1j" -> "xx1", "xx2j" -> "xx2", "xx1b" -> "xx1", "xx2b" -> "xx2",
                                      "mw1j" -> "xx1", "mw2j" -> "xx2", "mw1b" -> "xx1", "mw2b" -> "xx2")
    result.rootSiteChain shouldBe Map("xx1"  -> "xx1.mw1", "xx2" -> "xx1.mw1.xx2.mw2")
    result.mineSiteChain shouldBe Map("mw1"  -> "xx1.mw1", "mw2" -> "xx1.mw1.xx2.mw2")
    result.xtraSiteChain shouldBe Map("mw1j" -> "xx1j.mw1j", "mw2j" -> "xx1j.mw1j.xx2j.mw2j",
                                      "mw1b" -> "xx1b.mw1b", "mw2b" -> "xx1b.mw1b.xx2b.mw2b",
                                      "xx1j" -> "xx1j.mw1j", "xx2j" -> "xx1j.mw1j.xx2j.mw2j",
                                      "xx1b" -> "xx1b.mw1b", "xx2b" -> "xx1b.mw1b.xx2b.mw2b")
  }

  "createChainMap" should "create map with chains (complex pure chain)" in {
    val result = createChainMap("ph.zx.mw", Map("xx1" -> "xx1", "xx2" -> "xx1.xx2"), "jb")
    result.rootSiteLinks shouldBe Map("xx1"  -> "xx1", "xx2"  -> "xx2",
                                      "mw1"  -> "xx1", "mw2"  -> "xx2",
                                      "xx1j" -> "xx1", "xx2j" -> "xx2", "xx1b" -> "xx1", "xx2b" -> "xx2",
                                      "mw1j" -> "xx1", "mw2j" -> "xx2", "mw1b" -> "xx1", "mw2b" -> "xx2")
    result.rootSiteChain shouldBe Map("xx1" -> "xx1.ph1.zx1.mw1", "xx2" -> "xx1.ph1.zx1.mw1.xx2.ph2.zx2.mw2")
    result.mineSiteChain shouldBe Map("mw1" -> "xx1.ph1.zx1.mw1", "mw2" -> "xx1.ph1.zx1.mw1.xx2.ph2.zx2.mw2")
    result.xtraSiteChain shouldBe Map("mw1j" -> "xx1j.ph1j.zx1j.mw1j", "mw2j" -> "xx1j.ph1j.zx1j.mw1j.xx2j.ph2j.zx2j.mw2j",
                                      "mw1b" -> "xx1b.ph1b.zx1b.mw1b", "mw2b" -> "xx1b.ph1b.zx1b.mw1b.xx2b.ph2b.zx2b.mw2b",
                                      "xx1j" -> "xx1j.ph1j.zx1j.mw1j", "xx2j" -> "xx1j.ph1j.zx1j.mw1j.xx2j.ph2j.zx2j.mw2j",
                                      "xx1b" -> "xx1b.ph1b.zx1b.mw1b", "xx2b" -> "xx1b.ph1b.zx1b.mw1b.xx2b.ph2b.zx2b.mw2b")
  }

}

@RunWith(classOf[JUnitRunner])
class GeneralUtilSpec extends FlatSpec with Matchers {
  import GeneralUtil._

  "formatPrice" should "parse price" in {
    val x = formatPrice _
    x("25")           shouldBe "25"
    x("123")          shouldBe "123"
    x("1234")         shouldBe "1,234"
    x("123.00")       shouldBe "123.00"
    x("1234.00")      shouldBe "1,234.00"
    x("1234567")      shouldBe "1,234,567"
    x("12350000")     shouldBe "12,350,000"
    x("150788350.85") shouldBe "150,788,350.85"
    x("0")            shouldBe "0"
    x("0.00")         shouldBe "0.00"
    x("")             shouldBe ""
  }

  "configToMills" should "parse text into mills" in {
    val x = configToMills _
    x("25s") shouldBe 25L*1000L
    x("12m") shouldBe 12L*1000L*60L
    x("12")  shouldBe 12L*1000L*60L
    x("6h")  shouldBe 6L*1000L*60L*60L
    x("4d")  shouldBe 4L*1000L*60L*60L*24L
    x("2y")  shouldBe 2L*1000L*60L*60L*24L*365L
  }

  "configToBytes" should "parse text into bytes" in {
    val x = configToBytes _
    x("25k") shouldBe 25L*1024L
    x("12m") shouldBe 12L*1024L*1024L
    x("12")  shouldBe 12L*1024L*1024L
    x("2g")  shouldBe 2L*1024L*1024L*1024L
  }

  "smartToken" should "insert tokens" in {
    val x = smartToken _
    val a = "{a}, {b} {c}. {d}, {e}."
    x(a, "{a}", "X") shouldBe "X, {b} {c}. {d}, {e}."
    x(a, "{c}", "X") shouldBe "{a}, {b} X. {d}, {e}."
    x(a, "{a}", "")  shouldBe "{b} {c}. {d}, {e}."
    x(a, "{b}", "")  shouldBe "{a}, {c}. {d}, {e}."
    x(a, "{c}", "")  shouldBe "{a}, {b}. {d}, {e}."
    x(a, "{d}", "")  shouldBe "{a}, {b} {c}. {e}."
    x(a, "{e}", "")  shouldBe "{a}, {b} {c}. {d}."
    x(",? a,,! b!", "{a}", "") shouldBe "a, b!"
  }

  "digest" should "digest text" in {
    val x = digest("1234567890", "MD5")
    digest("0987654321", "MD5") should not be (x)
    digest("1234567890", "MD5") should be (x)
  }

  "printStackTrace" should "construct exception stack trace" in {
    val f = printStackTrace _
    f(new Exception("DEBUG")) should startWith ("java.lang.Exception: DEBUG\n\tat com.coldcore.haala.core.GeneralUtilSpec")
    f(new CoreException("DEBUG")) should startWith ("com.coldcore.haala.core.CoreException: DEBUG\n\tat com.coldcore.haala.core.GeneralUtilSpec")
    f(new NoTraceException("DEBUG")) shouldBe "DEBUG"
  }

  "generateRef" should "generate ref based on pattern" in {
    val f = generateRef(_, PartialFunction.empty)

    f("ms") should fullyMatch regex "[0-9]{13}"
    f("hms") should fullyMatch regex "[0-9A-F]{11}"
    f("sym8") should fullyMatch regex "[0-9A-Z]{8}"
    f("num6") should fullyMatch regex "[0-9]{6}"

    f("|RT|hms|sym8|ms|num6|") should fullyMatch regex "(RT)[0-9A-F]{11}[0-9A-Z]{8}[0-9]{13}[0-9]{6}"
    f("|RT|ABC|{1}|{2}|FOO|") shouldBe "RTABC{1}{2}FOO"

    generateRef("|ZX|uref|", { case "uref" => "foo" }) shouldBe "ZXFOO"
    generateRef("|ZX|UREF|", { case "UREF" => "foo" }) shouldBe "ZXFOO"
    generateRef("|ZX|UREF|", { case "uref" => "foo" }) shouldBe "ZXUREF"

    a [CoreException] should be thrownBy generateRef("zx")
    a [CoreException] should be thrownBy generateRef("|ZX|uref|")
    a [CoreException] should be thrownBy generateRef("|zx|uref|", { case "uref" => "foo" })
  }

}

@RunWith(classOf[JUnitRunner])
class TimestampSpec extends FlatSpec with Matchers with BeforeAndAfter {

  val t = Timestamp()
  val c = t.toCalendar

  implicit class TS(ts: Timestamp) { def s = ts.toString }

  before {
    c.set(Calendar.YEAR, 2017)
    c.set(Calendar.MONTH, 2)
    c.set(Calendar.DAY_OF_MONTH, 25)
    c.set(Calendar.HOUR_OF_DAY, 21)
    c.set(Calendar.MINUTE, 26)
    c.set(Calendar.SECOND, 59)
    c.set(Calendar.MILLISECOND, 987)
  }

  it should "return be yyyyMMddHHmmssSSS" in {
    t.toLong shouldBe 20170325212659987L

    c.set(Calendar.HOUR_OF_DAY, 0)
    c.set(Calendar.MINUTE, 0)
    c.set(Calendar.SECOND, 0)
    c.set(Calendar.MILLISECOND, 0)
    t.toLong shouldBe 20170325000000000L

    Timestamp("20170325"+"200146").s       shouldBe "20170325"+"200146"+"000"
    Timestamp("20170325"+"200146"+"000").s shouldBe "20170325"+"200146"+"000"
    Timestamp("20170325"+"200146"+"001").s shouldBe "20170325"+"200146"+"001"
    Timestamp("20170325"+"200146"+"021").s shouldBe "20170325"+"200146"+"021"
    Timestamp("20170325"+"200146"+"321").s shouldBe "20170325"+"200146"+"321"
    Timestamp("20170325"+"200146"+"320").s shouldBe "20170325"+"200146"+"320"

    Timestamp(20170325, 200146).s      shouldBe "20170325"+"200146"+"000"
    Timestamp(20170325, 200146,   0).s shouldBe "20170325"+"200146"+"000"
    Timestamp(20170325, 200146,   1).s shouldBe "20170325"+"200146"+"001"
    Timestamp(20170325, 200146,  21).s shouldBe "20170325"+"200146"+"021"
    Timestamp(20170325, 200146, 321).s shouldBe "20170325"+"200146"+"321"
    Timestamp(20170325, 200146, 320).s shouldBe "20170325"+"200146"+"320"

    Timestamp("20170325"+"000046").s shouldBe "20170325"+"000046"+"000"
    Timestamp("20170325"+"000000").s shouldBe "20170325"+"000000"+"000"
    Timestamp("10010101"+"000000").s shouldBe "10010101"+"000000"+"000"
    Timestamp("00010101"+"000000").s shouldBe "00010101"+"000000"+"000"
    Timestamp("99990101"+"000000").s shouldBe "99990101"+"000000"+"000"

    Timestamp(20170325, 46).s shouldBe "20170325"+"000046"+"000"
    Timestamp(20170325,  0).s shouldBe "20170325"+"000000"+"000"
    Timestamp(10010101,  0).s shouldBe "10010101"+"000000"+"000"
    Timestamp(   10101,  0).s shouldBe "00010101"+"000000"+"000"
    Timestamp(99990101,  0).s shouldBe "99990101"+"000000"+"000"
  }

  it should "roll over on hours excess" in {
    Timestamp("20170325"+"240000"+"000").s shouldBe "20170326"+"000000"+"000"
    Timestamp("20170325"+"250000"+"000").s shouldBe "20170326"+"010000"+"000"
    Timestamp("20170325"+"251234"+"567").s shouldBe "20170326"+"011234"+"567"

    Timestamp(20170325, 240000,   0).s shouldBe "20170326"+"000000"+"000"
    Timestamp(20170325, 250000,   0).s shouldBe "20170326"+"010000"+"000"
    Timestamp(20170325, 251234, 567).s shouldBe "20170326"+"011234"+"567"
  }

  "timespan" should "return difference" in {
    val (t1, t2, t3, t4, t5, t6) = (
      Timestamp("20170325"+"181256"+"000"),
      Timestamp("20170325"+"181256"+"000"),
      Timestamp("20170325"+"181256"+"256"),
      Timestamp("20170325"+"201359"+"000"),
      Timestamp("20170525"+"181256"+"000"),
      Timestamp("20170525"+"201359"+"256"))

    t1.timespan(t2) shouldBe (0,  0, 0, 0, 0)
    t1.timespan(t3) shouldBe (0,  0, 0, 0, 256)
    t1.timespan(t4) shouldBe (0,  2, 1, 3, 0)
    t1.timespan(t5) shouldBe (61, 0, 0, 0, 0)

    t1.timespan(t6) shouldBe (61, 2, 1, 3, 256)
    t6.timespan(t1) shouldBe (61, 2, 1, 3, 256) // reverse
  }

}
