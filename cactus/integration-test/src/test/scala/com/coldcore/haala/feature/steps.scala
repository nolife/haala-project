package com.coldcore.haala
package feature

import cucumber.api.java.en._
import cucumber.api.java.Before
import com.coldcore.haala.integration.test.helper._

class Steps extends LoginSteps with HttpSteps with FacadeSteps with ConfigSteps with ApiSteps {
  import ContextHolder._

  override def httpClient = context.httpClient

  @Before
  def before {
    context.loadConfig()
    httpClient.setup
  }

}