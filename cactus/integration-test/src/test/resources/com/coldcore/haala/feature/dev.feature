@dev
Feature: My Dev Feature

  @001
  Scenario: Login as admin user
    Given logged in as admin
    And logged out

  Scenario: Response code 200 on login
    Given user credentials admin / admin
    And logged in
    Then http response code 200
