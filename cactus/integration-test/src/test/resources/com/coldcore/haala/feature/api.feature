@api
Feature: API

  Background: API dev features enabled
    Given api dev features enabled

  Scenario: Check that correct domain was selected for the request
    Given http GET page /api/system-status
    Then http response contains string timestamp
