@admin-basic
Feature: Admin basic operations

  Background: Login as admin user
    Given logged in as admin

  Scenario: Goes to account page after login
    Then facade reply is string /my/account.htm
    And http GET page /my/account.htm
    Then http response contains string <title>My Account</title>

  Scenario: Goes to login page tying to access account page after logout
    Given logged out
    And http GET page /my/account.htm
    Then http response contains string <title>Signing in</title>
