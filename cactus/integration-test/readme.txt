
Running integration tests
=========================
1. Set "Dev/ApiFeatures" and "Dev/NoCaptcha" settings to 1
2. Start the server with working control panel
3. Change "base.url" in "config.properties" to match your control panel address
4. Run the tests with Maven "mvn integration-test"

Cucumber uses feature files from the following folder:
  src/test/resources/com/coldcore/haala/feature
