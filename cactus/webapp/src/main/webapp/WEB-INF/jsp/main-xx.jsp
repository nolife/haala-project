<%@ include file="/WEB-INF/jsp/includes/taglibs.jsp" %>

<%--
  Default: Layout based on JSP overrides.
  This page is an example, overwrite it with a site eg. main-abc.jsp
--%>

<div class="m_wrapper">

  <div class="m_wide m_top">

    <%--<jsp:include page="/WEB-INF/jsp/includes/header.jsp"/>--%>

  </div>

  <div class="m_outer">
    <div class="m_inner">

      <div class="m_float-wrap">

        <div class="m_center">

          <%--<jsp:include page="/WEB-INF/jsp/pages/${pageName}.jsp"/>--%>

        </div>

        <div class="m_left">

          <%--<jsp:include page="/WEB-INF/jsp/includes/mainmenu.jsp"/>--%>

        </div>

        <div class="m_clear"></div>

      </div>

      <div class="m_right">

        <%--<jsp:include page="/WEB-INF/jsp/includes/sidebar.jsp"/>--%>

      </div>

      <div class="m_clear"></div>

    </div>
  </div>

  <div class="m_wide m_bottom">

    <%--<jsp:include page="/WEB-INF/jsp/includes/footer.jsp"/>--%>

  </div>

</div>
