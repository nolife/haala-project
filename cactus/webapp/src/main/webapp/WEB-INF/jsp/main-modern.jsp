<%@ include file="/WEB-INF/jsp/includes/taglibs.jsp" %>

<%--
  Modern: HTML5 layout based on Freemarker templates and Dynamic controller (ftl-page option).
  Optional top and bottom decorators with page content in either Freemarker or JSP.
--%>

<%-- Top decorator (optional) --%>
<h:freemarker-load-orelse template="pages/layout-top.ftl"/>

<%--
  1) Use single full layout page with included page content in Freemarker
  2) Use JSP with top and bottom decorators
  3) Use dynamic.jsp with decorators which may include page content in Freemarker or JSP
--%>
<h:freemarker-load-orelse template="pages/layout.ftl"/>
<c:if test="${not loadOrElse}">
  <%-- Include content from satchel when no template exists --%>
  <div id="content">
    <jsp:include page="/WEB-INF/jsp/pages/${pageName}.jsp"/>
  </div>
</c:if>

<%-- Bottom decorator (optional) --%>
<h:freemarker-load-orelse template="pages/layout-bottom.ftl"/>
