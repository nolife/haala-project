<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="/WEB-INF/jsp/includes/taglibs.jsp" %>

<html>
  <body>

    <c:choose>
      <c:when test="${service == 'paypal'}">

        <c:if test="${demo != 'y'}">
          <form name="myform" action="https://www.paypal.com/cgi-bin/webscr" method="post" style="display:none;">
            <input type="hidden" name="cmd" value="_s-xclick">
            <input type="hidden" name="hosted_button_id" value="${product}">
            <input type="hidden" name="item_name" value="${text}">
            <input type="hidden" name="quantity" value="${quantity}">
            <input type="hidden" name="custom" value="${key}">
            <input type="hidden" name="notify_url" value="${notifyURL}">
          <%--<input type="hidden" name="return" value="${completeURL}">--%>
          <%--<input type="hidden" name="cancel_return" value="${cancelURL}">--%>
            <input type="image" src="https://www.paypal.com/en_US/GB/i/btn/btn_buynowCC_LG.gif" border="0" name="submit" alt="">
            <img alt="" border="0" src="https://www.paypal.com/en_GB/i/scr/pixel.gif" width="1" height="1">
          </form>
        </c:if>

        <c:if test="${demo == 'y'}">
          <form name="myform" action="https://www.sandbox.paypal.com/cgi-bin/webscr" method="post" style="display:none;">
            <input type="hidden" name="cmd" value="_s-xclick">
            <input type="hidden" name="hosted_button_id" value="${product}">
            <input type="hidden" name="item_name" value="${text}">
            <input type="hidden" name="quantity" value="${quantity}">
            <input type="hidden" name="custom" value="${key}">
            <input type="hidden" name="notify_url" value="${notifyURL}">
          <%--<input type="hidden" name="return" value="${completeURL}">--%>
          <%--<input type="hidden" name="cancel_return" value="${cancelURL}">--%>
            <input type="image" src="https://www.sandbox.paypal.com/en_US/GB/i/btn/btn_buynowCC_LG.gif" border="0" name="submit" alt="PayPal - The safer, easier way to pay online.">
            <img alt="" border="0" src="https://www.sandbox.paypal.com/en_GB/i/scr/pixel.gif" width="1" height="1">
          </form>
        </c:if>

        <h:label key="chout.redir"/>
        <script type="text/javascript">
          document.myform.submit();
        </script>

      </c:when>
      <c:otherwise>
        <h:label key="chout.error"/>
      </c:otherwise>
    </c:choose>

  </body>
</html>