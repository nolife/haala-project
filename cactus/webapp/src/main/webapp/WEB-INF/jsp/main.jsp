<%@ include file="/WEB-INF/jsp/includes/taglibs.jsp" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<%--
  Main entry point. This page decides which layout to use based on controller options.
--%>

<h:on-ctrl-option option="html4">
  <!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
</h:on-ctrl-option>
<h:on-ctrl-option option="html5">
  <!DOCTYPE html>
</h:on-ctrl-option>

<html>

  <head>

    <%--
       If DynamicController handled the request then use dynamicPageName
       instead of pageName to attach correct CSS and JS files
    --%>
    <h:use-dynamic-pagename>
      <jsp:include page="/WEB-INF/jsp/includes/head.jsp"/>
    </h:use-dynamic-pagename>

  </head>

  <body id="x">

    <jsp:include page="/WEB-INF/jsp/includes/cms/pageidx.jsp"/>

    <%-- Siteable JSP layout --%>
    <h:on-ctrl-option option="layout-xx">
      <h:layout path="main"/>
    </h:on-ctrl-option>

    <%-- HTML5 layout (Freemarker or JSP based) --%>
    <h:on-ctrl-option option="layout-modern">
      <jsp:include page="/WEB-INF/jsp/main-modern.jsp"/>
    </h:on-ctrl-option>

    <jsp:include page="/WEB-INF/jsp/includes/system.jsp"/>

  </body>

</html>