<%@ include file="/WEB-INF/jsp/includes/taglibs.jsp" %>

<h:cms-enabled>
  <div id="cms-messages" style="display:none;">
    <div data-key="tooltip-edit"><h:label key="cms.tooltip-edit"/></div>
    <div data-key="tooltip-done"><h:label key="cms.tooltip-done"/></div>
    <div data-key="tooltip-exit"><h:label key="cms.tooltip-exit"/></div>
    <div data-key="tooltip-version"><h:label key="cms.tooltip-version" tokens="ver:${resourceVersion}"/></div>
    <div data-key="confirm-version"><h:label key="cms.confirm-version" tokens="ver:${resourceVersion}"/></div>
    <div data-key="dialog-confirm-title"><h:label key="cms.dialog-confirm-title"/></div>
    <div data-key="dialog-yes"><h:label key="cms.dialog-yes"/></div>
    <div data-key="dialog-no"><h:label key="cms.dialog-no"/></div>
    <div data-key="anchor-1"><h:label key="cms.anchor-1"/></div>
    <div data-key="anchor-2"><h:label key="cms.anchor-2"/></div>
    <div data-key="anchor-2n"><h:label key="cms.anchor-2"/></div>
    <div data-key="anchor-2v"><h:label key="cms.anchor-2"/></div>
    <div data-key="anchor-2p"><h:label key="cms.anchor-2p"/></div>
    <div data-key="anchor-2t"><h:label key="cms.anchor-2t"/></div>
    <div data-key="anchor-2m"><h:label key="cms.anchor-2m"/></div>
    <div data-key="anchor-3k"><h:label key="cms.anchor-3k"/></div>
    <div data-key="anchor-3d"><h:label key="cms.anchor-3d"/></div>
    <div data-key="anchor-4c"><h:label key="cms.anchor-4c"/></div>
    <div data-key="anchor-4j"><h:label key="cms.anchor-4j"/></div>
    <div data-key="anchor-pg"><h:label key="cms.anchor-pg"/></div>
    <div data-key="picture-frame"><h:label key="cms.picture-frame"/></div>
    <div data-key="text-frame"><h:label key="cms.text-frame"/></div>
    <div data-key="text-frame-delete"><h:label key="cms.text-frame-delete"/></div>
    <div data-key="file-frame"><h:label key="cms.file-frame"/></div>
    <div data-key="upload"><h:label key="cms.upload"/></div>
    <div data-key="file-open"><h:label key="cms.file-open"/></div>
    <div data-key="cancel"><h:label key="cms.cancel"/></div>
    <div data-key="save"><h:label key="cms.save"/></div>
    <div data-key="delete"><h:label key="cms.delete"/></div>

    <h:site var="sites" action="input"/>
    <c:forEach items="${sites}" var="site" varStatus="vs">
      <h:site var="lang" action="lang" params="key=${sites[vs.index]}"/>
      <div data-key="lang-${site}"><h:label key="lng.${lang}"/></div>
    </c:forEach>
  </div>

  <ul id="cms-anchor-menu" class="contextMenu">
    <li class="edit"><a href="#edit"><h:label key="cms.edit"/></a></li>
    <h:site var="sites" action="input"/>
    <c:forEach items="${sites}" var="site" varStatus="vs">
      <h:site var="lang" action="lang" params="key=${sites[vs.index]}"/>
      <li class="<c:if test="${vs.first}">separator</c:if>" >
        <a href="#root-${site}"><h:label key="lng.${lang}"/></a>
      </li>
    </c:forEach>
  </ul>
</h:cms-enabled>
