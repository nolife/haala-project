<%@ include file="/WEB-INF/jsp/includes/taglibs.jsp" %>

<div id="quick-navigation-wrap" style="display:none;">
  <div id="quick-navigation" class="quick-navigation">
    <p><h:label key="nav.quick"/></p>
  </div>

  <div id="recently-visited" class="quick-navigation">
    <p><h:label key="nav.recnt"/></p>
    <h:util var="count" value="${recentLinks}" action="size"/>
    <c:if test="${count > 1 || count == 1 && recentLinks[0].uri != currentURI}">
      <c:forEach items="${recentLinks}" var="link" varStatus="vs">
        <c:if test="${link.uri != currentURI}">
          <a href="${link.url}">${link.text}</a>
        </c:if>
      </c:forEach>
    </c:if>
  </div>

  <div class="nofloat"></div>
</div>

