<%@ include file="/WEB-INF/jsp/includes/taglibs.jsp" %>

<h:freemarker-load-orelse template="cms/pageidx.ftl"/>
<c:if test="${not loadOrElse}">
  <%-- Include content from siteable JSP layout when no template option --%>
  <h:layout path="includes/cms/pageidx"/>
</c:if>
