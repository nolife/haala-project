<%@ include file="/WEB-INF/jsp/includes/taglibs.jsp" %>

<h:user-inrole role="cms">
  <script type="text/javascript" src="/dwr/interface/CmsF.js?rv=${resourceVersion}"></script>
</h:user-inrole>

<h:cms-enabled>
  <script type="text/javascript" src="/haala/mere-cms.js?site=${mySite}&rv=${resourceVersion}"></script>
  <link type="text/css" rel="stylesheet" href="/haala/mere-cms.css?site=${mySite}&rv=${resourceVersion}"/>
</h:cms-enabled>
