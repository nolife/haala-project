<%@ include file="/WEB-INF/jsp/includes/taglibs.jsp" %>

<meta charset="UTF-8">

<%-- Title, icon and meta --%>
<h:freemarker-load-orelse template="site-meta.ftl"/>
<c:if test="${not loadOrElse}">
  <title>${pageTitle}</title>
  <link rel="icon" href="/favicon.ico">
  <c:if test="${metaDescription != null}"><meta name="description" content="${metaDescription}"></c:if>
  <meta name="keywords" content="${metaKeywords}">
</c:if>

<%-- External scripts --%>
<h:system var="env" action="env"/>
<c:if test="${env == 'live'}">
  <%-- online --%>
  <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>
</c:if>
<c:if test="${env == 'local'}">
  <%-- offline --%>
  <script type="text/javascript" src="/jquery-3.3.1.min.js"></script>
  <script type="text/javascript" src="/jquery-ui-1.12.1.min.js"></script>
</c:if>

<%-- Common scripts --%>
<script type='text/javascript' src="/dwr/engine.js?rv=${resourceVersion}"></script>
<script type='text/javascript' src="/dwr/util.js?rv=${resourceVersion}"></script>
<script type="text/javascript" src="/dwr/interface/LoginF.js?rv=${resourceVersion}"></script>
<script type="text/javascript" src="/dwr/interface/MiscF.js?rv=${resourceVersion}"></script>
<script type="text/javascript" src="/main.js?site=${mySite}&rv=${resourceVersion}"></script>
<script type="text/javascript" src="/lab.htm?site=${mySite}j&rv=${resourceVersion}"></script>

<jsp:include page="/WEB-INF/jsp/includes/cms/head.jsp"/>

<%-- Page specific scripts --%>
<c:forEach items="${pageFacade}" var="item">
  <script type="text/javascript" src="/dwr/interface/${item}.js?rv=${resourceVersion}"></script>
</c:forEach>
<script type="text/javascript" src="/pages/${pageName}.js?site=${mySite}&rv=${resourceVersion}"></script>

<%-- Common CSS --%>
<link type="text/css" rel="stylesheet" href="/main.css?site=${mySite}&rv=${resourceVersion}"/>

<%--Page specific CSS --%>
<link type="text/css" rel="stylesheet" href="/pages/${pageName}.css?site=${mySite}&rv=${resourceVersion}"/>

<%-- Script with dom loaded event --%>
<script type="text/javascript" src="/last.js?site=${mySite}&rv=${resourceVersion}"></script>

<h:freemarker-load template="site-ihead.ftl"/>
<h:label key="site.ihead"/>
