<%@ include file="/WEB-INF/jsp/includes/taglibs.jsp" %>

<%--
  Site tabs for jQuery UI Tabs

  [English] [Russian]

  <div>
    inlude this
    <div id="site-tab-xx1"> ... </div>
    <div id="site-tab-xx2"> ... </div>
  </div>
--%>

<ul>

  <%--
    If sites not provided use root sites to input default multilingual data.
    We will use anothe array of root sites to label the tabs (in case if
    provided sites do not comply with language).
  --%>
  <c:if test="${sites == null}"><h:site var="sites" action="input"/></c:if>
  <h:site var="labels" action="input"/>

  <c:forEach items="${sites}" var="site" varStatus="vs">
    <h:site var="lang" action="lang" params="key=${labels[vs.index]}"/>
    <li><a href="#site-tab-${site}"><h:label key="lng.${lang}"/></a></li>
  </c:forEach>
</ul>
