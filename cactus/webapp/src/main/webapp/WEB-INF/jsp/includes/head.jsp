<%@ include file="/WEB-INF/jsp/includes/taglibs.jsp" %>

<h:freemarker-load-orelse template="site-head.ftl"/>
<c:if test="${not loadOrElse}">
  <%-- Include content from siteable JSP layout when no template option --%>
  <h:layout path="includes/head"/>
</c:if>
