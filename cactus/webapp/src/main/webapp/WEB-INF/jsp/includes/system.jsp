<%@ include file="/WEB-INF/jsp/includes/taglibs.jsp" %>

<div id="sys-ivars" style="display:none;">
  {
    rootSite: "${rootSite}", mySite: "${mySite}", resourceVersion: "${resourceVersion}",
    inputSites: <h:site action="input" params="as=jsonArray"/>
    <h:user-inrole role="super">, superUser: "1"</h:user-inrole>, lang: "<h:site action="lang"/>"
    <%-- no comma before this --%> <jsp:include page="/WEB-INF/jsp/includes/cms/ivars.jsp"/>
  }
</div>

<jsp:include page="/WEB-INF/jsp/includes/cms/system.jsp"/>

<div id="sys-messages" style="display:none;">
  <h:system var="suspend" action="setting" params="type=sys;key=SuspendFlag"/>
  <c:if test="${suspend == '1'}">
    <div class="note critical"><h:label key="note.shutd"/></div>
  </c:if>
  <h:user-inrole role="super">
    <div class="note warning"><h:label key="note.shell" tokens="user:${shellUsername}"/></div>
  </h:user-inrole>
</div>

<div style="display:none;">
  <a href="/sitemap.htm">sitemap</a>
</div>

<h:freemarker-load template="site-bbody.ftl"/>
<h:label key="site.bbody"/>
