<%@ include file="/WEB-INF/jsp/includes/taglibs.jsp" %>

<h:freemarker-load-orelse template="pages/${pageName}.ftl"/>
<c:if test="${not loadOrElse}">
  <%-- Include content from satchel when no template exists --%>
  <c:set var="entry_id" value="${pageName}Page" scope="request"/>
  <h:layout path="includes/satchel"/>
</c:if>
