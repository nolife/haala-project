<%@ include file="/WEB-INF/jsp/includes/taglibs.jsp" %>

<h:freemarker-load-orelse template="pages/${dynamicPageName}.ftl"/>
<c:if test="${not loadOrElse}">
  <%-- Include content from satchel when no template exists --%>
  <c:set var="entry_id" value="${pageIndex}Page" scope="request"/>
  <h:layout path="includes/satchel"/>
</c:if>
