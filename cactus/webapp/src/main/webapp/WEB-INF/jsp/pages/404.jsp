<%@ include file="/WEB-INF/jsp/includes/taglibs.jsp" %>

<h:freemarker-load-orelse template="pages/404.ftl"/>
<c:if test="${not loadOrElse}">
  <%-- Default content when no template option --%>
  <h:label key="001"/>
</c:if>
