<%@ include file="/WEB-INF/jsp/includes/taglibs.jsp" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<% response.setIntHeader("Retry-After", 3600*24); %>

<%-- Display error content loaded from Freemarker or similar --%>
<c:if test="${errorContent != null}">
  ${errorContent}
</c:if>

<%-- Display default error content --%>
<c:if test="${errorContent == null}">

  <html>
    <head>
      <title>System Error</title>
      <style type="text/css">

        html, body { margin:0; padding:0; border:0; }
        body { color:#000; background:#FFF; font:16px Tahoma, Arial, Helvetica, sans-serif; text-align:center; }
        #wrap { margin:0 auto; width:600px; }
        .cont { padding:20px; }
        .outer { -moz-border-radius:10px; border-radius:10px; padding:10px; background:#9a9a9a; }
        .title { color:#FFF; letter-spacing:4px; text-transform:uppercase; font-weight:bold; padding:0 0 10px 0; }
        .inner { background:#FFF; padding:10px; -moz-border-radius:10px; border-radius:10px; }
        .link { color:#000; text-decoration:underline; }
        .link:hover { text-decoration:none; }

      </style>
    </head>
    <body>

    <div id="wrap">
      <div class="cont">
        <div class="outer">
          <div class="title">System &nbsp; Error</div>
          <div class="inner">
            The system error has occurred, we do apologize for the inconvenience!<br/>
            This error will be fixed soon.<br/><br/>
            <a class="link" href="javascript:history.back()">Go back</a>
          </div>
        </div>
      </div>
    </div>

    </body>
  </html>

</c:if>
