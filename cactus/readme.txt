HAALA Cactus
============

Cactus is the project module with special build process to accumulate
all addons (web applications) under its roof. It also provides CMS
control panel for general management and for addons.

This article contains
  *) installation steps
  *) configuration notes
  *) general information

Automated installation for Vagrant is available here
  cactus/resources/vagrant/readme.txt

Direct link
  https://bitbucket.org/nolife/haala-project/src/master/cactus/resources/vagrant



Installation
============

Use automated installation for Vagrant. If you prefer to install manually, refer
to the automated scripts to fill any gaps in the sections below. The scripts come
as individual steps and should be easy to read.


Pre-install requirements
------------------------

1. PostgreSQL v8+
     create a new HAALA database schema

2. Solr v5+
     configure a new HAALA core (available in downloads)

3. Tomcat v7+
     add HAALA datasource to 'context.xml' (example 'cactus/resources/env/tomcat/container/context.xml')

4. Create HAALA filesystem directory on a disk (available in downloads)
     enable read/write for a 'tomcat' user


Manual steps
------------

You need to:
  *) apply SQL on HAALA database
  *) copy feed files into HAALA filesystem directory
  *) configure Solr HAALA schema

Generic module:
  1. Run all SQL from 'generic/resources/db_pgsql/initial' (except for 000 which is for removal)
  2. Copy all feeds from 'generic/resources' into 'feed/1.generic' directory

Cactus module:
  1. Run 'cactus/resources/db_pgsql/initial/020-data.sql'
  2. Copy all feeds from 'cactus/resources/site/cp' into 'feed/2.cactus-cp' directory
  3. Copy all feeds from 'cactus/resources/site/me' into 'feed/2.cactus-me' directory
  4. Replace Solr schema content with 'cactus/resources/solr-schema-1.5.xml'

Banner, Toyshop, Wiki addon-ons (pack):
  1. Run all SQL from '[pack]/resources/db_pgsql/initial' (except for 000 which is for removal)
  2. Copy all feeds from '[pack]/resources' into 'feed/#.[pack]' directory (eg '3.banner' etc)
  3. Append content of '[pack]/resources/solr-schema-part.xml' to Solr schema

Optional example of CFTP website:
  1. Run 'cactus/resources/site/cft/db_pgsql/initial/020-data.sql'
  2. Copy all feeds from 'cactus/resources/site/cft' into 'feed/3.cactus-cft' directory


Post-install
------------

1. Update to match your environment (sql)
     update settings set value = '/path-to-your/haala-cactus.fs' where key = 'FsPath';  -- HAALA filesystem directory
     update settings set value = 'http://localhost:8983/solr/haala-cactus' where key = 'SolrURL';  -- HAALA Solr core URL

2. Enable the following tasks to update users (sql)
     update settings set value = '1' where key = 'AddMissingPacksFlag';
     update settings set value = '1' where key = 'SolrSubmitUsersFlag';

3. Configure Tomcat '/' to map to HAALA 'war' (example 'cactus/resources/env/tomcat/container/ROOT.xml')

4. Update the following to match your domains (sql)
     select * from domains where domain like '%EXAMPLE.ORG%'; -- adjust
     select * from settings where value like '%EXAMPLE.ORG%'; -- adjust
     select * from users where email like '%EXAMPLE.ORG%'; -- adjust

When Tomcat is started, the app starts to feed on files in HAALA filesystem directory.
Only when the directory is empty, app is fully configured and you may access websites.



LIVE config
===========

1. Switch environment to 'live' (sql)
     update settings set value = 'env=live' where key = 'Environment';

2. Disable 'Dev' settings (sql)
     update settings set value = '0' where key like 'Dev/%';

3. Review 'task' settings and adjust proper 'sleep' values (sql)
     select * from settings where key like '%Task';  -- adjust 'sleep' manually



Building
========

Build the project with Maven 3

Generic:
  mvn -f generic/pom.xml

Cactus LOCAL:
  mvn -f cactus/pom.xml

Cactus LIVE:
  mvn -f cactus/pom.xml -P tomcat-live



Running
=======
1. Start database
2. Start Solr
3. Start Tomcat



URLs
====
1. Default website: 'http://EXAMPLE.ORG'
2. Control panel: 'http://cpanel.EXAMPLE.ORG'
3. Toy shop: 'http://toys.EXAMPLE.ORG'
3. Example of CFTP website: 'http://cft.EXAMPLE.ORG'



Upgrade
=======
1. Follow instructions in 'cactus/resources/update' for your current version to upgrade to the next
2. Redeploy 'war' file



Dev flags (set to 1 or 0)
=========================
Dev/NoCaptcha      disable captcha checks
Dev/NoImages       conceal images
Dev/NoCache        disable caching (excl Hibernate and special caches)
Dev/NoEmail        do not send emails
Dev/LogEmail       log email content when sending
Dev/EmailSupport   redirect all emails to support
Dev/ApiFeatures    enable for integration tests



Notes
=====

*) Deployable package has a name similar to "cactus-1.23.war" with the "release.info" file included
   in its META-INF folder. The "release.info" file contains current project version and profile name
   used during the build.
   