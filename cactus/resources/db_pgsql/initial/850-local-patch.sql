-------------------------
-- LIVE to LOCAL patch --
-- use only as example --
-------------------------

update SETTINGS set value = 'env=local' where key = 'Environment';
update SETTINGS set value = '/vault/dev/haala-drv/haala-cactus.fs' where key = 'FsPath';
update SETTINGS set value = 'http://localhost/external/GBP.xml' where key = 'CurrencyRatesRSS';
update SETTINGS set value = 'sleep=1s, wipe=1m' where key = 'CleanCacheTask';
update SETTINGS set value = 'sleep=0, wait=10' where key = 'CleanFeedTask';
update SETTINGS set value = 'sleep=1s, wait=1s' where key = 'FeedTask';

update users set password = username;
