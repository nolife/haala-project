------------------------------
-- Extension to generic 020 --
------------------------------

INSERT INTO DOMAINS(ID, DOMAIN, DOMAIN_SITE, TYPE, META)
  VALUES (nextval('DOMAINS_SEQ'), 'cpanel.EXAMPLE.ORG', 'cp', 'cpanel', 'style=html5,layout-modern; resolve=site,currency');

insert into SETTINGS (id, key, value, site) values (nextval('SETTINGS_SEQ'), 'MainURL', 'http://cpanel.EXAMPLE.ORG', 'cp1');
insert into SETTINGS (id, key, value, site) values (nextval('SETTINGS_SEQ'), 'FacebookApiKey', '452045648182489', 'cp1');
insert into SETTINGS (id, key, value, site) values (nextval('SETTINGS_SEQ'), 'TuringParams', 'fcol=095574, bcol=FFFFFF, rcol=0789BA, size=100x29, font=Tahoma, fosz=18, n=4', 'cp1');
