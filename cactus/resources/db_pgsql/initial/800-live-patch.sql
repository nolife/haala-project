-------------------------
-- LOCAL to LIVE patch --
-- use only as example --
-------------------------

update SETTINGS set value = '/usr/local/haala_fs' where key = 'FsPath';

update SETTINGS set value = 'http://feeds.currencysource.com/GBP.xml' where key = 'CurrencyRatesRSS';

update SETTINGS set value = 'sleep=12h' where key = 'CleanAbyssTask';
update SETTINGS set value = 'sleep=5, wipe=10' where key = 'CleanActiveCacheTask';
update SETTINGS set value = 'sleep=5, wipe=1d' where key = 'CleanCacheTask';
update SETTINGS set value = 'sleep=1h, wait=1d' where key = 'CleanFeedTask';
update SETTINGS set value = 'sleep=12h, wait=1h' where key = 'CleanFilesTask';
update SETTINGS set value = 'sleep=1h, wait=1d' where key = 'CleanTmpTask';
update SETTINGS set value = 'sleep=5, wait=3' where key = 'CleanTuringTask';
update SETTINGS set value = 'sleep=1h, wait=1d' where key = 'CleanUsersTask';
update SETTINGS set value = 'sleep=2h' where key = 'CurrencyRatesTask';
update SETTINGS set value = 'sleep=15' where key = 'ForwardErrorsTask';
update SETTINGS set value = 'sleep=5, items=25' where key = 'ForwardMessagesTask';
update SETTINGS set value = 'sleep=1, wait=1' where key = 'FeedTask';
update SETTINGS set value = 'sleep=4h' where key = 'SolrOptimizeTask';
update SETTINGS set value = 'sleep=5, items=10' where key = 'SpamEmailsTask';
