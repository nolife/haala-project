
-- cleanup
delete from labels where key in ('01account.009', '01account.021');
delete from labels where key like 'loginC.%';
delete from labels where key like 'reminC.%';
delete from labels where key like 'propsC.%';
delete from labels where key like 'regisC.%';
delete from labels where key like 'agreeC.%';

delete from files where name = 'onload.js';
delete from files where name = 'jquery.sharrre.js';
delete from files where name like '%tipsy%';
delete from files where folder like '%tinymce-4.1.0%';
delete from files where folder like '%tinymce-3.4.7%';

-- Re-branding (proprietary data, change to your own website)
update settings set value = 'name=ColdCore, web=ColdCore.com, info=info@coldcore.com' where key = 'Brand' and site = 'pub';
update settings set value = 'support@coldcore.com' where key = 'SupportEmail' and site = 'pub';
update settings set value = 'http://coldcore.com' where key = 'MainURL' and site = 'pub';

update labels set value = 'ColdCore.com' where key = 'site.ttl' and site = 'me1';
update labels set value = '© 2009-2014 ColdCore.com' where key = 'site.copy' and site = 'me1';

update domains set domain = 'cpanel.coldcore.com' where domain_site = 'cp';
