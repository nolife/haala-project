
-- move web resources
update files set folder = replace(folder, 'web/gfx/x', 'web');
update files set folder = replace(folder, 'web/gfx', 'web');
update files set folder = replace(folder, 'web/css', 'web');
update files set folder = replace(folder, 'web/js', 'web');
update files set folder = replace(folder, 'web/ftl', 'web');
update files set folder = replace(folder, 'web/vm', 'web');

-- redundant files
delete from files where folder = 'web' and name = 'browser.css';
delete from files where folder = 'web' and name = 'dot.gif';
delete from files where folder = 'web' and name like 'browser-%.gif';
delete from files where folder = 'web/cms';
delete from files where folder = 'web/endorsed' and name = 'qtip-jgrowl.js';
delete from files where folder = 'web/endorsed' and name = 'jquery.qtip.js';
delete from files where folder = 'web/endorsed' and name = 'qtip.css';
delete from files where folder = 'web/endorsed/contextMenu';
delete from files where folder = 'web/endorsed' and name = 'contextMenu.css';
delete from files where folder = 'web/endorsed' and name = 'jquery.contextMenu.js';
delete from files where folder = 'web/endorsed' and name = 'jquery.sticky.js';
delete from files where folder = 'web/endorsed' and name = 'simplelightbox.min.css';
delete from files where folder = 'web/endorsed' and name = 'simple-lightbox.min.js';
delete from files where folder = 'web/theme/deepblue';
delete from files where folder = 'web/theme/lightness';

-- remove house from address
!!! script stop: manually move house into line_1 and then proceed !!!
select * from addresses order by id;

alter table addresses drop column house;
