-- abyss rework
update SETTINGS set value = 'sleep=12h' where key = 'CleanAbyssTask';

-- credit rework
alter table credit_entries drop constraint credit_entry_user_fk;
drop table credit_entries;
drop SEQUENCE credit_entries_seq;

delete from files where name in ('CreditF.js', 'credit.js', 'credit.css');
delete from labels where key in ('01manUserView.018', '01manUserView.019', '01manUserView.022');

-- bill and payment removal
delete from files where folder = 'web/pages/payment';
delete from files where folder = 'web/pages/manager/payment';
delete from files where folder = 'web/payment-manager';
delete from files where name = 'account-nav-payments.gif';

-- redundant settings
delete from settings where key = 'BillPayment';
delete from settings where key = 'BillConf';
delete from settings where key = 'CreditPurchase';
delete from settings where key = 'CreditPurchaseDemo';
delete from settings where key = 'CreditOnPaymentTask';

insert into SETTINGS (id, key, value, site) values (nextval('SETTINGS_SEQ'), 'PaymentEventTask', 'sleep=1', 'sys');

-- messages rework
delete from settings where key in ('ItemsExpire');
delete from settings where key like 'CleanMessagesTask%';
delete from settings where key = 'MessageConstants' and site = 'sys';

delete from labels where key in (
  'msg.type1', 'msg.type3',
  'msg.type4', 'msg.type5',
  'msg.emailT4', 'msg.emailT5',
  'msg.emailT1', 'msg.emailT3',
  '01messageView.032', '01messageView.028'
);
delete from labels where key like '%01messageEmail%' and site in ('cp1', 'cp2');
delete from labels where key like '%01messageUser%' and site in ('cp1', 'cp2');
delete from labels where key in ('01messages.016', '01messages.011', '01messages.012') and site in ('cp1', 'cp2');

delete from files where folder = 'web/pages/cp/message' and name in ('email.js', 'email.ftl', 'user.js', 'user.ftl');
delete from files where folder = 'web/pages/cp/message/view';

alter table messages drop constraint message_user_fk;
alter table message_forwards drop constraint message_forward_user_fk;

drop table messages;
drop table message_forwards;

DROP SEQUENCE messages_seq;
DROP SEQUENCE message_forwards_seq;

-- lease rework
delete from settings where key = 'LeaseConstants' and site = 'sys';
delete from settings where key like 'CleanLeasesTask%';

alter table leases drop constraint lease_user_fk;
alter table leases drop constraint leases_type_value_key;
drop index leases_ind1;
drop table leases;
DROP SEQUENCE leases_seq;

-- node manager
insert into SETTINGS (id, key, value, site) values (nextval('SETTINGS_SEQ'), 'NodeSummaryTask', 'sleep=1h', 'sys');
insert into SETTINGS (id, key, value, site) values (nextval('SETTINGS_SEQ'), 'NodeTestTask', 'sleep=1, emailFrom=root@EXAMPLE.ORG, emailTo=support@EXAMPLE.ORG', 'sys');

-- unused labels
delete from labels where key = '01account.h1' and site in ('cp1', 'cp2');
delete from labels where key = '01account.006' and site in ('cp1', 'cp2');

-- icons move
delete from files where folder = 'web' and name in (
  'messages-smallnav-fwd.gif',
  'messages-smallnav-nofwd.gif',
  'messages-smallnav-delete.gif',
  'messages-smallnav-viewactive.gif',
  'messages-smallnav-edit.gif',
  'messages-smallnav-abyss.gif',
  'messages-smallnav-email.gif',
  'messages-icon-unread.gif'
);
