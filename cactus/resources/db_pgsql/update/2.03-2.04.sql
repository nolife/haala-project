
-- Feed task refactored
update settings set key = 'FeedTask' where key = 'LabelsFeedTask';
delete from settings where key in ('FilesFeedTask', 'SettingsFeedTask');

-- Validator reg ex refactored
update settings set value = '([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?) .{5,100}' where key = 'RegEx/email';
delete from settings where key = 'RegEx/email/2';

-- Username / password min 5 symbols
update settings set value = '[a-zA-Z][0-9a-zA-Z_]{4,99}' where key = 'RegEx/username';
update settings set value = '.{5,100}' where key = 'RegEx/password';
update users set username = 'admin' where username = 'admino';
