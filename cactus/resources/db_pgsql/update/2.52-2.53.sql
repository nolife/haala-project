
-- update files size and meta
insert into SETTINGS (id, key, value, site) values (nextval('SETTINGS_SEQ'), 'RefreshFilesTask', 'sleep=1', 'sys');
insert into SETTINGS (id, key, value, site) values (nextval('SETTINGS_SEQ'), 'RefreshFilesFlag', '0', 'sys');

-- redundant files
delete from files where folder = 'web/js/haala' and name = 'cookie.js' and site = 'xx1';

-- new settings
insert into SETTINGS (id, key, value, site) values (nextval('SETTINGS_SEQ'), 'CssCtrlExtensions', 'css, jpg, jpeg, gif, png', 'sys');


-- date columns converted to number
-- generic schema
alter table files rename column updated to date_old;
alter table files add column updated bigint;
update files set updated =
  extract(year from date_old)*10000000000000+
  extract(month from date_old)*100000000000+
  extract(day from date_old)*1000000000+
  extract(hour from date_old)*10000000+
  extract(minute from date_old)*100000+
  extract(second from date_old)*1000;
alter table files alter column updated set not null;
alter table files drop column date_old;

alter table abyss_entries rename column updated to date_old;
alter table abyss_entries add column updated bigint;
update abyss_entries set updated =
  extract(year from date_old)*10000000000000+
  extract(month from date_old)*100000000000+
  extract(day from date_old)*1000000000+
  extract(hour from date_old)*10000000+
  extract(minute from date_old)*100000+
  extract(second from date_old)*1000;
alter table abyss_entries alter column updated set not null;
alter table abyss_entries drop column date_old;

alter table abyss_entries rename column created to date_old;
alter table abyss_entries add column created bigint;
update abyss_entries set created =
  extract(year from date_old)*10000000000000+
  extract(month from date_old)*100000000000+
  extract(day from date_old)*1000000000+
  extract(hour from date_old)*10000000+
  extract(minute from date_old)*100000+
  extract(second from date_old)*1000;
alter table abyss_entries alter column created set not null;
alter table abyss_entries drop column date_old;

alter table credit_entries rename column created to date_old;
alter table credit_entries add column created bigint;
update credit_entries set created =
  extract(year from date_old)*10000000000000+
  extract(month from date_old)*100000000000+
  extract(day from date_old)*1000000000+
  extract(hour from date_old)*10000000+
  extract(minute from date_old)*100000+
  extract(second from date_old)*1000;
alter table credit_entries alter column created set not null;
alter table credit_entries drop column date_old;

alter table messages rename column created to date_old;
alter table messages add column created bigint;
update messages set created =
  extract(year from date_old)*10000000000000+
  extract(month from date_old)*100000000000+
  extract(day from date_old)*1000000000+
  extract(hour from date_old)*10000000+
  extract(minute from date_old)*100000+
  extract(second from date_old)*1000;
alter table messages alter column created set not null;
alter table messages drop column date_old;

alter table users rename column created to date_old;
alter table users add column created bigint;
update users set created =
  extract(year from date_old)*10000000000000+
  extract(month from date_old)*100000000000+
  extract(day from date_old)*1000000000+
  extract(hour from date_old)*10000000+
  extract(minute from date_old)*100000+
  extract(second from date_old)*1000;
alter table users alter column created set not null;
alter table users drop column date_old;

alter table users rename column activated to date_old;
alter table users add column activated bigint;
update users set activated =
  extract(year from date_old)*10000000000000+
  extract(month from date_old)*100000000000+
  extract(day from date_old)*1000000000+
  extract(hour from date_old)*10000000+
  extract(minute from date_old)*100000+
  extract(second from date_old)*1000
  where date_old is not null;
alter table users drop column date_old;

alter table users rename column last_login to date_old;
alter table users add column last_login bigint;
update users set last_login =
  extract(year from date_old)*10000000000000+
  extract(month from date_old)*100000000000+
  extract(day from date_old)*1000000000+
  extract(hour from date_old)*10000000+
  extract(minute from date_old)*100000+
  extract(second from date_old)*1000
  where date_old is not null;
alter table users drop column date_old;

alter table leases rename column expires to date_old;
alter table leases add column expires bigint;
update leases set expires =
  extract(year from date_old)*10000000000000+
  extract(month from date_old)*100000000000+
  extract(day from date_old)*1000000000+
  extract(hour from date_old)*10000000+
  extract(minute from date_old)*100000+
  extract(second from date_old)*1000;
alter table leases alter column expires set not null;
alter table leases drop column date_old;

-- banner pack
alter table BANNER_banners rename column expires to date_old;
alter table BANNER_banners add column expires bigint;
update BANNER_banners set expires =
  extract(year from date_old)*10000000000000+
  extract(month from date_old)*100000000000+
  extract(day from date_old)*1000000000+
  extract(hour from date_old)*10000000+
  extract(minute from date_old)*100000+
  extract(second from date_old)*1000
  where date_old is not null;
alter table BANNER_banners drop column date_old;

-- toyshop pack
alter table TOYSHOP_warehouses rename column expected to date_old;
alter table TOYSHOP_warehouses add column expected bigint;
update TOYSHOP_warehouses set expected =
  extract(year from date_old)*10000000000000+
  extract(month from date_old)*100000000000+
  extract(day from date_old)*1000000000+
  extract(hour from date_old)*10000000+
  extract(minute from date_old)*100000+
  extract(second from date_old)*1000
  where date_old is not null;
alter table TOYSHOP_warehouses drop column date_old;


-- MAPS renamed to GEO_MAPS
alter table maps rename to geo_maps;
alter index maps_pkey rename to geo_maps_pkey;
alter sequence maps_seq rename to geo_maps_seq;
