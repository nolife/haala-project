
-- user_id removed, "o.user" should be accessed as "o.pack.user"

ALTER TABLE BANNER_banners drop CONSTRAINT BANNER_BANNER_USER_FK;
alter table BANNER_banners drop column user_id;

ALTER TABLE TOYSHOP_PRODUCTS drop CONSTRAINT TOYSHOP_PRODUCT_USER_FK;
alter table TOYSHOP_PRODUCTS drop column user_id;

ALTER TABLE WIKI_ARTICLES drop CONSTRAINT WIKI_ARTICLE_USER_FK;
alter table WIKI_ARTICLES drop column user_id;


-- domains refactor
alter table domains add column meta character varying(500);

update domains set meta='style=html5,layout-modern; resolve=site,currency' where domain_site='toy';
update domains set meta='style=html5,layout-modern; resolve=site,currency' where domain_site='me';
update domains set meta='style=html5,layout-modern' where domain_site='cft';
update domains set meta='style=html5,layout-modern; resolve=site,currency' where domain_site='cp';
update domains set meta='style=html5,layout-modern' where domain_site='bol';
update domains set meta='style=html5,layout-modern' where domain_site='ztc';
update domains set meta='style=html5,layout-modern' where domain_site='ebr';
update domains set meta='style=html5,layout-modern' where domain_site='dhm';
update domains set meta='style=html5,layout-modern; resolve=site,currency' where domain_site='esc';

alter table domains drop column default_site;
alter table domains drop column resolve_site;
alter table domains drop column default_currency;
alter table domains drop column resolve_currency;
alter table domains drop column style;


-- user refactor
alter table users drop column work_number;
alter table users drop column mobile_number;


-- URL to CP
insert into SETTINGS (id, key, value, site) values (nextval('SETTINGS_SEQ'), 'MainURL', 'http://cpanel.EXAMPLE.ORG', 'cp1');


-- banner refactor
alter table banner_banners add column meta character varying(500);

alter table banner_banners drop column new_window;
alter table banner_banners drop column position;
alter table banner_banners drop column languages;


-- submit users to Solr
insert into SETTINGS (id, key, value, site) values (nextval('SETTINGS_SEQ'), 'SolrSubmitUsersTask', 'sleep=1', 'sys');
insert into SETTINGS (id, key, value, site) values (nextval('SETTINGS_SEQ'), 'SolrSubmitUsersFlag', '0', 'sys');


-- redundant settings
delete from settings where key = 'SolrEnabledFlag';


-- (*** ignore error ***) missing constrains (ignore if present)
ALTER TABLE BANNER_banners
  ADD CONSTRAINT BANNER_banner_pack_fk FOREIGN KEY (pack_id)
      REFERENCES BANNER_banner_packs (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION;

-- (*** ignore error ***) redundant tables (ignore if no tables)
drop table satchel_label_map;
drop table satchel_file_map;
drop table satchels;
