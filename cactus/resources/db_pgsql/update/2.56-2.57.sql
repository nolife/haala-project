-- abyss rework
delete from abyss_entries;
alter table abyss_entries add column ref character varying(100) NOT NULL;
alter table abyss_entries add column pack character varying(100);
alter table abyss_entries add column keep bigint NOT NULL;

-- database time
CREATE OR REPLACE FUNCTION timestamp_fn() RETURNS timestamp AS $$
DECLARE
    ts timestamp without time zone;
BEGIN
    select current_timestamp into ts;
    return ts;
END;
$$ LANGUAGE plpgsql;
