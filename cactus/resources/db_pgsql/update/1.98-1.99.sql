
-- changed control panel URL and pages from 'manager' to 'cp' (need to re-import feed)
delete from files where (folder = 'web/css' or folder like 'web/css/%') and site in ('cp1','cp2');
delete from files where (folder = 'web/ftl' or folder like 'web/ftl/%') and site in ('cp1','cp2');
delete from files where (folder = 'web/js' or folder like 'web/js/%') and site in ('cp1','cp2');
delete from files where (folder = 'web/gfx' or folder like 'web/gfx/%') and site in ('cp1','cp2');


-- Toyshop pack: Picture relation changed from "one-to-many" to "many-to-many"

CREATE TABLE TOYSHOP_product_picture_map
(
  product_id bigint NOT NULL,
  picture_id bigint NOT NULL
)
WITHOUT OIDS;

ALTER TABLE TOYSHOP_product_picture_map
  ADD CONSTRAINT TOYSHOP_picture_product_fk FOREIGN KEY (product_id)
      REFERENCES TOYSHOP_products (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION;

ALTER TABLE TOYSHOP_product_picture_map
  ADD CONSTRAINT TOYSHOP_product_picture_fk FOREIGN KEY (picture_id)
      REFERENCES pictures (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION;

insert into TOYSHOP_product_picture_map (
  select owner_id, id from pictures where owner_type = 101
);


-- Wiki pack: Attachment relation changed from "one-to-many" to "many-to-many"

CREATE TABLE WIKI_article_attachment_map
(
  article_id bigint NOT NULL,
  attachment_id bigint NOT NULL
)
WITHOUT OIDS;

ALTER TABLE WIKI_article_attachment_map
  ADD CONSTRAINT WIKI_attachment_article_fk FOREIGN KEY (article_id)
      REFERENCES WIKI_articles (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION;

ALTER TABLE WIKI_article_attachment_map
  ADD CONSTRAINT WIKI_article_attachment_fk FOREIGN KEY (attachment_id)
      REFERENCES attachments (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION;

insert into WIKI_article_attachment_map (
  select owner_id, id from attachments where owner_type = 102
);


-- Picture relation changed from "one-to-many" to "many-to-many"

alter table pictures drop column owner_id;
alter table pictures drop column owner_type;
delete from settings where key = 'PictureConstants' and site = 'sys';


-- Attachment relation changed from "one-to-many" to "many-to-many"

alter table attachments drop column owner_id;
alter table attachments drop column owner_type;
drop constraint attachments_ind1;
delete from settings where key = 'AttachmentConstants' and site = 'sys';
