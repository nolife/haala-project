
-- home JS/CSS/FTL moved to root folder
update files set folder = 'web/js/pages' where folder = 'web/js/pages/home';
update files set folder = 'web/ftl/pages' where folder = 'web/ftl/pages/home';
update files set folder = 'web/css/pages' where folder = 'web/css/pages/home';

-- labels renamed
delete from labels where key like 'downtP.%';
delete from labels where key like 'browsP.%';

