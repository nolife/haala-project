
-- remove banner
delete from settings where key in ('SolrSubmitBannersTask', 'SolrSubmitBannersFlag');
delete from roles where role = 'ROLE_BANNER';
delete from files where name = 'BannerF.js';
delete from files where name = 'account-nav-banners.gif';
delete from labels where key in ('01account.028', '01account.029');
delete from labels where key like 'b.%';
delete from files where folder like 'b/%';

DROP TABLE banner_domain_map CASCADE;

DROP TABLE banner_options CASCADE;
DROP SEQUENCE banner_options_seq;

DROP TABLE banners CASCADE;
DROP SEQUENCE banners_seq;


-- cleanup
delete from files where folder like '%spam-manager%';
delete from files where folder like '%manager/spam%';

delete from files where folder like '%banner-manager%';
delete from files where folder like '%manager/banner%';

delete from files where folder like '%user-manager%';
delete from files where folder like '%manager/user%';
