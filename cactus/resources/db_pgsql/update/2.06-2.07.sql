
-- Scalability
insert into SETTINGS (id, key, value, site) values (nextval('SETTINGS_SEQ'), 'NodeHeartbeatTask', 'sleep=1', 'sys');
insert into SETTINGS (id, key, value, site) values (nextval('SETTINGS_SEQ'), 'NodeMaintenanceTask', 'sleep=1, dead=5m', 'sys');
