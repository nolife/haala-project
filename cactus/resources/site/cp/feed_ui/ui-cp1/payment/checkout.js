;(function($) {

  Haala.Misc.onDocReady(function() {
    initPaypal()
  })


  function initPaypal() {
    var ivars = Haala.Misc.evalJSON("#page-ivars")

    function msgFn(/*str*/ msg, /*{}*/ tokens) {
      return Haala.Misc.messageMapping("#page-messages", msg, tokens)
    }

    /* there is only PayPal option at the moment */
    if (ivars.paypal_enabled == "n") {
      $("#content .payment-status").html(msgFn("payment-disabled"))
      return
    }

    function onSuccess() {
      $("#content .checkout-paypal").hide()
      $("#content .payment-status").html(msgFn("payment-ok"))
    }

    function onError() {
      $("#content .checkout-paypal").hide()
      $("#content .payment-status").html(msgFn("payment-no"))
    }

    Haala.Misc.loadScript("https://www.paypalobjects.com/api/checkout.js", function() {
      $("#content .checkout-paypal .status").hide()

      paypal.Button.render({
          env: ivars.paypal_env, // 'production|sandbox' environment

          client: { // API key for PayPal REST App
              sandbox:    ivars.paypal_client,
              production: ivars.paypal_client
          },

          payment: function() {
              var env    = this.props.env
              var client = this.props.client

              return paypal.rest.payment.create(env, client, {
                  transactions: [
                      {
                          amount: { total: ivars.total, currency: ivars.currency }, // Note: must match the PayPal account currency
                          custom: "",                                               // Optional custom field
                          invoice_number: "ref="+ivars.ref                          // Note: must be unique, can be processed only once
                      }
                  ]
              })
          },

          commit: true, // Optional: show a 'Pay Now' button in the checkout flow

          onAuthorize: function(data, actions) {
            // Optional: display a confirmation page here
            return actions.payment.execute()
              .then(onSuccess) // Show a success page to the buyer
              .catch(onError)  // Show an error page to the buyer
          }

      }, '#checkout-paypal-button')

    })
  }

})(jQuery);
