<#import "/web/common.ftl" as c >
<#import "/web/common-htm.ftl" as htm >
<#include "/web/macros.ftl:"+currentSite>

<#if paymentEntry??>

  <div class="details">
    <p><@label "001"/> <span>${paymentEntry.abyss().ref()}</span></p>
    <h2><@label "002"/> <span><@h.paymentEntryProperty entry=paymentEntry property="price" template="{sign} {amount}" /></span></h2>
  </div>

  <@h.paymentPaypalConfig var="paypal_enabled" property="enabled" />
  <#if paypal_enabled == "y" >
    <div class="checkout-paypal">
      <p class="status"><@label "004"/></p>
      <div id="checkout-paypal-button"></div>
    </div>
  </#if>

  <p class="payment-status"></p>

  <@h.paymentEntryProperty var="total"    entry=paymentEntry property="price" template="{amount}" />
  <@h.paymentEntryProperty var="currency" entry=paymentEntry property="price" template="{currency}" />
  <@h.paymentPaypalConfig  var="paypal_env"     property="env" />
  <@h.paymentPaypalConfig  var="paypal_client"  property="client" />
  <@htm.nodiv "page-ivars">
    {
      ref: "${paymentEntry.ref()}", total: "${total}", currency: "${currency}",
      paypal_enabled: "${paypal_enabled}", paypal_env: "${paypal_env}", paypal_client: "${paypal_client}"
    }
  </@>

  <@htm.nodiv "page-messages">
    <div data-key="payment-disabled"><@label "005"/></div>
    <div data-key="payment-ok"><@label "006"/></div>
    <div data-key="payment-no"><@label "007"/></div>
  </@>

<#else>

  <@label "003"/>

</#if>
