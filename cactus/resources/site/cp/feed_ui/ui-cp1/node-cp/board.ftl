<#import "/web/common.ftl" as c >
<#import "/web/common-htm.ftl" as htm >
<#include "/web/macros.ftl:"+currentSite >

<#macro x_nodes >
  <div class="cell">
    <p><@label "002"/><p>
  </div>

  <@h.listNodeHeartbeats ; heartbeat, n >
    <div class="cell"></div>
    <div class="cell">
      <label><strong># ${n+1}</strong></label>
      <@h.nodeHeartbeatProperty var="host" property="host" heartbeat=heartbeat />
      <a href="view.htm?host=${host}">${host}</a>
    </div>
    <div class="cell">
      <label><@label "015"/></label>
      <p><@h.nodeHeartbeatProperty property="status" heartbeat=heartbeat /></p>
    </div>
    <div class="cell">
      <label><@label "016"/></label>
      <p><@h.nodeHeartbeatProperty property="beat" heartbeat=heartbeat template="{ago} ago" /></p>
    </div>
  </@>
</#macro>

<div class="form-panel">
  <h3 class="cell"><@label "001"/></h3>
  <div class="cell">
    <label><@label "004"/></label>
    <p><@h.nodeProperty property="uptime" /></p>
  </div>
  <div class="cell">
    <label><@label "005"/></label>
    <p><@h.nodeProperty property="timestamp" template="{date}" /></p>
  </div>
  <div class="cell">
    <label><@label "006"/></label>
    <p><@h.nodeProperty property="dbtime" template="{date}" /></p>
  </div>
  <div class="cell">
    <label><@label "007"/></label>
    <p><@h.nodeProperty property="hostname" /></p>
  </div>
  <div class="cell">
    <label><@label "008"/></label>
    <p><@h.nodeProperty property="ip" /></p>
  </div>
  <div class="cell">
    <label><@label "009"/></label>
    <p><@h.nodeProperty property="mac" /></p>
  </div>
  <div class="cell">
    <label><@label "010"/></label>
    <p><@h.nodeProperty property="memory" template="Used {used} of {total} (max {max})" /></p>
  </div>
  <div class="cell">
    <label><@label "011"/></label>
    <p><@h.nodeProperty property="requests" /></p>
  </div>

  <h3 class="cell"><@label "012"/></h3>
  <div class="cell">
    <label><@label "013"/></label>
    <p><@h.nodeProperty property="fstime" template="{ago} ago" /></p>
  </div>
  <div class="cell">
    <label><@label "014"/></label>
    <p><@h.nodeProperty property="filesystem" template="{files} files occupy {size} (free {free})" /></p>
  </div>

  <h3 class="cell">Nodes</h3>
  <@h.nodeProperty var="scaled" property="scaled" />
  <#if scaled == "y"> 
    <@x_nodes />
  <#else>
    <div class="cell">
      <p><@label "003"/><p>
    </div>
  </#if>
</div>

<@htm.nodiv "page-ivars">
  { nodeId: "<@h.nodeProperty property="host" />" }
</@>

<@htm.nodiv "context-navigation-content">
  <a class="send-email" href="#" title="<@label "018"/>" ><@label "017"/></a>
</@>

<@htm.nodiv "page-messages">
  <div data-key="dialog-send-email"><@label "019"/></div>
</@>
