
(function($) {

  Haala.Misc.onDocReady(function() {
    initNavigation()
  })


  function initNavigation() {
    $("#context-navigation").kdxMenu({
      items: [
        { element: "#context-navigation .send-email",
          gotoFn: function(/*json*/ item, /*json*/ model) {
            sendEmail()
          }}
      ]
    })
  }


  function sendEmail() {
    var ivars = Haala.Misc.evalJSON("#page-ivars")

    function msgFn(/*str*/ msg, /*{}*/ tokens) {
      return Haala.Misc.messageMapping("#page-messages", msg, tokens)
    }

    Haala.AJAX.call(null, NodeF.sendEmail, { itemId: ivars.nodeId }, function(data) {
      Haala.Messages.showOK(msgFn("dialog-send-email", { from: data.result.from, to: data.result.to }), msgFn("title-info"));
    })
  }

})(jQuery);
