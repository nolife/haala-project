<#import "/web/common.ftl" as c >
<#import "/web/common-htm.ftl" as htm >
<#include "/web/macros.ftl:"+currentSite >

<#assign hostParam ><@h.requestParamValue name="host" /></#assign>

<div class="form-panel">

  <@h.nodeHeartbeatProperty var="host" property="host" host=hostParam />
  <h3 class="cell"><@label "001"/></h3>
  <#if host??>
    <div class="cell">
      <label><@label "003"/></label>
      <p><@h.nodeHeartbeatProperty property="status" host=host /></p>
    </div>
    <div class="cell">
      <label><@label "004"/></label>
      <p><@h.nodeHeartbeatProperty property="beat" host=host template="{ago} ago" /></p>
    </div>
  <#else>
    <div class="cell">
      <p><@label "005"/></p>
    </div>
  </#if>

  <@h.nodeSummaryProperty var="host" property="host" host=hostParam />
  <h3 class="cell"><@label "006"/></h3>
  <#if host??>
    <div class="cell">
      <label><@label "008"/></label>
      <p><@h.nodeSummaryProperty property="dbtime" host=host template="{ago} ago" /></p>
    </div>
    <div class="cell">
      <label><@label "009"/></label>
      <p><@h.nodeSummaryProperty property="uptime" host=host /></p>
    </div>
    <div class="cell">
      <label><@label "010"/></label>
      <p><@h.nodeSummaryProperty property="timestamp" host=host template="{date}" /></p>
    </div>
    <div class="cell">
      <label><@label "011"/></label>
      <p><@h.nodeSummaryProperty property="dbtime" host=host template="{date}" /></p>
    </div>
    <div class="cell">
      <label><@label "012"/></label>
      <p><@h.nodeSummaryProperty property="hostname" host=host /></p>
    </div>
    <div class="cell">
      <label><@label "013"/></label>
      <p><@h.nodeSummaryProperty property="ip" host=host /></p>
    </div>
    <div class="cell">
      <label><@label "014"/></label>
      <p><@h.nodeSummaryProperty property="mac" host=host /></p>
    </div>
    <div class="cell">
      <label><@label "015"/></label>
      <p><@h.nodeSummaryProperty property="memory" host=host template="Used {used} of {total} (max {max})" /></p>
    </div>
    <div class="cell">
      <label><@label "016"/></label>
      <p><@h.nodeSummaryProperty property="requests" host=host /></p>
    </div>
  <#else>
    <div class="cell">
      <p><@label "007"/></p>
    </div>
  </#if>

</div>

<@htm.nodiv "page-ivars">
  { nodeId: "<@h.nodeHeartbeatProperty property="host" host=hostParam />" }
</@>

<@htm.nodiv "context-navigation-content">
  <a class="send-email" href="#" title="<@label "01manNodeBoard.018"/>" ><@label "01manNodeBoard.017"/></a>
</@>

<@htm.nodiv "page-messages">
  <div data-key="dialog-send-email"><@label "01manNodeBoard.019"/></div>
</@>

<@htm.nodiv "quick-navigation-content">
  <a href="board.htm"><@label "002"/></a>
</@>
