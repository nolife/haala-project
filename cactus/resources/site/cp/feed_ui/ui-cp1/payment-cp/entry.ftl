<#import "/web/common.ftl" as c >
<#import "/web/common-htm.ftl" as htm >
<#include "/web/macros.ftl:"+currentSite >

<#if paymentEntry??>

  <div class="form-panel">
    <h3 class="cell"><@label "001"/></h3>
    <div class="two-column">
      <div class="cell left">
        <label><@label "004"/></label>
        <p>${paymentEntry.ref()}}</p>
      </div>
      <div class="cell right">
        <label><@label "012"/></label>
        <p><@h.paymentEntryProperty entry=paymentEntry property="created" /></p>
      </div>
      <div class="cell left">
        <label><@label "005"/></label>
        <p><@h.paymentEntryProperty entry=paymentEntry property="price" template="{currency} {amount}" /><p>
      </div>
      <div class="cell right">
        <label><@label "013"/></label>
        <p><@h.paymentEntryProperty entry=paymentEntry property="expire" /></p>
      </div>
    </div>
    <div class="cell">
      <label><@label "006"/></label>
      <p>${paymentEntry.url()!""}</p>
    </div>
    <div class="nofloat"></div>
  </div>

  <div id="entries-list" class="item-list">
    <div class="head">
      <p class="column-1"><@label "007"/></p>
      <p class="column-2"><@label "008"/></p>
      <p class="column-3"><@label "014"/></p>
      <p class="column-4"><@label "009"/></p>
      <p class="column-5"><@label "010"/></p>
      <p class="column-6"><@label "011"/></p>
    </div>
    <div class="rows"></div>
    <div class="foot">
      <div class="page-spread"></div>
    </div>
  </div>

  <@htm.nodiv "quick-navigation-content">
    <a href="entries.htm"><@label "002"/></a>
  </@>

  <@htm.nodiv "context-navigation-content">
    <a class="add" href="#" title="<@label "019"/>"><@label "018"/></a>
    <a class="push" href="#" title="<@label "021"/>"><@label "020"/></a>
  </@>

  <@htm.nodiv "page-ivars">
    { entryId: ${paymentEntry.abyss().id()}, ref: "${paymentEntry.abyss().ref()}", pack: "${paymentEntry.abyss().pack()}" }
  </@>

  <@htm.nodiv "page-messages">
    <div data-key="edit-title"><@label "015"/></div>
    <div data-key="add-title"><@label "022"/></div>
    <div data-key="confirm-push"><@label "023"/></div>
    <div data-key="button-ok"><@label "btn.ok"/></div>
    <div data-key="button-cancel"><@label "btn.cancel"/></div>
  </@>

  <@htm.nodiv "page-templates">

    <div id="edit-frame">
      <div class="form-panel">
        <div class="cell value">
          <label><@label "016"/></label>
          <textarea></textarea>
          <p class="error"></p>
        </div>
        <div class="cell meta">
          <label><@label "017"/></label>
          <textarea></textarea>
          <p class="error"></p>
        </div>
      </div>
    </div>

  </@>

<#else>

  <@label "003"/>

</#if>
