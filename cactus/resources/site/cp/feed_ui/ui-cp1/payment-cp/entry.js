;(function($) {

  Haala.Misc.onDocReady(function() {
    initNavigation()
    initEntriesList()
  })


  function initNavigation() {
    var ivars = Haala.Misc.evalJSON("#page-ivars")

    function msgFn(/*str*/ msg, /*{}*/ tokens) {
      return Haala.Misc.messageMapping("#page-messages", msg, tokens)
    }

    $("#context-navigation").kdxMenu({
      items: [
        { element: "#context-navigation .add",
          gotoFn: function(/*{}*/ item, /*{}*/ model) {
            editFrame()
        }},
        { element: "#context-navigation .push",
          gotoFn: function(/*{}*/ item, /*{}*/ model) {
            Haala.Messages.showYN(msgFn("confirm-push"), "title-confirm", function() {
              Haala.AJAX.call("body", PaymentF.push, { itemId: ivars.entryId }, function() {
                document.location = document.location.href
              })
            })
        }}
      ]
    })
  }


  function initEntriesList() {
    var ivars = Haala.Misc.evalJSON("#page-ivars")

    function rowFn(model, /*{}*/ vo) {
      return vo ?
        $((
            "<div><a href='#'>" +
              "<p class='column-1'>{ref}</p>" +
              "<p class='column-2'>{pack}</p>" +
              "<p class='column-3'>{value}</p>" +
              "<p class='column-4'>{meta}</p>" +
              "<p class='column-5'>{updated}</p>" +
              "<p class='column-6'>{keep}</p>" +
            "</a></div>"
          ).replace("{ref}", vo.ref)
           .replace("{pack}", vo.pack)
           .replace("{value}", vo.value)
           .replace("{meta}", vo.meta)
           .replace("{updated}", vo.created)
           .replace("{keep}", vo.keep)
        ).click(function(event) {
          event.preventDefault()
          Haala.AJAX.call("body", AbyssF.load, { itemId: vo.id }, function(data) {
            editFrame(data.result)
          })
        }) :
        $("<div></div>")
    }

    function loadItemsFn(model) {
      model.userData.facadeSearch.command.target = 2
      model.userData.facadeSearch.command.params = [ ivars.ref, ivars.pack ]
      model.userData.facadeSearch.command.max = 999
      model.userData.facadeSearch.call(AbyssF.search, "#entries-list")
    }

    new Haala.TableBuilder({ facadeSearch: true })
      .plugin( $('#entries-list .rows').listing({ delayLoad: true, makeRow: rowFn, loadItems: loadItemsFn, onRender: Haala.Code.stretch }) )
      .build()

  }

  function editFrame(/*{}*/ vo) {
    var ivars = Haala.Misc.evalJSON("#page-ivars")

    function msgFn(/*str*/ msg, /*{}*/ tokens) {
      return Haala.Misc.messageMapping("#page-messages", msg, tokens)
    }

    function editEntry(/*{}*/ command) {
      Haala.AJAX.call("body", AbyssF.edit, command,
        function() {
          $("#entries-list .rows").listing("page")
        })
    }

    function addEntry(/*{}*/ command) {
      Haala.AJAX.call("body", AbyssF.add, command,
        function() {
          $("#entries-list .rows").listing("page")
        })
    }

    function onSubmitFn(model, /*{}*/ values) {
      var command = Haala.Misc.collectInput(values, ["meta", "value"])
      if (model.editVO) {
        command.entryId = model.editVO.id
        editEntry(command)
      } else {
        command.parentId = ivars.entryId
        addEntry(command)
      }
      $("#edit-frame").dialog("close")
    }

    if (!this.editFrameBuilt) { // build only once
      this.editFrameBuilt = true

      var panelOptions = {
        fields: [{ name: "meta",  selector: ".meta textarea"  },
                 { name: "value", selector: ".value textarea" }],
        onSubmit: onSubmitFn
      }

      new Haala.FormBuilder()
        .plugin( $('#edit-frame').formpanel(panelOptions) )
        .validate( new Haala.UIValidator(Haala.UIValidator.Integ) )
        .build()
    }

    $('#edit-frame').formpanel("model", { editVO: vo || null })

    if (vo) {
      $("#edit-frame .meta textarea").val(vo.meta)
      $("#edit-frame .value textarea").val(vo.value)
    }

    $("#edit-frame").dialog({
      zIndex: 70, width: 900, height: 300, resizable: false,
      buttons: [
        { text: msgFn("button-ok"), click: function() {
          $('#edit-frame').formpanel("submit")
        }},
        { text: msgFn("button-cancel"), click: function() {
          $(this).dialog("close")
        }}],
      close: function() {
        $('#edit-frame').formpanel("reset")
        $("#edit-frame textarea").val("")
      }
    })

    $("#edit-frame").dialog("option", "title", msgFn(vo ? "edit-title" : "add-title"))
  }

})(jQuery);
