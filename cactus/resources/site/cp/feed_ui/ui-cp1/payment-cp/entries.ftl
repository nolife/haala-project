<#import "/web/common.ftl" as c >
<#import "/web/common-htm.ftl" as htm >
<#include "/web/macros.ftl:"+currentSite >

<div id="entries-list" class="item-list" data-state="${perPage},${searchState!}">
  <div class="filter">
    <@h.searchFilterParam var="tmpval" index=1 />
    <input class="ref" type="text" value="${tmpval!}">
    <@h.searchFilterParam var="tmpval" index=2 />
    <input class="pack" type="text" value="${tmpval!}">
    <@h.searchFilterParam var="tmpval" index=3 />
    <input class="username" type="text" value="${tmpval!}">
    <span class="on" title="<@label "005"/>" ></span>
    <span class="off" title="<@label "006"/>" ></span>
  </div>
  <div class="head">
    <a href="#" class="column-1 ref"><@label "002"/><span class="arrow"></span></a>
    <a href="#" class="column-2 pack"><@label "003"/><span class="arrow"></span></a>
    <a href="#" class="column-3 username"><@label "004"/><span class="arrow"></span></a>
    <p class="column-4"></p>
    <p class="column-5"><@label "001"/></p>
    <p class="column-6"><@label "009"/></p>
    <p class="column-7"><@label "007"/></p>
    <p class="column-8"><@label "008"/></p>
  </div>
  <div class="rows"></div>
  <div class="foot">
    <div class="page-spread"></div>
  </div>
</div>
