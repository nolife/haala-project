;(function($) {

  Haala.Misc.onDocReady(function() {
    initEntriesList()
  })


  function initEntriesList() {

    function rowFn(model, /*{}*/ vo) {
      return vo ?
        $((
            "<div><a href='entry.htm?id={id}'>" +
              "<p class='column-1'>{ref}</p>" +
              "<p class='column-2'>{pack}</p>" +
              "<p class='column-3'>{username}</p>" +
              "<p class='column-4'></p>" +
              "<p class='column-5'>{total}</p>" +
              "<p class='column-6'>{date}</p>" +
              "<p class='column-7'>{expire}</p>" +
              "<p class='column-8'>{active}</p>" +
            "</a></div>"
          ).replace("{id}", vo.id)
           .replace("{ref}", vo.ref)
           .replace("{pack}", vo.pack)
           .replace("{username}", vo.username)
           .replace("{total}", vo.currency+" "+vo.amount)
           .replace("{date}", vo.created)
           .replace("{expire}", vo.expire)
           .replace("{active}", "<span class='"+(vo.active ? "active" : "inactive")+"'></span>")
        ) :
        $("<div></div>")
    }

    function loadItemsFn(model) {
      model.userData.facadeSearch.command.target = 1
      model.userData.facadeSearch.call(PaymentF.search, "#entries-list")
    }

    new Haala.TableBuilder({ facadeSearch: true })
      .plugin( $('#entries-list .head').sortcolumn({ columns: [{ selector: ".ref", name: "ref" }, { selector: ".pack", name: "pac" }, { selector: ".username", name: "usr" }]}) )
      .plugin( $('#entries-list .foot .page-spread').pagination({ nextprev: "always" }) )
      .plugin( $('#entries-list .rows').listing({ delayLoad: true, makeRow: rowFn, loadItems: loadItemsFn, onRender: Haala.Code.stretch }) )
      .state( $("#entries-list").attr("data-state") )
      .build()

    $("#entries-list .filter .on").click(function(event) {
      event.preventDefault()
      var command = { target: 1, params: [
        $("#entries-list .filter input.ref").val(),
        $("#entries-list .filter input.pack").val(),
        $("#entries-list .filter input.username").val()
      ] }
      Haala.AJAX.call(null, PaymentF.filter, command, function(data) {
        $('#entries-list .rows').listing("page", 1)
      })
    })

    $("#entries-list .filter .off").click(function(event) {
      event.preventDefault()
      var command = { target: 1, params: [] }
      Haala.AJAX.call(null, PaymentF.filter, command, function(data) {
        $("#entries-list .filter input").val("")
        $('#entries-list .rows').listing("page", 1)
      })
    })

  }

})(jQuery);
