<#import "/web/common.ftl" as c >
<#import "/web/common-htm.ftl" as htm >
<#include "/web/macros.ftl:"+currentSite>

<#if messageEntry??>

  <@h.messageEntryProperty var="template" entry=messageEntry property="template" />
  <#include "/web/pages/cp/message/${template}" >

  <@htm.nodiv "quick-navigation-content">
    <#if messageEntry.user().ref()=="*"> <#assign trg="?trg=2" /> </#if>
    <a href="/my/message/messages.htm${trg!}"><@label "005"/></a>
  </@>

<#else>

  <@label "x.nomsg"/>

</#if>