<#import "/web/common.ftl" as c >
<#import "/web/common-htm.ftl" as htm >
<#include "/web/macros.ftl:"+currentSite>

<div id="message-list" class="item-list" data-state="${perPage},${searchState!}">
  <div class="head">
    <a href="#" class="column-1 read">&nbsp;<span class="arrow"></span></a>
    <a href="#" class="column-2 date"><@label "003"/><span class="arrow"></span></a>
    <a href="#" class="column-3 subject"><@label "004"/><span class="arrow"></span></a>
  </div>
  <div class="rows"></div>
  <div class="foot">
    <div class="page-spread"></div>
  </div>
</div>

<@htm.nodiv "context-navigation-content">
  <a class="start-fwd-email" href="#" title="<@label "007"/>" ><@label "005"/></a>
  <a class="stop-fwd-email" href="#" title="<@label "008"/>" ><@label "006"/></a>
  <a class="delete-read" href="#" title="<@label "010"/>" ><@label "009"/></a>
</@>

<@htm.nodiv "page-ivars">
  { trg: ${trg}, fwdEmail: "${fwdEmail}" }
</@>

<@htm.nodiv "page-messages">
  <div data-key="confirm-start-fwd-email"><@label "013"/></div>
  <div data-key="confirm-stop-fwd-email"><@label "014"/></div>
  <div data-key="confirm-delete-read"><@label "015"/></div>
</@>
