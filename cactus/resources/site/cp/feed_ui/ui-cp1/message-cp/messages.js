;(function($) {

  Haala.Misc.onDocReady(function() {
    initNavigation()
    initMessageList()
  })


  function initNavigation() {
    var ivars = Haala.Misc.evalJSON("#page-ivars")
    function msgFn(/*string*/ msg, /*json*/ tokens) {
      return Haala.Misc.messageMapping("#page-messages", msg, tokens)
    }

    function submitHandler(/*fn*/ callTarget, /*int*/ target, /*string*/ msg) {
      Haala.Messages.showYN(msgFn(msg), "title-confirm", function() {
        Haala.AJAX.call("body", callTarget, { target: target },
          function() {
            document.location = document.location.href
            return Haala.AJAX.DO_NOTHING
          })
      })
    }

    if (ivars.fwdEmail == "y") $("#context-navigation .stop-fwd-email").show()
    else $("#context-navigation .start-fwd-email").show()

    $("#context-navigation").kdxMenu({
      items: [
        { element: "#context-navigation .start-fwd-email",
            gotoFn: function(/*json*/ item, /*json*/ model) {
              submitHandler(ivars.trg == "2" ? MessageF.asetup : MessageF.setup, 1, "confirm-start-fwd-email")
            }},
        { element: "#context-navigation .stop-fwd-email",
            gotoFn: function(/*json*/ item, /*json*/ model) {
              submitHandler(ivars.trg == "2" ? MessageF.asetup : MessageF.setup, 2, "confirm-stop-fwd-email")
            }},
        { element: "#context-navigation .delete-read",
            gotoFn: function(/*json*/ item, /*json*/ model) {
              submitHandler(ivars.trg == "2" ? MessageF.aremove : MessageF.remove, 1, "confirm-delete-read")
            }}
      ]
    })
  }


  function initMessageList() {
    var ivars = Haala.Misc.evalJSON("#page-ivars")

    function rowFn(model, /*{}*/ vo) {
      return vo ?
        $((
            "<div><a href='view.htm?ref={ref}'>" +
              "<p class='column-1 {read}'></p>" +
              "<p class='column-2'>{date}</p>" +
              "<p class='column-3'>{subject}</p>" +
            "</a></div>"
          ).replace("{ref}", vo.ref)
           .replace("{read}", vo.read ? "read" : "unread")
           .replace("{date}", vo.created)
           .replace("{subject}", vo.subject)
        ) :
        $("<div></div>")
    }

    function loadItemsFn(model) {
      model.userData.facadeSearch.command.target = ivars.trg
      model.userData.facadeSearch.call(ivars.trg == 2 ? MessageF.asearch : MessageF.search, "#message-list")
    }

    new Haala.TableBuilder({ facadeSearch: true })
      .plugin( $('#message-list .head').sortcolumn({ columns: [{ selector: ".read", name: "read" }, { selector: ".date", name: "date", order: "desc" }, { selector: ".subject", name: "subject" }]}) )
      .plugin( $('#message-list .foot .page-spread').pagination({ nextprev: "always" }) )
      .plugin( $('#message-list .rows').listing({ delayLoad: true, makeRow: rowFn, loadItems: loadItemsFn, onRender: Haala.Code.stretch }) )
      .state( $("#message-list").attr("data-state") )
      .build()

  }

//  function initMessageList() {
//    var ivars = Haala.Misc.evalJSON("#page-ivars")
//
//    var listModel = {
//      sort: {
//        items: [
//          { element: $("#message-list .head .read"), field: "red" },
//          { element: $("#message-list .head .date"), field: "dat", order: "desc" },
//          { element: $("#message-list .head .type"), field: "typ" }
//        ]
//      },
//      loadItemsFn: function(/*json*/ model, /*fn*/ callback) {
//        Haala.AquaList.Integ.loadItemsFn(
//          ivars.trg == "2" ? MessageF.asearch : MessageF.search, { target: ivars.trg }, model, callback);
//      },
//      rowElementFn: function(/*json*/ vo, /*int*/ position, /*json*/ model) {
//        return vo ?
//          $("<div class='data'>" +
//            "  <a href='view.htm?id="+vo.id+"'>" +
//            "    <p class='column-1 "+(vo.read == "1" ? "read" : "unread")+"'></p>" +
//            "    <p class='column-2'>"+vo.created+"</p>" +
//            "    <p class='column-3'>"+vo.type+"</p>" +
//            "  </a>" +
//            "</div>").aclick(function() {
//                        document.location = "view.htm?id="+vo.id;
//                      }) :
//          $("<div class='empty'></div>");
//      }
//    };
//    var list = $("#message-list .rows").aquaList(
//      $.extend(true,
//        Haala.AquaList.Integ.DefaultOptions,
//        Haala.AquaList.Integ.parseState($("#message-list").attr("data-state")),
//        listModel));
//    list.setPageSpread($("#message-list .page-spread").pageSpread(Haala.PageSpread.Integ.DefaultOptions));
//    list.addRenderedFn(Haala.Code.stretch);
//
//  };

})(jQuery);
