<#import "/web/common.ftl" as c >
<#import "/web/common-htm.ftl" as htm >
<#include "/web/macros.ftl:"+currentSite>

<div class="form-panel">
  <div class="two-column">
    <div class="cell left">
      <label><@label "002"/></label>
      <p><@h.messageEntryProperty entry=messageEntry property="subject" /></p>
    </div>
    <div class="cell right">
      <label><@label "022"/></label>
      <p><@h.messageEntryProperty entry=messageEntry property="meta.abyssId" /></p>
    </div>
    <div class="cell left">
      <label><@label "003"/></label>
      <p><@h.messageEntryProperty entry=messageEntry property="created" /></p>
    </div>
    <div class="cell right">
      <label><@label "023"/></label>
      <p><@h.messageEntryProperty entry=messageEntry property="meta.paymentRef" /></p>
    </div>
    <div class="cell left">
      <label><@label "020"/></label>
      <p>
        <@h.messageEntryProperty var="userId" entry=messageEntry property="meta.userId" />
        <@h.user var="tmpusr" userId=userId />
        ${tmpusr}.username()
      </p>
    </div>
    <div class="cell right">
      <label><@label "024"/></label>
      <p>
        <@h.messageEntryProperty var="abyssId" entry=messageEntry property="meta.abyssId" />
        <a href="/su/payment/entry.htm?id=${abyssId}"><@label "025" /></a>
      </p>
    </div>
  </div>
  <div class="cell text">
    <p><@h.messageEntryProperty entry=messageEntry property="text" /></p>
  </div>
</div>

<#--
<@htm.nodiv "context-navigation-content">
  <@h.onRole role="admin">
    <@h.messageEntryProperty var="tmpval" entry=messageEntry property="meta.abyssId" />
    <a class="abyss" href="/su/abyss.htm?key=${tmpval}" title="<@label "031"/>" ><@label "026"/></a>
  </@>
</@>
--#>