<#import "/web/common.ftl" as c >
<#import "/web/common-htm.ftl" as htm >
<#include "/web/macros.ftl:"+currentSite>

<div class="form-panel">
  <div class="two-column">
    <div class="cell left">
      <label><@label "002"/></label>
      <p><@h.messageEntryProperty entry=messageEntry property="subject" /></p>
    </div>
    <div class="cell right"></div>
    <div class="cell left">
      <label><@label "003"/></label>
      <p><@h.messageEntryProperty entry=messageEntry property="created" /></p>
    </div>
    <div class="cell right"></div>
  </div>
  <div class="cell text">
    <p><@h.messageEntryProperty entry=messageEntry property="html" /></p>
  </div>
</div>
