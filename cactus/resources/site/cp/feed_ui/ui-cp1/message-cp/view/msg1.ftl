<#import "/web/common.ftl" as c >
<#import "/web/common-htm.ftl" as htm >
<#include "/web/macros.ftl:"+currentSite>

<div class="form-panel">
  <div class="two-column">
    <div class="cell left">
      <label><@label "002"/></label>
      <p><@h.messageEntryProperty entry=messageEntry property="subject" /></p>
    </div>
    <div class="cell right">
      <label><@label "007"/></label>
      <p><@h.messageEntryProperty entry=messageEntry property="senderName" /></p>
    </div>
    <div class="cell left">
      <label><@label "003"/></label>
      <p><@h.messageEntryProperty entry=messageEntry property="created" /></p>
    </div>
    <div class="cell right">
      <label><@label "008"/></label>
      <p><@h.messageEntryProperty entry=messageEntry property="senderEmail" /></p>
    </div>
  </div>
  <div class="cell text">
    <p><@h.messageEntryProperty entry=messageEntry property="text" /></p>
  </div>
</div>

<#--
<@htm.nodiv "context-navigation-content">
  <@h.onRole role="admin">
    <@h.messageHeaderValue var="tmpval" key="email" message=message/>
    <a class="email" href="/su/message/email.htm?to=${tmpval}" title="<@label "032"/>" ><@label "028"/></a>
  </@>
</@>
--#>