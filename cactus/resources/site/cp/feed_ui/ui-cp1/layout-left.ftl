
<#-- Page context navigation -->
<div id="context-navigation" class="navigation-pane small"></div>
<div class="nofloat"></div>


<#-- Include packs left column (priority area) -->
<@packContent "left-column-top"/>

<#-- Recent pages and page links -->
<@htm.nodiv "quick-navigation-wrap">
  <div id="quick-navigation" class="quick-navigation">
    <p><@label "nav.quick"/></p>
  </div>
  <div id="recently-visited" class="quick-navigation">
    <p><@label "nav.recnt"/></p>
    <@h.listRecentLinks ; link>
      <a href="${link.url()}">${link.text()}</a>
    </@>
  </div>
  <div class="nofloat"></div>
</@>

<#-- Include packs left column (regular) -->
<@packContent "left-column"/>
