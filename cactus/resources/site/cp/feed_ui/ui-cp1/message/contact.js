
(function($) {

  Haala.Misc.onDocReady(function() {
    initFeedbackForm();
  });


  var initFeedbackForm = function() {
    $("#page-form .submit button").button();
    var msgFn = function(/*string*/ msg, /*json*/ tokens) {
      return Haala.Misc.messageMapping("#page-messages", msg, tokens);
    };

    var finp = Haala.Util.panelFormInput;
    var panel = $("#page-form").kdxPanel(
      $.extend(true, {}, Haala.KdxPanel.Integ.DefaultOptions, {
        input: [
          finp("name",   { require: true }),
          finp("text",   { require: true }),
          finp("email",  { require: true, regex: ["email"] }),
          finp("turing", { userFn: Haala.InputValidator.Integ.turingUserFn })
        ]
      }));

    Haala.Util.formSubmitPanel(panel, function(/*json*/ container) {
      var command = Haala.Misc.collectInput(container,
        ["turing", "name", "email", "text"]);
      feedback(command);
    });

    var feedback = function(/*json*/ command) {
      Haala.AJAX.call("body", MessageF.feedback, command,
        function() {
          Haala.Messages.showOK(msgFn("dialog-text"), msgFn("dialog-title"), function() {
            document.location = "/";
          });
        });
    };

  };

})(jQuery);

