<#import "/web/common.ftl" as c >
<#import "/web/common-htm.ftl" as htm >

<#-- Print captcha image -->
<#macro captcha>
  <img src="<@htm.url path='${turingPath}'/>"/>
</#macro>

<#-- Print page header -->
<#macro pageHeader>
  <@h.onPage index="-,00018,00contact,00login,00reminder">
    <h1>${pageTitle}</h1><span class="arr"></span>
  </@>
</#macro>

<#-- AJAX loading sign -->
<#macro ajaxFrame>
  <div id="ajax-frame">
    <span class="arr"></span>
    <div></div>
    <p></p>
    <span class="arr"></span>
  </div>
</#macro>

<#macro label key tokens="">
  <@c.label key=key tokens=tokens />
</#macro>

<#--
   Include packs content. If the current user has a pack assigned and it is one of the installed packs
   then the pack template is included. The pack-xxx.ftl file must exist and it is included with the specified
   target (eg. abc), which can be verified by macro "<@onPackContent abc>" in the pack-xxx.ftl
-->
<#macro packContent target>
  <@h.onRole role="admin"><#local isAdmin=true /></@h.onRole>
  <@h.listInstalledPacks ; ipack>
    <#local hasPack=false />
    <@h.onPack pack=ipack><#local hasPack=true /></@>
    <#-- either user has an installed pack assigned or she is admin -->
    <#if hasPack || isAdmin!false >
      <#assign includeTarget=target/>
      <#local path="web/pack-${ipack}.ftl"/>
      <@h.onFile path=path><#include "/"+path+":"+currentSite ></@>
    </#if>
  </@>
</#macro>

<#macro onPackContent target>
  <#if includeTarget! == target>
    <#nested />
  </#if>
</#macro>



<#--
  Site tabs for jQuery UI Tabs

  [English] [Russian]

  <div>
    inlude this
    <div id="site-tab-xx1"> ... </div>
    <div id="site-tab-xx2"> ... </div>
  </div>

  If sites source not provided then use root sites to input default multilingual data.
  We will use anothe array of root sites to label the tabs (in case if provided sites
  do not comply with language).
-->
<#macro inputSiteTabs>
<ul>
  <@h.listInputSites var="labels" />
  <@h.listInputSites ; site, n >
    <@h.siteLocale var="lng" key=labels[n] />
    <li><a href="#site-tab-${site}"><@label "lng."+lng /></a></li>
  </@>
</ul>
</#macro>

<#-- Domain version of the inputSiteTabs macro -->
<#macro domainInputSiteTabs domain>
  <ul>
    <@h.listInputSites var="labels" />
    <@h.listDomainInputSites domain=domain ; site, n >
      <@h.siteLocale var="lng" key=labels[n] />
      <li><a href="#site-tab-${site}"><@label "lng."+lng /></a></li>
    </@>
  </ul>
</#macro>

