
(function($) {

  Haala.Misc.onDocReady(function() {
    initPropsForm();
  });




  var initPropsForm = function() {
    var ivars = Haala.Misc.evalJSON("#page-ivars");
    if (!ivars) return;

    $("#page-form .submit button").button();
    $("#page-form .activated input").datepicker({ dateFormat: 'dd/mm/yy' });

    var finp = Haala.Util.panelFormInput;
    var panel = $("#page-form").kdxPanel(
      $.extend(true, {}, Haala.KdxPanel.Integ.DefaultOptions, {
        input: [
          finp("username",  { require: true, regex: ["username"], length: [5] }),
          finp("disabled"),
          finp("activated", { regex: ["date"] })
        ]
      }));

    Haala.Util.formSubmitPanel(panel, function(/*json*/ container) {
      var command = Haala.Misc.collectInput(container, ["username", "disabled", "activated"]);
      command.userId = ivars.userId;

      command.domains = "";
      $("#page-form .domains input:checked").each(function() {
        command.domains += ","+$(this).attr("data-id");
      });
      if (command.domains) command.domains = command.domains.substring(1);

      command.roles = "";
      $("#page-form .roles input:checked").each(function() {
        command.roles += ","+$(this).attr("data-id");
      });
      if (command.roles) command.roles = command.roles.substring(1);

      editProps(command);
    });

    var editProps = function(/*json*/ command) {
      Haala.AJAX.call("body", UserF.editProps, command,
        function() { document.location = "view.htm?id="+ivars.userId; return Haala.AJAX.DO_NOTHING; });
    };

  };

})(jQuery);
