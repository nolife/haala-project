
(function($) {

  Haala.Misc.onDocReady(function() {
    initNavigation()
  })


  function initNavigation() {
    var ivars = Haala.Misc.evalJSON("#page-ivars")
    if (!ivars) return

    $("#context-navigation").kdxMenu({
      items: [
        { element: "#context-navigation .shell",
          gotoFn: function(/*json*/ item, /*json*/ model) {
            Haala.AJAX.call("body", LoginF.shellOn, { userId: ivars.userId },
              function() {
                document.location = "/my/account.htm"
                return Haala.AJAX.DO_NOTHING
              })
          }},
        { element: "#context-navigation .credit",
          gotoFn: function(/*json*/ item, /*json*/ model) {
            creditFrame()
          }},
        { element: "#context-navigation .payment",
          gotoFn: function(/*json*/ item, /*json*/ model) {
            paymentFrame()
          }}
      ]
    })
  }


  function creditFrame() {
    var ivars = Haala.Misc.evalJSON("#page-ivars")

    function msgFn(/*str*/ msg, /*{}*/ tokens) {
      return Haala.Misc.messageMapping("#page-messages", msg, tokens)
    }

    function creditUser(/*{}*/ command) {
      Haala.AJAX.call("body", UserF.credit, command, function() {
        document.location = document.location.href
      })
    }

    function onSubmitFn(model, /*{}*/ values) {
        var command = Haala.Misc.collectInput(values, ["amount", "message"])
        command.userId = ivars.userId
        creditUser(command)
        $("#credit-frame").dialog("close")
    }

    if (!this.creditFrameBuilt) { // build only once
      this.creditFrameBuilt = true

      var panelOptions = {
        fields: [{ name: "amount",  selector: ".amount input", validate: { require: true, regex: ["number"] } },
                 { name: "message", selector: ".message input" }],
        onSubmit: onSubmitFn
      }

      new Haala.FormBuilder()
        .plugin( $('#credit-frame').formpanel(panelOptions) )
        .validate( new Haala.UIValidator(Haala.UIValidator.Integ) )
        .build()
    }

    $("#credit-frame").dialog({
      zIndex: 70, width: 645, height: 200, resizable: false,
      buttons: [
        { text: msgFn("button-ok"), click: function() {
          $('#credit-frame').formpanel("submit")
        }},
        { text: msgFn("button-cancel"), click: function() {
          $(this).dialog("close")
        }}],
      title: msgFn("credit-title"),
      close: function() {
        $('#credit-frame').formpanel("reset")
        $("#credit-frame input").val("")
      }
    })

  }


  function paymentFrame() {
    var ivars = Haala.Misc.evalJSON("#page-ivars")

    function msgFn(/*str*/ msg, /*{}*/ tokens) {
      return Haala.Misc.messageMapping("#page-messages", msg, tokens)
    }

    function createPayment(/*{}*/ command) {
      Haala.AJAX.call("body", PaymentF.create, command,
        function(data) {
          Haala.Messages.showOK(data.result.url, msgFn("title-info"))
        })
    }

    function onSubmitFn(model, /*{}*/ values) {
        var command = Haala.Misc.collectInput(values, ["ref", "username", "currency", "amount", "message"])
        createPayment(command)
        $("#payment-frame").dialog("close")
    }

    if (!this.paymentFrameBuilt) { // build only once
      this.paymentFrameBuilt = true

      var panelOptions = {
        fields: [{ name: "ref",      selector: ".ref input",       validate: { require: true } },
                 { name: "username", selector: ".username input",  validate: { require: true } },
                 { name: "currency", selector: ".currency select", validate: { require: true } },
                 { name: "amount",   selector: ".amount input",    validate: { require: true, regex: ["float"] } },
                 { name: "message",  selector: ".text textarea" }],
        onSubmit: onSubmitFn
      }

      new Haala.FormBuilder()
        .plugin( $('#payment-frame').formpanel(panelOptions) )
        .validate( new Haala.UIValidator(Haala.UIValidator.Integ) )
        .build()
    }

    $("#payment-frame").dialog({
      zIndex: 70, width: 645, height: 300, resizable: false,
      buttons: [
        { text: msgFn("button-ok"), click: function() {
          $('#payment-frame').formpanel("submit")
        }},
        { text: msgFn("button-cancel"), click: function() {
          $(this).dialog("close")
        }}],
      title: msgFn("payment-title"),
      close: function() {
        $('#payment-frame').formpanel("reset")
        $("#payment-frame input, #payment-frame select, #payment-frame textarea").val("")
      }
    })

    $("#payment-frame .ref input").val(ivars.ref)
    $("#payment-frame .username input").val(ivars.username)
    $("#payment-frame .currency select").val(ivars.currency)
  }

})(jQuery);
