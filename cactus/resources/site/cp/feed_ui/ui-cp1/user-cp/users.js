
(function($) {

  Haala.Misc.onDocReady(function() {
    initUserList();
  });



  var initUserList = function() {
    var listModel = {
      sort: {
        items: [
          { element: $("#user-list .head .username"), field: "usr" },
          { element: $("#user-list .head .email"), field: "eml" }
        ]
      },
      loadItemsFn: function(/*json*/ model, /*fn*/ callback) {
        Haala.AquaList.Integ.loadItemsFn(UserF.search, { target: 1 }, model, callback);
      },
      rowElementFn: function(/*json*/ vo, /*int*/ position, /*json*/ model) {
        return vo ?
          $("<div class='data'>" +
            "  <a href='view.htm?id="+vo.id+"'>" +
            "    <p class='column-1'>"+vo.username+"</p>" +
            "    <p class='column-2'>"+vo.email+"</p>" +
            "    <p class='column-3'>"+vo.name+"</p>" +
            "  </a>" +
            "</div>").aclick(function() {
                        document.location = "view.htm?id="+vo.id;
                      }) :
          $("<div class='empty'></div>");
      }
    };
    var list = $("#user-list .rows").aquaList(
      $.extend(true, {},
        Haala.AquaList.Integ.DefaultOptions,
        Haala.AquaList.Integ.parseState($("#user-list").attr("data-state")),
        listModel));
    list.setPageSpread($("#user-list .page-spread").pageSpread(Haala.PageSpread.Integ.DefaultOptions));
    list.addRenderedFn(Haala.Code.stretch);

  };

})(jQuery);

