<#import "/web/common.ftl" as c >
<#import "/web/common-htm.ftl" as htm >
<#include "/web/macros.ftl:"+currentSite>

<#function x_hasDomain domainId>
  <#list user.domains() as d>
    <#if d.id()==domainId><#return true></#if>
  </#list>
  <#return false>
</#function>

<#function x_hasRole roleId>
  <#list user.roles() as r>
    <#if r.id()==roleId><#return true></#if>
  </#list>
  <#return false>
</#function>


<#if user??>

    <@htm.form id="page-form">
      <div class="form-panel">
        <div class="cell username">
          <label><@label "007"/></label>
          <input type="text" maxlength="100" value="${user.username()}"/>
          <p class="error"></p>
        </div>
        <div class="cell activated">
          <label><@label "008"/></label>
          <input type="text" maxlength="100" value="<@h.timestamp ts=user.activated() format="dd/MM/yyyy" />" />
          <p class="error"></p>
        </div>
        <div class="cell disabled">
          <label><@label "002"/></label>
          <@h.userMetaValue var="tmpdis" key="disabled" user=user/>
          <input type="checkbox" <#if tmpdis=="1"> checked="true" </#if> />
          <p class="error"></p>
        </div>
        <div class="cell roles">
          <label><@label "004"/></label>
          <@h.listRoles ; x >
            <p><input data-id="${x.id()}" type="checkbox" <#if x_hasRole(x.id()) > checked="true" </#if> /><span>${x.description()}</span></p>
          </@>
        </div>
        <div class="cell domains">
          <label><@label "003"/></label>
          <@h.listDomains ; x >
            <p><input data-id="${x.id()}" type="checkbox" <#if x_hasDomain(x.id()) > checked="true" </#if> /><span>${x.domain()}</span></p>
          </@>
        </div>
        <div class="cell submit">
          <button><@label "btn.save"/></button>
        </div>
      </div>
    </@>

    <@htm.nodiv "quick-navigation-content">
      <a href="view.htm?id=${user.id()}"><@label "005"/></a>
      <a href="users.htm"><@label "006"/></a>
    </@>

    <@htm.nodiv "page-ivars">
      { userId: "${user.id()}" }
    </@>

<#else>

    <@label "x.nousr"/>

</#if>

