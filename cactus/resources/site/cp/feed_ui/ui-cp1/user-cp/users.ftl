<#import "/web/common.ftl" as c >
<#import "/web/common-htm.ftl" as htm >
<#include "/web/macros.ftl:"+currentSite>

<div id="user-list" class="item-list" data-state="${perPage},${searchState!}">
  <div class="head">
    <a href="#" class="column-1 username"><@label "002"/><span class="arrow"></span></a>
    <a href="#" class="column-2 email"><@label "003"/><span class="arrow"></span></a>
    <p class="column-3 name"><@label "004"/></p>
  </div>
  <div class="rows"></div>
  <div class="foot">
    <div class="page-spread"></div>
  </div>
</div>
