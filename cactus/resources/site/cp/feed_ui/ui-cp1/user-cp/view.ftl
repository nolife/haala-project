<#import "/web/common.ftl" as c >
<#import "/web/common-htm.ftl" as htm >
<#include "/web/macros.ftl:"+currentSite>

<#if user??>

    <div id="user-summary" class="form-panel">
      <div class="two-column">
        <div class="cell left">
          <label><@label "006"/></label>
          <p>${user.username()}</p>
        </div>
        <div class="cell right">
          <label><@label "007"/></label>
          <p><@h.timestamp ts=user.activated() format="dd/MM/yyyy HH:mm" /></p>
        </div>
        <div class="cell"></div>
        <div class="cell">
          <label><@label "008"/></label>
          <p><@h.userFullName user=user/></p>
        </div>
        <div class="cell left">
          <label><@label "009"/></label>
          <p>${user.email()}</p>
        </div>
        <div class="cell right">
          <label><@label "010"/></label>
          <p>${user.primaryNumber()}</p>
        </div>
      </div>
      <h3 class="cell"><@label "003"/></h3>
      <div class="cell">
        <label><@label "011"/></label>
        <p><@h.userCredit user=user/></p>
      </div>
      <@packContent "user-view-table"/>
      <div class="nofloat"></div>
    </div>

    <@htm.nodiv "quick-navigation-content">
      <a href="users.htm"><@label "005"/></a>
    </@>

    <@htm.nodiv "context-navigation-content">
      <a class="shell" href="#" title="<@label "016"/>" ><@label "014"/></a>
      <a class="credit" href="#" title="<@label "017"/>" ><@label "015"/></a>
      <a class="props" href="props.htm?id=${user.id()}" title="<@label "026"/>" ><@label "025"/></a>
      <a class="payment" href="#" title="<@label "028"/>" ><@label "027"/></a>
    </@>

    <@htm.nodiv "page-ivars">
      {
        userId: ${user.id()}, username: "${user.username()}",
        currency: "<@h.defaultCurrency />",
        ref: "<@h.generateRef pattern="|D|hms|" />"
      }
    </@>

    <@htm.nodiv "page-messages">
      <div data-key="credit-title"><@label "021"/></div>
      <div data-key="payment-title"><@label "034"/></div>
      <div data-key="button-ok"><@label "btn.ok"/></div>
      <div data-key="button-cancel"><@label "btn.cancel"/></div>
    </@>

    <@htm.nodiv "page-templates">

      <div id="credit-frame">
        <div class="form-panel">
          <div class="two-column">
            <div class="cell left amount">
              <label><@label "023"/></label>
              <input type="text" maxlength="100"/>
              <p class="error"></p>
            </div>
            <div class="cell right message">
              <label><@label "024"/></label>
              <input type="text" maxlength="100"/>
              <p class="error"></p>
            </div>
          </div>
        </div>
      </div>

      <div id="payment-frame">
        <div class="form-panel">
          <div class="two-column">
            <div class="cell left ref">
              <label><@label "029"/></label>
              <input type="text" maxlength="100"/>
              <p class="error"></p>
            </div>
            <div class="cell right username">
              <label><@label "030"/></label>
              <input type="text" maxlength="100"/>
              <p class="error"></p>
            </div>
            <div class="cell left currency">
              <label><@label "031"/></label>
              <select>
                <option value=""></option>
                <@h.listCurrencies ; cur >
                  <option value="${cur}" ><@label "cur.${cur}"/></option>
                </@>
              </select>
              <p class="error"></p>
            </div>
            <div class="cell right amount">
              <label><@label "032"/></label>
              <input type="text" maxlength="100"/>
              <p class="error"></p>
            </div>
          </div>
          <div class="cell text">
            <label><@label "033"/></label>
            <textarea></textarea>
            <p class="error"></p>
          </div>
        </div>
      </div>

    </@>

<#else>

    <@label "x.nousr"/>

</#if>

