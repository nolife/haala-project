<#import "/web/common.ftl" as c >
<#import "/web/common-htm.ftl" as htm >
<#include "/web/macros.ftl:"+currentSite >

<div class="navigation-pane">
  <a class="edit-details" href="details.htm" title="<@label "016"/>" ><@label "002"/></a>
  <a class="change-password" href="password.htm" title="<@label "017"/>" ><@label "003"/></a>
  <@h.countUserUnreadMessages var="unread" />
  <a class="view-messages" href="/my/message/messages.htm" title="<@label "018" "unread:${unread}"/>" ><@label "004" "unread:${unread}"/></a>
</div>
<div class="navigation-pane">
  <@packContent "account-items"/>
  <@h.onRole role="cms">
    <a class="customize-website" href="/my/cms/web.htm" title="<@label "011"/>" ><@label "007"/></a>
  </@>
</div>
<@h.onRole role="admin">
  <div class="navigation-pane">
    <@h.countUserUnreadMessages var="unread" userRef="system"/>
    <a class="sys-messages" href="/my/message/messages.htm?trg=2" title="<@label "022" "unread:${unread}"/>" ><@label "015" "unread:${unread}"/></a>
    <a class="user-cp" href="/su/user/users.htm" title="<@label "023"/>" ><@label "010"/></a>
    <a class="setting-cp" href="/su/setting/settings.htm" title="<@label "024"/>" ><@label "012"/></a>
    <a class="label-cp" href="/su/label/labels.htm" title="<@label "025"/>" ><@label "013"/></a>
    <a class="file-cp" href="/su/file/files.htm" title="<@label "020"/>" ><@label "014"/></a>
    <a class="spam-cp" href="/su/spam/main.htm" title="<@label "031"/>" ><@label "030"/></a>
    <a class="payment-cp" href="/su/payment/entries.htm" title="<@label "027"/>" ><@label "026"/></a>
    <a class="node-cp" href="/su/node/board.htm" title="<@label "006"/>" ><@label "001"/></a>
  </div>
</@>
<div class="nofloat"></div>
