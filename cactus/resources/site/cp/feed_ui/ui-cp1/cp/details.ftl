<#import "/web/common.ftl" as c >
<#import "/web/common-htm.ftl" as htm >
<#include "/web/macros.ftl:"+currentSite >

<@htm.form id="page-form">
  <@h.user var="user" />
  <div class="form-panel">
    <h3 class="cell"><@label "002"/></h3>
    <div class="two-column">
      <div class="cell urername">
        <label><@label "019"/></label>
        <p>${user.username()}</p>
      </div>
      <div class="cell salutation">
        <label><@label "004"/></label>
        <select tabindex="1">
          <option value=""></option>
          <#list 1..4 as n>
            <option value="${n}" <#if n == user.salutation()> selected="true" </#if> >
              <@h.label key="salut${n}" pindex="0" />
            </option>
          </#list>
        </select>
        <p class="error"></p>
      </div>
      <div class="cell left first-name">
        <label><@label "005"/></label>
        <input type="text" maxlength="100" tabindex="1" value="${user.firstName()}"/>
        <p class="error"></p>
      </div>
      <div class="cell right last-name">
        <label><@label "006"/></label>
        <input type="text" maxlength="100" tabindex="1" value="${user.lastName()}"/>
        <p class="error"></p>
      </div>
      <div class="cell left email">
        <label><@label "007"/></label>
        <input type="text" maxlength="100" tabindex="1" value="${user.email()}"/>
        <p class="error"></p>
      </div>
      <div class="cell right verify-email">
        <label><@label "008"/></label>
        <input type="text" maxlength="100" tabindex="1" value="${user.email()}"/>
        <p class="error"></p>
      </div>
      <div class="cell primary-number">
        <label><@label "009"/></label>
        <input type="text" maxlength="100" tabindex="1" value="${user.primaryNumber()}"/>
        <p class="error"></p>
      </div>
    </div>
    <h3 class="cell"><@label "003"/></h3>
    <div class="two-column">
      <div class="cell left address-line1">
        <label><@label "013"/></label>
        <input type="text" maxlength="100" tabindex="1" value="${(user.address().line1())!}"/>
        <p class="error"></p>
      </div>
      <div class="cell right address-town">
        <label><@label "015"/></label>
        <input type="text" maxlength="100" tabindex="2" value="${(user.address().town())!}"/>
        <p class="error"></p>
      </div>
      <div class="cell left address-line2">
        <label><@label "014"/></label>
        <input type="text" maxlength="100" tabindex="1" value="${(user.address().line2())!}"/>
        <p class="error"></p>
      </div>
      <div class="cell right address-country">
        <label><@label "016"/></label>
        <@h.realCountries var="countries" />
        <select tabindex="2">
          <option value=""></option>
          <#list countries as o>
            <option value="${o.code()}" <#if o.code() == (user.address().country())!> selected="true" </#if> >
              <@h.countryName obj=o />
            </option>
          </#list>
        </select>
        <p class="error"></p>
      </div>
      <div class="cell left"></div>
      <div class="cell right address-postcode">
        <label><@label "017"/></label>
        <input type="text" maxlength="100" tabindex="2" value="${(user.address().postcode())!}"/>
        <p class="error"></p>
      </div>
    </div>
    <h3 class="cell">&nbsp;</h3>
    <div class="two-column">
      <div class="cell password">
        <label><@label "018"/></label>
        <input type="password" maxlength="100" tabindex="2"/>
        <p class="error"></p>
      </div>
    </div>
    <div class="cell submit">
      <button><@label "btn.save"/></button>
    </div>
  </div>
</@>
