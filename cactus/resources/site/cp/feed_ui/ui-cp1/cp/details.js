;(function($) {

  Haala.Misc.onDocReady(function() {
    initDetailsForm()
  })


  function initDetailsForm() {
    $("#page-form .submit button").button()

    function editDetails(/*{}*/ command) {
      Haala.AJAX.call("body", UserF.editDetails, command,
        function() {
          document.location = "account.htm"
          return Haala.AJAX.DO_NOTHING
        },
        function(data) {
          $("#page-form .password input").val("")
        })
    }

    function verifyEmail() {
      var email1 = $("#page-form .email input").val(),
          email2 = $("#page-form .verify-email input").val()
      return email1 && email1 != email2 ? "nomat" : null
    }

    function onSubmitFn(model, /*{}*/ values) {
      var command = Haala.Misc.collectInput(values,
        ["salutation", "email", "firstName", "lastName", "primaryNumber", "password"])
      command.address = Haala.Misc.collectInput(values,
        ["address-line1", "address-line2", "address-town", "address-country", "address-postcode"],
        ["line1",         "line2",         "town",         "country",         "postcode"])
      editDetails(command)
    }

    var panelOptions = {
      fields: [{ name: "salutation",       selector: ".salutation select",      validate: { require: true } },
               { name: "email",            selector: ".email input",            validate: { require: true, regex: ["email"] } },
               { name: "verify-email",     selector: ".verify-email input",     validate: { require: true, userFn: verifyEmail } },
               { name: "firstName",        selector: ".first-name input",       validate: { require: true } },
               { name: "lastName",         selector: ".last-name input",        validate: { require: true } },
               { name: "primaryNumber",    selector: ".primary-number input",   validate: { require: true } },
               { name: "address-line1",    selector: ".address-line1 input",    validate: { require: true } },
               { name: "address-line2",    selector: ".address-line2 input"                                 },
               { name: "address-town",     selector: ".address-town input",     validate: { require: true } },
               { name: "address-country",  selector: ".address-country select", validate: { require: true } },
               { name: "address-postcode", selector: ".address-postcode input", validate: { require: true } },
               { name: "password",         selector: ".password input",         validate: { require: true, regex: ["password"], length: [5] } }],
      onSubmit: onSubmitFn
    }

    new Haala.FormBuilder()
      .plugin( $('#page-form').formpanel(panelOptions) )
      .validate( new Haala.UIValidator(Haala.UIValidator.Integ) )
      .build()
  }

})(jQuery);
