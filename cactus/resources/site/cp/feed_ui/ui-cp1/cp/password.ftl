<#import "/web/common.ftl" as c >
<#import "/web/common-htm.ftl" as htm >
<#include "/web/macros.ftl:"+currentSite >

<@htm.form id="page-form">
  <div class="form-panel">
    <div class="two-column">
      <div class="cell old-password">
        <label><@label "003"/></label>
        <input type="password" maxlength="100"/>
        <p class="error"></p>
      </div>
      <div class="cell left new-password">
        <label><@label "004"/></label>
        <input type="password" maxlength="100"/>
        <p class="error"></p>
      </div>
      <div class="cell right verify-password">
        <label><@label "005"/></label>
        <input type="password" maxlength="100"/>
        <p class="error"></p>
      </div>
    </div>
    <div class="cell submit">
      <button><@label "btn.save"/></button>
    </div>
  </div>
</@>
