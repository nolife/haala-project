;(function($) {

  Haala.Misc.onDocReady(function() {
    initPasswordForm()
  })


  function initPasswordForm() {
    $("#page-form .submit button").button()

    function changePassword(/*{}*/ command) {
      Haala.AJAX.call("body", UserF.changePassword, command,
        function() {
          document.location = "account.htm"
          return Haala.AJAX.DO_NOTHING
        },
        function() {
          $("#page-form .old-password input").val("")
        })
    }

    var verifyPassword = function() {
      var pass1 = $("#page-form .new-password input").val(),
          pass2 = $("#page-form .verify-password input").val()
      return pass1 && pass1 != pass2 ? "nomat" : null
    }

    function onSubmitFn(model, /*{}*/ values) {
      var command = Haala.Misc.collectInput(values, ["oldPassword", "password"])
      changePassword(command)
    }

    var panelOptions = {
      fields: [{ name: "oldPassword",     selector: ".old-password input",    validate: { require: true, regex: ["password"], length: [5] } },
               { name: "password",        selector: ".new-password input",    validate: { require: true, regex: ["password"], length: [5] } },
               { name: "verify-password", selector: ".verify-password input", validate: { require: true, userFn: verifyPassword } }],
      onSubmit: onSubmitFn
    }

    new Haala.FormBuilder()
      .plugin( $('#page-form').formpanel(panelOptions) )
      .validate( new Haala.UIValidator(Haala.UIValidator.Integ) )
      .build()
  }

})(jQuery);

