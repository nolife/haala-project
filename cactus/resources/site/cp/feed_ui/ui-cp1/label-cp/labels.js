
(function($) {

  Haala.Misc.onDocReady(function() {
    initNavigation();
    initLabelList();
  });


  var initNavigation = function() {
    $("#context-navigation").kdxMenu({
      items: [
        { element: "#context-navigation .add",
          gotoFn: function(/*json*/ item, /*json*/ model) {
            editFrame();
          }}
      ]
    });
  };


  var initLabelList = function() {
    var listModel = {
      sort: {
        items: [
          { element: $("#label-list .head .key"), field: "key" }
        ]
      },
      loadItemsFn: function(/*json*/ model, /*fn*/ callback) {
        Haala.AquaList.Integ.loadItemsFn(LabelF.search, { target: 1 }, model, callback);
      },
      rowElementFn: function(/*json*/ vo, /*int*/ position, /*json*/ model) {
        return vo ?
          $("<div class='data'>" +
            "  <a href='#'>" +
            "    <p class='column-1'>"+vo.key+"</p>" +
            "    <p class='column-2'>"+vo.value+"</p>" +
            "    <p class='column-3'>"+vo.site+"</p>" +
            "  </a>" +
            "</div>").aclick(function() {
                        Haala.AJAX.call("body", LabelF.load, { labelId: vo.id },
                          function(data) { editFrame(data.result); });
                      }) :
          $("<div class='empty'></div>");
      }
    };
    var list = $("#label-list .rows").aquaList(
      $.extend(true, {},
        Haala.AquaList.Integ.DefaultOptions,
        Haala.AquaList.Integ.parseState($("#label-list").attr("data-state")),
        listModel));
    list.setPageSpread($("#label-list .page-spread").pageSpread(Haala.PageSpread.Integ.DefaultOptions));
    list.addRenderedFn(Haala.Code.stretch);

    $("#label-list .filter .on").aclick(function() {
      var command = { target: 1, params: [$("#label-list .filter input").val()] };
      Haala.AJAX.call("body", LabelF.filter, command,
        function(data) { list.page(1); });
    });
    $("#label-list .filter .off").aclick(function() {
      var command = { target: 1, params: [] };
      Haala.AJAX.call("body", LabelF.filter, command,
        function(data) {
          $("#label-list .filter input").val("");
          list.page(1);
        });
    });

  };


  var editFrame = function(/*json*/ vo) {
    var isNew = !vo;
    var msgFn = function(/*string*/ msg, /*json*/ tokens) {
      return Haala.Misc.messageMapping("#page-messages", msg, tokens);
    };

    var finp = function(key, vld) { return Haala.Util.panelFormInput(key, vld, { root: "#edit-frame" })};
    var panel = $("#edit-frame").kdxPanel(
      $.extend(true, {}, Haala.KdxPanel.Integ.DefaultOptions, {
        input: [
          finp("key",   { require: true }),
          finp("site",  { require: true }),
          finp("value", { require: true })
        ]
      }));

    if (!isNew) {
      $("#edit-frame .key input").val(vo.key);
      $("#edit-frame .site input").val(vo.site);
      $("#edit-frame .value textarea").val(vo.value);
    }

    $("#edit-frame").dialog({
      zIndex: 70, width: 900, height: 270, resizable: false,
      buttons: [
        { text: msgFn("button-delete"), click: function() {
          var THIS = $(this);
          Haala.Messages.showYN(msgFn("confirm-delete"), "title-confirm", function() {
            removeLabel({ labelId: vo.id });
            THIS.dialog("close");
          });
        }},
        { text: msgFn("button-cancel"), click: function() {
          $(this).dialog("close");
        }},
        { text: msgFn("button-ok"), click: function() {
          if (panel.validate()) {
            var command = Haala.Misc.collectInput(panel.collect(), ["key", "value", "site"]);
            if (isNew) {
              addLabel(command);
            } else {
              command.labelId = vo.id;
              editLabel(command);
            }
            $(this).dialog("close");
          } else {
            Haala.Util.errorToViewport({ root: "#edit-frame" });
          }
        }}],
      close: function() {
        panel.validate("reset");
        $("#edit-frame input, #edit-frame textarea").val("");
      }
    });

    $("#edit-frame").dialog("option", "title", msgFn(isNew ? "add-title" : "edit-title"));

    var bpane = $("#edit-frame").next(".ui-dialog-buttonpane");
    bpane.find(".ui-dialog-buttonset").css({"width": "100%"});
    bpane.find("button").css({"float": "right"});
    bpane.find("button:first").css({"float": "left", "margin-left": "10px"});

    if (isNew) bpane.find("button:first").hide();
    else bpane.find("button:first").show();

    var editLabel = function(/*json*/ command) {
      Haala.AJAX.call("body", LabelF.edit, command,
        function() { $("#label-list .rows").aquaList().page(); });
    };

    var addLabel = function(/*json*/ command) {
      Haala.AJAX.call("body", LabelF.add, command,
          function() { $("#label-list .rows").aquaList().page(); });
    };

    var removeLabel = function(/*json*/ command) {
      Haala.AJAX.call("body", LabelF.remove, command,
          function() { $("#label-list .rows").aquaList().page(); });
    };

  };

})(jQuery);

