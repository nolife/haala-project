
<#-- Page context navigation -->
<div id="context-messages"></div>
<div class="nofloat"></div>


<#-- Include packs right column (priority area) -->
<@packContent "right-column-top"/>

<#-- Include packs right column (regular) -->
<@packContent "right-column"/>
