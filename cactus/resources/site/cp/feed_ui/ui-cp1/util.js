
var Haala = Haala || {};

(function($) {

  /** Control Panel site utils. */
  Haala.Util = {

    /** Create input field for kdxPanel-like object which renders to HTML form.
     *  Example: panelFormInput("first-name", { require: true }) -->
     *   { element: "#page-form .first-name input", key: "firstName",
     *     validate: { element: "#page-form .first-name .error", require: true }}
     */
    panelFormInput: function(/*str*/ key, /*json*/ vld, /*json*/ opts) {
      var root = (opts || {}).root || "#page-form";
      var elem = null;
      $.each(["select", "textarea", "input"], function(n, v) {
        if (!elem) elem = $(root+" ."+key).find(v);
        if (elem.length == 0) elem = null;
      });
      var inp = {};
      if (elem) {
        inp = { element: root+" ."+key+" "+elem[0].tagName.toLowerCase(), key: Haala.Misc.camelCase(key) };
        inp.validate = $.extend(true, { element: root+" ."+key+" .error" }, vld);
      }
      return inp;
    },

    /** Scrolls to the first error in HTML form. */
    errorToViewport: function(/*json*/ opts) {
      var root = (opts || {}).root || "#page-form";
      $(root+" .error:visible:first").toViewport();
    },

    /** On HTML form submit validate panel object and the call user's submitFn. */
    formSubmitPanel: function(/*obj*/ panel, /*fn*/ submitFn, /*json*/ opts) {
      var root = (opts || {}).root || "#page-form";
      $(root).submit(function() {
        if (panel.validate()) submitFn(panel.collect());
        else Haala.Util.errorToViewport(opts);
        Haala.Code.stretch();
      });
    }

};

})(jQuery);
