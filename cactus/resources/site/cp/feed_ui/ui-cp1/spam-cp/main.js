
(function($) {

  Haala.Misc.onDocReady(function() {
    initNavigation();
    initStorageList();
  });



  var initNavigation = function() {
    var ivars = Haala.Misc.evalJSON("#page-ivars");
    var msgFn = function(/*string*/ msg, /*json*/ tokens) {
      return Haala.Misc.messageMapping("#page-messages", msg, tokens);
    };

    $("#context-navigation").kdxMenu({
      items: [
        { element: "#context-navigation .stop-task",
            gotoFn: function(/*json*/ item, /*json*/ model) {
              Haala.Messages.showYN(msgFn("confirm-stop"), "title-confirm", function() {
                Haala.AJAX.call("body", SpamF.stopTask, "",
                  function() { document.location = document.location.href; });
              });
            }}
      ]
    });
  };



  var initStorageList = function() {
    var ivars = Haala.Misc.evalJSON("#page-ivars");

    var listModel = {
      loadItemsFn: function(/*json*/ model, /*fn*/ callback) {
        Haala.AquaList.Integ.loadItemsFn(SpamF.searchStorages, { target: 1 }, model, callback);
      },
      rowElementFn: function(/*json*/ vo, /*int*/ position, /*json*/ model) {
        return vo ?
          $("<div class='data'>" +
            "  <a href='edit-storage.htm?id="+vo.id+"'>" +
            "    <p class='column-1'>"+vo.description+"</p>" +
            "    <p class='column-2'>"+vo.total+"</p>" +
            "  </a>" +
            "</div>").aclick(function() {
                        document.location = "edit-storage.htm?id="+vo.id;
                      }) :
          position <= 10 ? $("<div class='empty'></div>") : null;
      }
    };
    var list = $("#storage-list .rows").aquaList(
      $.extend(true,
        Haala.AquaList.Integ.DefaultOptions,
        { rows: 999 },
        listModel));
    list.addRenderedFn(Haala.Code.stretch);

  };

})(jQuery);

