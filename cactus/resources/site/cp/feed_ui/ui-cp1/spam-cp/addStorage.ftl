<#import "/web/common.ftl" as c >
<#import "/web/common-htm.ftl" as htm >
<#include "/web/macros.ftl:"+currentSite>

<@htm.form id="page-form">
  <div class="form-panel">
    <div class="cell description">
      <label><@label "002"/></label>
      <input type="text" maxlength="100"/>
      <p class="error"></p>
    </div>
    <div class="cell submit">
      <button><@label "btn.save"/></button>
    </div>
  </div>
</@>

<@htm.nodiv "quick-navigation-content">
  <a href="main.htm"><@label "005"/></a>
</@>