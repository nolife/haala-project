<#import "/web/common.ftl" as c >
<#import "/web/common-htm.ftl" as htm >
<#include "/web/macros.ftl:"+currentSite>

<#if storage??>

  <@htm.form id="page-form" mode="upload">
    <div class="form-panel">
      <div class="cell description">
        <label><@label "002"/></label>
        <input type="text" maxlength="100" value="${storage.description()}"/>
        <p class="error"></p>
      </div>
      <div class="cell totak">
        <label><@label "003"/></label>
        <p>${storage.total()}</p>
      </div>
      <div class="cell append">
        <label><@label "004"/></label>
        <textarea></textarea>
        <p class="error"></p>
      </div>
      <div class="cell submit">
        <button><@label "btn.save"/></button>
      </div>
    </div>
  </@>

  <@htm.nodiv "context-navigation-content">
    <a class="delete" href="#" title="<@label "014"/>" ><@label "013"/></a>
  </@>

  <@htm.nodiv "quick-navigation-content">
    <a href="main.htm"><@label "005"/></a>
  </@>

  <@htm.nodiv "page-ivars">
    { storageId: ${storage.id()} }
  </@>

  <@htm.nodiv "page-messages">
    <div data-key="confirm-delete"><@label "015"/></div>
  </@>

<#else>

  <@label "x.nosst"/>

</#if>
