
(function($) {

  Haala.Misc.onDocReady(function() {
    initEditForm();
    initNavigation();
  });



  var initNavigation = function() {
    var ivars = Haala.Misc.evalJSON("#page-ivars");
    var msgFn = function(/*string*/ msg, /*json*/ tokens) {
      return Haala.Misc.messageMapping("#page-messages", msg, tokens);
    };

    $("#context-navigation").kdxMenu({
      items: [
        { element: "#context-navigation .delete",
            gotoFn: function(/*json*/ item, /*json*/ model) {
              Haala.Messages.showYN(msgFn("confirm-delete"), "title-confirm", function() {
                Haala.AJAX.call("body", SpamF.removeStorage, { storageId: ivars.storageId },
                  function() { document.location = "main.htm"; });
              });
            }}
      ]
    });
  };



  var initEditForm = function() {
    var ivars = Haala.Misc.evalJSON("#page-ivars");
    if (!ivars) return;

    $("#page-form .submit button").button();

    var finp = Haala.Util.panelFormInput;
    var input = [
      finp("description", { require: true }),
      finp("append")
    ];

    var panel = $("#page-form").kdxPanel(
      $.extend(true, {}, Haala.KdxPanel.Integ.DefaultOptions, {
        input: input
      }));

    Haala.Util.formSubmitPanel(panel, function(/*json*/ container) {
      var command = Haala.Misc.collectInput(container,
        ["description", "append"]);
      command.storageId = ivars.storageId;
      editStorage(command);
    });

    var editStorage = function(/*json*/ command) {
      Haala.AJAX.call("body", SpamF.updateStorage, command,
        function() { document.location = document.location.href; return Haala.AJAX.DO_NOTHING; });
    };
  };

})(jQuery);
