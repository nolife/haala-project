
(function($) {

  Haala.Misc.onDocReady(function() {
    initStartForm();
  });



  var initStartForm = function() {
    $("#page-form .submit button").button();

    var finp = Haala.Util.panelFormInput;
    var panel = $("#page-form").kdxPanel(
      $.extend(true, {}, Haala.KdxPanel.Integ.DefaultOptions, {
        input: [
          finp("storage", { require: true }),
          finp("from",    { require: true }),
          finp("subject", { require: true }),
          finp("text",    { require: true })
        ]
      }));

    Haala.Util.formSubmitPanel(panel, function(/*json*/ container) {
      var command = Haala.Misc.collectInput(container,
        ["from", "subject", "text"]);
      command.storageId = container.storage;
      startTask(command);
    });

    var startTask = function(/*json*/ command) {
      Haala.AJAX.call("body", SpamF.startTask, command,
        function(data) { document.location = "main.htm"; return Haala.AJAX.DO_NOTHING; });
    };
  };

})(jQuery);
