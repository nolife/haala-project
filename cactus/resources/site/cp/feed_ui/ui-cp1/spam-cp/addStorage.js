
(function($) {

  Haala.Misc.onDocReady(function() {
    initAddForm();
  });



  var initAddForm = function() {
    $("#page-form .submit button").button();

    var finp = Haala.Util.panelFormInput;
    var panel = $("#page-form").kdxPanel(
      $.extend(true, {}, Haala.KdxPanel.Integ.DefaultOptions, {
        input: [
          finp("description", { require: true })
        ]
      }));

    Haala.Util.formSubmitPanel(panel, function(/*json*/ container) {
      var command = { description: container.description };
      addStorage(command);
    });

    var addStorage = function(/*json*/ command) {
      Haala.AJAX.call("body", SpamF.createStorage, command,
        function(data) { document.location = "edit-storage.htm?id="+data.result; return Haala.AJAX.DO_NOTHING; });
    };
  };

})(jQuery);
