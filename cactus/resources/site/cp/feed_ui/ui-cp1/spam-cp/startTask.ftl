<#import "/web/common.ftl" as c >
<#import "/web/common-htm.ftl" as htm >
<#include "/web/macros.ftl:"+currentSite>

<@htm.form id="page-form">
  <div class="form-panel">
    <div class="cell storage">
      <label><@label "002"/></label>
      <select>
        <option value=""></option>
        <@h.listSpamStorages ; storage >
          <option value="${storage.id()}">${storage.description()}</option>
        </@>
      </select>
      <p class="error"></p>
    </div>
    <div class="cell from">
      <label><@label "003"/></label>
      <input type="text" maxlength="100"/>
      <p class="error"></p>
    </div>
    <div class="cell subject">
      <label><@label "004"/></label>
      <input type="text" maxlength="100" value="<@h.label key="spam.emailSubject" site="sys" bydefault="" />" />
      <p class="error"></p>
    </div>
    <div class="cell text">
      <label><@label "005"/></label>
      <textarea><@h.label key="spam.emailText" site="sys" bydefault="" /></textarea>
      <p class="error"></p>
    </div>
    <div class="cell submit">
      <button><@label "btn.submit"/></button>
    </div>
  </div>
</@>

<@htm.nodiv "quick-navigation-content">
  <a href="main.htm"><@label "006"/></a>
</@>
