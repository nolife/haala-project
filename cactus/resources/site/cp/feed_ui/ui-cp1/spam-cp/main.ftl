<#import "/web/common.ftl" as c >
<#import "/web/common-htm.ftl" as htm >
<#include "/web/macros.ftl:"+currentSite>

<div class="form-panel">
  <h3 class="cell"><@label "011"/></h3>
  <div class="two-column">
    <div class="cell left">
      <label><@label "012"/></label>
      <@h.spamTaskStatus var="tmpval"/>
      <p><@label "005_${tmpval}"/></p>
    </div>
    <div class="cell right">
      <label><@label "013"/></label>
      <p><@h.spamTaskError/></p>
    </div>
    <div class="cell left">
      <label><@label "014"/></label>
      <p><@h.printSpamTaskProgress/></p>
    </div>
    <div class="cell right">
      <label><@label "015"/></label>
      <p><@h.printSpamTaskRatio/></p>
    </div>
  </div>
  <div class="nofloat"></div>
</div>

<div id="storage-list" class="item-list">
  <h3 class="head"><@label "004"/></h3>
  <div class="head">
    <p class="column-1 description"><@label "002"/></p>
    <p class="column-2 total"><@label "003"/></p>
  </div>
  <div class="rows"></div>
</div>

<@htm.nodiv "context-navigation-content">
  <a class="start-task" href="start.htm" title="<@label "017"/>" ><@label "016"/></a>
  <a class="stop-task" href="#" title="<@label "019"/>" ><@label "018"/></a>
  <a class="add-storage" href="add-storage.htm" title="<@label "010"/>" ><@label "008"/></a>
</@>

<@htm.nodiv "page-messages">
  <div data-key="confirm-stop"><@label "006"/></div>
</@>
