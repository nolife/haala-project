<#import "/web/common.ftl" as c >
<#import "/web/common-htm.ftl" as htm >
<#include "/web/macros.ftl:"+currentSite>

<@htm.form id="page-form">
  <div class="form-panel">
    <h3 class="cell"><@label "001"/></h3>
    <div class="cell value">
      <label><@label "002"/></label>
      <input type="text" maxlength="100"/>
      <p class="error"></p>
    </div>
    <div class="cell title">
      <@label "003"/>
    </div>
    <div class="cell turing">
      <@captcha />
      <input type="text" maxlength="10"/>
      <p class="error"></p>
    </div>
    <div class="cell submit">
      <a href="login.htm"><@label "004"/></a>
      <button><@label "btn.remind"/></button>
    </div>
  </div>
</@>
