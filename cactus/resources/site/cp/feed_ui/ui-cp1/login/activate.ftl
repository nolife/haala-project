<#import "/web/common.ftl" as c >
<#import "/web/common-htm.ftl" as htm >
<#include "/web/macros.ftl:"+currentSite>

<#if user??>
  <h1><@label "001"/></h1>
  <@label "002"/>
<#else>
  <h1><@label "003"/></h1>
  <@label "004"/>
</#if>
