;(function($) {

  Haala.Misc.onDocReady(function() {
    initReminderForm()
  })


  function initReminderForm() {
    $("#page-form .submit button").button()

    function remind(/*{}*/ command) {
      Haala.AJAX.call("body", UserF.remindPassword, command,
        function(data) {
          document.location = "remok.htm?ref="+data.result
          return Haala.AJAX.DO_NOTHING
        })
    }

    function onSubmitFn(model, /*{}*/ values) {
      var command = Haala.Misc.collectInput(values, ["turing", "value"])
      remind(command)
    }

    var panelOptions = {
      fields: [{ name: "value",  selector: ".value input",  validate: { require: true, length: [5] } },
               { name: "turing", selector: ".turing input", validate: { userFn: Haala.InputValidator.Integ.turingUserFn } }],
      onSubmit: onSubmitFn
    }

    new Haala.FormBuilder()
      .plugin( $('#page-form').formpanel(panelOptions) )
      .validate( new Haala.UIValidator(Haala.UIValidator.Integ) )
      .build()
  }

})(jQuery);


