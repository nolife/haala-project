<#import "/web/common.ftl" as c >
<#import "/web/common-htm.ftl" as htm >
<#include "/web/macros.ftl:"+currentSite>

<@h.settingValue var="BrandName" key="Brand" mapKey="name" />

<#if requestParams.ref??>

  <@htm.form id="page-form">
    <div class="form-panel">
      <div class="cell">
        <p><@label "023" "BrandName:${BrandName}"/></p>
      </div>
      <h3 class="cell"><@label "024" "BrandName:${BrandName}"/></h3>
      <div class="cell privacy">
        <input type="checkbox"/>
        <p><@label "026" "BrandName:${BrandName}"/></p>
        <p class="error"></p>
      </div>
      <h3 class="cell">&nbsp;</h3>
      <div class="cell"><@label "025"/></div>
      <div class="cell turing">
        <@captcha />
        <input type="text" maxlength="10"/>
        <p class="error"></p>
      </div>
      <div class="cell submit">
        <button><@label "btn.submit"/></button>
      </div>
    </div>
  </@>

  <@htm.nodiv "page-ivars">
    { ref: "${requestParams.ref}" }
  </@>

  <@htm.nodiv "page-messages">
    <div data-key="dialog-title"><@label "002"/></div>
    <div data-key="dialog-text"><@label "003"/></div>
  </@>

<#else>

  <@label "004"/>

</#if>