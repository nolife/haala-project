;var h_fbLogin

;(function($) {

  Haala.Misc.onDocReady(function() {
    initLoginForm()
    facebookLogin()
  })


  function initLoginForm() {
    $("#page-form .submit button").button()

    function login(/*{}*/ command) {
      Haala.AJAX.call("body", LoginF.login, command,
        function(data) {
          document.location = data.result
          return Haala.AJAX.DO_NOTHING
        },
        function(data) {
          $("#page-form .password input").val("")
          if (data.errorCodes.indexOf(103) != -1) {
            document.location = "/agreement.htm?ref="+data.result
            return Haala.AJAX.DO_NOTHING
          }
        })
    }

    function onSubmitFn(model, /*{}*/ values) {
      var command = Haala.Misc.collectInput(values, ["turing", "username", "password"])
      login(command)
    }

    var panelOptions = {
      fields: [{ name: "username", selector: ".username input", validate: { require: true, regex: ["username"], length: [5] } },
               { name: "password", selector: ".password input", validate: { require: true, regex: ["password"], length: [5] } },
               { name: "turing",   selector: ".turing input",   validate: { userFn: Haala.InputValidator.Integ.turingUserFn } }],
      onSubmit: onSubmitFn
    }

    new Haala.FormBuilder()
      .plugin( $('#page-form').formpanel(panelOptions) )
      .validate( new Haala.UIValidator(Haala.UIValidator.Integ) )
      .build()
  }
  

  function facebookLogin() {
    var ivars = Haala.Misc.evalJSON("#page-ivars")
    if (!ivars) return

    var callback = function() {
      FB.init({ appId: ivars.fbApiKey, status: true, cookie: true, xfbml: true })

      h_fbLogin = function() {
        FB.login(function(response) {
          if (response.authResponse) login(response.authResponse.accessToken)
        }, { scope: "email" })
      }

      var login = function(/*str*/ token) {
        Haala.AJAX.call("body", LoginF.fbLogin, { token: token },
          function(data) {
            document.location = data.result
            return Haala.AJAX.DO_NOTHING
          },
          function(data) {
            $("#page-form .password input").val("")
            if (data.errorCodes.indexOf(103) != -1) {
              document.location = "/agreement.htm?ref="+data.result
              return Haala.AJAX.DO_NOTHING
            }
          })
      }
    }

    Haala.Misc.loadScript("//connect.facebook.net/en_US/all.js", callback)
  }

})(jQuery);
