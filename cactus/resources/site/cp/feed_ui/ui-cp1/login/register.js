
;(function($) {

  Haala.Misc.onDocReady(function() {
    initRegistrationForm()
  })


  var initRegistrationForm = function() {
    $("#page-form .submit button").button()

    function register(/*{}*/ command) {
      Haala.AJAX.call("body", UserF.register, command,
        function() {
          document.location = "regok.htm"
          return Haala.AJAX.DO_NOTHING
        })
    }

    function verifyEmail() {
      var email1 = $("#page-form .email input").val(),
          email2 = $("#page-form .verify-email input").val()
      return email1 && email1 != email2 ? "nomat" : null
    }

    function verifyPassword() {
      var pass1 = $("#page-form .password input").val(),
          pass2 = $("#page-form .verify-password input").val()
      return pass1 && pass1 != pass2 ? "nomat" : null
    }

    function onSubmitFn(model, /*{}*/ values) {
      var command = Haala.Misc.collectInput(values,
        ["salutation", "email", "firstName", "lastName", "primaryNumber", "turing", "username", "password"])
      command.address = Haala.Misc.collectInput(values,
        ["address-line1", "address-line2", "address-town", "address-country", "address-postcode"],
        ["line1",         "line2",         "town",         "country",         "postcode"])
      register(command)
    }

    var panelOptions = {
      fields: [{ name: "salutation",       selector: ".salutation select",      validate: { require: true } },
               { name: "email",            selector: ".email input",            validate: { require: true, regex: ["email"] } },
               { name: "verify-email",     selector: ".verify-email input",     validate: { require: true, userFn: verifyEmail } },
               { name: "firstName",        selector: ".first-name input",       validate: { require: true } },
               { name: "lastName",         selector: ".last-name input",        validate: { require: true } },
               { name: "primaryNumber",    selector: ".primary-number input",   validate: { require: true } },
               { name: "address-line1",    selector: ".address-line1 input",    validate: { require: true } },
               { name: "address-line2",    selector: ".address-line2 input"                                 },
               { name: "address-town",     selector: ".address-town input",     validate: { require: true } },
               { name: "address-country",  selector: ".address-country select", validate: { require: true } },
               { name: "address-postcode", selector: ".address-postcode input", validate: { require: true } },
               { name: "username",         selector: ".username input",         validate: { require: true, regex: ["username"], length: [5] } },
               { name: "password",         selector: ".password input",         validate: { require: true, regex: ["password"], length: [5] } },
               { name: "verify-password",  selector: ".verify-password input",  validate: { require: true, userFn: verifyPassword } },
               { name: "privacy",          selector: ".privacy input",          validate: { require: true } },
               { name: "turing",           selector: ".turing input",           validate: { userFn: Haala.InputValidator.Integ.turingUserFn } }],
      onSubmit: onSubmitFn
    }

    new Haala.FormBuilder()
      .plugin( $('#page-form').formpanel(panelOptions) )
      .validate( new Haala.UIValidator(Haala.UIValidator.Integ) )
      .build()

    $("#page-form .username input").change(function() {
      if ($(this).val()) {
        var err = $("#page-form .username .error").html("").hide()
        Haala.AJAX.call(null, UserF.usernameTaken, $(this).val(),
          function(data) {
            if (data.result) err.html(Haala.Misc.messageMapping(null, "valid.taken")).show()
          }, null, Haala.AJAX.DO_NOTHING)
      }
    })

  }

})(jQuery);
