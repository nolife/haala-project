<#import "/web/common.ftl" as c >
<#import "/web/common-htm.ftl" as htm >
<#include "/web/macros.ftl:"+currentSite>

<@h.settingValue var="BrandName" key="Brand" mapKey="name" />

<@htm.form id="page-form">
  <div class="form-panel">
    <div class="cell">
      <p><@label "023" "BrandName:${BrandName}"/></p>
    </div>
    <h3 class="cell"><@label "002"/></h3>
    <div class="two-column">
      <div class="cell username">
        <label><@label "005"/></label>
        <input type="text" maxlength="100" tabindex="1"/>
        <p class="error"></p>
      </div>
      <div class="cell left password">
        <label><@label "006"/></label>
        <input type="password" maxlength="100" tabindex="1"/>
        <p class="error"></p>
      </div>
      <div class="cell right verify-password">
        <label><@label "007"/></label>
        <input type="password" maxlength="100" tabindex="1"/>
        <p class="error"></p>
      </div>
    </div>
    <h3 class="cell"><@label "003"/></h3>
    <div class="two-column">
      <div class="cell salutation">
        <label><@label "008"/></label>
        <select tabindex="1">
          <option value=""></option>
          <#list 1..4 as n>
            <option value="${n}">
              <@h.label key="salut${n}" pindex="0" />
            </option>
          </#list>
        </select>
        <p class="error"></p>
      </div>
      <div class="cell left first-name">
        <label><@label "009"/></label>
        <input type="text" maxlength="100" tabindex="1"/>
        <p class="error"></p>
      </div>
      <div class="cell right last-name">
        <label><@label "010"/></label>
        <input type="text" maxlength="100" tabindex="1"/>
        <p class="error"></p>
      </div>
      <div class="cell left email">
        <label><@label "011"/></label>
        <input type="text" maxlength="100" tabindex="1"/>
        <p class="error"></p>
      </div>
      <div class="cell right verify-email">
        <label><@label "012"/></label>
        <input type="text" maxlength="100" tabindex="1"/>
        <p class="error"></p>
      </div>
      <div class="cell primary-number">
        <label><@label "013"/></label>
        <input type="text" maxlength="100" tabindex="1"/>
        <p class="error"></p>
      </div>
      <div class="cell left work-number">
        <label><@label "014"/></label>
        <input type="text" maxlength="100" tabindex="1"/>
        <p class="error"></p>
      </div>
      <div class="cell right mobile-number">
        <label><@label "015"/></label>
        <input type="text" maxlength="100" tabindex="1"/>
        <p class="error"></p>
      </div>
    </div>
    <h3 class="cell"><@label "004"/></h3>
    <div class="two-column">
      <div class="cell left address-line1">
        <label><@label "017"/></label>
        <input type="text" maxlength="100" tabindex="1"/>
        <p class="error"></p>
      </div>
      <div class="cell right address-town">
        <label><@label "019"/></label>
        <input type="text" maxlength="100" tabindex="2"/>
        <p class="error"></p>
      </div>
      <div class="cell left address-line2">
        <label><@label "018"/></label>
        <input type="text" maxlength="100" tabindex="1"/>
        <p class="error"></p>
      </div>
      <div class="cell right address-country">
        <label><@label "020"/></label>
        <@h.realCountries var="countries" />
        <select tabindex="2">
          <option value=""></option>
          <#list countries as o>
            <option value="${o.code()}">
              <@h.countryName obj=o />
            </option>
          </#list>
        </select>
        <p class="error"></p>
      </div>
      <div class="cell left"></div>
      <div class="cell right address-postcode">
        <label><@label "021"/></label>
        <input type="text" maxlength="100" tabindex="2"/>
        <p class="error"></p>
      </div>
    </div>
    <h3 class="cell"><@label "024" "BrandName:${BrandName}"/></h3>
    <div class="cell privacy">
      <input type="checkbox"/>
      <p><@label "026" "BrandName:${BrandName}"/></p>
      <p class="error"></p>
    </div>
    <h3 class="cell">&nbsp;</h3>
    <div class="cell"><@label "025"/></div>
    <div class="cell turing">
      <@captcha />
      <input type="text" maxlength="10"/>
      <p class="error"></p>
    </div>
    <div class="cell submit">
      <button><@label "btn.register"/></button>
    </div>
  </div>
</@>

