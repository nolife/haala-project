<#import "/web/common.ftl" as c >
<#import "/web/common-htm.ftl" as htm >
<#include "/web/macros.ftl:"+currentSite>

<@h.user var="user" userRef=requestParams.ref! />
<#if (user.activated())??>
  <@label "003"/>
<#else>
  <@label "002"/>
</#if>

