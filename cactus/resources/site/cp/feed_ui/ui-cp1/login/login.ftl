<#import "/web/common.ftl" as c >
<#import "/web/common-htm.ftl" as htm >
<#include "/web/macros.ftl:"+currentSite>

<@htm.form id="page-form">
<div class="form-panel">
  <h3 class="cell"><@label "002"/></h3>
  <div class="cell username">
    <label><@label "008"/></label>
    <input type="text" maxlength="100"/>
    <p class="error"></p>
  </div>
  <div class="cell password">
    <label><@label "009"/></label>
    <input type="password" maxlength="100"/>
    <p class="error"></p>
  </div>
  <div class="cell title">
    <@label "004"/>
  </div>
  <div class="cell turing">
    <@captcha />
    <input type="text" maxlength="10"/>
    <p class="error"></p>
  </div>
  <div class="cell submit">
    <a class="reminder" href="reminder.htm"><@label "010"/></a>
    <button><@label "btn.login"/></button>
    <a class="fb-login" href="#" onclick="javascript:h_fbLogin(); return false;" title='<@label "003"/>'><div class="icon"></div></a>
    <div class="nofloat"></div>
  </div>
</div>
</@>

<a class="browser" target="_blank" href="http://www.mozilla-europe.org/en/firefox">
  <span><@label "005"/></span>
</a>

<div id="fb-root"></div>

<@htm.nodiv "page-ivars">
  { fbApiKey: '<@h.settingValue key="FacebookApiKey"/>' }
</@>
