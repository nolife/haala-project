
(function($) {

  Haala.Misc.onDocReady(function() {
    initAgreementForm();
  });


  var initAgreementForm = function() {
    var ivars = Haala.Misc.evalJSON("#page-ivars");
    if (!ivars) return;

    var msgFn = function(/*string*/ msg, /*json*/ tokens) {
      return Haala.Misc.messageMapping("#page-messages", msg, tokens);
    };

    $("#page-form .submit button").button();

    var finp = Haala.Util.panelFormInput;
    var panel = $("#page-form").kdxPanel(
      $.extend(true, {}, Haala.KdxPanel.Integ.DefaultOptions, {
        input: [
          finp("privacy", { require: true }),
          finp("turing",  { userFn: Haala.InputValidator.Integ.turingUserFn })
        ]
      }));

    Haala.Util.formSubmitPanel(panel, function(/*json*/ container) {
      var command = { ref: ivars.ref, turing: container.turing };
      acceptAgreement(command);
    });

    var acceptAgreement = function(/*json*/ command) {
      Haala.AJAX.call("body", UserF.acceptAgreement, command,
          function() {
            Haala.Messages.showOK(msgFn("dialog-text"), msgFn("dialog-title"), function() {
              document.location = "/login.htm";
            });
          });
    };

  };

})(jQuery);
