;(function($) {

  Haala.Misc.onDocReady(function() {
    initNavigation()
    initSettingList()
  })


  function initNavigation() {
    $("#context-navigation").kdxMenu({
      items: [
        { element: "#context-navigation .add",
          gotoFn: function(/*{}*/ item, /*{}*/ model) {
            editFrame()
        }}
      ]
    })
  }


  function initSettingList() {

    function rowFn(model, /*{}*/ vo) {
      return vo ?
        $(
          "<div><a href='#'><p class='column-1'>{key}</p><p class='column-2'>{value}</p><p class='column-3'>{site}</p></a></div>"
            .replace("{key}", vo.key).replace("{value}", vo.value).replace("{site}", vo.site)
        ).click(function(event) {
          event.preventDefault()
          Haala.AJAX.call("body", SettingF.load, { settingId: vo.id }, function(data) {
            editFrame(data.result)
          })
        }) :
        $("<div></div>")
    }

    function loadItemsFn(model) {
      model.userData.facadeSearch.command.target = 1
      model.userData.facadeSearch.call(SettingF.search, "#setting-list")
    }

    new Haala.TableBuilder({ facadeSearch: true })
      .plugin( $('#setting-list .head').sortcolumn({ columns: [{ selector: ".key", name: "key" }, { selector: ".site", name: "sit" }]}) )
      .plugin( $('#setting-list .foot .page-spread').pagination({ nextprev: "always" }) )
      .plugin( $('#setting-list .rows').listing({ delayLoad: true, makeRow: rowFn, loadItems: loadItemsFn, onRender: Haala.Code.stretch }) )
      .state( $("#setting-list").attr("data-state") )
      .build()

    $("#setting-list .filter .on").click(function(event) {
      event.preventDefault()
      var command = { target: 1, params: [$("#setting-list .filter input.key").val(), $("#setting-list .filter input.value").val()] }
      Haala.AJAX.call(null, SettingF.filter, command, function(data) {
        $('#setting-list .rows').listing("page", 1)
      })
    })

    $("#setting-list .filter .off").click(function(event) {
      event.preventDefault()
      var command = { target: 1, params: [] }
      Haala.AJAX.call(null, SettingF.filter, command, function(data) {
        $("#setting-list .filter input").val("")
        $('#setting-list .rows').listing("page", 1)
      })
    })

  }


  function editFrame(/*{}*/ vo) {

    function msgFn(/*str*/ msg, /*{}*/ tokens) {
      return Haala.Misc.messageMapping("#page-messages", msg, tokens)
    }

    function editSetting(/*{}*/ command) {
      Haala.AJAX.call("body", SettingF.edit, command,
        function() {
          $("#setting-list .rows").listing("page")
        })
    }

    function addSetting(/*{}*/ command) {
      Haala.AJAX.call("body", SettingF.add, command,
        function() {
          $("#setting-list .rows").listing("page")
        })
    }

    function removeSetting(/*{}*/ command) {
      Haala.AJAX.call("body", SettingF.remove, command,
        function() {
          $("#setting-list .rows").listing("page")
        })
    }

    function onSubmitFn(model, /*{}*/ values) {
        var command = Haala.Misc.collectInput(values, ["key", "value", "site"])
        if (model.editVO) {
          command.settingId = model.editVO.id
          editSetting(command)
        } else {
          addSetting(command)
        }
        $("#edit-frame").dialog("close")
    }

    if (!this.editFrameBuilt) { // build only once
      this.editFrameBuilt = true

      var panelOptions = {
        fields: [{ name: "key",   selector: ".key input",      validate: { require: true } },
                 { name: "site",  selector: ".site input",     validate: { require: true } },
                 { name: "value", selector: ".value textarea", validate: { require: true } }],
        onSubmit: onSubmitFn
      }

      new Haala.FormBuilder()
        .plugin( $('#edit-frame').formpanel(panelOptions) )
        .validate( new Haala.UIValidator(Haala.UIValidator.Integ) )
        .build()
    }

    $('#edit-frame').formpanel("model", { editVO: vo || null })

    if (vo) {
      $("#edit-frame .key input").val(vo.key)
      $("#edit-frame .site input").val(vo.site)
      $("#edit-frame .value textarea").val(vo.value)
    }

    $("#edit-frame").dialog({
      zIndex: 70, width: 900, height: 270, resizable: false,
      buttons: [
        { text: msgFn("button-delete"), click: function() {
          var THIS = $(this)
          Haala.Messages.showYN(msgFn("confirm-delete"), "title-confirm", function() {
            removeSetting({ settingId: vo.id })
            THIS.dialog("close")
          })
        }},
        { text: msgFn("button-cancel"), click: function() {
          $(this).dialog("close")
        }},
        { text: msgFn("button-ok"), click: function() {
          $('#edit-frame').formpanel("submit")
        }}],
      close: function() {
        $('#edit-frame').formpanel("reset")
        $("#edit-frame input, #edit-frame textarea").val("")
      }
    })

    $("#edit-frame").dialog("option", "title", msgFn(vo ? "edit-title" : "add-title"))

    var bpane = $("#edit-frame").next(".ui-dialog-buttonpane")
    bpane.find(".ui-dialog-buttonset").css({"width": "100%"})
    bpane.find("button").css({"float": "right"})
    bpane.find("button:first").css({"float": "left", "margin-left": "10px"})

    if (vo) bpane.find("button:first").show()
    else bpane.find("button:first").hide()

  }

})(jQuery);

