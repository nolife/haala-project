<#import "/web/common.ftl" as c >
<#import "/web/common-htm.ftl" as htm >
<#include "/web/macros.ftl:"+currentSite >

<div id="setting-list" class="item-list" data-state="${perPage},${searchState!}">
  <div class="filter">
    <@h.searchFilterParam var="tmpval" index=1 />
    <input class="key" type="text" value="${tmpval!}">
    <@h.searchFilterParam var="tmpval" index=2 />
    <input class="value" type="text" value="${tmpval!}">
    <span class="on" title="<@label "005"/>" ></span>
    <span class="off" title="<@label "006"/>" ></span>
  </div>
  <div class="head">
    <a href="#" class="column-1 key"><@label "002"/><span class="arrow"></span></a>
    <p class="column-2 value"><@label "003"/></p>
    <a href="#" class="column-3 site"><@label "004"/><span class="arrow"></span></a>
  </div>
  <div class="rows"></div>
  <div class="foot">
    <div class="page-spread"></div>
  </div>
</div>

<@htm.nodiv "context-navigation-content">
  <a class="add" href="#" title="<@label "014"/>"><@label "013"/></a>
</@>

<@htm.nodiv "page-messages">
  <div data-key="edit-title"><@label "007"/></div>
  <div data-key="add-title"><@label "011"/></div>
  <div data-key="button-ok"><@label "btn.ok"/></div>
  <div data-key="button-cancel"><@label "btn.cancel"/></div>
  <div data-key="button-delete"><@label "btn.delete"/></div>
  <div data-key="confirm-delete"><@label "012"/></div>
</@>

<@htm.nodiv "page-templates">

  <div id="edit-frame">
    <div class="form-panel">
      <div class="two-column">
        <div class="cell left key">
          <label><@label "008"/></label>
          <input type="text" maxlength="100"/>
          <p class="error"></p>
        </div>
        <div class="cell right site">
          <label><@label "010"/></label>
          <input type="text" maxlength="100"/>
          <p class="error"></p>
        </div>
      </div>
      <div class="cell value">
        <label><@label "009"/></label>
        <textarea></textarea>
        <p class="error"></p>
      </div>
    </div>
  </div>

</@>
