<#import "/web/common.ftl" as c >
<#import "/web/common-htm.ftl" as htm >
<#include "/web/macros.ftl:"+currentSite>

<header>
  <div class="logo">
    <a href="/"><@htm.noimg "home"/></a>
  </div>
  <div id="change-site">
    <a href="#" class="en" data-site="xx1"></a>
    <a href="#" class="ru" data-site="xx2"></a>
  </div>
</header>

<nav id="mainmenu">
  <@pageHeader />
  <@ajaxFrame />
  <@h.onRole role="current">
    <a class="logout" href="#" onclick="return false;"><@label "x.4"/></a>
    <a class="contact" href="/contact.htm"><@label "x.2"/></a>
    <a class="account" href="/my/account.htm"><@label "x.1"/></a>
  </@>
  <@h.onRole role="-,current">
    <a class="contact" href="/contact.htm"><@label "x.2"/></a>
    <a class="register" href="/register.htm"><@label "x.5"/></a>
    <a class="login" href="/login.htm"><@label "x.3"/></a>
  </@>
</nav>

<aside id="left-column">
  <#include "/web/pages/layout-left.ftl:"+currentSite >
</aside>

<aside id="right-column">
  <#include "/web/pages/layout-right.ftl:"+currentSite >
</aside>
