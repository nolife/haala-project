
var Haala = Haala || {};

/* js::import

   { path = web/haala; js = util }

   { path = web/endorsed/sticky-1.0.4; js = jquery.sticky }

 */

(function($) {

  Haala.Code = {

    init: function() {

      if (Haala.MereCms && Haala.MereCms.Integ) Haala.MereCms.Integ.init();
      Haala.InputValidator.Integ.turingLength = 4;
      Haala.Misc.tooltips();
      $(window).resize(Haala.Code.stretch);

      $("button.history-back").button().click(function() {
        history.back();
      });

      $("#mainmenu").kdxMenu({
        items: [
          { element: "#mainmenu .logout",
            gotoFn: function(/*json*/ item, /*json*/ model) {
              if (Haala.Misc.systemVar("superUser") == "1")
                Haala.AJAX.call("body", LoginF.shellOff, "",
                    function(data) { document.location = "/su/user/view.htm?id="+data.result; return Haala.AJAX.DO_NOTHING; });
              else Haala.AJAX.call("body", LoginF.logout, "",
                  function() { document.location = "/"; return Haala.AJAX.DO_NOTHING; });
            }}
        ]
      });

      $("#change-site a[data-site]").aclick(function(event) {
        Haala.AJAX.call("body", MiscF.changeSite, $(event.target).attr("data-site"),
            function() { document.location = document.location.href; return Haala.AJAX.DO_NOTHING; });
      });

      $("#quick-navigation").quickNavigation({ contentElement: "#quick-navigation-content" });
      if ($("#quick-navigation").quickNavigation().hasItems()) {
        $("#quick-navigation").show();
        $("#quick-navigation-wrap").show();
      }
      if ($("#recently-visited").quickNavigation().hasItems()) {
        $("#recently-visited").show();
        $("#quick-navigation-wrap").show();
      }
      $("#context-navigation-content").children().appendTo($("#context-navigation"));
      $("#sys-messages").children().appendTo($("#context-messages"));

      $("#mainmenu").sticky({ topSpacing: 0 });
    },

    ready: function() {
      Haala.Code.stretch();
    },

    stretch: function() {
      var h = $("#content").height();
      $("#left-column").height(h+20);
      $("#right-column").height(h+20);
    }

  };

})(jQuery);
