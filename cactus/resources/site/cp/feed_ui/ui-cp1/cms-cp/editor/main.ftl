<#import "/web/common.ftl" as c >
<#import "/web/common-htm.ftl" as htm >
<#include "/web/macros.ftl:"+currentSite>

<#function x_pageTitle pidx>
  <#local sitettl ><@h.label key="site.ttl" site=dsite /></#local>
  <#local pagettl ><@h.label key="ttl.p${pidx}" site=dsite bydefault=sitettl /></#local>
  <#return pagettl >
</#function>

<#macro x_pageMappingsList>
  <@cms.listEditablePages site="."+dsite ; x, n ><#local total=n /></@>
  <#if total?? >
    <div id="page-mappings" class="item-list">
      <div class="head">
        <p><@label key="001" tokens="total:${total}"/></p>
      </div>
      <div class="rows">
        <@cms.listEditablePages site="."+dsite ; x, n >
          <#local hc=x.handlerConf() />
          <div class="data <#if n%2==0>even</#if>">
            <a href="editPage.htm?id=${domain.id()}&pidx=${hc.index()}">
              <p class="pattern"><span><@label "013"/></span><span>${x.pattern()}</span></p>
              <p class="title"><span><@label "014"/></span><span>${x_pageTitle(hc.index())}</span></p>
            </a>
          </div>
        </@>
      </div>
    </div>

  <#else>

    <@label "002"/>

  </#if>
</#macro>

<#if domain?? >

  <@h.domainDefaultSite var="dsite" domain=domain mode="key" />
  <@x_pageMappingsList />

  <@htm.nodiv "page-ivars">
    {
      domainId: "${domain.id()}",
      pageIndexes: [<@cms.listEditablePages site="."+dsite ; x >"${x.handlerConf().index()}",</@>""]
    }
  </@>

  <@htm.nodiv "context-navigation-content">
    <a class="add-webpage" href="#" title="<@label "015"/>"><@label "016"/></a>
  </@>

  <@htm.nodiv "quick-navigation-content">
    <a href="../web.htm?id=${domain.id()}"><@label "017"/></a>
  </@>

  <@htm.nodiv "page-messages">
    <div data-key="add-webpage-title"><@label "003"/></div>
    <div data-key="button-ok"><@label "btn.ok"/></div>
    <div data-key="button-cancel"><@label "btn.cancel"/></div>
    <div data-key="index-exists"><@label "004"/></div>
  </@>

  <@htm.nodiv "page-templates">

    <div id="add-webpage-frame">
      <div class="form-panel">
        <div class="cell pattern">
          <label><@label "005"/></label>
          <input type="text" maxlength="250" value="" title="<@label "006"/>"/>
          <p class="error"></p>
        </div>
        <div class="cell index">
          <label><@label "007"/></label>
          <input type="text" maxlength="100" value="" title="<@label "008"/>" placeholder="<@label "009"/>"/>
          <p class="error"></p>
        </div>
        <div class="cell template">
          <label><@label "010"/></label>
          <input type="text" maxlength="100" value="" title="<@label "011"/>" placeholder="<@label "009"/>"/>
          <p class="error"></p>
        </div>
      </div>
    </div>

  </@>

<#else>

  <@label "x.nodom"/>

</#if>
