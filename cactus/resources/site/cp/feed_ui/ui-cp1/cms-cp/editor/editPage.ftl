<#import "/web/common.ftl" as c >
<#import "/web/common-htm.ftl" as htm >
<#include "/web/macros.ftl:"+currentSite>

<#macro x_setPageMapping>
  <#local pidx><@h.requestParamValue name="pidx" /></#local>
  <@cms.listEditablePages site="."+dsite ; x >
    <#local hc=x.handlerConf() />
    <#if hc.index()==pidx><#assign pageMapping=x /></#if>
  </@>
</#macro>

<#function x_sitemap>
  <#list (pageMapping.params()!"")?split(",") as x >
    <#if x?trim=="sitemap" ><#return true ></#if>
  </#list>
  <#return false >
</#function>

<#function x_hidden>
  <#list (pageMapping.params()!"")?split(",") as x >
    <#if x?trim=="lock" ><#return true ></#if>
  </#list>
  <#return false >
</#function>

<#macro x_selectDomainSites>
  <select>
    <option></option>
    <#if domain?? >
      <@h.listInputSites var="labels" />
      <@h.listDomainInputSites domain=domain ; site, n >
        <@h.siteLocale var="lng" key=labels[n] />
        <option value="${site}">${site} &nbsp; <@label "lng."+lng /></option>
      </@>
      <@h.listDomainInputSites domain=domain xtra="j" ; site, n >
        <@h.siteLocale var="lng" key=labels[n] />
        <option value="${site}"><@label "lng."+lng /> &nbsp; ${site}</option>
      </@>
    </#if>
  </select>
</#macro>


<#if domain?? >

  <@h.domainDefaultSite var="dsite" domain=domain mode="key" />
  <@x_setPageMapping />

  <#if pageMapping?? >

    <#assign pageMappingHC=pageMapping.handlerConf() />

    <div class="navigation-pane small">
      <a class="page" href="#" title="<@label "001"/>"><@label "002"/></a>
      <a class="system" href="#" title="<@label "003"/>"><@label "004"/></a>
      <a class="delete" href="#" title="<@label "005"/>"><@label "006"/></a>
      <a class="save" href="#" title="<@label "007"/>"><@label "008"/></a>
      <a class="view" href="#" title="<@label "009"/>"><@label "010"/></a>
    </div>
    <div class="nofloat"></div>

    <@htm.form id="page-form">
      <div class="form-panel">
        <h3 class="cell"><@label "011"/></h3>
        <div class="two-column">
          <div class="cell left pattern">
            <label><@label "012"/></label>
            <input type="text" maxlength="250" value="${pageMapping.pattern()}" title="<@label "013"/>"/>
            <p class="error"></p>
          </div>
          <div class="cell right sitemap">
            <label><@label "014"/></label>
            <input type="checkbox" <#if x_sitemap() > checked="true" </#if> title="<@label "015"/>"/>
            <p class="error"></p>
          </div>
          <div class="cell left"></div>
          <div class="cell right hidden">
            <label><@label "016"/></label>
            <input type="checkbox" <#if x_hidden() > checked="true" </#if> title="<@label "017"/>"/>
            <p class="error"></p>
          </div>
        </div>
        <div class="cell description">
          <label><@label "018"/></label>
          <input type="text" maxlength="250"
                 value="<@h.label key="met.d${pageMappingHC.index()}" site="."+dsite bydefault="" />" />
        </div>
        <div class="cell keywords">
          <label><@label "019"/></label>
          <input type="text" maxlength="250"
                 value="<@h.label key="met.k${pageMappingHC.index()}" site="."+dsite bydefault="" />" />
        </div>
        <div class="tabs site-tabs">
          <@domainInputSiteTabs domain />
          <@h.listDomainInputSites domain=domain ; site >
            <div id="site-tab-${site}">
              <div class="cell page-title">
                <label><@label "020"/></label>
                <input type="text" maxlength="250"
                       value="<@h.label key="ttl.p${pageMappingHC.index()}" site="."+site bydefault="" />" />
                <p class="error"></p>
              </div>
            </div>
          </@>
        </div>
      </div>

      <div class="form-panel">
        <h3 class="cell"><@label "021"/></h3>
        <div class="cell page-body">
          <label>
            <@label "022"/> <br/>
            <a class="save icon" href="#" title="<@label "023"/>"></a>
            <a class="backup icon" href="#" title="<@label "025"/>"></a>
            <a class="rich icon" href="#" title="<@label "024"/>"></a>
            <a class="links icon" href="#" title="<@label "026"/>"></a>
          </label>
          <#assign tmppath="web/pages/${pageMappingHC.pageName()}.ftl" />
          <textarea class="fix" data-path="${tmppath}"><@h.printFile path="${tmppath}" site="."+dsite output="html-lite" /></textarea>
          <p class="error"></p>
        </div>
        <div class="cell page-css">
          <label>
            <@label "027"/> <br/>
            <a class="save icon" href="#" title="<@label "023"/>"></a>
            <a class="backup icon" href="#" title="<@label "025"/>"></a>
          </label>
          <#assign tmppath="web/pages/${pageMappingHC.pageName()}.css" />
          <textarea class="css" data-draft="2" data-path="${tmppath}"><@h.printFile path="${tmppath}" site="."+dsite output="html-lite" /></textarea>
        </div>
        <div class="cell page-js">
          <label>
            <@label "028"/> <br/>
            <a class="save icon" href="#" title="<@label "023"/>"></a>
            <a class="backup icon" href="#" title="<@label "025"/>"></a>
          </label>
          <#assign tmppath="web/pages/${pageMappingHC.pageName()}.js" />
          <textarea class="js" data-path="${tmppath}"><@h.printFile path="${tmppath}" site="."+dsite output="html-lite" /></textarea>
        </div>
      </div>

      <div class="form-panel decor">
        <h3 class="cell expander"><@label "029"/></h3>
        <div class="expand">
          <@h.hasFile path="web/pages/layout-top.ftl" site="."+dsite var="tmphas" />
          <#if tmphas >
            <div class="cell top-layout">
              <label>
                <@label "030"/> <br/>
                <a class="save icon" href="#" title="<@label "023"/>"></a>
                <a class="backup icon" href="#" title="<@label "025"/>"></a>
              </label>
              <#assign tmppath="web/pages/layout-top.ftl" />
              <textarea data-path="${tmppath}"><@h.printFile path="${tmppath}" site="."+dsite output="html-lite" /></textarea>
              <p class="error"></p>
            </div>
            <div class="cell bottom-layout">
              <label>
                <@label "081"/> <br/>
                <a class="save icon" href="#" title="<@label "023"/>"></a>
                <a class="backup icon" href="#" title="<@label "025"/>"></a>
              </label>
              <#assign tmppath="web/pages/layout-bottom.ftl" />
              <textarea data-path="${tmppath}"><@h.printFile path="${tmppath}" site="."+dsite output="html-lite" /></textarea>
              <p class="error"></p>
            </div>
          <#else>
            <div class="cell layout">
              <label>
                <@label "085"/> <br/>
                <a class="save icon" href="#" title="<@label "023"/>"></a>
                <a class="backup icon" href="#" title="<@label "025"/>"></a>
              </label>
              <#assign tmppath="web/pages/layout.ftl" />
              <textarea data-path="${tmppath}"><@h.printFile path="${tmppath}" site="."+dsite output="html-lite" /></textarea>
              <p class="error"></p>
            </div>
          </#if>
          <div class="cell main-css">
            <label>
              <@label "031"/> <br/>
              <a class="save icon" href="#" title="<@label "023"/>"></a>
              <a class="backup icon" href="#" title="<@label "025"/>"></a>
            </label>
            <#assign tmppath="web/main.css" />
            <textarea class="css" data-draft="1" data-path="${tmppath}"><@h.printFile path="${tmppath}" site="."+dsite output="html-lite" /></textarea>
            <p class="error"></p>
          </div>
          <div class="cell code-js">
            <label>
              <@label "080"/> <br/>
              <a class="save icon" href="#" title="<@label "023"/>"></a>
              <a class="backup icon" href="#" title="<@label "025"/>"></a>
            </label>
            <#assign tmppath="web/haala/code.js" />
            <textarea class="js" data-path="${tmppath}"><@h.printFile path="${tmppath}" site="."+dsite output="html-lite" /></textarea>
          </div>
        </div>
      </div>

      <div id="picture-list" class="item-list" data-state="10,${searchState!}">
        <div class="head expander">
          <p><@label "032"/></p>
        </div>
        <div class="expand">
          <div class="rows"></div>
          <div class="foot">
            <div class="page-spread"></div>
          </div>
        </div>
      </div>
    </@>

    <@htm.form id="system-form">
      <div class="form-panel">
        <h3 class="cell"><@label "033"/></h3>
        <div class="two-column">
          <div class="cell left index">
            <label><@label "034"/></label>
            <input type="text" maxlength="100" value="${pageMappingHC.index()}" title="<@label "035"/>"/>
            <p class="error"></p>
          </div>
          <div class="cell right downtime">
            <label><@label "036"/></label>
            <input type="checkbox" <#if (pageMappingHC.downtime()!"0")=="0" > checked="true" </#if> title="<@label "037"/>"/>
            <p class="error"></p>
          </div>
          <div class="cell left template">
            <label><@label "038"/></label>
            <input type="text" maxlength="100" value="${pageMappingHC.pageName()}" title="<@label "039"/>"/>
            <p class="error"></p>
          </div>
          <div class="cell right handler">
            <label><@label "040"/></label>
            <input type="text" maxlength="100" value="${pageMapping.handler()}" title="<@label "041"/>"/>
            <p class="error"></p>
          </div>
          <div class="cell left mods">
            <label><@label "042"/></label>
            <input type="text" maxlength="100" value="${pageMappingHC.mod()!}" title="<@label "043"/>"/>
            <p class="error"></p>
          </div>
          <div class="cell right params">
            <label><@label "044"/></label>
            <input type="text" maxlength="100" value="${pageMapping.params()!}" title="<@label "045"/>"/>
            <p class="error"></p>
          </div>
          <div class="cell left facades">
            <label><@label "046"/></label>
            <input type="text" maxlength="100" value="${pageMappingHC.facade()!}" title="<@label "047"/>"/>
            <p class="error"></p>
          </div>
          <div class="cell right options">
            <label><@label "048"/></label>
            <input type="text" maxlength="100" value="${pageMappingHC.options()!}" title="<@label "049"/>"/>
            <p class="error"></p>
          </div>
        </div>
        <div class="nofloat"></div>
      </div>
    </@>

    <@htm.form id="save-form">
      <div class="form-panel">
        <h3 class="cell"><@label "050"/></h3>
        <div class="cell notes">
          <ol>
            <li class="index"><@label "051"/></li>
            <li class="template"><@label "052"/></li>
          </ol>
        </div>
        <div class="cell click">
          <p><@label "053"/></p>
        </div>
        <div class="cell submit">
          <button><@label "btn.save"/></button>
        </div>
      </div>
    </@>

    <@htm.form id="delete-form">
      <div class="form-panel">
        <h3 class="cell"><@label "054"/></h3>
        <div class="cell notes">
          <@label "055"/>
        </div>
        <div class="cell click">
          <p><@label "056"/></p>
        </div>
        <div class="cell submit">
          <button><@label "btn.delete"/></button>
        </div>
      </div>
    </@>

    <@htm.form id="view-form">
      <div class="form-panel">
        <h3 class="cell"><@label "057"/></h3>
        <div class="cell notes">
          <@label "058"/>
        </div>
        <div class="cell click">
          <p><@label "059"/></p>
        </div>
        <div class="cell submit">
          <button><@label "btn.view"/></button>
        </div>
      </div>
    </@>

    <ul id="page-menu-picturelist" class="contextMenu">
      <li class="copy"><a href="#copy-url"><@label "060"/></a></li>
      <li class="copy"><a href="#copy-img"><@label "061"/></a></li>
      <li class="delete"><a href="#delete"><@label "062"/></a></li>
    </ul>

    <ul id="page-menu-backupfile" class="contextMenu">
      <li class="create"><a href="#create1"><@label "082"/> #1</a></li>
      <li class="create"><a href="#create2"><@label "082"/> #2</a></li>
      <li class="create"><a href="#create3"><@label "082"/> #3</a></li>
      <li class="separator" />
      <li class="view"><a href="#view1"><@label "083"/> #1</a></li>
      <li class="view"><a href="#view2"><@label "083"/> #2</a></li>
      <li class="view"><a href="#view3"><@label "083"/> #3</a></li>
    </ul>

    <@htm.nodiv "page-ivars">
      {
        domainId: "${domain.id()}", defSite: "<@h.domainDefaultSite domain=domain mode="key" />",
        domainInputSites: <@h.listDomainInputSites domain=domain output="json" />,
        pageIndex: "${pageMappingHC.index()}", pageName: "${pageMappingHC.pageName()}",
        pageIndexes: [<@cms.listEditablePages site="."+dsite ; x >"${x.handlerConf().index()}",</@>""],
        websiteUrl: "<@h.settingValue key="MainURL" site=dsite />"
      }
    </@>

    <@htm.nodiv "page-messages">
      <div data-key="dialog-srvapi"><@label "063"/></div>
      <div data-key="dialog-saved"><@label "064"/></div>
      <div data-key="dialog-nochanges"><@label "065"/></div>
      <div data-key="dialog-notpic"><@label "066"/></div>
      <div data-key="dialog-confirm-backup"><@label "084"/></div>
      <div data-key="pattern-wild"><@label "067"/></div>
      <div data-key="sitemap-hide"><@label "068"/></div>
      <div data-key="index-labels"><@label "069"/></div>
      <div data-key="template-body"><@label "070"/></div>
      <div data-key="index-exists"><@label "071"/></div>
      <div data-key="copy-prompt"><@label "072"/></div>
      <div data-key="add-picture-title"><@label "073"/></div>
      <div data-key="button-ok"><@label "btn.ok"/></div>
      <div data-key="button-cancel"><@label "btn.cancel"/></div>
      <div data-key="file-frame"><@label "cms.file-frame"/></div>
      <div data-key="upload"><@label "cms.upload"/></div>
      <div data-key="cancel"><@label "cms.cancel"/></div>
      <div data-key="save"><@label "cms.save"/></div>
      <div data-key="delete"><@label "cms.delete"/></div>
      <div data-key="file-frame-delete"><@label "cms.file-frame-delete"/></div>
    </@>

    <@htm.nodiv "quick-navigation-content">
      <a href="main.htm?id=${domain.id()}"><@label "074"/></a>
    </@>

    <@htm.nodiv "context-navigation-content">
      <a class="add-picture" href="#" title="<@label "076"/>"><@label "075"/></a>
    </@>

    <@htm.nodiv "page-templates">

      <div id="add-file-frame">
        <div class="form-panel">
          <div class="cell left folder">
            <label><@label "077"/></label>
            <input type="text" maxlength="100"/>
            <p class="error"></p>
          </div>
          <div class="cell right site">
            <label><@label "078"/></label>
            <@x_selectDomainSites />
            <p class="error"></p>
          </div>
        </div>
        <@htm.form id="add-file-upload-form" mode="upload">
          <input name="dat1" type="file"/>
        </@>
      </div>

    </@>

  <#else>

    <@label "079"/>

  </#if>

<#else>

  <@label "x.nodom"/>

</#if>
