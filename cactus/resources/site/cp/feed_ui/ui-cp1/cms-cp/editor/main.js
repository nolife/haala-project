
(function($) {

  Haala.Misc.onDocReady(function() {
    initNavigation();
  });



  var initNavigation = function() {
    var ivars = Haala.Misc.evalJSON("#page-ivars");
    if (!ivars) return;

    $("#context-navigation").kdxMenu({
      items: [
        { element: "#context-navigation .add-webpage",
          gotoFn: function(/*json*/ item, /*json*/ model) {
            addWebpageFrame();
          }}
      ]
    });

  };

  var patternToIndex = function() {
    var value = "";
    var pattern = $("#add-webpage-frame .pattern input").val().trim();
    if (pattern.indexOf("*") == -1 && pattern.indexOf(" ") == -1)
      pattern.split("/").forEach(function(v) {
        if (v) value += (value ? v[0].toUpperCase() : v[0].toLowerCase()) + v.substring(1);
      });
    return value.split(".")[0];
  };

  var generateIndex = function() {
    var psel = "#add-webpage-frame .pattern input";
    var vsel = "#add-webpage-frame .index input";
    var f = function() {
      var pattern = $(psel).val().trim(), value = $(vsel).val().trim();
      if (pattern && !value) $(vsel).val("00"+patternToIndex());
    };
    $(psel).blur(f);
  };

  var generateTemplate = function() {
    var psel = "#add-webpage-frame .pattern input";
    var vsel = "#add-webpage-frame .template input";
    var f = function() {
      var pattern = $(psel).val().trim(), value = $(vsel).val().trim();
      if (pattern && !value) $(vsel).val("other/"+patternToIndex());
    };
    $(psel).blur(f);
  };

  var addWebpageFrame = function() {
    var ivars = Haala.Misc.evalJSON("#page-ivars");
    var msgFn = function(/*string*/ msg, /*json*/ tokens) {
      return Haala.Misc.messageMapping("#page-messages", msg, tokens);
    };
    var root = "#add-webpage-frame";
    var finp = function(key, vld) { return Haala.Util.panelFormInput(key, vld, { root: root })};

    var validateIndex = function() {
      var newval = $(root+" .index input").val().trim();
      var reason = null;
      if (ivars.pageIndexes.indexOf(newval) != -1) reason = msgFn("index-exists");
      return reason;
    };

    var panel = $(root).kdxPanel(
      $.extend(true, {}, Haala.KdxPanel.Integ.DefaultOptions, {
        input: [
          finp("pattern",  { require: true }),
          finp("index",    { require: true, userFn: validateIndex }),
          finp("template", { require: true }),
        ]
      }));

    $(root).dialog({
      zIndex: 70, width: 900, height: 280, resizable: false,
      buttons: [
        { text: msgFn("button-ok"), click: function() {
          if (panel.validate()) {
            var container = panel.collect();
            var command = Haala.Misc.collectInput(container,
              ["pattern", "index", "template"]);
            command.pageName = command.template;
            command.handler = "dynamicCtrl";
            command.options = "ftl-page";
            command.domainId = ivars.domainId;

            addPageMapping(command);
            $(this).dialog("close");
          } else {
            Haala.Util.errorToViewport({ root: root });
          }
        }},
        { text: msgFn("button-cancel"), click: function() {
          $(this).dialog("close");
        }}
      ],
      title: msgFn("add-webpage-title"),
      close: function() {
        panel.validate("reset");
      }
    });

    var addPageMapping = function(/*json*/ command) {
      Haala.AJAX.call("body", CmsF.addPageMapping, command,
        function() { document.location = "editPage.htm?id="+ivars.domainId+"&pidx="+command.index; return Haala.AJAX.DO_NOTHING; });
    };

    generateIndex();
    generateTemplate();
  };

})(jQuery);
