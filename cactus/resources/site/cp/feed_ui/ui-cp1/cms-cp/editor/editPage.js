
(function($) {

  Haala.Misc.onDocReady(function() {
    initNavigation();
    initForms();
    confirmUnload();
    initPictureGallery();
    initTemplateBackup();
    onTemplateEdit();
    onOtherEdit();
  });



  var pictureListReady = false;


  var msgFn = function(/*string*/ msg, /*json*/ tokens) {
    return Haala.Misc.messageMapping("#page-messages", msg, tokens);
  };


  var confirmUnload = function() {
    $(window).bind('beforeunload', function(e) {
      if (!hasChanges()) e = null;
      else return true;
    });

  };


  var initNavigation = function() {
    var ivars = Haala.Misc.evalJSON("#page-ivars");
    if (!ivars) return;

    $("#content .navigation-pane").kdxMenu({
      items: [
        { element: "#content .navigation-pane .page",
          gotoFn: function(/*json*/ item, /*json*/ model) {
            if (validate()) switchForm("#page-form");
          }},
        { element: "#content .navigation-pane .system",
          gotoFn: function(/*json*/ item, /*json*/ model) {
            if (validate()) switchForm("#system-form");
          }},
        { element: "#content .navigation-pane .view",
          gotoFn: function(/*json*/ item, /*json*/ model) {
            switchForm("#view-form");
          }},
        { element: "#content .navigation-pane .save",
          gotoFn: function(/*json*/ item, /*json*/ model) {
            if (!hasChanges()) Haala.Messages.showOK(msgFn("dialog-nochanges"), msgFn("title-info"));
            else if (validate()) switchForm("#save-form");
          }},
        { element: "#content .navigation-pane .delete",
          gotoFn: function(/*json*/ item, /*json*/ model) {
            switchForm("#delete-form");
          }}
      ]
    });

    $("#context-navigation").kdxMenu({
      items: [
        { element: "#context-navigation .add-picture",
          gotoFn: function(/*json*/ item, /*json*/ model) {
            addFileFrame();
          }}
      ]
    });

  };

  var validate = function() {
    var b = true;
    ["#page-form", "#system-form"].forEach(function(form) {
      var panel = $(form).kdxPanel();
      if (b && !panel.validate()) {
        b = false;
        switchForm(form);
        Haala.Util.errorToViewport({ root: form });
      }
    });

    return b;
  };

  var switchForm = function(/*selector*/ form) {
    if (!$(form).is(":visible")) {
      ["#page-form", "#system-form", "#save-form", "#delete-form", "#view-form"].forEach(function(v) {
        $(v).hide();
      });
      $(form).show();
      Haala.Code.stretch();
    }
  };

  var initForms = function() {
    var ivars = Haala.Misc.evalJSON("#page-ivars");
    if (!ivars) return;

    var validateSitemap = function() {
      var pattern = $("#page-form .pattern input").val();
      var hidden = $("#page-form .hidden input:checked").val();
      var checked = $("#page-form .sitemap input:checked").val();
      var reason = null;
      if (checked && pattern.contains("*")) reason = msgFn("pattern-wild");
      else if (checked && hidden) reason = msgFn("sitemap-hide");
      return reason;
    };

    var validateIndex = function() {
      var forbid = $("#page-form input").hasClass("change-label");
      var newval = $("#system-form .index input").val().trim();
      var changed = ivars.pageIndex != newval;
      var reason = null;
      if (changed)
        if (forbid) reason = msgFn("index-labels");
        else if (ivars.pageIndexes.indexOf(newval) != -1) reason = msgFn("index-exists");

      if (changed) $("#save-form .notes .index").show();
      else $("#save-form .notes .index").hide();
      return reason;
    };

    var validateTemplate = function() {
      var forbid = $("#page-form textarea").hasClass("change-template");
      var changed = ivars.pageName != $("#system-form .template input").val().trim();
      var reason = null;
      $("#save-form .notes .template").hide();
      if (forbid && changed) reason = msgFn("template-body");

      if (changed) $("#save-form .notes .template").show();
      else $("#save-form .notes .template").hide();
      return reason;
    };

    /* Using domain input sites instead of root input sites as the changes should be saved only for this domain.
     eg. domain owner wants to change its page title
     */
    var sites = ivars.domainInputSites;
    var finp = Haala.Util.panelFormInput;

    var input = [                          
      finp("pattern", { require: true }),
      finp("sitemap", { userFn: validateSitemap }),
      finp("hidden"),
      finp("keywords"),
      finp("description"),
      finp("page-body", { require: true }), // templates to upload
      finp("page-css"),
      finp("page-js"),
      finp("top-layout"),
      finp("bottom-layout"),
      finp("layout"),
      finp("main-css"),
      finp("code-js")
    ];
    $.each(sites, function(n, site) {
      input.push({ element: "#site-tab-"+site+" .page-title input", key: "title_"+site});
    });
    $("#page-form").kdxPanel(
        $.extend(true, {}, Haala.KdxPanel.Integ.DefaultOptions, {
          input: input
        }));

    var sform = "#system-form";
    $(sform).kdxPanel(
        $.extend(true, {}, Haala.KdxPanel.Integ.DefaultOptions, {
          input: [
            finp("index", { require: true, userFn: validateIndex }, { root: sform }),
            finp("handler", { require: true }, { root: sform }),
            finp("mods", {}, { root: sform }),
            finp("params", {}, { root: sform }),
            finp("facades", {}, { root: sform }),
            finp("options", {}, { root: sform }),
            finp("downtime", {}, { root: sform }),
            finp("template", { require: true, userFn: validateTemplate }, { root: sform })
          ]
        }));

    sform = "#save-form";
    Haala.Util.formSubmitPanel($(sform).kdxPanel(), save, { root: sform });
    $(sform+" .submit button").button();

    sform = "#delete-form";
    Haala.Util.formSubmitPanel($(sform).kdxPanel(), removePageMapping, { root: sform });
    $(sform+" .submit button").button();

    sform = "#view-form";
    Haala.Util.formSubmitPanel($(sform).kdxPanel(), viewPage, { root: sform });
    $(sform+" .submit button").button();

    $("#page-form .site-tabs").tabs();

    $.each($("#page-form textarea"), function() {
      var x = $(this);
      x.focus(function() {
        x.addClass("expanded");
        x.prop("wrap", "off");
        Haala.Code.stretch();
      });
      x.blur(function() {
        x.removeClass("expanded");
        x.prop("wrap", null);
        Haala.Code.stretch();
      });
    });

    $("#page-form .decor .expander").aclick(function() {
      var elem = $("#page-form .decor .expand")
      if (elem.is(":visible")) elem.hide();
      else elem.show();
      Haala.Code.stretch();
    });

  };

  var save = function() {
    var ivars = Haala.Misc.evalJSON("#page-ivars");
    var incRV = $("#page-form textarea.css").hasClass("change-template") ||
        $("#page-form textarea.js").hasClass("change-template");

    var incResourceVersionFn = function(/*fn*/ onSuccessFn) {
      var successFn = function() { setTimeout(onSuccessFn, 10); };
      return incRV ? function() { Haala.AJAX.call("body", CmsF.incVersion, { site: ivars.defSite }, successFn, onFailFn); } : successFn;
    };

    var updatePageMappingFn = function(/*fn*/ onSuccessFn) {
      var successFn = function() {
        setTimeout(onSuccessFn, 10);
      };
      var pageData = $("#page-form").kdxPanel().collect();
      var systData = $("#system-form").kdxPanel().collect();
      var command = Haala.Misc.collectInput(systData,
          ["index", "handler", "mods", "params", "facades", "options", "downtime", "template"],
          ["index", "handler", "mod",  "params", "facade",  "options", "downtime", "pageName"]);
      command.pattern = pageData.pattern;
      command.domainId = ivars.domainId;
      command.oindex = ivars.pageIndex;
      return function() { Haala.AJAX.call("body", CmsF.editPageMapping, command, successFn, onFailFn); };
    };

    var updateLabelsFn = function(/*fn*/ onSuccessFn) {
      var successFn = function() {
        $("#page-form input").removeClass("change-label");
        setTimeout(onSuccessFn, 10);
      };
      var data = $("#page-form").kdxPanel().collect();
      var command = Haala.Misc.collectInput(data, ["keywords", "description"]);
      command.title = Haala.Misc.collectSiteableInput(data, ["title"], null, ivars.domainInputSites);
      command.index = $("#system-form").kdxPanel().collect().index;
      command.domainId = ivars.domainId;
      var changed = $("#page-form input").hasClass("change-label");
      return changed ? function() { Haala.AJAX.call("body", CmsF.editPageLabels, command, successFn, onFailFn); } : successFn;
    };

    var updateTemplatesFn = function(/*fn*/ onSuccessFn) {
      var changed = [];
      $("#page-form").kdxPanel().model.input.forEach(function(input) {
        var elem = input.element;
        if (elem.hasClass("change-template")) {
          var text = Haala.Misc.inputValue(elem);
          if (elem.hasClass("fix")) text = postProcessImgTags(text);
          changed.push({ path: elem.attr("data-path"), value: text })
        }
      });

      var successFn = function() {
        $("#page-form textarea").removeClass("change-template");
        setTimeout(onSuccessFn, 10);
      };
      var editFileFn = function(/*str*/ path, /*str*/ value, /*fn*/ nextFn) {
        var command = { path: path, site: ivars.defSite, value: value };
        return function() { Haala.AJAX.call("body", CmsF.editTextFile, command, nextFn, onFailFn); };
      };
      var fnx = [];
      changed.forEach(function(x,n) {
        var nextFn = function() { setTimeout(fnx[n-1] ? fnx[n-1] : successFn, 10); };
        fnx.push(editFileFn(x.path, x.value, nextFn));
      });
      return fnx.length ? fnx[fnx.length-1] : successFn;
    };

    var onFailFn = function() {
      Haala.Messages.closeAJAX();
      return false;
    };

    Haala.Messages.showAJAX();
    updatePageMappingFn( // 1 of 4
        updateLabelsFn( // 2 of 4
            updateTemplatesFn( // 3 of 4
                incResourceVersionFn( // 4 of 4
                    function() {
                      disregardChanges();
                      Haala.Messages.showOK(msgFn("dialog-saved"), msgFn("title-info"), function() {
                        document.location = "editPage.htm?id="+ivars.domainId+"&pidx="+$("#system-form").kdxPanel().collect().index;
                      });
                    }))))();

  };

  var removePageMapping = function() {
    var ivars = Haala.Misc.evalJSON("#page-ivars");
    var command = {};
    command.domainId = ivars.domainId;
    command.index = ivars.pageIndex;

    disregardChanges();
    Haala.AJAX.call("body", CmsF.removePageMapping, command,
        function() { document.location = "main.htm?id="+ivars.domainId; return Haala.AJAX.DO_NOTHING; });
  };

  var viewPage = function() {
    var ivars = Haala.Misc.evalJSON("#page-ivars");
    var url = ivars.websiteUrl, ptr = $("#page-form").kdxPanel().collect().pattern;
    if (url[url.length-1] == "/") url = url.substring(0, url.length-1);
    if (!ptr || ptr.contains("*")) ptr = "/";
    if (ptr[0] != "/") ptr = "/"+ptr;
    window.open(url+ptr);
  };

  var imgTags = function(/*str*/ html) {
    var srcval = function(/*str*/ tag) {
      var attr = "";
      tag.split(" ").forEach(function(x) {
        if (x[0] == "s" && x[1] == "r" && x[2] == "c" && x[3] == "=") attr = x;
      });
      var c = attr.indexOf("'") != -1 ? "'" : attr.indexOf("\"") != -1 ? "\"" : "";
      return c ? attr.substring(attr.indexOf(c)+1, attr.lastIndexOf(c)) : null
    };

    var tags = [], i = 0;
    do {
      i = html.indexOf("<img ", i);
      if (i != -1) {
        var tag = "", z = i;
        for (; z < html.length; z++) {
          tag += html[z];
          if (html[z] == "<" && tag != "<") { tag = ""; break; } //broken - not closed
          if (html[z] == ">") break; //closed
        }
        if (tag && tag[tag.length-1] == ">") tags.push(tag);
        i = z-1; //step back in case of broken tag
      }
    } while (i != -1);

    var data = [];
    tags.forEach(function(x) {
      data.push({ tag: x, src: srcval(x) });
    });
    return data;
  };

  var preProcessImgTags = function(/*str*/ html) {
    var ivars = Haala.Misc.evalJSON("#page-ivars");
    imgTags(html).forEach(function(x) {
      if (!x.src.contains("://") && !x.src.contains("?")) {
        var nsrc = x.src+"?site="+ivars.defSite+"&cms=1&nc=1&salt="+Math.random();
        var ntag = x.tag.replace(x.src, nsrc);
        html = html.replace(x.tag, ntag);
      }
    });

    return html;
  };

  var postProcessImgTags = function(/*str*/ html) {
    var ivars = Haala.Misc.evalJSON("#page-ivars");
    imgTags(html).forEach(function(x) {
      if (x.src.contains("?site="+ivars.defSite)) {
        var nsrc = x.src.substring(0, x.src.indexOf("?"));
        var ntag = x.tag.replace(x.src, nsrc);
        html = html.replace(x.tag, ntag);
      }
    });

    return html;
  };

  var copyToClipboard = function(/*str*/ text) {
    window.prompt(msgFn("copy-prompt"), text);
  };

  var initPictureGallery = function() {
    var ivars = Haala.Misc.evalJSON("#page-ivars");

    var addContextMenu = function(/*element*/ e, /*json*/ vo) {
      e.contextMenu({ menu: "page-menu-picturelist" },
          function(action, el, pos) {
            if (action == "copy-url") copyToClipboard(vo.src);
            if (action == "copy-img") copyToClipboard(vo.img);
            if (action == "delete") removeFile(vo);
          });
    };

    var listModel = {
      loadItemsFn: function(/*json*/ model, /*fn*/ callback) {
        Haala.AquaList.Integ.loadItemsFn(CmsF.searchFiles, { target: 3, domainId: ivars.domainId }, model, callback);
      },
      rowElementFn: function(/*json*/ vo, /*int*/ position, /*json*/ model) {
        if (vo) {
          vo.src = "/"+vo.path+"?site="+vo.site+"&cms=1&nc=1&salt="+Math.random();
          vo.img = "<img src='"+vo.src+"' />";
          var e = $("<div class='data'><div><img src='"+ vo.src+"' /></div><p>"+vo.name+"</p></div>");
          e.aclick(function() { copyToClipboard(vo.src); });
          addContextMenu(e, vo);
          return e;
        } else {
          return $("<div class='empty'></div>");
        }
      }
    };

    $("#picture-list .expander").aclick(function() {
      var elem = $("#picture-list .expand");
      if (elem.is(":visible")) elem.hide();
      else {
        elem.show();

        if (!pictureListReady) { //create on first expand
          pictureListReady = true;
          var list = $("#picture-list .rows").aquaList(
              $.extend(true, {},
                  Haala.AquaList.Integ.DefaultOptions,
                  Haala.AquaList.Integ.parseState($("#picture-list").attr("data-state")),
                  listModel));
          list.addListener("rendered", Haala.Code.stretch);
          Haala.AquaList.Integ.addPageSpread(list,
              $("#picture-list .page-spread").pageSpread(Haala.PageSpread.Integ.DefaultOptions));
        }
      }
      Haala.Code.stretch();
    });

    var removeFile = function(/*json*/ vo) {
      var cmd = { fileId: vo.id };
      var msg = msgFn("file-frame-delete", { name: vo.name, site: vo.site });
      Haala.Messages.showYN(msg, msgFn("title-confirm"), function() {
        Haala.AJAX.call("body", CmsF.removeFile , cmd, function() { refreshPicturesList(); });
      });
    };

  };

  var addFileFrame = function() {
    var ivars = Haala.Misc.evalJSON("#page-ivars");

    var finp = function(key, vld) { return Haala.Util.panelFormInput(key, vld, { root: "#add-file-frame" })};
    var panel = $("#add-file-frame").kdxPanel(
        $.extend(true, {}, Haala.KdxPanel.Integ.DefaultOptions, {
          input: [
            finp("folder", { require: true }),
            finp("site",   { require: true })
          ]
        }));

    $("#add-file-frame").dialog({
      zIndex: 70, width: 370, height: 240, resizable: false,
      buttons: [
        { text: msgFn("upload"), click: function() {
          if (panel.validate()) {
            var command = Haala.Misc.collectInput(panel.collect(), ["folder", "site"]);
            addFile(command);
            $(this).dialog("close");
          } else {
            Haala.Util.errorToViewport({ root: "#add-setting-frame" });
          }
        }},
        { text: msgFn("button-cancel"), click: function() {
          $(this).dialog("close");
        }}
      ],
      title: msgFn("add-picture-title"),
      close: function() {
        panel.validate("reset");
        $("#add-setting-frame input").val("");
      }
    });

    var validatePicture = function() {
      var path = $("#add-file-upload-form input").val();
      var b = true;
      if (path) {
        var name = (function () {
          var xs = path.split("\\");
          xs = xs[xs.length-1].split("/");
          return xs[xs.length-1];
        })();
        var ext = (function () {
          var xs = name.split(".");
          return xs[xs.length-1];
        })();
        var allowed = ["jpg", "gif", "png"];
        if (allowed.indexOf(ext) == -1) {
          b = false;
          Haala.Messages.showOK(msgFn("dialog-notpic", { name: name }), msgFn("title-error"));
        }
      }
      return b;
    };

    var addFile = function(/*json*/ command) {
      if (validatePicture()) {
        var form = $("#add-file-upload-form").uploadForm(
            $.extend(true, {}, Haala.UploadForm.Integ.DefaultOptions, {
              uploadSuccessFn: function (/*json*/ model) {
                Haala.AJAX.call("body", CmsF.addFile, command, function () {
                  refreshPicturesList(1);
                });
              }
            }));
        form.upload();
      }
    };

    $("#add-file-frame .folder input").val("web/gfx/x");
    $("#add-file-frame .site select").val(ivars.defSite);
  };

  var refreshPicturesList = function(/*int*/ num) {
    if (pictureListReady) $("#picture-list .rows").aquaList().page(num);
  };


  var initTemplateBackup = function() {
    var ivars = Haala.Misc.evalJSON("#page-ivars");

    var addContextMenu = function(/*element*/ e, /*str*/ path) {
      e.contextMenu({ menu: "page-menu-backupfile" },
          function(action, el, pos) {
            if (action == "create1") backup(path, "up1");
            if (action == "create2") backup(path, "up2");
            if (action == "create3") backup(path, "up3");
            if (action == "view1") view(path, "up1")
            if (action == "view2") view(path, "up2")
            if (action == "view3") view(path, "up3")
          });
    };

    $.each($("#page-form a.backup"), function() {
      $(this).click(function(event) { event.preventDefault(); });
      var path = $(this).parents(".cell").find("textarea").attr("data-path");
      if (path) addContextMenu($(this), path)
    });

    var backup = function(/*str*/ path, /*str*/ sfx) {
      Haala.Messages.showYN(msgFn("dialog-confirm-backup"), msgFn("title-confirm"), function() {
        var command = { path: path, site: ivars.defSite, sfx: sfx };
        Haala.AJAX.call("body", CmsF.backupFile, command);
      });
    };

    var view = function(/*str*/ path, /*str*/ sfx) {
      window.open("/bin.htm?path="+path+"&site="+ivars.defSite+"&sfx="+sfx+"&cms=1&nc=1&salt="+Math.random())
    };

  };


  var onTemplateEdit = function() {
    var ivars = Haala.Misc.evalJSON("#page-ivars");

    $.each($("#page-form a.save"), function() {
      var x = $(this);
      var tarea = x.parents(".cell").find("textarea");
      var path = tarea.attr("data-path");
      if (path)
        x.aclick(function() {
          var text = tarea.val();
          if (tarea.hasClass("fix")) text = postProcessImgTags(text);
          var command = { path: path, site: ivars.defSite, value: text };
          Haala.AJAX.call("body", CmsF.editTextFile, command, function() {
            if (tarea.hasClass("css") || tarea.hasClass("js"))
              Haala.AJAX.call(null, CmsF.incVersion, { site: ivars.defSite });
            x.hide();
            tarea.removeClass("change-template");
            if (tarea.hasClass("css")) setTimeout(reapplyRichEdit, 10); //refresh on CSS change
          });
        });
    });

//    $.each($("#page-form a.backup"), function() {
//      var x = $(this);
//      var tarea = x.parents(".cell").find("textarea");
//      var path = tarea.attr("data-path");
//      if (path)
//        x.aclick(function() {
//          var command = { path: path, site: ivars.defSite, number: 1 };
//          Haala.AJAX.call("body", CmsF.backupFile, command, function() {
//            x.hide();
//            setTimeout(refreshBackups, 10);
//          });
//        });
//
//      var refreshBackups = function() {
//        Haala.AJAX.call("body", CmsF.loadFilePath, { path: path, site: ivars.defSite }, function(data) {
//          console.log(data); //todo
//        });
//      }
//    });

    $("#page-form textarea").change(function() {
      var x = $(this);
      x.addClass("change-template");
      x.parents(".cell").find("a.save").show();
      x.parents(".cell").find("a.backup").show();
    });

    var applyRichEdit = function() {
      var tarea = $("#page-form .page-body textarea");
      /* construct CSS to load in TinyMCE editor (draft versions with image ULRs modified) */
      var css = (function() {
        var pfx = "/bin.htm?path=web", sfx = "&site="+ivars.defSite+"&cms=1&nc=1&salt="+Math.random();
        var tinym = "/pages/cp/cms/editor/tinymce.css&nc=1";
        var draft = "/pages/draft-css/"+ivars.pageName+"-draft{n}.css";
        var x = pfx+tinym+", "+
            pfx+draft.replace("{n}", "1")+sfx+", "+
            pfx+draft.replace("{n}", "2")+sfx;
        return x;
      })();

      Haala.Misc.tinymce4(tarea, {
        plugins: "autolink lists table image hr link advlist contextmenu media fullscreen paste code directionality visualchars nonbreaking anchor charmap textcolor",
        toolbar1: "code | undo redo | bold italic underline strikethrough | alignleft aligncenter alignright alignjustify | formatselect fontselect fontsizeselect",
        toolbar2: "table | bullist numlist | outdent indent | forecolor backcolor | link unlink anchor image media | charmap hr nonbreaking | ltr rtl | fullscreen",
        menubar: true,
        body_id: "content",
        content_css: css,
        setup: function(editor) {
          editor.on("change", function() { tarea.trigger("change"); });
        },
        width: "730" // tinymce-4.9.1 bug: width of textarea calculated incorrectly
      });
      Haala.Code.stretch();
    };

    var reapplyRichEdit = function() {
      var tarea = $("#page-form .page-body textarea");
      if (tarea.hasClass("rich")) {
        tinyMCE.remove();
        draftCss(applyRichEdit);
      }
    };

    $("#page-form .page-body a.rich").aclick(function() {
      var tarea = $("#page-form .page-body textarea");
      var text = tarea.val();
      if (text.contains("<#") || text.contains("<@")) {
        Haala.Messages.showOK(msgFn("dialog-srvapi"), msgFn("title-warning"));
      } else {
        $("#page-form .page-body a.rich").hide();
        tarea.addClass("rich");
        text = preProcessImgTags(text);
        tarea.val(text);
        draftCss(applyRichEdit);

        $("#page-form .page-body a.links").show().aclick(function() {
          tarea.val(preProcessImgTags(tarea.val()));
        });
      }
    });
  };


  var onOtherEdit = function() {
    $("#system-form input").change(function() { $(this).addClass("change-input"); });
    $("#page-form input").change(function() { $(this).addClass("change-input"); });

    var addOrRemParam = function(/*str*/ key, /*bool*/ add) {
      var inp = $("#system-form .params input");
      var value = "";
      inp.val().split(",").forEach(function(v) {
        v = v.trim();
        if (v && v != key) value += ", "+v;
      });
      if (add) value += ", "+key;
      inp.val(value ? value.substring(2) : "");
    };

    $("#page-form .sitemap input").change(function() {
      addOrRemParam("sitemap", $(this).is(":checked"));
    });

    $("#page-form .hidden input").change(function() {
      addOrRemParam("lock", $(this).is(":checked"));
      var sm = $("#page-form .sitemap input");
      if ($(this).is(":checked") == sm.is(":checked")) sm.trigger("click");
    });

    $("#page-form .page-title input").change(function() { $(this).addClass("change-label"); });
    $("#page-form .description input").change(function() { $(this).addClass("change-label"); });
    $("#page-form .keywords input").change(function() { $(this).addClass("change-label"); });
  };


  var hasChanges = function() {
    var b =
        $("#system-form .change-input").length ||
        $("#page-form .change-input").length ||
        $("#page-form .change-template").length;
    return b;
  };

  var disregardChanges = function() {
    $("#system-form input").removeClass("change-input");
    $("#page-form input").removeClass("change-input");
    $("#page-form textarea").removeClass("change-template");
  };

  /** Create draft CSS with modified image URLs to load into TinyMCE */
  var draftCss = function(/*fn*/ callback) {
    var ivars = Haala.Misc.evalJSON("#page-ivars");
    var cmds = [];

    $.each($("#page-form textarea.css"), function() {
      var tarea = $(this);
      var path = tarea.attr("data-path");
      var draftN = tarea.attr("data-draft");
      if (path && draftN) {
        var text = tarea.val();
        /* xxxxx.[jpg|gif|png] -> xxxxx.[jpg|gif|png]?site=... */
        var sfx = "?site="+ivars.defSite+"&cms=1&nc=1&salt="+Math.random();
        ["jpg", "png", "gif"].forEach(function(ext) {
          ["."+ext+")", "."+ext+"\"", "."+ext+"'"].forEach(function(v) {
            var pfx = v.substring(0, v.length-1), last = v.substring(v.length-1);
            text = Haala.Misc.replaceAll(text, v, pfx+sfx+last);
          });
        });
        /* xxxxx.css -> xxxxx-draft2.css */
        path = "web/pages/draft-css/"+ivars.pageName+"-draft"+draftN+".css";
        var command = { path: path, site: ivars.defSite, value: text };
        cmds.push(command);
      }
    });

    var createDraftsFn = function(/*fn*/ onSuccessFn) {
      var successFn = function() {
        setTimeout(onSuccessFn, 10);
      };
      var editFileFn = function(/*str*/ path, /*str*/ value, /*fn*/ nextFn) {
        var command = { path: path, site: ivars.defSite, value: value };
        return function() { Haala.AJAX.call("body", CmsF.editTextFile, command, nextFn); };
      };
      var fnx = [];
      cmds.forEach(function(x,n) {
        var nextFn = function() { setTimeout(fnx[n-1] ? fnx[n-1] : successFn, 10); };
        fnx.push(editFileFn(x.path, x.value, nextFn));
      });
      return fnx[fnx.length-1];
    };

    createDraftsFn(callback)();
  };

})(jQuery);
