<#import "/web/common.ftl" as c >
<#import "/web/common-htm.ftl" as htm >
<#include "/web/macros.ftl:"+currentSite>

<#macro x_webpageSelect>
  <select>
    <option value=""></option>
    <@cms.listEditablePages site="."+dsite ; x >
      <#local hc=x.handlerConf() />
      <#local text >
        ${hc.index()} &nbsp; - &nbsp; ${hc.pageName()} &nbsp; - &nbsp; <@h.label key="ttl.p${hc.index()}" site="."+dsite bydefault=""/>
      </#local>
      <option value="${hc.index()}" <#if hc.index() == (webpage_hc.index())!> selected="true" </#if> > ${text} </option>
    </@>
  </select>
</#macro>

<#-- Choose webpage by request "pidx" parameter -->
<#macro x_webpage>
  <#local pidx=requestParams.pidx!/>
  <@cms.listEditablePages site="."+dsite ; x >
    <#if x.handlerConf().index() == pidx>
      <#assign webpage=x />
      <#assign webpage_hc=x.handlerConf() />
    </#if>
  </@>
</#macro>

<#macro x_selectDomainSites>
  <select>
    <option></option>
    <#if domain?? >
      <@h.listInputSites var="labels" />
      <@h.listDomainInputSites domain=domain ; site, n >
        <@h.siteLocale var="lng" key=labels[n] />
        <option value="${site}">${site} &nbsp; <@label "lng."+lng /></option>
      </@>
      <@h.listDomainInputSites domain=domain xtra="j" ; site, n >
        <@h.siteLocale var="lng" key=labels[n] />
        <option value="${site}"><@label "lng."+lng /> &nbsp; ${site}</option>
      </@>
    </#if>
  </select>
</#macro>


<#if domain?? >

  <@h.domainDefaultSite var="dsite" domain=domain mode="key" />
  <@x_webpage />

  <#if webpage?? >

    <@htm.form id="page-form">
      <div class="form-panel">
        <div class="cell select-webpage">
          <label><@label "003"/></label>
          <@x_webpageSelect />
        </div>
        <h3 class="cell"><@label "011"/></h3>
        <div class="two-column">
          <div class="cell left pattern">
            <label><@label "012"/></label>
            <input type="text" maxlength="100" value="${webpage.pattern()!}"/>
            <p class="error"></p>
          </div>
          <div class="cell right params">
            <label><@label "013"/></label>
            <input type="text" maxlength="100" value="${webpage.params()!}"/>
            <p class="error"></p>
          </div>
          <div class="cell left index">
            <label><@label "006"/></label>
            <input type="text" maxlength="100" value="${webpage_hc.index()}"/>
            <p class="error"></p>
          </div>
          <div class="cell right options">
            <label><@label "007"/></label>
            <input type="text" maxlength="100" value="${webpage_hc.options()!}"/>
            <p class="error"></p>
          </div>
          <div class="cell left pageName">
            <label><@label "008"/></label>
            <input type="text" maxlength="100" value="${webpage_hc.pageName()!}"/>
            <p class="error"></p>
          </div>
          <div class="cell right mod">
            <label><@label "009"/></label>
            <input type="text" maxlength="100" value="${webpage_hc.mod()!}"/>
            <p class="error"></p>
          </div>
          <div class="cell left handler">
            <label><@label "044"/></label>
            <input type="text" maxlength="100" value="${webpage.handler()!}"/>
            <p class="error"></p>
          </div>
          <div class="cell right facade">
            <label><@label "005"/></label>
            <input type="text" maxlength="100" value="${webpage_hc.facade()!}"/>
            <p class="error"></p>
          </div>
          <div class="cell left downtime">
            <label><@label "010"/></label>
            <input type="checkbox" <#if webpage_hc.downtime()! == "1" > checked="true" </#if> />
            <p class="error"></p>
          </div>
          <div class="cell right facade">
          </div>
        </div>
        <div class="cell submit">
          <button><@label "btn.save"/></button>
          <button type="button" class="remove"><@label "btn.delete"/></button>
        </div>
      </div>
    </@>

  <#else>

    <div class="form-panel">
      <div class="cell select-webpage">
        <label><@label "003"/></label>
        <@x_webpageSelect />
      </div>
      <div class="cell no-webpage">
        <p><@label "004"/></p>
      </div>
    </div>

  </#if>

  <div id="label-list" class="item-list" data-state="5,">
    <div class="filter">
      <input type="text" value="">
      <span class="on" title="<@label "025"/>" ></span>
      <span class="off" title="<@label "026"/>" ></span>
    </div>
    <div class="head">
      <a href="#" class="column-1 key"><@label "033"/><span class="arrow"></span></a>
      <p class="column-2 value"><@label "034"/></p>
      <p class="column-3 site"><@label "024"/></p>
    </div>
    <div class="rows"></div>
    <div class="foot">
      <div class="page-spread"></div>
    </div>
  </div>

  <div id="setting-list" class="item-list" data-state="5,">
    <div class="filter">
      <input class="key" type="text" value="">
      <input class="value" type="text" value="">
      <span class="on" title="<@label "025"/>" ></span>
      <span class="off" title="<@label "026"/>" ></span>
    </div>
    <div class="head">
      <a href="#" class="column-1 key"><@label "022"/><span class="arrow"></span></a>
      <p class="column-2 value"><@label "023"/></p>
      <p class="column-3 site"><@label "024"/></p>
    </div>
    <div class="rows"></div>
    <div class="foot">
      <div class="page-spread"></div>
    </div>
  </div>

  <div id="file-list" class="item-list" data-state="5,">
    <div class="filter">
      <input class="value" type="text" value="">
      <span class="on" title="<@label "025"/>" ></span>
      <span class="off" title="<@label "026"/>" ></span>
    </div>
    <div class="head">
      <a href="#" class="column-1 folder"><@label "027"/><span class="arrow"></span></a>
      <p class="column-2 name"><@label "028"/></p>
      <p class="column-3 site"><@label "024"/></p>
    </div>
    <div class="rows"></div>
    <div class="foot">
      <div class="page-spread"></div>
    </div>
  </div>

  <@htm.nodiv "quick-navigation-content">
    <a href="web.htm?id=${domain.id()}"><@label "002"/></a>
  </@>

  <@htm.nodiv "context-navigation-content">
    <a class="add-webpage" href="#" title="<@label "015"/>"><@label "014"/></a>
    <a class="add-label" href="#" title="<@label "019"/>"><@label "018"/></a>
    <a class="add-setting" href="#" title="<@label "021"/>"><@label "020"/></a>
    <a class="add-file" href="#" title="<@label "039"/>"><@label "038"/></a>
  </@>

  <@htm.nodiv "page-messages">
    <div data-key="add-webpage-title"><@label "016"/></div>
    <div data-key="button-ok"><@label "btn.ok"/></div>
    <div data-key="button-cancel"><@label "btn.cancel"/></div>
    <div data-key="webpage-delete"><@label "017"/></div>
    <div data-key="add-label-title"><@label "029"/></div>
    <div data-key="add-setting-title"><@label "030"/></div>
    <div data-key="add-file-title"><@label "041"/></div>
    <div data-key="dialog-confirm-title"><@label "cms.dialog-confirm-title"/></div>
    <div data-key="picture-frame"><@label "cms.picture-frame2"/></div>
    <div data-key="text-frame"><@label "cms.text-frame2"/></div>
    <div data-key="text-frame-delete"><@label "cms.text-frame-delete"/></div>
    <div data-key="file-frame-delete"><@label "cms.file-frame-delete"/></div>
    <div data-key="picture-frame-delete"><@label "cms.picture-frame-delete"/></div>
    <div data-key="file-frame"><@label "cms.file-frame"/></div>
    <div data-key="upload"><@label "cms.upload"/></div>
    <div data-key="file-open"><@label "cms.file-open"/></div>
    <div data-key="cancel"><@label "cms.cancel"/></div>
    <div data-key="save"><@label "cms.save"/></div>
    <div data-key="delete"><@label "cms.delete"/></div>
  </@>

  <ul id="page-menu-textframe" class="contextMenu">
    <li class="edit"><a href="#edit-normal"><@label "031"/></a></li>
    <li class="edit"><a href="#edit-rich"><@label "032"/></a></li>
    <li class="edit"><a href="#edit-plain"><@label "042"/></a></li>
  </ul>

  <ul id="page-menu-fileframe" class="contextMenu">
    <li class="edit"><a href="#edit-file"><@label "035"/></a></li>
    <li class="edit"><a href="#edit-picture"><@label "036"/></a></li>
    <li class="edit"><a href="#edit-text"><@label "037"/></a></li>
    <li class="edit"><a href="#edit-plain"><@label "043"/></a></li>
  </ul>

  <@htm.nodiv "page-ivars">
    { domainId: "${domain.id()}" }
  </@>

  <@htm.nodiv "page-templates">

    <#-- Copy of the above form with page index cleared -->
    <div id="add-webpage-frame">
      <div class="form-panel">
        <div class="two-column">
          <div class="cell left pattern">
            <label><@label "012"/></label>
            <input type="text" maxlength="100" value="${(webpage.pattern())!}"/>
            <p class="error"></p>
          </div>
          <div class="cell right params">
            <label><@label "013"/></label>
            <input type="text" maxlength="100" value="${(webpage.params())!}"/>
            <p class="error"></p>
          </div>
          <div class="cell left index">
            <label><@label "006"/></label>
            <input type="text" maxlength="100" value=""/>
            <p class="error"></p>
          </div>
          <div class="cell right options">
            <label><@label "007"/></label>
            <input type="text" maxlength="100" value="${(webpage_hc.options())!}"/>
            <p class="error"></p>
          </div>
          <div class="cell left pageName">
            <label><@label "008"/></label>
            <input type="text" maxlength="100" value="${(webpage_hc.pageName())!}"/>
            <p class="error"></p>
          </div>
          <div class="cell right mod">
            <label><@label "009"/></label>
            <input type="text" maxlength="100" value="${(webpage_hc.mod())!}"/>
            <p class="error"></p>
          </div>
          <div class="cell left handler">
            <label><@label "044"/></label>
            <input type="text" maxlength="100" value="${(webpage.handler())!}"/>
            <p class="error"></p>
          </div>
          <div class="cell right facade">
            <label><@label "005"/></label>
            <input type="text" maxlength="100" value="${(webpage_hc.facade())!}"/>
            <p class="error"></p>
          </div>
          <div class="cell left downtime">
            <label><@label "010"/></label>
            <input type="checkbox" <#if (webpage_hc.downtime())! == "1" > checked="true" </#if> />
            <p class="error"></p>
          </div>
          <div class="cell right">
          </div>
        </div>
      </div>
    </div>

    <div id="add-label-frame">
      <div class="form-panel">
        <div class="cell left key">
          <label><@label "022"/></label>
          <input type="text" maxlength="100"/>
          <p class="error"></p>
        </div>
        <div class="cell right site">
          <label><@label "024"/></label>
          <@x_selectDomainSites />
          <p class="error"></p>
        </div>
      </div>
    </div>

    <div id="add-setting-frame">
      <div class="form-panel">
        <div class="cell left key">
          <label><@label "022"/></label>
          <input type="text" maxlength="100"/>
          <p class="error"></p>
        </div>
        <div class="cell right site">
          <label><@label "024"/></label>
          <@x_selectDomainSites />
          <p class="error"></p>
        </div>
      </div>
    </div>

    <div id="add-file-frame">
      <div class="form-panel">
        <div class="cell left folder">
          <label><@label "040"/></label>
          <input type="text" maxlength="100"/>
          <p class="error"></p>
        </div>
        <div class="cell right site">
          <label><@label "024"/></label>
          <@x_selectDomainSites />
          <p class="error"></p>
        </div>
      </div>
      <@htm.form id="add-file-upload-form" mode="upload">
        <input name="dat1" type="file"/>
      </@>
    </div>

  </@>

<#else>

  <@label "x.nodom"/>

</#if>
