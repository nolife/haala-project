<#import "/web/common.ftl" as c >
<#import "/web/common-htm.ftl" as htm >
<#include "/web/macros.ftl:"+currentSite>

<#macro x_domainSelect>
  <select>
    <option value=""></option>
    <@h.listUserDomains ; x >
      <option value="${x.id()}" <#if x.domain() == (domain.domain())!> selected="true" </#if> > ${x.domain()} </option>
    </@>
  </select>
</#macro>


<#if domain?? >

  <@h.domainDefaultSite var="dsite" domain=domain mode="key" />
  <@htm.form id="page-form" mode="upload">
    <div class="form-panel">
      <div class="cell select-domain">
        <label><@label "002"/></label>
        <@x_domainSelect />
      </div>
      <div class="cell keywords">
        <label><@label "005"/></label>
        <textarea><@h.label key="meta.key" site="${dsite}" bydefault=""/></textarea>
        <p class="error"></p>
      </div>
      <div class="cell inside-head">
        <label><@label "006"/></label>
        <textarea><@h.label key="site.ihead" site="${dsite}" bydefault=""/></textarea>
        <p class="error"></p>
      </div>
      <div class="cell before-body">
        <label><@label "007"/></label>
        <textarea><@h.label key="site.bbody" site="${dsite}" bydefault=""/></textarea>
        <p class="error"></p>
      </div>
      <div class="tabs site-tabs">
        <@domainInputSiteTabs domain />
        <@h.listDomainInputSites domain=domain ; site >
          <div id="site-tab-${site}">
            <div class="cell website-title">
              <label><@label "004"/></label>
              <input type="text" maxlength="100" value="<@h.label key= "site.ttl" site="."+site bydefault=""/>" />
              <p class="error"></p>
            </div>
          </div>
        </@>
      </div>
      <h3 class="cell"><@label "008"/></h3>
      <div class="cell fav-icon">
        <img src="" />
      </div>
      <div class="file-upload">
        <div class="cell">
          <label><@label "011"/></label>
          <input name="dat1" type="file" size="20"/>
          <span data-clear="dat1" title="<@label "012"/>" ></span>
        </div>
      </div>
      <div class="cell submit">
        <button><@label "btn.save"/></button>
      </div>
    </div>
  </@>

  <@htm.nodiv "context-navigation-content">
    <a class="edit-site" href="#" title="<@label "010"/>" ><@label "009"/></a>
    <a class="structure" href="structure.htm?id=${domain.id()}" title="<@label "014"/>" ><@label "013"/></a>
    <a class="editor" href="editor/main.htm?id=${domain.id()}" title="<@label "016"/>" ><@label "015"/></a>
  </@>

  <@htm.nodiv "page-ivars">
    {
      domainId: "${domain.id()}", defSite: "<@h.domainDefaultSite domain=domain mode="key" />",
      domainInputSites: <@h.listDomainInputSites domain=domain output="json" />
    }
  </@>

<#else>

  <div class="form-panel">
    <div class="cell select-domain">
      <label><@label "002"/></label>
      <@x_domainSelect />
    </div>
    <div class="cell no-domain">
      <p><@label "003"/></p>
    </div>
  </div>

</#if>
