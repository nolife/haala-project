
/* js::import

   { path = web/haala;
     js = mere-cms-nocore, mere-cms-frames }

 */


(function($) {

  Haala.Misc.onDocReady(function() {
    initSelectWebpage();
    initNavigation();
    initMappingForm();

    initLabelList(function() { // 1
      initSettingList(function() { // 2
        initFileList(); // 3
      });
    });

  });


  var initSelectWebpage = function() {
    var ivars = Haala.Misc.evalJSON("#page-ivars");
    if (!ivars) return;

    $("#content .select-webpage select").change(function() {
      var pageIndex = $(this).val();
      if (!pageIndex) document.location = "structure.htm?id="+ivars.domainId;
      else document.location = "structure.htm?id="+ivars.domainId+"&pidx="+pageIndex;
    });
  };


  var initNavigation = function() {
    var ivars = Haala.Misc.evalJSON("#page-ivars");
    if (!ivars) return;

    $("#context-navigation").kdxMenu({
      items: [
        { element: "#context-navigation .add-webpage",
          gotoFn: function(/*json*/ item, /*json*/ model) {
            addWebpageFrame();
          }},
        { element: "#context-navigation .add-label",
          gotoFn: function(/*json*/ item, /*json*/ model) {
            addLabelFrame();
          }},
        { element: "#context-navigation .add-setting",
          gotoFn: function(/*json*/ item, /*json*/ model) {
            addSettingFrame();
          }},
        { element: "#context-navigation .add-file",
          gotoFn: function(/*json*/ item, /*json*/ model) {
            addFileFrame();
          }}
      ]
    });
  };


  var addOrEditMappingInput = function(/*str*/ wrapper) {
    var finp = function(key, vld) { return Haala.Util.panelFormInput(key, vld, { root: wrapper })};
    return [
      finp("pattern",  { require: true }),
      finp("handler",  { require: true }),
      finp("params"),
      finp("index",    { require: true }),
      finp("options"),
      finp("pageName", { require: true }),
      finp("mod"),
      finp("downtime"),
      finp("facade")
    ];
  };

  var selectedPageIndex = function() {
    return $("#content .select-webpage select").val();
  };

  var initMappingForm = function() {
    var ivars = Haala.Misc.evalJSON("#page-ivars");
    if (!ivars) return;

    var msgFn = function(/*string*/ msg, /*json*/ tokens) {
      return Haala.Misc.messageMapping("#page-messages", msg, tokens);
    };

    $("#page-form .submit button").button();

    var panel = $("#page-form").kdxPanel(
      $.extend(true, {}, Haala.KdxPanel.Integ.DefaultOptions, {
        input: addOrEditMappingInput("#page-form")
      }));

    Haala.Util.formSubmitPanel(panel, function(/*json*/ container) {
      var command = Haala.Misc.collectInput(container,
        ["pattern", "params", "index", "options", "pageName", "mod", "downtime", "facade", "handler"]);
      command.domainId = ivars.domainId;
      command.oindex = selectedPageIndex();
      editPageMapping(command);
    });

    $("#page-form .remove").click(function() {
      Haala.Messages.showYN(msgFn("webpage-delete"), "title-confirm", function() {
        var command = {};
        command.domainId = ivars.domainId;
        command.index = selectedPageIndex();
        removePageMapping(command);
      });
    });

    var editPageMapping = function(/*json*/ command) {
      Haala.AJAX.call("body", CmsF.editPageMapping, command,
          function() { document.location = document.location.href; return Haala.AJAX.DO_NOTHING; });
    };

    var removePageMapping = function(/*json*/ command) {
      Haala.AJAX.call("body", CmsF.removePageMapping, command,
          function() { document.location = "structure.htm?id="+ivars.domainId; return Haala.AJAX.DO_NOTHING; });
    };

  };



  var addWebpageFrame = function() {
    var ivars = Haala.Misc.evalJSON("#page-ivars");
    var msgFn = function(/*string*/ msg, /*json*/ tokens) {
      return Haala.Misc.messageMapping("#page-messages", msg, tokens);
    };

    var panel = $("#add-webpage-frame").kdxPanel(
        $.extend(true, {}, Haala.KdxPanel.Integ.DefaultOptions, {
          input: addOrEditMappingInput("#add-webpage-frame")
        }));

    var optinp = $("#add-webpage-frame .options input");
    if (!optinp.val()) optinp.val("ftl-page");

    var handler = $("#add-webpage-frame .handler input");
    if (!handler.val()) handler.val("dynamicCtrl");

    $("#add-webpage-frame").dialog({
      zIndex: 70, width: 900, height: 280, resizable: false,
      buttons: [
        { text: msgFn("button-ok"), click: function() {
          if (panel.validate()) {
            var container = panel.collect();
            var command = Haala.Misc.collectInput(container,
                ["pattern", "params", "index", "options", "pageName", "mod", "downtime", "facade", "handler"]);
            command.domainId = ivars.domainId;

            addPageMapping(command);
            $(this).dialog("close");
          } else {
            Haala.Util.errorToViewport({ root: "#add-webpage-frame" });
          }
        }},
        { text: msgFn("button-cancel"), click: function() {
          $(this).dialog("close");
        }}
      ],
      title: msgFn("add-webpage-title"),
      close: function() {
        panel.validate("reset");
      }
    });

    var addPageMapping = function(/*json*/ command) {
      Haala.AJAX.call("body", CmsF.addPageMapping, command,
          function() { document.location = "structure.htm?id="+ivars.domainId+"&pidx="+command.index; return Haala.AJAX.DO_NOTHING; });
    };

  };


  var initLabelList = function(/*fn*/ readyFn) {
    var ivars = Haala.Misc.evalJSON("#page-ivars");
    if (!ivars) return;
    if (!$("#label-list").length) return;

    var addContextMenu = function(/*element*/ e, /*json*/ vo) {
      e.contextMenu({ menu: "page-menu-textframe" },
          function(action, el, pos) {
            if (action == "edit-normal") richTextFrame(vo);
            if (action == "edit-rich") richTextFrame(vo, "rf");
            if (action == "edit-plain") richTextFrame(vo, "", true);
          });
    };

    var listModel = {
      sort: {
        items: [
          { element: $("#label-list .head .key"), field: "key" }
        ]
      },
      loadItemsFn: function(/*json*/ model, /*fn*/ callback) {
        Haala.AquaList.Integ.loadItemsFn(CmsF.searchLabels, { target: 2, domainId: ivars.domainId }, model, callback);
      },
      rowElementFn: function(/*json*/ vo, /*int*/ position, /*json*/ model) {
        if (vo) {
          var e = $("<div class='data'>" +
                    "  <a href='#'>" +
                    "    <p class='column-1'>"+vo.key+"</p>" +
                    "    <p class='column-2'>"+vo.value+"</p>" +
                    "    <p class='column-3'>"+vo.site+"</p>" +
                    "  </a>" +
                    "</div>");
          e.aclick(function() { richTextFrame(vo); });
          addContextMenu(e, vo);
          return e;
        } else {
          return $("<div class='empty'></div>");
        }
      }
    };

    $("#label-list .filter input").val(selectedPageIndex());
    Haala.AJAX.call("body", CmsF.filterLabels, { target: 2, params: [selectedPageIndex()] }, function() { //reset filter then proceed
      var list = $("#label-list .rows").aquaList(
          $.extend(true, {},
              Haala.AquaList.Integ.DefaultOptions,
              Haala.AquaList.Integ.parseState($("#label-list").attr("data-state")),
              listModel));
      list.addListener("rendered", Haala.Code.stretch);
      Haala.AquaList.Integ.addPageSpread(list,
        $("#label-list .page-spread").pageSpread(Haala.PageSpread.Integ.DefaultOptions));

      $("#label-list .filter .on").aclick(function() {
        var command = { target: 2, params: [$("#label-list .filter input").val()] };
        Haala.AJAX.call("body", CmsF.filterLabels, command,
            function(data) { list.page(1); });
      });
      $("#label-list .filter .off").aclick(function() {
        var command = { target: 2, params: [] };
        Haala.AJAX.call("body", CmsF.filterLabels, command,
            function(data) {
              $("#label-list .filter input").val("");
              list.page(1);
            });
      });

      if (readyFn) setTimeout(readyFn, 10);
    });

  };

  var refreshLabelList = function() {
    $("#label-list .rows").aquaList().page();
  };

  var refreshSettingList = function() {
    $("#setting-list .rows").aquaList().page();
  };

  var refreshFileList = function() {
    $("#file-list .rows").aquaList().page();
  };

  var richTextFrame = function(/*json*/ vo, /*str*/ opts, /*bool*/ plainText) {
    new Haala.MereCms.TextFrame($.extend(true, {},
        { state: { opts: opts, plainText: plainText, enableDelete: true, itemId: vo.key },
          message: {
            mappingFn: function(/*string*/ msg, /*json*/ tokens) {
              return Haala.Misc.messageMapping("#page-messages", msg, tokens);
            }},
          loadFn: function(/*json*/ cmd, /*json*/ model, /*fn*/ callback, /*fn*/ fail) {
            cmd = { labelId: vo.id };
            Haala.AJAX.call("body", CmsF.loadLabel, cmd, callback, fail);
          },
          saveFn: function(/*json*/ cmd, /*json*/ model, /*fn*/ callback, /*fn*/ fail) {
            cmd = { labelId: vo.id, key: vo.key, site: vo.site, value: model.textElement.val() };
            Haala.AJAX.call("body", CmsF.editLabel, cmd, function() { refreshLabelList(); callback(); }, fail);
          },
          deleteFn: function(/*json*/ cmd, /*json*/ model, /*fn*/ callback, /*fn*/ fail) {
            cmd = { labelId: vo.id };
            Haala.AJAX.call("body", CmsF.removeLabel, cmd, function() { refreshLabelList(); callback(); }, fail);
          }
        }
    ));
  };

  var richSettingFrame = function(/*json*/ vo) {
    new Haala.MereCms.TextFrame($.extend(true, {},
        { state: { plainText: true, enableDelete: true, itemId: vo.key },
          message: {
            mappingFn: function(/*string*/ msg, /*json*/ tokens) {
              return Haala.Misc.messageMapping("#page-messages", msg, tokens);
            }},
          loadFn: function(/*json*/ cmd, /*json*/ model, /*fn*/ callback, /*fn*/ fail) {
            cmd = { settingId: vo.id };
            Haala.AJAX.call("body", CmsF.loadSetting, cmd, callback, fail);
          },
          saveFn: function(/*json*/ cmd, /*json*/ model, /*fn*/ callback, /*fn*/ fail) {
            cmd = { settingId: vo.id, key: vo.key, site: vo.site, value: model.textElement.val() };
            Haala.AJAX.call("body", CmsF.editSetting, cmd, function() { refreshSettingList(); callback(); }, fail);
          },
          deleteFn: function(/*json*/ cmd, /*json*/ model, /*fn*/ callback, /*fn*/ fail) {
            cmd = { settingId: vo.id };
            Haala.AJAX.call("body", CmsF.removeSetting, cmd, function() { refreshSettingList(); callback(); }, fail);
          }
        }
    ));
  };

  var textFileFrame = function(/*json*/ vo, /*bool*/ plainText) {
    new Haala.MereCms.TextFrame($.extend(true, {},
        { state: { opts: "rf", plainText: plainText, enableDelete: true, itemId: vo.path },
          message: {
            mappingFn: function(/*string*/ msg, /*json*/ tokens) {
              return Haala.Misc.messageMapping("#page-messages", msg, tokens);
            }},
          loadFn: function(/*json*/ cmd, /*json*/ model, /*fn*/ callback, /*fn*/ fail) {
            cmd = { fileId: vo.id };
            Haala.AJAX.call("body", CmsF.loadTextFile, cmd, callback, fail);
          },
          saveFn: function(/*json*/ cmd, /*json*/ model, /*fn*/ callback, /*fn*/ fail) {
            cmd = { path: vo.path, site: vo.site, value: model.textElement.val() };
            Haala.AJAX.call("body", CmsF.editTextFile, cmd, function() { refreshFileList(); callback(); }, fail);
          },
          deleteFn: function(/*json*/ cmd, /*json*/ model, /*fn*/ callback, /*fn*/ fail) {
            cmd = { fileId: vo.id };
            Haala.AJAX.call("body", CmsF.removeFile, cmd, function() { refreshFileList(); callback(); }, fail);
          }
        }
    ));
  };

  var fileFrame = function(/*json*/ vo) {
    new Haala.MereCms.FileFrame($.extend(true, {},
        { state: { opts: "", enableDelete: true, itemId: vo.path },
          message: {
            mappingFn: function(/*string*/ msg, /*json*/ tokens) {
              return Haala.Misc.messageMapping("#page-messages", msg, tokens);
            }},
          loadFn: function(/*json*/ cmd, /*json*/ model, /*fn*/ callback, /*fn*/ fail) {
            cmd = { fileId: vo.id };
            Haala.AJAX.call("body", CmsF.loadFile2, cmd, callback, fail);
          },
          saveFn: function(/*json*/ cmd, /*json*/ model, /*fn*/ callback, /*fn*/ fail) {
            cmd = { path: vo.path, site: vo.site };
            var form = $("#cms-file-upload-form").uploadForm(
                $.extend(true, {}, Haala.UploadForm.Integ.DefaultOptions));
            form.addListener("success", function() {
              Haala.AJAX.call("body", CmsF.editFile2, cmd, function() { refreshFileList(); callback(); }, fail);
            });
            form.upload();
          },
          deleteFn: function(/*json*/ cmd, /*json*/ model, /*fn*/ callback, /*fn*/ fail) {
            cmd = { fileId: vo.id };
            Haala.AJAX.call("body", CmsF.removeFile , cmd, function() { refreshFileList(); callback(); }, fail);
          }
        }
    ));
  };

  var pictureFrame = function(/*json*/ vo) {
    new Haala.MereCms.PictureFrame($.extend(true, {},
        { state: { opts: "", enableDelete: true, itemId: vo.path },
          message: {
            mappingFn: function(/*string*/ msg, /*json*/ tokens) {
              return Haala.Misc.messageMapping("#page-messages", msg, tokens);
            }},
          loadFn: function(/*json*/ cmd, /*json*/ model, /*fn*/ callback, /*fn*/ fail) {
            cmd = { fileId: vo.id };
            Haala.AJAX.call("body", CmsF.loadPicture, cmd, callback, fail);
          },
          saveFn: function(/*json*/ cmd, /*json*/ model, /*fn*/ callback, /*fn*/ fail) {
            cmd = { path: vo.path, site: vo.site };
            var form = $("#cms-picture-upload-form").uploadForm(
                $.extend(true, {}, Haala.UploadForm.Integ.DefaultOptions));
            form.addListener("success", function() {
              Haala.AJAX.call("body", CmsF.editFile2, cmd, function() { refreshFileList(); callback(); }, fail);
            });
            form.upload();
          },
          deleteFn: function(/*json*/ cmd, /*json*/ model, /*fn*/ callback, /*fn*/ fail) {
            cmd = { fileId: vo.id };
            Haala.AJAX.call("body", CmsF.removeFile , cmd, function() { refreshFileList(); callback(); }, fail);
          }
        }
    ));
  };


  var addLabelFrame = function() {
    var msgFn = function(/*string*/ msg, /*json*/ tokens) {
      return Haala.Misc.messageMapping("#page-messages", msg, tokens);
    };

    var finp = function(key, vld) { return Haala.Util.panelFormInput(key, vld, { root: "#add-label-frame" })};
    var panel = $("#add-label-frame").kdxPanel(
        $.extend(true, {}, Haala.KdxPanel.Integ.DefaultOptions, {
          input: [
            finp("key",  { require: true }),
            finp("site", { require: true })
          ]
        }));

    $("#add-label-frame").dialog({
      zIndex: 70, width: 370, height: 220, resizable: false,
      buttons: [
        { text: msgFn("button-ok"), click: function() {
          if (panel.validate()) {
            var command = Haala.Misc.collectInput(panel.collect(), ["key", "value", "site"]);
            addLabel(command);
            $(this).dialog("close");
          } else {
            Haala.Util.errorToViewport({ root: "#add-label-frame" });
          }
        }},
        { text: msgFn("button-cancel"), click: function() {
          $(this).dialog("close");
        }}
      ],
      title: msgFn("add-label-title"),
      close: function() {
        panel.validate("reset");
        $("#add-label-frame input").val("");
      }
    });

    var addLabel = function(/*json*/ command) {
      Haala.AJAX.call("body", CmsF.addLabel, command,
          function(data) {
            refreshLabelList();
            richTextFrame(data.result);
          });
    };

  };


  var addSettingFrame = function() {
    var msgFn = function(/*string*/ msg, /*json*/ tokens) {
      return Haala.Misc.messageMapping("#page-messages", msg, tokens);
    };

    var finp = function(key, vld) { return Haala.Util.panelFormInput(key, vld, { root: "#add-setting-frame" })};
    var panel = $("#add-setting-frame").kdxPanel(
        $.extend(true, {}, Haala.KdxPanel.Integ.DefaultOptions, {
          input: [
            finp("key",  { require: true }),
            finp("site", { require: true })
          ]
        }));

    $("#add-setting-frame").dialog({
      zIndex: 70, width: 370, height: 220, resizable: false,
      buttons: [
        { text: msgFn("button-ok"), click: function() {
          if (panel.validate()) {
            var command = Haala.Misc.collectInput(panel.collect(), ["key", "value", "site"]);
            addSetting(command);
            $(this).dialog("close");
          } else {
            Haala.Util.errorToViewport({ root: "#add-setting-frame" });
          }
        }},
        { text: msgFn("button-cancel"), click: function() {
          $(this).dialog("close");
        }}
      ],
      title: msgFn("add-setting-title"),
      close: function() {
        panel.validate("reset");
        $("#add-setting-frame input").val("");
      }
    });

    var addSetting = function(/*json*/ command) {
      Haala.AJAX.call("body", CmsF.addSetting, command,
          function(data) {
            refreshSettingList();
            richSettingFrame(data.result);
          });
    };

  };




  var initSettingList = function(/*fn*/ readyFn) {
    var ivars = Haala.Misc.evalJSON("#page-ivars");
    if (!ivars) return;
    if (!$("#setting-list").length) return;

    var listModel = {
      sort: {
        items: [
          { element: $("#setting-list .head .key"), field: "key" }
        ]
      },
      loadItemsFn: function(/*json*/ model, /*fn*/ callback) {
        Haala.AquaList.Integ.loadItemsFn(CmsF.searchSettings, { target: 2, domainId: ivars.domainId }, model, callback);
      },
      rowElementFn: function(/*json*/ vo, /*int*/ position, /*json*/ model) {
        if (vo) {
          var e = $("<div class='data'>" +
              "  <a href='#'>" +
              "    <p class='column-1'>"+vo.key+"</p>" +
              "    <p class='column-2'>"+vo.value+"</p>" +
              "    <p class='column-3'>"+vo.site+"</p>" +
              "  </a>" +
              "</div>");
          e.aclick(function() { richSettingFrame(vo); });
          return e;
        } else {
          return $("<div class='empty'></div>");
        }
      }
    };

    $("#setting-list .filter input.key").val(selectedPageIndex());
    Haala.AJAX.call("body", CmsF.filterSettings, { target: 2, params: [selectedPageIndex()] }, function() { //reset filter then proceed
      var list = $("#setting-list .rows").aquaList(
          $.extend(true, {},
              Haala.AquaList.Integ.DefaultOptions,
              Haala.AquaList.Integ.parseState($("#setting-list").attr("data-state")),
              listModel));
      list.addListener("rendered", Haala.Code.stretch);
      Haala.AquaList.Integ.addPageSpread(list,
        $("#setting-list .page-spread").pageSpread(Haala.PageSpread.Integ.DefaultOptions));

      $("#setting-list .filter .on").aclick(function() {
        var command = {
          target: 2,
          params: [$("#setting-list .filter input.key").val(), $("#setting-list .filter input.value").val()]
        };
        Haala.AJAX.call("body", CmsF.filterSettings, command,
            function(data) { list.page(1); });
      });
      $("#setting-list .filter .off").aclick(function() {
        var command = { target: 2, params: [] };
        Haala.AJAX.call("body", CmsF.filterSettings, command,
            function(data) {
              $("#setting-list .filter input").val("");
              list.page(1);
            });
      });

      if (readyFn) setTimeout(readyFn, 10);
    });

  };




  var initFileList = function(/*fn*/ readyFn) {
    var ivars = Haala.Misc.evalJSON("#page-ivars");
    if (!ivars) return;
    if (!$("#file-list").length) return;

    var addContextMenu = function(/*element*/ e, /*json*/ vo) {
      e.contextMenu({ menu: "page-menu-fileframe" },
          function(action, el, pos) {
            if (action == "edit-file") fileFrame(vo);
            if (action == "edit-picture") pictureFrame(vo);
            if (action == "edit-text") textFileFrame(vo);
            if (action == "edit-plain") textFileFrame(vo, true);
          });
    };

    var listModel = {
      sort: {
        items: [
          { element: $("#file-list .head .folder"), field: "fld" }
        ]
      },
      loadItemsFn: function(/*json*/ model, /*fn*/ callback) {
        Haala.AquaList.Integ.loadItemsFn(CmsF.searchFiles, { target: 2, domainId: ivars.domainId }, model, callback);
      },
      rowElementFn: function(/*json*/ vo, /*int*/ position, /*json*/ model) {
        if (vo) {
          var e = $("<div class='data'>" +
              "  <a href='#'>" +
              "    <p class='column-1'>"+vo.folder+"</p>" +
              "    <p class='column-2'>"+vo.name+"</p>" +
              "    <p class='column-3'>"+vo.site+"</p>" +
              "  </a>" +
              "</div>");
          e.aclick(function() { fileFrame(vo); });
          addContextMenu(e, vo);
          return e;
        } else {
          return $("<div class='empty'></div>");
        }
      }
    };

    $("#file-list .filter input").val(selectedPageIndex());
    Haala.AJAX.call("body", CmsF.filterFiles, { target: 2, params: [selectedPageIndex()] }, function() { //reset filter then proceed
      var list = $("#file-list .rows").aquaList(
          $.extend(true, {},
              Haala.AquaList.Integ.DefaultOptions,
              Haala.AquaList.Integ.parseState($("#file-list").attr("data-state")),
              listModel));
      list.addListener("rendered", Haala.Code.stretch);
      Haala.AquaList.Integ.addPageSpread(list,
        $("#file-list .page-spread").pageSpread(Haala.PageSpread.Integ.DefaultOptions));

      $("#file-list .filter .on").aclick(function() {
        var command = { target: 2, params: [$("#file-list .filter input").val()] };
        Haala.AJAX.call("body", CmsF.filterFiles, command,
            function(data) { list.page(1); });
      });
      $("#file-list .filter .off").aclick(function() {
        var command = { target: 2, params: [] };
        Haala.AJAX.call("body", CmsF.filterFiles, command,
            function(data) {
              $("#file-list .filter input").val("");
              list.page(1);
            });
      });

      if (readyFn) setTimeout(readyFn, 10);
    });

  };


  var addFileFrame = function() {
    var msgFn = function(/*string*/ msg, /*json*/ tokens) {
      return Haala.Misc.messageMapping("#page-messages", msg, tokens);
    };

    var finp = function(key, vld) { return Haala.Util.panelFormInput(key, vld, { root: "#add-file-frame" })};
    var panel = $("#add-file-frame").kdxPanel(
        $.extend(true, {}, Haala.KdxPanel.Integ.DefaultOptions, {
          input: [
            finp("folder", { require: true }),
            finp("site",   { require: true })
          ]
        }));

    $("#add-file-frame").dialog({
      zIndex: 70, width: 370, height: 240, resizable: false,
      buttons: [
        { text: msgFn("upload"), click: function() {
          if (panel.validate()) {
            var command = Haala.Misc.collectInput(panel.collect(), ["folder", "site"]);
            addFile(command);
            $(this).dialog("close");
          } else {
            Haala.Util.errorToViewport({ root: "#add-file-frame" });
          }
        }},
        { text: msgFn("button-cancel"), click: function() {
          $(this).dialog("close");
        }}
      ],
      title: msgFn("add-file-title"),
      close: function() {
        panel.validate("reset");
        $("#add-file-frame input").val("");
      }
    });

    var addFile = function(/*json*/ command) {
      var form = $("#add-file-upload-form").uploadForm(
          $.extend(true, {}, Haala.UploadForm.Integ.DefaultOptions, {
            uploadSuccessFn: function(/*json*/ model) {
              Haala.AJAX.call("body", CmsF.addFile, command, refreshFileList);
            }
          }));
      form.upload();
    };

  };

})(jQuery);

