
(function($) {

  Haala.Misc.onDocReady(function() {
    initSelectDomain();
    initNavigation();
    initCustomizeForm();
  });



  var initSelectDomain = function() {
    $("#content .select-domain select").change(function() {
      var domainId = $(this).val();
      if (!domainId) document.location = "web.htm";
      else document.location = "web.htm?id="+domainId;
    });
  };

  var initNavigation = function() {
    var ivars = Haala.Misc.evalJSON("#page-ivars");
    if (!ivars) return;

    $("#context-navigation").kdxMenu({
      items: [
        { element: "#context-navigation .edit-site",
            gotoFn: function(/*json*/ item, /*json*/ model) {
              Haala.AJAX.call("body", CmsF.switchOn, { domainId: ivars.domainId },
                function(data) {
                  if (data.result) window.open(data.result);
                  else document.location = "/";
                });
            }}
      ]
    });
  };

  var initCustomizeForm = function() {
    var ivars = Haala.Misc.evalJSON("#page-ivars");
    if (!ivars) return;

    $("#page-form .site-tabs").tabs();
    $("#page-form .submit button").button();

    /* Using domain input sites instead of root input sites as the changes should be saved only for this domain.
       eg. domain owner wants to change its default title
     */
    var sites = ivars.domainInputSites;
    var finp = Haala.Util.panelFormInput;
    var input = [
      finp("keywords"),
      finp("inside-head"),
      finp("before-body")
    ];
    $.each(sites, function(n, site) {
      input.push({ element: "#site-tab-"+site+" .website-title input", key: "title_"+site});
    });
    var panel = $("#page-form").kdxPanel(
      $.extend(true, {}, Haala.KdxPanel.Integ.DefaultOptions, {
        input: input
      }));

    var refreshFavicon = function() {
      var img = $("#page-form .fav-icon img");
      img.attr("src", "/bin.htm?path=web/etc/favicon.ico&site="+ivars.defSite+"&cms=1&nc=1&salt="+Math.random());
    };
    refreshFavicon();

    var submitHandler = function() {
      if (panel.validate()) {
        var container = panel.collect();
        var command = Haala.Misc.collectInput(container, ["keywords", "insideHead", "beforeBody"]);
        command.info = Haala.Misc.collectSiteableInput(container, ["title"], null, sites);
        command.domainId = ivars.domainId;

        editDomain(command);
      } else {
        Haala.Util.errorToViewport();
      }
      Haala.Code.stretch();
    };

    var uploadForm = initEmbeddedUploadForm(submitHandler);
    $("#page-form .submit button").click(function(event) {
      event.preventDefault();
      if (!uploadForm.hasFiles()) submitHandler(); //call submit handler without upload
      else {
        if (panel.validate()) uploadForm.upload(); //upload then call submit handler
        else Haala.Util.errorToViewport();
      }
    });

    var editDomain = function(/*json*/ command) {
      Haala.AJAX.call("body", CmsF.editDomain, command,
        function() { document.location = document.location.href; return Haala.AJAX.DO_NOTHING; });
    };
  };

  var initEmbeddedUploadForm = function(/*fn*/ successFn) {
    var ivars = Haala.Misc.evalJSON("#page-ivars");

    var form = $("#page-form").uploadForm(
      $.extend(true, {}, Haala.UploadForm.Integ.DefaultOptions, {
        uploadSuccessFn: function(/*json*/ model) {
          Haala.AJAX.call(null, CmsF.editFile, { path: "web/etc/favicon.ico", site: ivars.defSite },
            function() { successFn(); }); //using body will prevent successFn from making AJAX call
        }
      }));
    return form;
  };

})(jQuery);

