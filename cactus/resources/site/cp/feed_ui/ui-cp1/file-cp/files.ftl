<#import "/web/common.ftl" as c >
<#import "/web/common-htm.ftl" as htm >
<#include "/web/macros.ftl:"+currentSite >

<div id="file-list" class="item-list" data-state="${perPage},${searchState!}">
  <div class="filter">
    <@h.searchFilterParam var="tmpval" index=1 />
    <input type="text" value="${tmpval!}">
    <span class="on" title="<@label "005"/>" ></span>
    <span class="off" title="<@label "006"/>" ></span>
  </div>
  <div class="head">
    <a href="#" class="column-1 folder"><@label "002"/><span class="arrow"></span></a>
    <p class="column-2 name"><@label "003"/></p>
    <p class="column-3 site"><@label "004"/></p>
  </div>
  <div class="rows"></div>
  <div class="foot">
    <div class="page-spread"></div>
  </div>
</div>

<@htm.nodiv "context-navigation-content">
  <a class="add" href="#" title="<@label "014"/>"><@label "013"/></a>
</@>

<@htm.nodiv "page-messages">
  <div data-key="add-file-title"><@label "007"/></div>
  <div data-key="button-cancel"><@label "btn.cancel"/></div>
  <div data-key="file-frame-delete"><@label "cms.file-frame-delete"/></div>
  <div data-key="file-frame"><@label "cms.file-frame"/></div>
  <div data-key="file-open"><@label "cms.file-open"/></div>
  <div data-key="cancel"><@label "cms.cancel"/></div>
  <div data-key="save"><@label "cms.save"/></div>
  <div data-key="delete"><@label "cms.delete"/></div>
  <div data-key="upload"><@label "cms.upload"/></div>
  <div data-key="dialog-confirm-title"><@label "cms.dialog-confirm-title"/></div>
</@>


<@htm.nodiv "page-templates">

  <div id="add-file-frame">
    <div class="form-panel">
      <div class="cell left folder">
        <label><@label "002"/></label>
        <input type="text" maxlength="100"/>
        <p class="error"></p>
      </div>
      <div class="cell right site">
        <label><@label "004"/></label>
        <input type="text" maxlength="100"/>
        <p class="error"></p>
      </div>
    </div>
    <@htm.form id="add-file-upload-form" mode="upload">
      <input name="dat1" type="file"/>
    </@>
  </div>

</@>
