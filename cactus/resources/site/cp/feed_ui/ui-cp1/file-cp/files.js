
/* js::import

 { path = web/haala;
 js = mere-cms-nocore, mere-cms-frames }

 */


(function($) {

  Haala.Misc.onDocReady(function() {
    initNavigation();
    initFileList();
  });


  var initNavigation = function() {
    $("#context-navigation").kdxMenu({
      items: [
        { element: "#context-navigation .add",
          gotoFn: function(/*json*/ item, /*json*/ model) {
            addFileFrame();
          }}
      ]
    });
  };


  var initFileList = function() {
    var listModel = {
      sort: {
        items: [
          { element: $("#file-list .head .folder"), field: "fld" }
        ]
      },
      loadItemsFn: function(/*json*/ model, /*fn*/ callback) {
        Haala.AquaList.Integ.loadItemsFn(CmsF.searchFiles, { target: 1 }, model, callback);
      },
      rowElementFn: function(/*json*/ vo, /*int*/ position, /*json*/ model) {
        return vo ?
          $("<div class='data'>" +
            "  <a href='#'>" +
            "    <p class='column-1'>"+vo.folder+"</p>" +
            "    <p class='column-2'>"+vo.name+"</p>" +
            "    <p class='column-3'>"+vo.site+"</p>" +
            "  </a>" +
            "</div>").aclick(function() { fileFrame(vo); }) :
          $("<div class='empty'></div>");
      }
    };
    var list = $("#file-list .rows").aquaList(
      $.extend(true, {},
        Haala.AquaList.Integ.DefaultOptions,
        Haala.AquaList.Integ.parseState($("#file-list").attr("data-state")),
        listModel));
    list.setPageSpread($("#file-list .page-spread").pageSpread(Haala.PageSpread.Integ.DefaultOptions));
    list.addRenderedFn(Haala.Code.stretch);

    $("#file-list .filter .on").aclick(function() {
      var command = { target: 1, params: [$("#file-list .filter input").val()] };
      Haala.AJAX.call("body", CmsF.filterFiles, command,
        function(data) { list.page(1); });
    });
    $("#file-list .filter .off").aclick(function() {
      var command = { target: 1, params: [] };
      Haala.AJAX.call("body", CmsF.filterFiles, command,
        function(data) {
          $("#file-list .filter input").val("");
          list.page(1);
        });
    });

  };

  var refreshFileList = function() {
    $("#file-list .rows").aquaList().page();
  };

  var fileFrame = function(/*json*/ vo) {
    new Haala.MereCms.FileFrame($.extend(true, {},
      { state: { opts: "", enableDelete: true, itemId: vo.path },
        message: {
          mappingFn: function(/*string*/ msg, /*json*/ tokens) {
            return Haala.Misc.messageMapping("#page-messages", msg, tokens);
          }},
        loadFn: function(/*json*/ cmd, /*json*/ model, /*fn*/ callback, /*fn*/ fail) {
          cmd = { fileId: vo.id };
          Haala.AJAX.call("body", CmsF.loadFile2, cmd, callback, fail);
        },
        saveFn: function(/*json*/ cmd, /*json*/ model, /*fn*/ callback, /*fn*/ fail) {
          cmd = { path: vo.path, site: vo.site };
          var form = $("#cms-file-upload-form").uploadForm(
            $.extend(true, {}, Haala.UploadForm.Integ.DefaultOptions));
          form.addListener("success", function() {
            Haala.AJAX.call("body", CmsF.editFile2, cmd, function() { refreshFileList(); callback(); }, fail);
          });
          form.upload();
        },
        deleteFn: function(/*json*/ cmd, /*json*/ model, /*fn*/ callback, /*fn*/ fail) {
          cmd = { fileId: vo.id };
          Haala.AJAX.call("body", CmsF.removeFile , cmd, function() { refreshFileList(); callback(); }, fail);
        }
      }
    ));
  };

  var addFileFrame = function() {
    var msgFn = function(/*string*/ msg, /*json*/ tokens) {
      return Haala.Misc.messageMapping("#page-messages", msg, tokens);
    };

    var finp = function(key, vld) { return Haala.Util.panelFormInput(key, vld, { root: "#add-file-frame" })};
    var panel = $("#add-file-frame").kdxPanel(
      $.extend(true, {}, Haala.KdxPanel.Integ.DefaultOptions, {
        input: [
          finp("folder", { require: true }),
          finp("site",   { require: true })
        ]
      }));

    $("#add-file-frame").dialog({
      zIndex: 70, width: 370, height: 240, resizable: false,
      buttons: [
        { text: msgFn("upload"), click: function() {
          if (panel.validate()) {
            var command = Haala.Misc.collectInput(panel.collect(), ["folder", "site"]);
            addFile(command);
            $(this).dialog("close");
          } else {
            Haala.Util.errorToViewport({ root: "#add-setting-frame" });
          }
        }},
        { text: msgFn("button-cancel"), click: function() {
          $(this).dialog("close");
        }}
      ],
      title: msgFn("add-file-title"),
      close: function() {
        panel.validate("reset");
        $("#add-setting-frame input").val("");
      }
    });

    var addFile = function(/*json*/ command) {
      var form = $("#add-file-upload-form").uploadForm(
        $.extend(true, {}, Haala.UploadForm.Integ.DefaultOptions, {
          uploadSuccessFn: function(/*json*/ model) {
            Haala.AJAX.call("body", CmsF.addFile, command, refreshFileList);
          }
        }));
      form.upload();
    };

  };

})(jQuery);

