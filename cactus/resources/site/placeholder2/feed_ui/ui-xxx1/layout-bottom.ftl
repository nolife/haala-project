<#import "/web/common.ftl" as c >
<#import "/web/common-htm.ftl" as htm >

<aside>
  <section id="news">
    <h3>Latest news</h3>
    <ul>
      <li><a href="#">Kitchen Fitters London</a> 08/04/2014</li>
      <li><a href="#">Brickwork and Pointing in Maida Vale</a> 28/07/2013</li>
      <li><a href="#">Brick Facework in Central London</a> 14/06/2013</li>
      <li><a href="#">Facing Brickwork in Central London</a> 12/06/2013</li>
      <li><a href="#">Residential Building in Wimbledon</a> 26/05/2013</li>
    </ul>
  </section>

  <section id="areas">
    <h3>Areas we cover in London</h3>
    <p>
      Central London, South London, West London, North London, East London, Greater London, Southwark,
      Camberwell, Brixton, Battersea, Kennington, Clapham, Dulwich, Norwood, Streatham, Tooting, Wandsworth,
      Roehampton, Putney, Fulham, Kensington, Chelsea, Brompton, City of Westminster, Hammersmith, Paddington,
      Merylebone, Hampstead, Finchley, Holborn, Finsbury, Islington, Holloway, Stoke Newington, Hackney,
      Shoreditch, City of London, Bethnal Green, Stepney, Poplar, Greenwich, Charlton, Blackheath, Lee,
      Crofton Park, Lewisham, Hither Green, Catford, Sydenham, Grove Park, New, Eltham, Shooters Hill,
      Abbey Wood, Plumstead
    </p>
  </section>
</aside>

<footer>
  <p>Copyright &copy; 2015 <span>Altana Building Company Ltd</span>. All Rights Reserved.</p>
  <@htm.copyright copyright="n" vendor="y"/>
</footer>
