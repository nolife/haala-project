
var Haala = Haala || {};

(function($) {

  Haala.Code = {

    init: function() {

      if (Haala.MereCms) Haala.MereCms.Integ.init();
      Haala.Misc.tooltips();

      $("button.history-back").button().click(function() {
        history.back();
      });

    }
  };

})(jQuery);
