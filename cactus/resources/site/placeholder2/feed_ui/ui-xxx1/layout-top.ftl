<#import "/web/common.ftl" as c >

<header>
  <div class="logo"><a href="/"></a></div>
  <div class="info">
    <p>Phone <span>020 8422 3333</span></p>
    <p>Mobile <span>079 6242 4206</span></p>
  </div>
</header>

<nav id="main-menu">
  <ul>
    <li><a href="/">Home</a></li>
    <li><a href="services.htm">Services</a></li>
    <li><a href="contact.htm">Contact</a></li>
  </ul>
</nav>

<@h.onPage index="00018">
  <aside id="banner"></aside>
  <section id="home-about">
    <h1>Altana Building Company Ltd</h1>
    <p>The structural & specialist construction, property development & maintenance, quality building services provider.</p>
  </section>
</@>

<nav id="column-menu">
  <section>
    <h3>Our specialities</h3>
    <ul>
      <li><a href="services.htm#services-design">Design & Build</a></li>
      <li><a href="services.htm#services-brickwork">Brickwork & Masonry</a></li>
      <li><a href="services.htm#services-carpentry">Carpentry & Joinery</a></li>
      <li><a href="services.htm#services-kitchen">Kitchen Fitting</a></li>
    </ul>
  </section>

  <section>
    <h3>Project Categories</h3>
    <ul>
      <li><a href="services.htm#services-fitout">Fit-out</a></li>
      <li><a href="services.htm#services-extension">House Extension</a></li>
      <li><a href="services.htm#services-newbuild">New Build</a></li>
      <li><a href="services.htm#services-refurbishment">Refurbishment</a></li>
      <li><a href="services.htm#services-renovation">Renovation</a></li>
    </ul>
  </section>
</nav>
