<#import "/web/common.ftl" as c >

<section>
  <p>
    Welcome and thank you for visiting our web site. We do our best to inform you and keep
    updated about the services we provide to our clients . Altana Building Company Ltd is
    operating as the structural & specialist construction, property development & maintenance,
    quality building services supplier for London and surrounding areas, such as Essex, Kent,
    Sussex, Surrey, Buckinghamshire and Hertfordshire.
  </p>
</section>

<section>
  <h2>Commitment</h2>
  <p>
    In our company we are committed to provide wide range of quality building services for construction
    industry, as well as for private and commercial sectors at no compromise prices for the quality of
    works that we undertake.  We aim to provide our customers with best attention, care and knowledge
    together with the competitiveness and reliability everyone deserves when it comes to small or medium
    building projects or to large construction sites. Our experienced personnel is highly skilled and can
    show adequate expertise in their fields, so we can guarantee that all carried operations are in
    highest standard. Our experience helps us fit in our schedules for satisfaction and comfort of our
    clients through the whole building process.
  </p>
</section>

<section>
  <h2>Structural & Specialist Construction</h2>
  <p>
    As structural & specialist construction services make the core of our portfolio, we are flexible and
    proactive in growing to a well established construction and property services provider within
    United Kingdom. With range of works we proudly undertake to show the excellence of construction and
    property services, you can rely on us to make your building project or a property count.
  </p>
  <p>
    Want to find more about what we exactly do? Please visit our services menu sections.
  </p>
</section>
