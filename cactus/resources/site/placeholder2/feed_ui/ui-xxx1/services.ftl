<#import "/web/common.ftl" as c >

<section>
  <h1>Services</h1>
  <p>
    We provide a wide range of building, construction and maintenance services.
    Bellow you can see a list of activities our company is specializing in. Feel
    free to browse the ones you are care about, you can do it by clicking on any
    of them to open particular service page.
  </p>
</section>

<section id="services-design">
  <h2>Design & Build</h2>
  <p>
    Every successful building project starts from a good design, this is why we are so
    fascinated of proposing great designs to our clients. We place design services,
    construction and maintenance works to the highest standards. For this reason we
    collaborate with the best specialists in different fields of design & build routine,
    from interior and landscape designers to structural and electrical engineers. Even
    if our main activities are construction and building maintenance services for residential
    and commercial sectors, we understand the importance  of quality design and build
    management.
  </p>
</section>

<section id="services-brickwork">
  <h2>Brickwork & Masonry</h2>
  <p>
    Masonry  is one of the oldest crafts in the world. First time people shaped the stone and
    put it to build something, we can count it as a beginning of masonry building era ant it
    dates back to bronze age or even earlier. Bricks are nearly as solid as stone, so what used
    to be built of stone and clay now bricks and mortar have replaced this demand. Out of bricks
    correctly built structure can even last longer than concrete structures and with good
    architect the building itself can become to a state of art. This is why it is so versatile
    and so widely used in interiors and exteriors.
  </p>
</section>

<section id="services-carpentry">
  <h2>Carpentry & Joinery</h2>
  <p>
    Woodwork is one of the oldest and widely used methods in construction and building works.
    It is involved from start to finish of many building sites and every successful project is
    highly dependant on quality carpentry and joinery works. We are hard working professional
    carpenters and joiners based in London so you don’t have to look any further if looking for
    professional carpenters and joiners in London and surrounding areas such as Essex, Kent,
    Sussex, Surrey, Buckinghamshire and Hertfordshire.
  </p>
</section>

<section id="services-painting">
  <h2>Painting & Decorating</h2>
  <p>
    Painting and decorating are as much important for us as all others services we offer to our
    clients. We are a contractors with a unique ideas about how these finishing works are done.
    We pay close attention to  every detail in interior and exterior, offering authentic paintwork
    repairs, unique decorations are made to client’s requests, modern and highly skilled works are
    always carried out with a detailed precision so people who see our finished properties will
    love it for many years as a valuable part of any space.
  </p>
</section>

<section id="services-fitout">
  <h2>Kitchen Fitters London</h2>
  <p>
    We are London based professional kitchen installers offering our top of the range kitchen fitting
    and remodelling services we provide in London and surrounding areas exceeding M25 radius. Altana
    Building Company Ltd is a small London based company of building industry professionals. Here we
    established a team of highly skilled kitchen fitters supported by creative professionals of interior
    design. One of the key roles in our vision is to incorporate the latest technology into contemporary
    designs with a lowest carbon footprint possible. We work towards this by managing our logistics and
    workforce wisely to contribute to this goal.
  </p>
</section>

<section id="services-extension">
  <h2>House Extension in Maida Vale</h2>
  <p>
    Here is our most recent work in Maida Vale, London. We have built the extension of them reclaimed
    bricks and finishing with weather struck pointing now. This brickwork and pointing is done by our
    experienced bricklayers. Precision and attention to detail are our most important values. Please
    have a look at more photos below.
  </p>
</section>

<section id="services-newbuild">
  <h2>New Build Brick Facework</h2>
  <p>
    A brick facework is rising well in Central London. Our hard work is getting a shape. It takes our
    skilled bricklayers to do their best as they always do.
  </p>
</section>

<section id="services-refurbishment">
  <h2>Refurbishment</h2>
  <p>
    Most of the time our experienced kitchen fitters in London are working throughout town within designated
    areas, on the other hand, for sophisticated and demanding projects we team up and service suburbs of
    London and more remote areas of Essex, Kent, Sussex, Surrey, Buckinghamshire and Hertfordshire.
  </p>
</section>

<section id="services-renovation">
  <h2>Renovation</h2>
  <p>
    You can visit our Kitchen Fitting Services page to find more about kitchen installation works, also,
    please check Design & Build Services section for more general information about our profile of
    building works.
  </p>
</section>
