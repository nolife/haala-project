<#macro wikiArticleTitle article >
  <h1 class="wiki-article-title">
    <@wiki.articleText article=article key="title" />
    <#if parent??><span><a href="${parent.ref()}"><@wiki.articleText article=parent key="title" /></a></span></#if>
  </h1>
</#macro>

<#macro wikiArticleText article >
  <section class="wiki-article-text">
    <@wiki.articleText article=article key="text" />
  </section>
</#macro>

<#macro wikiArticleItems article >
  <#if article.items()?size != 0 >
    <nav class="wiki-article-items">
      <ul>
        <@wiki.listArticleItems article=article ; x>
          <li><a href="view.htm?ref=${x.ref()}" <#if x.status() == 0> class="private" </#if> ><@wiki.articleText article=x key="title" /></a></li>
        </@wiki.listArticleItems>
      </ul>
    </nav>
  </#if>
</#macro>

<#assign TYPE_ARTICLE=0 TYPE_BOOK=1 />

<#if article??>

  <@wiki.articleParent var="parent" article=article />

  <@wikiArticleTitle article />
  <#if article.type()==TYPE_ARTICLE ><@wikiArticleText article /></#if>
  <@wikiArticleItems article />

  <@htm.nodiv "page-ivars">
    { articleId: "${article.id()}", ref: "${article.ref()}", parentRef: "${(parent.ref())!}" }
  </@>

<#else>

  <div id="no-object">
    <h3>The requested article does not exist!</h3>
    <button class="history-back">Back</button>
  </div>

</#if>
