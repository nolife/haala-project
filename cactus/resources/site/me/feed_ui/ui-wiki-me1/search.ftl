<#if articles?? && articles?size != 0 >

  <ul class="article-list">
    <#list articles as o>
      <li><a href="${o.ref()}"><@wiki.articleText article=o key="title" /></a></li>
    </#list>
  </ul>

<#else>

  <div id="no-object">
    <h3>No articles found, please refine your search query!</h3>
  </div>

</#if>

<@htm.nodiv "page-ivars">
  { ref: "${requestParams.ref!}" }
</@>
