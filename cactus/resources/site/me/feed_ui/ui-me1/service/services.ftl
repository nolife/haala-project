<section>
  <h2>Our services</h2>
  <p>
    We can help you expand your business with the range of IT services. Please use the links on the left to find
    out more about web design, software development, hosting and search engine optimization.
    And if your requirement does not fall into any of the category, please contact us too see if we
    can help you to acheive your goals.
  </p>
</section>
