<section>
  <h2>Business software solutions</h2>
  <p>
    We provide you with unique solutions that are tailor made for your specific business needs and goals.
    Having strong focus on business requirements we strive for your complete customer satisfaction.
  </p>
  <p>
    Whenever possible we opt for open source products to save cost. And as the open source projects
    constantly imrove and get bug fixes they are the obvious choise to built our software upon.
  </p>
</section>
