<section>
  <h2>Modern web development</h2>
  <p>
    By using web development and HTML5 website design we can build great websites that customers return to.
    Your visitors will have great online experience resulting in increased retention rates and more profit.
  </p>
  <p>
    Building your website we use modern technologies such as HTML5, WEB20, CSS3, AJAX on top of our
    open source CMS. The result is a robust and stable website which is easy to customize and expand with
    logic specific to your business model.
  </p>
  <p>
    We may also provide you with websites built on top of other open source CMS such as Wordpress.
  </p>
</section>
