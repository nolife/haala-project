<section>
  <h2>Professional SEO</h2>
  <p>
    We specialize in search engine optimization.
    Even with the well designed website, a search engine optimization is critical to find success online.
    We will optimize your website to provide content for search engines so that your website gets immediate increase
    in traffic, visibility on all search engines, more customers and profits.
  </p>
  <p>
    The majority of online users are searching for websites through Google or Bing and instead find your rival websites.
    We will analyze and optimize elements of your website to ensure that your website ranks as highly as possible and
    maintains the best possible user experience.
  </p>
</section>
