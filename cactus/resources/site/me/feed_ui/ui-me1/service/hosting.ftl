<section>
  <h2>Cheap hosting</h2>
  <p>
    We offer cheap hosting for the web based solutions based on top of our open source CMS platform.
  </p>
  <p>
    Alternatively, if you opt for any other open source CMS such as Wordpress, we may purchase a hosting
    for you and install your web based solution on it.
  </p>
</section>
