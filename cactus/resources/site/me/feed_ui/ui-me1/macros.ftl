
<#-- Print page header -->
<#macro pageHeader>
  <@h.onPage index="-,00018">
    <div class="title">
      <@h.label var="tmpval" key="brief" bydefault=""/>
      <h1>${pageTitle}</h1>
      <p>${tmpval}</p>
    </div>
  </@>
</#macro>

<#macro label key tokens="">
  <@c.label key=key tokens=tokens />
</#macro>

<#-- Print captcha image -->
<#macro captcha>
  <img src="<@htm.url path='${turingPath}'/>"/>
</#macro>

<#-- Slider on the home page -->
<#macro slider>
  <@h.onPage index="00018">
    <#local count=3 />
    <div id="slider">
      <#list 1..count as n>
        <div class="card card-${n}">
          <div class="left">
            <div class="stripe"></div>
            <@label "slideH${n}"/>
            <img src="<@htm.url "web/home-slide${n}.jpg"/>"/>
          </div>
          <div class="text"><@label "slideT${n}"/></div>
        </div>
      </#list>
      <div class="anchors">
        <span class="first"></span>
        <#list 1..count as n>
          <a href="#" class="anchor-${n}">${n}</a>
        </#list>
        <span class="last"></span>
      </div>
    </div>
  </@>
</#macro>
