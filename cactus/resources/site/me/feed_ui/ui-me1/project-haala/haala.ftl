<section>
  <h2>The open source web development platform.</h2>
  <p>
    Project HAALA is the open source project built for delivering web applications, which are easy to customize and
    adapt to your business model. It provides robust, multilingual, multi-tenancy websites, built-in CMS with plenty
    of features.
  </p>
  <p>
    The project is built on cutting edge technologies requiring developer's effort to produce sophisticated web
    applications. On the other hand, creating a simple website is as easy as applying design and adding pages.
    And it comes with CMS and demo websites.
  </p>
  <p>
    Please use the links on the left to learn more about the project.
  </p>
</section>
