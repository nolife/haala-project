<section>
  <h2>License (LGPL)</h2>
  <p>
    The Haala project and its artefacts (unless stated otherwise) fall under terms of the
    <a href="http://www.gnu.org/copyleft/lesser.html" target="_blank">GNU Lesser General Public License (LGPL) version 3</a>.
  </p>
  <h3>Why move from the GPL to the Lesser GPL?</h3>
  <p>
    Using the Lesser GPL permits use of an artefact in proprietary programs; using the ordinary GPL for an artefact
    makes it available only for free programs. In practice this means that you can incorporate an LGPL artefact in a
    proprietary project, however, if you modify its source code you are required to release that under the terms of
    the LGPL. More than that, the LGPLv3 is compatible with the APACHEv2 license.
  </p>
  <p>
    The LGPL is flexible enough to allow the use of artefacts in both open source and commercial projects. The LGPL
    guarantees that artefacts and any modifications made to them will stay open source.
  </p>
  <p>
    Importing artefacts' public interfaces in your Java code, extending them by sub-classing or implementation of an
    extension interface is considered to be dynamic linking. Hence this interpretation of the LGPL is that the use of
    the unmodified source does not affect the license of your application code. The use of the unmodified binaries of
    never affects the license of your application or distribution.
  </p>
  <h3>Submitting your modifications</h3>
  <p>
    If you modify artefacts and redistribute your modifications, the LGPL applies.<br>
    Please submit any modifications to email <span class="link">haala@coldcore.com</span>
  </p>
  <h3>Related links</h3>
  <p>
    <a href="http://www.gnu.org/copyleft/lesser.html" target="_blank">GNU Lesser General Public License official web page</a><br>
    <a href="http://en.wikipedia.org/wiki/GNU_Lesser_General_Public_License" target="_blank">GNU Lesser General Public License explained</a><br>
    <a href="http://www.fsf.org/licensing/licenses" target="_blank">The list of the open source licenses supported by the Free Software Foundation</a><br>
    <a href="http://www.apache.org/licenses/GPL-compatibility.html" target="_blank">Apache License v2.0 and GPL Compatibility</a>
  </p>
</section>
