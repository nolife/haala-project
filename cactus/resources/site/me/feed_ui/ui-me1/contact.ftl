<@htm.form id="page-form">
  <div class="form-panel">
    <div class="cell">
      Please fill in your full name with contact email address and we will do our best to get in touch with you
      within 24 hours. Ensure that your email address is correct as it is used by us to contact you.
    </div>
    <div class="cell name">
      <label>Your name</label>
      <@h.userFullName var="name" />
      <input type="text" value="${name!}" maxlength="100"/>
      <p class="error"></p>
    </div>
    <div class="cell email">
      <label>Your email</label>
      <@h.user var="user" />
      <input type="text" value="${(user.email())!}" maxlength="100"/>
      <p class="error"></p>
    </div>
    <div class="cell text">
      <label>&nbsp;</label>
      <textarea></textarea>
      <p class="error"></p>
    </div>
    <div class="cell title">
      Enter the text in the image
    </div>
    <div class="cell turing">
      <@m.captcha />
      <input type="text" maxlength="10"/>
      <p class="error"></p>
    </div>
    <div class="cell submit">
      <button><@label "btn.send"/></button>
    </div>
  </div>
</@>

<@htm.nodiv "page-messages">
  <div data-key="dialog-title">Thank you</div>
  <div data-key="dialog-text">Your message has been sent and we will contanct you shortly.</div>
</@>
