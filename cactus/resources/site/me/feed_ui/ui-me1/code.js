
var Haala = Haala || {};

(function($) {

  Haala.Code = {

    init: function() {

      if (Haala.MereCms) Haala.MereCms.Integ.init();
      Haala.Misc.tooltips();

      $("button.history-back").button().click(function() {
        history.back();
      });

      $(".logo").click(function() {
        document.location = "/";
      });

      var highlightOnProjectHaala = [
        "/project/haala/documentation.htm",
        "/project/haala/sourcecode.htm",
        "/project/haala/license.htm"
      ];

      $("#mainmenu").kdxMenu({
        items: [
          { element: "#mainmenu .home", highlightOnURL: ["/"] },
          { element: "#mainmenu .services",
            dropdown: { offset: { x: -10, y: 12 }, element: "#mainmenu-b" }},
          { element: "#mainmenu .projects", highlightOnURL: highlightOnProjectHaala,
            dropdown: { offset: { x: -10, y: 12 }, element: "#mainmenu-c" }},
          { element: "#mainmenu-c .haala", highlightOnURL: highlightOnProjectHaala }
        ]
      });

      $("#sidemenu").kdxMenu();

    },

    ready: function() {
      Haala.Code.stretch();
    },

    stretch: function() {
//      var h = $("#content").height();
//      $("#right-column").height(h+20);
    }

  };

})(jQuery);
