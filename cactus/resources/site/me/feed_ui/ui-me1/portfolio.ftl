<section>
  <a href="http://www.wilson-hawkins.co.uk" target="_blank">
    <img src="/web/portfolio/wilson-hawkins.jpg"/>
  </a>
  <p>
    Web design and the complete online solution for Wilson-Hawkins real estate agency. Built on top
    of our open source web development platform.
  </p>
</section>

<section>
  <a href="/project/haala.htm">
    <img src="/web/portfolio/project-haala.jpg"/>
  </a>
  <p>
    Project Haala is the open source project built for delivering web applications which are
    easy to customize and adapt to your business model.
  </p>
</section>

