
(function($) {

  Haala.Misc.onDocReady(function() {
    initSlider();
  });



  var initSlider = function() {
    $("#slider").cardSlide({
      hardDelay: 30000,
      cards: [
        { element: "#slider .card-1", anchor: "#slider .anchor-1"},
        { element: "#slider .card-2", anchor: "#slider .anchor-2"},
        { element: "#slider .card-3", anchor: "#slider .anchor-3"}
      ],
      slideCardsFn: function(/*json*/ card1, /*json*/ card2, /*json*/ model, /*fn*/ callback) {
        card1.element.hide("slide", { direction: "left" }, 800);
        card2.element.show("slide", { direction: "right" }, 800);
        setTimeout(callback, 800);
      }
    });
  };

})(jQuery);

