<#import "/web/common.ftl" as c >
<#import "/web/common-htm.ftl" as htm >
<#import "/web/macros.ftl:"+currentSite as m >

<header>

  <div class="logo">
    <h3>Web Design, Software Development, IT Services, SEO  and Hosting</h3>
  </div>

  <div class="enquiry">
    <p>+4420 3290 2266</p>
    <a href="mailto:info@coldcore.com">info@coldcore.com</a>
    <div></div>
  </div>

  <nav id="mainmenu">
    <div class="first"></div>
    <a class="home" href="/">Home</a>
    <div class="separator"></div>
    <a class="portfolio" href="/portfolio.htm">Portfolio</a>
    <div class="separator"></div>
    <a class="services" href="/services.htm">Services</a>
    <div class="separator"></div>
    <a class="projects" href="/projects.htm">Projects</a>
    <div class="separator"></div>
    <a class="team" href="/team.htm">Team</a>
    <div class="separator"></div>
    <a class="contact" href="/contact.htm">Contact</a>
    <div class="separator"></div>
    <a class="testimonials" href="/testimonials.htm">Testimonials</a>
    <div class="last"></div>
  </nav>

  <div id="mainmenu-b" class="mainmenu-dropdown">
    <a class="first" href="/service/webdesign.htm">Web Design</a>
    <a href="/service/softwaredev.htm">Software Development</a>
    <a href="/service/hosting.htm">Hosting</a>
    <a class="last" href="/service/seo.htm">SEO</a>
  </div>

  <div id="mainmenu-c" class="mainmenu-dropdown">
    <a class="haala first last" href="/project/haala.htm">Haala</a>
  </div>

  <@m.pageHeader/>
  <@m.slider/>

</header>


<#-- wrapper start -->
<div id="wrapper">

  <aside id="left-column">
    <@h.onPage index="00018,
                      00projects,00contact,
                      00services,00serviceWeb,00serviceDev,00serviceCon,00serviceHos,00serviceSeo,
                      00projectHaala,00projectHaalaDoc,00projectHaalaSrc,00projectHaalaLcs,
                      00wiki,00wikiSearch">
      <div class="quickhead">
        <div class="icon"></div>
        <h3>quick navigation</h3>
        <p>other sections available</p>
      </div>
      <nav id="sidemenu">
        <@h.onPage index="00018">
          <a href="/portfolio.htm">Portfolio</a>
          <a href="/services.htm">Services</a>
          <a href="/projects.htm">Projects</a>
          <a href="/testimonials.htm">Testimonials</a>
        </@>
        <@h.onPage index="00services,00serviceWeb,00serviceDev,00serviceCon,00serviceHos,00serviceSeo">
          <a href="/services.htm">Services</a>
          <a href="/service/webdesign.htm">Web Design</a>
          <a href="/service/softwaredev.htm">Software Development</a>
          <a href="/service/hosting.htm">Hosting</a>
          <a href="/service/seo.htm">SEO</a>
        </@>
        <@h.onPage index="00projects">
          <a href="/projects.htm">Projects</a>
          <a href="/project/haala.htm">Project Haala</a>
        </@>
        <@h.onPage index="00projectHaala,00projectHaalaDoc,00projectHaalaSrc,00projectHaalaLcs,
                          00wiki,00wikiSearch">
          <a href="/project/haala.htm">About</a>
          <a href="/wiki/CGVCDHWH">Documentation</a>
          <a href="/wiki/FXEKIGWQ">Source Code</a>
          <#--<a href="/project/haala/documentation.htm">Documentation</a>-->
          <#--<a href="/project/haala/sourcecode.htm">Source Code</a>-->
          <a href="/project/haala/license.htm">License</a>
        </@>
      </nav>
      <@h.onPage index="00contact">
        <section class="contact">
          <p>We based in <span class="link">London, UK.</span></p>
          <p>Our enquiry email is <a href="mailto:info@coldcore.com">info@coldcore.com</a></p>
          <p>You may call us on <span class="link">(+44) 020 3290 2266</span></p>
          <p>UK post address is available on request.</p>
        </section>
      </@>
      <@h.onPage index="00wiki,00wikiSearch">
        <@htm.form id="wiki-search">
          <p>Search documentation</p>
          <input type="text" maxlength="100" value="${requestParams.query!}"/>
          <button>Search</button>
        </@>
      </@>
    </@>
  </aside>

  <div id="content">
    <@c.pageContent />
  </div>

  <div class="nofloat"></div>
</div>
<#-- wrapper end -->


<footer>
  <nav>
    <a href="/">Home</a> |
    <a href="/portfolio.htm">Portfolio</a> |
    <a href="/services.htm">Services</a> |
    <a href="/projects.htm">Projects</a> |
    <a href="/project/haala.htm">Project Haala</a> |
    <a href="/team.htm">Team</a> |
    <a href="/contact.htm">Contact</a> |
    <a href="/testimonials.htm">Testimonials</a>
  </nav>
  <@htm.copyright vendor="y"/>
</footer>

<div id="bottomfill"></div>
