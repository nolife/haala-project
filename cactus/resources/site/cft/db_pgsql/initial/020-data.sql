------------------------------
-- Extension to generic 020 --
------------------------------

INSERT INTO DOMAINS(ID, DOMAIN, DOMAIN_SITE, TYPE, META)
VALUES (nextval('DOMAINS_SEQ'), 'cftp.EXAMPLE.ORG', 'cft', 'other', 'style=html5,layout-modern');

insert into SETTINGS (id, key, value, site) values (nextval('SETTINGS_SEQ'), 'MainURL', 'http://cftp.EXAMPLE.ORG', 'cft1');
