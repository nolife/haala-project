<section>
  <h1>Code samples</h1>
  <ul>
    <li><a href="samples/sample-1.htm">A better launcher using ApplicationContext</a></li>
    <li><a href="samples/sample-2.htm"">Using a callback for post upload/download logic</a></li>
    <li><a href="samples/sample-3.htm"">Forcing a server to work only the office hours</a></li>
    <li><a href="samples/sample-4.htm"">Extending the default PASS command for sign in</a></li>
    <li><a href="samples/sample-5.htm"">Implementing a new X-TIME command</a></li>
  </ul>
  <p>

  </p>
</section>
