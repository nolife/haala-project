<section>
  <h1>Code sample - Extending the default PASS command for sign in</h1>
  <p>
    The default PASS command rejects passwords for all users but anonymous. I will show how to change this
    behaviour to allow users to sign in.
  </p>
  <p>
    Let's assume that the class <i>UserProvider</i> already exists and able to validate usernames and passwords.
    This class exposes the following method:
  </p>

  <pre>
public MyUser login(String username, String password);
  </pre>

  <p>
    If a user provides a valid username and password then the method creates a new <i>MyUser</i> object and populates
    it with some user-specific info, in all other cases this method returns <i>null</i>.
  </p>
  <p>
    If you open the original <i>PassCommand</i> you will see that it can be easily extended to provide
    username/password validation mechanism. Let's extend this command:
  </p>
  
  <pre>
public class RegPassCommand extends PassCommand {

  protected boolean checkLogin(String username, String password) {
    UserProvider provider =
           (UserProvider) ObjectFactory.getObject("userProvider");

    MyUser user = provider.login(username, passowrd);
    if (user == null) return false;

    Session session = getConnection().getSession();
    session.setAttribute("myUser", user);
    return true;
  }
}  </pre>
  
  <p>
    In this example I am using the static method of the <i>ObjectFatory</i> to get the <i>UserProvider</i> bean 
    which validates supplied usernames and passwords (another option would be to write a setter and pass this 
    provider in your <i>beans.xml</i> file).
  </p>
  <p>
    If sign in operation is a success, the command saves acquired <i>MyUser</i> object into user's session just 
    in case. The parent <i>PassCommand</i> takes care of sending proper replies back to the user.
  </p>
  <p>
    The only thing left to do is to insert the <i>UserProvider</i> bean into your <i>beans.xml</i> file so that it 
    will be available through the <i>ObjectFactory</i> and to replace the default PASS command with the new one:
  </p>
  
  <pre>
&lt;bean id="userProvider"
      class="com.mysite.coloradoftp.UserProvider"&gt;
  ................
&lt;/bean&gt;

&lt;bean id="regPassCommand"
      class="com.mysite.coloradoftp.RegPassCommand"
      singleton="false"/&gt;

&lt;bean id="commandFactory"
      class="com.coldcore.coloradoftp.command.impl.GenericCommandFactory"&gt;
  &lt;constructor-arg index="0"&gt;
    &lt;map&gt;
      &lt;entry key="USER" value="userCommand"/&gt;
      &lt;entry key="PASS" value="regPassCommand"/&gt;
      &lt;entry key="PWD" value="pwdCommand"/&gt;
      ...............
    &lt;/map&gt;
  &lt;/constructor-arg&gt;
&lt;/bean&gt;
  </pre>

  <p>
    The first XML defines the <i>UserProvider</i> object (the class that you have to write yourself). The second
    XML defines a new PASS command named <i>regPassCommand</i>. And the final XML substitutes the value of the PASS
    key in the command factory with the new <i>regPassCommand</i>. Now if a user inputs PASS then this
    <i>regPassCommand</i> will be executed.
  </p>
</section>
