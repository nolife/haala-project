<section>
  <h1>Code sample - A better launcher using ApplicationContext</h1>
  <p>
    By <strong>Dmitri Toubelis</strong>
  </p>
  <p>
    I use Spring/Hibernate combination in my callback function and I could not make Hibernate transactions to work
    even though exactly same configuration worked just fine with web application. I tried couple of things and
    solution was to use ApplicationContext instead of BeanFactory. I modified <i>Launcher.java</i> for this and my
    version is attached. It has slightly extended behaviour also - it accepts multiple configuration files on command
    line, which I find quite useful.
  </p>

<pre>
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.FileSystemXmlApplicationContext;

import com.coldcore.coloradoftp.core.Core;
import com.coldcore.coloradoftp.factory.ObjectFactory;
import com.coldcore.coloradoftp.factory.ObjectName;
import com.coldcore.coloradoftp.factory.impl.SpringFactory;

/**
 * Launcher.
 * <p/>
 * A simple class to start an FTP server as a standalone application.
 * When started, FTP server can be killed by killing the process it is running in, but doing
 * so will terminate the server immediately without calling its stop or poisoned routines.
 * <p/>
 * <p/>
 * ColoradoFTP - The Open Source FTP Server (http://cftp.coldcore.com)
 */
public class Launcher {

  private static String filename = "conf/beans.xml";

  public static void main(String[] args) {

    System.out.println();
    System.out.println("========================================");
    System.out.println("ColoradoFTP - the open source FTP server");
    System.out.println("Make  sure  to  visit   www.coldcore.com");
    System.out.println("========================================");

    if (args.length == 0) {
      args = new String[]{filename};
      System.out.println("Using default configuration from: " + filename);
      System.out.println("To set a different configuration file(s) use 'Launcher filename1 filename2...'");
    }

    // create application context
    try {
      ApplicationContext context = new FileSystemXmlApplicationContext(args);
      ObjectFactory.setInternalFactory(new SpringFactory(context));
    } catch (Throwable e) {
      System.out.println("Cannot initialize object factory, terminating...");
      e.printStackTrace();
      System.exit(1);
    }
    System.out.println("Object factory initialized.");

    // start the service
    Core core = null;
    try {
      core = (Core) ObjectFactory.getObject(ObjectName.CORE);
      core.start();
    } catch (Throwable e) {
      System.out.println("Unable to start the server, terminating...");
      e.printStackTrace();
      System.exit(1);
    }

    System.out.println("Server started, use Ctrl+C to kill the process");
  }
}
</pre>
</section>
