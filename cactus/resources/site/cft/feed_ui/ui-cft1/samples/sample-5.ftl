<section>
  <h1>Code sample - Implementing a new X-TIME command</h1>
  <p>
    In this example I will show how to implement a very simple command that outputs current time. When a user 
    inputs X-TIME a server replies something like this:
  </p>

  <pre>
Server's time is: 10/10/2006 12:18:02
  </pre>

  <p>
    This is the source of the command:
  </p>
  
  <pre>
public class XTimeCommand extends AbstractCommand {

  private String pattern = "dd/MM/yyyy HH:mm";

  public void setPattern(String pattern) {
    this.pattern = pattern;
  }

  public Reply execute() {
    SimpleDateFormat sdf = new SimpleDateFormat(pattern);
    reply.setCode("200");
    reply.setText("Server's time is: "+sdf.format(new Date()));
    return reply;
  }
}    
  </pre>
  
  <p>
    Now you have to update your <i>beans.xml</i> so that the command becomes available, as follows:
  </p>
  
  <pre>
&lt;bean id="xtimeCommand"
      class="com.mysite.coloradoftp.XTimeCommand"
      singleton="false"&gt;
  &lt;property name="pattern" value="dd.MM.yyyy HH:mm:ss"/&gt;
&lt;/bean&gt;

&lt;bean id="commandFactory"
      class="com.coldcore.coloradoftp.command.impl.GenericCommandFactory"&gt;
  &lt;constructor-arg index="0"&gt;
    &lt;map&gt;
      &lt;entry key="USER" value="userCommand"/&gt;
      &lt;entry key="PASS" value="passCommand"/&gt;
      &lt;entry key="PWD" value="pwdCommand"/&gt;
      ...............
      &lt;entry key="X-TIME" value="xtimeCommand"/&gt;
    &lt;/map&gt;
  &lt;/constructor-arg&gt;
&lt;/bean&gt;
  </pre>

  <p>
    The first XML defines the command object (named <i>xtimeCommand</i>). The second XML piece shows how to add this
    <i>xtimeCommand</i> to the command factory. Now if a user inputs X-TIME then the <i>xtimeCommand</i> will be
    executed.
  </p>
  
</section>
