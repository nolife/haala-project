<section>
  <h1>Code sample - Forcing a server to work only the office hours</h1>
  <p>
    magine that for some weird reason your FTP server must accept connections only during certain hours:
    from 10:00 till 17:00. To implement this behaviour we will extend <i>ControlConnector</i>. The best place
    to insert the fix is into the <i>configure</i> method – where a connector configures a new connection
    before it goes into a connection pool:
  </p>

  <pre>
public class TimedControlConnector extends GenericControlConnector {

  public void configure(ControlConnection connection) {
    Date cur = new Date();
    boolean timeRight = cur.compareTo(TIME_1000) > 0 &&
                                      cur.compareTo(TIME_1700) < 0;
    if (!timeRight) {
      Reply reply = (Reply) ObjectFactory.getObject(ObjectName.REPLY);
      reply.setCode("421");
      reply.setText("Service not available, closing control connection.");
      connection.reply(reply);
      connection.poison();
    } else {
      super.configure(connection);
    }
  }
}
  </pre>

  <p>
    We will replace the default <i>controlConnector</i> with a new implementation in the <i>beans.xml</i> file
    as follows:
  </p>

  <pre>
&lt;bean id="controlConnector"
      class="com.mysite.coloradoftp.TimedControlConnector"/&gt;
  </pre>

  <p>
    Now if a user tries to connect outside of the office hours she will see <i>421</i> reply and her connection
    will be terminated.
  </p>
  <p>
    <i>Note:</i> I recommend to extend <i>IntelControlConnector</i> from the Intelligent Pack plug-in rather than
    <i>GenericControlConnector</i> as shown here. But this is just an example.
  </p>

</section>
