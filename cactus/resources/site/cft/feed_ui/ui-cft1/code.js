
var Haala = Haala || {};

(function($) {

  Haala.Code = {

    init: function() {

      if (Haala.MereCms) Haala.MereCms.Integ.init();

      $("button.history-back").button().click(function() {
        history.back();
      });

      $(".logo").click(function() {
        document.location = "/";
      });

      $("#mainmenu").kdxMenu({
        items: [
          { element: "#mainmenu .home", highlightOnURL: ["/"] },
          { element: "#mainmenu .documentation", highlightOnURL: [
            "/developer-corner.htm", "/plugins.htm", "/code-samples.htm", "/testing.htm", "/building-source.htm", "/run-embedded.htm" ]}
        ]
      });

      $("#left-column").height($("#content").height()+20);

    }

  };

})(jQuery);
