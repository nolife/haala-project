<#import "/web/common.ftl" as c >

<header>

  <div class="logo">
    <h1>ColoradoFTP</h1>
    <h2>The Open Source FTP Server</h2>
  </div>

</header>

<nav id="mainmenu">
  <a class="home" href="/">Home</a>
  <a class="demo" href="/demo.htm">Demo</a>
  <a class="documentation" href="/documentation.htm">Documentation</a>
  <a class="download" href="/download.htm">Download</a>
  <a class="contact" href="/contact.htm">Contact</a>
  <a class="license" href="/license.htm">License</a>
</nav>

<aside id="left-column">
  <div class="donate">
    Please donate and <br>
    support the project
    <form action="https://www.paypal.com/cgi-bin/webscr" method="post" target="_blank">
      <input name="cmd" value="_xclick" type="hidden">
      <input name="business" value="sergey@coldcore.com" type="hidden">
      <input name="item_name" value="ColoradoFTP Donation" type="hidden">
      <input name="no_shipping" value="0" type="hidden">
      <input name="no_note" value="1" type="hidden">
      <input name="currency_code" value="GBP" type="hidden">
      <input name="tax" value="0" type="hidden">
      <input name="lc" value="GB" type="hidden">
      <input name="bn" value="PP-DonationsBF" type="hidden">
      <input src="https://www.paypal.com/en_US/i/btn/x-click-but04.gif" name="submit" alt="Make payments with PayPal - it's fast, free and secure!" border="0" type="image">
      <img alt="" src="https://www.paypal.com/en_GB/i/scr/pixel.gif" border="0" height="1" width="1">
    </form>
  </div>
  <div class="updated">
    This website was last updated<br>
    02 January 2014
  </div>
</aside>

<div id="content">
  <@c.pageContent />
</div>

<footer>
  <p class="copyright">&copy; 2014</p>
  <a class="vendor" target="_blank" href="http://coldcore.com">
    <img src="/web/z/dot.gif"/>
  </a>
</footer>
