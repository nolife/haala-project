<section>
  <h1>Demo FTP Server</h1>
  <p>
    This FTP server has several accounts to demonstrate as much of the available features as possible.
    Demo resets itself 2 times a day at 12AM and 12PM GMT. Total data transfer speed restricted to 10 KB/sec.
    I recommend to use your favourite FTP client rather than clicking on URLs provided here. By using these
    URLs you will not be able to see the commands you send and the responses you get. Also you may only upload
    filenames containing English symbols and numbers.
  </p>

  <div class="verbose">
    <p class="caption">FTP server location</p>
    <p><span>Host</span> <span>cftp-demo.coldcore.com</span></p>
    <p><span>Port</span> <span>8021</span></p>
  </div>

  <div class="verbose configuration">
    <p class="caption">Configuration files</p>
    <p>
      <a href="<@h.url path="files/coloradoftp-tryout/beans.xml" nc="1"/>">beans.xml</a>
      <a href="<@h.url path="files/coloradoftp-tryout/filesystem.xml" nc="1"/>">filesystem.xml</a>
      <a href="<@h.url path="files/coloradoftp-tryout/xml-users.xml" nc="1"/>">xml-users.xml</a>
    </p>
  </div>

  <div class="verbose">
    <p class="caption">Regular account details</p>
    <p><span>Username</span> <span>ftpuser</span></p>
    <p><span>Password</span> <span>ftpuser123</span></p>
    <p>
      <span>URL</span>
      <span><a href="ftp://ftpuser:ftpuser123@cftp-demo.coldcore.com:8021/" target="_blank">ftp://ftpuser:ftpuser123@cftp-demo.coldcore.com:8021/</a></span>
    </p>
    <p class="offset">
      Test all basic features of the FTP server using this account. It does not have any restrictions.
      Create, delete and rename files or directories, upload and download files, stop and resume files transfers.
    </p>
  </div>

  <div class="verbose">
    <p class="caption">Anonymous account details</p>
    <p><span>Username</span> <span>anonymous</span></p>
    <p><span>Password</span> <span>some@email.com</span></p>
    <p>
      <span>URL</span>
      <span><a href="ftp://cftp-demo.coldcore.com:8021/" target="_blank">ftp://cftp-demo.coldcore.com:8021/</a></span>
    </p>
    <p class="offset">
      Anonymous user may only view, upload and download files - all other operations are disabled. This user may not
      overwrite existing files.
    </p>
  </div>

  <div class="verbose">
    <p class="caption">Ember account details</p>
    <p><span>Username</span> <span>ember</span></p>
    <p><span>Password</span> <span>EmbEr</span></p>
    <p>
      <span>URL</span>
      <span><a href="ftp://ember:EmbEr@cftp-demo.coldcore.com:8021/" target="_blank">ftp://ember:EmbEr@cftp-demo.coldcore.com:8021/</a></span>
    </p>
    <p class="offset">
      This user demonstrates the flexibility of the Hard File System plug-in. Directories surrounded with ~ are
      virtual folders located outside of the user home. All the directories have names explaining what you can and
      what you cannot do once you enter them. You will not be able to delete or rename any of those directories.
      Also the root directory does not allow you to change anything in it.
    </p>
  </div>

  <div class="verbose">
    <p class="caption">Procam account details</p>
    <p><span>Username</span> <span>procam</span></p>
    <p><span>Password</span> <span>procam456</span></p>
    <p>
      <span>URL</span>
      <span><a href="ftp://procam:procam456@cftp-demo.coldcore.com:8021/" target="_blank">ftp://procam:procam456@cftp-demo.coldcore.com:8021/</a></span>
    </p>
    <p class="offset">
      This user demonstrates the features of the Web Cam plug-in. Upload a picture (GIF or JPG no more than 100 KB)
      and it will be posted to HTTP server bypassing the hard drive. Your picture will appear on this page:
      <a href="http://cftp-demo.coldcore.com/cftp-webcam/display.html" target="_blank">http://cftp-demo.coldcore.com/cftp-webcam/display.html</a>
    </p>
  </div>
</section>


