<section class="prime-edition">
  <h2>Prime Edition</h2>
  <p>
    Production ready FTP server featuring all essencial plug-ins. This is ideal as an out-of-the-box FTP server
    to enable files exchange.
  </p>
  <p class="center">
    <a href="<@h.url path="files/coloradoftp-prime.zip" nc="1"/>">Download Prime Edition (Build 8)</a>
  </p>
</section>

<section class="beta-edition">
  <h2>Beta Edition</h2>
  <p>
    This FTP server is mounted to an in-memory file system and should be used as an underlying FTP server in Java
    tests. Example tests available <a href="<@h.url path="files/betatest.zip" nc="1"/>">here</a>.
  </p>
  <p class="center">
    <a href="<@h.url path="files/coloradoftp-beta.zip" nc="1"/>">Download Beta Edition (Build 2)</a>
  </p>
</section>

<section class="source-code">
  <h2>Source Code</h2>
  <p>
    ColoradoFTP is the open source Java project available in
    <a href="https://bitbucket.org/nolife/coloradoftp" target="_blank">Bitbucket GIT repository</a>
  </p>
</section>

<section class="compiled-libraries">
  <h2>Compiled Libraries</h2>
  <p>
    Maven dependencies, which must be manually installed into your local repository in order to build the project.
  </p>
  <p class="center">
    <a href="<@h.url path="files/libraries.zip" nc="1"/>">Download Libraries (Build 1)</a>
  </p>
</section>
