<section>
  <h1>Contact</h1>
  <p>
    Please email all your questions, regards, bugs, patches, modifications etc. to
    <span class="link">cftp @ coldcore.com</span>
  </p>
</section>
