<section class="about">
  <h1>What is ColoradoFTP?</h1>
  <p>
    ColoradoFTP is the open source Java FTP server. It is fast, reliable and extendable.
    Fully compatible with RFC 959 and RFC 3659 (File Transfer Protocol and Extensions) this
    implementation makes it easy to extend the functionality with virtually any feature.
    Well commented source code and existing plug-ins make it possible to shape the FTP
    server just the way you want! Run it on any platform featuring the latest
    <a href="http://java.sun.com" target="_blank">Java VM</a> – Windows, Linux, Unix etc.
  </p>
</section>

<section class="source">
  <h2>Source Code</h2>
  <p>
    ColoradoFTP is the open source Java project available in<br>
    <a href="https://bitbucket.org/nolife/coloradoftp" target="_blank">Bitbucket GIT repository</a>
  </p>
</section>

<section class="develop">
  <h2>Develop with me</h2>
  <p>
    ColoradoFTP is the open source Java project distributed under the LGPL license. It is capable
    to handle any sort of work. I developed the original set of interfaces and base implementations
    just enough to make it run and provide basic FTP services. I am looking into Java community to
    extend my work with additional features by writing plug-ins others may apply to their builds.
    As a developer I may write a generic plug-in to fulfil your requirements, publish your plug-ins
    on the project web site, re-factor and maintain your implementations, adjust the core if your
    idea cannot fit into existing architecture. Develop with me!
  </p>
  <p>
    This project is developed and maintained by its author Sergei Abramov. You are welcome to share
    your implementations, ideas and suggestions. Please send everything to
    <span class="link">cftp @ coldcore.com</span>
  </p>
</section>

<section class="events">
  <h2>Events</h2>
  <ul>
    <li>
      <p>
        <span class="event-date">02 January 2014</span><br>
        The web site was redesigned, <a href="documentation.htm">documentation</a> simplified and the source
        code moved to Bitbucket GIT repository. Minor updates and the demo FTP server is finally working.
      </p>
    </li>
    <li>
      <p>
        <span class="event-date">24 April 2008</span><br>
        The project's license has changed from the GPL to the LGPL. Read more in the <a href="license.htm">license</a>
        section.
      </p>
    </li>
    <li>
      <p>
        <span class="event-date">25 February 2008</span><br>
        In a bid to provide yet better support to users this <a href="http://cftp-wiki.coldcore.com">project's Wiki</a>
        has been assembled.
      </p>
    </li>
    <li>
      <p>
        <span class="event-date">07 July 2007</span><br>
        Significant efforts has been made on the project during the last month. RFC 3659 implemented, post
        upload/download logic added, tests written and many bugs fixed. Aside from that, the FTP server
        handled 1500+ simultaneous file transfers just fine.
      </p>
    </li>
  </ul>
</section>

<div class="nofloat"></div>
