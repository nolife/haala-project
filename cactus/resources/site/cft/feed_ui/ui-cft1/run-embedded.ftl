<section>
  <h1>Run as Embedded and In-Memory</h1>
  <p>
    You can look at how the <i>testpack</i> <a href="testing.htm">(testing the server)</a> is implemented and use its
    <i>beans.xml</i> which uses in-memory file system. Or you can download
    <a href="<@h.url path="files/betatest.zip" nc="1"/>">this simple test case</a> which requires the
    <i>Beta Edition FTP Server</i> to be present on your disk (available in Download section) and see how that works.
    Both approaches do the same thing: run embedded FTP server which uses in-memoty file system.
  </p>
</section>
