<section>
  <h1>Building from Source</h1>
  <p>
    Download the source and use Maven 3 to build it. It will complain about missing libraries, which are available in
    the GIT repository (or JAR can be downloaded from this web site in Download section) and have to be manually
    installed into your local Maven repository.
  </p>
</section>
