<section>
  <h1>Testing the Server</h1>
  <p>
    There is the <strong>testpack</strong> project available in the GIT repository. It is an IntelliJ project which
    uses Maven 3 to get all of the dependencies. It will complain about missing libraries, which are available in the
    GIT repository (or JAR can be downloaded from this web site in Download section) and have to be manually installed
    into your local Maven repository.
  </p>
  <p>
    The project is preconfigured with tests (testpack.ipr) which allows you to simply select "test Examples" from
    IntelliJ run configuration and run that portion of tests. It can take quite a while if you wish to run all the
    tests, including "test Concurrent" which is an optioal stress test. The testpack uses in-memory filesystem and
    requires <i>2121</i> and <i>6000-6005</i> ports to be available.
  </p>
</section>
