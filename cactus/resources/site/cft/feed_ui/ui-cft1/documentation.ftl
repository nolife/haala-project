<section>
  <h1>About</h1>
  <p>
    ColoradoFTP is the open source FTP server distributed under the LGPL license. As a Java developer you may shape
    this server any way you want – replace any parts or add new features easily. The goal of this project was to
    create an FTP server featuring the possibility to include tasks specific to particular business. Fast and stable
    FTP server to handle the work you need. Built with the Spring Framework it utilizes all the benefits of transparent
    application wiring giving you the true freedom to shape the server as you wish. You may extend basic
    implementations to provide the functionality you require. More than that, there are also plug-ins which you may
    apply to enhance your FTP server.
  </p>

  <h2>How to run the server?</h2>
  <ol>
    <li>Download a binazy ZIP and extract it.</li>
    <li>Go into the extracted folder and run start.bat if you are on Windows or start.sh if you are on Linux/Unix.</li>
    <li>Point your FTP client to host <i>127.0.0.1</i> and <i>port 21</i>.</li>
  </ol>
  <p>There is no GUI available and to kill the server use <i>Ctrl+C</i></p>

  <h2>For Developers</h2>
  <p>
    If you good with Java or want to dive into some technical stuff then go to
    <a href="developer-corner.htm">Developer's Corner</a> which also includes
  </p>
  <p>
    <a href="plugins.htm">List of plug-ins</a><br/>
    <a href="code-samples.htm">Code samples</a><br/>
    <a href="testing.htm">Testing the server</a><br/>
    <a href="building-source.htm">Building from source</a><br/>
    <a href="run-embedded.htm">Runnung the server as embedded and in-memory</a>
  </p>

  <h2>Contributors</h2>
  <div class="verbose">
    <p>
      <span>Dmitri Toubelis</span>
      <span>Assisted in the development of the Gateway plug-in and supplied code samples.</span>
    </p>
    <p>
      <span>Sakke Wiik</span>
      <span>Tested the project on very early stages which resulted in many valuable project updates.</span>
    </p>
    <p>
      <span>Shane Duan and Tao Wen</span>
      <span>Assisted in the development of the COTTA File System plug-in.</span>
    </p>
    <p>
      <span>Sergei Abramov</span>
      <span>Project lead.</span>
    </p>
  </div>
</section>
