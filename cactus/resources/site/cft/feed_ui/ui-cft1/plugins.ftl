<section>
  <h1>Plug-ins</h1>
  <p>
    <strong>Intelligent Pack</strong><br/>
    Intelligent Pack plug-in adds ability to tune your FTP server for production. This plug-in includes features to
    optimize the performance such as limiting amount of simultaneous connections, data transfer speed and limiting
    amount of simultaneously open files. This plug-in divides connections into Local and Remote groups –
    Local Connections are those from a local network and Remote Connections are those from outside of the local
    network. You may apply data transfer speed restrictions to either of those groups.
  </p>
  <p>
    <i>Developers Note:</i> Intelligent Pack wrapper classes may be installed on top of almost any 3rd party
    implementation. And it is recommended to extend the classes of this plug-in rather than the plain core classes.
  </p>
  <p>
    <strong>Impl 3659</strong><br/>
    This plug-in is the implementation of RFC 3659. Almost all FTP clients take advantage of the features outlined
    in the RFC.
  </p>
  <p>
    <strong>Hard File System</strong><br/>
    Hard File System is a file system that works with your hard drive. It hides the actual structure of the hard drive:
    user sees virtual paths (Unix-style) mounted to the real paths on the drive. Hard File System stores home folders
    of all users in the same directory and does not allow users to browse outside of the home folder, but using the
    virtual folders feature you can mount any arbitrary directory to the home folder. For any configured user you may
    create links to directories anywhere on your hard drive, set permissions on any directory, apply restrictions based
    on the names of files, allow/forbid file operations. This plug-in provides total control over your file system.
  </p>
  <p>
    <i>Note:</i> Hard File System sets a transferred.file attribute into user's session on invocation of its save and
    read methods. This attribute points to the file being transferred and may be queried later to implement post
    upload/download logic.
  </p>
  <p>
    <strong>COTTA File System</strong><br/>
    The plug-in uses an underlying COTTA file system. It also hides the actual structure of the file system: user sees
    virtual paths (Unix-style) mounted to the real paths on the drive. The plug-in stores home folders of all users in
    the same directory and does not allow users to browse outside of the home folder, but using the virtual folders
    feature you can mount any arbitrary directory to the home folder. For any configured user you may create links to
    directories anywhere on your file system, set permissions on any directory, apply restrictions based on the names
    of files, allow/forbid file operations. This plug-in provides total control over your file system. As mentioned
    before, the plug-in uses a COTTA file system. The main goal of the COTTA project is that it provides an abstraction
    of a file system. The project contains several file system implementations (FTP, physical, in-memory etc). You can
    mount any of those file system to this plug-in. The COTTA related materials and downloads are available on the
    projects' homepage at <a href="http://cotta.sourceforge.net" target="_blank">http://cotta.sourceforge.net</a>
  </p>
  <p>
    <i>Note:</i> COTTA File System sets a transferred.file attribute into user's session on invocation of its save and
    read methods. This attribute points to the file being transferred and may be queried later to implement post
    upload/download logic.
  </p>
  <p>
    <strong>Gateway</strong><br/>
    Gateway plug-in is the set of modules to allow registered users to sign in into your FTP server. Currently it
    includes modules to load usernames and passwords from Spring XML file, plain XML file, database and Acegi. With
    the latter 2 options you have an ability to update and register new users without restarting your FTP server.
    Currently no other sing in mechanisms exist, but with a little bit of work this plug-in may be extended (with more
    DAO implementations) to utilize other sign in mechanisms.
  </p>
  <p>
    <i>Note:</i> When a user successfully signs in, a new user object is created and saved into user's session under
    the user.object key. This is done in the new PASS command supplied by this plug-in.
  </p>
  <p>
    <i>Note:</i> When using the Acegi module, in addition to the <i>ExternalAcegiUserDAO</i> which is installed by default,
    there is also <i>AcegiUserDAO</i> where you have to set authenticationManager and allowedUserRoles through setters.
    This class is useful if you want to define Acegi configuration in the same beans.xml rather than in an external
    file to reuse the existing objects. There is an example of <i>AcegiUserDAO</i> in the acegi-conf.xml file.
  </p>
  <p>
    <strong>Web Cam</strong><br/>
    This plug-in is for web camera owners who wish to display web camera images on their web site. Almost every web
    camera application knows how to upload an image file to FTP server but most of them cannot submit images to web
    servers directly bypassing the FTP server. When using FTP server the image is saved onto your drive first and then
    picked up by the web site, this step is unnecessary and does absolutely no good to your hard drive. If you need to
    stream your web camera to Servlet/JSP/PHP this plug-in is just for that. Web Cam plug-in wraps existing
    ColoradoFTP file system and intercepts uploads made by selected users. Plug-in then reads uploaded files into
    memory and feeds them to your web server without writing the files to your hard drive. You can upload any files
    (not necessarily pictures) provided your web server knows how to handle those files. Currently it is possible to
    stream files only to a web server via POST, but with a little bit of work this plug-in may be extended (with more
    TASK implementations) to utilize other delivery mechanisms.
  </p>
  <p>
    <i>Note:</i> If you have troubles writing scripts to accept images then take a look
    at <i>LiveStreamServlet.java</i> example. This Java Servlet accepts images, stores them in its cache and gives
    them away upon web page requests. This example (and a deployable WAR) is available in the GIT repository.
  </p>
</section>
