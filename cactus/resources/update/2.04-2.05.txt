
Change log:
1. Removed banner logic from Generic
2. Code refactored
3. Bugfix
4. Banner pack added

Manual update:
1. run Cactus 2.04-2.05.sql
2. deploy Cactus Solr schema
3. remove "inactBanners" from "SystemParams" setting
4. feed Generic FTL, Cactus CP FTL, Toyshop FTL
5. install banner pack (optional)

