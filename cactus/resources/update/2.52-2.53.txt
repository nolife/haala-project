
Change log:
1. Code refactored
2. Java v8, Spring v5, dependencies bumped
3. Automated installation for Vagrant

Manual update:
1. Run Cactus 2.52-2.53.sql
2. Run task to refresh files
3. Feed: Generic JS, Cactus CP FLT, Banner FTL, Toyshop FTL
