Change log:
1. Code refactored
2. UI and feed refactored
3. Libraries upgraded

Manual update:
1. Run Cactus 2.55-2.56.sql
2. Cactus CFT: remove layout-top.ftl, layout-bottom.ftl
3. Cactus ME: remove layout-top.ftl, layout-bottom.ftl
4. Feed Generic: endorsed, files, settings, themes, ui
5. Feed Cactus CFT/ME/CP: files, ui
6. Feed Banner, Wiki, Toyshop: ui
7. Remove labels CP: 01details.012, 00register.016
8. Solr submit files
