
Change log:
1. CMS bugfix
2. Code refactored
3. Upgraded jQuery, jQueryUI, TinyMCE, qTip
4. Rebranded ColdCore.com

Manual update:
1. run Cactus 2.05-2.06.sql
2. feed Generic JS, FTL, button-labels, endorsed/tinymce-4, endorsed/qtip, endorsed/context-menu,
                themes, files/root-etc
3. feed Cactus CP: JS, CSS, x-labels
4. feed Toyshop JS
5. feed Wiki JS
6. feed Banner JS
7. feed site CFT: JS, FTL
8. feed site ME: JS, FTL
9. Generic URL mapping (and chained)
