Change log:
1. Code refactored
2. PayPal integrated
3. Messages rework
4. Lease rework
5. Minor bug fix
6. Nodes manager added
7. Added zip and sql feed
8. Simpler upgrade process

Changes:
1. Cactus 2.56-2.57.sql, 2.56-2.57-feed.sql
2. Solr schema: abyss
3. Feed generic: settings, dwr, ui, failed-call-labels, message-labels, email-labels, files/var
4. Feed Cactus CP: settings, ui, labels
5. Feed URL mappings
6. Solr submit: labels, files, abyss

Upgrade:
1. Run Cactus 2.56-2.57.sql
2. Update Solr schema: abyss
3. Feed https://coldcore.com/files/haala/cactus-2.57-feed.zip
