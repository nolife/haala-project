
Change log:
1. Code refactored
2. Minor bug fix
3. Site chain refactored
4. Facade validators refactored
5. jQuery and themes upgraded

Manual update:
1. Run Cactus 2.54-2.55.sql
2. Feed SYS settings
3. Feed Generic: JS, themes
4. Feed Cactus CP: style
5. Feed Cactus CFT: style
6. Feed Cactus ME: style
7. Feed Toyshop: style
