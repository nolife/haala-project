
Change log:
1. Site multiple locales support
2. Platform horizontal scaling
3. Minor bugfix
4. Replicated EhCache via JMS
5. Heartbeat mechanism
6. Cache improvements
7. Excel plug-in removed

Manual update:
1. run Cactus 2.06-2.07.sql
2. feed Generic JS
