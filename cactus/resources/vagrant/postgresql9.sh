#!/usr/local/bin/bash -e

echo
echo "Installing PostgreSQL 9"
echo "-----------------------"

if grep -Fxq "postgresql" $history
then
   echo "Seems to be already installed"
   echo "Skip!"
else


sudo pkg install -y postgresql96-server

# post install note
# ======================================================================
# To initialize the database, run
#   /usr/local/etc/rc.d/postgresql initdb
# You can then start PostgreSQL by running:
#   /usr/local/etc/rc.d/postgresql start
# For postmaster settings, see ~pgsql/data/postgresql.conf
# NB. FreeBSD's PostgreSQL port logs to syslog by default
#     See ~pgsql/data/postgresql.conf for more info
# To run PostgreSQL at startup, add
#     'postgresql_enable="YES"' to /etc/rc.conf
# ======================================================================

# rc.conf
echo "Patching /etc/rc.conf"
file=/etc/rc.conf
echo "" | sudo tee -a $file
echo '# enable PostgreSQL' | sudo tee -a $file
echo 'postgresql_enable="YES"' | sudo tee -a $file
echo "" | sudo tee -a $file
sudo rm -f $file-e

echo "Initializing the database"
sudo /usr/local/etc/rc.d/postgresql initdb

# postgresql.conf
echo "Patching postgresql.conf"
file=/var/db/postgres/data96/postgresql.conf
sudo sed -i -e "s;#listen_addresses = \'localhost\';listen_addresses = \'*\';g" $file
sudo rm -f $file-e

# pg_hba.conf
echo "Patching pg_hba.conf"
file=/var/db/postgres/data96/pg_hba.conf

echo "" | sudo tee -a $file
echo "# if pgAdmin cannot connect then change this to what it shows" | sudo tee -a $file
echo "host  all  all  10.0.0.0/16  trust" | sudo tee -a $file
echo "host  all  all  192.168.0.0/16  trust" | sudo tee -a $file
echo "" | sudo tee -a $file
sudo rm -f $file-e

echo "Starting the database"
sudo service postgresql start
sleep 5


echo "postgresql" >> $history
echo "Done!"


fi


# ROLLBACK (remove 'postgresql' from history file)
#   sudo pkg remove -y postgresql96-server postgresql96-client
#   sudo rm -rf /var/db/postgres
#   revert /etc/rc.conf
