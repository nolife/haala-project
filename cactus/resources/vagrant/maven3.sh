#!/usr/local/bin/bash -e

echo
echo "Installing Maven 3"
echo "------------------"

if grep -Fxq "maven" $history
then
   echo "Seems to be already installed"
   echo "Skip!"
else


cd /usr/local
sudo wget https://archive.apache.org/dist/maven/maven-3/3.5.2/binaries/apache-maven-3.5.2-bin.tar.gz
sudo tar -xzf apache-maven-3.5.2-bin.tar.gz
sudo rm apache-maven-3.5.2-bin.tar.gz
sudo chown -R $user apache-maven-3.5.2
sudo ln -s /usr/local/apache-maven-3.5.2/bin/mvn /usr/local/bin/mvn

echo "Installing 3rd party JARs"
cd ~

jar=misc-scala-2.12-0.9.jar
wget --no-check-certificate https://bitbucket.org/nolife/misc-scala/downloads/$jar
mvn -q install:install-file -Dfile=$jar -DgroupId=com.coldcore -DartifactId=misc-scala-2.12 -Dversion=0.9 -Dpackaging=jar
rm $jar

jar=misc5-1.03.jar
wget --no-check-certificate https://bitbucket.org/nolife/old-school-libs/downloads/$jar
mvn -q install:install-file -Dfile=$jar -DgroupId=com.coldcore -DartifactId=misc5 -Dversion=1.03 -Dpackaging=jar
rm $jar

jar=email-1.04.jar
wget --no-check-certificate https://bitbucket.org/nolife/old-school-libs/downloads/$jar
mvn -q install:install-file -Dfile=$jar -DgroupId=com.coldcore -DartifactId=email -Dversion=1.04 -Dpackaging=jar
rm $jar

jar=gfx-1.01.jar
wget --no-check-certificate https://bitbucket.org/nolife/old-school-libs/downloads/$jar
mvn -q install:install-file -Dfile=$jar -DgroupId=com.coldcore -DartifactId=gfx -Dversion=1.01 -Dpackaging=jar
rm $jar

jar=InetAddressLocator.jar
wget --no-check-certificate https://bitbucket.org/nolife/haala-project/downloads/InetAddressLocator-2.23.zip
unzip -q InetAddressLocator-2.23.zip
mvn -q install:install-file -Dfile=$jar -DgroupId=net.sf -DartifactId=inet-address-locator -Dversion=2.23 -Dpackaging=jar
rm $jar InetAddressLocator-2.23.zip


echo "maven" >> $history
echo "Done!"


fi


# ROLLBACK (remove 'maven' from history file)
#   sudo rm /usr/local/bin/mvn
#   sudo rm -rf /usr/local/apache-maven-3.5.2 
#   rm -rf ~/.m2
