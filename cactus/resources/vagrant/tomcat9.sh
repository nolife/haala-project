#!/usr/local/bin/bash -e

echo
echo "Installing Tomcat 9"
echo "-------------------"

if grep -Fxq "tomcat" $history
then
   echo "Seems to be already installed"
   echo "Skip!"
else


tomcat_dir=tomcat-cactus

cd /usr/local
sudo wget https://archive.apache.org/dist/tomcat/tomcat-9/v9.0.6/bin/apache-tomcat-9.0.6.tar.gz
sudo tar -xzf apache-tomcat-9.0.6.tar.gz
sudo rm apache-tomcat-9.0.6.tar.gz
sudo chown -R $user apache-tomcat-9.0.6
sudo ln -s apache-tomcat-9.0.6 $tomcat_dir

cd $tomcat_dir


# context.xml
echo "Patching context.xml"

sed -i -e "s;</Context>;;" conf/context.xml

echo "" >> conf/context.xml
echo "  <!-- disable session persistence across restarts -->" >> conf/context.xml
echo '  <Manager pathname="" />' >> conf/context.xml

cat $source_dir/cactus/resources/env/tomcat/container/context.xml >> conf/context.xml

sed -i -e "s;<?xml .*?>;;g" conf/context.xml
sed -i -e "s;<Context>;;g" conf/context.xml

sed -i -e '1i\
<?xml version="1.0" encoding="UTF-8"?>\
' conf/context.xml

sed -i -e '2i\
<Context>\
' conf/context.xml

sed -i -e "s;localhost:5432;$primary_ip:5432;" conf/context.xml

rm -f conf/context.xml-e


# setclasspath.sh
echo "Patching setclasspath.sh"

sed -i -e '2i\
 \
' bin/setclasspath.sh

sed -i -e '3i\
#set Java HOME and OPTS\
' bin/setclasspath.sh

sed -i -e '4i\
JAVA_HOME="/usr/local/java"\
' bin/setclasspath.sh

sed -i -e '5i\
JAVA_OPTS="-Djava.awt.headless=true -Xss256k -Xms512m -Xmx1024m -XX:PermSize=256m -XX:MaxPermSize=256m -XX:+UseConcMarkSweepGC -XX:+CMSPermGenSweepingEnabled -XX:+CMSClassUnloadingEnabled"\
' bin/setclasspath.sh

sed -i -e '6i\
 \
' bin/setclasspath.sh

rm -f bin/setclasspath.sh-e


echo "Installing libraries"
cd lib
wget --no-check-certificate https://jdbc.postgresql.org/download/postgresql-42.2.1.jar
wget http://central.maven.org/maven2/log4j/log4j/1.2.17/log4j-1.2.17.jar
wget http://central.maven.org/maven2/org/slf4j/slf4j-api/1.7.10/slf4j-api-1.7.10.jar
wget http://central.maven.org/maven2/org/slf4j/slf4j-log4j12/1.7.10/slf4j-log4j12-1.7.10.jar
cd ..

echo "Removing default webapps"
mv webapps webapps_default
mkdir webapps

# start it unless other argument given
start=${1:-start}
if [ $start = 'start' ]
then
  echo "Starting Tomcat"
  /usr/local/tomcat-cactus/bin/catalina.sh start
  sleep 5
fi


echo "tomcat" >> $history
echo "Done!"


fi


# ROLLBACK (remove 'tomcat' from history file)
#   /usr/local/tomcat-cactus/bin/catalina.sh stop
#   sudo rm -rf /usr/local/tomcat-cactus
#   sudo rm -rf /usr/local/apache-tomcat-9.0.6
