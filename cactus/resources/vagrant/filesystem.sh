#!/usr/local/bin/bash -e

echo
echo "Creating Filesystem"
echo "-------------------"

if grep -Fxq "filesystem" $history
then
   echo "Seems to be already created"
   echo "Skip!"
else


arg1=${1:-primary}
fs_dir=haala-cactus.fs

# install filesystem
if [ $arg1 = 'primary' ]
then

  cd /usr/local
  sudo wget --no-check-certificate https://bitbucket.org/nolife/haala-project/downloads/haala-cactus.fs.zip
  sudo unzip -q haala-cactus.fs.zip
  sudo rm haala-cactus.fs.zip
  sudo chown -R $user $fs_dir

  echo "Copying feed resources"
  counter=0

  for feed_dir in \
    generic/resources \
    cactus/resources/site/cp \
    cactus/resources/site/me \
    banner/resources \
    toyshop/resources \
    wiki/resources \

  do
    counter=$((counter + 1))
    mkdir $fs_dir/feed/$counter
    cp -r $source_dir/$feed_dir/feed_* $fs_dir/feed/$counter
  done

  echo "Applying final feed"
  mkdir $fs_dir/feed/9
  cp $script_dir/feed-* $fs_dir/feed/9


  # share filesystem as NFS server

  # exports
  echo "Patching /etc/exports"
  file=/etc/exports
  sudo touch $file
  echo "" | sudo tee -a $file
  echo "# share haala-cactus filesystem" | sudo tee -a $file
  echo "/usr/local/haala-cactus.fs" | sudo tee -a $file
  echo "" | sudo tee -a $file
  sudo rm -f $file-e

  # rc.conf
  echo "Patching /etc/rc.conf"
  file=/etc/rc.conf
  echo "" | sudo tee -a $file
  echo '# enable NFS server' | sudo tee -a $file
  echo 'rpcbind_enable="YES"' | sudo tee -a $file
  echo 'nfs_server_enable="YES"' | sudo tee -a $file
  echo 'mountd_enable="YES"' | sudo tee -a $file
  echo 'mountd_flags="-r"' | sudo tee -a $file
  echo 'rpc_lockd_enable="YES"' | sudo tee -a $file
  echo 'rpc_statd_enable="YES"' | sudo tee -a $file
  echo "" | sudo tee -a $file
  sudo rm -f $file-e

  echo "Sharing filesystem"
  sudo service nfsd start
  sudo service mountd reload
  sudo service statd start
  sudo service lockd start

fi

# use filesystem from primary node
if [ $arg1 = 'node' ]
then

  cd /usr/local
  sudo mkdir $fs_dir
  sudo chown -R $user $fs_dir


  # mount filesystem as NSF client

  # rc.conf
  echo "Patching /etc/rc.conf"
  file=/etc/rc.conf
  echo "" | sudo tee -a $file
  echo '# enable NFS client' | sudo tee -a $file
  echo 'nfs_client_enable="YES"' | sudo tee -a $file
  echo 'rpc_lockd_enable="YES"' | sudo tee -a $file
  echo 'rpc_statd_enable="YES"' | sudo tee -a $file
  echo "" | sudo tee -a $file
  sudo rm -f $file-e

  # fstab
  echo "Patching /etc/fstab"
  file=/etc/fstab
  echo "" | sudo tee -a $file
  echo "# haala-cactus NFS" | sudo tee -a $file
  echo "$primary_ip:/usr/local/$fs_dir  /usr/local/$fs_dir  nfs  rw  0  0" | sudo tee -a $file
  echo "" | sudo tee -a $file
  sudo rm -f $file-e

  echo "Mounting filesystem"
  sudo service nfsclient start
  sudo service statd start
  sudo service lockd start
  sudo mount $primary_ip:/usr/local/$fs_dir $fs_dir

fi


echo "filesystem" >> $history
echo "Done!"


fi


# ROLLBACK (remove 'filesystem' from history file)
#   sudo rm -rf /usr/local/haala-cactus.fs
#   revert /etc/exports
#   revert /etc/rc.conf
#   revert /etc/fstab
