#!/usr/local/bin/bash -e

echo
echo "Building Application"
echo "--------------------"

if grep -Fxq "build" $history
then
   echo "Seems to be already created"
   echo "Skip!"
else


arg1=${1:-primary}

# build application
if [ $arg1 != 'node' ]
then

  cd /usr/local
  sudo git clone https://nolife@bitbucket.org/nolife/haala-project.git
  sudo chown -R $user haala-project

  cd haala-project

  # change 'master' if you want to use another branch
  git checkout master
  git pull


  if [ $arg1 = 'scaled' ]
  then
    # hazelcast.xml allows 2 nodes by IP
    echo "Patching hazelcast.xml"
    members=192.168.50.11-12
    file=$source_dir/generic/resources/hibernate/hazelcast.xml
    sed -i -e 's;tcp-ip enabled="false";tcp-ip enabled="true";' $file
    sed -i -e "s;<member>127.0.0.1</member>;<member>$members</member>;" $file
    rm -f $file-e

    # config.properties uses IP filter to identify nodes
    echo "Patching config.properties"
    filter=192.168.
    file=$source_dir/cactus/resources/env/tomcat/config.properties
    sed -i -e "s;heartbeat.ip_prefix=;heartbeat.ip_prefix=$filter;" $file
    rm -f $file-e
  fi


  echo "Building generic"
  mvn -f generic/pom.xml clean install -Dmaven.test.skip=true

  echo "Building cactus"
  mvn -f cactus/pom.xml clean install -Dmaven.test.skip=true

fi

# copy application from primary node
if [ $arg1 = 'node' ]
then

  cd /usr/local
  sudo mkdir -p $source_dir/cactus/webapp
  sudo mkdir -p $source_dir/cactus/resources
  sudo chown -R $user haala-project

  sshpass -p 'vagrant' scp -o StrictHostKeyChecking=no -r $primary_ip:$source_dir/cactus/webapp/target $source_dir/cactus/webapp
  sshpass -p 'vagrant' scp -o StrictHostKeyChecking=no -r $primary_ip:$source_dir/cactus/resources/env $source_dir/cactus/resources

fi


echo "build" >> $history
echo "Done!"


fi


# ROLLBACK (remove 'build' from history file)
#   sudo rm -rf /usr/local/haala-project
