#!/bin/sh -e

#
#
#

export user=vagrant
export source_dir=/usr/local/haala-project
export script_dir=/vagrant
export history=~/haala-install
export primary_ip=192.168.50.11


echo "HAALA CACTUS"
echo "============"
echo 

echo "Installing required packages"
sudo pkg install -y nano wget unzip bash lsof git sshpass


# Steps to run

run_step=${1:-none}

if [ $run_step = 'none' ]
then
  echo
  echo "No step to run"
fi

# non-scaled app for better performance
if [ $run_step = 'one' ]
then
  $script_dir/java8.sh
  $script_dir/maven3.sh
  $script_dir/build.sh
  $script_dir/postgresql9.sh
  $script_dir/pgsql-schema.sh
  $script_dir/solr6.sh
  $script_dir/tomcat9.sh
  $script_dir/filesystem.sh
  $script_dir/deploy.sh
  $script_dir/finalise.sh
fi

# scaled app primary node
if [ $run_step = 'primary' ]
then
  $script_dir/java8.sh
  $script_dir/maven3.sh
  $script_dir/build.sh scaled
  $script_dir/postgresql9.sh
  $script_dir/pgsql-schema.sh scaled
  $script_dir/solr6.sh
  $script_dir/tomcat9.sh
  $script_dir/filesystem.sh
  $script_dir/deploy.sh
  $script_dir/finalise.sh
fi

# scaled app node which uses resources from primary
if [ $run_step = 'node' ]
then
  $script_dir/java8.sh
  $script_dir/build.sh node
  $script_dir/tomcat9.sh
  $script_dir/filesystem.sh node
  $script_dir/deploy.sh
  $script_dir/finalise.sh node
fi


# troubleshooting steps

if [ $run_step = 'java' ]
then
  $script_dir/java8.sh $2
fi

if [ $run_step = 'maven' ]
then
  $script_dir/maven3.sh $2
fi

if [ $run_step = 'build' ]
then
  $script_dir/build.sh $2
fi

if [ $run_step = 'postgresql' ]
then
  $script_dir/postgresql9.sh $2
fi

if [ $run_step = 'pgsql-schema' ]
then
  $script_dir/pgsql-schema.sh $2
fi

if [ $run_step = 'solr' ]
then
  $script_dir/solr6.sh $2
fi

if [ $run_step = 'tomcat' ]
then
  $script_dir/tomcat9.sh $2
fi

if [ $run_step = 'filesystem' ]
then
  $script_dir/filesystem.sh $2
fi

if [ $run_step = 'deploy' ]
then
  $script_dir/deploy.sh $2
fi

if [ $run_step = 'finalise' ]
then
  $script_dir/finalise.sh $2
fi


echo
echo "ALL DONE"
