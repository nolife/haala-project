#!/usr/local/bin/bash -e

echo
echo "Finalising"
echo "----------"

if grep -Fxq "finalise" $history
then
   echo "Seems to be already finalised"
   echo "Skip!"
else


echo "Script to start on boot"
sudo cp $script_dir/my.services /etc/rc.d
sudo chmod ugo+rx /etc/rc.d/my.services

arg1=${1:-primary}

if [ $arg1 = 'node' ]
then
  # remove Solr lines
  sudo sed -i -e 2,3d /etc/rc.d/my.services
  sudo rm -f /etc/rc.d/my.services-e
fi

echo "Verify running services"
netstat -a | grep LISTEN
sleep 5


# primary node waits for feed
if [ $arg1 != 'node' ]
then

  echo "Waiting for Cactus to feed (may take a few minutes)"
  echo "  to monitor the progress:"
  echo "    tail -f /usr/local/tomcat-cactus/logs/*"
  echo "  if nothing is happening:"
  echo "    touch /usr/local/tomcat-cactus/deploy/*.war"

  fed=0
  dir=/usr/local/haala-cactus.fs/feed

  ls $dir
  sleep 5

  while [ $fed -lt 1 ]
  do
    if [ "$(ls -A $dir)" ]; then
      echo -n "."
      sleep 5
    else
      echo "....!"
      fed=1
    fi
  done

fi


echo "finalise" >> $history
echo "Done!"


fi


# ROLLBACK (remove 'finalise' from history file)
#   sudo rm /etc/rc.d/my.services
