﻿Install HAALA Cactus on Vagrant FreeBSD
=======================================

Command line interface
  $     CLI of a Vagrant box  (FreeBSD)
  mac>  CLI of a host machine (Mac)


Installation steps
------------------

Install Vagrant and VirtualBox (or any other supported VM provider)

1. copy content of this directory into an empty directory <directory_name>
2. start then stop vagrant to build a new virtual machine (VM)
     mac> cd <directory_name>
     mac> vagrant up
     mac> vagrant halt
3. open the new VM settings in VirtualBox and copy/paste your NAT MAC address into Vagrant file
     config.vm.base_mac = "08002726C4F8"
4. start vagrant then ssh into it and run the script
     mac> vagrant up
     mac> vagrant ssh
     $ chmod u+x /vagrant/*.sh

     use for better caching performance, non-scalable application
     $ /vagrant/install.sh one

     or if you plan to scale the application across two or more nodes
     $ /vagrant/install.sh primary


What it does
------------

The script will install everything needed to run the application in VM, apply configuration
and deploy, making the application immediately available. If you shut down vagrant, the
application will start again on boot.

Forwarded ports to the host machine
  *) 5433 to access the database in VM with SQL editor
  *) 8082 to access the web application running in VM


Try it out
----------

On the host machine add this to your /etc/hosts (Windows C:\Windows\System32\drivers\etc\hosts)
  mac> sudo nano /etc/hosts
  # Vagrant HAALA
  127.0.0.1  example.org  cpanel.example.org  toys.example.org

Try the following links in Firefox or Chrome browser
  *) http://example.org:8082
  *) http://cpanel.example.org:8082
  *) http://toys.example.org:8082

To sing into the control panel use
  admin / admin
  tester / tester

Enjoy!



Troubleshooting
===============

The script should produce no errors and there should be no errors in any of the logs

1. Script saves each completed step into ~/haala-install
   On completion it should read in the following order
     $ cat ~/haala-install

     java
     maven
     build
     postgresql
     pgsql-schema
     solr
     tomcat
     filesystem
     deploy
     finalise

   If some step is missing then you need to rollback the missing step and all of the steps after it
   See ROLLBACK section at the end of each shell file run by the script and do what it says.
   Then correct the error and re-run the script to continue from the failed step.

2. If some of the feed is broken then the dotted line in finalise step will keep going forever.
   Check the tomcat log for any errors and try to resolve if found.
   Then rollback the filesystem step and all of the steps after it and re-run the script.

3. If you interrupt the running script then it should continue from the point where it stopped.
   But you need to rollback the current step and only then re-run the script (see point 1.)

4. Script produces immense output and stops on a failed step. It should be easy to correct errors
   on abnormal script termination. But if the application is not running in the very end, it may be
   hard to find what failed, then step by step may help (see point 5.)

   To run a single step
     $ /vagrant/install.sh <step_name>

   E.g. to run just "build" step use "install.sh build" but remember to rollback first.

5. To run the script step by step, use the order from point 1. and check for errors
     $ /vagrant/install.sh java
     ...
     $ /vagrant/install.sh finalise

6. While the application feeds it prints 'task' warnings in Tomcat log, ignore those
     10:26:18 WARN [CACTUS] [ToyshopSolrSubmitProductsTask] Not configured task ToyshopSolrSubmitProductsTask



Scaling to another Vagrant node
===============================

Follow the earlier steps to build and run primary node. It must be up and running prior
to building a secondary node to share resources.


Node installation steps
-----------------------

1. copy content of this directory into an empty directory <node2>
2. replace Vagrant file with Vagrant-node2 (different IP, port forwards, low memory)
3. start vagrant and ssh into it as in "readme.txt" and run the script (use 'node' as argument!)
   $ chmod u+x /vagrant/*.sh
   $ /vagrant/install.sh node


What it does
------------

The primary node shares resources (Solr, database and filesystem). The script follows the same steps
to build <node2> (with some minor changes) thus there should be no errors. <node2> is configured to
use the resources of primary node. To add another node you need to configure the database to accept
connections from a new IP (already configured for 192.168.x.x) then add this IP to hazelcast.xml and
redeploy on every node.

Forwarded ports to the host machine:
  *) 8084 to access the web application running in VM


Try it out
----------

You now have 2 nodes running
  *) http://example.org:8082
  *) http://example.org:8084


Load balancer
-------------

Tomcat sessions are not synchronised and require singing into each control panel.
If you install a load balancer then make sure to use sticky sessions.
