#!/usr/local/bin/bash -e

echo
echo "Configuring Schema"
echo "------------------"

if grep -Fxq "pgsql-schema" $history
then
   echo "Seems to be already configured"
   echo "Skip!"
else


db_name=haala-cactus

psql -U postgres -w -d postgres -q -f $script_dir/pgsql-initial.sql

echo "Applying scripts"

for sql_script in \
  generic/resources/db_pgsql/initial/010-create-schema.sql \
  generic/resources/db_pgsql/initial/020-data.sql \
  generic/resources/db_pgsql/initial/030-countries.sql \
  generic/resources/db_pgsql/initial/040-locations.sql \
  generic/resources/db_pgsql/initial/042-locations.sql \
  generic/resources/db_pgsql/initial/050-postcodes.sql \
  cactus/resources/db_pgsql/initial/020-data.sql \
  banner/resources/db_pgsql/initial/010-create-schema.sql \
  banner/resources/db_pgsql/initial/020-data.sql \
  toyshop/resources/db_pgsql/initial/010-create-schema.sql \
  toyshop/resources/db_pgsql/initial/020-data.sql \
  wiki/resources/db_pgsql/initial/010-create-schema.sql \
  wiki/resources/db_pgsql/initial/020-data.sql \

do
  psql -U postgres -w -d $db_name -q -f $source_dir/$sql_script
done

echo "Updating settings table"
psql -U postgres -w -d $db_name -q -c "update settings set value = '/usr/local/haala-cactus.fs' where key = 'FsPath' and site = 'sys'"
psql -U postgres -w -d $db_name -q -c "update settings set value = 'http://$primary_ip:8983/solr/haala-cactus' where key = 'SolrURL' and site = 'sys'"

echo "Updating domains table"
psql -U postgres -w -d $db_name -q -c "update domains set domain = 'cpanel.example.org' where domain_site = 'cp'"
psql -U postgres -w -d $db_name -q -c "update domains set domain = 'toys.example.org' where domain_site = 'toy'"


arg1=${1:-none}

if [ $arg1 = 'scaled' ]
then
  echo "Patching SystemParams"
  psql -U postgres -w -d $db_name -q -c "update settings set value = concat(value, ', scaled=y') where key = 'SystemParams' and site = 'sys'"
fi  


echo "pgsql-schema" >> $history
echo "Done!"


fi


# ROLLBACK (remove 'pgsql-schema' from history file)
# do postgresql rollback OR remove db_user and haala-cactus schema with SQL editor
