#!/usr/local/bin/bash -e

echo
echo "Deploying Application"
echo "---------------------"

if grep -Fxq "deploy" $history
then
   echo "Seems to be already deployed"
   echo "Skip!"
else


tomcat_dir=tomcat-cactus

cd /usr/local/$tomcat_dir
rm -rf deploy
mkdir deploy
cp $source_dir/cactus/webapp/target/*.war deploy

war_path=$(find deploy/*)
war_file=$(basename $war_path)

# ROOT.xml
echo "Patching ROOT.xml"
mkdir -p conf/Catalina/localhost
cp $source_dir/cactus/resources/env/tomcat/container/ROOT.xml conf/Catalina/localhost
sed -i -e "s;cactus-1.80.war;$war_file;" conf/Catalina/localhost/ROOT.xml
rm -f conf/Catalina/localhost/ROOT.xml-e


echo "deploy" >> $history
echo "Done!"


fi


# ROLLBACK (remove 'deploy' from history file)
# do tomcat rollback OR
#   rm -rf /usr/local/tomcat-cactus/deploy
#   rm /usr/local/tomcat-cactus/conf/Catalina/localhost/ROOT.xml
