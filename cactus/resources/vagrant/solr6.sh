#!/usr/local/bin/bash -e

echo
echo "Installing Solr 6"
echo "-----------------"

if grep -Fxq "solr" $history
then
   echo "Seems to be already installed"
   echo "Skip!"
else


solr_dir=solr

cd /usr/local
sudo wget https://archive.apache.org/dist/lucene/solr/6.6.0/solr-6.6.0.tgz
sudo tar -xzf solr-6.6.0.tgz
sudo rm solr-6.6.0.tgz
sudo chown -R $user solr-6.6.0
sudo ln -s solr-6.6.0 $solr_dir

echo "Creating Solr core"
cd $solr_dir

cd server/solr
wget --no-check-certificate https://bitbucket.org/nolife/haala-project/downloads/solr-home-5.0.0.zip
unzip -q solr-home-5.0.0.zip
rm solr-home-5.0.0.zip
cd ../..

# schema.xml
echo "Patching schema.xml"
schema=server/solr/haala-cactus/conf/schema.xml

cp $source_dir/cactus/resources/solr-schema-1.5.xml $schema

for schema_part in \
  banner \
  toyshop \
  wiki \

do
  part=$source_dir/$schema_part/resources/solr-schema-part.xml

  sed -i -e "s;</schema>;;" $schema
  cat $part >> $schema
done

sed -i -e "s;<?xml .*?>;;g" $schema
sed -i -e "s;<schema>;;g" $schema

sed -i -e '1i\
<?xml version="1.0" encoding="UTF-8"?>\
' $schema

rm -f $schema-e

# start it unless other argument given
start=${1:-start}
if [ $start = 'start' ]
then
  echo "Starting Solr"
  /usr/local/solr/bin/solr start -m 256m
  sleep 5
fi


echo "solr" >> $history
echo "Done!"


fi


# ROLLBACK (remove 'solr' from history file)
#   /usr/local/solr/bin/solr stop
#   sudo rm -rf /usr/local/solr
#   sudo rm -rf /usr/local/solr-6.6.0
