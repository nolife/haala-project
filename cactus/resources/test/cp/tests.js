
QUnit.module( "util" );

QUnit.test( "panelFormInput should create JSON", function( assert ) {
  var fixture = $( "#qunit-fixture" );
  fixture.append(
      "<form id='page-form'>" +
          "<div class='first-name'>" +
            "<input />" +
            "<p class='error'></p>" +
          "</div>" +
      "</form>" );
  fixture.append(
      "<div class='wrapper'>" +
          "<div class='currency'>" +
            "<select />" +
          "</div>" +
      "</div>" );


  var hu = Haala.Util;
  var result = hu.panelFormInput("first-name", { require: true, length: [2] });
  var expected = { element: "#page-form .first-name input", key: "firstName",
                   validate: { element: "#page-form .first-name .error", require: true, length: [2] }};
  assert.deepEqual( result, expected, "Full JSON object" );

  result = hu.panelFormInput("unknown", { require: true });
  assert.deepEqual( result, {}, "Non-existent element is empty JSON" );

  result = hu.panelFormInput("first-name");
  expected = { element: "#page-form .first-name input", key: "firstName" };
  assert.deepEqual( result, expected, "JSON without validation" );

  result = hu.panelFormInput("currency", null, { root: ".wrapper" });
  expected = { element: ".wrapper .currency select", key: "currency" };
  assert.deepEqual( result, expected, "Another root through opts" );
});
