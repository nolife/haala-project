#!/bin/sh

# Update version
# "versions-cactus 123-SNAPSHOT" will update modules and parent versions to 123-SNAPSHOT
# It also does this for all of the Cactus packs

mvn -f banner/pom.xml   versions:set versions:update-parent versions:commit -DnewVersion=$1 -DparentVersion=[$1] -DallowSnapshots=true
mvn -f contrib/pom.xml  versions:set versions:update-parent versions:commit -DnewVersion=$1 -DparentVersion=[$1] -DallowSnapshots=true
mvn -f toyshop/pom.xml  versions:set versions:update-parent versions:commit -DnewVersion=$1 -DparentVersion=[$1] -DallowSnapshots=true
mvn -f wiki/pom.xml     versions:set versions:update-parent versions:commit -DnewVersion=$1 -DparentVersion=[$1] -DallowSnapshots=true

mvn -f cactus/pom.xml   versions:set versions:update-parent versions:commit -DnewVersion=$1 -DparentVersion=[$1] -DallowSnapshots=true
