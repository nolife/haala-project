# The Project HAALA

## The open source web development platform.

Project HAALA is the open source project built for delivering web applications, which are easy to customize and
adapt to your business model. It provides robust, multilingual, multi-tenancy websites, built-in CMS with plenty
of features.

The project is built on cutting edge technologies requiring developer's effort to produce sophisticated web
applications. On the other hand, creating a simple website is as easy as applying design and adding pages.
And it comes with CMS and demo websites.

Learn more by reading [Documentation and Guides](http://coldcore.com/project/haala.htm).

Feedback email ** cftp@coldcore.com **

## Features

* Scala for the backend and HTML with jQuery for the frontend. With well known Java web application technologies
  such as Spring Framework, Hibernate, Freemarker, JSP and more.

* Resources are kept in the database, including URL mappings and page templates. Updating CSS, JavaScript, HTML
  is easy and does not require redeploy.

* Freemarker for page templates allows you to write custom macros and backend directives. Or you could use plain
  old JSPs with tag libraries.

* Design websites using your favorite frameworks and tools as long as those do not conflict with jQuery.

* Multilingual support can even apply different Look & Feel per language.

* It is fast. Search engine support enables faster database queries and user searches. On top of that, frontend
  and backend caches for extra speed.

* CMS with quite a few options: modify a website by clicking directly on design elements, modify its pages and
  templates with WYSIWYG editor, or change resources in plain text and upload files.

* More than one website? Easy!

* Horizontal scaling.

* Open source.

## How to use it?

Write your own web application following the project structure, with database SQL, view templates, services and
other resources. There are also Placeholders you can copy and develop on top of those. Read
[this article](https://contented-cows.blogspot.com/2018/04/bookman-haala-add-on-in-java.html)
if you would rather use Java 8 instead of Scala and annotations instead of XML.

## Examples and Demo

* http://wh-2016.coldcore.com

    Estate agency website with mobile friendly design.

* http://wh-2010.coldcore.com

    The very same estate agency but with old design.

* http://coldcore.com

    The application includes this as demo.

* http://toys.coldcore.com

    The application also includes Toy Shop demo.

* http://cftp.coldcore.com

    ColoradoFTP website, included as demo.

## Installation

Requirements, configuration, step-by-step guide and automated installation scripts are
[available here](https://bitbucket.org/nolife/haala-project/src/master/cactus/).

## Contribution policy

Contributions via pull requests are gladly accepted from their original author. Along with any pull requests,
please state that the contribution is your original work and that you license the work to the project under
the project's open source license. Whether or not you state this explicitly, by submitting any copyrighted material
via pull request, email, or other means you agree to license the material under the project's open source license
and warrant that you have the legal authority to do so.

## License

This code is open source software licensed under the
[GNU Lesser General Public License v3](http://www.gnu.org/licenses/lgpl-3.0.en.html).
