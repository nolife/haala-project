#!/bin/sh

# Update version
# "versions-generic 123-SNAPSHOT" will update modules versions to 123-SNAPSHOT

mvn -f generic/pom.xml versions:set versions:commit -DnewVersion=$1
